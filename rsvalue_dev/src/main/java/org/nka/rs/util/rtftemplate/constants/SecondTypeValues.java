package org.nka.rs.util.rtftemplate.constants;

/**
 * Перечисление, в котором содержатся статические данные
 * для форм выписок 2КС и 2НБ
 */
public enum SecondTypeValues {

    NAME("ВЫПИСКА ОТ %s № %s\nО КАДАСТРОВОЙ СТОИМОСТИ 1 МЕТРА КВАДРАТНОГО ЗЕМЕЛЬ\nОЦЕНОЧНОЙ ЗОНЫ"),

    NB_VALUE(" НА 1 ЯНВАРЯ %s ГОДА ДЛЯ ОПРЕДЕЛЕНИЯ\nНАЛОГОВОЙ БАЗЫ ЗЕМЕЛЬНОГО НАЛОГА"),

    TNAME("\nОписание земельного участка по данным обращения (заявления)"),

    MAIN_TFIELD_CADASTER_VALUE("Кадастровая стоимость 1 метра квадратного земель оценочной зоны%s"),

    TFIELD_ADDRESS("Адрес (местоположение) условного земельного участка"),

    ATTACHMENT_COMMON("Схема расположения <***> оценочной зоны № %s по виду функционального использования земель «%s»"),

    ATTACHMENT("ПРИЛОЖЕНИЕ:\n");

    SecondTypeValues(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }
}
