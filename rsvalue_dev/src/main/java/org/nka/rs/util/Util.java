package org.nka.rs.util;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zgurskaya on 04.10.2016.
 */
public class Util  {

    private static final String OBFUSCATE_KEY = "FBF6D0BF723286A8BC9A407EB4FC6FB4094D3554C320829A2DD7B8DB7588B87907D57BACB29B6BD5CCB1D54F35F48672C15FF1739BB266050F68E0D19D5A2131";
    private static final String OBFUSCATE_KEY_SHORTER = "FBF6D0BF723286A8BC9A407EB4FC6FB4094D3554C320829A2DD7B8DB7588B87907D57BACB29B6BD5CCB1D54F35F48672C15FF1739BB266050F68E0D19D5A2131";

    //метод создания токена
    public static String createToken(String username) {
        StringBuilder object = new StringBuilder();
        SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy:HH:mm");
        Date date = new Date();
        String currentDate = format.format(date);

        object.append(username);
        object.append(":");
        object.append(currentDate);
        object.append(":");

        return get_SHA_512_SecurePassword(object.toString(), OBFUSCATE_KEY_SHORTER);
    }

    //метод для шифрования пароля в реквесте и токена.
    // todo Для шифрования пароля соль пока не передается
    public static String get_SHA_512_SecurePassword(String passwordToHash, String salt) {

        String generatedPassword = null;
        if (passwordToHash != null) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-512");
                md.update(salt.getBytes("UTF-8"));
                byte[] bytes = md.digest(passwordToHash.getBytes("UTF-8"));
                generatedPassword = DatatypeConverter.printHexBinary(bytes);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            generatedPassword = "";
        }
        return generatedPassword;
    }

}
