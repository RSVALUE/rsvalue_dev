package org.nka.rs.util.rtftemplate.functions;

import com.lowagie.text.*;

import static org.nka.rs.util.rtftemplate.functions.Constants.*;

/**
 * Класс, в котором расположены функции для работы с ячейками
 */
public class CellFunctions {

    /**
     * Инициализирует и возвращает объект типа Cell
     *
     * @param phrase  Текст, помещаемый в параграф
     * @param colSpan Объединение столбцов
     * @param hAlign  Тип горизонтального выравнивания
     * @param font    Шрифт, который будет использоваться
     * @return Cell с заданными значениями полей
     */
    private static Cell addCell(String phrase, int colSpan, int hAlign, Font font) throws BadElementException {
        Cell cell = new Cell(new Phrase(phrase, font));
        cell.setColspan(colSpan);
        cell.setHorizontalAlignment(hAlign);
        return cell;
    }

    /**
     * Инициализирует и возвращает объект типа Cell. Вертикальное
     * выравнивание установлено по центру. Параметр <i>header</i>
     * установлен в true - ячейка обозначена как header Таблицы
     *
     * @param phrase  Текст, помещаемый в параграф
     * @param colSpan Объединение столбцов
     * @param hAlign  Тип горизонтального выравнивания
     * @param font    Шрифт, который будет использоваться
     * @return Cell с заданными значениями полей
     */
    private static Cell addHeaderCell(String phrase, int colSpan, int hAlign, Font font) throws BadElementException {
        Cell cell = new Cell(new Phrase(phrase, font));
        cell.setColspan(colSpan);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(hAlign);
        cell.setHeader(true);
        return cell;
    }

    /**
     * Добавляет данные с указанным параметром ColSpan в ячейку
     * Если данные добавляются в столбец "Адрес земельного участка",
     * то выравнивание идет по левому краю, иначе выравнивание будет
     * по центру столбцы
     *
     * @param phrase  Текст, помещаемый в ячейку
     * @param colSpan Объединение столбцов
     * @return Cell с заданными значениями полей
     */
    public static Cell addData(String phrase, int colSpan) throws BadElementException {
        if (colSpan == COLADDRESS) {
            return addCell(phrase, colSpan, Element.ALIGN_LEFT, TIMES_SMALL);
        } else {
            return addCell(phrase, colSpan, Element.ALIGN_CENTER, TIMES_NORMAL);
        }
    }

    /**
     * Добавляет данные с указанным параметром ColSpan в ячейку
     * Если данные добавляются в столбец "Адрес земельного участка",
     * то выравнивание идет по левому краю, иначе выравнивание будет
     * по центру столбцы. Ячейка является header'ом таблицы.
     *
     * @param phrase  Текст, помещаемый в ячейку
     * @param colSpan Объединение столбцов
     * @return Cell с заданными значениями полей
     */
    public static Cell addHeaderData(String phrase, int colSpan) throws BadElementException {
        if (colSpan == COLADDRESS) {
            return addHeaderCell(phrase, colSpan, Element.ALIGN_LEFT, TIMES_SMALL);
        } else {
            return addHeaderCell(phrase, colSpan, Element.ALIGN_CENTER, TIMES_NORMAL);
        }
    }

    /**
     * Возвращает ячейку Cell с текстом <i>phrase</> и параметром объединения
     * столбков <i>ColSpan</>
     *
     * @param phrase  Текст, помещаемый в ячейку
     * @param colSpan Объединение столбцов
     * @return Cell с заданными значениями, выравниванием по левому краю и жирным шрифтом TimesNewRoman 12-го размера
     */
    public static Cell addCellBold(String phrase, int colSpan) throws BadElementException {
        return addCell(phrase, colSpan, Element.ALIGN_LEFT, TIMES_NORMAL_BOLD);
    }

    /**
     * Добавляет ячейке параметр RowSpan - объединение строк
     *
     * @param cell    Ячейку, которой добавляется объединение строк
     * @param rowSpan Количество объединяемых строк
     * @return Cell с объединенными строками
     */
    public static Cell addRowSpan(Cell cell, int rowSpan) {
        cell.setRowspan(rowSpan);
        return cell;
    }

    /**
     * Устанавливается свойство <i>setBorder</i> получаемой ячейки cell
     * в значение NO_BORDER
     *
     * @param cell Ячейка, к которой применяется свойство <i>setBorder</i>
     * @return Cell с установленным свойством <i>setBorder</i>
     */
    public static Cell addNoBorder(Cell cell) {
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    /**
     * Инициализирует и возвращает ячейку. Устанавливает в нее текст <i>phrase</>.
     * Устанавливает параметры setBorder в значение NO_BORDER, вертикальное выравнивание
     * по верхнему краю и горизонтальное выравнивание по левому краю.
     *
     * @param phrase Текст, помещаемый в ячейку
     * @return Cell с установленными по умолчанию значениями
     */
    private static Cell addDataCell(String phrase) throws BadElementException {
        Cell cell = new Cell(new Phrase(phrase, TIMES_NORMAL));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_TOP);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        return cell;
    }

    /**
     * Инициализирует и возвращает ячейку. Устанавливает в нее элемент paragraph.
     * Устанавливает парметры ColSpan в значение TABLE_COLS, Border в значение
     * NO_BORDER и горизонтальное выравнивание по верхнему краю
     *
     * @param paragraph Параграф, который будет размещаться внутри ячейки
     * @return Cell с установленными параметрами
     */
    public static Cell addSignatureHeaderCell(Paragraph paragraph) throws BadElementException {
        Cell cell = new Cell(paragraph);
        cell.setColspan(TABLE_COLS);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_TOP);
        return cell;
    }

    /**
     * Ячейка инициализируется функцией addDataCell(String phrase). После чего
     * добавляет ячейку параметр ColSpan со значение COLSPANRIGHT.
     *
     * @param phrase Текст, помещаемый в ячейку
     * @return Cell с установленными параметрами
     */
    public static Cell addRightCell(String phrase) throws BadElementException {
        Cell cell = addDataCell(phrase);
        cell.setColspan(COLSPANRIGHT);
        return cell;
    }

    /**
     * Ячейка инициализируется функцией addDataCell(String phrase). После чего
     * добавляет ячейку параметр ColSpan со значение COLSPANLEFT.
     *
     * @param phrase Текст, помещаемый в ячейку
     * @return Cell с установленными параметрами
     */
    public static Cell addLeftCell(String phrase) throws BadElementException {
        Cell cell = addDataCell(phrase);
        cell.setColspan(COLSPANLEFT);
        return cell;
    }

    /**
     * Инициализирует ячейку с текстом <i>phrase</i>, выравниванием по центру
     * и шрифтом TimesNewRoman размера 12
     *
     * @param phrase Текст, помещаемый в ячейку
     * @return Cell с допотнительными параметрами
     */
    public static Cell addSignatureCell(String phrase) throws BadElementException {
        return addCell(phrase, 0, Element.ALIGN_CENTER, TIMES_SMALL);
    }
}