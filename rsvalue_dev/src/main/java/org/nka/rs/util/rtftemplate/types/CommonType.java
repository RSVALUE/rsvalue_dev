package org.nka.rs.util.rtftemplate.types;

import com.lowagie.text.*;
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter;
import org.nka.rs.util.rtftemplate.constants.CommonTypeValues;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.nka.rs.util.rtftemplate.constants.Fonts.*;

/**
 * Класс, в котором содержатся обобщенные статические данные и функции
 * для всех форм выписок.
 */
public class CommonType {

    /**
     * Путь к логотипу НКА
     */
    private final static String LOGO_DEST = CommonType.class.getClassLoader().getResource("nka-logo.png").toString();

    /**
     * Количество колонок в таблице шапки
     */
    private final static int HEADER_COLS = 1;

    /**
     * Ширина картинки
     */
    private final static float LOGO_WIDTH = 44.75F;

    /**
     * Высота картинки
     */
    private final static float LOGO_HEIGHT = 19.25F;

    /**
     * Размер шрифта надстрочного текста
     */
    private final static int TEXT_RISE = 10;

    /**
     * Размер отступа
     */
    final static int PADDING = 5;

    /**
     * Размер отступа
     */
    final static int BIG_PADDING = 9;

    /**
     * Ширина таблицы
     */
    final static int WIDTH = 100;

    /**
     * Сноски начинаются с первой
     */
    final static int ONE_STAR = 1;

    /**
     * Сноски начинаются со второй
     */
    final static int TWO_STAR = 2;

    /**
     * Отступ после параграфа
     */
    private final static float NORMAL_LEADING = 12F;

    /**
     * Отступ после параграфа
     */
    final static float SMALL_LEADING = 11F;

    /**
     * Функция, создающая и возвращающая новый объект типа RtfHeaderFooter
     * с выравниванием текста по правому краю
     *
     * @param header Строка-header
     * @return HeaderFooter для добавления в документ
     */
    public static HeaderFooter addHeader(String header) throws BadElementException {
        return new RtfHeaderFooter(addParagraph(Element.ALIGN_RIGHT, header));
    }

    /**
     * Функция, создающая и возвращающая новый объект типа Paragraph с заданным
     * выравниванием текста и содержащий все принимаемые объекты.
     *
     * @param hAlign тип горизонтального выравнивания
     * @param data Объекты, помещаемые в параграф
     * @return Paragraph с содержимым
     */
    public static Paragraph addParagraph(int hAlign, Object... data) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(hAlign);
        paragraph.setFont(TIMES_NORMAL);
        paragraph.setLeading(NORMAL_LEADING);
        paragraph.addAll(Arrays.asList(data));
        return paragraph;
    }

    /**
     * Функция, создающая и возвращающая новый объект типа Paragraph с заданным
     * выравниванием текста, шрифтом 9-го размера и содержащим принимаемую строку
     *
     * @param hAlign тип горизонтального выравнивания
     * @param phrase Строка, помещаемая в параграф
     * @return Paragraph с содержимым
     */
    private static Paragraph addSmallParagraph(int hAlign, String phrase) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(hAlign);
        paragraph.setFont(TIMES_SMALL);
        paragraph.setLeading(NORMAL_LEADING);
        paragraph.add(phrase);
        return paragraph;
    }

    /**
     * Функция, создающая и возвращающая новый объект типа Paragraph с заданным
     * выравниванием текста, жиршым шрифтом 11-го размера и содержащим все принимаемые объекты.
     *
     * @param hAlign тип горизонтального выравнивания
     * @param data Объекты, помещаемые в параграф
     * @return Paragraph с содержимым
     */
    public static Paragraph addBoldParagraph(int hAlign, Object... data) throws BadElementException {
        Paragraph paragraph = addParagraph(hAlign, data);
        paragraph.setFont(TIMES_NORMAL_BOLD);
        return paragraph;
    }

    /**
     * Функция, создающая и возвращающая новый объект типа Paragraph с выравниванием
     * текста по центру, отступом после абзаца и содержащим принимаемую строку.
     *
     * @param phrase Строка, помещаемая в параграф
     * @return Paragraph с содержимым
     */
    private static Paragraph addSimpleParagraph(String phrase) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setFont(TIMES_NORMAL);
        paragraph.setLeading(NORMAL_LEADING);
        paragraph.add(phrase);
        return paragraph;
    }

    /**
     * Функция, создающая и возвращающая новый объект типа Paragraph с заданным
     * выравниваниемтекста, отступом после абзаца и содержащим принимаемую строку.
     *
     * @param hAlign тип горизонтального выравнивания
     * @param phrase Строка, помещаемая в параграф
     * @return Paragraph с содержимым
     */
    private static Paragraph addSimpleParagraph(int hAlign, String phrase) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(hAlign);
        paragraph.setFont(TIMES_NORMAL);
        paragraph.setLeading(NORMAL_LEADING);
        paragraph.add(phrase);
        return paragraph;
    }

    /**
     * Функция формирующая таблицу-шапку документа, включающая в себя название ГУП,
     * название РС и лого НКА.
     *
     * @return Table с содержимым
     */
    public static Table addHeaderTable() throws BadElementException, IOException {
        Table table = new Table(HEADER_COLS);
        table.setWidth(WIDTH);
        table.addCell(addHeaderCell(CommonTypeValues.HEADER_CONTINUE.getValue()));
        table.addCell(addLogoCell());
        table.addCell(addHeaderEndCell(CommonTypeValues.HEADER_END.getValue()));
        return table;
    }

    /**
     * Фукнция формирующая ячейку, у который видимая только граница сверху, а
     * выравнивание текста по центру
     *
     * @param phrase Строка, помещаемая в ячейку
     * @return Cell с содержимым
     */
    private static Cell addHeaderCell(String phrase) throws BadElementException {
        Cell cell = new Cell(addSimpleParagraph(phrase));
        cell.setBorder(Rectangle.TOP);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку Cell с логтипом НКА. Получает картинку nka-logo.png,
     * скалирует ее до необходимых размеров и помещает в ячейку без видимых границ.
     * Устанавливает вертикальное выравнивание по нижней границу, а горизонтальное -
     * по центру.
     *
     * @return Cell с логотипом
     */
    private static Cell addLogoCell() throws BadElementException, IOException {
        Image image = Image.getInstance(LOGO_DEST);
        image.scaleAbsolute(LOGO_WIDTH, LOGO_HEIGHT);
        Cell cell = new Cell(image);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку с наименованием НКА
     *
     * @param phrase Константная строка с наименованием ГУП НКА
     * @return Cell с содержимым
     */
    private static Cell addHeaderEndCell(String phrase) throws BadElementException {
        Cell cell = addHeaderCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку со строкой <i>phrase</i>,
     * объединением столбцов colSpan и объединением строк rowSpan
     *
     * @param phrase Строка, помещаемая в ячейку
     * @param colSpan Количество объединяемых столбцов
     * @param rowSpan Количество объединяемых строк
     * @return Cell с содержимым
     */
    static Cell addHeaderCell(String phrase, int colSpan, int rowSpan) throws BadElementException {
        Cell cell = addDataCell(phrase);
        addRowSpan(cell, rowSpan);
        addColSpan(cell, colSpan);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку со строкой <i>phrase</i> и
     * объединением столбцов colSpan
     *
     * @param phrase Строка, помещаемая в ячейку
     * @param colSpan Количество объединяемых столбцов
     * @return Cell с содержимым
     */
    static Cell addMainDataCell(String phrase, int colSpan) throws BadElementException {
        Cell cell = addDataCell(phrase);
        cell.setColspan(colSpan);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку с параграфом <i>paragraph</i>,
     * объединением столбцов colSpan и горизонтальным выравниванием hAlign
     *
     * @param paragraph Параграф, помещаемый в ячейку
     * @param colSpan Количество объединяемых столбцов
     * @param hAlign Тип горизонтального выравнивания
     * @return Cell с содержимым
     */
    static Cell addMainDataCell(Paragraph paragraph, int colSpan, int hAlign) throws BadElementException {
        Cell cell = new Cell(paragraph);
        cell.setColspan(colSpan);
        cell.setHorizontalAlignment(hAlign);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку со строкой <i>phrase</i>,
     * вертикальным выравниванием vAlign и горизонтальным выравниванием
     * hAlign
     *
     * @param vAlign Тип вертикального выравнивания
     * @param hAlign Тип горизонтального выравнивания
     * @param phrase Строка, помещаемая в ячейку
     * @return Cell с содержимым
     */
    static Cell addDataCell(int vAlign, int hAlign, String phrase) throws BadElementException {
        Cell cell = new Cell(addSimpleParagraph(hAlign, phrase));
        cell.setVerticalAlignment(vAlign);
        cell.setHorizontalAlignment(hAlign);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку с параграфом <i>paragraph</i>,
     * вертикальным выравниванием vAlign и горизонтальным выравниванием
     * hAlign
     *
     * @param vAlign Тип вертикального выравнивания
     * @param hAlign Тип горизонтального выравнивания
     * @param paragraph Параграф, помещаемый в ячейку
     * @return Cell с содержимым
     */
    private static Cell addSmallDataCell(int vAlign, int hAlign, Paragraph paragraph) throws BadElementException {
        Cell cell = new Cell(paragraph);
        cell.setVerticalAlignment(vAlign);
        cell.setHorizontalAlignment(hAlign);
        return cell;
    }

    /**
     * Функция, создающая и возвращающая ячейку со строкой <i>phrase</i>,
     * а так же вертикальным и горизонтальным выравниванием текста по центру
     *
     * @param phrase Строка, помещаемая в ячейку
     * @return Cell с содержимым
     */
    static Cell addDataCell(String phrase) throws BadElementException {
        Cell cell = new Cell(addSimpleParagraph(phrase));
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    }

    /**
     * Функция, добавляющая параметр NO_BORDER принимаемому объекту cell
     *
     * @param cell Модифицируемая ячейка
     * @return Модифицируемый Cell
     */
    private static Cell addNoBorder(Cell cell) throws BadElementException {
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    /**
     * Функция, добавляющая параметр RowSpan принимаемому объекту cell
     *
     * @param cell Модифицируемая ячейка
     * @param rowSpan Количество объединяемых строк
     * @return Модифицируемый Cell
     */
    private static Cell addRowSpan(Cell cell, int rowSpan) {
        cell.setRowspan(rowSpan);
        return cell;
    }

    /**
     * Функция, добавляющая параметр ColSpan принимаемому объекту cell
     *
     * @param cell Модифицируемая ячейка
     * @param colSpan Количество объединяемых столбцов
     * @return Модифицируемый Cell
     */
    private static Cell addColSpan(Cell cell, int colSpan) {
        cell.setColspan(colSpan);
        return cell;
    }

    /**
     * Функция, инкапсулирующия базовый метод String.format() для строк
     * с одним форматируемым параметром
     *
     * @param value Форматируемая строка
     * @param formatValue Форматирующее значение
     * @return Отформатированная строка
     */
    static String fmtString(String value, String formatValue) {
        return String.format(value, formatValue);
    }

    /**
     * Функция, формирующая и возвращающая объект типа Paragraph с основаниями выписки
     *
     * @param foundationList Числовые параметры основания
     * @param foundationSet Набор основания
     * @param starCount Параметр, отражающий количество сносок
     * @return Paragraph с содержимым
     */
    public static Paragraph addFoundations(List<String> foundationList, Set<Integer> foundationSet, int starCount) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
        paragraph.setFont(TIMES_SMALL);
        paragraph.setLeading(SMALL_LEADING);
        paragraph.add(CommonTypeValues.FOUNDATION.getValue());
        addToParagraph(paragraph, foundationList, foundationSet, starCount);
        return paragraph;
    }

    /**
     * Функция добавляющая в параграф основания. Проходится по номерам оснований
     * в наборе и для каждого номера прикрепляет свое основание. Проверяет параметр
     * количества сносок и в зависимости от его значения выполняет необходимые действия
     *
     * @param paragraph Параграф, в который добавляются основания
     * @param list Список с основаниями
     * @param foundationSet Набор с номерами оснований
     * @param starCount Параметр, отражающий количество сносок
     */
    private static void addToParagraph(Paragraph paragraph, List<String> list, Set<Integer> foundationSet, int starCount) {
        int i = 0;
        Chunk chunk;
        for (Integer foundNumber: foundationSet) {
            chunk = new Chunk(String.valueOf(foundNumber));
            chunk.setTextRise(TEXT_RISE);
            paragraph.add(chunk);
            if (i == 0) {
                paragraph.add(initFoundationFirstString(list.get(i), starCount) + "\n");
            } else {
                paragraph.add(list.get(i) + "\n");
            }
            i++;
        }
    }

    /**
     * Функция, проверяющая параметр, отражающий количество сносок.
     * В зависимости от этого параметра в первое основание добавляется
     * необходимое значение
     *
     * @param firstString Строка, содержащая первое основание
     * @param starCount Параметр, отражающий количество сносок
     * @return Отформатированная строка
     */
    private static String initFoundationFirstString(String firstString, int starCount) {
        if (starCount == 1) {
            return String.format(firstString, "<**>");
        } else {
            return String.format(firstString, "<***>");
        }
    }

    /**
     * Функция, добавляющая в данные о функциональном назначение номер сноски в виде
     * надстрочного символа
     *
     * @param purpose Строка с функциональным назначением
     * @param foundationNumber Номер сноски
     * @return Paragraph с содержимым
     */
    static Paragraph addToParagraph(String purpose, Integer foundationNumber) {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setFont(TIMES_NORMAL);
        Chunk chunk = new Chunk(String.valueOf(foundationNumber));
        chunk.setTextRise(TEXT_RISE);
        paragraph.add(purpose);
        paragraph.add(chunk);
        return paragraph;
    }

    /**
     * Функция формирующа и возвращающая таблицу с должностью и фио
     * исполнителя выписки, а так же содержащую сноски к документу
     *
     * @param position Должность исполнителя
     * @param fio Фамилия исполнителя
     * @param starCount Параметр, отражающий количество сносок
     * @param secondType Параметр, отвечающий за документы 2-го типа
     * @return Table с содержимым
     */
    public static Table addBottomTable(String position, String fio, int starCount, boolean secondType) throws BadElementException {
        Table table = new Table(100);
        table.setTableFitsPage(true);
        table.setWidth(WIDTH);
        table.addCell(addColSpan(addNoBorder(addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, position)), 65));
        table.addCell(addColSpan(addNoBorder(addDataCell(Element.ALIGN_BOTTOM, Element.ALIGN_RIGHT, fmtString(CommonTypeValues.SIGNATURE.toString(), fio))), 35));
        table.addCell(addColSpan(addNoBorder(new Cell()), 65));
        table.addCell(addColSpan(addNoBorder(addDataCell(Element.ALIGN_BOTTOM, Element.ALIGN_LEFT, CommonTypeValues.SIGNATURE_MP.toString())), 35));
        if (starCount == ONE_STAR) {
            addFootnotes(table, secondType);
        } else if (starCount == TWO_STAR) {
            addAllFootnotes(table, secondType);
        }
        return table;
    }

    /**
     * Функция, добавляющая все сноски в таблицу <i>table</i>
     *
     * @param table Таблица, в которую добавляются сноски
     * @param secondType Параметр, отвечающий за документы 2-го типа
     */
    private static void addAllFootnotes(Table table, boolean secondType) throws BadElementException {
        StringBuilder strBuilder = new StringBuilder("\n\n");
        strBuilder.append("     <*> ").append(CommonTypeValues.FOOTNOTE_ONE.getValue());
        strBuilder.append("     <**> ").append(CommonTypeValues.FOOTNOTE_TWO.getValue());
        strBuilder.append("     <***> ").append(CommonTypeValues.FOOTNOTE_THREE.getValue());
        if (secondType) {
            strBuilder.append("     <****> ").append(CommonTypeValues.FOOTNOTE_FOUR.getValue());
        }
        table.addCell(addNoBorder(addColSpan(addSmallDataCell(Element.ALIGN_CENTER, Element.ALIGN_JUSTIFIED,
                addSmallParagraph(Element.ALIGN_JUSTIFIED, strBuilder.toString())), 100)));
    }

    /**
     * Функция, добавляющая не все сноски в таблицу <i>table</i>
     *
     * @param table Таблица, в которую добавляются сноски
     * @param secondType Параметр, отвечающий за документы 2-го типа
     */
    private static void addFootnotes(Table table, boolean secondType) throws BadElementException {
        StringBuilder strBuilder = new StringBuilder("\n\n");
        strBuilder.append("     <*> ").append(CommonTypeValues.FOOTNOTE_TWO.getValue());
        strBuilder.append("     <**> ").append(CommonTypeValues.FOOTNOTE_THREE.getValue());
        if (secondType) {
            strBuilder.append("     <***> ").append(CommonTypeValues.FOOTNOTE_FOUR.getValue());
        }
        table.addCell(addNoBorder(addColSpan(addSmallDataCell(Element.ALIGN_CENTER, Element.ALIGN_JUSTIFIED,
                addSmallParagraph(Element.ALIGN_JUSTIFIED, strBuilder.toString())), 100)));
    }


}
