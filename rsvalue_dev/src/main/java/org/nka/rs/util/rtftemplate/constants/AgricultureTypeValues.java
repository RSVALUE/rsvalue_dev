package org.nka.rs.util.rtftemplate.constants;

/**
 * Перечисление, в котором содержатся статические данные
 * для формы выписки 1СХ
 */
public enum AgricultureTypeValues {

    NAME("ВЫПИСКА ОТ %s № %s\nО КАДАСТРОВОЙ СТОИМОСТИ СЕЛЬСКОХОЗЯЙСТВЕННЫХ ЗЕМЕЛЬ"),

    TNAME("Описание оценочной зоны"),

    TFIELD_AREA("Наименовение области"),

    TFIELD_DISTRICT("Наименование района"),

    TFIELD_LANDUSER("Наименование землепользователя"),

    TFIELD_UNP("Учетный номер плательщика (землепользователя"),

    TFIELD_ZONE_NUMBER("Номер оценочной зоны"),

    MAIN_TFIELD_PURPOSE("Вид земель"),

    MAIN_TFIELD_CADASTER_VALUE("Кадастровая стоимость 1 гектара сельскохозяйственных земель");

    AgricultureTypeValues(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }
}
