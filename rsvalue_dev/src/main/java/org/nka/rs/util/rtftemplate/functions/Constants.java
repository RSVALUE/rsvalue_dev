package org.nka.rs.util.rtftemplate.functions;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;

/**
 * Created by Tihonovich on 10.02.2017.
 */
public class Constants {

    public final static String FILE_NAME = "rtfDoc.rtf";

    private final static String FONT_DEST = "C:/Windows/Fonts/times.ttf";

    final static Font TIMES_NORMAL = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 12);

    final static Font TIMES_SMALL = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 11);

    public final static Font TIMES_SMALL_SIX = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 6);

    public final static Font TIMES_SMALL_EIGHT = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 8);

    final static Font TIMES_SMALL_TEN = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 10);

    final static Font TIMES_NORMAL_BOLD = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 12, Font.BOLD);

    final static Font TIMES_SMALL_TEN_BOLD = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 10, Font.BOLD);

    public final static float MARGIN_LEFT = 84.5F;

    public final static float MARGIN_RIGHT = 42.4F;

    public final static float MARGIN_TOP = 56.75F;

    public final static float MARGIN_BOTTOM = 56.75F;

    final static float FIRST_LINE_INDENT = 35.5F;

    public final static int TABLE_COLS = 100;

    public final static int COLNUMBER = 5;

    public final static int COLTYPE = 10;

    public final static int COLADDRESS = 60;

    public final static int COLCADNUMBER = 25;

    public final static int COLSPANLEFT = 40;

    public final static int COLSPANCENTER = 10;

    public final static int COLSPANRIGHT = 50;

    public static final float TABLE_WIDTH = 100F;

    public final static String MAIN_DATA_HEADER = "СЧЕТ-АКТ № %s";

    public final static String INFO_CUSTOMER_HEADER = "Сведения о Заказчике:";

    public final static String INFO_CUSTOMER = "Заказчик";

    public final static String INFO_CUST_REPRESENTATIVE = "Представитель Заказчика";

    public final static String INFO_CUST_FOUNDATIONS = "Основания представительства";

    public final static String INFO_EXECUTOR_HEADER = "Сведения об Исполнителе:";

    public final static String INFO_EXECUTOR = "Исполнитель";

    public final static String INFO_EXEC_REPRESENTATIVE = "Представитель исполнителя";

    public final static String INFO_EXEC_FOUNDATIONS = "Основания представительства";

    public final static String CUSTOMER_SIGNATURE = INFO_CUSTOMER + ": ____________________";

    public final static String EXECUTOR_SIGNATURE = INFO_EXECUTOR + ": ____________________";

    public final static String MAIN_DATA_FIRST_NEW = "Заказчик и исполнитель составили настоящий счет-акт, о том, что в соответствии с " +
            "заявлением от %s. Исполнитель оказал Заказчику услуги по выдаче из Регистра стоимости земель, земельных участков государственного " +
            "земельного кадастра документов на следующие объекты:";

    public final static String MAIN_DATA_SECOND = "Стоимость оказанных услуг составляет %s, в том числе НДС (%s) – %s " +
            "(согласно Прейскуранту %s от %s).";

    public final static String MAIN_DATA_THIRD = "Настоящий счет-акт является основанием для проведения взаимных расчетов и платежей между Исполнителем " +
            "и Заказчиком.\nЗаказчик к качеству и объему оказанных услуг претензий не имеет.";

    public final static String TABLE_HEADER_NUMBER = "№ п/п";

    public final static String TABLE_HEADER_TYPE = "Тип выписки";

    public final static String TABLE_HEADER_ADDRESS = "Адрес земельного участка";

    public final static String TABLE_HEADER_CADNUMBER = "Кадастровый номер\nземельного участка";

    public final static String TABLE_FOOTER_EXEC = "Исполнитель:";

    public final static String TABLE_FOOTER_CUST = "Заказчик:";

    public final static String SIGNATURE_NAME_LEFT = "__________/_________________";

    public final static String SIGNATURE_NAME_RIGHT = "__________/_______________________";

    public final static String SIGNATURE_INFO = "(подпись)          ФИО";

    public final static String SIGNATURE_PLACE = "МП";

    public final static String SIGNATURE_DATE = "Дата подписания";

    public final static String YEAR_STRING = "«____»____________ %s г.";

    public final static String DATE = "«__»_______201_г.";
}
