package org.nka.rs.util.nativedll;

import com.sun.jna.WString;

/**
 * Created by Shcherbina on 26.11.2016.
 */
public class NativeImpl {
    public static String getHash (String str){
        NativeInterface nativeDll = NativeInterface.INSTANCE;
        WString pass = new WString(str);
        return (nativeDll.SHA512CPPHash(pass)).toString();
    }
}
