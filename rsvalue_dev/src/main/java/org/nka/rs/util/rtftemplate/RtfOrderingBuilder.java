package org.nka.rs.util.rtftemplate;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.rtf.RtfWriter2;
import org.nka.rs.entity.pojos.docs.BuildInfo;
import org.nka.rs.util.rtftemplate.constants.*;
import org.nka.rs.util.rtftemplate.types.*;

import java.io.FileOutputStream;
import java.io.IOException;

import static org.nka.rs.util.rtftemplate.types.CommonType.*;

/**
 * Класс для создания выписок по объектам заказов<br>
 * Содержит в себе <i>public</i> метод <i>build</i>, <i>private</i> методы,
 * в которых инкапсулировано построение форм выписок и строковые константы
 * для обозначения типов форм.
 */
public class RtfOrderingBuilder {

    /**
     * Константа для обозначения формы типа "1КС"
     */
    public final static String ONE_KS = "1КС";

    /**
     * Константа для обозначения формы типа "2КС"
     */
    public final static String TWO_KS = "2КС";

    /**
     * Константа для обозначения формы типа "3КС"
     */
    public final static String THREE_KS = "3КС";

    /**
     * Константа для обозначения формы типа "1НБ"
     */
    public final static String ONE_NB = "1НБ";

    /**
     * Константа для обозначения формы типа "2НБ"
     */
    public final static String TWO_NB = "2НБ";

    /**
     * Константа для обозначения формы типа "1СХ"
     */
    public final static String ONE_SX = "1СХ";

    /**
     * Статическая функция, которыя принимает в качестве параметра объект
     * типа BuildInfo, в котором содержатся сведения по форме выписки. <br>
     * Назначение функции в определении типа выписки и делегировании обязанностей
     * по формированию на соответствующий метод.
     *
     * @param buildInfo Объект, содержащий в себе сведения по форме выписки
     */
    public static void build(BuildInfo buildInfo) throws IOException {
        buildInfo.setDocSecondType(false);
        if (buildInfo.getDocType().equals(ONE_KS)) {
            buildFirstType(buildInfo);
        } else if (buildInfo.getDocType().equals(TWO_KS)) {
            buildInfo.setDocSecondType(true);
            buildSecondType(buildInfo);
        } else if (buildInfo.getDocType().equals(THREE_KS)) {
            buildThirdType(buildInfo);
        } else if (buildInfo.getDocType().equals(ONE_NB)) {
            buildFirstNBType(buildInfo);
        } else if (buildInfo.getDocType().equals(TWO_NB)) {
            buildInfo.setDocSecondType(true);
            buildSecondNBType(buildInfo);
        } else if (buildInfo.getDocType().equals(ONE_SX)) {
            buildAgricultureType(buildInfo);
        }
    }

    /**
     * Статическая функция, отвечающая за формирование выписки типа 1КС.<br>
     * Создает новый документ с необходимыми сведениями и сохраняет его на жестком
     * диске.
     *
     * @param buildInfo Объект, содержащий в себе сведения по форме выписки
     */
    private static void buildFirstType(BuildInfo buildInfo) throws IOException {
        Document document = new Document();
        RtfWriter2.getInstance(document, new FileOutputStream(buildInfo.getDocName()));
        try {
            document.setHeader(CommonType.addHeader(CommonTypeValues.FORM_1KS.getValue()));
            document.open();
            //Шапка документа (до названия выписки, одинаковый для всех типов форм)
            document.add(addParagraph(Element.ALIGN_CENTER, CommonTypeValues.HEADER_START.getValue(), addHeaderTable()));
            //Название выписки с применением форматирования
            document.add(addBoldParagraph(Element.ALIGN_CENTER, String.format(FirstTypeValues.NAME.getValue(), buildInfo.getDocDate(), buildInfo.getDocNumber())));
            //Верхняя таблица
            document.add(FirstType.addHeadParagraph(FirstTypeValues.TNAME.getValue(), buildInfo.getDocInfo()));
            //Основная таблица
            document.add(FirstType.addMainParagraph(String.format(CommonTypeValues.MAIN_THEADER.getValue(), buildInfo.getDocSearchDate()), buildInfo.getDocObjects(),
                    buildInfo.getDocFoundationNumberList(), null, buildInfo.getDocStarCount()));
            //Список оснований
            document.add(addFoundations(buildInfo.getDocFoundationList(), buildInfo.getDocFoundationNumberSet(), buildInfo.getDocStarCount()));
            //Таблица подписи и сноски
            document.add(addBottomTable(buildInfo.getDocExecPost(), buildInfo.getDocExecFullName(), buildInfo.getDocStarCount(), buildInfo.isDocSecondType()));
        } catch (DocumentException e) {
            System.err.println(e.getMessage());
        } finally {
            document.close();
        }
    }

    /**
     * Статическая функция, отвечающая за формирование выписки типа 1НБ.<br>
     * Создает новый документ с необходимыми сведениями и сохраняет его на жестком
     * диске.
     *
     * @param buildInfo Объект, содержащий в себе сведения по форме выписки
     */
    private static void buildFirstNBType(BuildInfo buildInfo) throws IOException {
        Document document = new Document();
        RtfWriter2.getInstance(document, new FileOutputStream(buildInfo.getDocName()));
        try {
            document.setHeader(CommonType.addHeader(CommonTypeValues.FORM_1NB.getValue()));
            document.open();
            //Шапка документа (до названия выписки, одинаковый для всех типов форм)
            document.add(addParagraph(Element.ALIGN_CENTER, CommonTypeValues.HEADER_START.getValue(), addHeaderTable()));
            //Название выписки с применением форматирования
            document.add(addBoldParagraph(Element.ALIGN_CENTER, String.format(FirstTypeValues.NAME.getValue(), buildInfo.getDocDate(), buildInfo.getDocNumber()),
                    String.format(FirstTypeValues.NB_VALUE.getValue(), buildInfo.getDocNbYear())));
            //Верхняя таблица
            document.add(FirstType.addHeadParagraph(FirstTypeValues.TNAME.getValue(), buildInfo.getDocInfo()));
            //Основная таблица
            document.add(FirstType.addMainParagraph(String.format(CommonTypeValues.MAIN_THEADER.getValue(), buildInfo.getDocSearchDate()), buildInfo.getDocObjects(),
                    buildInfo.getDocFoundationNumberList(), buildInfo.getDocNbYear(), buildInfo.getDocStarCount()));
            //Список оснований
            document.add(addFoundations(buildInfo.getDocFoundationList(), buildInfo.getDocFoundationNumberSet(), buildInfo.getDocStarCount()));
            //Таблица подписи и сноски
            document.add(addBottomTable(buildInfo.getDocExecPost(), buildInfo.getDocExecFullName(), buildInfo.getDocStarCount(), buildInfo.isDocSecondType()));
        } catch (DocumentException e) {
            System.err.println(e.getMessage());
        } finally {
            document.close();
        }
    }

    /**
     * Статическая функция, отвечающая за формирование выписки типа 2КС.<br>
     * Создает новый документ с необходимыми сведениями и сохраняет его на жестком
     * диске.
     *
     * @param buildInfo Объект, содержащий в себе сведения по форме выписки
     */
    private static void buildSecondType(BuildInfo buildInfo) throws IOException {
        Document document = new Document();
        RtfWriter2.getInstance(document, new FileOutputStream(buildInfo.getDocName()));
        try {
            document.setHeader(CommonType.addHeader(CommonTypeValues.FORM_2KS.getValue()));
            document.open();
            //Шапка документа (до названия выписки, одинаковый для всех типов форм)
            document.add(addParagraph(Element.ALIGN_CENTER, CommonTypeValues.HEADER_START.getValue(), addHeaderTable()));
            //Название выписки с применением форматирования
            document.add(addBoldParagraph(Element.ALIGN_CENTER, String.format(SecondTypeValues.NAME.getValue(), buildInfo.getDocDate(), buildInfo.getDocNumber())));
            //Верхняя таблица
            document.add(SecondType.addHeadParagraph(SecondTypeValues.TNAME.getValue(), buildInfo.getDocInfo().get(0)));
            //Основная таблица
            document.add(SecondType.addMainParagraph(String.format(CommonTypeValues.MAIN_THEADER.getValue(), buildInfo.getDocSearchDate()), buildInfo.getDocObjects(),
                    buildInfo.getDocFoundationNumberList(), buildInfo.getDocNbYear(), buildInfo.getDocStarCount()));
            //Список оснований
            document.add(addFoundations(buildInfo.getDocFoundationList(), buildInfo.getDocFoundationNumberSet(), buildInfo.getDocStarCount()));
            //Список приложений для второго типа выписок
            document.add(SecondType.addAttachments(buildInfo.getDocAttachment(), buildInfo.getDocStarCount()));
            //Таблица подписи и сноски
            document.add(addBottomTable(buildInfo.getDocExecPost(), buildInfo.getDocExecFullName(), buildInfo.getDocStarCount(), buildInfo.isDocSecondType()));
        } catch (DocumentException e) {
            System.err.println(e.getMessage());
        } finally {
            document.close();
        }
    }

    /**
     * Статическая функция, отвечающая за формирование выписки типа 2НБ.<br>
     * Создает новый документ с необходимыми сведениями и сохраняет его на жестком
     * диске.
     *
     * @param buildInfo Объект, содержащий в себе сведения по форме выписки
     */
    private static void buildSecondNBType(BuildInfo buildInfo) throws IOException {
        Document document = new Document();
        RtfWriter2.getInstance(document, new FileOutputStream(buildInfo.getDocName()));
        try {
            document.setHeader(CommonType.addHeader(CommonTypeValues.FORM_2NB.getValue()));
            document.open();
            //Шапка документа (до названия выписки, одинаковый для всех типов форм)
            document.add(addParagraph(Element.ALIGN_CENTER, CommonTypeValues.HEADER_START.getValue(), addHeaderTable()));
            //Название выписки с применением форматирования
            document.add(addBoldParagraph(Element.ALIGN_CENTER, String.format(SecondTypeValues.NAME.getValue(), buildInfo.getDocDate(), buildInfo.getDocNumber()),
                    String.format(SecondTypeValues.NB_VALUE.getValue(), buildInfo.getDocNbYear())));
            //Верхняя таблица
            document.add(SecondType.addHeadParagraph(SecondTypeValues.TNAME.getValue(), buildInfo.getDocInfo().get(0)));
            //Основная таблица
            document.add(SecondType.addMainParagraph(String.format(CommonTypeValues.MAIN_THEADER.getValue(), buildInfo.getDocSearchDate()), buildInfo.getDocObjects(),
                    buildInfo.getDocFoundationNumberList(), buildInfo.getDocNbYear(), buildInfo.getDocStarCount()));
            //Список оснований
            document.add(addFoundations(buildInfo.getDocFoundationList(), buildInfo.getDocFoundationNumberSet(), buildInfo.getDocStarCount()));
            //Список приложений для второго типа выписок
            document.add(SecondType.addAttachments(buildInfo.getDocAttachment(), buildInfo.getDocStarCount()));
            //Таблица подписи и сноски
            document.add(addBottomTable(buildInfo.getDocExecPost(), buildInfo.getDocExecFullName(), buildInfo.getDocStarCount(), buildInfo.isDocSecondType()));
        } catch (DocumentException e) {
            System.err.println(e.getMessage());
        } finally {
            document.close();
        }
    }

    /**
     * Статическая функция, отвечающая за формирование выписки типа 3КС.<br>
     * Создает новый документ с необходимыми сведениями и сохраняет его на жестком
     * диске.
     *
     * @param buildInfo Объект, содержащий в себе сведения по форме выписки
     */
    private static void buildThirdType(BuildInfo buildInfo) throws IOException {
        Document document = new Document();
        RtfWriter2.getInstance(document, new FileOutputStream(buildInfo.getDocName()));
        try {
            document.setHeader(CommonType.addHeader(CommonTypeValues.FORM_3KS.getValue()));
            document.open();
            //Шапка документа (до названия выписки, одинаковый для всех типов форм)
            document.add(addParagraph(Element.ALIGN_CENTER, CommonTypeValues.HEADER_START.getValue(), addHeaderTable()));
            //Название выписки с применением форматирования
            document.add(addBoldParagraph(Element.ALIGN_CENTER, ThirdTypeValues.NAME.getValue()));
            //Основная таблица
            document.add(ThirdType.addMainParagraph(String.format(ThirdTypeValues.DATE.getValue(), buildInfo.getDocDate(), buildInfo.getDocNumber()), buildInfo.getDocInfo(),
                    buildInfo.getDocStarCount()));
            //Список оснований
            document.add(addFoundations(buildInfo.getDocFoundationList(), buildInfo.getDocFoundationNumberSet(), buildInfo.getDocStarCount()));
            //Таблица подписи и сноски
            document.add(addBottomTable(buildInfo.getDocExecPost(), buildInfo.getDocExecFullName(), buildInfo.getDocStarCount(), buildInfo.isDocSecondType()));
        } catch (DocumentException e) {
            System.err.println(e.getMessage());
        } finally {
            document.close();
        }
    }

    /**
     * Статическая функция, отвечающая за формирование выписки типа 1СХ.<br>
     * Создает новый документ с необходимыми сведениями и сохраняет его на жестком
     * диске.
     *
     * @param buildInfo Объект, содержащий в себе сведения по форме выписки
     */
    private static void buildAgricultureType(BuildInfo buildInfo) throws IOException {
        Document document = new Document();
        RtfWriter2.getInstance(document, new FileOutputStream(buildInfo.getDocName()));
        try {
            document.setHeader(CommonType.addHeader(CommonTypeValues.FORM_1SX.getValue()));
            document.open();
            //Шапка документа (до названия выписки, одинаковый для всех типов форм)
            document.add(addParagraph(Element.ALIGN_CENTER, CommonTypeValues.HEADER_START.getValue(), addHeaderTable()));
            //Название выписки с применением форматирования
            document.add(addBoldParagraph(Element.ALIGN_CENTER, String.format(AgricultureTypeValues.NAME.getValue(), buildInfo.getDocDate(), buildInfo.getDocNumber())));
            //Верхняя таблица
            document.add(AgricultureType.addHeadParagraph(AgricultureTypeValues.TNAME.getValue(), buildInfo.getDocInfo()));
            //Основная таблица
            document.add(AgricultureType.addMainParagraph(String.format(CommonTypeValues.MAIN_THEADER.getValue(), buildInfo.getDocSearchDate()), buildInfo.getDocObjects(),
                    buildInfo.getDocFoundationNumberList(), buildInfo.getDocStarCount()));
            //Список оснований
            document.add(addFoundations(buildInfo.getDocFoundationList(), buildInfo.getDocFoundationNumberSet(), buildInfo.getDocStarCount()));
            //Таблица подписи и сноски
            document.add(addBottomTable(buildInfo.getDocExecPost(), buildInfo.getDocExecFullName(), buildInfo.getDocStarCount(), buildInfo.isDocSecondType()));
        } catch (DocumentException e) {
            System.err.println(e.getMessage());
        } finally {
            document.close();
        }
    }
}
