package org.nka.rs.util.rtftemplate.types;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import org.nka.rs.util.rtftemplate.constants.AgricultureTypeValues;
import org.nka.rs.util.rtftemplate.constants.CommonTypeValues;

import java.util.List;

/**
 * Класс, в котором содержатся некоторые статические данные для
 * формы выписки 1СХ и специфические функции для формирование
 * выписок данного типа.
 */
public class AgricultureType {

    /**
     * Количество колонок в верхней таблице
     */
    private final static int HEAD_TABLE_COLS = 2;

    /**
     * Количество колонок в главной таблице
     */
    private final static int MAIN_TABLE_COLS = 100;

    /**
     * Количество объединяемых колонок для колонки "Функциональное назначение"
     */
    private final static int TCOLSPAN_PURPOSE = 28;

    /**
     * Количество объединяемых колонок для колонки "Дата оценки"
     */
    private final static int TCOLSPAN_DATE = 12;

    /**
     * Количество объединяемых колонок для колонки "Номер оценочной зоны"
     */
    private final static int TCOLSPAN_NUMBER = 12;

    /**
     * Количество объединяемых колонок для колонки "Кадастровый номер"
     */
    private final static int TCOLSPAN_CADASTER = 48;

    /**
     * Количество объединяемых строк
     */
    private final static int ROW_SPAN = 2;

    /**
     * Функция, формирующая верхний параграф, включающий в себя
     * шапку документа и верхнюю таблицу
     *
     * @param headerValue Форматированная строка с датой и номером выписки
     * @param valueList Список с элементами для верхней таблицы
     * @return Paragraph с содержимым
     */
    public static Paragraph addHeadParagraph(String headerValue, List<String> valueList) throws BadElementException {
        return CommonType.addBoldParagraph(Element.ALIGN_CENTER, headerValue, addHeadTable(valueList));
    }

    /**
     * Функция, формирующая и возвращающая объект типа Table, в котором
     * содержатся данные из входного параметра
     *
     * @param valueList Список с элементами для верхней таблицы
     * @return Table с содержимым
     */
    private static Table addHeadTable(List<String> valueList) throws BadElementException {
        Table table = new Table(HEAD_TABLE_COLS);
        table.setWidth(CommonType.WIDTH);
        table.setPadding(CommonType.PADDING);
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, AgricultureTypeValues.TFIELD_AREA.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(0)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, AgricultureTypeValues.TFIELD_DISTRICT.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(1)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, AgricultureTypeValues.TFIELD_LANDUSER.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(2)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, AgricultureTypeValues.TFIELD_UNP.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(3)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, AgricultureTypeValues.TFIELD_ZONE_NUMBER.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(4)));
        return table;
    }

    /**
     * Функция, формирующая и возвращающая объект типа Table, в котором
     * содержатся основные данные выписки
     *
     * @param valueList Список с элементами для главной таблицы
     * @param foundationList Список с числовыми элементами оснований (для сносок)
     * @param starCount Параметр, отражающий количество сносок
     * @return Table с содержимым
     */
    private static Table addMainTable(List<String[]> valueList, List<Integer> foundationList, int starCount) throws BadElementException {
        Table table = new Table(MAIN_TABLE_COLS);
        table.setWidth(CommonType.WIDTH);
        table.setPadding(CommonType.BIG_PADDING);
        setAgricultureTableHeaders(table, starCount);
        setAgricultureTableData(table, valueList, foundationList);
        return table;
    }

    /**
     * Функция, добавляющая в таблицу <i>table</i> статические заголовки.
     *
     * @param table Таблица, в которую добавляются заголовки
     * @param starCount Параметр, отражающий количество сносок
     */
    private static void setAgricultureTableHeaders(Table table, int starCount) throws BadElementException {
        table.addCell(CommonType.addHeaderCell(AgricultureTypeValues.MAIN_TFIELD_PURPOSE.getValue(), TCOLSPAN_PURPOSE, ROW_SPAN));
        table.addCell(CommonType.addHeaderCell(CommonTypeValues.MAIN_TFIELD_DATE.getValue(), TCOLSPAN_DATE, ROW_SPAN));
        table.addCell(CommonType.addHeaderCell(CommonTypeValues.MAIN_TFIELD_ZONE_NUMBER.getValue(), TCOLSPAN_NUMBER, ROW_SPAN));
        table.addCell(CommonType.addHeaderCell(AgricultureTypeValues.MAIN_TFIELD_CADASTER_VALUE.getValue(), TCOLSPAN_CADASTER, 1));
        checkStarCount(table, starCount);
    }

    /**
     * Функция, добавляющая в таблицу <i>table</i> данные по форме выписки.
     *
     * @param table Таблица, в которую добавляются данные
     * @param valueList Данные, добавляемые в таблицу
     * @param foundationList Список с числовыми элементами оснований (для сносок)
     */
    private static void setAgricultureTableData(Table table, List<String[]> valueList, List<Integer> foundationList) throws BadElementException {
        SecondType.setSecondTableData(table, valueList, foundationList);
    }

    /**
     * Функция, проверяющая параметр, отражающий количество сносок
     *
     * @param table Таблица, для которой происходит проверка
     * @param starCount Параметр, отражающий количество сносок
     */
    private static void checkStarCount(Table table, int starCount) throws BadElementException {
        SecondType.checkStarCount(table, starCount);
    }

    /**
     * Функция, добавляющия основной параграф документа. Включает в себя
     * заголовок перед основной таблицей и саму таблицу
     *
     * @param tHeader Заголовок, содержащий сведения по дате поиска
     * @param valueList Данные, добавляемые в таблицу
     * @param foundationList Список с числовыми элементами оснований (для сносок)
     * @param starCount Параметр, отражающий количество сносок
     * @return Paragraph с содержимым
     */
    public static Paragraph addMainParagraph(String tHeader, List<String[]> valueList, List<Integer> foundationList, int starCount) throws BadElementException {
        return CommonType.addBoldParagraph(Element.ALIGN_CENTER, tHeader, addMainTable(valueList, foundationList, starCount));
    }
}
