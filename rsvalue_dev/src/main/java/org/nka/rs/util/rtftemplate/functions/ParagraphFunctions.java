package org.nka.rs.util.rtftemplate.functions;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;

import java.util.Arrays;

import static org.nka.rs.util.rtftemplate.functions.Constants.*;

/**
 * Класс, в котором расположены функции для работы с параграфами
 */
public class ParagraphFunctions {

    /**
     * Добавляет параграф с текстом 10-го размера и жирным шрифтом
     *
     * @param phrase Текст, который будет находится в параграфе
     * @return Paragraph с содержимым
     */
    public static Paragraph addInfoHeaderParagraph(String phrase) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setFont(TIMES_SMALL_TEN_BOLD);
        paragraph.add(phrase);
        return paragraph;
    }

    /**
     * Добавляет параграф с текстом 10-го размера и обычным шрифтом
     *
     * @param phrase Текст, который будет находится в параграфе
     * @return Paragraph с содержимым
     */
    public static Paragraph addInfoParagraph(String phrase) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setFont(TIMES_SMALL_TEN);
        paragraph.add(phrase);
        return paragraph;
    }

    /**
     * Инициализирует и возвращает новый параграф с заданным горизонтальным
     * выравниваем, отступом первом строки(<i>FIRST_LINE_INDENT</i>), шрифтом
     * TimesNewRoman размера 12. После чего добавляет по-порядку все объекты
     * из массива данных data.
     *
     * @param hAlign Тип горизонтального выравнивания
     * @param data   Объекты, который будут добавляться в параграф
     * @return Paragraph со всеми объектами <i>data</i>
     */
    public static Paragraph addParagraph(int hAlign, Object... data) throws BadElementException {
        Paragraph paragraph = new Paragraph();
        paragraph.setFirstLineIndent(FIRST_LINE_INDENT);
        paragraph.setAlignment(hAlign);
        paragraph.setFont(TIMES_NORMAL);
        paragraph.addAll(Arrays.asList(data));
        return paragraph;
    }

    /**
     * Инициализирует и возвращает новый параграф с заданным горизонтальным
     * выравниваем, отступом первом строки(<i>FIRST_LINE_INDENT</i>), шрифтом
     * TimesNewRoman размера 12. Добавляет только 1 объект типа String.
     *
     * @param hAlign Тип горизонтального выравнивания
     * @param phrase Текст, помещаемый в параграф
     * @return Paragraph с текстом
     */
    public static Paragraph addParagraph(int hAlign, String phrase) throws BadElementException {
        Paragraph paragraph = new Paragraph(phrase);
        paragraph.setFirstLineIndent(FIRST_LINE_INDENT);
        paragraph.setAlignment(hAlign);
        paragraph.setFont(TIMES_NORMAL);
        return paragraph;
    }

    /**
     * Инициализирует и возвращает новый параграф с горизонтальным выравниванием
     * по центру, отступом первом строки(<i>FIRST_LINE_INDENT</i>), шрифтом
     * TimesNewRoman размера 12. Добавляет только 1 объект типа String.
     *
     * @param phrase Текст, помещаемый в параграф
     * @return Paragraph с текстом
     */
    public static Paragraph addHeaderParagraph(String phrase) throws BadElementException {
        Paragraph paragraph = new Paragraph(phrase);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setFont(TIMES_NORMAL);
        return paragraph;
    }
}
