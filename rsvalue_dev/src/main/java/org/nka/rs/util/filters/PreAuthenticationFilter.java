package org.nka.rs.util.filters;

import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.pojos.common.UserAccount;
import org.nka.rs.entity.security.RestAuthentication;
import org.nka.rs.service.ISubjectService;
import org.nka.rs.service.IUserAccountService;
import org.nka.rs.service.IUserEntityService;
import org.nka.rs.util.nativedll.NativeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/*
* Created by zgurskaya on 19.09.2016.
*/

//фильтр для получения данных о состоянии пользователя системы
@Service
public class PreAuthenticationFilter {

    @Autowired
    IUserEntityService userEntityService;

    @Autowired
    IUserAccountService userAccounntService;

    @Autowired
    ISubjectService subjectService;

    public RestAuthentication attemptAuthentication(String username, String password) {
        RestAuthentication restAuthentication = new RestAuthentication();
        String token = "";
        if (username == null) {
            username = "";
        }
        UserAccount userAccount = null;
        try {
            userAccount = checkCredentials(username, password);
            if (userAccount.getUserEntity().getToken() == null) {
                token = userEntityService.createAndSaveToken(userAccount.getUserEntity().getUserId(), userAccount.getUserEntity().getUsername());
                restAuthentication = setAuthentication(restAuthentication, userAccount, token);
            } else {
                restAuthentication = setAuthentication(restAuthentication, userAccount, userAccount.getUserEntity().getToken());
            }
        } catch (BadCredentialsException e) {
            restAuthentication.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            restAuthentication.setMessage(e.getMessage());
        }
        return restAuthentication;
    }

    public RestAuthentication getUser(String username, String token) {
        RestAuthentication restAuthentication = new RestAuthentication();
        try {
            if (token != null && !token.equals("")) {
                UserAccount userAccount = userAccounntService.getAccountByName(username);
                if (userAccount != null) {
                    String accessToken = userEntityService.getToken(userAccount.getUserEntity().getUserId());
                    if (accessToken.equals(token)) {
                        restAuthentication = setAuthentication(restAuthentication, userAccount, token);
                    }
                }
            }
        } catch (Exception e) {
            restAuthentication.setMessage(e.getMessage());
            restAuthentication.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
        }
        return restAuthentication;
    }

    private UserAccount checkCredentials(String username, String password) throws BadCredentialsException {
        UserAccount userAccount = userAccounntService.getAccountByName(username);
        if (userAccount == null || !userAccount.getUserEntity().getPassword().equals(NativeImpl.getHash(username + password)))
            throw new BadCredentialsException("Неправильные данные");
        return userAccount;
    }

    private List<GrantedAuthority> getGroup(String group) {
        List<GrantedAuthority> groups = new ArrayList<GrantedAuthority>();
        groups.add(new SimpleGrantedAuthority(group));
        return groups;
    }

    private RestAuthentication setAuthentication(RestAuthentication authentication, UserAccount userAccount, String token) {
        String personData = subjectService.getFullNameById(userAccount.getUserEntity().getUserId());
        UserDetails userDetails = new User(userAccount.getUserEntity().getUsername(), "[SECURED]",
                true, true, true, true, getGroup(userAccount.getUserEntity().getGroup().getGroupName()));
        authentication.setCode(ErrorCodeEnum.SUCCESS.getValue());
        authentication.setUserDetails(userDetails);
        authentication.setUserId(userAccount.getUserId());
        authentication.setPersonData(personData);
        authentication.setToken(token);
        authentication.setTorId(userAccount.getRegCode().getAnalyticCode());
        return authentication;
    }

}
