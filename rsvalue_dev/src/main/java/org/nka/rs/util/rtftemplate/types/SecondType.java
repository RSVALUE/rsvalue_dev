package org.nka.rs.util.rtftemplate.types;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import org.nka.rs.util.rtftemplate.constants.CommonTypeValues;
import org.nka.rs.util.rtftemplate.constants.SecondTypeValues;

import java.util.List;

import static org.nka.rs.util.rtftemplate.constants.Fonts.TIMES_SMALL;
import static org.nka.rs.util.rtftemplate.types.CommonType.ONE_STAR;

/**
 * Класс, содержащий константы и метода для формирования
 * выписок 2-го типа
 */
public class SecondType {

    /**
     * Количество колонок в верхней таблице
     */
    private final static int HEAD_TABLE_COLS = 2;

    /**
     * Количество колонок в главной таблице
     */
    private final static int MAIN_TABLE_COLS = 100;

    /**
     * Количество объединяемых колонок для колонки "Функциональное назначение"
     */
    private final static int TCOLSPAN_PURPOSE = 28;

    /**
     * Количество объединяемых колонок для колонки "Дата оценки"
     */
    private final static int TCOLSPAN_DATE = 12;

    /**
     * Количество объединяемых колонок для колонки "Номер оценочной зоны"
     */
    private final static int TCOLSPAN_NUMBER = 12;

    /**
     * Количество объединяемых колонок для колонки "Кадастровый номер"
     */
    private final static int TCOLSPAN_CADASTER = 48;

    /**
     * Количество объединяемых колонок для колонки "Стоимость в USD полная"
     */
    private final static int TCOLSPAN_USD = 19;

    /**
     * Количество объединяемых колонок для колонки "Стоимость в BYN\BYR полная"
     */
    private final static int TCOLSPAN_BYN = 29;

    /**
     * Количество объединяемых строк"
     */
    private final static int ROW_SPAN = 2;

    /**
     * Функция, формирующая и возвращающая объект типа Table, в котором
     * содержатся данные из входного параметра
     *
     * @param value Строка с элементами для верхней таблицы
     * @return Table с содержимым
     */
    private static Table addHeadTable(String value) throws BadElementException {
        Table table = new Table(HEAD_TABLE_COLS);
        table.setWidth(CommonType.WIDTH);
        table.setPadding(CommonType.PADDING);
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, SecondTypeValues.TFIELD_ADDRESS.getValue()));
        table.addCell(CommonType.addDataCell(value));
        return table;
    }

    /**
     * Функция, формирующая верхний параграф, включающий в себя
     * шапку документа и верхнюю таблицу
     *
     * @param headerValue Форматированная строка с датой и номером выписки
     * @param value Список с элементами для верхней таблицы
     * @return Paragraph с содержимым
     */
    public static Paragraph addHeadParagraph(String headerValue, String value) throws BadElementException {
        return CommonType.addBoldParagraph(Element.ALIGN_CENTER, headerValue, addHeadTable(value));
    }

    /**
     * Функция, формирующая и возвращающая объект типа Table, в котором
     * содержатся основные данные выписки
     *
     * @param valueList Список с элементами для главной таблицы
     * @param foundationList Список с числовыми элементами оснований (для сносок)
     * @param year Год поиска, применяется для проверки на поиск по налоговой базу
     * @param starCount Параметр, отражающий количество сносок
     * @return Table с содержимым
     */
    private static Table addMainTable(List<String[]> valueList, List<Integer> foundationList,  String year, int starCount) throws BadElementException {
        Table table = new Table(MAIN_TABLE_COLS);
        table.setWidth(CommonType.WIDTH);
        table.setPadding(CommonType.BIG_PADDING);
        setSecondTableHeaders(table, year, starCount);
        setSecondTableData(table, valueList, foundationList);
        return table;
    }

    /**
     * Функция, добавляющая в таблицу <i>table</i> статические заголовки.
     *
     * @param table Таблица, в которую добавляются заголовки
     * @param year Год поиска, применяется для проверки на поиск по налоговой базу
     * @param starCount Параметр, отражающий количество сносок
     */
    private static void setSecondTableHeaders(Table table, String year, int starCount) throws BadElementException {
        table.addCell(CommonType.addHeaderCell(CommonTypeValues.MAIN_TFIELD_PURPOSE.getValue(), TCOLSPAN_PURPOSE, ROW_SPAN));
        table.addCell(CommonType.addHeaderCell(CommonTypeValues.MAIN_TFIELD_DATE.getValue(), TCOLSPAN_DATE, ROW_SPAN));
        table.addCell(CommonType.addHeaderCell(CommonTypeValues.MAIN_TFIELD_ZONE_NUMBER.getValue(), TCOLSPAN_NUMBER, ROW_SPAN));
        if (year != null) {
            table.addCell(CommonType.addHeaderCell(String.format(SecondTypeValues.MAIN_TFIELD_CADASTER_VALUE.getValue(), CommonTypeValues.MAIN_TFIELD_CADASTER_NB + year), TCOLSPAN_CADASTER, 1));
        } else {
            table.addCell(CommonType.addHeaderCell(String.format(SecondTypeValues.MAIN_TFIELD_CADASTER_VALUE.getValue(), ""), TCOLSPAN_CADASTER, 1));
        }
        checkStarCount(table, starCount);

    }

    /**
     * Функция, добавляющая в таблицу <i>table</i> данные по форме выписки.
     *
     * @param table Таблица, в которую добавляются данные
     * @param valueList Данные, добавляемые в таблицу
     * @param foundationList Список с числовыми элементами оснований (для сносок)
     */
    static void setSecondTableData(Table table, List<String[]> valueList, List<Integer> foundationList) throws BadElementException {
        int i = 0;
        for (String[] strArray : valueList) {
            table.addCell(CommonType.addMainDataCell(CommonType.addToParagraph(strArray[0], foundationList.get(i)), TCOLSPAN_PURPOSE, Element.ALIGN_LEFT));
            table.addCell(CommonType.addMainDataCell(strArray[1], TCOLSPAN_DATE));
            table.addCell(CommonType.addMainDataCell(strArray[2], TCOLSPAN_NUMBER));
            table.addCell(CommonType.addMainDataCell(strArray[3], TCOLSPAN_USD));
            table.addCell(CommonType.addMainDataCell(strArray[4], TCOLSPAN_BYN));
            i++;
        }
    }

    /**
     * Функция, проверяющая параметр, отражающий количество сносок
     *
     * @param table Таблица, для которой происходит проверка
     * @param starCount Параметр, отражающий количество сносок
     */
    static void checkStarCount(Table table, int starCount) throws BadElementException {
        if (starCount == 1) {
            table.addCell(CommonType.addHeaderCell(CommonTypeValues.MAIN_TFIELD_USD.getValue(), TCOLSPAN_USD, 1));
            table.addCell(CommonType.addHeaderCell(CommonType.fmtString(CommonTypeValues.MAIN_TFIELD_BYN.getValue(), " <*>"), TCOLSPAN_BYN, 1));
        } else if (starCount == 2) {
            table.addCell(CommonType.addHeaderCell(CommonTypeValues.MAIN_TFIELD_USD.getValue(), TCOLSPAN_USD, 1));
            table.addCell(CommonType.addHeaderCell(CommonType.fmtString(CommonTypeValues.MAIN_TFIELD_BYN.getValue(), " <*> <**>"), TCOLSPAN_BYN, 1));
        }
    }

    /**
     * Функция, добавляющия основной параграф документа. Включает в себя
     * заголовок перед основной таблицей и саму таблицу
     *
     * @param tHeader Заголовок, содержащий сведения по дате поиска
     * @param valueList Данные, добавляемые в таблицу
     * @param foundationList Список с числовыми элементами оснований (для сносок)
     * @param starCount Параметр, отражающий количество сносок
     * @return Paragraph с содержимым
     */
    public static Paragraph addMainParagraph(String tHeader, List<String[]> valueList, List<Integer> foundationList, String year, int starCount) throws BadElementException {
        return CommonType.addBoldParagraph(Element.ALIGN_CENTER, tHeader, addMainTable(valueList, foundationList, year, starCount));
    }

    /**
     * Функция, добавляющая список приложений к документу
     *
     * @param attachmentList Список приложений
     * @param starCount Параметр, отражающий количество сносок
     * @return Paragraph со списком приложений
     */
    public static Paragraph addAttachments(List<String> attachmentList, int starCount) {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
        paragraph.setFont(TIMES_SMALL);
        paragraph.setLeading(CommonType.SMALL_LEADING);
        paragraph.add(SecondTypeValues.ATTACHMENT.getValue());
        addToParagraph(paragraph, attachmentList, starCount);
        return paragraph;
    }

    /**
     * Функция добавляет к входному параметру <i>paragraph</i> список приложений.
     * Проверяет количество сносок и форматирует первую запись списка в соотвествии
     *
     * @param paragraph Параграф, к которому добавляется список приложения
     * @param list Список приложений
     * @param starCount Параметр, отражающий количество сносок
     */
    private static void addToParagraph(Paragraph paragraph, List<String> list, int starCount) {
        for (String str : list) {
            if (starCount == ONE_STAR) {
                paragraph.add(String.format(str, "<***>") + "\n");
            } else {
                paragraph.add(String.format(str, "<****>") + "\n");
            }
        }
    }


}
