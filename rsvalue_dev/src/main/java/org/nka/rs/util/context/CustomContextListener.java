package org.nka.rs.util.context;

import oracle.ucp.UniversalConnectionPoolException;
import oracle.ucp.admin.UniversalConnectionPoolManager;
import oracle.ucp.admin.UniversalConnectionPoolManagerImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * Собственный контекстный слушатель. Используется для
 * отслеживания событий, происходящих в приложении
 */

@WebListener
public class CustomContextListener extends ContextLoaderListener {

    private Log log = LogFactory.getLog(ContextLoader.class);

    /**
     * Функция, происходящая по событию разрушения контекста.
     * Дерегистрирует все загруженные в память драйвера и останавливает
     * потоки пула конекшенов.
     *
     * @param event Событие контекста
     */
    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if (log.isInfoEnabled()) {
            log.info("Closing CustomContextListener");
        }
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                if (log.isInfoEnabled()) {
                    log.info(String.format("Deregister jdbc driver: %s", driver));
                }
            } catch (SQLException e) {
                if (log.isErrorEnabled()) {
                    log.error(String.format("Error deregister driver %s", driver));
                }
            }
        }
        UniversalConnectionPoolManager ucpManager = null;
        try {
            ucpManager = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
            if (ucpManager != null) {
                String[] poolNames = ucpManager.getConnectionPoolNames();
                if (poolNames != null) {
                    for (String poolName : poolNames) {
                        ucpManager.destroyConnectionPool(poolName);
                        if (log.isInfoEnabled()) {
                            log.info(String.format("Destroying ConnectionPool: %s", poolName));
                        }
                    }
                }
            }
        } catch (UniversalConnectionPoolException e) {
            if (log.isErrorEnabled()) {
                log.error(String.format("Error destroying ConnectionPoolManager: %s", ucpManager));
            }
        }
        super.contextDestroyed(event);
    }
}
