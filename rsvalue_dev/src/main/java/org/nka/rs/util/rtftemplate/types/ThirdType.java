package org.nka.rs.util.rtftemplate.types;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import org.nka.rs.util.rtftemplate.constants.ThirdTypeValues;

import java.util.List;

/**
 * Класс, содержащий константы и метода для формирования
 * выписок типа 3КС
 */
public class ThirdType {

    /**
     * Количество колонок в главной таблице
     */
    private final static int MAIN_TABLE_COLS = 2;

    /**
     * Функция, добавляющия основной параграф документа. Включает в себя
     * заголовок перед основной таблицей и саму таблицу
     *
     * @param tHeader Заголовок, содержащий сведения по дате поиска
     * @param valueList Данные, добавляемые в таблицу
     * @param starCount Параметр, отражающий количество сносок
     * @return Paragraph с содержимым
     */
    public static Paragraph addMainParagraph(String tHeader, List<String> valueList,  int starCount) throws BadElementException {
        return CommonType.addBoldParagraph(Element.ALIGN_LEFT, tHeader, addMainTable(valueList, starCount));
    }

    /**
     * Функция, формирующая и возвращающая объект типа Table, в котором
     * содержатся основные данные выписки
     *
     * @param valueList Список с элементами для главной таблицы
     * @param starCount Параметр, отражающий количество сносок
     * @return Table с содержимым
     */
    private static Table addMainTable(List<String> valueList, int starCount) throws BadElementException {
        Table table = new Table(MAIN_TABLE_COLS);
        table.setWidth(CommonType.WIDTH);
        table.setPadding(CommonType.PADDING);
        setThirdTableData(table, valueList, starCount);
        return table;
    }

    /**
     * Функция, добавляющая в таблицу <i>table</i> статические заголовки.
     *
     * @param table Таблица, в которую добавляются заголовки
     * @param starCount Параметр, отражающий количество сносок
     */
    private static void setThirdTableData(Table table, List<String> valueList, int starCount) throws BadElementException {
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, ThirdTypeValues.MAIN_TFIELD_NAME_SS.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(0)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, ThirdTypeValues.MAIN_TFIELD_NAME_NEAREST_SS.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(1)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, ThirdTypeValues.MAIN_TFIELD_ZONE_NUMBER.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(2)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, ThirdTypeValues.MAIN_TFIELD_CADASTER_USD.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(3)));
        if (starCount == 1) {
            table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, CommonType.fmtString(ThirdTypeValues.MAIN_TFIELD_CADASTER_BYN.getValue(), " <*>")));
        } else if (starCount == 2) {
            table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, CommonType.fmtString(ThirdTypeValues.MAIN_TFIELD_CADASTER_BYN.getValue(), " <*><**>")));
        }
        table.addCell(CommonType.addDataCell(valueList.get(4)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, ThirdTypeValues.MAIN_TFIELD_DATE.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(5)));
        table.addCell(CommonType.addDataCell(Element.ALIGN_CENTER, Element.ALIGN_LEFT, ThirdTypeValues.MAIN_TFIELD_USD_RATE.getValue()));
        table.addCell(CommonType.addDataCell(valueList.get(6)));
    }
}
