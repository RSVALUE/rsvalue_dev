package org.nka.rs.util.rtftemplate.constants;

/**
 * Перечисление, в котором содержатся статические данные
 * для форм выписок 1КС и 1НБ
 */
public enum FirstTypeValues {

    NAME("ВЫПИСКА ОТ %s № %s\nО КАДАСТРОВОЙ СТОИМОСТИ ЗЕМЕЛЬНОГО УЧАСТКА"),

    NB_VALUE(" НА 1 ЯНВАРЯ %s\nГОДА ДЛЯ ОПРЕДЕЛЕНИЯ НАЛОГОВОЙ БАЗЫ ЗЕМЕЛЬНОГО НАЛОГА"),

    TNAME("\nОписание земельного участка по данным единого государственного регистра\n недвижимого имущества, прав на него и сделок с ним"),

    TFIELD_ADDRESS("Адрес (местоположение)"),

    TFIELD_CADNUMBER("Кадастровый номер"),

    TFIELD_SQUARE("Площадь, (кв.м)"),

    TFIELD_PURPOSE_CLASS("Целевое назначение по единому классификатору назначений объектов недвижимого имущества"),

    TFIELD_PURPOSE_COMMITTEE("Целевое назначение по решению местного исполнительного комитета"),

    MAIN_TFIELD_CADASTER_VALUE("Кадастровая стоимость земельного участка%s"),

    MAIN_TFIELD_USD_SQ_METER("долл.США / кв.м"),

    MAIN_TFIELD_BYN_SQ_METER("белорусских рублей%s/ кв.м");

    FirstTypeValues(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }
}
