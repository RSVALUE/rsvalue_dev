package org.nka.rs.util.rtftemplate.constants;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;

/**
 * Класс с используемыми шрифтами
 */
public class Fonts {

    private final static String FONT_DEST = "times.ttf";

    public final static Font TIMES_NORMAL = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 11);

    public final static Font TIMES_SMALL = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 9);

    public final static Font TIMES_NORMAL_BOLD = FontFactory.getFont(FONT_DEST, BaseFont.IDENTITY_H, 11, Font.BOLD);
}
