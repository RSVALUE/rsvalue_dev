package org.nka.rs.util.rtftemplate.constants;

/**
 * Перечисление, в котором содержатся статические данные,
 * присутствующие во всех типах выписок
 */
public enum CommonTypeValues {

    FORM_1KS("Форма 1КС"),

    FORM_2KS("Форма 2КС"),

    FORM_3KS("Форма 3КС"),

    FORM_1NB("Форма 1НБ"),

    FORM_2NB("Форма 2НБ"),

    FORM_1SX("Форма 1СХ"),

    QUOTATION("«»"),

    HEADER_START("ГОСУДАРСТВЕННЫЙ ЗЕМЕЛЬНЫЙ КАДАСТР\nРЕСПУБЛИКИ БЕЛАРУСЬ"),

    HEADER_CONTINUE("РЕГИСТР СТОИМОСТИ ЗЕМЕЛЬ, ЗЕМЕЛЬНЫХ УЧАСТКОВ"),

    HEADER_END("ГУП «НАЦИОНАЛЬНОЕ КАДАСТРОВОЕ АГЕНТСТВО»"),

    MAIN_THEADER("Стоимостные показатели%s"),

    MAIN_TFIELD_PURPOSE("Вид функционального использования земель"),

    MAIN_TFIELD_DATE("Дата оценки"),

    MAIN_TFIELD_ZONE_NUMBER("Номер оценочной зоны"),

    MAIN_TFIELD_CADASTER_NB(" на 1 января "),

    MAIN_TFIELD_USD("долл. США"),

    MAIN_TFIELD_BYN("белорусских рублей%s"),

    FOUNDATION("ОСНОВАНИЕ:\n"),

    SIGNATURE("________________/%s/"),

    SIGNATURE_MP("\n\nМП"),

    FOOTNOTE_ONE("В денежных образцах 2009 года согласно Указу Президента РБ №50 от 04.11.2015 " +
            "«О проведении деноминации официальной денежной единицы Республики Беларусь»\n"),

    FOOTNOTE_TWO("По курсу доллара США, установленному Национальным банком Республики Беларусь на дату кадастровой оценки земель\n"),

    FOOTNOTE_THREE("Регистр стоимости – регистр стоимости земель, земельных участков государственного земельного кадастра\n"),

    FOOTNOTE_FOUR("Схема расположения - схема расположения на территории района, сельского (поселкового) Совета, " +
            "населенного пункта, садоводческого или дачного кооператива");

    CommonTypeValues(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }
}
