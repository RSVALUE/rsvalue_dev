package org.nka.rs.util.connection;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

import java.sql.*;
import java.util.ResourceBundle;

/**
 * Created by Shcherbina on 21.09.2016.
 */
public class UtilConnection {

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("hibernate");

    private static PoolDataSource poolInstance;

    public static Connection getPooledConnection() throws SQLException{
        if (poolInstance == null) {
            return initPoolInstance().getConnection();
        } else {
            return poolInstance.getConnection();
        }
    }


    private static PoolDataSource initPoolInstance() {
        try {
            poolInstance = PoolDataSourceFactory.getPoolDataSource();
            poolInstance.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
            poolInstance.setURL(BUNDLE.getString("hibernate.connection.url"));
            poolInstance.setUser(BUNDLE.getString("hibernate.connection.username"));
            poolInstance.setPassword(BUNDLE.getString("hibernate.connection.password"));
            poolInstance.setInitialPoolSize(3);
            poolInstance.setMinPoolSize(3);
            poolInstance.setMaxPoolSize(100);
            poolInstance.setInactiveConnectionTimeout(30);
            poolInstance.setMaxStatements(10);
            poolInstance.setAbandonedConnectionTimeout(10);
            poolInstance.setConnectionPoolName("UniversalConnectionPool-DSp");
            System.out.println("PoolDataSource initialization");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return poolInstance;
    }

    public static Connection getJDBCMySqlConnection(){
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = BUNDLE.getString("db.url");
        String username = BUNDLE.getString("db.username");
        String password = BUNDLE.getString("db.password");
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
        }catch (Exception e){
            e.printStackTrace();
        }
        return conn;
    }

    public static ResultSet getAllObjectsResultSetbyStatment (String exequte, Connection connection){
        Statement statement = null;
        ResultSet result=null;
        try {
            statement = connection.createStatement();
        result = statement.executeQuery(exequte);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }



}
