package org.nka.rs.util.rtftemplate;

import com.lowagie.text.*;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.field.RtfPageNumber;
import com.lowagie.text.rtf.field.RtfTotalPageNumber;
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.nka.rs.util.rtftemplate.functions.Constants.*;
import static org.nka.rs.util.rtftemplate.functions.ParagraphFunctions.*;
import static org.nka.rs.util.rtftemplate.functions.CellFunctions.*;

/**
 * Класс для генерации Rtf документа (СЧЕТ-АКТ)
 */
public class RtfBillActBuilder {

    //Статическая информация для симуляции генерации
    private static final List<String[]> DATA_LIST;

    //Инициализация списка для таблицы
    static {
        java.util.List<String[]> list = new ArrayList<String[]>();
        String[] str;
        str = new String[3];
        str[0] = "1КС";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001001520";
        list.add(str);
        str = new String[3];
        str[0] = "1НБ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000006";
        list.add(str);
        str = new String[3];
        str[0] = "2НБ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000235";
        list.add(str);
        str = new String[3];
        str[0] = "1СХ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000234";
        list.add(str);
        str = new String[3];
        str[0] = "2КС";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000543";
        list.add(str);
        str = new String[3];
        str[0] = "2НБ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000345";
        list.add(str);
        str = new String[3];
        str[0] = "2НБ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000345";
        list.add(str);
        str = new String[3];
        str[0] = "2НБ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000345";
        list.add(str);
        str = new String[3];
        str[0] = "2НБ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000345";
        list.add(str);
        str = new String[3];
        str[0] = "2НБ";
        str[1] = "Брестсткая область, Лунинецкий р-н, район производственной базы РУПП «Гранит»(2 км северо-восточнее д.Ситница)";
        str[2] = "124775500001000345";
        list.add(str);
        DATA_LIST = Collections.unmodifiableList(list);
    }

    //Дата и номер заявления
    private final static String ASSERTION_INFO = "16.06.2016 г. № 12852ф";

    //Сумма числом и прописью
    private final static String SUM = "25,05 (двадцать пять) рублей 05 (пять) копеек";

    //Сумма НДС числом и прописью
    private final static String NDS_SUM = "4,12 (четыре) рубля 12 (двенадцать) копеек";

    //Номер счет-актка
    private final static String ACT_NUMBER = "РСФ – 57/16";

    //Короткое имя организации
    private final static String ORG_SHORT_NAME = "РУП \"Национальное кадастровое агентство\"";

    //НДС
    private final static String NDS_PERCENT = "20%";

    //Дата и номер прейскуранта
    private final static String PRICE = "01.02.2016 №2";

    //Год
    private final static String YEAR = "201_";

    //Функция, симулирующая генерацию
    public static void main(String[] args) {
        Document document = new Document();
        RtfWriter2 writer = null;
        try {
            writer = RtfWriter2.getInstance(document, new FileOutputStream(FILE_NAME));
            document.open();
            document.setMargins(MARGIN_LEFT, MARGIN_RIGHT, MARGIN_TOP, MARGIN_BOTTOM);
            document.add(addHeaderParagraph(String.format(MAIN_DATA_HEADER, ACT_NUMBER)));
            document.add(addParagraph(Element.ALIGN_RIGHT, DATE, addInfoTable()));
            document.add(addParagraph(Element.ALIGN_JUSTIFIED, String.format(MAIN_DATA_FIRST_NEW, ASSERTION_INFO),
                    addDataTable()));
            document.add(addParagraph(Element.ALIGN_JUSTIFIED, String.format(MAIN_DATA_SECOND, SUM, NDS_PERCENT, NDS_SUM, ORG_SHORT_NAME, PRICE), addSignatureTable()));

            RtfHeaderFooter footer = new RtfHeaderFooter(addFooterTable());
            writer.setFooter(footer);
        } catch (Exception de) {
            System.err.println(de.getMessage());
        } finally {
            document.close();
        }
        System.out.println("END");
    }

    /**
     * Функция добавляет к документу таблицу "Сведения о заказчике"
     *
     * @return Table с содержимым
     */
    private static Table addInfoTable() throws BadElementException {
        Table infoTable = new Table(1);
        infoTable.setWidth(100);
        infoTable.addCell(addNoBorder(new Cell(addInfoHeaderParagraph(INFO_CUSTOMER_HEADER))));
        infoTable.addCell(addNoBorder(new Cell(addInfoParagraph(INFO_CUSTOMER))));
        infoTable.addCell(addNoBorder(new Cell(addInfoParagraph(INFO_CUST_REPRESENTATIVE))));
        infoTable.addCell(addNoBorder(new Cell(addInfoParagraph(INFO_CUST_FOUNDATIONS))));
        infoTable.addCell(addNoBorder(new Cell(addInfoHeaderParagraph(INFO_EXECUTOR_HEADER))));
        infoTable.addCell(addNoBorder(new Cell(addInfoParagraph(INFO_EXECUTOR))));
        infoTable.addCell(addNoBorder(new Cell(addInfoParagraph(INFO_EXEC_REPRESENTATIVE))));
        infoTable.addCell(addNoBorder(new Cell(addInfoParagraph(INFO_EXEC_FOUNDATIONS))));
        return infoTable;
    }

    /**
     * Добавляет таблицу Футеров
     * Таблица состоит из 2 столбцов. В левых столбцах отображается
     * Исполнитель и номер счет-акта, в правых - Заказчик и номера
     * страниц документа. Если в документе 1 страница (количество
     * строк в таблице с данными < 7), то в футере отображается
     * только номер счет-акта.
     *
     * @return Table с содержимым
     */
    private static Table addFooterTable() throws BadElementException {
        Table footerTable = new Table(2);
        footerTable.setWidth(100);
        Paragraph pLeft = new Paragraph();
        pLeft.setFont(TIMES_SMALL_SIX);
        pLeft.setAlignment(Element.ALIGN_LEFT);
        pLeft.add(String.format(MAIN_DATA_HEADER, ACT_NUMBER));
        Paragraph pRight = new Paragraph();
        pRight.setFont(TIMES_SMALL_EIGHT);
        pRight.setAlignment(Element.ALIGN_RIGHT);
        pRight.add("Страница ");
        pRight.add(new RtfPageNumber());
        pRight.add(" из ");
        pRight.add(new RtfTotalPageNumber());
        int stringCount = getDataLength();
        if ((DATA_LIST.size() >= 7) || (stringCount >= 7)) {
            footerTable.addCell(addNoBorder(addSignatureCell(EXECUTOR_SIGNATURE)));
            footerTable.addCell(addNoBorder(addSignatureCell(CUSTOMER_SIGNATURE)));
            footerTable.addCell(addNoBorder(new Cell()));
            footerTable.addCell(addNoBorder(new Cell()));
            footerTable.addCell(addNoBorder(new Cell(pLeft)));
            footerTable.addCell(addNoBorder(new Cell(pRight)));
        } else {
            footerTable.addCell(addNoBorder(new Cell(pLeft)));
            footerTable.addCell(addNoBorder(new Cell()));
        }

        return footerTable;
    }

    /**
     * Подсчитывает количество строк в таблице, исходя из данных. Принимаем
     * максимальную длину строки равную 48 символов и каждый элемент нашего
     * List с данными и делим на максимальную длину с округлением в большую
     * сторону. После чего прибавляем получившееся значение к выходному
     * параметру.
     *
     * @return Количество строк в таблице (с округлением в большую сторону)
     */
    private static int getDataLength() {
        int stringCount = 0;
        for (String[] strArray : DATA_LIST) {
            stringCount += Math.ceil(strArray[1].length() / 48D);
        }
        System.out.println(stringCount);
        return stringCount;
    }

    /**
     * Инициализиует объект Table полученными данными по заказу. Для того,
     * чтобы header'ы отображались на последующих страницах используются
     * ячейки header'ы, а так же после их определения вызывается функция
     * endHeaders(). После проходим по итерируемому объекту и добавляем
     * ячейки с необходимыми данным.
     *
     * @return Table с данными по заказу
     */
    private static Table addDataTable() throws BadElementException {
        Table table = new Table(TABLE_COLS);
        table.setWidth(TABLE_WIDTH);
        table.setPadding(2);
        table.normalize();
        table.addCell(addHeaderData(TABLE_HEADER_NUMBER, COLNUMBER));
        table.addCell(addHeaderData(TABLE_HEADER_TYPE, COLTYPE));
        table.addCell(addHeaderData(TABLE_HEADER_ADDRESS, COLADDRESS));
        table.addCell(addHeaderData(TABLE_HEADER_CADNUMBER, COLCADNUMBER));
        table.endHeaders();
        int i = 1;
        for (String[] strArray : DATA_LIST) {
            table.addCell(addData("" + i, COLNUMBER));
            table.addCell(addData(strArray[0], COLTYPE));
            table.addCell(addData(strArray[1], COLADDRESS));
            table.addCell(addData(strArray[2], COLCADNUMBER));
            i++;
        }
        table.complete();
        return table;
    }

    /**
     * Инициализирует объект Table статическими данным. Свойство объекта
     * setTableFistPage устанавливается в true, что означает, что таблица
     * будет неразрывна между страницами. Сперва указывается сводная статическая
     * информация, после чего располагается таблица с информацией по Исполнителю
     * и Заказчику
     *
     * @return Table со сводной информацией и для подписей
     */
    private static Table addSignatureTable() throws BadElementException {
        Table table = new Table(TABLE_COLS);
        table.setTableFitsPage(true);
        table.setWidth(TABLE_WIDTH);
        table.setBorder(Rectangle.NO_BORDER);
        table.addCell(addNoBorder(addSignatureHeaderCell(addParagraph(Element.ALIGN_JUSTIFIED, MAIN_DATA_THIRD))));
        table.addCell(addNoBorder(addRowSpan(addCellBold(TABLE_FOOTER_EXEC, COLSPANLEFT), 10)));
        table.addCell(addNoBorder(addRowSpan(addCellBold("", COLSPANCENTER), 15)));
        table.addCell(addNoBorder(addRowSpan(addCellBold(TABLE_FOOTER_CUST, COLSPANRIGHT), 10)));

        table.addCell(addLeftCell(SIGNATURE_NAME_LEFT));
        table.addCell(addRightCell(SIGNATURE_NAME_RIGHT));
        table.addCell(addLeftCell(SIGNATURE_INFO));
        table.addCell(addRightCell(SIGNATURE_INFO));
        table.addCell(addLeftCell(SIGNATURE_PLACE));
        table.addCell(addRightCell(SIGNATURE_PLACE));

        table.addCell(addLeftCell(SIGNATURE_DATE));
        table.addCell(addRightCell(SIGNATURE_DATE));

        table.addCell(addLeftCell(String.format(YEAR_STRING, YEAR)));
        table.addCell(addRightCell(String.format(YEAR_STRING, YEAR)));
        return table;
    }

}
