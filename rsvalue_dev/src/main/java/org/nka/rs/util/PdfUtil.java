package org.nka.rs.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.util.Map;

/**
 * Created by zgurskaya on 02.11.2016.
 */
public class PdfUtil {

    private final static String fontDest = "C:/Windows/Fonts/times.ttf";

    private final static Font timesLarge = FontFactory.getFont(fontDest, BaseFont.IDENTITY_H, 18);
    private final static Font timesNormal = FontFactory.getFont(fontDest, BaseFont.IDENTITY_H, 12);
    private final static Font timesSmall = FontFactory.getFont(fontDest, BaseFont.IDENTITY_H, 10);


    //добавить ячейку с нормальным шрифтом
    private static PdfPCell addCellWithNormalFont(String phrase) {
        return new PdfPCell(new Phrase(phrase, timesNormal));
    }

    //добавить строку
    public static Paragraph addCommonParagraph(String text, Font font, int alignment, float spacingBefore, float spacingAfter) {
        Paragraph paragraph = new Paragraph(text, font);
        paragraph.setSpacingBefore(spacingBefore);
        paragraph.setSpacingAfter(spacingAfter);
        paragraph.setAlignment(alignment);
        return paragraph;
    }

    //добавить таблицу с нумерацией в первой колонке
    public static Paragraph addTwoColumnsTableWithNumeric(Map<String, String> data) {
        Paragraph paragraph = new Paragraph();
        PdfPTable table = new PdfPTable(3);
        PdfPCell cell = new PdfPCell();
        table.setWidthPercentage(100);
        float[] columnWidths = new float[]{20, 20, 20};
        try {
            table.setWidths(columnWidths);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        int i = 1;
        for (String name : data.keySet()) {
            table.addCell("" + i);
            table.addCell(addCellWithNormalFont(name));
            table.addCell(addCellWithNormalFont(data.get(name)));
            i++;
        }
        paragraph.add(table);
        return paragraph;
    }


    //добавить таблицу
    public static Paragraph addTwoColumnsTable(Map<String, String> data) {
        Paragraph paragraph = new Paragraph();
        PdfPTable table = new PdfPTable(2);
        PdfPCell cell;
        table.setWidthPercentage(100);
        float[] columnWidths = new float[]{30, 30};
        try {
            table.setWidths(columnWidths);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        for (String name : data.keySet()) {
            cell = addCellWithNormalFont(name);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
            cell = addCellWithNormalFont(data.get(name) == null ? "-" : data.get(name));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
        }
        paragraph.add(table);
        return paragraph;
    }

    public static Paragraph addHeaderTableCell(float[] columnWidths, String[] data) {
        Paragraph paragraph = new Paragraph();
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(100);
        try {
            table.setWidths(columnWidths);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        for (String name : data) {
            table.addCell(addCellWithNormalFont(name));
        }
        paragraph.add(table);
        return paragraph;
    }

    //создать титульник
    public static void onStartPage(PdfWriter writer, Document document) {
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Top Left"), 30, 800, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Top Right"), 550, 800, 0);
    }

    //создать футер - в этой библиотеке футер только однострочный, count - величина отступа следующей строки снизу
    public static void onEndPage(PdfWriter writer, String remark, int count) {
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase(remark, timesSmall), 30, 30 + count, 0);
    }

}
