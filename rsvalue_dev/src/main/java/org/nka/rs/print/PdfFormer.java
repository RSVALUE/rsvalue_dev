package org.nka.rs.print;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * Created by zgurskaya on 02.11.2016.
 */
public class PdfFormer {

    public Document createPDF(String destination, String header, String tableHeader, Map<String, String> data, String note) throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(destination));

        document.open();

        /*document.add(PdfUtil.addHeaderParagraph(header, Element.ALIGN_CENTER, 0, 50));
        document.add(PdfUtil.addHeaderParagraph(tableHeader, Element.ALIGN_LEFT, 0, 5));
        document.add(PdfUtil.addTableParagraph(data));
        document.add(PdfUtil.addNoteParagraph(note));
        PdfUtil.onEndPage(pdfWriter,new String(""));*/
        document.close();
        pdfWriter.close();

        return new Document();
    }
}
