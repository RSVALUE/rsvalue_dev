package org.nka.rs.print;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.constant.messageConstant.MessageConstant;
import org.nka.rs.entity.constant.messageConstant.SearchMethodsEnum;
import org.nka.rs.entity.dto.emessage.CadastralInfo;
import org.nka.rs.entity.dto.emessage.CadastralValueInfo;
import org.nka.rs.entity.dto.emessage.PdfData;
import org.nka.rs.util.PdfUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * Created by zgurskaya on 02.11.2016.
 */
public class PdfMessageBuilder implements IConstant, MessageConstant {

    private final static String fontDest = "C:/Windows/Fonts/times.ttf";
    private final static Font timesLarge = FontFactory.getFont(fontDest, BaseFont.IDENTITY_H, 18);
    private final static Font timesNormal = FontFactory.getFont(fontDest, BaseFont.IDENTITY_H, 12);
    private final static Font timesSmall = FontFactory.getFont(fontDest, BaseFont.IDENTITY_H, 10);

    public String createPDF(PdfData data) throws DocumentException, IOException {
        int searchMethodValue = data.getSearchMethodValue();
        Boolean isNb = data.getNb();
        CadastralInfo commonInfo = data.getData();
        Document document = new Document();
        long timeout = System.currentTimeMillis();
        String secure = String.valueOf(timeout) + String.valueOf(searchMethodValue);

        Paragraph emptyParagraph = new Paragraph("\n");
        String fileName = searchMethodValue + "_" + commonInfo.getSearchDate() + "_" + (int) (Math.random() * 100) + ".pdf";
        String destination = directory + File.separator + fileName;

        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(destination));

        document.open();

        document.add(PdfUtil.addCommonParagraph(HEADER_1, timesNormal, Element.ALIGN_CENTER, 0, 10));
        document.add(PdfUtil.addCommonParagraph(HEADER_2, timesNormal, Element.ALIGN_CENTER, 0, 10));
        document.add(PdfUtil.addCommonParagraph(HEADER_3, timesNormal, Element.ALIGN_CENTER, 0, 10));

        Map<String, String> info = new TreeMap<String, String>();

        if (searchMethodValue == SearchMethodsEnum.CADNUM_SEARCH.getTypeValue()) {
            document.add(PdfUtil.addCommonParagraph(INFO_TABLE_NAME_CADASTR_SEARCH, timesNormal, Element.ALIGN_CENTER, 5, 5));
            info.put(INFO_COLUMN_NAME_ADDRESS, commonInfo.getAddress());
            info.put(INFO_COLUMN_NAME_CADNUM, commonInfo.getCadNum());
            info.put(INFO_COLUMN_NAME_SQUARE, commonInfo.getSquare().toString());
            info.put(INFO_COLUMN_NAME_PURPOSE_DIC, commonInfo.getPurposeDic());
            info.put(INFO_COLUMN_NAME_PURPOSE_GOV, commonInfo.getPurposeGov());
            document.add(PdfUtil.addTwoColumnsTable(info));
        } else if (searchMethodValue == SearchMethodsEnum.ADDRESS_POINT_SEARCH.getTypeValue()) {
            document.add(PdfUtil.addCommonParagraph(INFO_TABLE_NAME_COORD_SEARCH, timesNormal, Element.ALIGN_CENTER, 5, 5));
            info.put(INFO_COLUMN_NAME_ADDRESS, commonInfo.getAddress());
            document.add(PdfUtil.addTwoColumnsTable(info));
        } else if (searchMethodValue == SearchMethodsEnum.COMMUNITY_GARDEN_SEARCH.getTypeValue()) {
            document.add(PdfUtil.addCommonParagraph(INFO_TABLE_NAME_COMMUNITY_GARDEN_SEARCH, timesNormal, Element.ALIGN_CENTER, 5, 5));
            info.put(INFO_COLUMN_NAME_ADDRESS, commonInfo.getAddress());
            info.put(INFO_COLUMN_NAME_GARDEN_NAME, commonInfo.getCommunityGardenName());
            info.put(INFO_COLUMN_NAME_NEAR_LOCALITY, commonInfo.getNearLocality());
            document.add(PdfUtil.addTwoColumnsTable(info));
        } else if (searchMethodValue == SearchMethodsEnum.LANDS_OUT_LOCALITY_SEARCH.getTypeValue()) {
            document.add(PdfUtil.addCommonParagraph(INFO_TABLE_NAME_VNP_SEARCH, timesNormal, Element.ALIGN_CENTER, 5, 5));
            info.put(INFO_COLUMN_NAME_ADDRESS, commonInfo.getAddress());
            info.put(INFO_COLUMN_NAME_COST_ZONE_NAME, commonInfo.getZoneName());
            document.add(PdfUtil.addTwoColumnsTable(info));
        } else if (data.getSearchMethodValue() == SearchMethodsEnum.FARM_LANDS.getTypeValue()) {
            document.add(PdfUtil.addCommonParagraph(INFO_TABLE_NAME_FARMLAND_SEARCH, timesNormal, Element.ALIGN_CENTER, 5, 5));
            info.put(INFO_COLUMN_NAME_ADDRESS, commonInfo.getAddress());
            info.put(INFO_COLUMN_NAME_TENANT_NAME, commonInfo.getTenantName());
            info.put(INFO_COLUMN_NAME_UNP, commonInfo.getUnp().toString());
            document.add(PdfUtil.addTwoColumnsTable(info));
        }

        document.add(emptyParagraph);
        document.add(PdfUtil.addCommonParagraph(TABLE_NAME_COST, timesNormal, Element.ALIGN_CENTER, 5, 5));

        if (!data.getNb()) {
            PdfPTable table = getHeaderCostCellsWithoutNb(searchMethodValue, commonInfo.getSearchDate());
            table = getBodyCostCellsWithoutNb(table, data.getValuesCharacters(), commonInfo.getRemark());
            document.add(table);
        } else {
            PdfPTable table = getHeaderCostCellsWithNb(searchMethodValue, commonInfo.getSearchDate());
            table = getBodyCostCellsWithNb(table, data.getValuesCharacters(), commonInfo.getRemark());
            document.add(table);
        }

        if (isNb) {
            String year = commonInfo.getSearchDate().substring(0, 4);
            if (year.equals("2012")) {
                PdfUtil.onEndPage(pdfWriter, REMARK_2012_1, 10);
                PdfUtil.onEndPage(pdfWriter, REMARK_2012_2, 0);
            } else if (year.equals("2017")) {
                PdfUtil.onEndPage(pdfWriter, REMARK_2017_1, 20);
                PdfUtil.onEndPage(pdfWriter, REMARK_2017_2, 10);
                PdfUtil.onEndPage(pdfWriter, REMARK_2017_3, 0);
            } else {
                PdfUtil.onEndPage(pdfWriter, REMARK_ALL + commonInfo.getSearchDate().split("-")[0], 0);
            }
        } else {
            PdfUtil.onEndPage(pdfWriter, REMARK_, 10);
            PdfUtil.onEndPage(pdfWriter, REMARK_2, 0);
        }
        document.close();
        pdfWriter.close();

        return fileName;
    }

    public static PdfPTable getHeaderCostCellsWithoutNb(int searchMethodValue, String date) {
        PdfPTable table = new PdfPTable(new float[]{34, 15, 15, 18, 18});
        table.setWidthPercentage(100);
        PdfPCell cell = new PdfPCell();

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_1, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_2, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_3, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        if (searchMethodValue != SearchMethodsEnum.FARM_LANDS.getTypeValue()) {
            cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_4 + date + COST_COLUMN_NAME_4_2, timesNormal));
            cell.setBorderWidth(1f);
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
        } else {
            cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_4_FARM_LAND + date + COST_COLUMN_NAME_4_2, timesNormal));
            cell.setBorderWidth(1f);
            cell.setColspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
        }
        table.completeRow();

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_5, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_6, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        table.completeRow();

        return table;
    }

    public static PdfPTable getBodyCostCellsWithoutNb(PdfPTable table, List<CadastralValueInfo> valuesCharacters, String remark) {

        PdfPCell cell;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        for (CadastralValueInfo data : valuesCharacters) {

            cell = new PdfPCell(data.getFuncCodeName() == null ? new Paragraph("-") : new Paragraph(data.getFuncCodeName().toString(), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);


            cell = new PdfPCell(data.getDateVal() == null ? new Paragraph("-") : new Paragraph(formatter.format(data.getDateVal()), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(data.getZoneNum() == null ? new Paragraph("-") : new Paragraph(data.getZoneNum().toString(), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(data.getCostD() == null ? new Paragraph("-") : new Paragraph(data.getCostD().toString(), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(data.getCostR() == null ? new Paragraph("-") : new Paragraph(data.getCostR().toString(), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            table.completeRow();

        }

        cell = new PdfPCell(new Paragraph(FOUNDATION, timesNormal));
        cell.setColspan(5);
        cell.setBorderWidth(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(remark, timesNormal));
        cell.setColspan(5);
        cell.setBorderWidth(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        return table;
    }

    public static PdfPTable getHeaderCostCellsWithNb(int searchMethodValue, String date) {
        PdfPTable table = new PdfPTable(new float[]{35, 15, 15, 35});
        table.setWidthPercentage(100);
        PdfPCell cell;
        date = date.split("-")[0];
        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_1, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_2, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_3, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(COST_COLUMN_NAME_7_1 + date + COST_COLUMN_NAME_7_2, timesNormal));
        cell.setBorderWidth(1f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        table.completeRow();

        return table;
    }

    public static PdfPTable getBodyCostCellsWithNb(PdfPTable table, List<CadastralValueInfo> valuesCharacters, String remark) {

        PdfPCell cell;
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

        for (CadastralValueInfo data : valuesCharacters) {

            cell = new PdfPCell(data.getFuncCodeName() == null ? new Paragraph("-") : new Paragraph(data.getFuncCodeName().toString(), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(data.getDateVal() == null ? new Paragraph("-") : new Paragraph(formatter.format(data.getDateVal()), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(data.getZoneNum() == null ? new Paragraph("-") : new Paragraph(data.getZoneNum().toString(), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(data.getCostNB() == null ? new Paragraph("-") : new Paragraph(data.getCostNB().toString(), timesNormal));
            cell.setBorderWidth(1f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            table.completeRow();

        }

        cell = new PdfPCell(new Paragraph(FOUNDATION, timesNormal));
        cell.setColspan(4);
        cell.setBorderWidth(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph(remark, timesNormal));
        cell.setColspan(4);
        cell.setBorderWidth(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);

        return table;
    }


}
