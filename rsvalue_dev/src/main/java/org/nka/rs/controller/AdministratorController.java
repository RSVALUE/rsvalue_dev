package org.nka.rs.controller;

import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.pojos.common.*;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.*;
import org.nka.rs.util.nativedll.NativeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/admin")
public class AdministratorController {

    @Autowired
    private IUserEntityService userEntityService;

    @Autowired
    private IUserAccountService userAccountService;

    @Autowired
    private ITorService torService;

    @Autowired
    private IGroupService groupService;

    @Autowired
    private ISubjectService subjectService;

    @RequestMapping(path = "/tors", method = RequestMethod.GET)
    @ResponseBody
    public List getTors() {
        return torService.getAll(TORDic.class);
    }

    @RequestMapping(path = "/groups", method = RequestMethod.GET)
    @ResponseBody
    public List getGroups() {
        return groupService.getAll(Group.class);
    }

    @RequestMapping(path = "/subjects", method = RequestMethod.GET)
    @ResponseBody
    public List<Subject> getSubjects() {
        return subjectService.getAll(Subject.class);
    }

    @RequestMapping(path = "/username", method = RequestMethod.GET)
    @ResponseBody
    public UserEntity getUsername(@RequestParam(value = "username") String username) {
        UserEntity userEntity = userEntityService.getUserByName(username);
        System.out.println(userEntity.getUsername() + userEntity.getPassword());
        userEntity.setPassword(NativeImpl.getHash(userEntity.getUsername() + userEntity.getPassword()));
        return userEntity;
    }

    @RequestMapping(path = "/user", method = RequestMethod.POST)
    @ResponseBody
    public Integer addUser(@RequestBody UserData userData) {
        System.out.println(userData);
        return (Integer) userAccountService.add(userData);
    }

    @RequestMapping(path = "/accounts", method = RequestMethod.GET)
    @ResponseBody
    public List<UserAccount> getAccounts(@RequestParam(value = "page") Integer page,
                                         @RequestParam(value = "record_count") Integer count) {
        Integer startRecord = (page - 1) * count;
        return userAccountService.getAll(startRecord, count);
    }

    @RequestMapping(path = "/user", method = RequestMethod.DELETE)
    public ResponseWithData delUser(@RequestParam(value = "id") Long id) {
        return userAccountService.deleteById(id);
    }

    @RequestMapping(path = "/user", method = RequestMethod.PUT)
    public void editUser(@RequestBody UserData userData) {
        userAccountService.updateElement(userData);
    }

    @RequestMapping(path = "/account", method = RequestMethod.GET)
    @ResponseBody
    public UserAccount getAcc() {
        return userAccountService.getFullAccount(1L);
    }

    @RequestMapping(path = "/delete/filter", method = RequestMethod.GET)
    @ResponseBody
    public List<UserAccount> getFilter(@RequestParam(value = "surname") String surname,
                                       @RequestParam(value = "login") String login,
                                       @RequestParam(value = "org") String org,
                                       @RequestParam(value = "position") String position,
                                       @RequestParam(value = "group") String group,
                                       @RequestParam(value = "page") Integer page,
                                       @RequestParam(value = "record_count") Integer count) {
        Integer startRecord = (page - 1) * count;
        return userAccountService.getFilterElements(surname, login, org, position, group, startRecord, count);
    }

    @RequestMapping(path = "/edit/filter", method = RequestMethod.GET)
    @ResponseBody
    public List<UserAccount> getFilter(@RequestParam(value = "surname") String surname,
                                       @RequestParam(value = "login") String login,
                                       @RequestParam(value = "org") String org,
                                       @RequestParam(value = "group") String group,
                                       @RequestParam(value = "page") Integer page,
                                       @RequestParam(value = "record_count") Integer count) {
        Integer startRecord = (page - 1) * count;
        return userAccountService.getFilterElements(surname, login, org, group, startRecord, count);
    }

    @RequestMapping(path = "/delete/filter/row", method = RequestMethod.GET)
    @ResponseBody
    public Long getFilterRow(@RequestParam(value = "surname") String surname,
                             @RequestParam(value = "login") String login,
                             @RequestParam(value = "org") String org,
                             @RequestParam(value = "position") String position,
                             @RequestParam(value = "group") String group) {
        return userAccountService.getFilterRowCount(surname, login, org, position, group);
    }

    @RequestMapping(path = "/edit/filter/row", method = RequestMethod.GET)
    @ResponseBody
    public Long getFilterRow(@RequestParam(value = "surname") String surname,
                             @RequestParam(value = "login") String login,
                             @RequestParam(value = "org") String org,
                             @RequestParam(value = "group") String group) {
        return userAccountService.getFilterRowCount(surname, login, org, group);
    }

    @RequestMapping(path = "/rowcount", method = RequestMethod.GET)
    @ResponseBody
    public Long getRowCount() {
        return userAccountService.getRowCount();
    }

    @RequestMapping(path = "/logins", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getLogins() {
        return userEntityService.getLogins();
    }

    @RequestMapping(path = "/tryfilter", method = RequestMethod.GET)
    @ResponseBody
    public List<FilterResponse> tryFilter(@RequestParam(value = "surname") String surname,
                                       @RequestParam(value = "login") String login,
                                       @RequestParam(value = "org") Integer org,
                                       @RequestParam(value = "group") Integer group,
                                       @RequestParam(value = "page") Integer page,
                                       @RequestParam(value = "record_count") Integer count) {
        return userAccountService.getFilterFunction(surname, login, org, group, page, count);
    }

}
