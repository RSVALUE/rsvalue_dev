package org.nka.rs.controller;

import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.DecisionOrganizationDic;
import org.nka.rs.entity.dictionary.LoadDocumentTypeDic;
import org.nka.rs.entity.dictionary.ValueOrganizationDic;
import org.nka.rs.entity.dto.DocInfo;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.SaveDocResponse;
import org.nka.rs.service.IDecOrgService;
import org.nka.rs.service.IDocumentLoadService;
import org.nka.rs.service.ILoadDocTypeService;
import org.nka.rs.service.IValueOrgService;
import org.nka.rs.service.impl.DocumentLoadServiceImpl;
import org.nka.rs.service.impl.LoadDocTypeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 02.06.2016.
 */

@RestController
@RequestMapping(value = "/docLoad")
public class DocumentLoadController implements IConstant {

    Long commonDocId = null;

    public DocumentLoadController() {
    }

    @Autowired
    IValueOrgService valOrgService /*= new ValueOrgServiceImpl()*/;
    @Autowired
    IDecOrgService decOrgService /*= new DecOrgServiceImpl()*/;
    @Autowired
    ILoadDocTypeService loadDocTypeService = new LoadDocTypeServiceImpl();
    @Autowired
    IDocumentLoadService documentLoadService = new DocumentLoadServiceImpl();

    //получение списка местных исполнительных организация и организаций, проводивших оценку
    
    @RequestMapping(value = "/getOrgDic", method = {RequestMethod.POST})
    public List getDictionaries() {
        List allDic = new ArrayList();
        List<DecisionOrganizationDic> decDic = null;
        List<ValueOrganizationDic> valDic = null;

        decDic = decOrgService.getActualOrganization();
        valDic = valOrgService.getActualOrganization();
        allDic.add(decDic);
        allDic.add(valDic);

        return allDic;
    }

    //получение списка обязательных документов
    @RequestMapping(value = "/getNeedDocTypeList", method = {RequestMethod.POST})
    public List<LoadDocumentTypeDic> getNeedDocTypeList(Integer costTypeId, Integer methodTypeId, Integer polyType) {
        return loadDocTypeService.getNeedDocTypes(costTypeId, methodTypeId, polyType);
    }

    //получение списка необязательных документов
    @RequestMapping(value = "/getNotNeedDocTypeList", method = {RequestMethod.POST})
    public List<LoadDocumentTypeDic> getNotNeedDocTypeList(Integer costTypeId, Integer methodTypeId, Integer polyType) {
        return loadDocTypeService.getNotNeedDocTypes(costTypeId, methodTypeId, polyType);
    }

    @RequestMapping(value = "/saveDoc", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<SaveDocResponse> saveDoc(@RequestParam MultipartFile file,
                                                   @RequestParam Integer costTypeId,
                                                   @RequestParam Integer funcCode,
                                                   @RequestParam Long ateId,
                                                   @RequestParam Integer docTypeId,
                                                   @RequestParam String docNum,
                                                   @RequestParam Long dwd_id,
                                                   @RequestParam Long userId) {
        DocInfo docInfo = new DocInfo();
        SaveDocResponse res = new SaveDocResponse();

        //получены ли дополнительные данные для формирования директорий для сохранения файла на сервере
        //получены ли все небходимые данные
        if (funcCode == null || ateId == null || costTypeId == null || docTypeId == null) {
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Не полностью предоставлена информация по загружаемому объекту");
        } else {
            docInfo.setCostTypeId(costTypeId);
            docInfo.setDocTypeId(docTypeId);
            docInfo.setAteId(ateId);
            docInfo.setFuncCode(funcCode);
            docInfo.setDocNum(docNum);

            try {
                res = documentLoadService.saveFile(docInfo, file, userId, dwd_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //response.setCharacterEncoding("utf-8");

        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<SaveDocResponse>(res, h, HttpStatus.OK);
    }

    @RequestMapping(value = "/getCommonDocId", method = RequestMethod.POST)
    public Long getCommonDocId() {
        this.commonDocId = documentLoadService.getCommonDocId();
        return commonDocId;
    }

    @RequestMapping(value = "/clearCommonDocId", method = RequestMethod.POST)
    public void clearCommonDocId() {
        this.commonDocId = null;
    }

    @RequestMapping(value = "/deleteFile", method = {RequestMethod.POST})
    public ResponseEntity<Response> deleteDoc(HttpServletResponse response, String docId) {
        Response res = new Response();

        String path = "";
        Long id = Long.parseLong(docId);
        path = documentLoadService.deleteDoc(id);
        if (path != null && !path.equals("")) {
            File serverFile = new File(path);
            if (serverFile.exists()) {
                Boolean isDelete;
                isDelete = serverFile.delete();
                if (!isDelete) {
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    res.setMessage("Не могу удалить файл по пути " + path + ". Обратитесь к администратору");
                } else {
                    res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    res.setMessage("Файл удален");
                }
            } else {
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                res.setMessage("По указанному пути " + path + " файла не существует. Обратитесь к администратору");
            }
        } else if (path.equals("-1")) {
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при удалении сведений о файле из базы данных. Обратитесь к администратору");
        }

        response.setCharacterEncoding("utf-8");

        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<Response>(res, h, HttpStatus.OK);
    }

}
