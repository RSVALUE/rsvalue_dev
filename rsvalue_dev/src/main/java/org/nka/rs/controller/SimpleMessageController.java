package org.nka.rs.controller;


import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dto.emessage.AddressMessage;
import org.nka.rs.entity.dto.emessage.CadastralValueMessage;
import org.nka.rs.entity.dto.emessage.PdfData;
import org.nka.rs.entity.pojos.load.ATE;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.IAddressService;
import org.nka.rs.service.IMessageService;
import org.nka.rs.service.impl.AddressServiceImpl;
import org.nka.rs.service.impl.MessageServiceImpl;
import org.nka.rs.service.search.AddressService;
import org.nka.rs.service.search.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by zgurskaya on 29.09.2016.
 */

@RestController
@RequestMapping(value = "/message")

public class SimpleMessageController {

    @Autowired
    IMessageService messageService = new MessageServiceImpl();
    @Autowired
    IAddressService addressService = new AddressServiceImpl();

    private AddressService addresssService = new AddressService();

    private MessageService messagesService = new MessageService();


    //Boolean isNb - false - поиск кадастровой стоимости; true- поиск налоговой базы

    // + поиск по кадастровому номеру Boolean isCadastral - true - поиск кадастровой стоимости; false - поиск налоговой базы
    @RequestMapping(value = "/cadastrValueSearch", method = RequestMethod.POST)
    public ResponseWithData cadastrValueSearch (Long soato, Integer blockNum, Integer parcelNum, Boolean isNb, String date) {
            return messagesService.cadastrValueSearch(soato, blockNum, parcelNum, isNb, date);
    }

    // + поиск по адресу
    @RequestMapping(value = "/addressSearch", method = RequestMethod.POST)
    public AddressMessage addressSearch (Long objectnumber, Boolean isNb, String date) {
        return messagesService.addressSearch(objectnumber, isNb, date);
        //return messageService.addressSearch(objectnumber, isNb, date);
    }

    // + поиск по карте
    @RequestMapping(value = "/mapSearch", method = RequestMethod.POST)
    public AddressMessage mapSearch (String coord, Boolean isCadNum) {
        return messageService.mapSearch(coord, isCadNum);
    }

    // + поиск по кадастровому номеру после поиска по адресу, если найдены участки
    @RequestMapping(value = "/addressCadastrValueSearch", method = RequestMethod.POST)
    public ResponseWithData cadastrValueSearch (String cadnum, Boolean isNb, String date) {
        return messagesService.cadastrValueSearch(cadnum, isNb, date);
    }

    // + поиск по адресной точке, если кадастровые номера по заданному адресу не найдены
    @RequestMapping(value = "/pointValueSearch", method = RequestMethod.POST)
    public ResponseWithData pointValueSearch (String address, String zone, Boolean isNb, String date) {
        return messagesService.pointValueSearch(address, zone, isNb, date);
    }

    // + поиск по наименованию садового товарищества
    @RequestMapping(value = "/communityGardenNameSearch", method = RequestMethod.POST)
    public CadastralValueMessage communityGardenNameSearch (Long zoneId, Boolean isNb, String date) {
        return messagesService.communityGardenNameSearch(zoneId, isNb, date);
    }

    // + поиск по территории вне населенных пунктов
    @RequestMapping(value = "/landsOutLocalitySearch", method = RequestMethod.POST)
    public CadastralValueMessage landsOutLocalitySearch (Long zoneId, Boolean isNb, String date) {
        return messagesService.landsOutLocalitySearch(zoneId, isNb, date);
    }

    // + поиск для с/х земель по УНП землепользователя
    @RequestMapping(value = "/farmLandsUNPSearch", method = RequestMethod.POST)
    public CadastralValueMessage farmLandsUNPSearch(Long objectnumber, Long unp, Boolean isNb, String date) {
        return messagesService.farmLandsUNPSearch(objectnumber, unp, isNb, date);
    }

    // + поиск для с/х земель по наименованию землепользователя
    @RequestMapping(value = "/farmLandsNameSearch", method = RequestMethod.POST)
    public CadastralValueMessage farmLandsNameSearch (Long objectnumber, String tenantName, Boolean isNb, String date) {
        return messagesService.farmLandsNameSearch(objectnumber, tenantName, isNb, date);
    }

    // + поиск для с/х земель по номеру оценочной зоны
    @RequestMapping(value = "/farmLandsZonenNumberSearch", method = RequestMethod.POST)
    public CadastralValueMessage farmLandsZonenNumberSearch (String zoneId, Boolean isNb, String date) {
        return messagesService.farmLandsZonenNumberSearch(zoneId, isNb, date);
    }

    // + получение списка садовых товариществ по региону
    @RequestMapping(value = "/getCommunityGardenList", method = RequestMethod.POST)
    public List<String[]> getCommunityGardenList (Long objectnumber, String date) {
        return messagesService.getCommunityGardenList (objectnumber, date);
    }

    // + получение списка наименований землепользователей
    @RequestMapping(value = "/getTenantNameList", method = RequestMethod.POST)
    public List<String> getTenantNameList (Long objectnumber, String date) {
        return messagesService.getTenantNameList(objectnumber, date);
    }

    // + получение списка наименований сельсоветов по выбранному АТЕ
    @RequestMapping(value = "/getLandOutLocalityList", method = RequestMethod.POST)
    public List<String> getLandOutLocalityList (Integer funcode, Long objectnumber, String date) {
        return messagesService.getLandOutLocalityList(funcode, objectnumber, date);
    }

    // + получение списка оценочных зон по выбранному сельсовету
    @RequestMapping(value = "/getZoneNumberList", method = RequestMethod.POST)
    public List<Object []> getZoneNumberList (Integer funcode, Long objectnumber, String date, String zoneName) {
        return messagesService.getZoneNumberList(funcode, objectnumber, date, zoneName);
    }

    // + поиск элементов внутреннего адреса по выбранному элементу
    @RequestMapping(value = "/getAddressElementList", method = RequestMethod.POST)
    public List getAddressElementList(Integer parentElementId, Long objectnumber){
        return messagesService.getAddressElementList(parentElementId, objectnumber);
    }


    //поиск адресных элементов
    /*
    Long region - ИД области
    Long area - ИД района
    Long ss - ИД сельсовета
    Long snp - ИД населенного пункта
    Long bjd - ИД железной дороги
    Long road - ИД автодороги
    Long km - км автородоги
    Long street - ИД улицы
    Integer typeIn - тип входного параметра (код области, района и т.п.)
    Integer typeOut - тип выходного параметра
     */
     // +
    @RequestMapping(value = "/getListAddress", method = RequestMethod.POST)
    public List getListAddress(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street, Integer typeIn, Integer typeOut){
        return messagesService.getListAddress(region, area, ss, snp, bjd, road, km, street, typeIn, typeOut);
    }

    // + поиск по родительскому АТЕ для область-район
    @RequestMapping(value = "/getAteList", method = RequestMethod.POST)
    public List<ATE> getAteList(Long objectnumber){
        return addresssService.findAteForOtherCostType(objectnumber);
    }

    // + список периодов для налоговой базы
    @RequestMapping(value = "/getPeriodList", method = RequestMethod.POST)
    public List<String> getPeriodList(){
        return messagesService.getPeriodList();
    }

    // + получение списка оценочных зон для поиска по с/х землям
    @RequestMapping(value = "/getFarmZoneNumberList ", method = RequestMethod.POST)
    public List<String []> getFarmZoneNumberList (Long objectnumber, String date) {
        return messagesService.getFarmZoneNumberList(objectnumber, date);
    }

    // + получение списка функциональных назначений для вида оценки - вне населенных пунктов
    @RequestMapping(value = "/getFuncCode ", method = RequestMethod.POST)
    public List<FunctionalPurposeDic> getFuncCode () {
        return messagesService.getFuncCode(false);
    }

    @RequestMapping(value = "/getLoadFuncCode ", method = RequestMethod.POST)
    public List<FunctionalPurposeDic> getLoadFuncCode () {
        return messagesService.getFuncCode(true);
    }

    // + сформировать пдф документ
    @RequestMapping(value = "/createPdf", method = RequestMethod.POST)
    public ResponseWithData createPdf (@RequestBody PdfData data,
                                       @RequestParam(value = "isNb") boolean isNb) {
        data.setNb(isNb);
        return messagesService.createPdf(data);
    }

    @RequestMapping(path = "/pdf/get", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void getPdfFile(@RequestParam(value = "fileName") String fileName,
                        HttpServletResponse response) {
        messagesService.initPdfResponse(fileName, response);
    }

    // + удалить пдф документ по его ИД
    @RequestMapping(value = "/pdf/delete", method = RequestMethod.GET)
    public void deletePdf (@RequestParam(value = "fileName") String fileName) {
        messagesService.deletePdfByName(fileName);
    }

}
