package org.nka.rs.controller;

import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.CostTypeDic;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dictionary.MethodTypeDic;
import org.nka.rs.entity.dictionary.PolyTypeDic;
import org.nka.rs.entity.pojos.load.ATE;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.service.*;
import org.nka.rs.service.impl.*;
import org.nka.rs.service.search.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@RestController
@RequestMapping(value = "/paramLoad")
public class ParamLoadController implements IConstant {

    @Autowired
    ICostTypeService costTypeService = new CostTypeServiceImpl();
    @Autowired
    IMethodTypeService methodTypeService = new MethodTypeServiceImpl();
    @Autowired
    IAddressService addressService = new AddressServiceImpl();
    @Autowired
    IFunctionalPurposeService funcCodeService = new FunctionalPurposeServiceImpl();
    @Autowired
    IPolyTypeService polyTypeService = new PolyTypeServiceImpl();
    @Autowired
    IParamService paramService = new ParamServiceImpl();

    private AddressService addresssService = new AddressService();


    //for test
    @RequestMapping(value = "/str", method = RequestMethod.POST)
    public ResponseEntity<String> welcomeGuest(HttpServletResponse response, Long param) {
        String str = "Это первая страница и русский текст " + param;
        if (str.length() == 0) {
            return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
        }
        response.setCharacterEncoding("utf-8");
        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<String>(str, h, HttpStatus.OK);
    }

    @RequestMapping(value = "/getCostType", method = RequestMethod.GET)
    public CostTypeDic getCostTypeItem(Integer costTypeId) {
        CostTypeDic costType = null;
        try {
            costType = (CostTypeDic) costTypeService.getElementById(CostTypeDic.class, costTypeId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return costType;
    }


    //for work

    //получение списка видов оценки
    @RequestMapping(value = "/getCostTypes", method = RequestMethod.POST)
    public List<CostTypeDic> getCostTypesItem() {
        List<CostTypeDic> costTypes = null;
        try {
            costTypes = costTypeService.findDicForFirstStep();
            //costTypes = costTypeService.getAll(CostTypeDic.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return costTypes;
    }

    //получение списка методик оценки
    @RequestMapping(value = "/getMethodType", method = {RequestMethod.POST})
    public List<MethodTypeDic> getMethodTypes(Integer costTypeId) {
        List<MethodTypeDic> methodTypes = null;
        if (costTypeId.equals(FARM_LANDS)) {
            methodTypes = methodTypeService.getMethodTypesForFarmValue();
        } else {
            methodTypes = methodTypeService.getMethodTypesForNonFarmValue();
        }
        return methodTypes;
    }

    //получение списка Городов/районов
    @RequestMapping(value = "/getATE", method = RequestMethod.POST)
    public List<ATE> getATE(Integer costTypeId, Long ate) {
        List ateList;
        if (costTypeId.equals(CITY_AND_PGT)) {
            ateList = addressService.findAteForCityCostType(ate);
        } else {
            ateList = addresssService.findAteForOtherCostType(ate);
        }
        return ateList;
    }


    //получение списка функционального назначения земель
    @RequestMapping(value = "/getFuncCode", method = RequestMethod.POST)
    public List<FunctionalPurposeDic> getFuncCode(Integer costTypeId, Integer methodId, Long objectnumber) {
        List<FunctionalPurposeDic> funcCodes = null;
        if (costTypeId.equals(FARM_LANDS)) {
            funcCodes = funcCodeService.getFuncCodeForFarmValue();
        } else if (costTypeId.equals(COMMUNITY_GARDEN)) {
            funcCodes = funcCodeService.getFuncCodeForGarderCommunity();
        } else if (costTypeId.equals(PARCEL)) {
            funcCodes = funcCodeService.getFuncCodeForParcel();
        } else {
            if (costTypeId.equals(RURAL_LOCALITY) && objectnumber.equals(ATE_MINSK_REGION) && methodId.equals(METHOD_2010_2015)) {
                funcCodes = funcCodeService.getFuncCodeForMinskRegionGardenCommunity();
            } else {
                funcCodes = funcCodeService.getFuncCode();
            }
        }
        return funcCodes;
    }


    //получение списка геометрии
    @RequestMapping(value = "/getPolyType", method = RequestMethod.POST)
    public List<PolyTypeDic> getPolyType() {
        return polyTypeService.getPolyType();
    }


    //проверка наличия объекта в базе. валидация загружаемого объекта
    @RequestMapping(value = "/validateLoadObject", method = RequestMethod.POST)
    public ResponseEntity<Response> validateLoadObject(HttpServletResponse response, Integer costTypeId, Long objectnumber, Integer funcCodeId, String dateValStr) throws SQLException {
        String message = "";
        int code = 0;
        try {

            Object[] data = paramService.validateLoadObject(costTypeId, objectnumber, funcCodeId);
            //если объект по заданным параметрам в базе не найден - переход на страницу прикрепления документов
            if (data == null || data[0] == null) {
                code = ErrorCodeEnum.SUCCESS.getValue();
            } else {
                Long regNum = (Long) data[0];
                Date dateValDB = (Date) data[1];
                Date dateRegStartDB = (Date) data[2];
                Integer methodIdDB = (Integer) data[3];

                Date dateRegStart = new Date();

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date dateVal = null;
                dateVal = formatter.parse(dateValStr);

                //если дата оценки по новым данным больше даты оценки по объекту в базе
                if (dateVal.compareTo(dateValDB) > 0) {
                    //если методика оценки объекта в базе не равен методике 2016 года
                    if (!methodIdDB.equals(METHOD_2016)) {
                        code = ErrorCodeEnum.SUCCESS.getValue();
                    } else {
                        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy");
                        String yearDBStr = formatter1.format(dateRegStartDB);
                        String yearStr = formatter1.format(dateRegStart);
                        Integer yearDB = Integer.parseInt(yearDBStr);
                        Integer year = Integer.parseInt(yearStr);
                        //если методика оценки объекта в базе - методика 2016 - сравнить даты загрузки
                        if (year > (yearDB + 3)) {
                            code = ErrorCodeEnum.SUCCESS.getValue();
                        } else {
                            code = ErrorCodeEnum.NOMINAL_SUCCESS.getValue();
                            message = "Загрузка результатов кадастровой оценки по данному объекту проводилась " + formatter.format(dateRegStartDB) + ". Номер загрузки: " + regNum + " . Продолжить?";
                        }
                    }
                } else {
                    code = ErrorCodeEnum.UNSUCCESS.getValue();
                    message = "В базе данных имеется данный объект с датой оценки " + formatter.format(dateValDB) + ". Номер загрузки: " + regNum + ". Загрузка невозможна!";
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
            code = ErrorCodeEnum.EXCEPTION.getValue();
            message = "Exception when formatting date";
        } catch (Exception e) {
            e.printStackTrace();
            code = ErrorCodeEnum.EXCEPTION.getValue();
            message += "Exception when given data";
        }

        Response res = new Response();
        res.setMessage(message);
        res.setCode(code);

        response.setCharacterEncoding("utf-8");

        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<Response>(res, h, HttpStatus.OK);
    }
}


