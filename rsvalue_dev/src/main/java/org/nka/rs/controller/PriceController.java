package org.nka.rs.controller;

import org.nka.rs.entity.dictionary.OperationTypeDic;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.dto.price.PriceData;
import org.nka.rs.entity.dto.price.SimplePrice;
import org.nka.rs.entity.pojos.price.GetPriceObj;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.IPriceService;
import org.nka.rs.service.impl.PriceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by zgurskaya on 10.10.2016.
 */

@RestController
@RequestMapping(value = "/price")

public class PriceController {


    @Autowired
    IPriceService priceService = new PriceServiceImpl();

    // + получить список ТОРов
    @RequestMapping(value = "/getTorList", method = RequestMethod.POST)
    public List<TORDic> getTorList(Long userId) {
        return priceService.getTorList(userId);
    }

    // + получить данные по прайсам, количество страниц и общее количество записей
    @RequestMapping(value = "/getPriceListWithSorting", method = RequestMethod.POST)
    public Object[] getPriceListWithSorting(String filter, Integer sortingParam, String predicate, Integer page, Integer amount, Long userId, Integer torId) {
        return priceService.getPriceListWithSorting(filter, sortingParam, predicate, page, amount, userId, torId);
    }

    // + просмотр, создание и редактирование прайса в зависимости от входных параметров
    @RequestMapping(value = "/readCreateEditPrice", method = RequestMethod.POST)
    public ResponseWithData readCreateEditPrice(Long userId, Integer torId, Long priceId) {
        return priceService.readCreateEditPrice(userId, torId, priceId);
    }

    // + просмотр, создание и редактирование прайса в зависимости от входных параметров NEW
    @RequestMapping(value = "/readCreateEditPriceNew", method = RequestMethod.POST)
    public GetPriceObj readCreateEditPrice(Long userId, Long priceId) {
        return priceService.readCreateEditPrice(userId,  priceId);
    }

    // + сохранение прейскуранта
    @RequestMapping(value = "/savePriceOld", method = RequestMethod.POST)
    public Response savePrice(@RequestBody PriceData price) {
        return priceService.savePrice(price);
    }


    // + сохранение прейскуранта NEW
    @RequestMapping(value = "/savePrice", method = RequestMethod.POST)
    public Response savePrice2(@RequestBody SimplePrice price) {
        return priceService.savePrice2(price);
    }

    // + прекратить действие прейскуранта
    @RequestMapping(value = "/stopPrice", method = RequestMethod.POST)
    public Response stopPrice(Long userId, Long priceId, String date, Integer operTypeId) {
        return priceService.stopPrice(priceId, date, userId, operTypeId);
    }

    // + получить список типов операций деактуализации
    @RequestMapping(value = "/getOperType", method = RequestMethod.POST)
    public List<OperationTypeDic> getOperType() {
        return priceService.getOperType();
    }

    // + проверка номера прайса - вызывается при потере курсора при создании нового прайса
    @RequestMapping(value = "/validatePriceNumber", method = RequestMethod.POST)
    public Response validatePriceNumber(Integer torId, String priceNumber) {
        return priceService.validatePriceNumber(torId, priceNumber);
    }

}
