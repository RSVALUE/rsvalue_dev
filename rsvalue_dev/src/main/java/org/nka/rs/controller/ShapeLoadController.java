package org.nka.rs.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.json.JSONObject;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dto.*;
import org.nka.rs.entity.responseVar.Report;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.IAddressService;
import org.nka.rs.service.ILayerTypeService;
import org.nka.rs.service.ILoadDataService;
import org.nka.rs.service.impl.AddressServiceImpl;
import org.nka.rs.service.impl.LayerTypeServiceImpl;
import org.nka.rs.service.impl.LoadDataServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zgurskaya on 09.06.2016.
 */

@RestController
@RequestMapping(value = "/shapeLoad")
@MultipartConfig(fileSizeThreshold = 20971520,
        maxFileSize = 31457280)   // 30 MB)
public class ShapeLoadController implements IConstant {

    @Autowired
    ILayerTypeService layerTypeService = new LayerTypeServiceImpl();

    @Autowired
    ILoadDataService loadDataService = new LoadDataServiceImpl();

    @Autowired
    IAddressService addressService = new AddressServiceImpl();

    //получение данных по валидным тематическим слоям
    @RequestMapping(value = "/getValidLayer", method = {RequestMethod.POST})
    public List<LayerType> getValidLayers(String[] layerNames) {
        List layerList = Arrays.asList(layerNames);
        return layerTypeService.getValideLayers(layerList);
    }

    //получение данных по валидным тематическим слоям
    @RequestMapping(value = "/getValideLayerName", method = {RequestMethod.POST})
    public ResponseWithData getValideLayerName(String[] layerNames) {
        List layerList = Arrays.asList(layerNames);
        return layerTypeService.getValideLayerName(layerList);
    }

    //получение данных по всем тематическим слоям
    @RequestMapping(value = "/getLayersData", method = {RequestMethod.POST})
    public List<LayerType> getLayersData(String[] layerNames) {
        List layerList = Arrays.asList(layerNames);
        return layerTypeService.getLayersData(layerList);
    }

    //получение данных по валидным тематическим слоям
    @RequestMapping(value = "/getValideLayerValue", method = {RequestMethod.POST})
    public ResponseWithData getValideLayerValue(@RequestBody AdditionalLayer [] data) {
        return layerTypeService.getValideLayerValue(data);
    }

    //загрузка данных в базу (конечный этап)
    @RequestMapping(value = "/loadData", method = {RequestMethod.POST})
    @ResponseBody
    public Report loadData(@RequestBody LoadData data) throws SQLException {
        getObjectNumber(data);
        return loadDataService.loadData(data);
        /*return data;*/
    }

    //получение objectnumber по parentobjectnumber
    public void getObjectNumber(LoadData data) {
        List<Long> objectnumbers = new ArrayList<Long>();
        if (data.getCostTypeId().equals(CITY_AND_PGT) || data.getCostTypeId().equals(COMMUNITY_GARDEN)) {
            objectnumbers.add(data.getParentObjectNumber());
        } else if (data.getCostTypeId().equals(RURAL_LOCALITY)) {
            objectnumbers = addressService.findAteIdForSNP(data.getParentObjectNumber());
        } else if (data.getCostTypeId().equals(LANDS_OUT_LOCALITY)) {
            objectnumbers = addressService.findAteIdForParcelOutNP(data.getParentObjectNumber());
        }
        Long[] objectNumArr = new Long[objectnumbers.size()];
        objectNumArr = objectnumbers.toArray(objectNumArr);

        data.setObjectNumber(objectNumArr);
    }

    //получение перекодированных зон - только унифицированные поля
    @RequestMapping(value = "/returnZones", method = {RequestMethod.POST})
    @ResponseBody
    public List returnZones(@RequestParam (value = "geojson", required = true)JSONObject geojson) throws UnsupportedEncodingException {
        return loadDataService.encodingZones(geojson);
    }

    //получение перекодированных границ - унифицированные поля
    @RequestMapping(value = "/returnBounds", method = {RequestMethod.POST})
    @ResponseBody
    public List returnBounds(@RequestParam(value = "geojson", required = true) JSONObject geojson) throws UnsupportedEncodingException {
        return loadDataService.encodingBounds(geojson);
    }

    //отправка геометрии для зон на postgreSQL
    @RequestMapping(value = "/getZonesGeometry", method = {RequestMethod.POST})
    public String getGeoJsonForZones(@RequestParam(value = "geojson", required = true)JSONObject geojson,
                                     @RequestParam(value = "dwd_id", required = true)Long dwd_id) {
        return loadDataService.saveZonesGeometry(geojson, dwd_id);
    }

    //отправка геометрии для границ на postgreSQL
    @RequestMapping(value = "/getBoundsGeometry", method = {RequestMethod.POST})
    public String getGeoJsonForBounds(@RequestParam(value = "geojson", required = true)JSONObject geojson,
                                      @RequestParam(value = "dwd_id", required = true)Long dwd_id) {
       return loadDataService.saveBoundsGeometry(geojson, dwd_id);
    }

    //отправка геометрии для слоев (по одному слою) на postgreSQL
    @RequestMapping(value = "/getLayerGeometry", method = {RequestMethod.POST})
    public Response getGeoJsonForLayer(@RequestParam(value = "geojson", required = true)JSONObject geojson,
                                      @RequestParam(value = "dwd_id", required = true)Long dwd_id, @RequestParam(value = "name", required = true) String layerName) {
        return loadDataService.saveLayerGeometry(geojson, dwd_id, layerName);
    }

    //валидация полей для зон (наличие обязательных)
    @RequestMapping(value = "/validateZonesFields", method = {RequestMethod.POST})
    public ResponseEntity<Response> validateZonesFields(@RequestParam(value = "fields") String[] fields, @RequestParam(value = "costTypeId") Integer costTypeId, HttpServletResponse response) {
        Response res = loadDataService.validateZonesFields(fields, costTypeId);

        response.setCharacterEncoding("utf-8");
        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<Response>(res, h, HttpStatus.OK);
    }

    //валидация полей для границ (наличие обязательных)
    @RequestMapping(value = "/validateBoundsFields", method = {RequestMethod.POST})
    public ResponseEntity<Response> validateBoundsFields(@RequestBody String[] fields, HttpServletResponse response) {
        Response res = loadDataService.validateBoundsFields(fields);

        response.setCharacterEncoding("utf-8");
        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<Response>(res, h, HttpStatus.OK);
    }

    //валидация полей для зон (наличие обязательных)
    @RequestMapping(value = "/validateZonesValues", method = {RequestMethod.POST})
    public ResponseEntity<Response> validateZonesValues(@RequestBody ValidZoneVal val, HttpServletResponse response) throws ClassCastException, InvalidFormatException, HttpMessageNotReadableException {

        Response res = loadDataService.validateZonesValues(val.getZones(), val.getCostTypeId(), val.getObjectnumber());
        response.setCharacterEncoding("utf-8");
        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<Response>(res, h, HttpStatus.OK);
    }

    //валидация полей для зон (наличие обязательных)
    @RequestMapping(value = "/validateBoundsValues", method = {RequestMethod.POST})
    public ResponseEntity<Response> validateBoundsValues(@RequestBody Bound [] bounds, HttpServletResponse response) throws ClassCastException {
        Response res = loadDataService.validateBoundsValues(bounds);
        response.setCharacterEncoding("utf-8");
        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<Response>(res, h, HttpStatus.OK);
    }

    @RequestMapping(value = "/createReport", method = {RequestMethod.POST})
    public Object createReport(@RequestParam(value = "ateList") Long [] ateList, Long objectnumber, Integer costTypeId) {
        return loadDataService.createReport(ateList, objectnumber, costTypeId);
    }

    //получить список национальных валют
    @RequestMapping(value = "/getNationalCurrencyList", method = {RequestMethod.POST})
    public List getNationalCurrencyList() {
        return loadDataService.getNationalCurrency();
    }


}
