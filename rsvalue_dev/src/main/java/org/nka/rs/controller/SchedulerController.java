package org.nka.rs.controller;

import org.nka.rs.service.ISchedulerService;
import org.nka.rs.service.impl.SchedulerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by zgurskaya on 01.08.2016.
 */
public class SchedulerController {

    @Autowired
    ISchedulerService schedulerService = new SchedulerServiceImpl();


    //обновление данных по официальному курсу валют
    @Scheduled(cron = "0 0 12 * * ?")
    public void updateCurrency() {
        schedulerService.updateCurrency();
    }

    //ночная очистка данныех - если по какой-либо причине загрузка полностью не прошла
    @Scheduled(cron = "0 0 04 * * ?")
    public void nightClearData() {
        schedulerService.nightClearData();
    }

    //ночная очистка данныех - если по какой-либо причине загрузка полностью не прошла
    @Scheduled(cron = "0 0 04 * * ?")
    public void nightClearPdfFolder() {
        schedulerService.nightClearPdfFolder();
    }


}
