package org.nka.rs.controller;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.nka.rs.entity.dictionary.LoadDocumentTypeDic;
import org.nka.rs.entity.dto.*;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.SaveDocResponse;
import org.nka.rs.service.IRegistrService;
import org.nka.rs.service.search.RegisterService;
import org.nka.rs.service.impl.RegistrServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;


/**
 * Created by zgurskaya on 06.06.2016.
 */


@RestController
@RequestMapping(value = "/registr")
public class UploadRegistrController {


    @Autowired
    IRegistrService registrDTOService = new RegistrServiceImpl();

    private RegisterService registerService = new RegisterService();


    //
    // методы по получению списка загрузок
    //


    //получить записи журнала загрузок - все данные
    @RequestMapping(value = "/getRecords", method = {RequestMethod.POST})
    public Object[] getRegistr(@RequestBody DataForRegistrWork param) {
        return registerService.getRegister(param);
//        return registrDTOService.getRegistr(param);
    }

   /* //получить записи журнала загрузок по выбранным столбцам
    @RequestMapping(value = "/getManagedRecords", method = {RequestMethod.POST})
    public Object[] getManagedRegistr(@RequestBody DataForRegistrWork param) {
        return registrDTOService.getManagedRegistr(param);
    }

    //получить все данные по журналу загрузок
    @RequestMapping(value = "/getAllDataRecords", method = {RequestMethod.POST})
    public Object[] getAllDataRecords(@RequestBody DataForRegistrWork param) {
        return registrDTOService.getAllDataRecords(param);
    }

*/
    //
    // методы по получению конкретной записи
    //

    //получить данные по одной записи журнала загрузок
    @RequestMapping(value = "/getUniqueRecord", method = {RequestMethod.POST})
    public Registr getRegistr(@RequestParam("regNum") Long regNum) {
        return registrDTOService.getUniqueRecord(regNum);
    }

    //получить данные по одной записи журнала загрузок
    @RequestMapping(value = "/getUniqueRecordByParam", method = {RequestMethod.POST})
    public Registr getRegistr(@RequestParam("regNum") Long regNum, @RequestParam("funCode") String funcode) {
        return registrDTOService.getUniqueRecord(regNum, funcode);
    }


    //получить список документов для конкретной загрузки
    @RequestMapping(value = "/getDocumentList", method = {RequestMethod.POST})
    public List<DocumentData> getDocumentList(@RequestParam("regNum") Long regNum) {
        return registrDTOService.getDocumentList(regNum);
    }

    //получить список тематических слоев для конкретной загрузки
    @RequestMapping(value = "/getLayerList", method = {RequestMethod.POST})
    public List<ThematicLayer> getLayerList(@RequestParam("regNum") Long regNum) {
        return registrDTOService.getLayerList(regNum);
    }

    //получить список оценочных зон для конкретной загрузки
    @RequestMapping(value = "/getZoneList", method = {RequestMethod.POST})
    public Object[] getZoneList(@RequestParam("regNum") Long regNum, @RequestParam("sortingParam") Integer sortingParam, @RequestParam("predicate") String predicate, @RequestParam("page") Integer page) {
        return registrDTOService.getZoneList(regNum, sortingParam, predicate, page);
    }

    @RequestMapping(value = "/getRegInf", method = RequestMethod.GET)
    public Registr getRegInf(@RequestParam("regCode") Long regCode,
                             @RequestParam("funcCode") Integer funcCode) {
        return registerService.getLoadInf(regCode, funcCode);
    }

    @RequestMapping(value = "/getLayerList", method = RequestMethod.GET)
    public List<ThematicLayer> getRegInf(@RequestParam("regCode") Long regCode) {
        return registerService.getLayerList(regCode);
    }

    @RequestMapping(value = "/getZoneList", method = {RequestMethod.GET})
    public List<Object[]> getZoneList(@RequestParam("regCode") Long regCode,
                                      @RequestParam("funcCode") Integer funcCode) {
        return registerService.getZoneList(regCode, funcCode);
    }

    //получить список оценочных зон с данными по выбранным столбцам для конкретной загрузки
    @RequestMapping(value = "/getManagedZoneList", method = {RequestMethod.POST})
    public Object[] getManagedZoneList(@RequestParam("regNum") Long regNum, @RequestParam("columnNums") Integer[] columnNums, @RequestParam("sortingParam") Integer sortingParam, @RequestParam("predicate") String predicate, @RequestParam("page") Integer page) {
        return registrDTOService.getManagedZoneList(regNum, columnNums, sortingParam, predicate, page);
    }

    //поиск АТЕ по части строки
    @RequestMapping(value = "/searchByATE", method = {RequestMethod.POST})
    public List searchByATE(@RequestParam("partName") String partName) {
        return registrDTOService.searchByATE(partName.trim());
    }

    // TODO: 21.11.2016  - не помню, что за метод, проверить. возможно для него нужен второй параметр - функциональное назначение
    //получить список необязательных документов для загрузки
    @RequestMapping(value = "/getDocTypeList", method = {RequestMethod.POST})
    public List<LoadDocumentTypeDic> getDocTypeList(@RequestParam("regNum") Long regNum) {
        return registrDTOService.getDocTypeList(regNum);
    }

    //сохранение внесенных изменений
    @RequestMapping(value = "/saveEditDoc", method = {RequestMethod.POST})
    public Response saveEditDoc(@RequestBody EditDocumentData data) {
        return registrDTOService.saveEditDoc(data);
    }

    //загрузить новый файл
    @RequestMapping(value = "/editFile", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<SaveDocResponse> editFile(@RequestParam MultipartFile file, @RequestParam Integer docTypeId, @RequestParam Long regNum, @RequestParam Long userId) {
        SaveDocResponse res = registrDTOService.editFile(file, docTypeId, userId, regNum);

        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<SaveDocResponse>(res, h, HttpStatus.OK);
    }

    //загрузить новый архив с тематическими слоями - чтобы не передавать 10 раз одно и то же, в методе проводится валидация на значение, передача геометрии на постгре и передача данных на Оракл
    @RequestMapping(value = "/editLayer", method = {RequestMethod.POST})
    public Response editLayer(@RequestParam(value = "geojson", required = true) JSONObject geojson,
                              @RequestParam(value = "reg_num", required = true) Long reg_num,
                              @RequestParam(value = "name", required = true) String layerName,
                              @RequestParam(value = "userId", required = true) Long userId) {

        return registrDTOService.editLayer(geojson, reg_num, layerName, userId);
    }


    //скачивание документа
    @RequestMapping(value = "/downloadFile", method = {RequestMethod.POST})
    public void downloadFile(Long id, HttpServletResponse res) {

        res.reset();
        String path = registrDTOService.downloadFile(id);

        File fileToDownload = new File(path);
        if (path.endsWith(".pdf")) {
            res.setContentType("application/pdf");
        } else if (path.endsWith(".xls")) {
            res.setContentType("application/vnd.ms-excel");
        } else if (path.endsWith(".doc")) {
            res.setContentType("application/msword");
        } else if (path.endsWith(".docx")) {
            res.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        } else if (path.endsWith(".exe")) {
            res.setContentType("application/octet-stream");
        } else if (path.endsWith(".jpg")) {
            res.setContentType("image/jpeg");
        } else if (path.endsWith(".png")) {
            res.setContentType("image/png");
        } else if (path.endsWith(".rtf")) {
            res.setContentType("application/rtf");
        } else if (path.endsWith(".txt")) {
            res.setContentType("text/plain");
        } else if (path.endsWith(".xlsx")) {
            res.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        } else if (path.endsWith(".zip")) {
            res.setContentType("application/zip");
        } else {
            res.setContentType("APPLICATION/OCTET-STREAM");
        }

        /*HttpHeaders h = new HttpHeaders();
        h.add("Content-Transfer-Encoding", "binary");
        h.add("Content-disposition", "attachment; filename=" + fileToDownload.getName());*/
        res.setHeader("Content-Transfer-Encoding", "binary");
        res.setHeader("Content-disposition",
                "attachment; filename=" + fileToDownload.getName());
        res.setContentLength((int) fileToDownload.length());

        OutputStream out = null;
        BufferedInputStream fis = null;
        try

        {
            //fis = new BufferedInputStream(new FileInputStream(fileToDownload));
            fis = new BufferedInputStream(new ByteArrayInputStream(FileUtils.readFileToByteArray(fileToDownload)));
            byte[] b = new byte[(int) fileToDownload.length()];
            out = res.getOutputStream();
            int i = 0;
            while ((i = fis.read(b)) != -1) {
                out.write(b);
            }
            fis.close();
            out.flush();
        } catch (
                IOException ex
                )

        {
            ex.printStackTrace();
        } finally

        {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    @RequestMapping(value = "/getURL", method = {RequestMethod.POST})
    public ResponseEntity<String> getURL(HttpServletResponse response, Long id) {
        response.reset();
        String url = registrDTOService.downloadFile(id);
        url = url.replace("\\", "/");
        url = url.replace("//", "/");
        url = "file:/" + url;

        response.setCharacterEncoding("utf-8");
        HttpHeaders h = new HttpHeaders();
        h.add("Content-type", "text/html;charset=UTF-8");

        return new ResponseEntity<String>(url, h, HttpStatus.OK);
    }
}

