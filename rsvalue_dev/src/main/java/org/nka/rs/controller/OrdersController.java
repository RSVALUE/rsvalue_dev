package org.nka.rs.controller;

import org.nka.rs.entity.pojos.order.OrderData;
import org.nka.rs.entity.pojos.order.OrderObject;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.docs.FormService;
import org.nka.rs.service.orders.OrderProcessService;
import org.nka.rs.service.orders.OrdersService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * REST контроллер, принимающий запросы по модулю заказов
 */
@RestController
@RequestMapping(value = "/orders")
public class OrdersController {

    private OrdersService ordersService = new OrdersService();

    private OrderProcessService orderProcessService = new OrderProcessService();

    private FormService formService = new FormService();

    /**
     * Получает список заказов по указанному фильтру
     *
     * @param userId ID пользователя
     * @param orgId ID организации
     * @param urgency Срочность
     * @param decl Заказчик
     * @param isNb Для налогооблажения
     * @param orderState Статус заказа
     * @param filterUserId ID пользователя принявшего заказ
     * @param sort Поле сортировки
     * @param sortType Тип сортировки
     * @param page Страница выборки
     * @param qty Количество записей на странице
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseWithData getGroups(@RequestParam(value = "userId") Long userId,
                                      @RequestParam(value = "orgId") Integer orgId,
                                      @RequestParam(value = "urgency") Integer urgency,
                                      @RequestParam(value = "decl") String decl,
                                      @RequestParam(value = "isNb") Integer isNb,
                                      @RequestParam(value = "orderState") Integer orderState,
                                      @RequestParam(value = "filterUserId") Long filterUserId,
                                      @RequestParam(value = "sort") Integer sort,
                                      @RequestParam(value = "sortType") String sortType,
                                      @RequestParam(value = "page") Integer page,
                                      @RequestParam(value = "qty") Integer qty) {
        return ordersService.getOrders(userId, orgId, urgency, decl, isNb, orderState, filterUserId, sort, sortType, page, qty);
    }

    /**
     * Получает список пользователей в указанной организации
     *
     * @param userId ID пользователя
     * @param orgName Имя организации
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public ResponseWithData getUsersList(@RequestParam(value = "userId") Long userId,
                                         @RequestParam(value = "orgName") Long orgName) {
        return ordersService.getUserList(userId, orgName);
    }

    /**
     * Получает номер нового заказа, если пользователя сотрудник НКА
     * и ничего, если другой организации
     *
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/order/new", method = RequestMethod.GET)
    public ResponseWithData getNewOrder(@RequestParam(value = "userId") Long userId) {
        return ordersService.getNewOrder(userId);
    }

    /**
     * Получает возможные статусы заказов
     *
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/states", method = RequestMethod.GET)
    public ResponseWithData getOrderStates() {
        return ordersService.getAllStates();
    }

    /**
     * Добавляет новый заказ с данными в объекте OrderData
     *
     * @param order Объект OrderData с данными о заказе
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/order/add", method = RequestMethod.POST)
    public ResponseWithData addOrder(@RequestBody OrderData order) {
        return ordersService.addOrder(order);
    }

    /**
     * Сохраняет новые стоимости по объекту
     *
     * @param objId ID объекта
     * @param execId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/order/confirm", method = RequestMethod.GET)
    public ResponseWithData confirmOrder(@RequestParam(value = "objId") Long objId,
                                         @RequestParam(value = "execId") Long execId) {
        return orderProcessService.confirmOrder(objId, execId);
    }

    /**
     * Меняет статус заказа
     *
     * @param orderId ID заказа
     * @param state Новый статус
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/state", method = RequestMethod.PUT)
    public ResponseWithData changeOrderState(@RequestParam(value = "orderId") Long orderId,
                                             @RequestParam(value = "state") Integer state,
                                             @RequestParam(value = "userId") Long userId) {
        return ordersService.changeOrderState(orderId, state, userId);
    }

    /**
     * Получает данные по заказу для изменения\просмотра
     *
     * @param orderId ID заказа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/order/edit", method = RequestMethod.GET)
    public ResponseWithData getEditableOrder(@RequestParam(value = "orderId") Long orderId) {
        return ordersService.getEditableOrder(orderId);
    }

    /**
     * Поиск по адресу
     *
     * @param ate ATE ID
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/address", method = RequestMethod.GET)
    public ResponseWithData getAddress(@RequestParam(value = "ate") Integer ate,
                                       @RequestParam(value = "date") String date) {
        return orderProcessService.addressSearch(ate, date);
    }

    /**
     * Поиск по кадастровому номеру
     *
     * @param soato СОАТО
     * @param blockNum Номер блока
     * @param parcelNum Номер участка
     * @param date Дата поиска
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/cadnum", method = RequestMethod.GET)
    public ResponseWithData getCadNum(@RequestParam(value = "soato") Long soato,
                                      @RequestParam(value = "blockNum") Integer blockNum,
                                      @RequestParam(value = "parcelNum") Integer parcelNum,
                                      @RequestParam(value = "date") String date,
                                      @RequestParam(value = "userId") Long userId) {
        return orderProcessService.cadNumSearch(soato, blockNum, parcelNum, date, userId);
    }

    /**
     * Поиск по СТ по кадастровому номеру
     *
     * @param soato СОАТО
     * @param blockNum Номер блока
     * @param parcelNum Номер участка
     * @param date Дата поиска
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/st/cadnum", method = RequestMethod.GET)
    public ResponseWithData getSTCadNum(@RequestParam(value = "soato") Long soato,
                                        @RequestParam(value = "blockNum") Integer blockNum,
                                        @RequestParam(value = "parcelNum") Integer parcelNum,
                                        @RequestParam(value = "date") String date,
                                        @RequestParam(value = "userId") Long userId) {
        return orderProcessService.stCadNumSearch(soato, blockNum, parcelNum, date, userId);
    }

     /**
     * Возвращает дальнейший список адресов при поиске по адресу
     *
     * @param region Регион
     * @param area Область
     * @param ss Сельсовет
     * @param snp СНП
     * @param bjd Белорусская Ж\Д
     * @param road Номер дороги
     * @param km Километр дороги
     * @param street Улица
     * @param typeIn Входной тип
     * @param typeOut Выходной тип
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/address/list", method = RequestMethod.GET)
    public ResponseWithData getAddressList(@RequestParam(value = "region") Long region,
                                           @RequestParam(value = "area") Long area,
                                           @RequestParam(value = "ss") Long ss,
                                           @RequestParam(value = "snp") Long snp,
                                           @RequestParam(value = "bjd") Long bjd,
                                           @RequestParam(value = "road") Long road,
                                           @RequestParam(value = "km") Long km,
                                           @RequestParam(value = "street") Long street,
                                           @RequestParam(value = "typeIn") Integer typeIn,
                                           @RequestParam(value = "typeOut") Integer typeOut,
                                           @RequestParam(value = "userId") Long userId) {
        return orderProcessService.getAddressList(region, area, ss, snp, bjd, road, km, street, typeIn, typeOut, userId);
    }

    /**
     * Возвращает конечный список адресов
     *
     * @param parentId ID родительского ATE
     * @param ate ID ATE
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/address/element/list", method = RequestMethod.GET)
    public ResponseWithData getAddressElementList(@RequestParam(value = "parentId") Integer parentId,
                                                  @RequestParam(value = "number") Long ate) {
        return orderProcessService.getAddressElementList(parentId, ate);
    }

    /**
     * Возвращает список разрешенных адресов для данного пользователя
     *
     * @param parentId ID родительского ATE
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/ate/list", method = RequestMethod.GET)
    public ResponseWithData getAteList(@RequestParam(value = "parentId") Long parentId,
                                       @RequestParam(value = "userId") Long userId) {
        return orderProcessService.getAteList(parentId, userId);
    }

    /**
     * Возвращает список сельских товарищества по данному ATE
     *
     * @param ateId ID ATE
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/st/list", method = RequestMethod.GET)
    public ResponseWithData getSTList(@RequestParam(value = "number") Long ateId,
                                      @RequestParam(value = "date") String date) {
        return orderProcessService.getSTList(ateId, date);
    }

    /**
     * Возвращает список сельхоз земель по данному ATE
     *
     * @param ateId ID ATE
     * @param date Дата поиска
     * @param type Тип поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/sx/list", method = RequestMethod.GET)
    public ResponseWithData getSXList(@RequestParam(value = "number") Long ateId,
                                      @RequestParam(value = "date") String date,
                                      @RequestParam(value = "type") Integer type) {
        return orderProcessService.getSXList(ateId, date, type);
    }

    /**
     * Поиск по сельхоз землям
     *
     * @param type Тип поиска
     * @param text Текст поиска
     * @param ateId ID ATE
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/sx/search", method = RequestMethod.GET)
    public ResponseWithData getSXSearch(@RequestParam(value = "type") Integer type,
                                        @RequestParam(value = "text") String text,
                                        @RequestParam(value = "ateId") Long ateId,
                                        @RequestParam(value = "date") String date) {
        return orderProcessService.getSXSearch(type, text, ateId, date);
    }

    /**
     * Получает объекты заказа по его ID
     *
     * @param orderId ID объекта заказа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/info", method = RequestMethod.GET)
    public ResponseWithData orderProcess(@RequestParam(value = "orderId") Long orderId) {
        return orderProcessService.orderProcess(orderId);
    }

    /**
     * Добавляет объект в заказ
     *
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/add", method = RequestMethod.POST)
    public ResponseWithData addInOrder(@RequestBody OrderObject orderObject) {
        return orderProcessService.addInOrder(orderObject);
    }

    /**
     * Удаляет объект из заказа
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/remove", method = RequestMethod.POST)
    public ResponseWithData deleteFromOrder(@RequestParam(value = "objId") Long objId) {
        return orderProcessService.removeFromOrder(objId);
    }

    /**
     * Удаляет все объекты из заказа
     *
     * @param orderId ID заказа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/remove/all", method = RequestMethod.POST)
    public ResponseWithData deleteAllFromOrder(@RequestParam(value = "orderId") Long orderId) {
        return orderProcessService.removeAllFromOrder(orderId);
    }

    /**
     * Сбрасывает местоположение объекта
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/location/reset", method = RequestMethod.POST)
    public ResponseWithData resetLocation(@RequestParam(value = "objId") Long objId) {
        return orderProcessService.resetLocation(objId);
    }

    /**
     * Получает стоимости для объекта
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/cost", method = RequestMethod.GET)
    public ResponseWithData costSearch(@RequestParam(value = "objId") Long objId) {
        return orderProcessService.costSearch(objId);
    }

    /**
     * Обновляет номера оценочных зон для объекта
     *
     * @param objId ID объекта
     * @param funcCode Код функционального назначения
     * @param zoneId ID новой зоны
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/update/zone", method = RequestMethod.PUT)
    public ResponseWithData updateZone(@RequestParam(value = "objId") Long objId,
                                       @RequestParam(value = "funcCode") Integer funcCode,
                                       @RequestParam(value = "zoneId") Long zoneId) {
        return orderProcessService.updateZones(objId, funcCode, zoneId);
    }

    /**
     * Переопределяет местоположение объекта
     *
     * @param objId ID объекта
     * @param coordinates Коордитаны нового местоположения
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/update/location", method = RequestMethod.PUT)
    public ResponseWithData updateLocation(@RequestParam(value = "objId") Long objId,
                                           @RequestParam(value = "coordinates") String coordinates) {
        return orderProcessService.updateLocation(objId, coordinates);
    }

    /**
     * Получаем данные по счет акту
     *
     * @param orderId ID заказа
     * @param execId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/act", method = RequestMethod.GET)
    public ResponseWithData getActInfo(@RequestParam(value = "orderId") Long orderId,
                                       @RequestParam(value = "execId") Long execId) {
        return formService.getActInfo(orderId, execId);
    }

    /**
     * Сохраняет новые стоимости по заказу
     *
     * @param orderId ID заказа
     * @param execId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    @RequestMapping(path = "/process/save/all", method = RequestMethod.GET)
    public ResponseWithData saveAllOrder(@RequestParam(value = "orderId") Long orderId,
                                       @RequestParam(value = "execId") Long execId) {
        return orderProcessService.saveAllOrder(orderId, execId);
    }

    @RequestMapping(path = "/process/doc/create", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void createDoc(@RequestParam(value = "objId") Long objId,
                          @RequestParam(value = "execId") Long execId,
                          HttpServletResponse response) {
        formService.createDoc(objId, execId, response);
    }
}
