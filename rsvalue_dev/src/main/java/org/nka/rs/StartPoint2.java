package org.nka.rs;

import oracle.jdbc.OracleTypes;

import org.nka.rs.dao.JDBCDao.orders.OrderProcessDAO;
import org.nka.rs.entity.constant.commonConstant.IConstant;

import org.nka.rs.service.docs.FormService;
import org.nka.rs.util.connection.UtilConnection;
import org.nka.rs.util.nativedll.NativeImpl;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by zgurskaya on 27.09.2016.
 */
public class StartPoint2 implements IConstant {
    private static final String DATA = "{" +
            "\"features\":[{\"type\":\"Feature\"," +
            "\"bbox\":[26.836669921875,54.28875732421875,26.858535766601562,54.3043212890625],\"geometry\":" +
            "{\"type\":\"Polygon\",\"coordinates\":[[[26.8388671875,54.29638671875],[26.839874267578125,54.29827880859375],[26.841018676757812,54.3001708984375],[26.8424072265625,54.30242919921875],[26.843505859375,54.3043212890625],[26.8516845703125,54.302642822265625],[26.858474731445312,54.301239013671875],[26.858535766601562,54.30120849609375],[26.856781005859375,54.298309326171875],[26.856674194335938,54.298126220703125],[26.855758666992188,54.296600341796875],[26.857757568359375,54.296142578125],[26.857681274414062,54.2960205078125],[26.857681274414062,54.295989990234375],[26.85675048828125,54.29437255859375],[26.856338500976562,54.29376220703125],[26.85589599609375,54.293243408203125],[26.8555908203125,54.29327392578125],[26.855438232421875,54.293060302734375],[26.855300903320312,54.2928466796875],[26.85516357421875,54.292877197265625],[26.85504150390625,54.292694091796875],[26.854965209960938,54.292694091796875],[26.854949951171875,54.29266357421875],[26.854721069335938,54.292327880859375],[26.854736328125,54.292327880859375],[26.854202270507812,54.291473388671875],[26.854751586914062,54.2913818359375],[26.854736328125,54.291351318359375],[26.854751586914062,54.291351318359375],[26.854583740234375,54.29083251953125],[26.85357666015625,54.289031982421875],[26.853515625,54.288848876953125],[26.853469848632812,54.28875732421875],[26.853042602539062,54.28887939453125],[26.852310180664062,54.289154052734375],[26.851638793945312,54.289398193359375],[26.851669311523438,54.28948974609375],[26.851837158203125,54.289825439453125],[26.84918212890625,54.290435791015625],[26.84344482421875,54.2916259765625],[26.836669921875,54.2930908203125],[26.836807250976562,54.2933349609375],[26.8388671875,54.29638671875]]]},\"properties\":{\"_isDeleted\":false,\"Area\":\"1493645.09\",\"Cost_d_jm\":\"98.55\",\"Cost_r_jm\":\"1511067\",\"Objectnumb\":\"22669\",\"Num\":\"1522669005\",\"Name\":\"ã. Ìîëîäå÷íî\"}} ]} \n";


    public static void main(String[] args) throws SQLException, IOException {

        //PriceDaoImpl dao = new PriceDaoImpl();
        //System.out.println(dao.pointValueSearch("Аддр", "480485,84851,90151,172049,183589,412605,252748,260484,328374,5213,317285", false, "2016-11-29"));
        //System.out.println(dao.savePriceNew(new SimplePrice("6545DSFLSD", "2016-12-06","", "30,1010,0,0;20,1010,0,0;10,1010,0,0;30,1030,0,0;20,1030,0,0;10,1030,0,0;30,1050,0,0;20,1050,0,0;10,1050,0,0;30,1020,0,0;20,1020,0,0;10,1020,0,0;30,1040,0,0;20,1040,0,0;10,1040,0,0;30,2010,0,0;20,2010,0,0;10,2010,0,0;", (long)1, (long)-1)));
        //System.out.println(dao.getPriceNew((long)1, (long)23));

/*        RegistrDTODaoImpl registrDTODao = new RegistrDTODaoImpl();
        Long[] saveDocList = new Long[1];
        saveDocList[0] = 8115L;
        EditDocumentData editDocumentData = new EditDocumentData();
        editDocumentData.setRegNum(1696L);
        editDocumentData.setUserId(1L);
        editDocumentData.setSave(true);
        registrDTODao.saveEditDoc(editDocumentData);*/

//        OrdersDAO dao = new OrdersDAO();
//        System.out.println(dao.getOrders(38L, null, null, null, null, null, null, 1, "desc", 1, 5));

//        OrderObject orderObject = new OrderObject();
//        orderObject.setOrderId(1L);
//        orderObject.setDate("05.12.2017");
//        orderObject.setSearchType(83);
//        orderObject.setsId(1521122001L);
//        orderObject.setPrevAddress("");
//        orderObject.setCoordinates("");
//        OrderProcessDAO dao = new OrderProcessDAO();
//        dao.addInOrder(orderObject)
//        dao.addInOrder(orderObject);
    }

}
