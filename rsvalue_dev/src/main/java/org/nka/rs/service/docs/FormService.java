package org.nka.rs.service.docs;

import org.apache.commons.io.IOUtils;
import org.nka.rs.dao.JDBCDao.docs.FormExtractDao;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.pojos.docs.BuildInfo;
import org.nka.rs.entity.pojos.docs.FormInfo;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.util.rtftemplate.RtfOrderingBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import static org.nka.rs.service.orders.OrderProcessService.MESSAGE_END;
import static org.nka.rs.service.orders.OrderProcessService.MESSAGE_START;

/**
 * Класс-сервис с методами для формирования выписок
 * и работы с информацией по счет-акту
 */
public class FormService {

    /**
     * Константа-путь сохранения rtf документов
     */
    private final static String PATH = "\\\\ncastorage2\\RegStoimostiTEST\\RTF_DOCUMENTS";

    private static FormExtractDao formExtractDao = new FormExtractDao();

    /**
     * Метод отвечающий за создание и отправку на FE выписок.
     * Сперва метод получает информацию по выписке с помощью DAO-метода.
     * После чего инициализирует объект BuildInfo, с помощью которого
     * будет формироваться выписка. Далее происходит формирование самое выписки.
     * После успешного формирования происходит запись получившейся выписки в
     * потом объекта-ответа.
     *
     * @param objId ID объекта, по которому формируется выписка
     * @param execId ID пользователя, по запросу которого формируется выписка
     * @param response Объект-ответ, в поток которого будет записываться сформированный документ
     */
    public void createDoc(Long objId, Long execId, HttpServletResponse response) {
        FormInfo formInfo = formExtractDao.getDocInfo(objId, execId);
        BuildInfo buildInfo = initBuildInfo(formInfo);
        InputStream in = null;
        try {
            RtfOrderingBuilder.build(buildInfo);
            File file = new File(buildInfo.getDocName());
            String fileName = file.getName();
            in = new BufferedInputStream(new FileInputStream(file));
            response.getOutputStream();
            response.setContentLength(((int) file.length()));
            response.setContentType("application/rtf");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            IOUtils.copy(in, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Метод, проверяющий значения входных параметров и вызывающий соответствующий
     * DAO-метод при успешной проверке
     *
     * @param orderId ID заказа, по которому формируется счет-акт
     * @param execId ID пользователя, по запросу которого формируется счет-акт
     * @return Объект ResponseWithData с данными по результатам выполнения запроса
     */
    public ResponseWithData getActInfo(Long orderId, Long execId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (orderId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("orderId ");
            flag = true;
        }
        if (execId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("execId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return formExtractDao.getActInfo(orderId, execId);
        }
    }

    /**
     * Метод, формирующий название rtf-файла для выписок
     *
     * @param date Дата запроса
     * @param formType Тип выписки
     * @return Название rtf-файла
     */
    private String initDocName(String date, String formType) {
        String year = date.split(" ")[2];
        formType = translit(formType);
        return PATH + File.separator + year + "_" + formType + "_" + (int) (Math.random() * 100) +  ".rtf";
    }

    /**
     * Метод, транслитилирующий название форм выписок на
     * английский язык.
     *
     * @param formType Тип формы
     * @return Транслитное название типа формы
     */
    private String translit(String formType) {
        String formEng = "";
        if (formType.equals(RtfOrderingBuilder.ONE_KS)) {
            formEng = "1KC";
        } else if (formType.equals(RtfOrderingBuilder.TWO_KS)) {
            formEng = "2KC";
        } else if (formType.equals(RtfOrderingBuilder.THREE_KS)) {
            formEng = "3KC";
        } else if (formType.equals(RtfOrderingBuilder.ONE_NB)) {
            formEng = "1HB";
        } else if (formType.equals(RtfOrderingBuilder.TWO_NB)) {
            formEng = "2HB";
        } else if (formType.equals(RtfOrderingBuilder.ONE_SX)) {
            formEng = "1CX";
        }
        return formEng;
    }

    /**
     * Проверяет значение входящего параметра на null значение
     * @param nbYear Год налогооблажения
     * @return null, если год равен 0, иначе - строковое представление
     */
    private String initDocNbYear(int nbYear) {
        return (nbYear == 0) ? null : String.valueOf(nbYear);
    }

    /**
     * Метод парсит данные по объектам выписки в список массивов типа String,
     * для последующего удобства работы с этими данными
     *
     * @param funcPurpose Список функциональных назначений
     * @param valueDate Список дат оценок
     * @param zoneNumber Список номеров оценочных зон
     * @param costD Список стоимостей в USD за м2
     * @param costB Список стоимостей в BYN за м2
     * @param costSqD Список стоимости в USD, полная стоимость
     * @param costSqB Список стоимости в BYN, полная стоимость
     * @return Список массивов типа String с входными данными
     */
    private List<String[]> initDocObject(List<String> funcPurpose, List<String> valueDate, List<Long> zoneNumber, List<String> costD,
                                         List<String> costB, List<String> costSqD, List<String> costSqB) {
        List<String[]> stringArrayList = new ArrayList<String[]>();
        for (int i = 0; i < funcPurpose.size(); i++) {
            String[] stringArray = new String[7];
            stringArray[0] = funcPurpose.get(i);
            stringArray[1] = valueDate.get(i);
            stringArray[2] = String.valueOf(zoneNumber.get(i));
            stringArray[3] = costD.get(i);
            stringArray[4] = costB.get(i);
            stringArray[5] = costSqD.get(i);
            stringArray[6] = costSqB.get(i);
            stringArrayList.add(stringArray);
        }
        return stringArrayList;
    }

    /**
     * Добавляет к заголовку главное таблицы дату поиска
     *
     * @param searchDate Дата поиска
     * @return Сформированная строка
     */
    private String initSearchDate(String searchDate) {
        return " на " + searchDate;
    }

    /**
     * Метод, инициализирующий объект BuildInfo, с помощью которого
     * производится формирование выписки. В нем формируется имя документа,
     * устанавливаются необходимые списки и множества
     *
     * @param formInfo Объект с данными по выписке
     * @return Объект BuildInfo c данными для формирования выписки
     */
    private BuildInfo initBuildInfo(FormInfo formInfo) {
        BuildInfo buildInfo = new BuildInfo();
        buildInfo.setDocName(initDocName(formInfo.getFormDate(), formInfo.getFormName()));
        buildInfo.setDocType(formInfo.getFormName());
        buildInfo.setDocDate(formInfo.getFormDate());
        buildInfo.setDocNumber(formInfo.getFormNumber());
        buildInfo.setDocSearchDate(initSearchDate(formInfo.getSearchDate()));
        buildInfo.setDocNbYear(initDocNbYear(formInfo.getNbYear()));
        buildInfo.setDocStarCount(formInfo.getCurrency());
        buildInfo.setDocInfo(formInfo.getFormInfo());
        buildInfo.setDocObjects(initDocObject(formInfo.getFuncPurpose(), formInfo.getValueDate(), formInfo.getZoneNumber(),
                formInfo.getCostUSD(), formInfo.getCostBYN(), formInfo.getCostSquareUSD(), formInfo.getCostSquareBYN()));
        buildInfo.setDocFoundationNumberSet(new TreeSet<Integer>(formInfo.getFoundationNumber()));
        buildInfo.setDocFoundationNumberList(formInfo.getFoundationNumber());
        buildInfo.setDocFoundationList(formInfo.getFoundationList());
        buildInfo.setDocAttachment(formInfo.getAttachmentList());
        buildInfo.setDocExecPost(formInfo.getExecPost());
        buildInfo.setDocExecFullName(formInfo.getExecFullName());
        return buildInfo;
    }
}
