package org.nka.rs.service.impl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nka.rs.dao.*;
import org.nka.rs.dao.impl.*;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.LoadDocumentTypeDic;
import org.nka.rs.entity.dto.*;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.SaveDocResponse;
import org.nka.rs.service.IRegistrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/*
Created by zgurskaya on 06.06.2016.
*/

@Service
@Transactional
public class RegistrServiceImpl extends BaseServiceImpl implements IRegistrService, IConstant {

    @Autowired
    IRegistrDTODao registrDTODao = new RegistrDTODaoImpl();
    @Autowired
    ILoadDocTypeDao docTypeDao = new LoadDocTypeDaoImpl();
    @Autowired
    ILayerTypeDao layerTypeDao = new LayerTypeDaoImpl();
    @Autowired
    IDocumentLoadDao documentLoadDao = new DocumentLoadDao();
    @Autowired
    ILoadDataDao loadDataDao = new LoadDataDao();


    @Override
    public Object[] getRegistr(DataForRegistrWork param) {
        return registrDTODao.getRegistr(param);
    }

   /* @Override
    public Object[] getManagedRegistr(DataForRegistrWork param) {
        Object[] registrData = registrDTODao.getManagedRegistr(param);
        List<Registr> registrList = (List<Registr>) registrData[0];
        if (registrList != null) {
            for (Registr row : registrList) {
                if (row.getDateRegStop() != null) {
                    row.setActual(false);
                } else {
                    row.setActual(true);
                }
            }
        }
        return registrData;
    }

    @Override
    public Object[] getAllDataRecords(DataForRegistrWork param){
        Object[] registrData = registrDTODao.getAllDataRecords(param);
        List<Registr> registrList = (List<Registr>) registrData[0];
        if (registrList != null) {
            for (Registr row : registrList) {
                if (row.getDateRegStop() != null) {
                    row.setActual(false);
                } else {
                    row.setActual(true);
                }
            }
        }
        return registrData;
    }*/

    @Override
    public Registr getUniqueRecord(Long regNum) {
        Registr reg = registrDTODao.getUniqueRecord(regNum);
        if (reg.getDateRegStop() != null) {
            reg.setActual(true);
        } else {
            reg.setActual(false);
        }
        return reg;
    }


    @Override
    public Registr getUniqueRecord(Long regNum, String funCode) {
        Registr reg = registrDTODao.getUniqueRecord(regNum, funCode);
        if (reg.getDateRegStop() != null) {
            reg.setActual(true);
        } else {
            reg.setActual(false);
        }
        return reg;
    }

    @Override
    public List<DocumentData> getDocumentList(Long regNum) {
        //получить список загруженных документов
        List<DocumentData> docList = registrDTODao.getDocumentList(regNum);
        //получить данные по загрузке
        Object[] data = registrDTODao.getLoadInfo(regNum);
        Integer costTypeId = (Integer) data[0];
        Integer methodTypeId = (Integer) data[1];
        Integer polYtypeId = (Integer) data[2];
        //получить список обязательных документов
        List<LoadDocumentTypeDic> docTypeList = docTypeDao.getNeedDocTypes(costTypeId, methodTypeId, polYtypeId);

        for (DocumentData doc : docList) {
            String path = doc.getPath();
            if(path != null && path.length() != 0) {
                File file = new File(path);
                if (file.exists()) {
                    doc.setName(file.getName());
                    Long sizeL = file.length();
                    Double sizeD = sizeL.doubleValue();
                    doc.setSize(sizeD / 1024);
                }
                path = path.replace("\\", "/");
                path = path.replace("//", "/");
                path = "file:/" + path;
                doc.setPath(path);

                LOOP:
                for (LoadDocumentTypeDic dic : docTypeList) {
                    if (doc.getTypeId().equals(dic.getAnalyticCode())) {
                        doc.setNeed(true);
                        break LOOP;
                    } else {
                        doc.setNeed(false);
                    }
                }
            }
        }

        return docList;
    }

    @Override
    public List<ThematicLayer> getLayerList(Long regNum) {
        return registrDTODao.getLayerList(regNum);
    }

    @Override
    public Object[] getZoneList(Long regNum, Integer sortingParam, String predicate, Integer page) {
        return registrDTODao.getZoneList(regNum, sortingParam, predicate, page);
    }

    @Override
    public Object[] getManagedZoneList(Long regNum, Integer[] columnNums, Integer sortingParam, String predicate, Integer page) {
        return registrDTODao.getManagedZoneList(regNum, columnNums, sortingParam, predicate, page);
    }

    @Override
    public List searchByATE(String partName) {
        return registrDTODao.searchByATE(partName);
    }

    //// TODO: 26.07.2016 метод не написан 
    @Override
    public MultipartFile downloadDoc(Long docId) {
        String path = registrDTODao.downloadDoc(docId);
        return null;
    }

    @Override
    public String downloadFile(Long docId) {
        return registrDTODao.downloadDoc(docId);
    }

    @Override
    public List<LoadDocumentTypeDic> getDocTypeList(Long regNum) {
        List<LoadDocumentTypeDic> docTypeList;
        Object[] data = registrDTODao.getLoadInfo(regNum);
        Integer costTypeId = (Integer) data[0];
        Integer methodTypeId = (Integer) data[1];
        Integer polYtypeId = (Integer) data[2];
        Boolean isLayerSave = (Boolean) data[5];

        docTypeList = docTypeDao.getNotNeedDocTypes(costTypeId, methodTypeId, polYtypeId);
        if (docTypeList == null || docTypeList.size() == 0) {
            return null;
        } else {
            if (!isLayerSave) {
                LoadDocumentTypeDic layer = (LoadDocumentTypeDic) docTypeDao.getElementByID(LoadDocumentTypeDic.class, LAYERS_DOC);
                docTypeList.add(layer);
            }
            return docTypeList;
        }
    }

    @Override
    public Response saveEditDoc(EditDocumentData data) {
        Response res = registrDTODao.saveEditDoc(data);
        if (res.getCode() == ErrorCodeEnum.SUCCESS.getValue()) {
            if (res.getMessage() != null) {
                String pathes = res.getMessage();
                String[] pathArray = pathes.split(";");
                LOOP:
                for (String path : pathArray) {
                    if (path != null && !path.equals("")) {
                        File serverFile = new File(path);
                        if (serverFile.exists()) {
                            Boolean isDelete;
                            isDelete = serverFile.delete();
                            if (!isDelete) {
                                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                res.setMessage("Не могу удалить файл по пути " + path + ". Обратитесь к администратору");
                                break LOOP;
                            } else {
                                res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                                res.setMessage("");
                            }
                        } else {
                            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                            res.setMessage("По указанному пути " + path + " файла не существует. Обратитесь к администратору");
                            break LOOP;
                        }
                    }
                }
            }
            return res;
        } else {
            return res;
        }
    }

    @Override
    public SaveDocResponse editFile(MultipartFile file, Integer docTypeId, Long userId, Long regNum) {
        SaveDocResponse res = new SaveDocResponse();
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());
        res.setMessage("");

        //если документ - архив, высчитать размер исходного файла (папки)
        String name = file.getOriginalFilename();
        String[] part = name.split("\\.");
        int length = part.length;
        String extension = part[length - 1];
        if (extension.equals("zip")) {
            res = getValideZipLenght(file);
            if (res.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                return res;
            }
        }

        Object[] data = registrDTODao.getLoadInfo(regNum);
        if (data == null) {
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при получении данных о загрузке");
            return res;
        } else {
            DocInfo docInfo = new DocInfo();
            docInfo.setCostTypeId((Integer) data[0]);
            docInfo.setDocTypeId(docTypeId);
            docInfo.setAteId((Long) data[4]);
            docInfo.setFuncCode((Integer) data[3]);

            // Creating the directory to store file
            //путь к директории
            String path = getPath(docInfo);
            //путь к файлу
            String filePath = path + File.separator + file.getOriginalFilename();
            File outputFile = new File(filePath);

            int count = 1;
            String[] namePart = new String[2];
            Boolean isExist = true;

            while (isExist) {
                if (outputFile.exists()) {
                    namePart = file.getOriginalFilename().split("\\.");
                    filePath = path + File.separator + namePart[0] + "(" + count + ")." + namePart[1];
                    outputFile = new File(filePath);
                    count++;
                } else {
                    isExist = false;
                }
            }
            InputStream reader = null;
            FileOutputStream writer = null;
            int totalBytes = 0;

            try {
                byte[] buffer = file.getBytes();
                outputFile.createNewFile();
                reader = file.getInputStream();
                writer = new FileOutputStream(outputFile);
                int bytesRead = 0;
                while ((bytesRead = reader.read(buffer)) != -1) {
                    writer.write(buffer);
                    totalBytes += bytesRead;
                }
                Long id = registrDTODao.editFile(filePath, docTypeId, userId, regNum);

                if (id != null && id > 0 && !id.toString().equals("")) {
                    if (count != 1) {
                        res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                        res.setMessage("Файл с таким именем уже был загружен в файловое хранилище. Файл сохранен под именем " + namePart[0] + "(" + count + ")." + namePart[1]);
                        res.setName(namePart[0] + "(" + count + ")." + namePart[1]);
                        res.setId(id.toString());
                    } else {
                        res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                        res.setId(id.toString());
                    }
                } else {
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    res.setMessage("Ошибка при сохранении объекта в базу");
                    File serverFile = new File(filePath);
                    if (serverFile.exists()) {
                        serverFile.delete();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                res.setMessage("Ошибка при записи объекта в файловое хранилище");
            } catch (ClassCastException e) {
                e.printStackTrace();
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                res.setMessage("Ошибка при записи объекта в файловое хранилище");
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return res;
        }
    }

    public SaveDocResponse getValideZipLenght(MultipartFile file) {
        SaveDocResponse res = new SaveDocResponse();
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());

        Long size = 0L;
        try {
            ZipInputStream zis = new ZipInputStream(file.getInputStream());
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                size += entry.getSize();
            }
            if (size >= 31457280) {
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                res.setMessage("Размер распакованного архива больше 30 Mb!!! Угроза безопасности!!! Архив будет удален!!!");
                return res;
            } else {
                return res;
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка! Неверная структура zip-архива! В архиве не должно быть внутренних папок!!! Загрузка невозможна! Архив будет удален!!!");
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при получении размера распакованного файла");
            return res;
        }
    }

    public String getPath(DocInfo docInfo) {
        // Creating the directory to store file
        String path = directory + File.separator;
        if (docInfo.getCostTypeId().equals(CITY_AND_PGT)) {
            path += "CITY_AND_PGT" + File.separator;
        } else if (docInfo.getCostTypeId().equals(RURAL_LOCALITY)) {
            path += "RURAL_LOCALITY" + File.separator;
        } else if (docInfo.getCostTypeId().equals(COMMUNITY_GARDEN)) {
            path += "COMMUNITY_GARDEN" + File.separator;
        } else if (docInfo.getCostTypeId().equals(LANDS_OUT_LOCALITY)) {
            path += "LANDS_OUT_LOCALITY" + File.separator;
        } else if (docInfo.getCostTypeId().equals(FARM_LANDS)) {
            path += "FARM_LANDS" + File.separator;
        }

        if (docInfo.getFuncCode().equals(LIVING_APARTMENT_ZONE)) {
            path += "LIVING_APARTMENT_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(LIVING_MANOR_ZONE)) {
            path += "LIVING_MANOR_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(LIVING_MANOR_ZONE_FARMSTEAD_TYPE)) {
            path += "LIVING_MANOR_ZONE_FARMSTEAD_TYPE" + File.separator;
        } else if (docInfo.getFuncCode().equals(LIVING_MANOR_ZONE_COTTAGE_TYPE)) {
            path += "LIVING_MANOR_ZONE_COTTAGE_TYPE" + File.separator;
        } else if (docInfo.getFuncCode().equals(RECREATION_ZONE)) {
            path += "RECREATION_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(BUSINESS_ZONE)) {
            path += "BUSINESS_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(PRODUCTION_ZONE)) {
            path += "PRODUCTION_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(GARDENING_ASSOTIATIONS)) {
            path += "GARDENING_ASSOTIATIONS" + File.separator;
        } else if (docInfo.getFuncCode().equals(ARABLE_LAND)) {
            path += "ARABLE_LAND" + File.separator;
        } else if (docInfo.getFuncCode().equals(IMPROVED_MEADOW_LAND)) {
            path += "IMPROVED_MEADOW_LAND" + File.separator;
        } else if (docInfo.getFuncCode().equals(NATURAL_MEADOW_LAND)) {
            path += "NATURAL_MEADOW_LAND" + File.separator;
        }

        LocalDate date = LocalDate.now();
        path += date.getYear() + File.separator + docInfo.getAteId() + File.separator;

        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return path;
    }


    //запись нового тематического слоя при редактировании. до работы saveEditDoc - неактивен.
    @Override
    public Response editLayer(JSONObject geojson, Long reg_num, String layerName, Long userId) {
        Response response = new Response();
        String values = "";
        Long dwd_id;

        response = getValidateValue(geojson, layerName);
        if (response.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
            return response;
        } else {
            values = response.getMessage();
        }
        dwd_id =  getCommonDocId();
        response = loadDataDao.saveLayerGeometry(geojson, dwd_id, layerName);
        if(response.getCode() != ErrorCodeEnum.SUCCESS.getValue()){
            return  response;
        } else {
            response.setMessage(dwd_id.toString());
        }
        response = registrDTODao.insertEditLayer(reg_num, layerName, userId, values);
        if(response.getCode() != ErrorCodeEnum.SUCCESS.getValue()){
            return  response;
        } else {
            response.setMessage(dwd_id.toString());
        }
        return response;
    }

    public Long getCommonDocId() {
        return documentLoadDao.getCommonDocId();
    }

    public Response getValidateValue(JSONObject geojson, String layerName) {
        Response response = new Response();
        response.setCode(ErrorCodeEnum.SUCCESS.getValue());
        response.setMessage("");
        Integer analyticCode = registrDTODao.findLayerCodeByLayerFileName(layerName);

        String values = "";
        JSONArray features = null;
        JSONObject properties = null;

        if (geojson.has("features")) {
            features = geojson.getJSONArray("features");
            /*if ((analyticCode != GRUNT && analyticCode != DOSTTRANSP) && features.length() > 1) {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Несколько типов геометрии для слоя " + layerName + ". Загрузка невозможна!!!");
            } else {*/
            for (int i = 0; i < features.length(); i++) {
                JSONObject obj = features.getJSONObject(i);
                if (obj.has("properties")) {
                    properties = obj.getJSONObject("properties");
                    if (properties != null) {
                        //проверить на наличик поля code
                        if (properties.has("CODE")) {
                            try {
                                Integer code = properties.getInt("CODE");
                                if (code != null) {
                                    if (analyticCode.equals(DOSTTRANSP)) {
                                        List<Integer> disnance = layerTypeDao.getDistanceCodeValue();
                                        if (!disnance.contains(code)) {
                                            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                            response.setMessage("В слое Доступность, в поле code записан код, для которого нет сооответствия в классификаторе! Архив невалидный и будет удален");
                                            return response;
                                        } else {
                                            values += code + ";";
                                        }
                                    } else if (analyticCode.equals(GRUNT)) {
                                        List<Integer> ground = layerTypeDao.getGroundCodeValue();
                                        if (!ground.contains(code)) {
                                            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                            response.setMessage("В слое Грунт под строительство, в поле code записан код, для которого нет сооответствия в классификаторе! Архив невалидный и будет удален");
                                            return response;
                                        }else {
                                            values += code + ";";
                                        }
                                    }
                                } else {
                                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                    response.setMessage("В слое " + layerName + " имеется поле CODE со значением null!!! Загрузка невозможна!");
                                    return response;
                                }
                            } catch (JSONException e) {
                                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                response.setMessage("В слое " + layerName + " имеется поле CODE с текстовым значением!!! Загрузка невозможна!");
                                e.printStackTrace();
                                return response;
                            }
                        } else {
                            if (analyticCode.equals(DOSTTRANSP) || analyticCode.equals(GRUNT)) {
                                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                response.setMessage("В слоях Грунт под строительство или Доступность не имеется поля code! Архив невалидный и будет удален");
                                return response;
                            }
                        }
                    } else {
                        if (analyticCode.equals(DOSTTRANSP) || analyticCode.equals(GRUNT)){
                            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                            response.setMessage("В слоях Грунт под строительство или Доступность не имеется поля code! Архив невалидный и будет удален");
                            return response;
                        }
                    }
                } else {
                    if (analyticCode.equals(DOSTTRANSP) || analyticCode.equals(GRUNT)){
                        response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                        response.setMessage("В слоях Грунт под строительство или Доступность не имеется поля code! Архив невалидный и будет удален");
                        return response;
                    }
                }
            }
            /*}*/
        } else {
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Неверная структура слоя " + layerName);
            return response;
        }
        response.setMessage(values);
        return response;
    }


}
