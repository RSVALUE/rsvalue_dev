package org.nka.rs.service.impl;

import org.nka.rs.dao.ITorDao;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.service.ITorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TorServiceImpl extends BaseServiceSessionImpl<TORDic>  implements ITorService{

    @Autowired
    ITorDao torDao;

    @Override
    public List<String> getCodeNames() {
        return torDao.getCodeNames();
    }

    @Override
    public Integer getCodeByName(String name) {
        return torDao.getCodeByName(name);
    }
}
