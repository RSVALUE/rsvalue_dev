package org.nka.rs.service.impl;

import org.nka.rs.dao.ISubjectDao;
import org.nka.rs.entity.pojos.common.Subject;
import org.nka.rs.service.ISubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SubjectServiceImpl extends BaseServiceSessionImpl<Subject> implements ISubjectService {

    @Autowired
    ISubjectDao subjectDao;

    @Override
    public List<String> getSubjectNames() {
        return subjectDao.getSubjectNames();
    }

    @Override
    public Subject getSubjectByFullName(String surname, String firstName, String fatherName) {
        return subjectDao.getSubjectByFullName(surname, firstName, fatherName);
    }

    @Override
    public Long getIdByFullName(String surname, String firstName, String fatherName) {
        return subjectDao.getIdByFullName(surname, firstName, fatherName);
    }

    @Override
    public String getFullNameById(Long id) {
        return subjectDao.getFullNameById(id);
    }
}
