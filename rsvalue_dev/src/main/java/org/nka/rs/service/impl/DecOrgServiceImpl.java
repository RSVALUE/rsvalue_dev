package org.nka.rs.service.impl;

import org.nka.rs.dao.IDecOrgDao;
import org.nka.rs.dao.impl.DecOrgDaoImpl;
import org.nka.rs.entity.dictionary.DecisionOrganizationDic;
import org.nka.rs.service.IDecOrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 02.06.2016.
 */

@Service
@Transactional
public class DecOrgServiceImpl extends BaseServiceImpl implements IDecOrgService {

    @Autowired
    IDecOrgDao decOrgDao = new DecOrgDaoImpl();

    @Override
    public List<DecisionOrganizationDic> getActualOrganization() {
        return decOrgDao.getActualOrganization();
    }
}
