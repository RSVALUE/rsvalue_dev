package org.nka.rs.service;

import org.nka.rs.entity.dictionary.DecisionOrganizationDic;

import java.util.List;

/**
 * Created by zgurskaya on 02.06.2016.
 */
public interface IDecOrgService extends IBaseService {

    List<DecisionOrganizationDic> getActualOrganization();
}
