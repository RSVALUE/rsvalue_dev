package org.nka.rs.service.impl;

import org.nka.rs.dao.IAuthenticationDao;
import org.nka.rs.dao.impl.AuthenticationDaoImpl;
import org.nka.rs.service.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by zgurskaya on 04.10.2016.
 */

@Service
@Transactional
public class AuthenticationServiceImpl implements IAuthenticationService {


    @Autowired
    IAuthenticationDao authenticationDao = new AuthenticationDaoImpl();

    @Override
    public Long getUserId(String userName) {
        return authenticationDao.getUserId(userName);
    }

    @Override
    public String getPersonData(String userName) {
        return authenticationDao.getPersonData(userName);
    }

    @Override
    public void logout(String username, String token) {
        authenticationDao.logout(username, token);
    }
}
