package org.nka.rs.service;

import java.sql.SQLException;

/**
 * Created by zgurskaya on 17.06.2016.
 */
public interface IParamService extends IBaseService {

    Object[] validateLoadObject(Integer costTypeId, Long objectnumber, Integer funcCodeId) throws SQLException;
}
