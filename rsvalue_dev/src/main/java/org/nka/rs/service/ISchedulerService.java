package org.nka.rs.service;

/**
 * Created by zgurskaya on 01.08.2016.
 */
public interface ISchedulerService {

    //загрузка официального курса валют
    void updateCurrency();
    //ночная очистка данныех - если по какой-либо причине загрузка полностью не прошла
    void nightClearData();
    void nightClearPdfFolder();


}
