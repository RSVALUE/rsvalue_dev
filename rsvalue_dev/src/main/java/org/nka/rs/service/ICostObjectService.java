package org.nka.rs.service;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface ICostObjectService<CostObject>  extends IBaseService {

    List<CostObject> getCostObjectByRequiredParam(Integer costTypeId, Integer funcCodeId, Long objectnumber);

}
