package org.nka.rs.service;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface IMethodTypeService<MethodTypeDic> extends IBaseService {

    List<MethodTypeDic> findByParentCode(Integer id);
    List<MethodTypeDic> getMethodTypesForFarmValue();
    List<MethodTypeDic> getMethodTypesForNonFarmValue();
    MethodTypeDic findByCodeName(String name);
}
