package org.nka.rs.service;

import org.nka.rs.entity.dictionary.TORDic;

import java.util.List;

public interface ITorService extends IBaseService<TORDic> {

    List<String> getCodeNames();

    Integer getCodeByName(String name);

}
