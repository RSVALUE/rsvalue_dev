package org.nka.rs.service;

/**
 * Created by zgurskaya on 04.10.2016.
 */
public interface IAuthenticationService {

    //получить userId по логину
    Long getUserId(String userName);
    //получить ФИО по логину
    String getPersonData(String userName);
    //выйти из системы
    void logout(String username, String token);


}
