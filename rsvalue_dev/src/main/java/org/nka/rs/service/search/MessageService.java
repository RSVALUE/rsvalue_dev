package org.nka.rs.service.search;

import com.itextpdf.text.DocumentException;
import org.apache.commons.io.IOUtils;
import org.nka.rs.dao.JDBCDao.search.MessageDAO;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dto.emessage.AddressMessage;
import org.nka.rs.entity.dto.emessage.CadastralValueMessage;
import org.nka.rs.entity.dto.emessage.PdfData;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.print.PdfMessageBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

/**
 * MessageService, переписанный без использования аннотаций
 * Hibernate
 */
public class MessageService {

    private MessageDAO dao = new MessageDAO();

    public AddressMessage addressSearch(Long objectnumber, Boolean isNb, String date) {
        return dao.addressSearch(objectnumber, isNb, date);
    }

    public ResponseWithData cadastrValueSearch(Long soato, Integer blockNum, Integer parcelNum, Boolean isNb, String date) {
        return dao.cadastrValueSearch(soato, blockNum, parcelNum, isNb, date);
    }

    public ResponseWithData cadastrValueSearch(String cadNum, Boolean isNb, String date) {
        return dao.cadastrValueSearch(cadNum, isNb, date);
    }

    public ResponseWithData pointValueSearch(String address, String zone, Boolean isNb, String date) {
        return dao.pointValueSearch(address, zone, isNb, date);
    }

    public CadastralValueMessage communityGardenNameSearch(Long zoneId, Boolean isNb, String date) {
        return dao.communityGardenNameSearch(zoneId, isNb, date);
    }

    public CadastralValueMessage landsOutLocalitySearch(Long zoneId, Boolean isNb, String date) {
        return dao.landsOutLocalitySearch(zoneId, isNb, date);
    }

    public CadastralValueMessage farmLandsUNPSearch(Long objectnumber, Long unp, Boolean isNb, String date) {
        return dao.farmLandsUNPSearch(objectnumber, unp, isNb, date);
    }

    public CadastralValueMessage farmLandsNameSearch(Long objectnumber, String tenantName, Boolean isNb, String date) {
        return dao.farmLandsNameSearch(objectnumber, tenantName, isNb, date);
    }

    public CadastralValueMessage farmLandsZonenNumberSearch(String zoneId, Boolean isNb, String date) {
        return dao.farmLandsZonenNumberSearch(zoneId, isNb, date);
    }

    public List<String[]> getCommunityGardenList(Long objectnumber, String date) {
        return dao.getCommunityGardenList(objectnumber, date);
    }

    public List<String> getTenantNameList(Long objectnumber, String date) {
        return dao.getTenantNameList(objectnumber, date);
    }

    public List<Object[]> getZoneNumberList(Integer funcode, Long objectnumber, String date, String zoneName) {
        return dao.getZoneNumberList(funcode, objectnumber, date, zoneName);
    }

    public List<String[]> getFarmZoneNumberList(Long objectnumber, String date) {
        return dao.getFarmZoneNumberList(objectnumber, date);
    }

    public List<Object[]> getAddressElementList(Integer parentElementId, Long objectnumber) {
        return dao.getAddressElementList(parentElementId, objectnumber);
    }

    public List<String> getLandOutLocalityList(Integer funcode, Long objectnumber, String date) {
        return dao.getLandOutLocalityList(funcode, objectnumber, date);
    }

    public List<String> getPeriodList() {
        return dao.getPeriodList();
    }

    public List getListAddress(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street, Integer typeIn, Integer typeOut) {
        return dao.getListAddress(region, area, ss, snp, bjd, road, km, street, typeIn, typeOut);
    }

    public List<FunctionalPurposeDic> getFuncCode(boolean load) {
        return dao.getFuncCode(load);
    }

    public Long savePdfPath(String url) {
        return dao.savePdfPath(url);
    }

    public String searchPathById(Long id) {
        return dao.searchPathById(id);
    }

    /**
     * Метод, создающий pdf-документ
     *
     * @param data Данные pdf-документа
     * @return Объект-ответ с данными о результатах создания pdf-документа
     */
    public ResponseWithData createPdf(PdfData data) {
        ResponseWithData response = new ResponseWithData();
        PdfMessageBuilder builder = new PdfMessageBuilder();
        try {
            String fileName = builder.createPDF(data);
            if (fileName != null && !fileName.equals("")) {
                response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                response.setData(fileName);
            } else {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Ошибка при создании документа");
            }

        } catch (DocumentException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return response;
    }

    /**
     * Метод, записывающий pdf-документ в поток объекта-ответа.
     * Метод находит pdf-документ по входящему параметру его названия,
     * после чего копирует его в байтовый поток
     *
     * @param fileName Название pdf-документа
     * @param response Объект-ответ
     */
    public void initPdfResponse(String fileName, HttpServletResponse response) {
        String filePath = PdfMessageBuilder.directory + File.separator + fileName;
        File file = new File(filePath);
        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            response.getOutputStream();
            response.setContentLength(((int) file.length()));
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            IOUtils.copy(in, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    /**
     * Производит удаление pdf-документа по имени файла
     *
     * @param fileName Имя pdf-документа
     */
    public void deletePdfByName(String fileName) {
        String filePath = PdfMessageBuilder.directory + File.separator + fileName;
        File serverFile = new File(filePath);
        if (serverFile.exists()) {
            boolean isDeleted = serverFile.delete();
            if (!isDeleted) {
                System.out.println("Не могу удалить файл по пути " + filePath + ". Обратитесь к администратору");
            }
        } else {
            System.out.println("По указанному пути " + filePath + " файла не существует. Обратитесь к администратору");
        }
    }

}
