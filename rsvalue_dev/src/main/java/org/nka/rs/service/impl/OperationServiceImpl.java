package org.nka.rs.service.impl;

import org.nka.rs.dao.IOperationDao;
import org.nka.rs.entity.pojos.common.Operation;
import org.nka.rs.service.IOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

@Service
@Transactional
public class OperationServiceImpl extends BaseServiceSessionImpl<Operation> implements IOperationService{

    @Autowired
    IOperationDao operationDao;

    @Override
    public Operation getElementById(Long id) {
        return operationDao.getElementByID(id);
    }

    @Override
    public Long getIdByDate(Date date) {
        return operationDao.getIdByDate(date);
    }
}
