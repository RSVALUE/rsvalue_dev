package org.nka.rs.service.search;

import org.nka.rs.dao.JDBCDao.search.AddressDAO;
import org.nka.rs.entity.pojos.load.ATE;

import java.util.List;

/**
 * Сервис для AddressDAO
 */
public class AddressService {

    private AddressDAO dao = new AddressDAO();

    /**
     * Находит список ATE для других типов стоимостей
     * @param parentateId ID родительского ATE
     * @return Список ATE
     */
    public List<ATE> findAteForOtherCostType(Long parentateId) {return dao.findAteForOtherCostType(parentateId);
    }
}
