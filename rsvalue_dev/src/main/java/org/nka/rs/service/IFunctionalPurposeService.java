package org.nka.rs.service;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface IFunctionalPurposeService<FunctionalPurposeDic> extends IBaseService {

    List findByParentCode(Integer id);
    List<FunctionalPurposeDic> getFuncCodeForGarderCommunity();
    List<FunctionalPurposeDic> getFuncCodeForFarmValue();
    List<FunctionalPurposeDic> getFuncCodeForMinskRegionGardenCommunity();
    List<FunctionalPurposeDic> getFuncCodeForParcel();
    List<FunctionalPurposeDic> getFuncCode();
    FunctionalPurposeDic findByCodeName(String name);
}
