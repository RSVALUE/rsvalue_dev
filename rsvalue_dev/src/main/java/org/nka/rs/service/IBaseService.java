package org.nka.rs.service;

import org.hibernate.criterion.DetachedCriteria;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface IBaseService<E> {

    Serializable add(E e);

    void update(E e);
    void updateById(Class<E> clazz, Serializable id);

    void refresh(E el);
    void refreshById(Class<E> clazz, Serializable id);

    void delete(E e);
    void deleteById(Class<E> clazz, Serializable id);

    List<E> getAll(Class<E> clazz);
    E getElementById(Class<E> clazz, Serializable id);

    List<E> getCriterion(DetachedCriteria crio);
    E getUniqueResult(DetachedCriteria crio);


}
