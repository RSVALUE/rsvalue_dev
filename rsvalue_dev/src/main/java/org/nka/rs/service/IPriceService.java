package org.nka.rs.service;

import org.nka.rs.entity.dictionary.OperationTypeDic;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.dto.price.PriceData;
import org.nka.rs.entity.dto.price.SimplePrice;
import org.nka.rs.entity.pojos.price.GetPriceObj;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;

import java.util.List;

/**
 * Created by zgurskaya on 10.10.2016.
 */
public interface IPriceService  {

    List<TORDic> getTorList (Long userId);
    Object [] getPriceListWithSorting(String priceNum, Integer sortingParam, String predicate, Integer page, Integer amount, Long userId, Integer torId);
    ResponseWithData readCreateEditPrice(Long userId, Integer torId, Long priceId);
    GetPriceObj readCreateEditPrice(Long userId, Long priceId);
    List<OperationTypeDic> getOperType();
    Response stopPrice(Long priceId, String date, Long userId, Integer operTypeId);
    Response savePrice(PriceData price);

    Response savePrice2(SimplePrice price);

    Response validatePriceNumber(Integer torId, String priceNumber);
}
