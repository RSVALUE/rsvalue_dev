package org.nka.rs.service;

import org.nka.rs.entity.dictionary.CurrencyDic;

/**
 * Created by zgurskaya on 27.07.2016.
 */
public interface ICurrencyService extends IBaseService {

    CurrencyDic findByCodeName(String name);
    CurrencyDic findByShortName(String shortName);

}
