package org.nka.rs.service.impl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nka.rs.dao.IAddressDao;
import org.nka.rs.dao.ILoadDataDao;
import org.nka.rs.dao.IRegistrDTODao;
import org.nka.rs.dao.impl.AddressDaoImpl;
import org.nka.rs.dao.impl.LoadDataDao;
import org.nka.rs.dao.impl.RegistrDTODaoImpl;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.constant.loadConstant.*;
import org.nka.rs.entity.dto.Bound;
import org.nka.rs.entity.dto.LoadData;
import org.nka.rs.entity.dto.Zone;
import org.nka.rs.entity.responseVar.Report;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.ILoadDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.*;

import static org.json.JSONObject.getNames;

/**
 * Created by zgurskaya on 10.06.2016.
 */

@Service
@Transactional
public class LoadDataServiceImpl implements ILoadDataService, IConstant {

    private static final String ER1_PRE = "Отсутствует столбец: ";
    private static final String ER1_POST = ". Загрузка невозможна!";
    private static final String ER2_PRE = "Значение поля ";
    private static final String ER2_POST = " должно быть больше 0, поле не может быть пустым, либо заполненным символом «пробел»!";
    private static final String ER3 = "Внимание! Выявлены повторяющиеся номера оценочных зон. Загрузка невозможна!";
    private static final String ER4 = "Внимание! Не соответствуют регистрационные номера населенного пункта оценочным зонам! Количество не валидных записей ";
    private static final String ER5_PRE = "Значение поля ";
    private static final String ER5_POST = " не может быть пустым либо заполненным символом «пробел»!";
    private static final String ER6_PRE = "Внимание! Отсутствует столбец: ";
    private static final String ER6_POST = ". Продолжить?";
    private static final String ER7 = "Поле не соответствует форматно-логическому контролю. Загрузка невозможна";
    private static final String ER8 = "Выявлены несоответствия загружаемых объектов с действующим реестром АТЕ. Продолжить операцию?";
    private static final String ER9 = "Загружаемый архив не соответствует выбранному объекту оценки! Выберите другой файл";
    private static final String ER10 = "Внимание! Неверный числовой формат данных! Проверьте правильность указания формата для целочисленных значений!";


    @Autowired
    ILoadDataDao loadDataDao = new LoadDataDao();
    @Autowired
    IAddressDao addressDao = new AddressDaoImpl();
    @Autowired
    IRegistrDTODao registrDTODao = new RegistrDTODaoImpl();

    @Override
    public Report loadData(LoadData data) throws SQLException {
        if (data.getCostTypeId().equals(COMMUNITY_GARDEN)) {
            data.setPolyTypeId(POINT);
        } else if (data.getCostTypeId().equals(CITY_AND_PGT) || data.getCostTypeId().equals(LANDS_OUT_LOCALITY)) {
            data.setPolyTypeId(POLYGON);
        }
        Report report = new Report();
        ResponseWithData response = loadDataDao.loadData(data);
        if (response.getCode() == ErrorCodeEnum.SUCCESS.getValue()) {
            report = (Report) response.getData();
        } else if (response.getCode() == ErrorCodeEnum.UNSUCCESS.getValue()) {
            if (response.getData() != null) {
                clearErrorLoad((Long) response.getData());
                return new Report();
            }
        }
        return report;
    }

    @Override
    public Response validateZonesFields(String[] fields, Integer costTypeId) {
        Response res = new Response();

        if (costTypeId.equals(CITY_AND_PGT)) {
            res = validateCityZonesFields(fields);
        } else if (costTypeId.equals(RURAL_LOCALITY)) {
            res = validateRuralZonesFields(fields);
        } else if (costTypeId.equals(COMMUNITY_GARDEN)) {
            res = validateCommunityZonesFields(fields);
        } else if (costTypeId.equals(LANDS_OUT_LOCALITY)) {
            res = validateLandsZonesFields(fields);
        } else if (costTypeId.equals(FARM_LANDS)) {
            res = validateFarmLandsZonesFields(fields);
        } else {
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Неизвестный вид оценки");
        }
        return res;
    }

    public Response validateCityZonesFields(String[] fields) {
        Boolean isFieldExist = false;
        List<String> fieldList = Arrays.asList(fields);
        Response res = new Response();
        res.setMessage("");
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());

        LOOP_OUT:
        //встречается ли обязательное поле среди списка названий колонок
        for (CityFieldsEnum city : CityFieldsEnum.values()) {
            LOOP_IN:
            for (String field : fieldList) {
                String dbfColumnName = field.toLowerCase().trim();
                String needColumnName = city.getColumnName().toLowerCase().trim();
                //т.к. для данных колонок проставляется постфикс - по неполному совпадению
                if (city == CityFieldsEnum.COST_D || city == CityFieldsEnum.COST_R) {
                    if (dbfColumnName.matches(needColumnName + "(.*)")) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                } else {
                    if (city.getColumnName().equalsIgnoreCase(field)) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                }
            }
            //если не встретилось - вернуть сообщение
            if (isFieldExist == false) {
                res.setMessage(ER1_PRE + city.getColumnName() + ER1_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP_OUT;
            } else {
                isFieldExist = false;
            }
        }
        return res;
    }

    public Response validateFarmLandsZonesFields(String[] fields) {
        Boolean isFieldExist = false;
        List<String> fieldList = Arrays.asList(fields);
        Response res = new Response();
        res.setMessage("");
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());

        LOOP_OUT:
        //встречается ли обязательное поле среди списка названий колонок
        for (FarmLandsFieldsEnum farm : FarmLandsFieldsEnum.values()) {
            LOOP_IN:
            for (String field : fieldList) {
                String dbfColumnName = field.toLowerCase().trim();
                String needColumnName = farm.getColumnName().toLowerCase().trim();
                //т.к. для данных колонок проставляется постфикс - по неполному совпадению
                if (farm == FarmLandsFieldsEnum.COST_D || farm == FarmLandsFieldsEnum.COST_R) {
                    if (dbfColumnName.matches(needColumnName + "(.*)")) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                } else {
                    if (farm.getColumnName().equalsIgnoreCase(field)) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                }
            }
            //если не встретилось - вернуть сообщение
            if (isFieldExist == false) {
                res.setMessage(ER1_PRE + farm.getColumnName() + ER1_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP_OUT;
            } else {
                isFieldExist = false;
            }
        }
        return res;
    }

    public Response validateRuralZonesFields(String[] fields) {
        Boolean isFieldExist = false;
        List<String> fieldList = Arrays.asList(fields);
        Response res = new Response();
        res.setMessage("");
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());

        LOOP_OUT:
        //встречается ли обязательное поле среди списка названий колонок
        for (RuralLocalityFieldsEnum rural : RuralLocalityFieldsEnum.values()) {
            LOOP_IN:
            for (String field : fieldList) {
                String dbfColumnName = field.toLowerCase().trim();
                String needColumnName = rural.getColumnName().toLowerCase().trim();
                //т.к. для данных колонок проставляется постфикс - по неполному совпадению
                if (rural == RuralLocalityFieldsEnum.COST_D || rural == RuralLocalityFieldsEnum.COST_R) {
                    if (dbfColumnName.matches(needColumnName + "(.*)")) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                } else {
                    if (rural.getColumnName().equalsIgnoreCase(field)) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                }
            }
            //если не встретилось - вернуть сообщение
            if (isFieldExist == false) {
                res.setMessage(ER1_PRE + rural.getColumnName() + ER1_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP_OUT;
            } else {
                isFieldExist = false;
            }
        }
        return res;
    }

    public Response validateCommunityZonesFields(String[] fields) {
        Boolean isFieldExist = false;
        List<String> fieldList = Arrays.asList(fields);
        Response res = new Response();
        res.setMessage("");
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());

        LOOP_OUT:
        //встречается ли обязательное поле среди списка названий колонок
        for (CommunityGardenFieldsEnum community : CommunityGardenFieldsEnum.values()) {
            LOOP_IN:
            for (String field : fieldList) {
                String dbfColumnName = field.toLowerCase().trim();
                String needColumnName = community.getColumnName().toLowerCase().trim();
                //т.к. для данных колонок проставляется постфикс - по неполному совпадению
                if (community == CommunityGardenFieldsEnum.COST_D || community == CommunityGardenFieldsEnum.COST_R) {
                    if (dbfColumnName.matches(needColumnName + "(.*)")) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                } else {
                    if (community.getColumnName().equalsIgnoreCase(field)) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                }
            }
            //если не встретилось - вернуть сообщение
            if (isFieldExist == false) {
                if (community == CommunityGardenFieldsEnum.UNP) {
                    res.setMessage(ER6_PRE + community.getColumnName() + ER6_POST);
                    res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                    isFieldExist = false;
                } else {
                    res.setMessage(ER1_PRE + community.getColumnName() + ER1_POST);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    break LOOP_OUT;
                }
            } else {
                isFieldExist = false;
            }
        }
        return res;
    }

    public Response validateLandsZonesFields(String[] fields) {
        Boolean isFieldExist = false;
        List<String> fieldList = Arrays.asList(fields);
        Response res = new Response();
        res.setMessage("");
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());

        LOOP_OUT:
        //встречается ли обязательное поле среди списка названий колонок
        for (LandsOutLocalityFieldsEnum land : LandsOutLocalityFieldsEnum.values()) {
            LOOP_IN:
            for (String field : fieldList) {
                String dbfColumnName = field.toLowerCase().trim();
                String needColumnName = land.getColumnName().toLowerCase().trim();
                //т.к. для данных колонок проставляется постфикс - по неполному совпадению
                if (land == LandsOutLocalityFieldsEnum.COST_D || land == LandsOutLocalityFieldsEnum.COST_R) {
                    if (dbfColumnName.matches(needColumnName + "(.*)")) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                } else {
                    if (land.getColumnName().equalsIgnoreCase(field)) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                }
            }
            //если не встретилось - вернуть сообщение
            if (isFieldExist == false) {
                res.setMessage(ER1_PRE + land.getColumnName() + ER1_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP_OUT;
            } else {
                isFieldExist = false;
            }
        }
        return res;
    }

    @Override
    public Response validateZonesValues(Zone[] zones, Integer costTypeId, Long objectnumber) {
        Response res = new Response();

        if (zones == null) {
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage(ER10);
        } else {
            if (costTypeId.equals(CITY_AND_PGT)) {
                res = validateCityZonesValues(zones, objectnumber);
            } else if (costTypeId.equals(RURAL_LOCALITY)) {
                res = validateRuralZonesValues(zones, objectnumber);
            } else if (costTypeId.equals(COMMUNITY_GARDEN)) {
                res = validateCommunityZonesValues(zones, objectnumber);
            } else if (costTypeId.equals(LANDS_OUT_LOCALITY)) {
                res = validateLandsZonesValues(zones, objectnumber);
            } else if (costTypeId.equals(FARM_LANDS)) {
                res = validateFarmLandsZonesValues(zones, objectnumber);
            } else {
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                res.setMessage("Неизвестный вид оценки");
            }
        }
        return res;
    }

    public Response validateCityZonesValues(Zone[] zones, Long objectnumber) {
        Response res = new Response();
        List<String> zonenumberList = new ArrayList<String>();
        int count = 0;

        LOOP:
        for (int i = 0; i < zones.length; i++) {
            Zone z = zones[i];
            if (z.getNum() == null || z.getNum().equals(0) || z.getNum().trim().equals("")) {
                res.setMessage(ER2_PRE + " Num " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                zonenumberList.add(z.getNum());
            }
            if (z.getArea() == null || z.getArea().equals(0)) {
                res.setMessage(ER2_PRE + " Area " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_d() == null || z.getCost_d().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_d " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_r() == null || z.getCost_r().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_r " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getObjectnumb() == null || z.getObjectnumb().equals(0)) {
                res.setMessage(ER2_PRE + " Objectnumb " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                if (!z.getObjectnumb().equals(objectnumber)) {
                    res.setMessage(ER9);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                }
                Long obj = z.getObjectnumb();
                int last = 7;
                int first = last - obj.toString().length();
                String num = z.getNum();
                String subNum = num.substring(first, last);
                if (!subNum.equals(obj.toString())) {
                    count++;
                }
            }
            if (z.getName() == null || z.getName().trim().equals("")) {
                res.setMessage(ER5_PRE + " Name " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
        }

        if (res.getCode() == 0) {
            Set<String> zonenumberSet = new HashSet<String>(zonenumberList);
            if (zonenumberSet.size() < zonenumberList.size()) {
                res.setMessage(ER3);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else if (count != 0) {
                res.setMessage(ER4 + count);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else {
                res.setMessage("");
                res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                return res;
            }
        } else {
            return res;
        }
    }

    public Response validateFarmLandsZonesValues(Zone[] zones, Long objectnumber) {
        Response res = new Response();
        List<String> zonenumberList = new ArrayList<String>();

        LOOP:
        for (int i = 0; i < zones.length; i++) {
            Zone z = zones[i];
            if (z.getNum() == null || z.getNum().equals(0) || z.getNum().trim().equals("")) {
                res.setMessage(ER2_PRE + " Num " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                zonenumberList.add(z.getNum());
            }
            if (z.getUnp() != null) {
                String unpStr = z.getUnp().toString();
                if (unpStr.length() != 9) {
                    res.setMessage(ER7);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    break LOOP;
                }
            }
            if (z.getCost_d() == null || z.getCost_d().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_d " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_r() == null || z.getCost_r().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_r " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getObjectnumb() == null || z.getObjectnumb().equals(0)) {
                res.setMessage(ER2_PRE + " Objectnumb " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                if (!z.getObjectnumb().equals(objectnumber)) {
                    res.setMessage(ER9);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                }
            }
            if (z.getName() == null || z.getName().trim().equals("")) {
                res.setMessage(ER5_PRE + " Name " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
        }

        if (res.getCode() == 0) {
            Set<String> zonenumberSet = new HashSet<String>(zonenumberList);
            if (zonenumberSet.size() < zonenumberList.size()) {
                res.setMessage(ER3);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else {
                res.setMessage("");
                res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                return res;
            }
        } else {
            return res;
        }
    }

    public Response validateRuralZonesValues(Zone[] zones, Long objectnumber) {
        Response res = new Response();
        int count = 0;

        List<String> zonenumberList = new ArrayList<String>();
        Set<Long> dbfSNP = new HashSet<Long>();

        LOOP:
        for (int i = 0; i < zones.length; i++) {
            Zone z = zones[i];
            if (z.getNum() == null || z.getNum().equals(0) || z.getNum().equals("")) {
                res.setMessage(ER2_PRE + " Num " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                zonenumberList.add(z.getNum());
            }
            if (z.getObjectnumb() == null || z.getObjectnumb().equals(0)) {
                res.setMessage(ER2_PRE + " Objectnumb " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                Long obj = z.getObjectnumb();
                int last = 7;
                int first = last - obj.toString().length();
                String num = z.getNum();
                String subNum = num.substring(first, last);
                if (!subNum.equals(obj.toString())) {
                    count++;
                }
                dbfSNP.add(obj);
            }
            if (z.getSoato() == null || z.getSoato().equals("")) {
                res.setMessage(ER5_PRE + " SOATO " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getName() == null || z.getName().trim().equals("")) {
                res.setMessage(ER5_PRE + " Name " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCategory() == null || z.getCategory().trim().equals("")) {
                res.setMessage(ER5_PRE + " Category " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getDistr() == null || z.getDistr().trim().equals("")) {
                res.setMessage(ER5_PRE + " Distr " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getSs() == null || z.getSs().trim().equals("")) {
                res.setMessage(ER5_PRE + " Ss " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_d() == null || z.getCost_d().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_d " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_r() == null || z.getCost_r().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_r " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
        }

        if (res.getCode() == 0) {
            Set<String> zonenumberSet = new HashSet<String>(zonenumberList);
            if (zonenumberSet.size() < zonenumberList.size()) {
                res.setMessage(ER3);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else if (count != 0) {
                res.setMessage(ER4 + count);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else {
                List<Long> dbSNP = addressDao.findAteIdForSNP(objectnumber);
                List<Long> dbfCopy = new ArrayList<Long>(dbfSNP);
                List<Long> dbCopy = new ArrayList<Long>(dbSNP);

                //есть в базе - нет в дбф
                dbSNP.removeAll(dbfSNP);
                //есть в дбф - нет в базе
                dbfCopy.removeAll(dbCopy);

                if (dbSNP.size() != 0 || dbfCopy.size() != 0) {
                    res.setMessage(ER8);
                    res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                    return res;
                } else {
                    res.setMessage("");
                    res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    return res;
                }
            }
        } else {
            return res;
        }
    }

    public Response validateCommunityZonesValues(Zone[] zones, Long objectnumber) {
        Response res = new Response();
        List<String> zonenumberList = new ArrayList<String>();

        LOOP:
        for (int i = 0; i < zones.length; i++) {
            Zone z = zones[i];
            if (z.getNum() == null || z.getNum().equals(0) || z.getNum().trim().equals("")) {
                res.setMessage(ER2_PRE + " Num " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                zonenumberList.add(z.getNum());
            }
            if (z.getStname() == null || z.getStname().trim().equals("")) {
                res.setMessage(ER5_PRE + " Stname " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getUnp() != null) {
                String unpStr = z.getUnp().toString();
                if (unpStr.length() != 9) {
                    res.setMessage(ER7);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    break LOOP;
                }
            }
            if (z.getObjectnumb() != null) {
                if (!z.getObjectnumb().equals(objectnumber)) {
                    res.setMessage(ER9);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                }
            }
            if (z.getNpname() == null || z.getNpname().trim().equals("")) {
                res.setMessage(ER5_PRE + " Name " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_d() == null || z.getCost_d().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_d " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_r() == null || z.getCost_r().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_r " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
        }

        if (res.getCode() == 0) {
            Set<String> zonenumberSet = new HashSet<String>(zonenumberList);
            if (zonenumberSet.size() < zonenumberList.size()) {
                res.setMessage(ER3);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else {
                res.setMessage("");
                res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                return res;
            }
        } else {
            return res;
        }
    }

    public Response validateLandsZonesValues(Zone[] zones, Long objectnumber) {
        Response res = new Response();
        int count = 0;
        List<String> zonenumberList = new ArrayList<String>();
        Set<Long> dbfSS = new HashSet<Long>();

        LOOP:
        for (int i = 0; i < zones.length; i++) {
            Zone z = zones[i];
            if (z.getNum() == null || z.getNum().equals(0) || z.getNum().trim().equals("")) {
                res.setMessage(ER2_PRE + " Num " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                zonenumberList.add(z.getNum());
            }
            if (z.getDistr() == null || z.getDistr().trim().equals("")) {
                res.setMessage(ER5_PRE + " Distr " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getObjectnumb() == null || z.getObjectnumb().equals(0)) {
                res.setMessage(ER2_PRE + " Objectnumb " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                if (!z.getObjectnumb().equals(objectnumber)) {
                    res.setMessage(ER9);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                }
            }
            if (z.getRegn_ss() == null || z.getRegn_ss().equals(0)) {
                res.setMessage(ER2_PRE + " Regn_ss " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                Long regSs = z.getRegn_ss();
                int last = 7;
                int first = last - regSs.toString().length();
                String num = z.getNum();
                String subNum = num.substring(first, last);
                if (!subNum.equals(regSs.toString())) {
                    count++;
                }
                dbfSS.add(z.getRegn_ss());
            }
            if (z.getCost_d() == null || z.getCost_d().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_d " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (z.getCost_r() == null || z.getCost_r().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_r " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
        }

        if (res.getCode() == 0) {
            Set<String> zonenumberSet = new HashSet<String>(zonenumberList);
            if (zonenumberSet.size() < zonenumberList.size()) {
                res.setMessage(ER3);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            }
            if (count != 0) {
                res.setMessage(ER4 + count);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else {
                //получить список всех сельсоветов для выбранного района
                List<Long> dbSS = addressDao.findAteIdForParcelOutNP(objectnumber);
                //сравнить списки сельсоветов из шейп-файла и из базы
                List<Long> dbfCopy = new ArrayList<Long>(dbfSS);
                List<Long> dbCopy = new ArrayList<Long>(dbSS);

                //есть в базе - нет в дбф
                dbSS.removeAll(dbfSS);
                //есть в дбф - нет в базе
                dbfCopy.removeAll(dbCopy);

                if (dbSS.size() != 0 || dbfCopy.size() != 0) {
                    res.setMessage(ER8);
                    res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                    return res;
                } else {
                    res.setMessage("");
                    res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    return res;
                }
            }
        } else {
            return res;
        }
    }

    public Response validateBoundsFields(String[] fields) {
        Boolean isFieldExist = false;
        List<String> fieldList = Arrays.asList(fields);
        Response res = new Response();

        int count = 0;
        LOOP_OUT:
        //встречается ли обязательное поле среди списка названий колонок
        for (BoundFieldsEnum bound : BoundFieldsEnum.values()) {
            LOOP_IN:
            for (String field : fieldList) {
                String dbfColumnName = field.toLowerCase().trim();
                String needColumnName = bound.getColumnName().toLowerCase().trim();
                //т.к. для данных колонок проставляется постфикс - по неполному совпадению
                if (bound == BoundFieldsEnum.COST_D || bound == BoundFieldsEnum.COST_R) {
                    if (dbfColumnName.matches(needColumnName + "(.*)")) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                } else {
                    if (bound.getColumnName().equalsIgnoreCase(field)) {
                        isFieldExist = true;
                        break LOOP_IN;
                    }
                }
                if (bound.getColumnName().equalsIgnoreCase(field)) {
                    isFieldExist = true;
                    break LOOP_IN;
                }
            }
            //если не встретилось - вернуть сообщение
            if (isFieldExist == false) {
                if (bound.getColumnName().equals(BoundFieldsEnum.ADDRESS.getColumnName()) || bound.getColumnName().equals(BoundFieldsEnum.SQUARE.getColumnName())) {
                    res.setMessage(ER6_PRE + bound.getColumnName() + ER6_POST);
                    res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                } else if (bound.getColumnName().equals(BoundFieldsEnum.PUPRPOSE.getColumnName()) || bound.getColumnName().equals(BoundFieldsEnum.PURPOSE_GOV.getColumnName())) {
                    count++;
                } else {
                    res.setMessage(ER1_PRE + bound.getColumnName() + ER1_POST);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    break LOOP_OUT;
                }
            } else {
                isFieldExist = false;
            }
        }

        if (res.getCode() == 0) {
            if (count == 2) {
                res.setMessage(ER1_PRE + BoundFieldsEnum.PUPRPOSE.getColumnName() + " и " + BoundFieldsEnum.PURPOSE_GOV.getColumnName() + ER1_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                return res;
            } else {
                if (count == 2) {
                    res.setMessage(ER1_PRE + BoundFieldsEnum.PUPRPOSE.getColumnName() + " и " + BoundFieldsEnum.PURPOSE_GOV.getColumnName() + ER1_POST);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    return res;
                } else {
                    res.setMessage("");
                    res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    return res;
                }
            }
        } else {
            return res;
        }
    }

    public Response validateBoundsValues(Bound[] bounds) {
        Response res = new Response();
        res.setMessage("");
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());

        LOOP:
        for (int i = 0; i < bounds.length; i++) {
            Bound b = bounds[i];

            if (b.getNum() == null || b.getNum().equals(0) || b.getNum().trim().equals("")) {
                res.setMessage(ER2_PRE + " Num " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (b.getCadnum() == null || b.getCadnum().trim().equals("")) {
                res.setMessage(ER5_PRE + " Cadnum " + ER5_POST + b.getNum());
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            } else {
                if (b.getCadnum().length() != 18) {
                    res.setMessage(ER7);
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    break LOOP;
                }
            }
            if (b.getSoato() == null || b.getSoato().equals(0)) {
                res.setMessage(ER2_PRE + " Soato " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (b.getBlock_numb() == null || b.getBlock_numb().equals(0) || b.getBlock_numb().equals("")) {
                res.setMessage(ER5_PRE + " Block_numb " + ER5_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (b.getAddress() == null || b.getAddress().equals("")) {
                res.setMessage(ER6_PRE + " Address " + ER6_POST);
                res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
            }
            if (b.getSq() == null || b.getSq().equals(0) || b.getSq().equals("")) {
                res.setMessage(ER6_PRE + " Sq " + ER6_POST);
                res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
            }
            if ((b.getPurpose() == null || b.getPurpose().equals("")) && (b.getPurpose2() == null || b.getPurpose2().equals(""))) {
                res.setMessage(ER2_PRE + " Purpose и Purpose2 " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (b.getCost_d() == null || b.getCost_d().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_d " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
            if (b.getCost_r() == null || b.getCost_r().equals(0)) {
                res.setMessage(ER2_PRE + " Cost_r " + ER2_POST);
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                break LOOP;
            }
        }
        return res;
    }

    @Override
    public Object createReport(Long[] ateList, Long objectnumber, Integer costTypeId) {

        Object[] obj = new Object[2];
        List<Long> dbfList = Arrays.asList(ateList);
        List<Long> dbfCopy = new ArrayList<Long>(dbfList);
        List<Long> dbList = new ArrayList<Long>();
        if (costTypeId.equals(RURAL_LOCALITY)) {
            //получить список всех сельских населнных пунктов для выбранного района
            dbList = addressDao.findAteIdForSNP(objectnumber);
        } else if (costTypeId.equals(LANDS_OUT_LOCALITY)) {
            dbList = addressDao.findAteIdForParcelOutNP(objectnumber);
        }

        List<Long> dbCopy = new ArrayList<Long>(dbList);
        //есть в базе - нет в дбф
        dbList.removeAll(dbfList);
        //есть в дбф - нет в базе
        dbfCopy.removeAll(dbCopy);

        obj[0] = dbList;
        obj[1] = dbfCopy;
        return obj;
    }

    @Override
    public String saveZonesGeometry(JSONObject geometry, Long dwd_id) {
        return loadDataDao.saveZonesGeometry(geometry, dwd_id);
    }

    @Override
    public String saveBoundsGeometry(JSONObject geometry, Long dwd_id) {
        return loadDataDao.saveBoundsGeometry(geometry, dwd_id);
    }

    @Override
    public Response saveLayerGeometry(JSONObject geometry, Long dwd_id, String layerName) {
        Response response = getValidateValue(geometry, layerName);
        if (response.getCode() == ErrorCodeEnum.SUCCESS.getValue()) {
            return loadDataDao.saveLayerGeometry(geometry, dwd_id, layerName);
        } else {
            return response;
        }
    }

    public Response getValidateValue(JSONObject geojson, String layerName) {
        Response response = new Response();
        response.setCode(ErrorCodeEnum.SUCCESS.getValue());
        response.setMessage("");

        JSONArray features = null;
        JSONObject properties = null;
        if (geojson.has("features")) {
            features = geojson.getJSONArray("features");
            for (int i = 0; i < features.length(); i++) {
                JSONObject obj = features.getJSONObject(i);
                if (obj.has("properties")) {
                    properties = obj.getJSONObject("properties");
                    if (properties != null) {
                        if (properties.has("CODE")) {
                            try {
                                Integer code = properties.getInt("CODE");
                                if (code == null) {
                                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                    response.setMessage("В слое " + layerName + " имеется поле CODE со значением null!!! Загрузка невозможна!");
                                }
                            } catch (JSONException e) {
                                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                response.setMessage("В слое " + layerName + " имеется поле CODE с текстовым значением!!! Загрузка невозможна!");
                                e.printStackTrace();
                            }

                        }
                    }
                }
            }
        }

        return response;
    }

    @Override
    public List encodingZones(JSONObject geojson) throws UnsupportedEncodingException {
        String jsonStr = geojson.toString();
        byte[] jsonB = jsonStr.getBytes("windows-1252");
        String jsonEncode = new String(jsonB, "windows-1251");

        JSONObject object = new JSONObject(jsonEncode);
        JSONArray records = object.getJSONArray("records");

        HashMap<String, String> map = new HashMap();
        List jsonList = new ArrayList();

        for (Object row : records) {
            JSONObject object1 = (JSONObject) row;
            String[] names = getNames(object1);
            for (CityFieldsEnum city : CityFieldsEnum.values()) {
                for (String n : names) {
                    String dbfColumnName = n.toLowerCase().trim();
                    String needColumnName = city.getColumnName().toLowerCase().trim();

                    if (city == CityFieldsEnum.COST_D || city == CityFieldsEnum.COST_R) {
                        if (dbfColumnName.matches(needColumnName + "(.*)")) {
                            String val = (String) object1.get(n);
                            map.put(n, val);
                        }
                    } else {
                        if (city.getColumnName().equalsIgnoreCase(dbfColumnName)) {
                            String val = (String) object1.get(n);
                            map.put(n, val);
                        }
                    }
                }
            }
            jsonList.add(map);
            map = new HashMap<String, String>();
        }
        return jsonList;
    }

    @Override
    public List encodingBounds(JSONObject geojson) throws UnsupportedEncodingException {
        String jsonStr = geojson.toString();
        byte[] jsonB = jsonStr.getBytes("windows-1252");
        String jsonEncode = new String(jsonB, "windows-1251");

        JSONObject object = new JSONObject(jsonEncode);
        JSONArray records = object.getJSONArray("records");

        HashMap<String, String> map = new HashMap();
        List jsonList = new ArrayList();

        for (Object row : records) {
            JSONObject object1 = (JSONObject) row;
            String[] names = getNames(object1);
            for (BoundFieldsEnum bound : BoundFieldsEnum.values()) {
                for (String n : names) {
                    String dbfColumnName = n.toLowerCase().trim();
                    String needColumnName = bound.getColumnName().toLowerCase().trim();

                    if (bound == BoundFieldsEnum.COST_D || bound == BoundFieldsEnum.COST_R) {
                        if (dbfColumnName.matches(needColumnName + "(.*)")) {
                            String val = (String) object1.get(n);
                            map.put(n, val);
                        }
                    } else {
                        if (bound.getColumnName().equalsIgnoreCase(dbfColumnName)) {
                            String val = (String) object1.get(n);
                            map.put(n, val);
                        }
                    }
                }
            }
            jsonList.add(map);
            map = new HashMap<String, String>();
        }
        return jsonList;
    }

    @Override
    public List getNationalCurrency() {
        return loadDataDao.getNationalCurrency();
    }

    @Override
    public List getForeignCurrency() {
        return loadDataDao.getForeignCurrency();
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void clearErrorLoad(Long regNum) {
        Clob pathes = loadDataDao.clearErrorLoad(regNum);
        StringBuilder sb = new StringBuilder();
        try {
            Reader reader = pathes.getCharacterStream();
            BufferedReader br = new BufferedReader(reader);
            String line;
            while (null != (line = br.readLine())) {
                sb.append(line);
            }
            br.close();
            if (sb != null && !sb.equals("")) {
                String[] pathParts = sb.toString().split(";");
                for (String path : pathParts) {
                    if (path != null && !path.equals("")) {
                        File serverFile = new File(path);
                        if (serverFile.exists()) {
                            serverFile.delete();
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
