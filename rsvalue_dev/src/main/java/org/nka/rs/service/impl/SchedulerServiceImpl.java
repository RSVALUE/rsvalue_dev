package org.nka.rs.service.impl;

import org.json.JSONObject;
import org.nka.rs.dao.ISchedulerDao;
import org.nka.rs.dao.impl.SchedulerDaoImpl;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.service.ISchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.net.URL;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zgurskaya on 01.08.2016.
 */

@Service
@Transactional
public class SchedulerServiceImpl implements ISchedulerService, IConstant {


    @Autowired
    ISchedulerDao schedulerDao = new SchedulerDaoImpl();

    @Override
    public void updateCurrency() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-d");
        String currentDate = formatter.format(date);
        //Double euroRate = getEuroRate(currentDate);
        Double usdRate = getUsdRate(currentDate);
        /*if (euroRate != null && !euroRate.equals(0)) {
            schedulerDao.updateCurrency(EURO_ID, euroRate);
        }*/
        if (usdRate != null && !usdRate.equals(0)) {
            schedulerDao.updateCurrency(USD_ID, usdRate);
        }
    }

    public Double getEuroRate(String currentDate) {
        BufferedReader dis = null;
        String full = new String();
        Double euroRate = null;
        try {
            String euroPath = NBRB_URL + EURO_ID + "?onDate=" + currentDate;
            URL euro = new URL(euroPath);
            dis = new BufferedReader(new InputStreamReader(euro.openStream()));
            String line = dis.readLine();
            while (line != null) {
                full += line + "\n";
                line = dis.readLine();
            }
            if (full != null && !full.equals("")) {
                JSONObject euroString = new JSONObject(full);
                euroRate = (Double) euroString.get("Cur_OfficialRate");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return euroRate;
    }


    public Double getUsdRate(String currentDate) {
        BufferedReader dis = null;
        String full = new String();
        Double usdRate = null;
        try {
            String usdPath = NBRB_URL + USD_ID + "?onDate=" + currentDate;
            URL usd = new URL(usdPath);
            dis = new BufferedReader(new InputStreamReader(usd.openStream()));
            String line = dis.readLine();
            while (line != null) {
                full += line + "\n";
                line = dis.readLine();
            }
            if (full != null && !full.equals("")) {
                JSONObject usdString = new JSONObject(full);
                usdRate = (Double) usdString.get("Cur_OfficialRate");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return usdRate;
    }


    //ночная очистка данныех - если по какой-либо причине загрузка полностью не прошла
    public void nightClearData() {
        Clob pathes = schedulerDao.nightClearData();
        StringBuilder sb = new StringBuilder();
        if (pathes != null) {
            try {
                Reader reader = pathes.getCharacterStream();
                BufferedReader br = new BufferedReader(reader);
                String line;
                while (null != (line = br.readLine())) {
                    sb.append(line);
                }
                br.close();
                if (sb != null && !sb.equals("")) {
                    String[] pathParts = sb.toString().split(";");
                    for (String path : pathParts) {
                        if (path != null && !path.equals("")) {
                            File serverFile = new File(path);
                            if (serverFile.exists()) {
                                serverFile.delete();
                            }
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void nightClearPdfFolder() {
        String path = directory + File.separator + "PDF_DOCUMENTS";
        for (File myFile : new File(path).listFiles()) {
            if (myFile.isFile()) {
                myFile.delete();
            }
        }

    }
}
