package org.nka.rs.service.impl;

import org.nka.rs.dao.IDocumentLoadDao;
import org.nka.rs.dao.impl.DocumentLoadDao;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dto.DocInfo;
import org.nka.rs.entity.responseVar.SaveDocResponse;
import org.nka.rs.service.IDocumentLoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by zgurskaya on 17.06.2016.
 */

@Service
@Transactional
public class DocumentLoadServiceImpl extends BaseServiceImpl implements IDocumentLoadService, IConstant {

    @Autowired
    IDocumentLoadDao documentLoadDao = new DocumentLoadDao();

    @Override
    public String deleteDoc(Long docId) {
        return documentLoadDao.deleteDoc(docId);
    }

    @Override
    public SaveDocResponse saveFile(DocInfo docInfo, MultipartFile file, Long userId, Long commonDocId) {
        if (userId == null){
            userId = 2L;
        }
        SaveDocResponse res = new SaveDocResponse();
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());
        res.setMessage("");

        //если документ - архив, высчитать размер исходного файла (папки)
        String name = file.getOriginalFilename();
        String[] part = name.split("\\.");
        int length = part.length;
        String extension = part[length - 1];
        if (extension.equals("zip")) {
            res = getValideZipLenght(file);
            if (res.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                return res;
            }
        }

        //путь к директории
        String path = getPath(docInfo);
        //путь к файлу
        String filePath = path + File.separator + file.getOriginalFilename();

        File outputFile = new File(filePath);

        int count = 1;
        String[] namePart = new String[2];
        Boolean isExist = true;

        while (isExist) {
            if (outputFile.exists()) {
                namePart = file.getOriginalFilename().split("\\.");
                filePath = path + File.separator + namePart[0] + "(" + count + ")." + namePart[1];
                outputFile = new File(filePath);
                count++;
            } else {
                isExist = false;
            }
        }

        InputStream reader = null;
        FileOutputStream writer = null;
        int totalBytes = 0;

        try {
            byte[] buffer = file.getBytes();
            outputFile.createNewFile();
            reader = file.getInputStream();
            writer = new FileOutputStream(outputFile);
            int bytesRead = 0;
            while ((bytesRead = reader.read(buffer)) != -1) {
                writer.write(buffer);
                totalBytes += bytesRead;
            }
            Long id = documentLoadDao.saveDoc(docInfo, filePath, userId, commonDocId);
            if (id != null && id > 0 && !id.toString().equals("")) {
                if (count != 1) {
                    res.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                    res.setMessage("Файл с таким именем уже был загружен в файловое хранилище. Файл сохранен под именем " + namePart[0] + "(" + count + ")." + namePart[1]);
                    res.setName(namePart[0] + "(" + count + ")." + namePart[1]);
                    res.setId(id.toString());
                    return res;
                } else {
                    res.setId(id.toString());
                    return res;
                }
            } else {
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                res.setMessage("Ошибка при сохранении объекта в базу");
                File serverFile = new File(filePath);
                if (serverFile.exists()) {
                    serverFile.delete();
                }
                return res;
            }
        } catch (IOException e) {
            e.printStackTrace();
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при записи объекта в файловое хранилище");
            return res;
        } catch (ClassCastException e) {
            e.printStackTrace();
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при записи объекта в файловое хранилище");
            return res;
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPath(DocInfo docInfo) {
        // Creating the directory to store file
        String path = directory + File.separator;
        if (docInfo.getCostTypeId().equals(CITY_AND_PGT)) {
            path += "CITY_AND_PGT" + File.separator;
        } else if (docInfo.getCostTypeId().equals(RURAL_LOCALITY)) {
            path += "RURAL_LOCALITY" + File.separator;
        } else if (docInfo.getCostTypeId().equals(COMMUNITY_GARDEN)) {
            path += "COMMUNITY_GARDEN" + File.separator;
        } else if (docInfo.getCostTypeId().equals(LANDS_OUT_LOCALITY)) {
            path += "LANDS_OUT_LOCALITY" + File.separator;
        } else if (docInfo.getCostTypeId().equals(FARM_LANDS)) {
            path += "FARM_LANDS" + File.separator;
        }

        if (docInfo.getFuncCode().equals(LIVING_APARTMENT_ZONE)) {
            path += "LIVING_APARTMENT_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(LIVING_MANOR_ZONE)) {
            path += "LIVING_MANOR_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(LIVING_MANOR_ZONE_FARMSTEAD_TYPE)) {
            path += "LIVING_MANOR_ZONE_FARMSTEAD_TYPE" + File.separator;
        } else if (docInfo.getFuncCode().equals(LIVING_MANOR_ZONE_COTTAGE_TYPE)) {
            path += "LIVING_MANOR_ZONE_COTTAGE_TYPE" + File.separator;
        } else if (docInfo.getFuncCode().equals(RECREATION_ZONE)) {
            path += "RECREATION_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(BUSINESS_ZONE)) {
            path += "BUSINESS_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(PRODUCTION_ZONE)) {
            path += "PRODUCTION_ZONE" + File.separator;
        } else if (docInfo.getFuncCode().equals(GARDENING_ASSOTIATIONS)) {
            path += "GARDENING_ASSOTIATIONS" + File.separator;
        } else if (docInfo.getFuncCode().equals(ARABLE_LAND)) {
            path += "ARABLE_LAND" + File.separator;
        } else if (docInfo.getFuncCode().equals(IMPROVED_MEADOW_LAND)) {
            path += "IMPROVED_MEADOW_LAND" + File.separator;
        } else if (docInfo.getFuncCode().equals(NATURAL_MEADOW_LAND)) {
            path += "NATURAL_MEADOW_LAND" + File.separator;
        }

        LocalDate date = LocalDate.now();
        path += date.getYear() + File.separator + docInfo.getAteId() + File.separator;

        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return path;
    }

    public SaveDocResponse getValideZipLenght(MultipartFile file) {
        SaveDocResponse res = new SaveDocResponse();
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());
        res.setMessage("");

        Long size = 0L;

        /*try {
            byte[] zip = file.getBytes();
            byte[] bytes = new byte[4];
            byte[] name = new byte[4];


            bytes[0] = zip[22];
            bytes[1] = zip[23];
            bytes[2] = zip[24];
            bytes[3] = zip[25];
            for (int i = 0; i < bytes.length; i++) {
                size += ((long) bytes[i] & 0xffL) << (8 * i);
            }

        } catch (IOException e) {
            e.printStackTrace();
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при получении размера распакованного файла");
            return  res;
        }
*/

        try {
            ZipInputStream zis = new ZipInputStream(file.getInputStream(), Charset.forName("Cp1251"));
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                size += entry.getSize();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка! Неверная структура zip-архива!");
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при получении размера распакованного файла");
            return res;
        }
        if (size >= 31457280) {
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Размер распакованного архива больше 30 Mb!!! Угроза безопасности!!! Архив будет удален!!!");
            return res;
        } else {
            return res;
        }
    }

    @Override
    public Long getCommonDocId() {
        return documentLoadDao.getCommonDocId();
    }

}
