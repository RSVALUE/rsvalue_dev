package org.nka.rs.service.impl;

import org.nka.rs.dao.IValOrgDao;
import org.nka.rs.dao.impl.ValOrgDaoImpl;
import org.nka.rs.entity.dictionary.ValueOrganizationDic;
import org.nka.rs.service.IValueOrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 02.06.2016.
 */

@Service
@Transactional
public class ValueOrgServiceImpl extends BaseServiceImpl implements IValueOrgService {

    @Autowired
    IValOrgDao valOrgDao = new ValOrgDaoImpl();

    @Override
    public List<ValueOrganizationDic> getActualOrganization() {
        return valOrgDao.getActualOrganization();
    }
}
