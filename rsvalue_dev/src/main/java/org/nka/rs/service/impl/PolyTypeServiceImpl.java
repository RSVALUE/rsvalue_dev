package org.nka.rs.service.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.PolyTypeDic;
import org.nka.rs.service.IPolyTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */

@Service
@Transactional
public class PolyTypeServiceImpl extends BaseServiceImpl implements IPolyTypeService<PolyTypeDic>, IConstant {


    private DetachedCriteria query_poly_type = DetachedCriteria.forClass(PolyTypeDic.class);

    @Override
    public List<PolyTypeDic> getPolyType() {
        List<PolyTypeDic> result = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_poly_type);

        query_.add(Restrictions.ne("analyticCode",RASTR));
        query_.addOrder(Order.asc("codeName"));
        result  = super.getCriterion(query_);

        return result ;
    }
}
