package org.nka.rs.service.impl;

import org.nka.rs.dao.ILoadDocTypeDao;
import org.nka.rs.dao.impl.LoadDocTypeDaoImpl;
import org.nka.rs.entity.dictionary.LoadDocumentTypeDic;
import org.nka.rs.service.ILoadDocTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */
@Service
@Transactional
public class LoadDocTypeServiceImpl extends BaseServiceImpl implements ILoadDocTypeService<LoadDocumentTypeDic> {

    @Autowired
    ILoadDocTypeDao docTypeDao = new LoadDocTypeDaoImpl();

    @Override
    public List<LoadDocumentTypeDic> getNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType) {
        return docTypeDao.getNeedDocTypes(costTypeId, methodTypeId, polyType);
    }
    @Override
    public List<LoadDocumentTypeDic> getNotNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType) {
        return docTypeDao.getNotNeedDocTypes(costTypeId, methodTypeId, polyType);
    }
}
