package org.nka.rs.service.orders;

import org.nka.rs.dao.JDBCDao.orders.OrdersDAO;
import org.nka.rs.entity.pojos.order.OrderData;
import org.nka.rs.entity.responseVar.ResponseWithData;

/**
 * Класс-сервис, отвечающий за вызов функций соответствующего
 * класса из слоя DAO
 */
public class OrdersService {

    private OrdersDAO ordersDAO = new OrdersDAO();

    /**
     * Метод, передающий управление на DAO-слой
     *
     * @param userId ID пользователя
     * @param orgId ID организации
     * @param urgency Срочность
     * @param decl Заказчик
     * @param isNb Налогооблажение
     * @param orderState Статус заказа
     * @param filterUserId ID пользователя, принявшего заказ
     * @param sort Поле сортировки
     * @param sortType Тип сортировки
     * @param page Страницы выборки
     * @param qty Количество записей на странице
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getOrders(Long userId, Integer orgId, Integer urgency, String decl, Integer isNb, Integer orderState, Long filterUserId, Integer sort, String sortType, Integer page, Integer qty) {
        return ordersDAO.getOrders(userId, orgId, urgency, decl, isNb, orderState, filterUserId, sort, sortType, page, qty);
    }

    /**
     * Метод, передающий управление на DAO-слой
     *
     * @param userId ID пользователя
     * @param orgName Наименование организации
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getUserList(Long userId, Long orgName) {
        return ordersDAO.getUserList(userId, orgName);
    }

    /**
     * Метод, передающий управление на DAO-слой
     *
     * @param userId ID пользователя
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getNewOrder(Long userId) {
        return ordersDAO.getNewOrder(userId);
    }

    /**
     * Метод, передающий управление на DAO-слой
     *
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAllStates() {
        return ordersDAO.getAllStates();
    }

    /**
     * Метод, передающий управление на DAO-слой
     *
     * @param order Объект с данными по новому заказу
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData addOrder(OrderData order) {
        return ordersDAO.addOrder(order);
    }

    /**
     * Метод, передающий управление на DAO-слой
     *
     * @param orderId ID заказа
     * @param state Новый статус
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData changeOrderState(Long orderId, Integer state, Long userId) {
        return ordersDAO.changeOrderState(orderId, state, userId);
    }

    /**
     * Метод, передающий управление на DAO-слой
     *
     * @param orderId ID заказа
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getEditableOrder(Long orderId) {
        return ordersDAO.getEditableOrder(orderId);
    }
}
