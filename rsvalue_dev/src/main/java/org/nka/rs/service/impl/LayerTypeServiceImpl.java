package org.nka.rs.service.impl;

import org.nka.rs.dao.ILayerTypeDao;
import org.nka.rs.dao.impl.LayerTypeDaoImpl;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.LayerTypeDic;
import org.nka.rs.entity.dto.AdditionalLayer;
import org.nka.rs.entity.dto.LayerType;
import org.nka.rs.service.ILayerTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 09.06.2016.
 */
@Service
@Transactional
public class LayerTypeServiceImpl extends BaseServiceImpl implements ILayerTypeService, IConstant {

    @Autowired
    ILayerTypeDao layerTypeDao = new LayerTypeDaoImpl();

    @Override
    public List<LayerType> getValideLayers(List<String> layerNames) {
        List<LayerTypeDic> fullLayers = layerTypeDao.getActualLayers();
        List<LayerType> dtoList = new ArrayList<LayerType>();

        for (String name : layerNames){
            for (LayerTypeDic layer : fullLayers) {

                String partName = name.toLowerCase();
                String fileName = layer.getFileName().trim().toLowerCase();
                if (partName.matches(fileName + "(.*)")){
                    LayerType newLayer = new LayerType();
                    newLayer.setLayerName(name);
                    newLayer.setLayerTypeName(layer.getShortName());
                    newLayer.setAnalyticCode(layer.getAnalyticCode());
                    dtoList.add(newLayer);
                }

            }
        }
        return dtoList;
    }

    @Override
    public ResponseWithData getValideLayerName(List<String> layerNames) {
        List<LayerTypeDic> fullLayers = layerTypeDao.getActualLayers();
        List<LayerType> dtoList = new ArrayList<LayerType>();

        ResponseWithData response = new ResponseWithData();
        if (fullLayers == null || fullLayers.size() == 0){
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении данных по тематическим слоям");
            return response;
        } else {
            for (String name : layerNames) {
                for (LayerTypeDic layer : fullLayers) {

                    String partName = name.toLowerCase();
                    String fileName = layer.getFileName().trim().toLowerCase();
                    if (partName.matches(fileName + "(.*)")) {
                        LayerType newLayer = new LayerType();
                        newLayer.setLayerName(name);
                        newLayer.setLayerTypeName(layer.getShortName());
                        newLayer.setAnalyticCode(layer.getAnalyticCode());
                        dtoList.add(newLayer);
                    }

                }
            }
            if (layerNames.size() != dtoList.size()){
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Часть или все наименования тематических слоев не имеют аналогов в классификаторе. Архив невалидный и будет удален");
            } else {
                response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                response.setData(dtoList);
            }
            return response;
        }
    }

    @Override
    public List<LayerType> getLayersData(List<String> layerNames) {
        List<LayerTypeDic> fullLayers = layerTypeDao.getActualLayers();
        List<LayerType> dtoList = new ArrayList<LayerType>();

        Boolean isValid = false;
        for (String name : layerNames){
            for (LayerTypeDic layer : fullLayers) {
                String partName = name.toLowerCase();
                String fileName = layer.getFileName().trim().toLowerCase();
                if (partName.matches(fileName + "(.*)")){
                    isValid = true;
                    LayerType newLayer = new LayerType();
                    newLayer.setLayerName(name);
                    newLayer.setLayerTypeName(layer.getShortName());
                    newLayer.setAnalyticCode(layer.getAnalyticCode());
                    dtoList.add(newLayer);
                }
            }
            if(!isValid){
                LayerType newLayer = new LayerType();
                newLayer.setLayerName(name);
                dtoList.add(newLayer);
            }
        }
        return dtoList;
    }

    @Override
    public ResponseWithData getValideLayerValue(AdditionalLayer[] data){
        ResponseWithData response = new ResponseWithData();
        response.setCode(ErrorCodeEnum.SUCCESS.getValue());
        response.setMessage("");
        for (AdditionalLayer layer : data){
            if (layer.getAnalyticCode().equals(DOSTTRANSP) || layer.getAnalyticCode().equals(GRUNT)){
                if (layer.getCode() == null || layer.getCode().length == 0){
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("В слоях Грунт под строительство или Доступность не имеется поля code или в поле хранится значение null! Архив невалидный и будет удален");
                } else {
                    Object [] codes = layer.getCode();
                    Object code = codes[0];
                    try {
                        Integer codeNum = Integer.parseInt(code.toString());
                        if (layer.getAnalyticCode().equals(DOSTTRANSP)){
                            List<Integer> disnance = layerTypeDao.getDistanceCodeValue();
                            if(!disnance.contains(codeNum)){
                                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                response.setMessage("В слое Доступность, в поле code записан код, для которого нет сооответствия в классификаторе! Архив невалидный и будет удален");
                            }
                        } else {
                            List<Integer> ground = layerTypeDao.getGroundCodeValue();
                            if(!ground.contains(codeNum)){
                                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                                response.setMessage("В слое Грунт под строительство, в поле code записан код, для которого нет сооответствия в классификаторе! Архив невалидный и будет удален");
                            }
                        }
                    } catch (NumberFormatException e){
                        response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                        response.setMessage("В слоях Грунт под строительство или Доступность в поля code записаны не числовые данные! Архив невалидный и будет удален");
                    }
                }
            }
        }
        return response;
    }
}
