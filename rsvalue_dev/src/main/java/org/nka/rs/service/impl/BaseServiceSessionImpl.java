package org.nka.rs.service.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.nka.rs.dao.IBaseDao;
import org.nka.rs.dao.impl.BaseDaoSessionImpl;
import org.nka.rs.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
@Transactional
public class BaseServiceSessionImpl<E> implements IBaseService<E> {

    @Autowired
    @Qualifier("baseDaoSessionImpl")
    private IBaseDao<E> baseDao = new BaseDaoSessionImpl<E>();

    @Override
    public Serializable add(E e) {
        return baseDao.addElement(e);
    }

    @Override
    public void update(E e) {
        baseDao.updateElement(e);
    }

    @Override
    public void updateById(Class<E> clazz, Serializable id) {
        baseDao.updateElementById(clazz, id);
    }

    @Override
    public void refresh(E el) {
        baseDao.refreshElement(el);
    }

    @Override
    public void refreshById(Class<E> clazz, Serializable id) {
        baseDao.refreshElementById(clazz, id);
    }

    @Override
    public void delete(E e) {
        baseDao.deleteElement(e);
    }

    @Override
    public void deleteById(Class<E> clazz, Serializable id) {
        baseDao.deleteElementById(clazz, id);
    }

    @Override
    public List<E> getAll(Class<E> clazz) {
        return baseDao.getAllElements(clazz);
    }

    @Override
    public E getElementById(Class<E> clazz, Serializable id) {
        return baseDao.getElementByID(clazz, id);
    }

    @Override
    public List<E> getCriterion(DetachedCriteria crio) {
        return baseDao.getCriterion(crio);
    }

    @Override
    public E getUniqueResult(DetachedCriteria crio) {
        return baseDao.getUniqueResult(crio);
    }
}
