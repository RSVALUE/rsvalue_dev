package org.nka.rs.service.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.CostTypeDic;
import org.nka.rs.service.ICostTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Service
@Transactional
public class CostTypeServiceImpl extends BaseServiceImpl implements ICostTypeService<CostTypeDic>, IConstant {

    private DetachedCriteria query_cost_dic = DetachedCriteria.forClass(CostTypeDic.class);


    @Override
    public CostTypeDic findByCodeName(String name) {
        CostTypeDic dictionary = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_cost_dic);

        query_.add(Restrictions.eq("codeName", name));
        query_.add(Restrictions.eq("status", RELEVANT));
        dictionary = (CostTypeDic) super.getUniqueResult(query_);
        return  dictionary;
    }

    @Override
    public CostTypeDic findByShortName(String shortName) {
        CostTypeDic dictionary = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_cost_dic);

        query_.add(Restrictions.eq("codeShortName", shortName));
        query_.add(Restrictions.eq("status", RELEVANT));
        dictionary = (CostTypeDic) super.getUniqueResult(query_);
        return  dictionary;
    }

    @Override
    public List<CostTypeDic> findDicForFirstStep() {
        List dictionaries = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_cost_dic);

        query_.add(Restrictions.ne("analyticCode", PARCEL));
        dictionaries = super.getCriterion(query_);
        return  dictionaries;
    }

}
