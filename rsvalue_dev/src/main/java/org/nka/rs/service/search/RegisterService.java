package org.nka.rs.service.search;

import org.nka.rs.dao.JDBCDao.search.RegisterDAO;
import org.nka.rs.entity.dto.DataForRegistrWork;
import org.nka.rs.entity.dto.Registr;
import org.nka.rs.entity.dto.ThematicLayer;

import java.util.List;

/**
 * RegisterService, переписанный без использования аннотаций Hibernate
 */
public class RegisterService {

    private RegisterDAO dao = new RegisterDAO();

    public Object[] getRegister(DataForRegistrWork registrData) {
        return dao.getRegistr(registrData);
    }

    public Registr getLoadInf(Long regCode, Integer funcCode) {
        return dao.getLoadInf(regCode, funcCode);
    }

    public List<ThematicLayer> getLayerList(Long regCode) {
        return dao.getLayerList(regCode);
    }

    public List<Object[]> getZoneList(Long regCode, Integer funcCode) {
        return dao.getZoneList(regCode, funcCode);
    }
}
