package org.nka.rs.service;

import org.nka.rs.entity.dto.DocInfo;
import org.nka.rs.entity.responseVar.SaveDocResponse;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by zgurskaya on 17.06.2016.
 */
public interface IDocumentLoadService extends IBaseService{

    String deleteDoc(Long id);
    SaveDocResponse saveFile(DocInfo docInfo, MultipartFile file, Long userId, Long commonDocId);
    Long getCommonDocId();

}
