package org.nka.rs.service;

import org.nka.rs.entity.pojos.common.Group;

import java.util.List;

public interface IGroupService extends IBaseService<Group>{

    List<String> getGroupsNames();

    Integer getIdByName(String name);
}
