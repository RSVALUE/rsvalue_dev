package org.nka.rs.service.impl;

import org.nka.rs.dao.IGroupDao;
import org.nka.rs.entity.pojos.common.Group;
import org.nka.rs.service.IGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GroupServiceImpl extends BaseServiceSessionImpl<Group> implements IGroupService{

    @Autowired
    IGroupDao groupDao;

    @Override
    public List<String> getGroupsNames() {
        return groupDao.getGroupsNames();
    }

    @Override
    public Integer getIdByName(String name) {
        return groupDao.getIdByName(name);
    }
}
