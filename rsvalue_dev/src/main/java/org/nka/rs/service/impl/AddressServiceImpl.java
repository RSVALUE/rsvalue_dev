package org.nka.rs.service.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nka.rs.dao.IAddressDao;
import org.nka.rs.dao.impl.AddressDaoImpl;
import org.nka.rs.entity.pojos.load.ATE;
import org.nka.rs.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Service
@Transactional
public class AddressServiceImpl extends BaseServiceImpl implements IAddressService<ATE> {

    private DetachedCriteria query_ate = DetachedCriteria.forClass(ATE.class);

    @Autowired
    IAddressDao addressDao = new AddressDaoImpl();

    @Override
    public List<ATE> findATEbyName(String ateName) {
        List<ATE> result = null;
        if (ateName != null || !ateName.equals("")) {

            DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_ate);
            query_ = query_.add(Restrictions.eq("ateName", ateName));
            result = super.getCriterion(query_);
        }
        return result;
    }

    @Override
    public List<ATE> expandATE(String Tree_ids) {
        List<ATE> ate = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_ate);
        List<Integer> paret_int = new ArrayList<Integer>();

        for (String par_str : Tree_ids.substring(1).split(";"))
        {
            paret_int.add(Integer.parseInt(par_str));
        }
        query_ = query_.add(Restrictions.in("ate_id",paret_int.toArray()));
        ate = super.getCriterion(query_.addOrder(Order.asc("treeIds")));
        return ate;

    }

    @Override
    public List<ATE> findATEbyParentId(Long parentate) {
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_ate);
        List<ATE> result = null;
        query_ = parentate != null ? query_.add(Restrictions.eq("parentAteId", parentate)) : query_.add(Restrictions.isNull("parentAteId")) ;
        query_.addOrder(Order.asc("ateName"));
        result = super.getCriterion(query_);
        return  result;
    }

    @Override
    public List<ATE> findATEbyParentLike(Long parentateId, String ateName) {
        List<ATE> result = null;

        if( parentateId == null && (ateName == null || ateName.equals("" ))) {
            return new ArrayList<ATE>();
        }

        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_ate);
        query_ = ateName != null ? query_.add(Restrictions.like("ateName", ateName, MatchMode.ANYWHERE).ignoreCase()):query_;
        if(parentateId != null) {
            query_ =  query_.add(Restrictions.ne("ateId", parentateId));
            query_ =  query_.add(Restrictions.like("treeIds", ";" + String.valueOf(parentateId) + ";" , MatchMode.ANYWHERE).ignoreCase());
        }
        result = super.getCriterion(query_);

        return result;
    }

    @Override
    public List<ATE> findATEbyCategory(Integer categoryId) {
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_ate);
        List<ATE> result = null;
        query_.add(Restrictions.eq("analyticCode", categoryId));
        query_.addOrder(Order.asc("ateName"));
        result = super.getCriterion(query_);
        return  result;
    }


    @Override
    public String fillParentAte(ATE ate) {
        String value = "";
        List<ATE> ate_s = expandATE(ate.getTreeIds());

        for (ATE ate_obj : ate_s) {
            value += ate_obj.getAteName() + "; ";
        }
        return value;
    }

    @Override
    public List<ATE> findAteForCityCostType(Long parentateId) {
        return addressDao.findAteForCityCostType(parentateId);
    }

    @Override
    public List<ATE> findAteForOtherCostType(Long parentateId) {
        return addressDao.findAteForOtherCostType(parentateId);
    }

    @Override
    public List<Long> findAteIdForParcelOutNP(Long parentateId) {
        return addressDao.findAteIdForParcelOutNP(parentateId);
    }

    @Override
    public List<Long> findAteIdForSNP(Long parentateId) {
        return addressDao.findAteIdForSNP(parentateId);
    }


}
