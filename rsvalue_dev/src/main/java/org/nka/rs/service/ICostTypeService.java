package org.nka.rs.service;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface ICostTypeService<CostTypeDic> extends IBaseService {

    CostTypeDic findByCodeName(String name);
    CostTypeDic findByShortName(String shortName);
    List<CostTypeDic> findDicForFirstStep();
}
