package org.nka.rs.service.impl;

import org.nka.rs.dao.IUserAccountDao;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.pojos.common.FilterResponse;
import org.nka.rs.entity.pojos.common.UserAccount;
import org.nka.rs.entity.pojos.common.UserData;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.IUserAccountService;
import org.nka.rs.util.nativedll.NativeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

import static org.nka.rs.service.orders.OrderProcessService.MESSAGE_END;
import static org.nka.rs.service.orders.OrderProcessService.MESSAGE_START;

@Service
@Transactional
public class UserAccountServiceImpl extends BaseServiceSessionImpl<UserAccount> implements IUserAccountService {

    @Autowired
    private IUserAccountDao userAccountDao;

    @Override
    public List<UserAccount> getAll(Integer startRecord, Integer recordCount) {
        return userAccountDao.getAllElements(startRecord, recordCount);
    }

    @Override
    public Serializable add(UserData userData) {
        userData.setPassword(NativeImpl.getHash(userData.getUsername() + userData.getPassword()));
        System.out.println(userData.getPassword());
        return userAccountDao.addElement(userData);
    }

    @Override
    public Long getRowCount() {
        return userAccountDao.getRowCount();
    }

    @Override
    public UserAccount getFullAccount(Long id) {
        return userAccountDao.getFullAccount(id);
    }

    @Override
    public UserAccount getAccountByName(String username) {
        return userAccountDao.getAccountByName(username);
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String position, String group, Integer startRecord, Integer recordCount) {
        return userAccountDao.getFilterElements(surname, login, org, position, group, startRecord, recordCount);
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String group, Integer startRecord, Integer recordCount) {
        return userAccountDao.getFilterElements(surname, login, org, group, startRecord, recordCount);
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String position, String group) {
        return userAccountDao.getFilterRowCount(surname, login, org, position, group);
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String group) {
        return userAccountDao.getFilterRowCount(surname, login, org, group);
    }

    @Override
    public void updateElement(UserData userData) {
        if (userData.getPassword().equals("")) {
            userAccountDao.updateElement(userData, false);
        } else {
            userData.setPassword(NativeImpl.getHash(userData.getUsername() + userData.getPassword()));
            userAccountDao.updateElement(userData, true);
        }
    }

    @Override
    public List<FilterResponse> getFilterFunction(String surname, String login, Integer org, Integer group, Integer page, Integer recordCount) {
        return userAccountDao.getFilterFunction(surname, login, org, group, page, recordCount);
    }

    @Override
    public ResponseWithData deleteById(Long id) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (id == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("id ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
        } else {
            int result = userAccountDao.deleteById(id);
            if (result == 1) {
                responseWithData.setMessage("Пользователь удален");
            } else {
                responseWithData.setMessage("При удалении пользователя произошла ошибка");
            }
            responseWithData.setCode(result);
        }
        return responseWithData;
    }
}
