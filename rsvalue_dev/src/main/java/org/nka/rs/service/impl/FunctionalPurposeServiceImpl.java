package org.nka.rs.service.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.service.IFunctionalPurposeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Service
@Transactional
public class FunctionalPurposeServiceImpl extends BaseServiceImpl implements IFunctionalPurposeService<FunctionalPurposeDic>, IConstant {

    private DetachedCriteria query_funcCode_dic = DetachedCriteria.forClass(FunctionalPurposeDic.class);

    @Override
    public List findByParentCode(Integer parentId) {
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_funcCode_dic);
        List result = null;

        query_.add(Restrictions.eq("parentCode", parentId));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    public List getFuncCodeForGarderCommunity() {
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_funcCode_dic);
        List result = null;

        query_.add(Restrictions.eq("status", RELEVANT));query_.add(Restrictions.eq("analyticCode", GARDENING_ASSOTIATIONS));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    public List getFuncCodeForFarmValue() {

        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_funcCode_dic);
        List result = null;

        query_.add(Restrictions.gt("analyticCode", AGRICULTURE_PURPOSE));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    public List getFuncCodeForMinskRegionGardenCommunity() {
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_funcCode_dic);
        List result = null;

        query_.add(Restrictions.between("analyticCode", LIVING_APARTMENT_ZONE, PRODUCTION_ZONE));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    public List getFuncCodeForParcel(){
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_funcCode_dic);
        List result = null;

        query_.add(Restrictions.isNull("parentCode"));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    public List getFuncCode() {
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_funcCode_dic);
        List result = null;

        query_.add(Restrictions.in("analyticCode", new Integer[] {LIVING_APARTMENT_ZONE, LIVING_MANOR_ZONE, RECREATION_ZONE, BUSINESS_ZONE, PRODUCTION_ZONE}));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    @Override
    public FunctionalPurposeDic findByCodeName(String name) {
        FunctionalPurposeDic dictionary = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_funcCode_dic);

        query_.add(Restrictions.eq("codeName", name));
        query_.add(Restrictions.eq("status", RELEVANT));
        dictionary = (FunctionalPurposeDic) super.getUniqueResult(query_);
        return  dictionary;
    }

}
