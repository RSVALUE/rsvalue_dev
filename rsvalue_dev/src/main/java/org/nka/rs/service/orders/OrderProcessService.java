package org.nka.rs.service.orders;

import org.nka.rs.dao.JDBCDao.orders.OrderProcessDAO;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.pojos.order.OrderObject;
import org.nka.rs.entity.responseVar.ResponseWithData;

/**
 * Класс-сервис для OrderProcessDAO. Включает в себя
 * константы для типов поисков, а так же строковые
 * константы для инициализации неуспешных объектов-ответов
 * Типовой алгоритм: на вход приходят параметры\объекты, их
 * значения проверяются на not null-значение и при успешной
 * проверке вызывается соответствующий DAO-метод
 */
public class OrderProcessService {

    private static OrderProcessDAO orderProcessDAO = new OrderProcessDAO();

    //Блок типов поиска
    private final static int CADASTER_SEARCH = 10;

    private final static int ADDRESS_SEARCH = 20;

    private final static int LOCATION_SEARCH = 30;

    private final static int ST_SEARCH = 40;

    private final static int ST_NOTARIUS_SEARCH = 50;

    private final static int SNP_SEARCH = 70;

    private final static int SX_UNP_SEARCH = 81;

    private final static int SX_NAME_SEARCH = 82;

    private final static int SX_ZONE_SEARCH = 83;

    //Константы, использующиеся при инициализации неуспешных ответов
    public final static String MESSAGE_START = "ERROR: параметр(ы) ";

    public final static String MESSAGE_END = "не предоставлен(ы)";

    private final static String MESSAGE_PARAMETERS_MISSING = "Для заданного типа поиска отсутствуют обязательные параметры";

    /**
     * Сохраняет новые стоимости по заказу
     *
     * @param orderId ID заказа
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData saveAllOrder(Long orderId, Long userId){
        return orderProcessDAO.saveAllCost(orderId, userId);
    }

    /**
     * Сохраняет новые стоимости по объекту
     *
     * @param objId ID объекта
     * @param execId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData confirmOrder(Long objId, Long execId) {
        return orderProcessDAO.confirmOrder(objId, execId);
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param ate ID АТЕ
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData addressSearch(Integer ate, String date) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (ate == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("ate ");
            flag = true;
        }
        if (date.equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("date ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.addressSearch(ate, date);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param soato Номер СОАТО
     * @param blockNum Номер блока
     * @param parcelNum Номер участка
     * @param date Дата поиска
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData cadNumSearch(Long soato, Integer blockNum, Integer parcelNum, String date, Long userId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (soato == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("soato ");
            flag = true;
        }
        if (blockNum == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("blockNum ");
            flag = true;
        }
        if (parcelNum == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("parcelNum ");
            flag = true;
        }
        if (date.equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("date ");
            flag = true;
        }
        if (userId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("userId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.cadNumSearch(soato, blockNum, parcelNum, date, userId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param soato Номер СОАТО
     * @param blockNum Номер блока
     * @param parcelNum Номер участка
     * @param date Дата поиска
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData stCadNumSearch(Long soato, Integer blockNum, Integer parcelNum, String date, Long userId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (soato == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("soato ");
            flag = true;
        }
        if (blockNum == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("blockNum ");
            flag = true;
        }
        if (parcelNum == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("parcelNum ");
            flag = true;
        }
        if (date.equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("date ");
            flag = true;
        }
        if (userId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("userId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.stCadNumSearch(soato, blockNum, parcelNum, date, userId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param region Регион
     * @param area Область
     * @param ss Сельсовет
     * @param snp СНП
     * @param bjd Белорусская Ж\Д
     * @param road Номер дороги
     * @param km Километр дороги
     * @param street Улица
     * @param typeIn Входной тип
     * @param typeOut Выходной тип
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAddressList(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street,
                                           Integer typeIn, Integer typeOut, Long userId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        ResponseWithData responseWithData = new ResponseWithData();
        boolean flag = false;
        if (typeIn == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("typeIn ");
            flag = true;
        }
        if (typeOut == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("typeOut ");
            flag = true;
        }
        if (userId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("userId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.getAddressList(region, area, ss, snp, bjd, road, km, street, typeIn, typeOut, userId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param parentId ID родительского ATE
     * @param ate ID ATE
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAddressElementList(Integer parentId, Long ate) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (parentId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("parentId ");
            flag = true;
        }
        if (ate == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("ate ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.getAddressElementList(parentId, ate);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param parentId ID родительского ATE
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAteList(Long parentId, Long userId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (parentId == null) {
            parentId = -1L;
        }
        if (userId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("userId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.getAteList(parentId, userId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param ateId ID ATE
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getSTList(Long ateId, String date) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (ateId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("ateId ");
            flag = true;
        }
        if (date.equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("date ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.getSTList(ateId, date);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param ateId ID ATE
     * @param date Дата поиска
     * @param type Тип поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getSXList(Long ateId, String date, Integer type) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (ateId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("ateId ");
            flag = true;
        }
        if (date.equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("date ");
            flag = true;
        }
        if (type == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("type ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.getSXList(ateId, date, type);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param type Тип поиска
     * @param text Текст поиска
     * @param ateId ID ATE
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getSXSearch(Integer type, String text, Long ateId, String date) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (type == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("type ");
            flag = true;
        }
        if (text.equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("text ");
            flag = true;
        }
        if (ateId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("ateId ");
            flag = true;
        }
        if (date.equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("date ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.getSXSearch(type, text, ateId, date);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param orderId ID объекта заказа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData orderProcess(Long orderId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (orderId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("orderId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.orderProcess(orderId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData removeFromOrder(Long objId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (objId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("objId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.removeFromOrder(objId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param orderId ID заказа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData removeAllFromOrder(Long orderId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (orderId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("orderId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.truncateOrder(orderId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData resetLocation(Long objId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (objId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("objId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.resetLocation(objId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param objId ID объекта
     * @param coordinates Коордитаны нового местоположения
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData updateLocation(Long objId, String coordinates) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (objId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("objId ");
            flag = true;
        }
        if (coordinates == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("coordinates ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.updateLocation(objId, coordinates);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает метод,
     * проверяющий тип поиска
     *
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData addInOrder(OrderObject orderObject) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (orderObject.getOrderId() == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("orderId ");
            flag = true;
        }
        if (orderObject.getSearchType() == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("searchType ");
            flag = true;
        }
        if (orderObject.getDate().equals("")) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("date ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return checkSearchType(orderObject);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData costSearch(Long objId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (objId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("objId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.costSearch(objId);
        }
    }

    /**
     * Проводит проверку входных параметров. Если все хорошо, то вызывает соответствующий
     * DAO метод, иначе инициирует неуспешный объект-ответ
     *
     * @param objId ID объекта
     * @param funcCode Код функционального назначения
     * @param zoneId ID новой зоны
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData updateZones(Long objId, Integer funcCode, Long zoneId) {
        StringBuilder responseMessage = new StringBuilder();
        responseMessage.append(MESSAGE_START);
        boolean flag = false;
        ResponseWithData responseWithData = new ResponseWithData();
        if (objId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("objId ");
            flag = true;
        }
        if (funcCode == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("funcCode ");
            flag = true;
        }
        if (zoneId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseMessage.append("zoneId ");
            flag = true;
        }
        if (flag) {
            responseMessage.append(MESSAGE_END);
            responseWithData.setMessage(responseMessage.toString());
            return responseWithData;
        } else {
            return orderProcessDAO.updateZones(objId, funcCode, zoneId);
        }
    }

    /**
     * Метод, проверяющий тип поиска. Если ни один тип поиска не
     * подхдоит или параметры пропущеты вызывается метод инициализации
     * неуспешного объекта-ответа, иначе вызывается метод добавления
     * объекта в заказ
     *
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    private ResponseWithData checkSearchType(OrderObject orderObject) {
        boolean flag = true;
        switch (orderObject.getSearchType()) {
            case CADASTER_SEARCH:
                flag = checkCadaster(orderObject);
                break;
            case ADDRESS_SEARCH:
                flag = (checkAddressCadaster(orderObject) || checkAddressPoint(orderObject));
                break;
            case LOCATION_SEARCH:
                flag = checkLocation(orderObject);
                break;
            case ST_SEARCH:
                flag = (checkStCadaster(orderObject) || checkStAddress(orderObject));
                break;
            case ST_NOTARIUS_SEARCH:
                flag = checkStNotarius(orderObject);
                break;
            case SNP_SEARCH:
                flag = checkSNP(orderObject);
                break;
            case SX_UNP_SEARCH:
                flag = checkSX(orderObject);
                break;
            case SX_NAME_SEARCH:
                flag = checkSX(orderObject);
                break;
            case SX_ZONE_SEARCH:
                flag = checkSX(orderObject);
                break;
        }
        if (flag) {
            return initParametersMissing();
        } else {
            return orderProcessDAO.addInOrder(orderObject);
        }
    }

    /**
     * Проверка ID организации
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkOrgId(OrderObject orderObject) {
        return orderObject.getOrgId() == null;
    }

    /**
     * Проверка ID участка
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkParcelId(OrderObject orderObject) {
        return orderObject.getParcelId() == null;
    }

    /**
     * Проверка ID района
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkDistrId(OrderObject orderObject) {
        return orderObject.getDistrId() == null;
    }

    /**
     * Проверка ID адреса
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkAddressId(OrderObject orderObject) {
        return orderObject.getAddressId() == null;
    }

    /**
     * Проверка предыдущего адреса
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - equals(""), false - not equals
     */
    private boolean checkPrevAddress(OrderObject orderObject) {
        return orderObject.getPrevAddress().equals("");
    }

    /**
     * Проверка координат
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - equals(""), false - not equals
     */
    private boolean checkCoordinates(OrderObject orderObject) {
        return orderObject.getCoordinates().equals("");
    }

    /**
     * Проверка дополнительного параметра
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkSID(OrderObject orderObject) {
        return orderObject.getsId() == null;
    }

    /**
     * Проверка ID организации и участка
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkCadaster(OrderObject orderObject) {
        return (checkOrgId(orderObject) && checkParcelId(orderObject));
    }

    /**
     * Проверка ID адреса, ID организации, ID участка и дополнительного параметра
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkAddressCadaster(OrderObject orderObject) {
        return (checkAddressId(orderObject) && checkCadaster(orderObject) && checkSID(orderObject));
    }

    /**
     * Проверка ID адреса, координат и дополнительного параметра
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkAddressPoint(OrderObject orderObject) {
        return (checkAddressId(orderObject) && checkCoordinates(orderObject) && checkSID(orderObject));
    }

    /**
     * Проверка ID района и прерыдущего адреса
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkLocation(OrderObject orderObject) {
        return (checkDistrId(orderObject) && checkPrevAddress(orderObject));
    }

    /**
     * Проверка дополнительного параметра, ID организации и ID участка
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkStCadaster(OrderObject orderObject) {
        return (checkSID(orderObject) && checkCadaster(orderObject));
    }

    /**
     * Проверка дополнительного праметра и предыдущего адреса
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkStAddress(OrderObject orderObject) {
        return (checkSID(orderObject) && checkPrevAddress(orderObject));
    }

    /**
     * Проверка ID адреса
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkStNotarius(OrderObject orderObject) {
        return (checkDistrId(orderObject));
    }

    /**
     * Проверка дополнительного параметра и ID района
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkSNP(OrderObject orderObject) {
        return (checkSID(orderObject) && checkDistrId(orderObject));
    }

    /**
     * Проверка дополнительного параметра
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return True - null, false - not null
     */
    private boolean checkSX(OrderObject orderObject) {
        return (checkSID(orderObject));
    }

    /**
     * Функция, инициализирующая неуспешный объект-ответ
     * @return Объект ResponseWithData с кодом, сообщением и данными по
     * отсутствующим параметрам
     */
    private ResponseWithData initParametersMissing() {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
        responseWithData.setMessage(MESSAGE_PARAMETERS_MISSING);
        return responseWithData;
    }
}
