package org.nka.rs.service;

import org.nka.rs.entity.pojos.common.Operation;

import java.sql.Date;

public interface IOperationService extends IBaseService<Operation>{

    Operation getElementById(Long id);

    Long getIdByDate(Date date);
}
