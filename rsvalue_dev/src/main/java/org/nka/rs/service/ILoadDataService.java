package org.nka.rs.service;

import org.json.JSONObject;
import org.nka.rs.entity.dto.Bound;
import org.nka.rs.entity.dto.LoadData;
import org.nka.rs.entity.dto.Zone;
import org.nka.rs.entity.responseVar.Report;
import org.nka.rs.entity.responseVar.Response;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by zgurskaya on 10.06.2016.
 */
public interface ILoadDataService {

    Report loadData(LoadData data) throws SQLException;

    String saveZonesGeometry(JSONObject geometry, Long dwd_id);

    String saveBoundsGeometry(JSONObject geometry, Long dwd_id);

    Response saveLayerGeometry(JSONObject geometry, Long dwd_id, String layerName);

    List encodingZones(JSONObject geojson) throws UnsupportedEncodingException;

    List encodingBounds(JSONObject geojson) throws UnsupportedEncodingException;

    Response validateZonesFields(String[] fields, Integer costTypeId);

    Response validateBoundsFields(String[] fields);

    Response validateZonesValues(Zone[] zones, Integer costTypeId, Long objectnumber);

    Response validateBoundsValues(Bound[] bounds);

    Object createReport(Long[] ateList, Long objectnumber, Integer costTypeId);

    List getNationalCurrency();

    List getForeignCurrency();

    void clearErrorLoad(Long regNum);



}

