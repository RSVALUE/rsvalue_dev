package org.nka.rs.service;

import org.nka.rs.entity.dictionary.ValueOrganizationDic;

import java.util.List;

/**
 * Created by zgurskaya on 02.06.2016.
 */
public interface IValueOrgService extends IBaseService {

    List<ValueOrganizationDic> getActualOrganization();


}
