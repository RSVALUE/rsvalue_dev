package org.nka.rs.service.impl;

import org.nka.rs.dao.IParamDao;
import org.nka.rs.dao.impl.ParamDaoImpl;
import org.nka.rs.service.IParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;

/**
 * Created by zgurskaya on 17.06.2016.
 */

@Service
@Transactional
public class ParamServiceImpl extends BaseServiceImpl implements IParamService {


    @Autowired
    IParamDao paramDao = new ParamDaoImpl();

    @Override
    public Object[]  validateLoadObject(Integer costTypeId, Long objectnumber, Integer funcCodeId) throws SQLException {
        return paramDao.validateLoadObject(costTypeId, objectnumber, funcCodeId);
    }
}
