package org.nka.rs.service;

import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dto.emessage.*;
import org.nka.rs.entity.responseVar.ResponseWithData;

import java.util.List;

/**
 * Created by zgurskaya on 05.10.2016.
 */
public interface IMessageService {

    ResponseWithData cadastrValueSearch (Long soato, Integer blockNum, Integer parcelNum, Boolean isNb, String date);
    ResponseWithData cadastrValueSearch (String cadnum, Boolean isNb, String date);
    AddressMessage addressSearch (Long objectnumber, Boolean isNb, String date);
    AddressMessage mapSearch (String coord, Boolean isCadNum);
    ResponseWithData pointValueSearch (String address, String zone, Boolean isNb, String date);
    CadastralValueMessage communityGardenNameSearch (Long zoneId, Boolean isNb, String date);
    CadastralValueMessage landsOutLocalitySearch (Long zoneId, Boolean isNb, String date);
    CadastralValueMessage farmLandsUNPSearch(Long objectnumber, Long unp, Boolean isNb, String date);
    CadastralValueMessage farmLandsNameSearch (Long objectnumber, String tenantName, Boolean isNb, String date);
    CadastralValueMessage farmLandsZonenNumberSearch (String zoneId, Boolean isNb, String date);
    List<String[]> getCommunityGardenList (Long objectnumber, String date);
    List<String> getTenantNameList (Long objectnumber, String date);
    List<String> getLandOutLocalityList (Integer funcode, Long objectnumber, String date);
    List<Object []> getZoneNumberList (Integer funcode, Long objectnumber, String date, String zoneName);
    List<String []> getFarmZoneNumberList (Long objectnumber, String date);
    List getAddressElementList(Integer parentElementId, Long objectnumber);
    List<String> getPeriodList();
    List<FunctionalPurposeDic> getFuncCode();
    List getListAddress(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street, Integer typeIn, Integer typeOut);
    ResponseWithData createPdf(PdfData data);
    String searchPathById(Long id);
    void deletePdfById(Long id);

}
