package org.nka.rs.service.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.MethodTypeDic;
import org.nka.rs.service.IMethodTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Service
@Transactional
public class MethodTypeServiceImpl extends BaseServiceImpl implements IMethodTypeService<MethodTypeDic>, IConstant {

    private DetachedCriteria query_meth_dic = DetachedCriteria.forClass(MethodTypeDic.class);

    @Override
    public List findByParentCode(Integer parentId) {
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_meth_dic);
        List result = null;

        query_.add(Restrictions.eq("parentCode", parentId));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    public List<MethodTypeDic> getMethodTypesForNonFarmValue() {
        List<MethodTypeDic> result = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_meth_dic);
        //на данный момент доступен только один метод, потом можно будет все
        /*query_.add(Restrictions.eq("parentCode", FARM_METH));*/
        //а потом сказали, что нужно два последних. на всякий случай
        query_.add(Restrictions.eq("analyticCode", METHOD_2016));
        //query_.add(Restrictions.in("analyticCode", new Integer[] {METHOD_2010_2015, METHOD_2016}));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }


    public List<MethodTypeDic> getMethodTypesForFarmValue() {
        List<MethodTypeDic> result = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_meth_dic);

        query_.add(Restrictions.eq("parentCode", FARM_METH));
        query_.add(Restrictions.eq("status", RELEVANT));
        result = super.getCriterion(query_);
        return  result;
    }

    @Override
    public MethodTypeDic findByCodeName(String name) {
        MethodTypeDic dictionary = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_meth_dic);

        query_.add(Restrictions.eq("codeName", name));
        query_.add(Restrictions.eq("status", RELEVANT));
        dictionary = (MethodTypeDic) super.getUniqueResult(query_);
        return  dictionary;
    }


}
