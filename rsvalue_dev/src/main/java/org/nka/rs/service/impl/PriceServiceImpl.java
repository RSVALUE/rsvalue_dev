package org.nka.rs.service.impl;

import org.nka.rs.dao.IPriceDao;
import org.nka.rs.dao.impl.PriceDaoImpl;
import org.nka.rs.entity.dictionary.OperationTypeDic;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.dto.price.PriceData;
import org.nka.rs.entity.dto.price.SimplePrice;
import org.nka.rs.entity.pojos.price.GetPriceObj;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.service.IPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 10.10.2016.
 */

@Service
@Transactional
public class PriceServiceImpl implements IPriceService {

    @Autowired
    IPriceDao priceDao = new PriceDaoImpl();


    @Override
    public List<TORDic> getTorList(Long userId) {
        return priceDao.getTorList(userId);
    }

    @Override
    public Object[] getPriceListWithSorting(String priceNum, Integer sortingParam, String predicate, Integer page, Integer amount, Long userId, Integer torId) {
        return priceDao.getPriceListWithSorting(priceNum, sortingParam, predicate, page, amount, userId, torId);
    }

    @Override
    public ResponseWithData readCreateEditPrice(Long userId, Integer torId, Long priceId) {
        ResponseWithData response;
        if (priceId == null) {
            response = priceDao.getPricePattern(userId);
        } else {
            response = priceDao.getPrice(priceId, userId);
        }
        return response;
    }

    @Override
    public GetPriceObj readCreateEditPrice(Long userId, Long priceId) {
        GetPriceObj response = priceDao.getPriceNew(userId, priceId);
        return response;
    }

    @Override
    public List<OperationTypeDic> getOperType() {
        return priceDao.getOperType();
    }

    @Override
    public Response stopPrice(Long priceId, String date, Long userId, Integer operTypeId) {
        return priceDao.stopPrice(priceId, date, userId, operTypeId);
    }

    @Override
    public Response savePrice(PriceData price) {
        return priceDao.savePrice(price);
    }

    @Override
    public Response savePrice2(SimplePrice price) {
        return priceDao.savePriceNew(price);
    }

    @Override
    public Response validatePriceNumber(Integer torId, String priceNumber) {
        return priceDao.validatePriceNumber(torId, priceNumber);
    }
}
