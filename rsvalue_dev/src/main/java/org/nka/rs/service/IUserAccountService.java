package org.nka.rs.service;

import org.nka.rs.entity.pojos.common.FilterResponse;
import org.nka.rs.entity.pojos.common.UserAccount;
import org.nka.rs.entity.pojos.common.UserData;
import org.nka.rs.entity.responseVar.ResponseWithData;

import java.io.Serializable;
import java.util.List;

public interface IUserAccountService extends IBaseService<UserAccount>{

    List<UserAccount> getAll(Integer startRecord, Integer recordCount);

    Serializable add(UserData userData);

    Long getRowCount();

    UserAccount getFullAccount(Long id);

    UserAccount getAccountByName(String username);

    List<UserAccount> getFilterElements(String surname, String login, String org, String position, String group, Integer startRecord, Integer recordCount);

    List<UserAccount> getFilterElements(String surname, String login, String org, String group, Integer startRecord, Integer recordCount);

    Long getFilterRowCount(String surname, String login, String org, String position, String group);

    Long getFilterRowCount(String surname, String login, String org, String group);

    void updateElement(UserData userData);

    List<FilterResponse> getFilterFunction(final String surname, final String login, Integer org, Integer group, Integer page, Integer recordCount);

    ResponseWithData deleteById(Long id);
}
