package org.nka.rs.service.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nka.rs.entity.pojos.load.CostObject;
import org.nka.rs.service.ICostObjectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Service
@Transactional
public class CostObjectServiceImpl extends BaseServiceImpl implements ICostObjectService<CostObject>{

    private DetachedCriteria query_cost_object = DetachedCriteria.forClass(CostObject.class);

    @Override
    public List<CostObject> getCostObjectByRequiredParam(Integer costTypeId, Integer funcCodeId, Long objectnumber) {
        List<CostObject> costObjectList = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_cost_object);

        query_.add(Restrictions.eq("costType",costTypeId));
        query_.add(Restrictions.eq("methodType",funcCodeId));
        query_.add(Restrictions.eq("objectnumber",objectnumber));
        query_.addOrder(Order.asc("dateVal"));

        costObjectList = super.getCriterion(query_);

        return costObjectList;
    }
}
