package org.nka.rs.service.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.nka.rs.dao.IBaseDao;
import org.nka.rs.dao.impl.BaseDaoSessionImpl;
import org.nka.rs.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Service
@Transactional
public class BaseServiceImpl<E> implements IBaseService<E> {

    @Autowired
    @Qualifier("baseDaoSessionImpl")
    private IBaseDao<E> baseDao = new BaseDaoSessionImpl<E>();

    @Override
    public Serializable add(E e) {
        Serializable id = null;
        try {
            id = baseDao.addElement(e);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return id;
    }

    @Override
    public void update(E e) {
        try {
            baseDao.updateElement(e);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    @Override
    public void updateById(Class<E> clazz, Serializable id) {
        try {
            baseDao.updateElementById(clazz, id);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    @Override
    public void refresh(E el) {
        try {
            baseDao.refreshElement(el);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    @Override
    public void refreshById(Class<E> clazz, Serializable id) {
        try {
            baseDao.refreshElementById(clazz, id);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    @Override
    public void delete(E e) {
        try {
            baseDao.deleteElement(e);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    @Override
    public void deleteById(Class<E> clazz, Serializable id) {
        try {
            baseDao.deleteElementById(clazz, id);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    @Override
    public List<E> getAll(Class<E> clazz) {
        List<E> result = null;
        try {
            result = baseDao.getAllElements(clazz);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return result;
    }

    @Override
    public E getElementById(Class<E> clazz, Serializable id) {
        E e = null;
        try {
            e = baseDao.getElementByID(clazz, id);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return e;
    }

    @Override
    public List<E> getCriterion(DetachedCriteria crio) {
        List<E> result = null;
        try {
            result = baseDao.getCriterion(crio);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return result;
    }

    @Override
    public E getUniqueResult(DetachedCriteria crio) {
        E obj = null;
        try {
            obj = baseDao.getUniqueResult(crio);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return obj;
    }
}
