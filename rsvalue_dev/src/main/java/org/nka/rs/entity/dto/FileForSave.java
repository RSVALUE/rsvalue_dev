package org.nka.rs.entity.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by zgurskaya on 07.07.2016.
 */
public class FileForSave {

    private MultipartFile file;

    private DocInfo docInfo;

    private Long dwd_id;

    public FileForSave() {
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public DocInfo getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(DocInfo docInfo) {
        this.docInfo = docInfo;
    }

    public Long getDwd_id() {
        return dwd_id;
    }

    public void setDwd_id(Long dwd_id) {
        this.dwd_id = dwd_id;
    }
}
