package org.nka.rs.entity.dto.emessage;

/**
 * Created by zgurskaya on 02.12.2016.
 */
public class CadastralInfo {
    //адрес
    private String address;
    //дата, на которую ведется поиск
    private String searchDate;

    //данные при поиске по кадастровому номеру
    //кадастровый номер
    private String cadNum;
    //площадь
    private Double square;
    //назначение по классификатору
    private String purposeDic;
    //назначение по решению местного исполнительного органа
    private  String purposeGov;

    //данные при поиске по садовым товариществам
    //наименование садового товарищества
    private String communityGardenName;
    //наименование ближайшего населенного пункта
    private String nearLocality;

    //данные при поиске по ВНП
    //наименование оценочной зоны
    private String zoneName;

    //данные при поиске по с/х землям
    //УНП землепользователя
    private Long unp;
    //наименование землепользователя
    private String tenantName;


    //
    private String remark;

    public CadastralInfo() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

    public String getCadNum() {
        return cadNum;
    }

    public void setCadNum(String cadNum) {
        this.cadNum = cadNum;
    }

    public Double getSquare() {
        return square;
    }

    public void setSquare(Double square) {
        this.square = square;
    }

    public String getPurposeDic() {
        return purposeDic;
    }

    public void setPurposeDic(String purposeDic) {
        this.purposeDic = purposeDic;
    }

    public String getPurposeGov() {
        return purposeGov;
    }

    public void setPurposeGov(String purposeGov) {
        this.purposeGov = purposeGov;
    }

    public String getCommunityGardenName() {
        return communityGardenName;
    }

    public void setCommunityGardenName(String communityGardenName) {
        this.communityGardenName = communityGardenName;
    }

    public String getNearLocality() {
        return nearLocality;
    }

    public void setNearLocality(String nearLocality) {
        this.nearLocality = nearLocality;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public Long getUnp() {
        return unp;
    }

    public void setUnp(Long unp) {
        this.unp = unp;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
