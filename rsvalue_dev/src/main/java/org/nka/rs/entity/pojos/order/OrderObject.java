package org.nka.rs.entity.pojos.order;

/**
 * Класс, в котором содержатся поля-сведения о заказе
 */
public class OrderObject {

    private Long orderId;

    private Integer searchType;

    private String date;

    private Long distrId;

    private Long parcelId;

    private Long orgId;

    private Long addressId;

    private String prevAddress;

    private String coordinates;

    private Long sId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getSearchType() {
        return searchType;
    }

    public void setSearchType(Integer searchType) {
        this.searchType = searchType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getDistrId() {
        return distrId;
    }

    public void setDistrId(Long distrId) {
        this.distrId = distrId;
    }

    public Long getParcelId() {
        return parcelId;
    }

    public void setParcelId(Long parcelId) {
        this.parcelId = parcelId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getPrevAddress() {
        return prevAddress;
    }

    public void setPrevAddress(String prevAddress) {
        this.prevAddress = prevAddress;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Long getsId() {
        return sId;
    }

    public void setsId(Long sId) {
        this.sId = sId;
    }
}
