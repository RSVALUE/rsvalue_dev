package org.nka.rs.entity.pojos.price;

import java.util.ArrayList;

/**
 * Created by Shcherbina on 22.11.2016.
 */
public class GetPriceObj {
    private ArrayList <PriceEntity> priceEntity;
    private int type;

    private String inf;

    private int priceId;
    private String number_price;
    private String start_date;
    private String stop_date;
    private int tor;
    private String torName;
    private double nds;

    private String message;

    public GetPriceObj() {
    }

    public String getInf() {
        return inf;
    }

    public void setInf(String inf) {
        this.inf = inf;
    }

    public ArrayList<PriceEntity> getPriceEntity() {
        return priceEntity;
    }

    public void setPriceEntity(ArrayList<PriceEntity> priceEntity) {
        this.priceEntity = priceEntity;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public String getNumber_price() {
        return number_price;
    }

    public void setNumber_price(String number_price) {
        this.number_price = number_price;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStop_date() {
        return stop_date;
    }

    public void setStop_date(String stop_date) {
        this.stop_date = stop_date;
    }

    public int getTor() {
        return tor;
    }

    public void setTor(int tor) {
        this.tor = tor;
    }

    public String getTorName() {
        return torName;
    }

    public void setTorName(String torName) {
        this.torName = torName;
    }

    public double getNds() {
        return nds;
    }

    public void setNds(double nds) {
        this.nds = nds;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GetPriceObj(ArrayList<PriceEntity> priceEntity, int type, int priceId, String number_price, String start_date, String stop_date, int tor, String torName, double nds, String message) {
        this.priceEntity = priceEntity;
        this.type = type;
        this.priceId = priceId;
        this.number_price = number_price;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.tor = tor;
        this.torName = torName;
        this.nds = nds;
        this.message = message;


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GetPriceObj that = (GetPriceObj) o;

        if (type != that.type) return false;
        if (priceId != that.priceId) return false;
        if (tor != that.tor) return false;
        if (Double.compare(that.nds, nds) != 0) return false;
        if (!priceEntity.equals(that.priceEntity)) return false;
        if (!number_price.equals(that.number_price)) return false;
        if (!start_date.equals(that.start_date)) return false;
        if (!stop_date.equals(that.stop_date)) return false;
        if (!torName.equals(that.torName)) return false;
        return message.equals(that.message);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = priceEntity.hashCode();
        result = 31 * result + type;
        result = 31 * result + priceId;
        result = 31 * result + number_price.hashCode();
        result = 31 * result + start_date.hashCode();
        result = 31 * result + stop_date.hashCode();
        result = 31 * result + tor;
        result = 31 * result + torName.hashCode();
        temp = Double.doubleToLongBits(nds);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + message.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "GetPriceObj{" +
                "priceEntity=" + priceEntity +
                ", type=" + type +
                ", priceId=" + priceId +
                ", number_price='" + number_price + '\'' +
                ", start_date='" + start_date + '\'' +
                ", stop_date='" + stop_date + '\'' +
                ", tor=" + tor +
                ", torName='" + torName + '\'' +
                ", nds=" + nds +
                ", message='" + message + '\'' +
                '}';
    }
}
