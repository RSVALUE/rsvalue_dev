package org.nka.rs.entity.constant.loadConstant;

/**
 * Created by zgurskaya on 04.08.2016.
 */
public enum FarmLandsFieldsEnum {


    OBJECTNUMBER("Objectnumb"),
    NUM("Num"),
    NAME("Name"),
    UNP("Unp"),
    COST_D("Cost_d"),
    COST_R("Cost_r");

    private final String columnName;

    FarmLandsFieldsEnum(String columnName) {
        this.columnName = columnName;
    }

    public static FarmLandsFieldsEnum search(String columnName) {
        for (FarmLandsFieldsEnum param : FarmLandsFieldsEnum.values()) {
            if (columnName.equals(param.getColumnName())) {
                return param;
            }
        }
        return null;
    }

    public String getColumnName() {
        return columnName;
    }

}
