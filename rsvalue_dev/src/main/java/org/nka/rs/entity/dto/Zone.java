package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 09.06.2016.
 */

//класс для выгрузки данных из дбф-файла "Зоны"
public class Zone implements Comparable {

    //номер оценочной зоны = zonenumber
    private String num;
    //площадь, только для города (а еще СНП) = square
    private Double area;
    //цена в долларах = costD
    private Double cost_d;
    //цена в рублях = costR
    private Double cost_r;
    //АТЕ - для 10 - город, для 20 - СНП, для 40 - с/с, для 30 - р-н
    // = objectnumber
    private Long objectnumb;
    //наименование оценочной зоны = zone_name
    private String name;
    //поле не хранится в БД, проверяется наличие
    private Long soato;
    //категория = zone_desc
    private String category;
    //сельсовет = near_loc
    private String ss;
    //наименование района - в базе сохраняется как parentObjectnum
    private String distr;
    //наименование садового товарищества = zone_name
    private String stname;
    //УНП землевладельца
    private Long unp;
    //ближайший населенный пункт = near_loc
    private String npname;
    //objectnumber сельсовета
    private Long regn_ss;

    public Zone() {
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getCost_d() {
        return cost_d;
    }

    public void setCost_d(Double cost_d) {
        this.cost_d = cost_d;
    }

    public Double getCost_r() {
        return cost_r;
    }

    public void setCost_r(Double cost_r) {
        this.cost_r = cost_r;
    }

    public Long getObjectnumb() {
        return objectnumb;
    }

    public void setObjectnumb(Long objectnumb) {
        this.objectnumb = objectnumb;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSoato() {
        return soato;
    }

    public void setSoato(Long soato) {
        this.soato = soato;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSs() {
        return ss;
    }

    public void setSs(String ss) {
        this.ss = ss;
    }

    public String getDistr() {
        return distr;
    }

    public void setDistr(String distr) {
        this.distr = distr;
    }

    public String getStname() {
        return stname;
    }

    public void setStname(String stname) {
        this.stname = stname;
    }

    public Long getUnp() {
        return unp;
    }

    public void setUnp(Long unp) {
        this.unp = unp;
    }

    public String getNpname() {
        return npname;
    }

    public void setNpname(String npname) {
        this.npname = npname;
    }

    public Long getRegn_ss() {
        return regn_ss;
    }

    public void setRegn_ss(Long regn_ss) {
        this.regn_ss = regn_ss;
    }

    @Override
    public int compareTo(Object o) {
        Zone tmp = (Zone) o;
        if(this.getObjectnumb() < tmp.getObjectnumb()){
            return -1;
        } else if (this.getObjectnumb() > tmp.getObjectnumb()){
            return 1;
        }
        return 0;
    }
}

