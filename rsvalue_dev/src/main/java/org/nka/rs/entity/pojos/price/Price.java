package org.nka.rs.entity.pojos.price;

import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.pojos.common.Operation;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 07.10.2016.
 */

@Entity
@Table(name = "PRICE_LIST")

public class Price {

    //ИД прейскуранта
    @Id
    @Column(name = "PRICE_ID", unique = true, nullable = false)
    private Long priceId;

    //номер прейскуранта
    @Column(name = "NUMBER_PRICE")
    private String priceNumber;

    //файл-документ (документ-содержание)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "price")
    private List<PriceContent> priceContents;

    //ТОР
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_CODE", nullable = false)
    private TORDic tor;

    //дата начала действия
    @Temporal(TemporalType.DATE)
    @Column(name = "START_DATE", nullable = false)
    private Date startDate;

    //дата окончания действия прейскуранта
    @Temporal(TemporalType.DATE)
    @Column(name = "STOP_DATE", nullable = false)
    private Date stopDate;

    //Операция
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OOPER_ID", nullable = false)
    private Operation operation;

    //НДС - данные хранятся в другой таблице, но выводятся на фронтэнд при получении ResultSet
    private Double tax;


    public Price() {
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public String getPriceNumber() {
        return priceNumber;
    }

    public void setPriceNumber(String priceNumber) {
        this.priceNumber = priceNumber;
    }

    public List<PriceContent> getPriceContents() {
        return priceContents;
    }

    public void setPriceContents(List<PriceContent> priceContents) {
        this.priceContents = priceContents;
    }

    public TORDic getTor() {
        return tor;
    }

    public void setTor(TORDic tor) {
        this.tor = tor;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }
}
