package org.nka.rs.entity.dto.price;

import oracle.sql.NUMBER;

/**
 * Created by Shcherbina on 30.11.2016.
 */
public class SimplePrice {
    private String NUMBER_PRICE;      // VARCHAR2 , - номер прайса
    private String START_DATE;        // DATE , - дата начала действия прайса
    private String STOP_DATE;         // DATE DEFAULT NULL, - дата прекращения прайса
    private String CONTENT;           // VARCHAR2, - стоимости в виде строки  SUBJECT_TYPE1,DOC_TYPE1,PRICE_URG1,PRICE_NONURG1;SUBJECT_TYPE2,DOC_TYPE2,PRICE_URG2,PRICE_NONURG2;
    private Long EXEC;              // NUMBER, - id пользователя
    private Long PREVID;            //NUMBER DEFAULT NULL)  - если редактирование то присылается id предыдущего прейскуранта , если новый то это поле пусто

    public SimplePrice(String NUMBER_PRICE, String START_DATE, String STOP_DATE, String CONTENT, Long EXEC, Long PREVID) {
        this.NUMBER_PRICE = NUMBER_PRICE;
        this.START_DATE = START_DATE;
        this.STOP_DATE = STOP_DATE;
        this.CONTENT = CONTENT;
        this.EXEC = EXEC;
        this.PREVID = PREVID;
    }

    public SimplePrice() {
    }

    public String getNUMBER_PRICE() {
        return NUMBER_PRICE;
    }

    public void setNUMBER_PRICE(String NUMBER_PRICE) {
        this.NUMBER_PRICE = NUMBER_PRICE;
    }

    public String getSTART_DATE() {
        return START_DATE;
    }

    public void setSTART_DATE(String START_DATE) {
        this.START_DATE = START_DATE;
    }

    public String getSTOP_DATE() {
        return STOP_DATE;
    }

    public void setSTOP_DATE(String STOP_DATE) {
        this.STOP_DATE = STOP_DATE;
    }

    public String getCONTENT() {
        return CONTENT;
    }

    public void setCONTENT(String CONTENT) {
        this.CONTENT = CONTENT;
    }

    public Long getEXEC() {
        return EXEC;
    }

    public void setEXEC(Long EXEC) {
        this.EXEC = EXEC;
    }

    public Long getPREVID() {
        return PREVID;
    }

    public void setPREVID(Long PREVID) {
        this.PREVID = PREVID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimplePrice that = (SimplePrice) o;

        if (!NUMBER_PRICE.equals(that.NUMBER_PRICE)) return false;
        if (!START_DATE.equals(that.START_DATE)) return false;
        if (!STOP_DATE.equals(that.STOP_DATE)) return false;
        if (!CONTENT.equals(that.CONTENT)) return false;
        if (!EXEC.equals(that.EXEC)) return false;
        return PREVID.equals(that.PREVID);

    }

    @Override
    public int hashCode() {
        int result = NUMBER_PRICE.hashCode();
        result = 31 * result + START_DATE.hashCode();
        result = 31 * result + STOP_DATE.hashCode();
        result = 31 * result + CONTENT.hashCode();
        result = 31 * result + EXEC.hashCode();
        result = 31 * result + PREVID.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SimplePrice{" +
                "NUMBER_PRICE='" + NUMBER_PRICE + '\'' +
                ", START_DATE='" + START_DATE + '\'' +
                ", STOP_DATE='" + STOP_DATE + '\'' +
                ", CONTENT='" + CONTENT + '\'' +
                ", EXEC=" + EXEC +
                ", PREVID=" + PREVID +
                '}';
    }
}
