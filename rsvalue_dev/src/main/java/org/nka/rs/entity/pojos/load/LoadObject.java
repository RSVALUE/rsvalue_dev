package org.nka.rs.entity.pojos.load;

import org.nka.rs.entity.dictionary.DecisionOrganizationDic;
import org.nka.rs.entity.dictionary.PolyTypeDic;
import org.nka.rs.entity.dictionary.ValueOrganizationDic;
import org.nka.rs.entity.pojos.common.Operation;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//Основной объект в регистре стоимости.
@Entity
@Table(name = "CO_GROUP_LOAD")
public class LoadObject {

    //номер загрузки
    @Id
    @Column(name = "REG_NUM", unique = true, nullable = false)
    private Long regNum;

    //номер решения (может иметь буквенный код)
    @Column(name = "DEC_NUM", nullable = false, length = 20)
    private String decNum;

    //организация, принявшая решение об оценке
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEC_ORG", nullable = false)
    private DecisionOrganizationDic decOrganization;

    //дата решения о кадастровой оценке
    @Temporal(TemporalType.DATE)
    @Column(name = "DEC_DATE", nullable = false)
    private Date decDate;

    //организация, проводившая оценку
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VAL_ORG", nullable = false)
    private ValueOrganizationDic valOrganization;

    //операция
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OOPER_ID", nullable = false)
    private Operation operation;

    @Column(name = "REMARK", length = 250)
    private String remark;

    //тип геометрии: точка, полигон, растр
    @ManyToOne
    @JoinColumn(name = "POLY_TYPE", nullable = false)
    private PolyTypeDic polyType;

    //набор объектов оценки, загружаемых одномоментно
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "loadObject")
    private List<CostObject> costObjectList;

    //спиоск документов-оснований для загрузки данных оценки
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "loadObject")
    private List<RegistrationDocument> registrationDocumentList;

    //список слоев
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "loadObject")
    private List<Layer> layerList;


    public LoadObject() {
    }

    public Long getRegNum() {
        return regNum;
    }

    public void setRegNum(Long regNum) {
        this.regNum = regNum;
    }

    public String getDecNum() {
        return decNum;
    }

    public void setDecNum(String decNum) {
        this.decNum = decNum;
    }

    public Date getDecDate() {
        return decDate;
    }

    public void setDecDate(Date decDate) {
        this.decDate = decDate;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public DecisionOrganizationDic getDecOrganization() {
        return decOrganization;
    }

    public void setDecOrganization(DecisionOrganizationDic decOrganization) {
        this.decOrganization = decOrganization;
    }

    public ValueOrganizationDic getValOrganization() {
        return valOrganization;
    }

    public void setValOrganization(ValueOrganizationDic valOrganization) {
        this.valOrganization = valOrganization;
    }

    public List<CostObject> getCostObjectList() {
        return costObjectList;
    }

    public void setCostObjectList(List<CostObject> costObjectList) {
        this.costObjectList = costObjectList;
    }

    public List<RegistrationDocument> getRegistrationDocumentList() {
        return registrationDocumentList;
    }

    public void setRegistrationDocumentList(List<RegistrationDocument> registrationDocumentList) {
        this.registrationDocumentList = registrationDocumentList;
    }

    public List<Layer> getLayerList() {
        return layerList;
    }

    public void setLayerList(List<Layer> layerList) {
        this.layerList = layerList;
    }
}
