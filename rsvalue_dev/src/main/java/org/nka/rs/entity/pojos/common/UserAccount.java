package org.nka.rs.entity.pojos.common;

import org.nka.rs.entity.dictionary.TORDic;

import javax.persistence.*;

/**
 * Created by zgurskaya on 21.09.2016.
 */

@Entity
@Table(name = "USER_ACCOUNTS")
public class UserAccount {

    //ИД пользователя
    @Id
    @Column(name = "ACCOUNT_ID", nullable = false, unique = true)
    private Long userId;

    //а это данные по организации, в которой работает субъект - код ТОРа
    @ManyToOne
    @JoinColumn(name = "REGCODE", nullable = false)
    private TORDic regCode;

    //профессия, должность
    @Column(name = "PROFESSION")
    private String profession;

    //Операция
    @OneToOne
    @JoinColumn(name = "OOPERID", nullable = false)
    private Operation operation;

    //логин
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID", nullable = false)
    private UserEntity userEntity;

    //это данные по самому субъекту в Subjects
    @ManyToOne
    @JoinColumn(name = "SUBJECT_ID", nullable = false)
    private Subject subject;


    public UserAccount() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public TORDic getRegCode() {
        return regCode;
    }

    public void setRegCode(TORDic regCode) {
        this.regCode = regCode;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "userId=" + userId +
                ", subject=" + subject +
                ", regCode=" + regCode +
                ", profession='" + profession + '\'' +
                ", userEntity=" + userEntity +
                ", operation=" + operation +
                '}';
    }
}
