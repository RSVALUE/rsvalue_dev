package org.nka.rs.entity.constant.messageConstant;

/**
 * Created by zgurskaya on 02.11.2016.
 */
public enum SearchMethodsEnum {

    CADNUM_SEARCH(1, ""),
    ADDRESS_POINT_SEARCH(2, ""),
    COMMUNITY_GARDEN_SEARCH(3, ""),
    LANDS_OUT_LOCALITY_SEARCH(4, ""),
    FARM_LANDS(5, "");

    private final String typeName;
    private final Integer typeValue;


    SearchMethodsEnum(int typeValue, String typeName) {
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public static SearchMethodsEnum searchByName(String typeName) {
        for (SearchMethodsEnum param : SearchMethodsEnum.values()) {
            if (typeName.equals(param.getTypeName())) {
                return param;
            }
        }
        return null;
    }
    public static SearchMethodsEnum searchByValue(Integer typeValue) {
        for (SearchMethodsEnum param : SearchMethodsEnum.values()) {
            if (typeValue.equals(param.getTypeValue())) {
                return param;
            }
        }
        return null;
    }

    public String getTypeName() {
        return typeName;
    }

    public int getTypeValue() {
        return typeValue;
    }
}
