package org.nka.rs.entity.pojos.load;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//оценочная зона
@Entity
@Table(name = "COST_ZONE")
public class CostZone {

    //ИД оценочной зоны
    @Id
    @Column(name = "ZONE_ID", unique = true, nullable = false)
    private Long zoneId;

    //Объект оценки
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "REGID", nullable = false)
    private CostObject costObject;

    //Площадь объекта оценки
    @Column(name = "SQUARE", precision = 6)
    private Double square;

    // Номер оценочной зоны
    @Column(name = "ZONENUMBER", nullable = false, length = 20)
    private String zoneNum;

    //цена в долларах за кв.м.
    @Column(name = "COST_D", precision = 6, nullable = false)
    private Double costD;

    //цена в беларуских рублях за кв.м.
    @Column(name = "COST_R", precision = 6, nullable = false)
    private Double costR;

    //налоговая база на начало года (на 01.01.yyyy)
    @Column(name = "COST_NB", precision = 6, nullable = false)
    private Double costNB;

    //вспомогательные поля для оценочной зоны
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "costZone")
    private CostZoneData costZoneData;

    //Список земельнх участков
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "costZone")
    private Set<Parcel> parcels;


    public CostZone() {
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public CostObject getCostObject() {
        return costObject;
    }

    public void setCostObject(CostObject costObject) {
        this.costObject = costObject;
    }

    public String getZoneNum() {
        return zoneNum;
    }

    public void setZoneNum(String zoneNum) {
        this.zoneNum = zoneNum;
    }

    public Double getCostD() {
        return costD;
    }

    public void setCostD(Double costD) {
        this.costD = costD;
    }

    public Double getCostR() {
        return costR;
    }

    public void setCostR(Double costR) {
        this.costR = costR;
    }

    public Double getCostNB() {
        return costNB;
    }

    public void setCostNB(Double costNB) {
        this.costNB = costNB;
    }

    public CostZoneData getCostZoneData() {
        return costZoneData;
    }

    public void setCostZoneData(CostZoneData costZoneData) {
        this.costZoneData = costZoneData;
    }

    public Set<Parcel> getParcels() {
        return parcels;
    }

    public void setParcels(Set<Parcel> parcels) {
        this.parcels = parcels;
    }

    public Double getSquare() {
        return square;
    }

    public void setSquare(Double square) {
        this.square = square;
    }

    }
