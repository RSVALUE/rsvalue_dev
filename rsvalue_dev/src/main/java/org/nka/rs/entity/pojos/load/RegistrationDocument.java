package org.nka.rs.entity.pojos.load;

import org.nka.rs.entity.dictionary.LoadDocumentTypeDic;
import org.nka.rs.entity.pojos.common.Operation;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//Документы загрузки
@Entity
@Table(name = "REGDOC")
public class RegistrationDocument {

    //ИД документа загрузки
    @Id
    @Column(name = "REGDOC_ID", unique = true, nullable = false)
    private Long redDocId;

    //файл-документ (документ-содержание)
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "DOCID", nullable = false)
    private DocumentContent documentContent;

    //объект загрузки
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "REG_NUM", nullable = false)
    private LoadObject loadObject;

    //тип документа загрузки
    @ManyToOne
    @JoinColumn(name = "LDOC_TYPE", nullable = false)
    private LoadDocumentTypeDic loadDocType;

    //Дата загрузки
    @Temporal(TemporalType.DATE)
    @Column(name = "LOAD_DATE", nullable = false)
    private Date loadDate;

    //Актуальность
    @Column(name = "INSTORAGE")
    private Boolean actual;

    //Операция
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "OOPER_ID", nullable = false)
    private Operation operation;

    public RegistrationDocument() {
    }

    public Long getRedDocId() {
        return redDocId;
    }

    public void setRedDocId(Long redDocId) {
        this.redDocId = redDocId;
    }

    public LoadObject getLoadObject() {
        return loadObject;
    }

    public void setLoadObject(LoadObject loadObject) {
        this.loadObject = loadObject;
    }

    public DocumentContent getDocumentContent() {
        return documentContent;
    }

    public void setDocumentContent(DocumentContent documentContent) {
        this.documentContent = documentContent;
    }

    public Date getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(Date loadDate) {
        this.loadDate = loadDate;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public LoadDocumentTypeDic getLoadDocType() {
        return loadDocType;
    }

    public void setLoadDocType(LoadDocumentTypeDic loadDocType) {
        this.loadDocType = loadDocType;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }
}
