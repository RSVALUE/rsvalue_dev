package org.nka.rs.entity.pojos.load;

import org.nka.rs.entity.dictionary.LayerTypeDic;
import org.nka.rs.entity.pojos.common.Operation;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//Слои
@Entity
@Table(name = "LAYERS")
public class Layer {

    //ИД слоя
    @Id
    @Column(name = "LAYER_ID", unique = true, nullable = false)
    private Long layerId;

    //связь с объектом загрузки
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "REG_NUM", nullable = false)
    private LoadObject loadObject;

    //Классификатор, тип слоя
    @ManyToOne
    @JoinColumn(name = "LAYER_TYPE", nullable = false)
    private LayerTypeDic layerType;

    //операция
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "OOPER_ID", nullable = false)
    private Operation operation;

    //дата загрузки слоя
    @Temporal(TemporalType.DATE)
    @Column(name = "LAYER_START_DATE", nullable = false)
    private Date layerStartDate;

    //дата прекращения слоя
    @Temporal(TemporalType.DATE)
    @Column(name = "LAYER_STOP_DATE")
    private Date layerStopDate;

    //Список дополнительных данных по слою
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "layer")
    private List<LayerData> layerDataList;



    public Layer() {
    }

    public Long getLayerId() {
        return layerId;
    }

    public void setLayerId(Long layerId) {
        this.layerId = layerId;
    }

    public LoadObject getLoadObject() {
        return loadObject;
    }

    public void setLoadObject(LoadObject loadObject) {
        this.loadObject = loadObject;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Date getLayerStartDate() {
        return layerStartDate;
    }

    public void setLayerStartDate(Date layerStartDate) {
        this.layerStartDate = layerStartDate;
    }

    public Date getLayerStopDate() {
        return layerStopDate;
    }

    public void setLayerStopDate(Date layerStopDate) {
        this.layerStopDate = layerStopDate;
    }

    public LayerTypeDic getLayerType() {
        return layerType;
    }

    public void setLayerType(LayerTypeDic layerType) {
        this.layerType = layerType;
    }

    public List<LayerData> getLayerDataList() {
        return layerDataList;
    }

    public void setLayerDataList(List<LayerData> layerDataList) {
        this.layerDataList = layerDataList;
    }
}
