package org.nka.rs.entity.constant.loadConstant;

/**
 * Created by zgurskaya on 22.06.2016.
 */
public enum CityFieldsEnum {

    OBJECTNUMBER("Objectnumb"),
    NUM("Num"),
    AREA("Area"),
    NAME("Name"),
    COST_D("Cost_d"),
    COST_R("Cost_r");

    private final String columnName;

    CityFieldsEnum(String columnName) {
        this.columnName = columnName;
    }

    public static CityFieldsEnum search(String columnName) {
        for (CityFieldsEnum param : CityFieldsEnum.values()) {
            if (columnName.equals(param.getColumnName())) {
                return param;
            }
        }
        return null;
    }

    public String getColumnName() {
        return columnName;
    }

    /*public HashMap<String, Boolean> getColumnFlag() {
        HashMap<String, Boolean> map = new HashMap<String, Boolean>();

        map.put("num", true);
        map.put("area", true);
        map.put("cost_d", true);
        map.put("cost_r", true);
        map.put("objectnumb", true);
        map.put("name", true);
        map.put("soato", false);
        map.put("category", false);
        map.put("ss", false);
        map.put("distr", false);
        map.put("stname", false);
        map.put("unp", false);
        map.put("npname", false);
        map.put("regn_ss", false);

        return map;
    }*/


}
