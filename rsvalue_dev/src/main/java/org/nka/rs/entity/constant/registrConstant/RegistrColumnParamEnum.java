package org.nka.rs.entity.constant.registrConstant;

/**
 * Created by zgurskaya on 20.06.2016.
 */
public enum RegistrColumnParamEnum {

    REG_NUM("lo.regNum ", " regNum ", 1),                         //регистрационный номер
    DATE_START("trunc(co.dateRegStart) ", " dateRegStart ", 2),   //дата загрузки
    DATE_VAL("trunc(co.dateVal) ", " dateVal ", 3),               //дата оценки
    PARENT_ATE_NAME("ate.ateName ", " ateName ", 4),              //город/район
    COST_TYPE_NAME("costType.codeName ", " costTypeName ", 5),    //вид оценки
    FUNC_CODE_NAME("funcCode.codeName ", " funcCodeName ", 6),    //функциональное назначение
    DEC_NUM("lo.decNum " , " decNum ", 7),                        //номер решения
    DEC_DATE("trunc(lo.decDate) ", " decDate ", 8),               //дата решения
    DEC_ORG("lo.decOrganization.codeName ", " decOrganization ", 9), //наименование исполнительного органа
    VAL_ORG("lo.valOrganization.codeName ", " valOrganization ", 10),//организация, выполнившая оценку
    METHOD_TYPE_NAME("method.codeName ", " methodTypeName ", 11),   //методика оценки
    ACTUAL("trunc(co.dateRegStop)", " dateRegStop ", 12);			//актуальность


    private final String queryName;
    private final String alias;
    private final Integer columnNumber;

    RegistrColumnParamEnum(String queryName, String alias, Integer columnNumber) {
        this.queryName = queryName;
        this.alias = alias;
        this.columnNumber = columnNumber;
    }

    public static RegistrColumnParamEnum search(Integer columnNumber) {
        for (RegistrColumnParamEnum param : RegistrColumnParamEnum.values()) {
            if (columnNumber.equals(param.getColumnNumber())) {
                return param;
            }
        }
        return null;
    }

    public String getQueryName() {
        return queryName;
    }

    public String getAlias() {
        return alias;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }



}
