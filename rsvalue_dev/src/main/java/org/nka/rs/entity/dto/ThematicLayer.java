package org.nka.rs.entity.dto;

import java.util.Date;

/**
 * Created by zgurskaya on 20.07.2016.
 */
public class ThematicLayer {

    private String name;
    private Date startDate;

    public ThematicLayer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
