package org.nka.rs.entity.constant.messageConstant;

/**
 * Created by zgurskaya on 02.11.2016.
 */
public interface MessageConstant {

    final String HEADER_1 = "ГОСУДАРСТВЕННЫЙ КОМИТЕТ ПО ИМУЩЕСТВУ \n" +
            " РЕСПУБЛИКИ БЕЛАРУСЬ";
    final String HEADER_2 = "ГУП «НАЦИОНАЛЬНОЕ КАДАСТРОВОЕ АГЕНСТВО»";
    final String HEADER_3 = "РЕГИСТР СТОИМОСТИ ЗЕМЕЛЬ, ЗЕМЕЛЬНЫХ УЧАСТКОВ \nГОСУДАРСТВЕННОГО ЗЕМЕЛЬНОГО КАДАСТРА";

    final String INFO_TABLE_NAME_CADASTR_SEARCH = "Сведения о земельном участке по данным единого государственного регистра " +
            "недвижимого имущества, прав на него и сделок с ним";
    final String INFO_TABLE_NAME_VNP_SEARCH = "Сведения о землях, расположенных за пределами населеннх пунктов, " +
            "садоводческих товариществ и дачного строительства";
    final String INFO_TABLE_NAME_COORD_SEARCH = "Сведения об объекте";
    final String INFO_TABLE_NAME_FARMLAND_SEARCH = "Сведения о землепользователе";
    final String INFO_TABLE_NAME_COMMUNITY_GARDEN_SEARCH = "Сведения о садоводческом товариществе";

    final String INFO_COLUMN_NAME_ADDRESS = "Адрес (местоположение)";
    final String INFO_COLUMN_NAME_CADNUM = "Кадастровый номер";
    final String INFO_COLUMN_NAME_SQUARE = "Площадь (га.)";
    final String INFO_COLUMN_NAME_PURPOSE_DIC = "Целевое назначение по единому классификатору назначений объектов недвижимого имущества";
    final String INFO_COLUMN_NAME_PURPOSE_GOV = "Целевое назначение по решению местного исполнительного комитета";
    final String INFO_COLUMN_NAME_GARDEN_NAME = "Наименование садоводческого товарищества";
    final String INFO_COLUMN_NAME_NEAR_LOCALITY = "Ближайший населенный пункт";
    final String INFO_COLUMN_NAME_TENANT_NAME = "Наименование землепользователя";
    final String INFO_COLUMN_NAME_UNP = "Учетный номер плательщика (землепользователя)";
    final String INFO_COLUMN_NAME_COST_ZONE_NAME = "Наименование оценочной зоны";

    final String TABLE_NAME_COST = "Стоимостные показатели";
    final String COST_COLUMN_NAME_1 = "Вид функционального использования земель";
    final String COST_COLUMN_NAME_2 = "Дата оценки";
    final String COST_COLUMN_NAME_3 = "Номера оценочных зоны";
    final String COST_COLUMN_NAME_4 = "Кадастровая стоимость 1 кв. м. земель оценочных зон на ";
    final String COST_COLUMN_NAME_4_FARM_LAND = "Кадастровая стоимость 1 га. сельскохозяйственных земель в оценочных зон на ";
    final String COST_COLUMN_NAME_4_2 = "";
    final String COST_COLUMN_NAME_5 = "долл. США";
    final String COST_COLUMN_NAME_6 = "руб.<*>";
    final String COST_COLUMN_NAME_7_1 = "Кадастровая стоимость 1 кв.м земель на 1 января ";
    final String COST_COLUMN_NAME_7_2 = " года для исчисления налоговой базы земельного налога, рублей <*>";

    final String FOUNDATION = "ОСНОВАНИЕ:";

    final String REMARK_2017_1 = "<*> По курсу доллара США, установленному Национальным банком Республики Беларусь " +
            "по состоянию на 01 января 2016, ";
    final String REMARK_2017_2 = "с применением прогнозного индекса роста потребительских цен. Для вида " +
            "функционального использования";
    final String REMARK_ = "<*> По курсу доллара США, установленному Национальным банком Республики Беларусь " +
            "по состоянию на дату ";
    final String REMARK_2 = "кадастровой оценки земель";
    final String REMARK_2017_3 = "«Жилая многокваритирная зона» в белорусских рублях на дату кадастровой оценки";
    final String REMARK_2012_1 = "<*> По курсу доллара США, установленному Национальным банком Республики Беларусь " +
            "по состоянию на 01 января 2011,";
    final String REMARK_2012_2 = " с применением коэффициента 2,15, учитывающего уровень инфляции за 2011 год";
    final String REMARK_ALL = "<*> По курсу доллара США, установленному Национальным банком Республики Беларусь " +
            "по состоянию на 1 января ";


}
