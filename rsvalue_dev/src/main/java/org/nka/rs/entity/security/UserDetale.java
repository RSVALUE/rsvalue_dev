package org.nka.rs.entity.security;

/**
 * Created by Shcherbina on 26.11.2016.
 */
public class UserDetale {
    private int userId;
    private String name;
    private String surname;
    private String fathername;
    private String login;
    private int group;
    private int torId;
    private int actual;
    private int enabled;
    private int count;

    public UserDetale() {
    }

    public UserDetale(int userId, String name, String surname, String fathername, String login, int group, int torId, int actual, int enabled, int count) {
        this.userId = userId;
        this.name = name;
        this.surname = surname;
        this.fathername = fathername;
        this.login = login;
        this.group = group;
        this.torId = torId;
        this.actual = actual;
        this.enabled = enabled;
        this.count = count;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getTorId() {
        return torId;
    }

    public void setTorId(int torId) {
        this.torId = torId;
    }

    public int getActual() {
        return actual;
    }

    public void setActual(int actual) {
        this.actual = actual;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDetale that = (UserDetale) o;

        if (userId != that.userId) return false;
        if (group != that.group) return false;
        if (torId != that.torId) return false;
        if (actual != that.actual) return false;
        if (enabled != that.enabled) return false;
        if (count != that.count) return false;
        if (!name.equals(that.name)) return false;
        if (!surname.equals(that.surname)) return false;
        if (!fathername.equals(that.fathername)) return false;
        return login.equals(that.login);

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + fathername.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + group;
        result = 31 * result + torId;
        result = 31 * result + actual;
        result = 31 * result + enabled;
        result = 31 * result + count;
        return result;
    }

    @Override
    public String toString() {
        return "UserDetale{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", fathername='" + fathername + '\'' +
                ", login='" + login + '\'' +
                ", group=" + group +
                ", torId=" + torId +
                ", actual=" + actual +
                ", enabled=" + enabled +
                ", count=" + count +
                '}';
    }
}
