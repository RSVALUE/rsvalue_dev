package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 09.06.2016.
 */
public class LayerType {

    private String layerName;
    private String layerTypeName;
    private Integer analyticCode;

    public LayerType() {
    }

    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

    public String getLayerTypeName() {
        return layerTypeName;
    }

    public void setLayerTypeName(String layerTypeName) {
        this.layerTypeName = layerTypeName;
    }

    public Integer getAnalyticCode() {
        return analyticCode;
    }

    public void setAnalyticCode(Integer analyticCode) {
        this.analyticCode = analyticCode;
    }
}
