package org.nka.rs.entity.dictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 07.10.2016.
 */
@Entity
@Table(name = "X_DOC_OUT")

public class OrderingDocumentTypeDic {

    @Id
    @Column(name = "ID_CODE", nullable = false)
    private Integer analyticCode;

    //русское наименование
    @Column(name = "CODE_NAME")
    private String codeName;

    @Column(name = "CODE_SHORTNAME")
    private String codeShortName;

    //родительский (верхний код)
    @Column(name = "PARENT_CODE")
    private Integer parentCode;

    //родительский (верхний код)
    @Column(name = "REF_CODE")
    private Integer referenceCode;

    //актуальность кода классификатора
    @Column(name = "STATUS", nullable = false)
    private Boolean status;

    public OrderingDocumentTypeDic() {
    }

    public Integer getAnalyticCode() {
        return analyticCode;
    }

    public void setAnalyticCode(Integer analyticCode) {
        this.analyticCode = analyticCode;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeShortName() {
        return codeShortName;
    }

    public void setCodeShortName(String codeShortName) {
        this.codeShortName = codeShortName;
    }

    public Integer getParentCode() {
        return parentCode;
    }

    public void setParentCode(Integer parentCode) {
        this.parentCode = parentCode;
    }

    public Integer getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(Integer referenceCode) {
        this.referenceCode = referenceCode;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
