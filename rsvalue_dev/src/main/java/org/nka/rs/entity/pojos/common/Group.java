package org.nka.rs.entity.pojos.common;

import javax.persistence.*;

@Entity
@Table(name = "GROUPS")
public class Group {

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    private int groupId;

    @Column(name = "GROUP_NAME", nullable = false, unique = true)
    private String groupName;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int id) {
        this.groupId = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String name) {
        this.groupName = name;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupId=" + groupId +
                ", groupName='" + groupName + '\'' +
                '}';
    }
}
