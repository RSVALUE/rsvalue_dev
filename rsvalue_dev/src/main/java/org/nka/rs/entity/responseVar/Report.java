package org.nka.rs.entity.responseVar;

/**
 * Created by zgurskaya on 13.07.2016.
 */


//отчет по загрузке
public class Report {

    //номер загрузки
    private Long regNum;
    //регистрационный номер населенного пункта
    private String ateName;
    //наименование вида оценки
    private String costTypeName;
    //наименование функционального назначения
    private String funcCodeName;
    //количество записей Zones
    private Integer zonesQuantity;
    //количество записей Bounds
    private Integer boundsQuantity;
    //количество записей Layers
    private Integer layersQuantity;
    //количество загружаемых документов
    private Integer docQuantity;
    //количество деактуализированных записей
    private Integer decObjectQuantity;
    //количество СНП в шейпе
    private Integer shapeObjectQuantity;
    //количество СНП в базе по заданному району
    private Integer baseObjectQuantity;

    public Report() {
    }

    public Long getRegNum() {
        return regNum;
    }

    public void setRegNum(Long regNum) {
        this.regNum = regNum;
    }

    public String getAteName() {
        return ateName;
    }

    public void setAteName(String ateName) {
        this.ateName = ateName;
    }

    public String getCostTypeName() {
        return costTypeName;
    }

    public void setCostTypeName(String costTypeName) {
        this.costTypeName = costTypeName;
    }

    public String getFuncCodeName() {
        return funcCodeName;
    }

    public void setFuncCodeName(String funcCodeName) {
        this.funcCodeName = funcCodeName;
    }

    public Integer getZonesQuantity() {
        return zonesQuantity;
    }

    public void setZonesQuantity(Integer zonesQuantity) {
        this.zonesQuantity = zonesQuantity;
    }

    public Integer getBoundsQuantity() {
        return boundsQuantity;
    }

    public void setBoundsQuantity(Integer boundsQuantity) {
        this.boundsQuantity = boundsQuantity;
    }

    public Integer getLayersQuantity() {
        return layersQuantity;
    }

    public void setLayersQuantity(Integer layersQuantity) {
        this.layersQuantity = layersQuantity;
    }

    public Integer getDocQuantity() {
        return docQuantity;
    }

    public void setDocQuantity(Integer docQuantity) {
        this.docQuantity = docQuantity;
    }

    public Integer getDecObjectQuantity() {
        return decObjectQuantity;
    }

    public void setDecObjectQuantity(Integer decObjectQuantity) {
        this.decObjectQuantity = decObjectQuantity;
    }

    public Integer getShapeObjectQuantity() {
        return shapeObjectQuantity;
    }

    public void setShapeObjectQuantity(Integer shapeObjectQuantity) {
        this.shapeObjectQuantity = shapeObjectQuantity;
    }

    public Integer getBaseObjectQuantity() {
        return baseObjectQuantity;
    }

    public void setBaseObjectQuantity(Integer baseObjectQuantity) {
        this.baseObjectQuantity = baseObjectQuantity;
    }
}
