package org.nka.rs.entity.constant.commonConstant;

import java.util.ResourceBundle;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface IConstant {

    ResourceBundle BUNDLE = ResourceBundle.getBundle("hibernate");

    //life directory
    //public static final String directory = "//ncastorage2/RegStoimosti";
    //test directory
    String directory = "\\\\ncastorage2\\RegStoimostiTEST\\PDF_DOCUMENTS";


    //life schema
    //String SCHEMA_NAME = "RS_VALUE";
    //test schema
    String SCHEMA_NAME = BUNDLE.getString("hibernate.default_schema");

    //адрес нацбанка РБ для получения данных по валютам
    String NBRB_URL = "http://www.nbrb.by/API/ExRates/Rates/";
    //ИД евро
    Integer EURO_ID = 292;
    //ИД доллара
    Integer USD_ID = 145;

    Integer CURRENCY_BEL_DEFAULT = 974;


    Integer allowableFailureCounts = 3;
    Integer attemptAuthInterval = 2;
    Integer banInterval = 20;

    //значение при котором группировка данных в журнале не происходит
    Integer FALSE_GROUPING = 0;
    //дефолтное значение страницы в журнале загрузок
    Integer PAGE_DEFAULT = 1;
    //дефолтное значение для сортировки по убыванию/возрастанию
    String PREDICATE_DEFAULT = "DESC";

    Integer SORTING_DEFAULT = 1;
    //дефолтное значение для вывода строк количество на странице)
    //в журнале загрузок
    Integer AMOUNT_DEFAULT = 10;
    //в просмотре оценочных зон
    Integer ZONE_AMOUNT_DEFAULT = 50;
    //дефолтная настройка таблицы для просмотра оценочных зон
    Integer [] DEFAULT_ZONE_COLUMNS = {1, 2, 3, 4, 5};
    //дефолтная настройка таблиц для просмотра журнала загрузок
    Integer [] DEFAULT_REGISTR_COLUMNS = {1, 2, 3, 4, 5, 6, 7, 8, 9, 12};


    //ате минского района
    Long ATE_MINSK_REGION = 19824L;

    Boolean RELEVANT = true;
    Boolean NOT_RELEVANT = false;

    //типы геометрии
    Integer POINT = 10;
    Integer POLYGON = 20;
    Integer RASTR = 30;

    //методики оценки земель
    Integer NON_FARM_METH = 100;
    Integer METHOD_2003_2010 = 101;
    Integer METHOD_2010_2015 = 102;
    Integer METHOD_2016 = 103;
    Integer FARM_METH = 200;
    Integer FARM_METH_1 = 201;

    //виды кадастровой оценки
    Integer CITY_AND_PGT = 10;
    Integer RURAL_LOCALITY = 20;
    Integer COMMUNITY_GARDEN = 30;
    Integer LANDS_OUT_LOCALITY = 40;
    Integer FARM_LANDS = 50;
    Integer PARCEL = 60;

    //виды функционального использования
    Integer NON_FARM_PURPOSE = 100;
    Integer LIVING_APARTMENT_ZONE = 110;
    Integer LIVING_MANOR_ZONE = 120;
    Integer LIVING_MANOR_ZONE_FARMSTEAD_TYPE = 130;
    Integer LIVING_MANOR_ZONE_COTTAGE_TYPE = 140;
    Integer RECREATION_ZONE = 150;
    Integer BUSINESS_ZONE = 160;
    Integer PRODUCTION_ZONE = 170;
    Integer GARDENING_ASSOTIATIONS = 180;
    Integer AGRICULTURE_PURPOSE = 200;
    Integer ARABLE_LAND = 210;
    Integer IMPROVED_MEADOW_LAND = 220;
    Integer NATURAL_MEADOW_LAND = 230;

    //категории АТЕ для поиска
    Integer REGION = 100;
    Integer AREA = 200;
    Integer VILLAGE_COUNCIL = 300;
    Integer CITY = 400;
    Integer COUNTRY_LOCALITY = 500;
    Integer OTHER = 600;
/*
    public static final Integer IRRELEVANT = -1;
*/

    //виды документов для регистрации объектов оценки
    Integer CORE_DOCUMENTS_FOR_VALUE = 1000;
    Integer CORE_DOCUMENTS = 1100;
    Integer COPY_DECISION = 1101;
    Integer DECREE = 1102;
    Integer CONCLUSION_CADASTRAL_VALUE = 1103;
    Integer OTHER_DOCUMENT = 1104;
    Integer REPORT_DOCUMENTS = 1200;
    Integer COMMON_REPORT = 1201;
    Integer SCHEMES_EVALUATING_ZONING = 1202;
    Integer THEMATIC_LAYERS_INITIAL_INFO_RASTR = 1203;
    Integer TABLES_CADASTRAL_VALUE_COST_ZONES = 1204;
    Integer TABLES_CADASTRAL_VALUE_LAND = 1205;
    Integer COST_ZONES = 1206;
    Integer THEMATIC_LAYERS_INITIAL_INFO = 1207;
    Integer COST_PARCELS = 1208;
    Integer COSTING = 1209;
    Integer OTHER_REPORT_DOCUMENTS = 1210;
    Integer LOAD_SUCCESS_DOCUMENTS = 2000;
    Integer LOAD_SUCCESS_DOCUMENTS_BELOW = 2100;
    Integer NOTICE_ABOUT_SUCCESS = 2110;
    Integer ZONES_DOC = 1310;
    Integer BOUNDS_DOC = 1311;
    Integer LAYERS_DOC = 1312;


    //ВИДЫ ОПЕРАЦИЙ
    Integer COST_OBJECT_OPERATION = 1000;
    Integer COST_OBJECT_ADD = 1100;
    Integer COST_OBJECT_EDIT = 1200;
    Integer COST_OBJECT_STOP = 1300;
    Integer PRICE_DEACT = 3300;

    //ТЕМАТИЧЕСКИЕ СЛОИ
    Integer GRANIZA = 10;
    Integer CENTER = 20;
    Integer DOSTTRANSP = 30;
    Integer GAS = 40;
    Integer WATER = 50;
    Integer SEWERAGE = 60;
    Integer HEAT = 70;
    Integer PARK = 80;
    Integer REKREATION = 90;
    Integer PROM_RW = 100;
    Integer SZZ_WORK = 110;
    Integer GRUNT = 120;
    Integer PRIRODOHR = 130;
    Integer WATEROHR = 140;
    Integer IKZ = 150;
    Integer PRAWA_ZASTR = 160;
    Integer ZAPOVED_PRIROD = 170;
    Integer MICRO_CENTER = 180;
    Integer HARD_GRUNT = 190;
    Integer OZDOROV = 200;
    Integer AIR_POLLUT = 210;
    Integer EL_MAGNIT = 220;
    Integer PLOTNOST = 230;
    Integer NOISE_LEVEL = 240;
    Integer RADIO_ZAGR = 250;
    Integer UKLON = 260;
    Integer PAWODOK = 270;
    Integer U_WATER = 280;
    Integer ASFALT = 290;
    Integer HUTOR = 300;

    //типы субъектов
    Integer LEGAL_ENTITY = 10;
    Integer INDIVIDUAL = 20;
    Integer NOTARY = 30;
    Integer SYSTEM_USER = 40;

    //виды исходящих документов
    Integer COST_ZONE_SCHEMA = 2020;


}
