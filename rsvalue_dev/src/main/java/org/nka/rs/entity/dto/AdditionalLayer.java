package org.nka.rs.entity.dto;


/**
 * Created by zgurskaya on 13.06.2016.
 */
public class AdditionalLayer {

    //идентификатор типа слоя
    private Integer analyticCode;
    // для грунта и доступности цента = layerSpec
    private Object [] code;

    public AdditionalLayer() {
    }

    public Integer getAnalyticCode() {
        return analyticCode;
    }

    public void setAnalyticCode(Integer analyticCode) {
        this.analyticCode = analyticCode;
    }

    public Object [] getCode() {
        return code;
    }

    public void setCode(Object [] code) {
        this.code = code;
    }
}

