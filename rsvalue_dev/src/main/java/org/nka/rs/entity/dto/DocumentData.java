package org.nka.rs.entity.dto;

import java.util.Date;

/**
 * Created by zgurskaya on 20.07.2016.
 */
public class DocumentData {

    private Long id;
    private Boolean isNeed;
    private Integer typeId;
    private String typeName;
    private String name;
    private Double size;
    private Date loadDate;
    private String path;


    public DocumentData() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Date getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(Date loadDate) {
        this.loadDate = loadDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getNeed() {
        return isNeed;
    }

    public void setNeed(Boolean need) {
        isNeed = need;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }
}
