package org.nka.rs.entity.pojos.price;

/**
 * Created by Shcherbina on 22.11.2016.
 */
public class PriceEntity {
    private Long subj_id;
    private String subname;
    private Long docid;
    private String docname;
    private float urg;
    private float ndsurg;
    private float nonurg;
    private float nonndsurg;

    private float sum_urg;
    private float sum_nonurg;

    public PriceEntity() {
    }

    public PriceEntity(Long subj_id, String subname, Long docid, String docname, float urg, float ndsurg, float nonurg, float nonndsurg, float sum_urg, float sum_nonurg) {
        this.subj_id = subj_id;
        this.subname = subname;
        this.docid = docid;
        this.docname = docname;
        this.urg = urg;
        this.ndsurg = ndsurg;
        this.nonurg = nonurg;
        this.nonndsurg = nonndsurg;
        this.sum_urg = sum_urg;
        this.sum_nonurg = sum_nonurg;
    }

    @Override
    public String toString() {
        return "PriceEntity{" +
                "subj_id=" + subj_id +
                ", subname='" + subname + '\'' +
                ", docid=" + docid +
                ", docname='" + docname + '\'' +
                ", urg=" + urg +
                ", ndsurg=" + ndsurg +
                ", nonurg=" + nonurg +
                ", nonndsurg=" + nonndsurg +
                ", sum_urg=" + sum_urg +
                ", sum_nonurg=" + sum_nonurg +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PriceEntity that = (PriceEntity) o;

        if (Float.compare(that.urg, urg) != 0) return false;
        if (Float.compare(that.ndsurg, ndsurg) != 0) return false;
        if (Float.compare(that.nonurg, nonurg) != 0) return false;
        if (Float.compare(that.nonndsurg, nonndsurg) != 0) return false;
        if (Float.compare(that.sum_urg, sum_urg) != 0) return false;
        if (Float.compare(that.sum_nonurg, sum_nonurg) != 0) return false;
        if (!subj_id.equals(that.subj_id)) return false;
        if (!subname.equals(that.subname)) return false;
        if (!docid.equals(that.docid)) return false;
        return docname.equals(that.docname);

    }

    @Override
    public int hashCode() {
        int result = subj_id.hashCode();
        result = 31 * result + subname.hashCode();
        result = 31 * result + docid.hashCode();
        result = 31 * result + docname.hashCode();
        result = 31 * result + (urg != +0.0f ? Float.floatToIntBits(urg) : 0);
        result = 31 * result + (ndsurg != +0.0f ? Float.floatToIntBits(ndsurg) : 0);
        result = 31 * result + (nonurg != +0.0f ? Float.floatToIntBits(nonurg) : 0);
        result = 31 * result + (nonndsurg != +0.0f ? Float.floatToIntBits(nonndsurg) : 0);
        result = 31 * result + (sum_urg != +0.0f ? Float.floatToIntBits(sum_urg) : 0);
        result = 31 * result + (sum_nonurg != +0.0f ? Float.floatToIntBits(sum_nonurg) : 0);
        return result;
    }

    public Long getSubj_id() {
        return subj_id;
    }

    public void setSubj_id(Long subj_id) {
        this.subj_id = subj_id;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    public Long getDocid() {
        return docid;
    }

    public void setDocid(Long docid) {
        this.docid = docid;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public float getUrg() {
        return urg;
    }

    public void setUrg(float urg) {
        this.urg = urg;
    }

    public float getNdsurg() {
        return ndsurg;
    }

    public void setNdsurg(float ndsurg) {
        this.ndsurg = ndsurg;
    }

    public float getNonurg() {
        return nonurg;
    }

    public void setNonurg(float nonurg) {
        this.nonurg = nonurg;
    }

    public float getNonndsurg() {
        return nonndsurg;
    }

    public void setNonndsurg(float nonndsurg) {
        this.nonndsurg = nonndsurg;
    }

    public float getSum_urg() {
        return sum_urg;
    }

    public void setSum_urg(float sum_urg) {
        this.sum_urg = sum_urg;
    }

    public float getSum_nonurg() {
        return sum_nonurg;
    }

    public void setSum_nonurg(float sum_nonurg) {
        this.sum_nonurg = sum_nonurg;
    }
}
