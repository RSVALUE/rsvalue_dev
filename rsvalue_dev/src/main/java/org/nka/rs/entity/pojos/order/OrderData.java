package org.nka.rs.entity.pojos.order;

/**
 * Created by zgurskaya on 28.10.2016.
 */
public class OrderData {

    private Long orderId;

    private Long userId;

    private String orderNumber;

    private Integer urgency;

    private Integer isNb;

    private Integer declType;

    private String declName;

    private String unp;

    private String nkName;

    private String phoneNumber;

    private String email;

    private String address;

    private String index;

    private String ate;

    private String numIsx;

    private String dateIsx;

    private String numVx;

    private String dateVx;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getUrgency() {
        return urgency;
    }

    public void setUrgency(Integer urgency) {
        this.urgency = urgency;
    }

    public Integer getIsNb() {
        return isNb;
    }

    public void setIsNb(Integer isNb) {
        this.isNb = isNb;
    }

    public Integer getDeclType() {
        return declType;
    }

    public void setDeclType(Integer declType) {
        this.declType = declType;
    }

    public String getDeclName() {
        return declName;
    }

    public void setDeclName(String declName) {
        this.declName = declName;
    }

    public String getUnp() {
        return unp;
    }

    public void setUnp(String unp) {
        this.unp = unp;
    }

    public String getNkName() {
        return nkName;
    }

    public void setNkName(String nkName) {
        this.nkName = nkName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getAte() {
        return ate;
    }

    public void setAte(String ate) {
        this.ate = ate;
    }

    public String getNumIsx() {
        return numIsx;
    }

    public void setNumIsx(String numIsx) {
        this.numIsx = numIsx;
    }

    public String getDateIsx() {
        return dateIsx;
    }

    public void setDateIsx(String dateIsx) {
        this.dateIsx = dateIsx;
    }

    public String getNumVx() {
        return numVx;
    }

    public void setNumVx(String numVx) {
        this.numVx = numVx;
    }

    public String getDateVx() {
        return dateVx;
    }

    public void setDateVx(String dateVx) {
        this.dateVx = dateVx;
    }
}
