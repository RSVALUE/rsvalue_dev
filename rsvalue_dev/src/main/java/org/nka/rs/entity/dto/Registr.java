package org.nka.rs.entity.dto;

import java.util.Date;

/**
 * Created by zgurskaya on 07.06.2016.
 */

//dto для журнала загрузок
public class Registr {

    //номер загрузки
    private Long regNum;
    //дата загрузки
    private Date dateRegStart;
    //дата оценки
    private Date dateVal;
    //регистрационный номер населенного пункта
    private String ateName;
    //наименование вида оценки
    private String costTypeName;
    //номер решения об оценке
    private String decNum;
    //дата решения об оценке
    private Date decDate;
    //наименование организации, принявшей решение об оценке
    private String decOrganization;
    //наименование организации, проводившей оценку
    private String valOrganization;
    //дата деактуализации загрузки
    private Date dateRegStop;
    //функциональное назначение
    private String funcCodeName;
    //методика оценки
    private String methodTypeName;
    //актуальность
    private Boolean actual;
    //примечание
    private String remark;
    //название национальной валюты
    private String currencyName;


    public Registr() {
    }

    public Long getRegNum() {
        return regNum;
    }

    public void setRegNum(Long regNum) {
        this.regNum = regNum;
    }

    public Date getDateRegStart() {
        return dateRegStart;
    }

    public void setDateRegStart(Date dateRegStart) {
        this.dateRegStart = dateRegStart;
    }

    public Date getDateVal() {
        return dateVal;
    }

    public void setDateVal(Date dateVal) {
        this.dateVal = dateVal;
    }

    public String getAteName() {
        return ateName;
    }

    public void setAteName(String ateName) {
        this.ateName = ateName;
    }

    public String getCostTypeName() {
        return costTypeName;
    }

    public void setCostTypeName(String costTypeName) {
        this.costTypeName = costTypeName;
    }

    public String getDecNum() {
        return decNum;
    }

    public void setDecNum(String decNum) {
        this.decNum = decNum;
    }

    public Date getDecDate() {
        return decDate;
    }

    public void setDecDate(Date decDate) {
        this.decDate = decDate;
    }

    public String getDecOrganization() {
        return decOrganization;
    }

    public void setDecOrganization(String decOrganization) {
        this.decOrganization = decOrganization;
    }

    public String getValOrganization() {
        return valOrganization;
    }

    public void setValOrganization(String valOrganization) {
        this.valOrganization = valOrganization;
    }

    public Date getDateRegStop() {
        return dateRegStop;
    }

    public void setDateRegStop(Date dateRegStop) {
        this.dateRegStop = dateRegStop;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public String getFuncCodeName() {
        return funcCodeName;
    }

    public void setFuncCodeName(String funcCodeName) {
        this.funcCodeName = funcCodeName;
    }

    public String getMethodTypeName() {
        return methodTypeName;
    }

    public void setMethodTypeName(String methodTypeName) {
        this.methodTypeName = methodTypeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

}
