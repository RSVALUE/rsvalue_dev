package org.nka.rs.entity.security;

import org.nka.rs.util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Created by zgurskaya on 23.09.2016.
 */
public class RestHttpServletWrapper extends HttpServletRequestWrapper {

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";


    public RestHttpServletWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(String var1){
        String value;
        /*if (var1.equals(PASSWORD)){
            value = super.getHeader(PASSWORD);
            if(value != null) {
                value = Util.get_SHA_512_SecurePassword(super.getHeader(var1), "");
            }
        } else if (var1.equals(USERNAME)){
            value = super.getHeader(var1);
        } else {
            value = super.getParameter(var1);
        }
        return value;*/
        if (var1.equals(PASSWORD)){
            value = super.getParameter(PASSWORD);
            if(value != null) {
                value = Util.get_SHA_512_SecurePassword(super.getParameter(var1), "");
            }
        } else {
            value = super.getParameter(var1);
        }
        return value;
    }
}
