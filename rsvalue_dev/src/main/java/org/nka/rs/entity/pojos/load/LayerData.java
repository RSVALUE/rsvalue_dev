package org.nka.rs.entity.pojos.load;

import javax.persistence.*;

/**
 * Created by zgurskaya on 08.06.2016.
 */

@Entity
@Table(name = "LAYER_DATA")
public class LayerData {

    //ИД части слоя
    @Id
    @Column(name = "LAYER_DATA_ID", unique = true, nullable = false)
    private Long layerDataId;

    //ИД слоя
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LAYER_ID", nullable = false)
    private Layer layer;

    @Column(name = "LAYER_SPEC")
    private Integer param;

    public LayerData() {
    }

    public Long getLayerDataId() {
        return layerDataId;
    }

    public void setLayerDataId(Long layerDataId) {
        this.layerDataId = layerDataId;
    }

    public Layer getLayer() {
        return layer;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
    }

    public Integer getParam() {
        return param;
    }

    public void setParam(Integer param) {
        this.param = param;
    }
}
