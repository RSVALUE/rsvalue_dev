package org.nka.rs.entity.pojos.common;

/**
 * Класс, в котором содержатся сведения о пользователе (приходит с FE)
 */

public class UserData {

    private long id;

    private String fio;

    private String username;

    private String password;

    private String position;

    private int organization;

    private int group;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getOrganization() {
        return organization;
    }

    public void setOrganization(int organization) {
        this.organization = organization;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", position='" + position + '\'' +
                ", organization='" + organization + '\'' +
                ", group='" + group + '\'' +
                '}';
    }
}
