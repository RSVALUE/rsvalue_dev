package org.nka.rs.entity.responseVar;

/**
 * Created by zgurskaya on 06.07.2016.
 */
public class Response {

    private String message;
    private int code;

    public Response() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
