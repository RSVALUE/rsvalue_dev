package org.nka.rs.entity.dictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//тип геометрии: точка, полигон, растр
@Entity
@Table(name = "X_GEO")
public class PolyTypeDic {
    @Id
    @Column(name = "ID_CODE", nullable = false)
    private Integer analyticCode;

    //русское наименование
    @Column(name = "CODE_NAME")
    private String codeName;

    //актуальность кода классификатора
    @Column(name = "STATUS", nullable = false)
    private Boolean status;

    public PolyTypeDic() {
    }

    public Integer getAnalyticCode() {
        return analyticCode;
    }

    public void setAnalyticCode(Integer analyticCode) {
        this.analyticCode = analyticCode;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }


}
