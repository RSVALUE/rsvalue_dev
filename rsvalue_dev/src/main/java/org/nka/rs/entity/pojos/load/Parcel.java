package org.nka.rs.entity.pojos.load;

import org.nka.rs.entity.pojos.load.CostZone;

import javax.persistence.*;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//Земельный участок
@Entity
@Table(name = "PARCELS")
public class Parcel {

    //ИД земельного участка
    @Id
    @Column(name = "PARCEL_ID", unique = true, nullable = false)
    private Long parcelId;

    //оценочная зона
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ZONEID", nullable = false)
    private CostZone costZone;

    //кадастровый номер земельного участка
    @Column(name = "CADNUMN", nullable = false)
    private String cadastrialNum;

    //площадь земельного участка
    @Column(name = "SQUARE", precision = 6)
    private Double square;

    //адрес земельного участка
    @Column(name = "ADDRESS")
    private String address;

    //назначение земельного участка - не классификатор, текстовое поле
    @Column(name = "PURPOSE", nullable = false)
    private String purpose;

    //назначение земельного участка по исполкому или другому гос. органу - не классификатор, текстовое поле
    @Column(name = "PURPOSE_GOV", nullable = false)
    private String purposeGov;

    //цена в долларах за кв.м.
    @Column(name = "COST_D", precision = 6, nullable = false)
    private Double costD;

    //цена в беларуских рублях за кв.м.
    @Column(name = "COST_R", precision = 6, nullable = false)
    private Double costR;


    public Parcel() {
    }

    public Long getParcelId() {
        return parcelId;
    }

    public void setParcelId(Long parcelId) {
        this.parcelId = parcelId;
    }

    public CostZone getCostZone() {
        return costZone;
    }

    public void setCostZone(CostZone costZone) {
        this.costZone = costZone;
    }

    public String getCadastrialNum() {
        return cadastrialNum;
    }

    public void setCadastrialNum(String cadastrialNum) {
        this.cadastrialNum = cadastrialNum;
    }

    public Double getSquare() {
        return square;
    }

    public void setSquare(Double square) {
        this.square = square;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurposeGov() {
        return purposeGov;
    }

    public void setPurposeGov(String purposeGov) {
        this.purposeGov = purposeGov;
    }

    public Double getCostD() {
        return costD;
    }

    public void setCostD(Double costD) {
        this.costD = costD;
    }

    public Double getCostR() {
        return costR;
    }

    public void setCostR(Double costR) {
        this.costR = costR;
    }

}
