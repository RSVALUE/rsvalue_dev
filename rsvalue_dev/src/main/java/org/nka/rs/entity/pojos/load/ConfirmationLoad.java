package org.nka.rs.entity.pojos.load;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 28.07.2016.
 */

//Класс для подтверждения окончания загрузки

@Entity
@Table(name = "LOAD_INF")
public class ConfirmationLoad {

    @Id
    @Column(name = "LOAD_ID", nullable = false, unique = true)
    private Long loadId;

    @Column(name = "REG_NUM")
    private Long regNum;

    @Column(name = "DWD_ID")
    private Long dwdId;

    @Column(name = "STATUS")
    private Boolean status;

    @Column(name = "STORAGE_DEL")
    private String pathesDel;

    public ConfirmationLoad() {
    }

    public Long getLoadId() {
        return loadId;
    }

    public void setLoadId(Long loadId) {
        this.loadId = loadId;
    }

    public Long getRegNum() {
        return regNum;
    }

    public void setRegNum(Long regNum) {
        this.regNum = regNum;
    }

    public Long getDwdId() {
        return dwdId;
    }

    public void setDwdId(Long dwdId) {
        this.dwdId = dwdId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getPathesDel() {
        return pathesDel;
    }

    public void setPathesDel(String pathesDel) {
        this.pathesDel = pathesDel;
    }
}
