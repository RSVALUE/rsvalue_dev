package org.nka.rs.entity.pojos.docs;

import java.util.List;

/**
 * Класс, в котором содержатся данные по счет-акту
 */
public class ActInfo {

    private String actType;

    private String actName;

    private String actDate;

    private String subjName;

    private String cadOrg;

    private String execPost;

    private String execFullName;

    private String dateIn;

    private String numberIn;

    private List<String> objTypeList;

    private List<String> objAddressList;

    private List<String> objCadNumList;

    private String sum;

    private String sumString;

    private String ndsSum;

    private String ndsSumString;

    private String priceDate;

    private String priceNumber;

    private String requisites;

    private String execFIO;

    private String year;

    public String getActType() {
        return actType;
    }

    public void setActType(String actType) {
        this.actType = actType;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName;
    }

    public String getActDate() {
        return actDate;
    }

    public void setActDate(String actDate) {
        this.actDate = actDate;
    }

    public String getSubjName() {
        return subjName;
    }

    public void setSubjName(String subjName) {
        this.subjName = subjName;
    }

    public String getCadOrg() {
        return cadOrg;
    }

    public void setCadOrg(String cadOrg) {
        this.cadOrg = cadOrg;
    }

    public String getExecPost() {
        return execPost;
    }

    public void setExecPost(String execPost) {
        this.execPost = execPost;
    }

    public String getExecFullName() {
        return execFullName;
    }

    public void setExecFullName(String execFullName) {
        this.execFullName = execFullName;
    }

    public String getDateIn() {
        return dateIn;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public String getNumberIn() {
        return numberIn;
    }

    public void setNumberIn(String numberIn) {
        this.numberIn = numberIn;
    }

    public List<String> getObjTypeList() {
        return objTypeList;
    }

    public void setObjTypeList(List<String> objTypeList) {
        this.objTypeList = objTypeList;
    }

    public List<String> getObjAddressList() {
        return objAddressList;
    }

    public void setObjAddressList(List<String> objAddressList) {
        this.objAddressList = objAddressList;
    }

    public List<String> getObjCadNumList() {
        return objCadNumList;
    }

    public void setObjCadNumList(List<String> objCadNumList) {
        this.objCadNumList = objCadNumList;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getSumString() {
        return sumString;
    }

    public void setSumString(String sumString) {
        this.sumString = sumString;
    }

    public String getNdsSum() {
        return ndsSum;
    }

    public void setNdsSum(String ndsSum) {
        this.ndsSum = ndsSum;
    }

    public String getNdsSumString() {
        return ndsSumString;
    }

    public void setNdsSumString(String ndsSumString) {
        this.ndsSumString = ndsSumString;
    }

    public String getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }

    public String getPriceNumber() {
        return priceNumber;
    }

    public void setPriceNumber(String priceNumber) {
        this.priceNumber = priceNumber;
    }

    public String getRequisites() {
        return requisites;
    }

    public void setRequisites(String requisites) {
        this.requisites = requisites;
    }

    public String getExecFIO() {
        return execFIO;
    }

    public void setExecFIO(String execFIO) {
        this.execFIO = execFIO;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "ActInfo{" +
                "actType='" + actType + '\'' +
                ",\nactName='" + actName + '\'' +
                ",\nactDate='" + actDate + '\'' +
                ",\nsubjName='" + subjName + '\'' +
                ",\ncadOrg='" + cadOrg + '\'' +
                ",\nexecPost='" + execPost + '\'' +
                ",\nexecFullName='" + execFullName + '\'' +
                ",\ndateIn='" + dateIn + '\'' +
                ",\nnumberIn='" + numberIn + '\'' +
                ",\nobjTypeList=" + objTypeList +
                ",\nobjAddressList=" + objAddressList +
                ",\nobjCadNumList=" + objCadNumList +
                ",\nsum='" + sum + '\'' +
                ",\nsumString='" + sumString + '\'' +
                ",\nndsSum='" + ndsSum + '\'' +
                ",\nndsSumString='" + ndsSumString + '\'' +
                ",\npriceDate='" + priceDate + '\'' +
                ",\npriceNumber='" + priceNumber + '\'' +
                ",\nrequisites='" + requisites + '\'' +
                ",\nexecFIO='" + execFIO + '\'' +
                ",\nyear='" + year + '\'' +
                '}';
    }
}
