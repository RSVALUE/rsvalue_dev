package org.nka.rs.entity.pojos.docs;

import java.util.List;

/**
 * Класс, в котором содержится информация по выписке
 */
public class FormInfo {
    private String formName;

    private String formDate;

    private String searchDate;

    private String formNumber;

    private List<String> formInfo;

    private int currency;

    private List<String> funcPurpose;

    private List<String> valueDate;

    private List<Long> zoneNumber;

    private List<String> costUSD;

    private List<String> costBYN;

    private List<String> costSquareUSD;

    private List<String> costSquareBYN;

    private List<Integer> foundationNumber;

    private List<String> foundationList;

    private List<String> attachmentList;

    private int nbYear;

    private String execPost;

    private String execFullName;

    private List<Long> zoneId;

    private List<String> parentAte;

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormDate() {
        return formDate;
    }

    public void setFormDate(String formDate) {
        this.formDate = formDate;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public List<String> getFormInfo() {
        return formInfo;
    }

    public void setFormInfo(List<String> formInfo) {
        this.formInfo = formInfo;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public List<String> getFuncPurpose() {
        return funcPurpose;
    }

    public void setFuncPurpose(List<String> funcPurpose) {
        this.funcPurpose = funcPurpose;
    }

    public List<String> getValueDate() {
        return valueDate;
    }

    public void setValueDate(List<String> valueDate) {
        this.valueDate = valueDate;
    }

    public List<Long> getZoneNumber() {
        return zoneNumber;
    }

    public void setZoneNumber(List<Long> zoneNumber) {
        this.zoneNumber = zoneNumber;
    }

    public List<String> getCostUSD() {
        return costUSD;
    }

    public void setCostUSD(List<String> costUSD) {
        this.costUSD = costUSD;
    }

    public List<String> getCostBYN() {
        return costBYN;
    }

    public void setCostBYN(List<String> costBYN) {
        this.costBYN = costBYN;
    }

    public List<String> getCostSquareUSD() {
        return costSquareUSD;
    }

    public void setCostSquareUSD(List<String> costSquareUSD) {
        this.costSquareUSD = costSquareUSD;
    }

    public List<String> getCostSquareBYN() {
        return costSquareBYN;
    }

    public void setCostSquareBYN(List<String> costSquareBYN) {
        this.costSquareBYN = costSquareBYN;
    }

    public List<Integer> getFoundationNumber() {
        return foundationNumber;
    }

    public void setFoundationNumber(List<Integer> foundationNumber) {
        this.foundationNumber = foundationNumber;
    }

    public List<String> getFoundationList() {
        return foundationList;
    }

    public void setFoundationList(List<String> foundationList) {
        this.foundationList = foundationList;
    }

    public List<String> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<String> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public int getNbYear() {
        return nbYear;
    }

    public void setNbYear(int nbYear) {
        this.nbYear = nbYear;
    }

    public String getExecPost() {
        return execPost;
    }

    public void setExecPost(String execPost) {
        this.execPost = execPost;
    }

    public String getExecFullName() {
        return execFullName;
    }

    public void setExecFullName(String execFullName) {
        this.execFullName = execFullName;
    }

    public List<Long> getZoneId() {
        return zoneId;
    }

    public void setZoneId(List<Long> zoneId) {
        this.zoneId = zoneId;
    }

    public List<String> getParentAte() {
        return parentAte;
    }

    public void setParentAte(List<String> parentAte) {
        this.parentAte = parentAte;
    }

    @Override
    public String toString() {
        return "FormInfo{" +
                "formName='" + formName + '\'' +
                ",\nformDate='" + formDate + '\'' +
                ",\nsearchDate='" + searchDate + '\'' +
                ",\nformNumber='" + formNumber + '\'' +
                ",\nformInfo=" + formInfo +
                ",\ncurrency=" + currency +
                ",\nfuncPurpose=" + funcPurpose +
                ",\nvalueDate=" + valueDate +
                ",\nzoneNumber=" + zoneNumber +
                ",\ncostUSD=" + costUSD +
                ",\ncostBYN=" + costBYN +
                ",\ncostSquareUSD=" + costSquareUSD +
                ",\ncostSquareBYN=" + costSquareBYN +
                ",\nfoundationNumber=" + foundationNumber +
                ",\nfoundationList=" + foundationList +
                ",\nattachmentList=" + attachmentList +
                ",\nnbYear=" + nbYear +
                ",\nexecPost='" + execPost + '\'' +
                ",\nexecFullName='" + execFullName + '\'' +
                ",\nzoneId=" + zoneId +
                ",\nparentAte=" + parentAte +
                '}';
    }
}