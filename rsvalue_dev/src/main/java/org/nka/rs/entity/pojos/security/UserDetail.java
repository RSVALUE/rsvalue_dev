package org.nka.rs.entity.pojos.security;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.nka.rs.entity.pojos.common.UserEntity;

import javax.persistence.*;

@Entity
@Table(name = "USER_DETAIL")
public class UserDetail {

    @Id
    @Column(name = "USER_ID", unique = true, nullable = false)
    private long userId;

    @Column(name = "SALT")
    private String salt;

    @Column(name = "TOKEN")
    private String token;

    @OneToOne
    @PrimaryKeyJoinColumn
    @JsonBackReference
    private UserEntity userDet;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserEntity getUserDet() {
        return userDet;
    }

    public void setUserDet(UserEntity userDet) {
        this.userDet = userDet;
    }
}