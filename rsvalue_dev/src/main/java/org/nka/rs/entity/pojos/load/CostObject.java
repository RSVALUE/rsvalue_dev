package org.nka.rs.entity.pojos.load;

import org.nka.rs.entity.dictionary.CostTypeDic;
import org.nka.rs.entity.dictionary.CurrencyDic;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dictionary.MethodTypeDic;
import org.nka.rs.entity.pojos.common.Operation;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//Объект оценки
@Entity
@Table(name = "COST_OBJECT")
public class CostObject {


    //ИД объекта
    @Id
    @Column(name = "REG_ID", nullable = false, unique = true)
    private Long regId;

    //Классификатор, тип объекта оценки
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COST_TYPE", nullable = false)
    private CostTypeDic costType;

    //Классификатор, метод оценки (not null?)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "METHOD_TYPE", nullable = false)
    private MethodTypeDic methodType; //classifier 1

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FUNCCODE", nullable = false)
    private FunctionalPurposeDic funcCode; //classifier

    //Классификатор вида валют
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUR_TYPE", nullable = false)
    private CurrencyDic currency;

    //район в случае оценки СНП и с/х земель
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENTOBJECTNUM", nullable = false)
    private ATE parentObjectnumber;

    //АТЕ - для 10 - город, для 20 - СНП, для 40 - с/с, для 30 - р-н
    //регистрационный номер объекта в регистре АТЕ для Города, ПГТ и СНП; регистрационный номер сельсовета для "За пределами НП, СТ и ДК"
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "OBJECTNUMBER", nullable = false, insertable = false, updatable = false)
    private ATE objectnumber;

    //Население объекта оценки
    @Column(name = "POPULATION")
    private Long population;

    //Дата загрузки данных по объекту оценки
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_REG_START", nullable = false)
    private Date dateRegStart;

    //Дата деактуализации данных по объекту оценки
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_REG_STOP")
    private Date dateRegStop;

    //Дата проведения оценки
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_VAL", nullable = false)
    private Date dateVal;

    //Операция
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "OOPER_ID", nullable = false)
    private Operation operation;

    //Список оценочных зон
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "costObject")
    private List<CostZone> costZones;

    //Дополнительные сведения по загрузке данных (объект загрузки)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "REG_NUM", nullable = false)
    private LoadObject loadObject;


    public CostObject() {
    }

    public Long getRegId() {
        return regId;
    }

    public void setRegId(Long regId) {
        this.regId = regId;
    }

    public ATE getParentObjectnumber() {
        return parentObjectnumber;
    }

    public void setParentObjectnumber(ATE parentObjectnumber) {
        this.parentObjectnumber = parentObjectnumber;
    }

    public ATE getObjectnumber() {
        return objectnumber;
    }

    public void setObjectnumber(ATE objectnumber) {
        this.objectnumber = objectnumber;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public Date getDateRegStart() {
        return dateRegStart;
    }

    public void setDateRegStart(Date dateRegStart) {
        this.dateRegStart = dateRegStart;
    }

    public Date getDateRegStop() {
        return dateRegStop;
    }

    public void setDateRegStop(Date dateRegStop) {
        this.dateRegStop = dateRegStop;
    }

    public Date getDateVal() {
        return dateVal;
    }

    public void setDateVal(Date dateVal) {
        this.dateVal = dateVal;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public List<CostZone> getCostZones() {
        return costZones;
    }

    public void setCostZones(List<CostZone> costZones) {
        this.costZones = costZones;
    }

    public LoadObject getLoadObject() {
        return loadObject;
    }

    public void setLoadObject(LoadObject loadObject) {
        this.loadObject = loadObject;
    }

    public CostTypeDic getCostType() {
        return costType;
    }

    public void setCostType(CostTypeDic costType) {
        this.costType = costType;
    }

    public MethodTypeDic getMethodType() {
        return methodType;
    }

    public void setMethodType(MethodTypeDic methodType) {
        this.methodType = methodType;
    }

    public FunctionalPurposeDic getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(FunctionalPurposeDic funcCode) {
        this.funcCode = funcCode;
    }

    public CurrencyDic getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyDic currency) {
        this.currency = currency;
    }
}
