package org.nka.rs.entity.pojos.load;

import javax.persistence.*;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//дополнительные сведения по оценочной зоне
@Entity
@Table(name = "COST_ZONE_DATA")

public class CostZoneData {

    //ИД дополнительных сведений по оценочной зоне
    @Id
    @Column(name = "ZONE_DATA_ID", unique = true, nullable = false)
    private Long costZoneDataId;

    //оценочная зона
    @OneToOne(optional = false)
    @JoinColumn(name = "ZONE_ID", nullable = false)
    private CostZone costZone;

    //описание оценочной зоны или может записываться категория: деревня, агрогородок и пр.
    @Column(name = "ZONEDESC", length = 500)
    private String zoneDescription;

    //наименование сельсовета для СНП, наименование ближайшего населенного пункта для СТ и ДК
    @Column(name = "NEAR_LOC", length = 250)
    private String nearLoc;

    //название зоны: название города или населенного пункта для Города, СНП или ПГТ, название СТ или ДК, для с/х земель - название сельсовета
    @Column(name = "ZONE_NAME", length = 200)
    private String zoneName;

    //УНП садового товарищества или другого землепользователя или с/х земли
    @Column(name = "ST_UNP")
    private Long unp;

    //примечание -  в старой базе есть параметр - номер СТ - его записывать сюда при конвертации, и, в случае, если заказчик захочем выводить это поле в дальнейшем
    @Column(name = "VPRM", length = 100)
    private String remark;

    public CostZoneData() {
    }

    public Long getCostZoneDataId() {
        return costZoneDataId;
    }

    public void setCostZoneDataId(Long costZoneDataId) {
        this.costZoneDataId = costZoneDataId;
    }

    public CostZone getCostZone() {
        return costZone;
    }

    public void setCostZone(CostZone costZone) {
        this.costZone = costZone;
    }

    public String getZoneDescription() {
        return zoneDescription;
    }

    public void setZoneDescription(String zoneDescription) {
        this.zoneDescription = zoneDescription;
    }

    public String getNearLoc() {
        return nearLoc;
    }

    public void setNearLoc(String nearLoc) {
        this.nearLoc = nearLoc;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String stName) {
        this.zoneName = stName;
    }

    public Long getUnp() {
        return unp;
    }

    public void setUnp(Long stUNP) {
        this.unp = stUNP;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
