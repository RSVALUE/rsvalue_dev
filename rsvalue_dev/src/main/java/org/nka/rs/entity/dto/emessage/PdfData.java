package org.nka.rs.entity.dto.emessage;

import java.util.List;

/**
 * Created by zgurskaya on 30.11.2016.
 */
public class PdfData {

    //метод поиска
    private int searchMethodValue;
    //общие данные по поиску
    private CadastralInfo data;
    //для налогооблажения
    private boolean isNb;
    //стоимостные показатели для всех видов поиска
    private List<CadastralValueInfo> valuesCharacters;


    public PdfData() {
    }

    public int getSearchMethodValue() {
        return searchMethodValue;
    }

    public void setSearchMethodValue(int searchMethodValue) {
        this.searchMethodValue = searchMethodValue;
    }

    public CadastralInfo getData() {
        return data;
    }

    public void setData(CadastralInfo data) {
        this.data = data;
    }

    public boolean getNb() {
        return isNb;
    }

    public void setNb(boolean nb) {
        isNb = nb;
    }

    public List<CadastralValueInfo> getValuesCharacters() {
        return valuesCharacters;
    }

    public void setValuesCharacters(List<CadastralValueInfo> valuesCharacters) {
        this.valuesCharacters = valuesCharacters;
    }
}
