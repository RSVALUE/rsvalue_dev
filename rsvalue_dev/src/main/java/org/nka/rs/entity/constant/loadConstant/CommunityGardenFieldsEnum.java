package org.nka.rs.entity.constant.loadConstant;

/**
 * Created by zgurskaya on 22.06.2016.
 */
public enum CommunityGardenFieldsEnum {

    NUM("Num"),
    STNAME("Stname"),
    UNP("Unp"),
    NPNAME("Npname"),
    COST_D("Cost_d"),
    COST_R("Cost_r");

    private final String columnName;

    CommunityGardenFieldsEnum(String columnName) {
        this.columnName = columnName;
    }

    public static CommunityGardenFieldsEnum search(String columnName) {
        for (CommunityGardenFieldsEnum param : CommunityGardenFieldsEnum.values()) {
            if (columnName.equals(param.getColumnName())) {
                return param;
            }
        }
        return null;
    }

    public String getColumnName() {
        return columnName;
    }


}
