package org.nka.rs.entity.dto.emessage;

import java.util.List;

/**
 * Created by zgurskaya on 30.09.2016.
 */
public class AddressMessage {

    //код ответа: -1 - вывести сообщение response о невозможности дальнейшего поиска; 1 - продолжить поиск по кадастровому номеру, 2 - продолжить поиск по адресной точке
    public Integer code;
    //список найденных кадастровых номеров по данному адресу
    public List<String> cadnumList;
    //вывод адреса для сообщения о невозможности дальнейшего поиска или при нахождении адресной точки без земельных участков
    public String address;
    //строка с оценочными зонами для поиска по адресной точки
    public String zones;
    //сообщение о невозможности дальнейшего поиска
    public String response;

    public String coordinates;

    public AddressMessage() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<String> getCadnumList() {
        return cadnumList;
    }

    public void setCadnumList(List<String> cadnumList) {
        this.cadnumList = cadnumList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZones() {
        return zones;
    }

    public void setZones(String zones) {
        this.zones = zones;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "AddressMessage{" +
                "code=" + code +
                ", cadnumList=" + cadnumList +
                ", address='" + address + '\'' +
                ", zones='" + zones + '\'' +
                ", response='" + response + '\'' +
                '}';
    }
}
