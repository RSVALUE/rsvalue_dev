package org.nka.rs.entity.responseVar;

/**
 * Created by zgurskaya on 02.08.2016.
 */
public class SaveDocResponse {

    //код события
    private int code;
    //сообщение
    private String message;
    //новое название документа (если есть)
    private String name;
    //ИД документа (regDocId)
    private String id;

    public SaveDocResponse() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
