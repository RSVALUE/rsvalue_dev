package org.nka.rs.entity.pojos.message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 09.11.2016.
 */

//PDF-documents
@Entity
@Table(name = "PDF_REQUEST")

public class PdfRequest {

    //ИД пдф-документа
    @Id
    @Column(name = "PDF_ID")
    private Long id;

    //путь к документу в папке
    @Column(name = "PDF_PATH")
    private String path;

    public PdfRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
