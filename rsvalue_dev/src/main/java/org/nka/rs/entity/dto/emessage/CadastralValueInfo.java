package org.nka.rs.entity.dto.emessage;

import java.util.Date;

/**
 * Created by zgurskaya on 02.12.2016.
 */
public class CadastralValueInfo {

    //todo класс практически идентичен CadastralValueData - но тут нет строки основания и объекта основания - их в исходном классе можно убрать,
    // т.к. пришлось формировать новый параметр remark из оснований в дао. Т.е. смысла эти параметры уже не несут.
    // Кроме наличия метода toString, который правильно формирует строку. Но метод можно вынести в util

    //функциональное назначение
    private String funcCodeName;
    //дата оценки
    private Date dateVal;
    //номер оценочной зоны
    private String zoneNum;
    //стоимость в долларах
    private Double costD;
    //стоимость в рублях
    private Double costR;
    //значение налоговой базы
    private Double costNB;

    private Foundation foundation;
    //основание в виде строки
    private  String foundationStr;

    public CadastralValueInfo() {
    }

    public String getFuncCodeName() {
        return funcCodeName;
    }

    public void setFuncCodeName(String funcCodeName) {
        this.funcCodeName = funcCodeName;
    }

    public Date getDateVal() {
        return dateVal;
    }

    public void setDateVal(Date dateVal) {
        this.dateVal = dateVal;
    }

    public String getZoneNum() {
        return zoneNum;
    }

    public void setZoneNum(String zoneNum) {
        this.zoneNum = zoneNum;
    }

    public Double getCostD() {
        return costD;
    }

    public void setCostD(Double costD) {
        this.costD = costD;
    }

    public Double getCostR() {
        return costR;
    }

    public void setCostR(Double costR) {
        this.costR = costR;
    }

    public Double getCostNB() {
        return costNB;
    }

    public void setCostNB(Double costNB) {
        this.costNB = costNB;
    }

    public Foundation getFoundation() {
        return foundation;
    }

    public void setFoundation(Foundation foundation) {
        this.foundation = foundation;
    }

    public String getFoundationStr() {
        return foundationStr;
    }

    public void setFoundationStr(String foundationStr) {
        this.foundationStr = foundationStr;
    }
}
