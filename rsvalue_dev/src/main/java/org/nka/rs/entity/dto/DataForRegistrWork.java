package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 19.07.2016.
 */
public class DataForRegistrWork {


    //дата загрузки, с которой отображаются данные
    private String dateFrom;
    //дата загрузки, до которой отображаются данные
    private String dateTo;
    //ИД города/района
    private Long ateId;
    //ИД вида оценки
    private Integer costTypeId;
    //ИД функционального назначения
    private Integer funcCodeId;
    //параметр сортировки
    private Integer sortingParam;
    //предикат - по возрастанию или убыванию
    private String predicate;
    //номер отображаемой страницы
    private Integer page;
    //количество записей на странице
    private Integer amount;

    private Integer actual;

    public DataForRegistrWork() {
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public Long getAteId() {
        return ateId;
    }

    public void setAteId(Long ateId) {
        this.ateId = ateId;
    }

    public Integer getCostTypeId() {
        return costTypeId;
    }

    public void setCostTypeId(Integer costTypeId) {
        this.costTypeId = costTypeId;
    }

    public Integer getFuncCodeId() {
        return funcCodeId;
    }

    public void setFuncCodeId(Integer funcCodeId) {
        this.funcCodeId = funcCodeId;
    }

    public Integer getSortingParam() {
        return sortingParam;
    }

    public void setSortingParam(Integer sortingParam) {
        this.sortingParam = sortingParam;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getActual() {
        return actual;
    }

    public void setActual(Integer actual) {
        this.actual = actual;
    }
}
