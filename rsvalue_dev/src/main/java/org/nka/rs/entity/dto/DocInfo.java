package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 09.06.2016.
 */
//класс для описательной информации по документу и создания логики в файловом хранилище
public class DocInfo {


    //идентификатор вида оценки
    private Integer costTypeId;
    //идентификатор функционального использования
    private Integer funcCode;
    //идентификатор объекта загрузки (город, район)
    private Long ateId; //or objectnumber
    //тип документа по классификатору
    private Integer docTypeId;
    //это поле заполняется только для типа документа "Решение" и дублируется с номером решения для объекта загрузки. В остальных случаях - пустое
    private String docNum;

    public DocInfo() {
    }

    public Integer getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(Integer docTypeId) {
        this.docTypeId = docTypeId;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public Integer getCostTypeId() {
        return costTypeId;
    }

    public void setCostTypeId(Integer costTypeId) {
        this.costTypeId = costTypeId;
    }

    public Integer getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(Integer funcCode) {
        this.funcCode = funcCode;
    }

    public Long getAteId() {
        return ateId;
    }

    public void setAteId(Long ateId) {
        this.ateId = ateId;
    }

    @Override
    public String toString() {
        return "DocInfo{" +
                "costTypeId=" + costTypeId +
                ", funcCode=" + funcCode +
                ", ateId=" + ateId +
                ", docTypeId=" + docTypeId +
                ", docNum='" + docNum + '\'' +
                '}';
    }
}
