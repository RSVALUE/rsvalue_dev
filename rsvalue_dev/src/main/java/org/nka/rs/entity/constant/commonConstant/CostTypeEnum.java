package org.nka.rs.entity.constant.commonConstant;

/**
 * Created by zgurskaya on 13.07.2016.
 */
public enum CostTypeEnum {

    CITY_AND_PGT(10, "Кадастровая оценка земель, земельных участков городов, поселков городского типа"),
    RURAL_LOCALITY(20, "Кадастровая оценка земель, земельных участков сельских населенных пунктов"),
    COMMUNITY_GARDEN(30, "Кадастровая оценка земель садоводческих товариществ и дачных кооперативов"),
    LANDS_OUT_LOCALITY(40, "Кадастровая оценка земель расположенных за пределами населенных пунктов, садоводческих товариществ и дачных кооперативов"),
    FARM_LANDS(50, "Кадастровая оценка сельскохозяйственных земель"),
    PARCEL(60, "Кадастровая оценка одного участка");

    private final String typeName;
    private final Integer typeValue;


    CostTypeEnum(Integer typeValue, String typeName) {
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public static CostTypeEnum searchByName(String typeName) {
        for (CostTypeEnum param : CostTypeEnum.values()) {
            if (typeName.equals(param.getTypeName())) {
                return param;
            }
        }
        return null;
    }
    public static CostTypeEnum searchByValue(Integer typeValue) {
        for (CostTypeEnum param : CostTypeEnum.values()) {
            if (typeValue.equals(param.getTypeValue())) {
                return param;
            }
        }
        return null;
    }

    public String getTypeName() {
        return typeName;
    }

    public Integer getTypeValue() {
        return typeValue;
    }
}
