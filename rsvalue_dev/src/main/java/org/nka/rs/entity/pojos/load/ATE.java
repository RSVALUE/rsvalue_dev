package org.nka.rs.entity.pojos.load;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.nka.rs.entity.dictionary.ATECategoryDic;

import javax.persistence.*;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//Адреса
@Entity
@Table(name = "ATE")
public class ATE {

    //ИД адреса - это objectnumber in ObjectCost.class
    @Id
    @Column(name = "ATE_ID", unique = true, nullable = false, insertable = false, updatable = false)
    private Long ateId;

    //наименование территориальной единицы
    @Column(name = "ATE_NAME", length = 60, nullable = false)
    private String ateName;

    //СОАТО
    @Column(name = "SOATO", nullable = false)
    private String soato;

    //список связанных ИД
    @Column(name = "TREE_IDS", length = 60, nullable = false)
    private String treeIds;

    //ИД родительского АТЕ при поиске по СНП
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENTATE_ID", insertable = false, updatable = false)
    @JsonIgnore
    private ATE parentATE;

    //этот ИД был создан для того, чтобы получать города и ПГТ по ИД района, в то  время как парентом данного ПГТ может быть с/с данного района
    @Column(name = "PARENTATE_ID")
    private Long parentAteId;

    //а вот здесь оригинальные родительские ИД
    @Column(name = "ORIGIN_PARENTATE_ID")
    private Long originParentId;

    //Классификатор, категория объекта
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CATEGORY")
    private ATECategoryDic category;

    //Классификатор, родительская категория объекта
    @Column(name = "PARENT_CODE")
    private Integer parentCategory;

    //Актуальность
    @Column(name = "ACTUAL")
    private Boolean actual;



    public ATE() {
    }

    public Long getAteId() {
        return ateId;
    }

    public void setAteId(Long ateId) {
        this.ateId = ateId;
    }

    public String getAteName() {
        return ateName;
    }

    public void setAteName(String ateName) {
        this.ateName = ateName;
    }

    public ATECategoryDic getCategory() {
        return category;
    }

    public void setCategory(ATECategoryDic category) {
        this.category = category;
    }

    public String getSoato() {
        return soato;
    }

    public void setSoato(String soato) {
        this.soato = soato;
    }

    public String getTreeIds() {
        return treeIds;
    }

    public void setTreeIds(String treeIds) {
        this.treeIds = treeIds;
    }

    public ATE getParentATE() {
        return parentATE;
    }

    public void setParentATE(ATE parentATE) {
        this.parentATE = parentATE;
    }

    public Long getParentAteId() {
        return parentAteId;
    }

    public void setParentAteId(Long parentAteId) {
        this.parentAteId = parentAteId;
    }

    public Integer getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Integer parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Long getOriginParentId() {
        return originParentId;
    }

    public void setOriginParentId(Long originParentId) {
        this.originParentId = originParentId;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }
}
