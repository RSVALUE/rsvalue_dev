package org.nka.rs.entity.pojos.common;

import org.nka.rs.entity.dictionary.SubjectTypeDic;
import org.nka.rs.entity.dictionary.TORDic;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Entity
@Table(name = "SUBJECTS")
public class Subject {

    //ИД субъекта
    @Id
    @Column(name = "SUBJECT_ID", unique = true, nullable = false)
    private Long subjectId;

    //фамилия в случае физических лиц
    @Column(name = "SURNAME", length = 50)
    private String surname;

    //имя в случае физических лиц
    @Column(name = "FIRSTNAME", length = 30)
    private String firstName;

    //отчество в случае физических лиц
    @Column(name = "FATHERNAME", length = 30)
    private String fatherName;

    public Subject() {
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "subjectId=" + subjectId +
                ", surname='" + surname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", fatherName='" + fatherName + '\'' +
                '}';
    }
}
