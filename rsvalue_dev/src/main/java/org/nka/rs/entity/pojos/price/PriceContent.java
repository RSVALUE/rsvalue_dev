package org.nka.rs.entity.pojos.price;

import org.nka.rs.entity.dictionary.OrderingDocumentTypeDic;
import org.nka.rs.entity.dictionary.SubjectTypeDic;

import javax.persistence.*;

/**
 * Created by zgurskaya on 14.10.2016.
 */

@Entity
@Table(name = "PRICE_CONTENT")

public class PriceContent {

    @Id
    @Column(name = "PRICECONTENT_ID", unique = true, nullable = false)
    private Long contentId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "PRICE_ID", nullable = false)
    private Price price;

    //Тип субъекта
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUBJECT_TYPE", nullable = false)
    private SubjectTypeDic subjectType;

    //тип документа
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "DOC_TYPE", insertable = false, updatable = false)
    private OrderingDocumentTypeDic docType;

    //цена с НДС для срочных заказов
    @Column(name = "PRICE_URG")
    private Double priceUrg;

    //цена с НДС для несрочных заказов
    @Column(name = "PRICE_NONURG")
    private Double priceNonUrg;

    //сумма НДС для срочных заказов - высчитываются в функции, не мапятся на таблицу
    private Double ndsUrg;

    //сумма НДС для несрочных заказов - высчитываются в функции, не мапятся на таблицу
    private Double ndsNonUrg;

    public PriceContent() {
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public SubjectTypeDic getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectTypeDic subjectType) {
        this.subjectType = subjectType;
    }

    public OrderingDocumentTypeDic getDocType() {
        return docType;
    }

    public void setDocType(OrderingDocumentTypeDic docType) {
        this.docType = docType;
    }

    public Double getPriceUrg() {
        return priceUrg;
    }

    public void setPriceUrg(Double priceUrg) {
        this.priceUrg = priceUrg;
    }

    public Double getPriceNonUrg() {
        return priceNonUrg;
    }

    public void setPriceNonUrg(Double priceNonUrg) {
        this.priceNonUrg = priceNonUrg;
    }

    public Double getNdsUrg() {
        return ndsUrg;
    }

    public void setNdsUrg(Double taxUrg) {
        this.ndsUrg = taxUrg;
    }

    public Double getNdsNonUrg() {
        return ndsNonUrg;
    }

    public void setNdsNonUrg(Double taxNonUrg) {
        this.ndsNonUrg = taxNonUrg;
    }
}
