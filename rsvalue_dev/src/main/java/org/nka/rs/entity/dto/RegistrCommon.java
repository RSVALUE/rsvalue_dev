package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 22.11.2016.
 */
public class RegistrCommon {
    //номер загрузки
    private Long regNum;
    //дата загрузки
    private String dateRegStart;
    //дата оценки
    private String dateVal;
    //регистрационный номер населенного пункта
    private String ateName;
    //наименование вида оценки
    private String costTypeName;
    //функциональное назначение
    private String funcCodeName;
    //регистрационный номер населенного пункта
    private Long ateId;
    //наименование вида оценки
    private Integer costTypeId;
    //функциональное назначение
    private Integer funcCodeId;
    //актуальность
    private Boolean actual;

    public RegistrCommon() {
    }

    public Long getRegNum() {
        return regNum;
    }

    public void setRegNum(Long regNum) {
        this.regNum = regNum;
    }

    public String getDateRegStart() {
        return dateRegStart;
    }

    public void setDateRegStart(String dateRegStart) {
        this.dateRegStart = dateRegStart;
    }

    public String getDateVal() {
        return dateVal;
    }

    public void setDateVal(String dateVal) {
        this.dateVal = dateVal;
    }

    public String getAteName() {
        return ateName;
    }

    public void setAteName(String ateName) {
        this.ateName = ateName;
    }

    public String getCostTypeName() {
        return costTypeName;
    }

    public void setCostTypeName(String costTypeName) {
        this.costTypeName = costTypeName;
    }

    public String getFuncCodeName() {
        return funcCodeName;
    }

    public void setFuncCodeName(String funcCodeName) {
        this.funcCodeName = funcCodeName;
    }

    public Long getAteId() {
        return ateId;
    }

    public void setAteId(Long ateId) {
        this.ateId = ateId;
    }

    public Integer getCostTypeId() {
        return costTypeId;
    }

    public void setCostTypeId(Integer costTypeId) {
        this.costTypeId = costTypeId;
    }

    public Integer getFuncCodeId() {
        return funcCodeId;
    }

    public void setFuncCodeId(Integer funcCodeId) {
        this.funcCodeId = funcCodeId;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    @Override
    public String toString() {
        return "RegistrCommon{" +
                "regNum=" + regNum +
                ", dateRegStart='" + dateRegStart + '\'' +
                ", dateVal='" + dateVal + '\'' +
                ", ateName='" + ateName + '\'' +
                ", costTypeName='" + costTypeName + '\'' +
                ", funcCodeName='" + funcCodeName + '\'' +
                ", ateId=" + ateId +
                ", costTypeId=" + costTypeId +
                ", funcCodeId=" + funcCodeId +
                ", actual=" + actual +
                '}';
    }
}

