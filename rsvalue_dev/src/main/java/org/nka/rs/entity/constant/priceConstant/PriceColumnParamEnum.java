package org.nka.rs.entity.constant.priceConstant;

/**
 * Created by zgurskaya on 24.10.2016.
 */
public enum PriceColumnParamEnum {

    PRICE_NUM(" price.priceNumber ", " priceNumber ", 1),       //номер прейскуранта
    DATE_START(" price.startDate ", " startDate ", 2),          //дата начала действия прайса
    DATE_STOP(" price.stopDate ", " stopDate ", 3);             //дата окончания действия прайса


    private final String queryName;
    private final String alias;
    private final Integer columnNumber;

    PriceColumnParamEnum(String queryName, String alias, Integer columnNumber) {
        this.queryName = queryName;
        this.alias = alias;
        this.columnNumber = columnNumber;
    }

    public static PriceColumnParamEnum search(Integer columnNumber) {
        for (PriceColumnParamEnum param : PriceColumnParamEnum.values()) {
            if (columnNumber.equals(param.getColumnNumber())) {
                return param;
            }
        }
        return null;
    }

    public String getQueryName() {
        return queryName;
    }

    public String getAlias() {
        return alias;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }



}
