package org.nka.rs.entity.dto.price;

import java.util.Date;

/**
 * Created by zgurskaya on 24.10.2016.
 */
public class PriceCommonInfo {

    //ИД прейскуранта
    private Long priceId;

    private String priceNumber;

    //дата начала действия прейскуранта
    private Date startDate;

    //дата окончания действия прейскуранта
    private Date stopDate;

    //ФИО и должность специалиста, осуществившего последнюю операцию с прейскурантом
    private String userInfo;


    public PriceCommonInfo() {
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public String getPriceNumber() {
        return priceNumber;
    }

    public void setPriceNumber(String priceNumber) {
        this.priceNumber = priceNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }
}
