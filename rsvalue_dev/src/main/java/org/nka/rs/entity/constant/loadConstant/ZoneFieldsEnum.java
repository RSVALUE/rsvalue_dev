package org.nka.rs.entity.constant.loadConstant;

/**
 * Created by zgurskaya on 08.07.2016.
 */
public enum ZoneFieldsEnum {

    NUM("Num"),
    AREA("Area"),
    COST_D("Cost_d"),
    COST_R("Cost_r"),
    OBJECTNUMBER("Objectnumb"),
    NAME("Name"),
    SOATO("Soato"),
    CATEGORY("Category"),
    Ss("Ss"),
    DISTR("Distr"),
    ST_NAME("Stname"),
    UNP("Unp"),
    NP_NAME("Npname"),
    REGN_SS("Regn_ss");

    private final String columnName;

    ZoneFieldsEnum(String columnName) {
        this.columnName = columnName;
    }

    public static ZoneFieldsEnum search(String columnName) {
        for (ZoneFieldsEnum param : ZoneFieldsEnum.values()) {
            if (columnName.equals(param.getColumnName())) {
                return param;
            }
        }
        return null;
    }

    public String getColumnName() {
        return columnName;
    }


}
