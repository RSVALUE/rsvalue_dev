package org.nka.rs.entity.constant.loadConstant;

/**
 * Created by zgurskaya on 22.06.2016.
 */
public enum LandsOutLocalityFieldsEnum {

    OBJECTNUMBER("Objectnumb"),
    DISTR("Distr"),
    REGNSS("Regn_ss"),
    NAME("Name"),
    NUM("Num"),
    COST_D("Cost_d"),
    COST_R("Cost_r");

    private final String columnName;

    LandsOutLocalityFieldsEnum(String columnName) {
        this.columnName = columnName;
    }

    public static LandsOutLocalityFieldsEnum search(String columnName) {
        for (LandsOutLocalityFieldsEnum param : LandsOutLocalityFieldsEnum.values()) {
            if (columnName.equals(param.getColumnName())) {
                return param;
            }
        }
        return null;
    }

    public String getColumnName() {
        return columnName;
    }
}
