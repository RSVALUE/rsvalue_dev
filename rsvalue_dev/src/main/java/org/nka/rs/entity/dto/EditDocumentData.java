package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 26.07.2016.
 */

//класс для передачи сведений по редактированию документов
public class EditDocumentData {

    //номер загрузки
    private Long regNum;
    //список документов на удаление
    private Long [] deleteDocList;
    //список документов на сохранение
    private Long [] saveDocList;
    //ИД пользователя
    private Long userId;
    //это сохранение или отмена изменений
    private Boolean isSave;
    //ид загрузки на постгре - если среди документов были тематические слои
    private Long dwdId;

    public EditDocumentData() {
    }

    public Long[] getDeleteDocList() {
        return deleteDocList;
    }

    public void setDeleteDocList(Long[] deleteDocList) {
        this.deleteDocList = deleteDocList;
    }

    public Long[] getSaveDocList() {
        return saveDocList;
    }

    public void setSaveDocList(Long[] saveDocList) {
        this.saveDocList = saveDocList;
    }

    public Long getRegNum() {
        return regNum;
    }

    public void setRegNum(Long regNum) {
        this.regNum = regNum;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getSave() {
        return isSave;
    }

    public void setSave(Boolean save) {
        isSave = save;
    }

    public Long getDwdId() {
        return dwdId;
    }

    public void setDwdId(Long dwdId) {
        this.dwdId = dwdId;
    }
}
