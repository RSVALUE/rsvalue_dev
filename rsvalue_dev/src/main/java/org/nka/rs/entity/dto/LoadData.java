package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 02.06.2016.
 */
public class LoadData {

    //из выбранного пользователем
    //from first page

    private Long userId;
    //код вида оценки
    private Integer costTypeId; //CostObject
    //код методики оценки
    private Integer methosTypeId; //CostObject
    //список получают из dbf-файла - но чтобы мне проще было работать включила. Ты передаешь null
    //АТЕ - для 10 - город, для 20 - СНП, для 30 - с/с, для 40 - р-н
    private Long[] objectNumber; //CostObject
    //для 10 вида оценки - город, для 20-40 видов - р-н
    private Long parentObjectNumber; //CostObject
    //население
    private Long population; //CostObject
    //код функционального назначения
    private Integer funcCodeId; //CostZone
    //дата оценки
    private String dateVal; //CostObject
    //примечание
    private String remark; //LoadObject
    //дата загрузки
    private String dateRegStart; //CostObject
    //код типа геометрии - еще не знаю, нужен ли будет. Он использовался только для вывода видов документов
    private Integer polyTypeId;
    //код валюты
    private Integer currencyId;

    //from seconde page
    //код организации, проводившей оценку
    private Integer valOrgId;
    //код организации, принявшей решение об оценке
    private Integer decOrgId;
    //номер решения об оценке
    private String decNum;
    //дата решения об оценке
    private String decDate;

    //идентификатор загрузки - используется на постгресе и при загрузке документов и архивов дбф
    private Long dwdId;

    //список ИД документов - я тебе их возвращаю в строковом представлении
    private String[] docList;
    //список зон
    private Zone[] zoneList;
    //список слоев
    private AdditionalLayer[] layerList;
    //список участков
    private Bound[] boundList;


    public LoadData() {
    }

    public Integer getCostTypeId() {
        return costTypeId;
    }

    public void setCostTypeId(Integer costTypeId) {
        this.costTypeId = costTypeId;
    }

    public Integer getMethosTypeId() {
        return methosTypeId;
    }

    public void setMethosTypeId(Integer methosTypeId) {
        this.methosTypeId = methosTypeId;
    }

    public Long[] getObjectNumber() {
        return objectNumber;
    }

    public void setObjectNumber(Long[] objectNumber) {
        this.objectNumber = objectNumber;
    }

    public Long getParentObjectNumber() {
        return parentObjectNumber;
    }

    public void setParentObjectNumber(Long parentObjectNumber) {
        this.parentObjectNumber = parentObjectNumber;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public Integer getFuncCodeId() {
        return funcCodeId;
    }

    public void setFuncCodeId(Integer funcCodeId) {
        this.funcCodeId = funcCodeId;
    }

    public String getDateVal() {
        return dateVal;
    }

    public void setDateVal(String dateVal) {
        this.dateVal = dateVal;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDateRegStart() {
        return dateRegStart;
    }

    public void setDateRegStart(String dateRegStart) {
        this.dateRegStart = dateRegStart;
    }

    public Integer getPolyTypeId() {
        return polyTypeId;
    }

    public void setPolyTypeId(Integer polyTypeId) {
        this.polyTypeId = polyTypeId;
    }

    public Integer getValOrgId() {
        return valOrgId;
    }

    public void setValOrgId(Integer valOrgId) {
        this.valOrgId = valOrgId;
    }

    public Integer getDecOrgId() {
        return decOrgId;
    }

    public void setDecOrgId(Integer decOrgId) {
        this.decOrgId = decOrgId;
    }

    public String getDecNum() {
        return decNum;
    }

    public void setDecNum(String decNum) {
        this.decNum = decNum;
    }

    public String getDecDate() {
        return decDate;
    }

    public void setDecDate(String decDate) {
        this.decDate = decDate;
    }

    public String[] getDocList() {
        return docList;
    }

    public void setDocList(String[] docList) {
        this.docList = docList;
    }

    public Zone[] getZoneList() {
        return zoneList;
    }

    public void setZoneList(Zone[] zoneList) {
        this.zoneList = zoneList;
    }

    public AdditionalLayer[] getLayerList() {
        return layerList;
    }

    public void setLayerList(AdditionalLayer[] layerList) {
        this.layerList = layerList;
    }

    public Bound[] getBoundList() {
        return boundList;
    }

    public void setBoundList(Bound[] boundList) {
        this.boundList = boundList;
    }

    public Long getDwdId() {
        return dwdId;
    }

    public void setDwdId(Long dwdId) {
        this.dwdId = dwdId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
