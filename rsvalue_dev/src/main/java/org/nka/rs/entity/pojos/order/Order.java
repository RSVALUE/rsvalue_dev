package org.nka.rs.entity.pojos.order;

import org.nka.rs.entity.dictionary.OrderingDocumentTypeDic;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.pojos.common.Operation;
import org.nka.rs.entity.pojos.common.Subject;

import java.util.Date;

/**
 * Created by zgurskaya on 27.10.2016.
 */
public class Order {

    //заказчик
    public String customer;
    //вид документа (соглашения)
    public OrderingDocumentTypeDic document;
    //территориальная организация
    public TORDic tor;
    //номер соглашения
    public String contractNumber;
    //дата соглашения
    public Date contractDate;
    //номер исходящего
    public String outgoingNumber;
    //дата исходящего
    public Date outgoingDate;
    //номер входящего
    public String incomingNumber;
    //дата входящего
    public Date incomingDate;
    //дата внесения заказа
    public Date orderEntryDate;
    //дата закрытия заказа
    public Date orderClosingDate;
    //для налогооблажения
    public Boolean isTaxation;
    //срочность
    public Boolean isUrgency;
    //отметка об оплате
    public Boolean isPaid;
    //специалист, принявший заказ
    public Subject subject;
    //операция
    public Operation operation;

    public Order() {
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public OrderingDocumentTypeDic getDocument() {
        return document;
    }

    public void setDocument(OrderingDocumentTypeDic document) {
        this.document = document;
    }

    public TORDic getTor() {
        return tor;
    }

    public void setTor(TORDic tor) {
        this.tor = tor;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public String getOutgoingNumber() {
        return outgoingNumber;
    }

    public void setOutgoingNumber(String outgoingNumber) {
        this.outgoingNumber = outgoingNumber;
    }

    public Date getOutgoingDate() {
        return outgoingDate;
    }

    public void setOutgoingDate(Date outgoingDate) {
        this.outgoingDate = outgoingDate;
    }

    public String getIncomingNumber() {
        return incomingNumber;
    }

    public void setIncomingNumber(String incomingNumber) {
        this.incomingNumber = incomingNumber;
    }

    public Date getIncomingDate() {
        return incomingDate;
    }

    public void setIncomingDate(Date incomingDate) {
        this.incomingDate = incomingDate;
    }

    public Date getOrderEntryDate() {
        return orderEntryDate;
    }

    public void setOrderEntryDate(Date orderEntryDate) {
        this.orderEntryDate = orderEntryDate;
    }

    public Date getOrderClosingDate() {
        return orderClosingDate;
    }

    public void setOrderClosingDate(Date orderClosingDate) {
        this.orderClosingDate = orderClosingDate;
    }

    public Boolean getTaxation() {
        return isTaxation;
    }

    public void setTaxation(Boolean taxation) {
        isTaxation = taxation;
    }

    public Boolean getUrgency() {
        return isUrgency;
    }

    public void setUrgency(Boolean urgency) {
        isUrgency = urgency;
    }

    public Boolean getPaid() {
        return isPaid;
    }

    public void setPaid(Boolean paid) {
        isPaid = paid;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
