package org.nka.rs.entity.pojos.docs;

import java.util.List;
import java.util.Set;

/**
 * Класс, в котром содержится информация о том, как будет
 * формироваться выписка
 */
public class BuildInfo {

    private String docName;

    private String docType;

    private String docDate;

    private String docNumber;

    private String docSearchDate;

    private String docNbYear;

    private int docStarCount;

    private boolean docSecondType;

    private List<String> docInfo;

    private List<String[]> docObjects;

    private Set<Integer> docFoundationNumberSet;

    private List<Integer> docFoundationNumberList;

    private List<String> docFoundationList;

    private List<String> docAttachment;

    private String docExecPost;

    private String docExecFullName;

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public String getDocSearchDate() {
        return docSearchDate;
    }

    public void setDocSearchDate(String docSearchDate) {
        this.docSearchDate = docSearchDate;
    }

    public String getDocNbYear() {
        return docNbYear;
    }

    public void setDocNbYear(String docNbYear) {
        this.docNbYear = docNbYear;
    }

    public int getDocStarCount() {
        return docStarCount;
    }

    public void setDocStarCount(int docStarCount) {
        this.docStarCount = docStarCount;
    }

    public boolean isDocSecondType() {
        return docSecondType;
    }

    public void setDocSecondType(boolean docSecondType) {
        this.docSecondType = docSecondType;
    }

    public List<String> getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(List<String> docInfo) {
        this.docInfo = docInfo;
    }

    public List<String[]> getDocObjects() {
        return docObjects;
    }

    public void setDocObjects(List<String[]> docObjects) {
        this.docObjects = docObjects;
    }

    public Set<Integer> getDocFoundationNumberSet() {
        return docFoundationNumberSet;
    }

    public void setDocFoundationNumberSet(Set<Integer> docFoundationNumberSet) {
        this.docFoundationNumberSet = docFoundationNumberSet;
    }

    public List<Integer> getDocFoundationNumberList() {
        return docFoundationNumberList;
    }

    public void setDocFoundationNumberList(List<Integer> docFoundationNumberList) {
        this.docFoundationNumberList = docFoundationNumberList;
    }

    public List<String> getDocFoundationList() {
        return docFoundationList;
    }

    public void setDocFoundationList(List<String> docFoundationList) {
        this.docFoundationList = docFoundationList;
    }

    public List<String> getDocAttachment() {
        return docAttachment;
    }

    public void setDocAttachment(List<String> docAttachment) {
        this.docAttachment = docAttachment;
    }

    public String getDocExecPost() {
        return docExecPost;
    }

    public void setDocExecPost(String docExecPost) {
        this.docExecPost = docExecPost;
    }

    public String getDocExecFullName() {
        return docExecFullName;
    }

    public void setDocExecFullName(String docExecFullName) {
        this.docExecFullName = docExecFullName;
    }
}
