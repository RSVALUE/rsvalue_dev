package org.nka.rs.entity.constant.loadConstant;

/**
 * Created by zgurskaya on 30.06.2016.
 */
public enum BoundFieldsEnum {

    NUM("Num"),
    CADNUM("Cadnum"),
    SOATO("Soato"),
    BLOCK_NUMB("Block_numb"),
    ADDRESS("Address"),
    PUPRPOSE("Purpose"),
    PURPOSE_GOV("Purpose2"),
    SQUARE("Sq"),
    COST_D("Cost_d"),
    COST_R("Cost_r");

    private final String columnName;

    BoundFieldsEnum(String columnName) {
        this.columnName = columnName;
    }

    public static BoundFieldsEnum search(String columnName) {
        for (BoundFieldsEnum param : BoundFieldsEnum.values()) {
            if (columnName.equals(param.getColumnName())) {
                return param;
            }
        }
        return null;
    }

    public String getColumnName() {
        return columnName;
    }

}
