package org.nka.rs.entity.dto.price;

/**
 * Created by zgurskaya on 14.11.2016.
 */
public class Content {


    private Integer subjectTypeId;
    private Integer docTypeId;
    private Double priceUrg;
    private Double priceNonUrg;

    public Content() {
    }

    public Integer getSubjectTypeId() {
        return subjectTypeId;
    }

    public void setSubjectTypeId(Integer subjectTypeId) {
        this.subjectTypeId = subjectTypeId;
    }

    public Integer getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(Integer docTypeId) {
        this.docTypeId = docTypeId;
    }

    public Double getPriceUrg() {
        return priceUrg;
    }

    public void setPriceUrg(Double priceUrg) {
        this.priceUrg = priceUrg;
    }

    public Double getPriceNonUrg() {
        return priceNonUrg;
    }

    public void setPriceNonUrg(Double priceNonUrg) {
        this.priceNonUrg = priceNonUrg;
    }
}
