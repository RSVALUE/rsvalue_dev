package org.nka.rs.entity.pojos.common;

import org.nka.rs.entity.dictionary.OperationTypeDic;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//ОПЕРАЦИИ
@Entity
@Table(name = "OPERATIONS")
public class Operation {

    //ИД операции
    @Id
    @Column(name = "OOPER_ID", unique = true, nullable = false)
    private Long operId;

    //Тип операции (внесении, редактирование, деактуализация, внесение ошибочной записи)not null
    @ManyToOne
    @JoinColumn(name = "OPER_TYPE", nullable = false)
    private OperationTypeDic operType;

    //Пользователь, выполнивший загрузку not null
    @Column(name = "EXECUTOR")
    private Long executor;

    //Операция-прародитель
    @Column(name = "OPER_PARENT_ID")
    private Long parentOperationId;

    //дата проведения
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "OPER_DATE", nullable = false)
    private Date operDate;

    public Operation() {
    }

    public Long getOperId() {
        return operId;
    }

    public void setOperId(Long operId) {
        this.operId = operId;
    }

    public Date getOperDate() {
        return operDate;
    }

    public void setOperDate(Date operDate) {
        this.operDate = operDate;
    }

    public Long getExecutor() {
        return executor;
    }

    public void setExecutor(Long executor) {
        this.executor = executor;
    }

    public Long getParentOperationId() {
        return parentOperationId;
    }

    public void setParentOperationId(Long parentOperationId) {
        this.parentOperationId = parentOperationId;
    }

    public OperationTypeDic getOperType() {
        return operType;
    }

    public void setOperType(OperationTypeDic operType) {
        this.operType = operType;
    }

}
