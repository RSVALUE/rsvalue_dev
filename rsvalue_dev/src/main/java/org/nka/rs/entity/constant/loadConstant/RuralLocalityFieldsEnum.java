package org.nka.rs.entity.constant.loadConstant;

/**
 * Created by zgurskaya on 22.06.2016.
 */
public enum RuralLocalityFieldsEnum {

    OBJECTNUMBER("Objectnumb"),
    NUM("Num"),
    SOATO("Soato"),
    NAME("Name"),
    CATEGORY("Category"),
    DISTR("Distr"),
    SS("Ss"),
    COST_D("Cost_d"),
    COST_R("Cost_r");

    private final String columnName;

    RuralLocalityFieldsEnum(String columnName) {
        this.columnName = columnName;
    }

    public static RuralLocalityFieldsEnum search(String columnName) {
        for (RuralLocalityFieldsEnum param : RuralLocalityFieldsEnum.values()) {
            if (columnName.equals(param.getColumnName())) {
                return param;
            }
        }
        return null;
    }

    public String getColumnName() {
        return columnName;
    }


}
