package org.nka.rs.entity.security;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by zgurskaya on 04.10.2016.
 */
public class RestAuthentication {

    //указывает, прошла или нет авторизация
    private Integer code;
    //сообщение в случае ошибки авторизации
    private String message;
    //сведения о пользователе из Spring Security
    private UserDetails userDetails;
    //ИД пользователя
    private Long userId;
    //ФИО пользователя
    private String personData;
    //сформированный токен
    private String token;
    //ИД тора
    private Integer torId;

    public RestAuthentication() {
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPersonData() {
        return personData;
    }

    public void setPersonData(String personData) {
        this.personData = personData;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTorId() {
        return torId;
    }

    public void setTorId(Integer torId) {
        this.torId = torId;
    }
}
