package org.nka.rs.entity.constant.registrConstant;

/**
 * Created by zgurskaya on 25.07.2016.
 */
public enum ZoneColumnParamEnum {

    ZONE_NUM("zone.zoneNum " , " num ", 1),                //номер зоны
    PARENT_ATE_NAME("parent_ate.ateName ", " distr ", 2),   //наименование объекта загрузки (город/район)
    ZONE_NAME("zone_data.zoneName ", " name ", 3),         //наименование зоны
    COST_D("zone.costD ", " cost_d ", 4),                   //цена в долларах
    COST_R("zone.costR ", " cost_r ", 5),                   //цена в рублях
    OBJECTNUMBER("ate.ateId ", " objectnumb ", 6),             //ИД АТЕ
    SQUARE("zone.square " , " area ", 7),                 //площадь
    ZONE_DESC("zone_data.zoneDescription ", " category ", 8), //описание зоны (категория: деревня, агрогородок и пр)
    NEAR_LOC("zone_data.nearLoc ", " npname ", 9),          //наименование ближайшего насейленного пункта/сельсовета
    UNP("zone_data.unp ", " unp ", 10);			        //УНП


    private final String queryName;
    private final String alias;
    private final Integer columnNumber;

    ZoneColumnParamEnum(String queryName, String alias, Integer columnNumber) {
        this.queryName = queryName;
        this.alias = alias;
        this.columnNumber = columnNumber;
    }

    public static ZoneColumnParamEnum search(Integer columnNumber) {
        for (ZoneColumnParamEnum param : ZoneColumnParamEnum.values()) {
            if (columnNumber.equals(param.getColumnNumber())) {
                return param;
            }
        }
        return null;
    }

    public String getQueryName() {
        return queryName;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }

    public String getAlias() {
        return alias;
    }
}
