package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 09.06.2016.
 */

//данные из слоя Bounds (заносятся в земельные участки)
public class Bound {

    //номер зоны - привязка = zonenumber
    private String num;
    //кадастровый номер участка = cadnum
    private String cadnum;
    //поле не хранится в БД, проверяется наличие
    private Long soato;
    //блок-номер земельного участка
    private String block_numb;
    //строковый адрес = address
    private String address;
    //целевое назначение земель = purpose
    private String purpose;
    //целевое назначение земель по решению местного исполнительного органа
    // = purpose_gov
    private String purpose2;
    //площадь земельного участка = square
    private Double sq;
    //стоимость в долларах
    private Double cost_d;
    //стоимость в рублях
    private Double cost_r;

    public Bound() {
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getCadnum() {
        return cadnum;
    }

    public void setCadnum(String cadnum) {
        this.cadnum = cadnum;
    }

    public Long getSoato() {
        return soato;
    }

    public void setSoato(Long soato) {
        this.soato = soato;
    }

    public String getBlock_numb() {
        return block_numb;
    }

    public void setBlock_numb(String block_numb) {
        this.block_numb = block_numb;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurpose2() {
        return purpose2;
    }

    public void setPurpose2(String purpose2) {
        this.purpose2 = purpose2;
    }

    public Double getSq() {
        return sq;
    }

    public void setSq(Double sq) {
        this.sq = sq;
    }

    public Double getCost_d() {
        return cost_d;
    }

    public void setCost_d(Double cost_d) {
        this.cost_d = cost_d;
    }

    public Double getCost_r() {
        return cost_r;
    }

    public void setCost_r(Double cost_r) {
        this.cost_r = cost_r;
    }

}

