package org.nka.rs.entity.constant.commonConstant;

/**
 * Created by zgurskaya on 06.07.2016.
 */
public enum ErrorCodeEnum {

    SUCCESS(1),
    NOMINAL_SUCCESS(2),
    UNSUCCESS(3),
    EXCEPTION(4);

    private final int value;

    ErrorCodeEnum(int value) {
        this.value = value;
    }

    public static ErrorCodeEnum search(Integer value) {
        for (ErrorCodeEnum param : ErrorCodeEnum.values()) {
            if (value.equals(param.getValue())) {
                return param;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }


}
