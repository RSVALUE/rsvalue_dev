package org.nka.rs.entity.dictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 01.06.2016.
 */
@Entity
@Table(name = "X_OPERATIONS")

public class OperationTypeDic {

    @Id
    @Column(name = "ID_CODE", nullable = false)
    private Integer analyticCode;

    //русское наименование
    @Column(name = "CODE_NAME")
    private String codeName;

    //родительский (верхний код)
    @Column(name = "PARENT_CODE")
    private Integer parentCode;

    //родительский (верхний код)
    @Column(name = "REF_CODE")
    private Integer referenceCode;

    //актуальность кода классификатора
    @Column(name = "STATUS", nullable = false)
    private Boolean status;

    public OperationTypeDic() {
    }

    public Integer getAnalyticCode() {
        return analyticCode;
    }

    public void setAnalyticCode(Integer analyticCode) {
        this.analyticCode = analyticCode;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public Integer getParentCode() {
        return parentCode;
    }

    public void setParentCode(Integer parentCode) {
        this.parentCode = parentCode;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(Integer reerenceCode) {
        this.referenceCode = reerenceCode;
    }

}
