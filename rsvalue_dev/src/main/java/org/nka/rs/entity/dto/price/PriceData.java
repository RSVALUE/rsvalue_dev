package org.nka.rs.entity.dto.price;

import java.util.List;

/**
 * Created by zgurskaya on 14.11.2016.
 */
public class PriceData {

    private Long userId;
    private Long priceId;
    private String priceNumber;
    private Integer torId;
    private Double tax;
    private List<Content> priceContents;
    private String startDate;
    private String stopDate;

    public PriceData() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public String getPriceNumber() {
        return priceNumber;
    }

    public void setPriceNumber(String priceNumber) {
        this.priceNumber = priceNumber;
    }

    public List<Content> getPriceContents() {
        return priceContents;
    }

    public void setPriceContents(List<Content> priceContents) {
        this.priceContents = priceContents;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStopDate() {
        return stopDate;
    }

    public void setStopDate(String stopDate) {
        this.stopDate = stopDate;
    }

    public Integer getTorId() {
        return torId;
    }

    public void setTorId(Integer torId) {
        this.torId = torId;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }
}
