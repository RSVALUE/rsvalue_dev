package org.nka.rs.dao.impl;

import org.hibernate.SessionFactory;
import org.nka.rs.dao.IGroupDao;
import org.nka.rs.entity.pojos.common.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GroupDaoImpl extends BaseDaoSessionImpl<Group> implements IGroupDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<String> getGroupsNames() {
        return sessionFactory.getCurrentSession().createQuery("Select g.groupName from Group g").list();
    }

    @Override
    public Integer getIdByName(String name) {
        return (Integer) sessionFactory.getCurrentSession().createQuery("Select g.groupId from Group g where groupName=:groupName").setString("groupName", name).uniqueResult();
    }
}
