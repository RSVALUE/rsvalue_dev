package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.FilterResponse;
import org.nka.rs.entity.pojos.common.UserAccount;
import org.nka.rs.entity.pojos.common.UserData;

import java.io.Serializable;
import java.util.List;

/**
 * Интрейфейс DAO для сущности UserAccount
 */
public interface IUserAccountDao extends IBaseDao<UserAccount>{

    /**
     * Получает объекты UserAccount, начиная с startRecord записи,
     * количеством recordCount
     *
     * @param startRecord Позиция начального элемента
     * @param recordCount Количество записей
     * @return Список объектов UserAccount
     */
    List<UserAccount> getAllElements(Integer startRecord, Integer recordCount);

    /**
     * Добавляет объект UserAccount с данными из userData
     *
     * @param userData Данные, которыми будет заполняться объект UserAccount
     * @return ID объекта
     */
    Serializable addElement(UserData userData);

    /**
     * Получает количество записей в таблице
     *
     * @return Количество записей
     */
    Long getRowCount();

    /**
     * Получает объект UserAccount по ID
     *
     * @param id ID объекта
     * @return Объект UserAccount
     */
    UserAccount getFullAccount(Long id);

    /**
     * Получает объект UserAccount по параметру username
     * @param username Login объекта
     * @return Объект UserAccount
     */
    UserAccount getAccountByName(String username);

    /**
     * Получает список объектов UserAccount по указанным фильтрам,
     * начиная с позиции startRecord, количеством recordCount
     *
     * @param surname Фильтр фамилии
     * @param login Фильтр логина
     * @param org Фильтр организации
     * @param position Фильтра должности
     * @param group Фильтр группы пользователя
     * @param startRecord Позиция стартовой записи в выборке
     * @param recordCount Количество записей
     * @return Список объектов UserAccount
     */
    List<UserAccount> getFilterElements(String surname, String login, String org, String position, String group, Integer startRecord, Integer recordCount);

    /**
     * Получает список объектов UserAccount по указанным фильтрам,
     * начиная с позиции startRecord, количеством recordCount
     *
     * @param surname Фильтр фамилии
     * @param login Фильтр логина
     * @param org Фильтр организации
     * @param group Фильтр группы пользователя
     * @param startRecord Позиция стартовой записи в выборке
     * @param recordCount Количество записей
     * @return Список объектов UserAccount
     */
    List<UserAccount> getFilterElements(String surname, String login, String org, String group, Integer startRecord, Integer recordCount);

    /**
     * Получает количество записей в выборке с указанными
     * фильтрами
     *
     * @param surname Фильтр фамилии
     * @param login Фильтр логина
     * @param org Фильтр организации
     * @param position Фильтра должности
     * @param group Фильтр группы пользователя
     * @return Список объектов UserAccount
     */
    Long getFilterRowCount(String surname, String login, String org, String position, String group);

    /**
     * Получает количество записей в выборке с указанными
     * фильтрами
     *
     * @param surname Фильтр фамилии
     * @param login Фильтр логина
     * @param org Фильтр организации
     * @param group Фильтр группы пользователя
     * @return Список объектов UserAccount
     */
    Long getFilterRowCount(String surname, String login, String org, String group);

    /**
     * Обновляет объект UserAccount данными из userData
     *
     * @param userData Данные для обновления объекта
     * @param check Условие изменения пароля
     * @return ID обновляемого объекта
     */
    Serializable updateElement(UserData userData, boolean check);

    /**
     * Функция фильтрации, использующая функцию на базе, вместо HQL запросов
     *
     * @param surname Фамилия
     * @param login Логин
     * @param org Организация
     * @param group Группы пользователя
     * @param page Номер страницы
     * @param recordCount Количество записей
     * @return Список объектов, помещенных в объект FilterResponse
     */
    List<FilterResponse> getFilterFunction(final String surname, final String login, Integer org, Integer group, Integer page, Integer recordCount);

    int deleteById(Long id);
}
