package org.nka.rs.dao.JDBCDao.orders;

import oracle.jdbc.OracleTypes;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.pojos.order.OrderObject;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.util.connection.UtilConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO класс, в котором содержатся методы для
 * вызова функций на базе по обработке заказа.
 * Типовой алгоритм действий: создается строковая
 * переменная с текстом запроса и Wild-параметрами
 * (input\output), после чего input Wild-параметры заполняются
 * конкретными значениями из входных параметров, а output -
 * в зависимости от результата выполнения функции на базе.
 * Затем выполняется запрос, а его результат записывается в
 * необходимую переменную, или, при надобности, инициализируется
 * соответствующий объект. По сути, все DAO классы, использующие JDBC
 * реализованы таким образом.
 */
public class OrderProcessDAO {

    /**
     * Функция поиска по адресу
     *
     * @param ate ID АТЕ
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData addressSearch(Integer ate, String date) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SEARCH_ADDR1 ( ?,? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;

        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setMessage("Данные получены");
        List<SearchData> searchDataList = new ArrayList<SearchData>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setInt(2, ate);
            callableStatement.setString(3, date);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    searchDataList.add(addSearchData(rs));
                }
            }
            if (!flag) {
                responseWithData = initSuccessResponse(searchDataList);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Функция поиска по кадастровому номер
     *
     * @param soato Номер СОАТО
     * @param blockNum Номер блока
     * @param parcelNum Номер участка
     * @param date Дата поиска
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData cadNumSearch(Long soato, Integer blockNum, Integer parcelNum, String date, Long userId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SEARCH_CADNUM1 ( ?,?,?,?,? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<SearchData> searchDataList = new ArrayList<SearchData>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            setCallableStatementParameters(callableStatement, soato, blockNum, parcelNum, date, userId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    searchDataList.add(addSearchData(rs));
                }
            }
            if (!flag) {
                responseWithData = initSuccessResponse(searchDataList);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Поиск по СТ по кадастровому номеру
     *
     * @param soato Номер СОАТО
     * @param blockNum Номер блока
     * @param parcelNum Номер участка
     * @param date Дата поиска
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData stCadNumSearch(Long soato, Integer blockNum, Integer parcelNum, String date, Long userId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SEARCH_ST_CADNUM ( ?,?,?,?,? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<SearchData> searchDataList = new ArrayList<SearchData>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            setCallableStatementParameters(callableStatement, soato, blockNum, parcelNum, date, userId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    searchDataList.add(addSearchSTData(rs));
                }
            }
            if (!flag) {
                responseWithData = initSuccessResponse(searchDataList);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Функция, возвращающая дальнейший список адресов при поиске по адресу
     *
     * @param region Регион
     * @param area Область
     * @param ss Сельсовет
     * @param snp СНП
     * @param bjd Белорусская Ж\Д
     * @param road Номер дороги
     * @param km Километр дороги
     * @param street Улица
     * @param typeIn Входной тип
     * @param typeOut Выходной тип
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAddressList(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street,
                                           Integer typeIn, Integer typeOut, Long userId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_LIST_ADDR ( ?,?,?,?,?,?,?,?,?,?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<DataInfo> addressList = new ArrayList<DataInfo>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (region != null) {
                callableStatement.setLong(2, region);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (area != null) {
                callableStatement.setLong(3, area);
            } else {
                callableStatement.setNull(3, OracleTypes.NUMBER);
            }
            if (ss != null) {
                callableStatement.setLong(4, ss);
            } else {
                callableStatement.setNull(4, OracleTypes.NUMBER);
            }
            if (snp != null) {
                callableStatement.setLong(5, snp);
            } else {
                callableStatement.setNull(5, OracleTypes.NUMBER);
            }
            if (bjd != null) {
                callableStatement.setLong(6, bjd);
            } else {
                callableStatement.setNull(6, OracleTypes.NUMBER);
            }
            if (road != null) {
                callableStatement.setLong(7, road);
            } else {
                callableStatement.setNull(7, OracleTypes.NUMBER);
            }
            if (km != null) {
                callableStatement.setLong(8, km);
            } else {
                callableStatement.setNull(8, OracleTypes.NUMBER);
            }
            if (street != null) {
                callableStatement.setLong(9, street);
            } else {
                callableStatement.setNull(9, OracleTypes.NUMBER);
            }
            callableStatement.setInt(10, typeIn);
            callableStatement.setInt(11, typeOut);
            callableStatement.setLong(12, userId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                addressList.add(addDataInfo(rs));
            }
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("Данные получены");
            responseWithData.setData(addressList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Функция, возвращающая конечный список адресов
     *
     * @param parentId ID родительского ATE
     * @param ate ID ATE
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAddressElementList(Integer parentId, Long ate) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_LIST_TYPEADDR ( ?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<DataInfo> addressElementList = new ArrayList<DataInfo>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setInt(2, parentId);
            callableStatement.setLong(3, ate);

            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                addressElementList.add(addDataInfo(rs));
            }
            responseWithData = initSuccessResponse(addressElementList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Функция, возвращающая список разрешенных адресов для данного пользователя
     *
     * @param parentId ID родительского ATE
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAteList(Long parentId, Long userId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_ATELIST ( ?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData;
        List<DataInfo> dataInfoList = new ArrayList<DataInfo>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, parentId);
            callableStatement.setLong(3, userId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                dataInfoList.add(addDataInfo(rs));
            }
            responseWithData = initSuccessResponse(dataInfoList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Возвращает список сельских товарищества по данному ATE
     *
     * @param ateId ID ATE
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getSTList(Long ateId, String date) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_LISTST ( ?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData;
        DataInfo dataInfo;
        List<DataInfo> dataInfoList = new ArrayList<DataInfo>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, ateId);
            callableStatement.setString(3, date);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                dataInfo = addDataInfo(rs);
                dataInfo.setType(rs.getInt(3));
                dataInfoList.add(dataInfo);
            }
            responseWithData = initSuccessResponse(dataInfoList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Возвращает список сельхоз земель по данному ATE
     *
     * @param ateId ID ATE
     * @param date Дата поиска
     * @param type Тип поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getSXList(Long ateId, String date, Integer type) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_LISTSX ( ?,?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData;
        List<String> dataInfoList = new ArrayList<String>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, ateId);
            callableStatement.setString(3, date);
            callableStatement.setInt(4, type);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                dataInfoList.add(rs.getString(1));
            }
            responseWithData = initSuccessResponse(dataInfoList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Поиск по сельхоз землям
     *
     * @param type Тип поиска
     * @param text Текст поиска
     * @param ateId ID ATE
     * @param date Дата поиска
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getSXSearch(Integer type, String text, Long ateId, String date) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SEARCH_SX ( ?,?,?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData;
        DataInfo dataInfo;
        List<DataInfo> dataInfoList = new ArrayList<DataInfo>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setInt(2, type);
            callableStatement.setString(3, text);
            callableStatement.setLong(4, ateId);
            callableStatement.setString(5, date);

            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                dataInfo = addDataInfo(rs);
                dataInfo.setCoordinates(rs.getString(3));
                dataInfo.setType(rs.getInt(4));
                dataInfoList.add(dataInfo);
            }
            responseWithData = initSuccessResponse(dataInfoList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Получает объекты заказа по его ID
     *
     * @param orderId ID объекта заказа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData orderProcess(Long orderId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.ORDER_PROCESSING ( ? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();

        List<OrderObjectInfo> orderObjectInfoList = new ArrayList<OrderObjectInfo>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, orderId);

            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    orderObjectInfoList.add(addObjectInfo(rs));
                }
            }
            if (!flag) {
                responseWithData = initGettingObjectSuccessResponse(orderObjectInfoList);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Добавляет объект в заказ
     *
     * @param orderObject Объект с данными необходимыми для добавления в заказ
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData addInOrder(OrderObject orderObject) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.ADD_IN_ORDER ( ?,?,?,?,?,?,?,?,?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<OrderObjectInfo> orderObjectInfoList = new ArrayList<OrderObjectInfo>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, orderObject.getOrderId());
            callableStatement.setInt(3, orderObject.getSearchType());
            callableStatement.setString(4, orderObject.getDate());
            if (orderObject.getDistrId() != null) {
                callableStatement.setLong(5, orderObject.getDistrId());
            } else {
                callableStatement.setNull(5, OracleTypes.NUMBER);
            }
            if (orderObject.getParcelId() != null) {
                callableStatement.setLong(6, orderObject.getParcelId());
            } else {
                callableStatement.setNull(6, OracleTypes.NUMBER);
            }
            if (orderObject.getOrgId() != null) {
                callableStatement.setLong(7, orderObject.getOrgId());
            } else {
                callableStatement.setNull(7, OracleTypes.NUMBER);
            }
            if (orderObject.getAddressId() != null) {
                callableStatement.setLong(8, orderObject.getAddressId());
            } else {
                callableStatement.setNull(8, OracleTypes.NUMBER);
            }
            if (!orderObject.getPrevAddress().equals("")) {
                callableStatement.setString(9, orderObject.getPrevAddress());
            } else {
                callableStatement.setNull(9, OracleTypes.VARCHAR);
            }
            if (!orderObject.getCoordinates().equals("")) {
                callableStatement.setString(10, orderObject.getCoordinates());
            } else {
                callableStatement.setNull(10, OracleTypes.VARCHAR);
            }
            if (orderObject.getsId() != null) {
                callableStatement.setLong(11, orderObject.getsId());
            } else {
                callableStatement.setNull(11, OracleTypes.NUMBER);
            }
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    orderObjectInfoList.add(addObjectInfo(rs));
                }
            }
            if (!flag) {
                responseWithData = initAddingObjectSuccessResponse(orderObjectInfoList);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Удаляет объект из заказа
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData removeFromOrder(Long objId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.DELETE_FROM_ORDER ( ? )}", IConstant.SCHEMA_NAME);

        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<OrderObjectInfo> orderObjectInfoList = new ArrayList<OrderObjectInfo>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, objId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    orderObjectInfoList.add(addObjectInfo(rs));
                }
            }
            if (!flag) {
                responseWithData = initRemoveObjectSuccessResponse(orderObjectInfoList);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Удаляет все объекты из заказа
     *
     * @param orderId ID заказа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData truncateOrder(Long orderId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.TRUNCATE_ORDER ( ? )}", IConstant.SCHEMA_NAME);

        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<OrderObjectInfo> orderObjectInfoList = new ArrayList<OrderObjectInfo>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, orderId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    orderObjectInfoList.add(addObjectInfo(rs));
                }
            }
            if (!flag) {
                responseWithData = initRemoveAllObjectSuccessResponse(orderObjectInfoList);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Получает стоимости для объекта
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData costSearch(Long objId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SEARCH_COST ( ? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<CostObject> costObjectList = new ArrayList<CostObject>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, objId);

            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                responseWithData = resolveCostMessage(rs.getInt(1));
                costObjectList.add(addCostObject(rs));
            }
            responseWithData.setData(costObjectList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Обновляет номера оценочных зон для объекта
     *
     * @param objId ID объекта
     * @param funcCode Код функционального назначения
     * @param zoneId ID новой зоны
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData updateZones(Long objId, Integer funcCode, Long zoneId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.UPDATE_ZONENUMBER ( ?,?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<CostObject> costObjectList = new ArrayList<CostObject>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, objId);
            callableStatement.setInt(3, funcCode);
            callableStatement.setLong(4, zoneId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                responseWithData = resolveCostMessage(rs.getInt(1));
                costObjectList.add(addCostObject(rs));
            }
            responseWithData.setData(costObjectList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Сохраняет новые стоимости по заказу
     *
     * @param orderId ID заказа
     * @param exec ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData saveAllCost(Long orderId, Long exec) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SAVE_ALLCOST ( ?,? )}", IConstant.SCHEMA_NAME);
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        int res;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
            callableStatement.setLong(2, orderId);
            callableStatement.setLong(3, exec);
            callableStatement.execute();

            res = callableStatement.getInt(1);

            responseWithData.setData(res);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Сбрасывает местоположение объекта
     *
     * @param objId ID объекта
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData resetLocation(Long objId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.RESET_LOCATION ( ?,? )}", IConstant.SCHEMA_NAME);

        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<OrderObjectInfo> orderObjectInfo = new ArrayList<OrderObjectInfo>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, objId);
            callableStatement.setNull(3, OracleTypes.VARCHAR);

            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                responseWithData = resolveCostMessage(rs.getInt(1));
                orderObjectInfo.add(addObjectInfo(rs));
            }
            responseWithData.setData(orderObjectInfo);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Сохраняет новые стоимости по объекту
     *
     * @param objId ID объекта
     * @param execId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData confirmOrder(Long objId, Long execId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SAVE_COST ( ?,?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<OrderObjectInfo> orderObjectInfoList = new ArrayList<OrderObjectInfo>();
        boolean flag = false;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, objId);
            callableStatement.setLong(3, execId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                if (flag = checkType(rs, responseWithData)) {
                    break;
                } else {
                    orderObjectInfoList.add(addObjectInfo(rs));
                }
            }
            if (!flag) {
                responseWithData = initGettingObjectSuccessResponse(orderObjectInfoList);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка при записи данных в базу (%s)", e.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка (%s)", e.getMessage()));
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Переопределяет местоположение объекта
     *
     * @param objId ID объекта
     * @param coordinates Коордитаны нового местоположения
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData updateLocation(Long objId, String coordinates) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.UPDATE_LOCATION ( ?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<OrderObjectInfo> orderObjectInfo = new ArrayList<OrderObjectInfo>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, objId);
            callableStatement.setString(3, coordinates);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                responseWithData = resolveCostMessage(rs.getInt(1));
                orderObjectInfo.add(addObjectInfo(rs));
            }
            responseWithData.setData(orderObjectInfo);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Устанавливает входные параметры в объект callableStatement
     * @param callableStatement Объект, в который устанавливаются параметры
     * @param soato СОАТО
     * @param blockNum Номер блока
     * @param parcelNum Номер участка
     * @param date Дата поиска
     * @param userId ID пользователя
     */
    private void setCallableStatementParameters(CallableStatement callableStatement, Long soato, Integer blockNum, Integer parcelNum, String date, Long userId) throws SQLException {
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        callableStatement.setLong(2, soato);
        callableStatement.setInt(3, blockNum);
        callableStatement.setInt(4, parcelNum);
        callableStatement.setString(5, date);
        callableStatement.setLong(6, userId);
    }

    /**
     * Проверяет результат запроса по первому полю
     *
     * @param rs Объект с выборкой по заросу
     * @param responseWithData Объект-ответ, в котором содержатся сведения по запросу
     * @return True, если в результате ничего нельзя вернуть, false, если вернуть можно
     */
    private boolean checkType(ResultSet rs, ResponseWithData responseWithData) throws SQLException {
        boolean flag = false;
        int type = rs.getInt(1);
        if (type == -1 || type == 0) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage("По вашему запросу ничего не найдено");
            flag = true;
        } else if (type == -2) {
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("По данному адресу не найдены координаты");
        } else if (type == 2) {
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("Участи найдены");
        }
        return flag;
    }

    /**
     * Инициализирует объект-ответ успешными данными
     * @param data Данные для объекта-ответа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    private ResponseWithData initSuccessResponse(Object data) {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
        responseWithData.setMessage("Участи найдены");
        responseWithData.setData(data);
        return responseWithData;
    }

    /**
     * Инициализирует объект-ответ успешными данными при получении объектов заказа
     * @param data Данные для объекта-ответа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    private ResponseWithData initGettingObjectSuccessResponse(Object data) {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
        responseWithData.setMessage("Объекты загружены");
        responseWithData.setData(data);
        return responseWithData;
    }

    /**
     * Инициализирует объект-ответ успешными данными при добавлении объекта
     * @param data Данные для объекта-ответа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    private ResponseWithData initAddingObjectSuccessResponse(Object data) {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
        responseWithData.setMessage("Объект успешно добавлен в заказ");
        responseWithData.setData(data);
        return responseWithData;
    }

    /**
     * Инициализирует объект-ответ успешными данными при удалении объекта
     * @param data Данные для объекта-ответа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    private ResponseWithData initRemoveObjectSuccessResponse(Object data) {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
        responseWithData.setMessage("Объект успешно удалён из заказа");
        responseWithData.setData(data);
        return responseWithData;
    }

    /**
     * Инициализирует объект-ответ успешными данными при удалении всех объектов
     * @param data Данные для объекта-ответа
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    private ResponseWithData initRemoveAllObjectSuccessResponse(Object data) {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
        responseWithData.setMessage("Все объекты удалены из заказа");
        responseWithData.setData(data);
        return responseWithData;
    }

    /**
     * Инициализирует объект-ответ неуспешными
     * @return Объект ResponseWithData с кодом, сообщением
     */
    private ResponseWithData initUnsuccessResponse() {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
        responseWithData.setMessage("Ошибка при получении данных из базы.\nОбратитесь к системному адмиристратору");
        return responseWithData;
    }

    /**
     * Инициализирует объект с поисковыми данными из объекта выборки
     * @param rs Объект выборки запроса
     * @return Объект SearchData с данными поиска
     */
    private SearchData addSearchData(ResultSet rs) throws SQLException {
        SearchData searchData = new SearchData();
        searchData.setOrgId(rs.getInt(2));
        searchData.setParcelUID(rs.getInt(3));
        searchData.setCadNum(rs.getString(4));
        searchData.setInfo(rs.getString(5));
        searchData.setSquare(rs.getFloat(6));
        searchData.setPurposeName(rs.getString(7));
        searchData.setCoordinates(rs.getString(8));
        searchData.setType(rs.getInt(9));
        return searchData;
    }

    /**
     * Инициализирует объект с поисковыми данными по СТ из объекта выборки
     * @param rs Объект выборки запроса
     * @return Объект SearchData с данными поиска
     */
    private SearchData addSearchSTData(ResultSet rs) throws SQLException {
        SearchData searchData = new SearchData();
        searchData.setOrgId(rs.getInt(2));
        searchData.setParcelUID(rs.getInt(3));
        searchData.setInfo(rs.getString(4));
        return searchData;
    }

    /**
     * Инициализирует объект с информацией из выборки
     * @param rs Объект выборки запроса
     * @return Объект DataInfo с информацией из выборки
     */
    private DataInfo addDataInfo(ResultSet rs) throws SQLException {
        DataInfo dataInfo = new DataInfo();
        dataInfo.setId(rs.getLong(1));
        dataInfo.setInfo(rs.getString(2));
        return dataInfo;
    }

    /**
     * Инициализирует объект заказа информацией из выборки
     * @param rs Объект выборки запроса
     * @return Объект OrderObjectInfo c информаций из выборки
     */
    private OrderObjectInfo addObjectInfo(ResultSet rs) throws SQLException {
        OrderObjectInfo orderObjectInfo = new OrderObjectInfo();
        orderObjectInfo.setObjectId(rs.getLong(2));
        orderObjectInfo.setOrderId(rs.getLong(3));
        orderObjectInfo.setCadNum(rs.getString(4));
        orderObjectInfo.setInfo(rs.getString(5));
        orderObjectInfo.setSquare(rs.getFloat(6));
        orderObjectInfo.setDate(rs.getString(7));
        orderObjectInfo.setPurpose(rs.getString(8));
        orderObjectInfo.setDocType(rs.getInt(9));
        orderObjectInfo.setDocName(rs.getString(10));
        orderObjectInfo.setDocNum(rs.getString(11));
        orderObjectInfo.setIsDefine(rs.getInt(12));
        orderObjectInfo.setIsNb(rs.getInt(13));
        orderObjectInfo.setIsNk(rs.getInt(14));
        orderObjectInfo.setCoordinates(rs.getString(15));
        return orderObjectInfo;
    }

    /**
     * Инициализирует данные по стоимостях объекта заказа информацией из выборки
     * @param rs Объект выборки запроса
     * @return Объект CostObject c информаций из выборки
     */
    private CostObject addCostObject(ResultSet rs) throws SQLException {
        CostObject costObject = new CostObject();
        costObject.setFuncCode(rs.getInt(2));
        costObject.setFuncName(rs.getString(3));
        costObject.setCostDate(rs.getString(4));
        costObject.setZoneNumber(rs.getString(5));
        costObject.setZoneName(rs.getString(6));
        costObject.setCostUSD(rs.getDouble(7));
        costObject.setCostBYN(rs.getDouble(8));
        costObject.setCostNB(rs.getDouble(9));
        costObject.setZones(rs.getString(10));
        costObject.setZoneId(rs.getLong(11));
        return costObject;
    }

    /**
     * Проверяет результат запроса и в зависимости от значения устанавливает
     * необходимое сообщение
     *
     * @param type Результат запроса
     * @return Объект ResponseWithData с кодом и сообщением
     */
    private ResponseWithData resolveCostMessage(int type) {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(type);
        switch(type) {
            case -1:
                responseWithData.setMessage("Для определения оценочных зон измените местоположение объекта с помощью поиска по карте");
                break;
            case -2:
                responseWithData.setMessage("Для данного объекта изменение местоположения не доступно");
                break;
            case -3:
                responseWithData.setMessage("Для данного объекта это действие не доступно");
                break;
            case -4:
                responseWithData.setMessage("Геоданные объекта не найдены. Воспользуйтеь поиском по карте");
                break;
            case -5:
                responseWithData.setMessage("Внимание! Система определила стоимость не по всем функциональным назначениям");
                break;
            default:
                responseWithData.setMessage("");
                break;
        }
        return responseWithData;
    }
}

/**
 * Вспомогательный класс с полями для объектов, получившихся
 * в результате поиска
 */
class SearchData {

    private Integer orgId;

    private Integer parcelUID;

    private String cadNum;

    private String info;

    private Float square;

    private String purposeName;

    private String coordinates;

    private Integer type;

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getParcelUID() {
        return parcelUID;
    }

    public void setParcelUID(Integer parcelUID) {
        this.parcelUID = parcelUID;
    }

    public String getCadNum() {
        return cadNum;
    }

    public void setCadNum(String cadNum) {
        this.cadNum = cadNum;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Float getSquare() {
        return square;
    }

    public void setSquare(Float square) {
        this.square = square;
    }

    public String getPurposeName() {
        return purposeName;
    }

    public void setPurposeName(String purposeName) {
        this.purposeName = purposeName;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SearchData{" +
                "orgId=" + orgId +
                ", parcelUID=" + parcelUID +
                ", info='" + info + '\'' +
                ", square=" + square +
                ", purposeName='" + purposeName + '\'' +
                ", coordinates='" + coordinates + '\'' +
                '}';
    }
}

/**
 * Вспомогательный класс с полями для объектов, получившихся
 * в результате поиска
 */
class DataInfo {

    private Long id;

    private String info;

    private String coordinates;

    private Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}

/**
 * Вспомогательный класс с полями для объектов заказа
 */
class OrderObjectInfo {

    private Long objectId;

    private Long orderId;

    private String cadNum;

    private String info;

    private Float square;

    private String date;

    private String purpose;

    private Integer docType;

    private String docName;

    private String docNum;

    private Integer isDefine;

    private Integer isNb;

    private Integer isNk;

    private String coordinates;

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getCadNum() {
        return cadNum;
    }

    public void setCadNum(String cadNum) {
        this.cadNum = cadNum;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Float getSquare() {
        return square;
    }

    public void setSquare(Float square) {
        this.square = square;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Integer getDocType() {
        return docType;
    }

    public void setDocType(Integer docType) {
        this.docType = docType;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public Integer getIsDefine() {
        return isDefine;
    }

    public void setIsDefine(Integer isDefine) {
        this.isDefine = isDefine;
    }

    public Integer getIsNb() {
        return isNb;
    }

    public void setIsNb(Integer isNb) {
        this.isNb = isNb;
    }

    public Integer getIsNk() {
        return isNk;
    }

    public void setIsNk(Integer isNk) {
        this.isNk = isNk;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }
}

/**
 * Вспомогательный класс с полями для стоимостей объекта заказа
 */
class CostObject {

    private Integer funcCode;

    private String funcName;

    private String costDate;

    private String zoneNumber;

    private String zoneName;

    private Double costUSD;

    private Double costBYN;

    private Double costNB;

    private String zones;

    private Long zoneId;

    public Integer getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(Integer funcCode) {
        this.funcCode = funcCode;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getCostDate() {
        return costDate;
    }

    public void setCostDate(String costDate) {
        this.costDate = costDate;
    }

    public String getZoneNumber() {
        return zoneNumber;
    }

    public void setZoneNumber(String zoneNumber) {
        this.zoneNumber = zoneNumber;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public Double getCostUSD() {
        return costUSD;
    }

    public void setCostUSD(Double costUSD) {
        this.costUSD = costUSD;
    }

    public Double getCostBYN() {
        return costBYN;
    }

    public void setCostBYN(Double costBYN) {
        this.costBYN = costBYN;
    }

    public Double getCostNB() {
        return costNB;
    }

    public void setCostNB(Double costNB) {
        this.costNB = costNB;
    }

    public String getZones() {
        return zones;
    }

    public void setZones(String zones) {
        this.zones = zones;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public CostObject() {
    }
}