package org.nka.rs.dao;

import org.nka.rs.entity.dictionary.TORDic;

import java.util.List;

/**
 * Интрейфейс DAO для сущности Tor
 */
public interface ITorDao extends IBaseDao<TORDic>{

    /**
     * Возвращает список с кодами Торов
     *
     * @return Список с кодами
     */
    List<String> getCodeNames();

    /**
     * Возвращает код Тора по имени
     *
     * @param name Имя Тора
     * @return Код Тора
     */
    Integer getCodeByName(String name);

}
