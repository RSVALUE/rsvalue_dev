package org.nka.rs.dao.impl;

import org.hibernate.SessionFactory;
import org.nka.rs.dao.ISubjectDao;
import org.nka.rs.entity.pojos.common.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubjectDaoImpl extends BaseDaoSessionImpl<Subject> implements ISubjectDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<String> getSubjectNames() {
        //TODO remote procedure surname + firstname + fathername, do the service for this dao
        return null;
    }

    @Override
    public Subject getSubjectByFullName(String surname, String firstName, String fatherName) {
        return (Subject) sessionFactory.getCurrentSession()
                .createQuery("from Subject s where s.surname=:surname and s.firstName=:firstName and s.fatherName=:fatherName")
                .setString("surname", surname).setString("firstName", firstName).setString("fatherName", fatherName)
                .uniqueResult();
    }

    @Override
    public Long getIdByFullName(String surname, String firstName, String fatherName) {
        return (Long) sessionFactory.getCurrentSession()
                .createQuery("Select s.id from Subject s where s.surname=:surname and s.firstName=:firstName and s.fatherName=:fatherName")
                .setString("surname", surname).setString("firstName", firstName).setString("fatherName", fatherName)
                .uniqueResult();
    }

    @Override
    public String getFullNameById(Long id) {
        return (String) sessionFactory.getCurrentSession()
                .createQuery("Select u.subject.surname || ' ' || u.subject.firstName || ' ' || u.subject.fatherName from UserAccount u where u.userEntity.id=:id")
                .setLong("id", id)
                .uniqueResult();
    }

    @Override
    public List<Subject> getAllElements(Class<Subject> clazz) {
        return (List<Subject>) sessionFactory.getCurrentSession().createCriteria(clazz).list();
    }
}
