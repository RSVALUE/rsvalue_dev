package org.nka.rs.dao;

import org.json.JSONObject;
import org.nka.rs.entity.dto.LoadData;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;

import java.sql.Clob;
import java.util.List;

/**
 * Created by zgurskaya on 10.06.2016.
 */
public interface ILoadDataDao  {

    ResponseWithData loadData(LoadData data);

    String saveZonesGeometry(JSONObject geometry, Long dwd_id);

    String saveBoundsGeometry(JSONObject geometry, Long dwd_id);

    Response saveLayerGeometry(JSONObject geometry, Long dwd_id, String layerName);

    List getNationalCurrency();

    List getForeignCurrency();

    Clob clearErrorLoad(Long regNum);


}
