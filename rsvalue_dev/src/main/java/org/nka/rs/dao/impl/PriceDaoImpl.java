package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.nka.rs.dao.IPriceDao;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.OperationTypeDic;
import org.nka.rs.entity.dictionary.OrderingDocumentTypeDic;
import org.nka.rs.entity.dictionary.SubjectTypeDic;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.dto.price.Content;
import org.nka.rs.entity.dto.price.PriceCommonInfo;
import org.nka.rs.entity.dto.price.PriceData;
import org.nka.rs.entity.dto.price.SimplePrice;
import org.nka.rs.entity.pojos.price.GetPriceObj;
import org.nka.rs.entity.pojos.price.Price;
import org.nka.rs.entity.pojos.price.PriceContent;
import org.nka.rs.entity.pojos.price.PriceEntity;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 10.10.2016.
 */
@Repository
public class PriceDaoImpl implements IPriceDao, IConstant {


    @Autowired
    SessionFactory sessionFactory;

    //+
    @Override
    public List<TORDic> getTorList(Long userId) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<TORDic> torList = new ArrayList<TORDic>();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_PRICE.GET_LIST_TOR(?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, userId);
            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                TORDic tor = new TORDic();
                tor.setAnalyticCode(rs.getInt(1));
                tor.setCodeName(rs.getString(2));
                torList.add(tor);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return torList;
    }

    @Override
    public Object[] getPriceListWithSorting(String priceNum, Integer sortingParam, String predicate, Integer page, Integer amount, Long userId, Integer torId) {
        //[0] - список данных по прайсам, [1] - количество страниц, [2] - количество записей всего
        Object[] result = new Object[3];

        Connection connection = null;
        CallableStatement call = null;
        ResultSet rs = null;

        List<PriceCommonInfo> priceList = new ArrayList<PriceCommonInfo>();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_PRICE.GET_LISTPRICE(?,?,?,?,?,?,?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, userId);
            if (torId != null) {
                call.setInt(3, torId);
            } else {
                call.setNull(3, OracleTypes.NUMBER);
            }
            if (priceNum != null && !priceNum.equals("")) {
                call.setString(4, priceNum);
            } else {
                call.setNull(4, OracleTypes.VARCHAR);
            }
            if (sortingParam != null) {
                call.setInt(5, sortingParam);
            } else {
                call.setNull(5, OracleTypes.NUMBER);
            }
            if (predicate != null && !predicate.equals("")) {
                call.setString(6, predicate);
            } else {
                call.setNull(6, OracleTypes.VARCHAR);
            }
            call.setInt(7, page);
            call.setInt(8, amount);
            call.execute();
            rs = (ResultSet) call.getObject(1);

            Integer amountAll = null;

            while (rs.next()) {
                PriceCommonInfo price = new PriceCommonInfo();
                price.setPriceId(rs.getLong(1));
                price.setPriceNumber(rs.getString(2));
                price.setStartDate(rs.getDate(3));
                price.setStopDate(rs.getDate(4));
                price.setUserInfo(rs.getString(5));
                amountAll = rs.getInt(6);
                priceList.add(price);
            }
            result[0] = priceList;
            if (amountAll != null) {
                if (amountAll % amount == 0) {
                    result[1] = amountAll / amount;
                } else {
                    result[1] = amountAll / amount + 1;
                }
            }
            result[2] = amountAll;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public ResponseWithData getPrice(Long priceId, Long userId) {
        Connection connection = null;
        CallableStatement call = null;
        ResultSet rs = null;
        ResponseWithData response = new ResponseWithData();
        Price price = new Price();
        List<PriceContent> contents = new ArrayList<PriceContent>();

        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_PRICE.GET_PRICE_DETAIL(?,?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, priceId);
            call.setLong(3, userId);
            call.execute();

            rs = (ResultSet) call.getObject(1);

            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy");
            while (rs.next()) {
                Integer isEdit = rs.getInt(1);
                if (isEdit.equals(1)) {
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                } else {
                    response.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                    response.setMessage("У Вас нет прав для редактирования данного прейскуранта!");
                }

                String priceData = rs.getString(2);
                String[] part = priceData.split(";");
                TORDic tor = new TORDic();
                if (part != null && part.length != 0) {
                    if (part[0] != null && !part[0].equals("")) {
                        price.setPriceId(Long.parseLong(part[0]));
                    }
                    if (part[1] != null && !part[1].equals("")) {
                        price.setPriceNumber(part[1]);
                    }
                    if (part[2] != null && !part[2].equals("")) {
                        price.setStartDate(formatter.parse(part[2]));
                    }
                    if (part[3] != null && !part[3].equals("")) {
                        price.setStopDate(formatter.parse(part[3]));
                    }
                    if (part[4] != null && !part[4].equals("")) {
                        tor.setAnalyticCode(Integer.parseInt(part[4]));
                    }
                    if (part[5] != null && !part[5].equals("")) {
                        tor.setCodeName(part[5]);
                    }
                    if (part[6] != null && !part[6].equals(""))
                        price.setTax(Double.parseDouble(part[6]));
                }
                price.setTor(tor);
                PriceContent content = new PriceContent();
                SubjectTypeDic subjType = new SubjectTypeDic();
                OrderingDocumentTypeDic docType = new OrderingDocumentTypeDic();
                subjType.setAnalyticCode(rs.getInt(3));
                subjType.setCodeName(rs.getString(4));
                docType.setAnalyticCode(rs.getInt(5));
                docType.setCodeName(rs.getString(6));
                content.setDocType(docType);
                content.setSubjectType(subjType);
                content.setPriceUrg(rs.getDouble(7));
                content.setNdsUrg(rs.getDouble(8));
                content.setPriceNonUrg(rs.getDouble(9));
                content.setNdsNonUrg(rs.getDouble(10));
                contents.add(content);
            }
            price.setPriceContents(contents);
            response.setData(price);

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении данных. Обратитесь к администратору!");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    public Response validatePriceNumber(Integer torId, String priceNumber) {
        Connection connection = null;
        CallableStatement call = null;
        Response response = new Response();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_PRICE.VERIFY_PRICENUM(?,?)}");
            call.registerOutParameter(1, OracleTypes.NUMBER);
            call.setInt(2, torId);
            call.setString(3, priceNumber.trim());
            call.execute();

            Integer isExist = call.getInt(1);
            if (isExist.equals(1)) {
                response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            } else {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("У Вас имеется прейскурант с таким номером. Измените номер прейскуранта!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении данных. Обратитесь к администратору!");
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    public ResponseWithData getPricePattern(Long userId) {
        Connection connection = null;
        CallableStatement call = null;
        ResultSet rs = null;
        ResponseWithData response = new ResponseWithData();
        Price price = new Price();
        List<PriceContent> contents = new ArrayList<PriceContent>();

        try {

            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_PRICE.GET_PRICE_NEW(?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, userId);
            call.execute();

            rs = (ResultSet) call.getObject(1);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            while (rs.next()) {
                Integer isActivPrice = rs.getInt(1);
                if (isActivPrice.equals(1)) {
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                } else {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Невозможно создать новый прейскурант, т.к. у Вас имеется действующий. Проставьте дату окончания действия актуального прейскуранта!");
                    return response;
                }
                String priceData = rs.getString(2);
                String[] part = priceData.split(";");
                TORDic tor = new TORDic();
                if (part != null && part.length != 0) {
                    if (part[0] != null && !part[0].equals("")) {
                        price.setTax(Double.parseDouble(part[0]));
                    }
                    if (part[1] != null && !part[1].equals("")) {
                        price.setStartDate(formatter.parse(part[1]));
                    }
                    if (part[2] != null && !part[2].equals("")) {
                        tor.setAnalyticCode(Integer.parseInt(part[2]));
                    }
                    if (part[3] != null && !part[3].equals("")) {
                        tor.setCodeName(part[3]);
                    }
                }
                price.setTor(tor);
                PriceContent content = new PriceContent();
                SubjectTypeDic subjType = new SubjectTypeDic();
                OrderingDocumentTypeDic docType = new OrderingDocumentTypeDic();
                subjType.setAnalyticCode(rs.getInt(3));
                subjType.setCodeName(rs.getString(4));
                docType.setAnalyticCode(rs.getInt(5));
                docType.setCodeName(rs.getString(6));
                content.setDocType(docType);
                content.setSubjectType(subjType);
                content.setPriceUrg(rs.getDouble(7));
                content.setNdsUrg(rs.getDouble(8));
                content.setPriceNonUrg(rs.getDouble(9));
                content.setNdsNonUrg(rs.getDouble(10));
                contents.add(content);
            }
            price.setPriceContents(contents);
            response.setData(price);

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(4);
            response.setMessage("Ошибка при получении данных. Обратитесь к администратору!");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }


    //GET PRICE FROM BD
    @Override
    public GetPriceObj getPriceNew (Long userId, Long priceId) {
        Connection connection = null;
        CallableStatement call = null;
        ResultSet rs = null;
        GetPriceObj response = new GetPriceObj();
        ArrayList<PriceEntity> priceEntities = new ArrayList<PriceEntity>();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_PRICE.CREATE_EDIT_VIEW(?,?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, priceId);
            call.setLong(3, userId);
            call.execute();

            rs = (ResultSet) call.getObject(1);

            int i=0, code = 0;
            while (rs.next()) {
                if(i==0){
                    code = rs.getInt(1);
                    response.setType(code);
                    if(code ==1 || code == 0){
                    String[] inf = (rs.getString(2)).split(";");
                        response.setPriceId(Integer.parseInt(inf[0]));
                        response.setNumber_price(inf[1]);
                        response.setStart_date(inf[2]);
                        response.setStop_date(inf[3]);
                        response.setTor(Integer.parseInt(inf[4]));
                        response.setTorName(inf[5]);
                        //response.setNds(Double.parseDouble(inf[6]));
                    }else if (code == 3) {
                        String[] inf = (rs.getString(2)).split(";");
                        response.setNds(Double.parseDouble(inf[0]));
                        response.setStart_date(inf[1]);
                        response.setTor(Integer.parseInt(inf[2]));
                        response.setTorName(inf[3]);
                    }
                }
                if(code == 3){
                    priceEntities.add(new PriceEntity(rs.getLong(3), rs.getString(4), rs.getLong(5), rs.getString(6), 0,0,0,0,0,0));
                    response.setNds(rs.getDouble(11));
                }else if(code != -1){
                    priceEntities.add(new PriceEntity(rs.getLong(3), rs.getString(4), rs.getLong(5), rs.getString(6), rs.getFloat(10), rs.getFloat(11), rs.getFloat(7), rs.getFloat(8), rs.getFloat(12), rs.getFloat(9)));
                    response.setNds(rs.getDouble(13));
                }
                i++;
            }
            response.setPriceEntity(priceEntities);

        } catch (Exception e) {
            e.printStackTrace();
            response.setType(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении данных. Обратитесь к администратору!");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.getMessage();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.getMessage();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.getMessage();
            }
        }
        return response;
    }

    // +
    @Override
    public Response stopPrice(Long priceId, String date, Long userId, Integer operTypeId) {
        CallableStatement call = null;
        Connection connection = null;

        Response response = new Response();
        response.setCode(ErrorCodeEnum.SUCCESS.getValue());
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{= call " + SCHEMA_NAME + ".PKG_PRICE.DEACT_PRICE(?,?,?,?)}");
            call.setLong(1, priceId);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date stopDate = formatter.parse(date);
                java.sql.Date sqlDate = new java.sql.Date(stopDate.getTime());
                call.setDate(2, sqlDate);
            } catch (ParseException e) {
                e.printStackTrace();
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Ошибка при преобразовании даты. Попробуйте еще раз!");
            }
            call.setLong(3, userId);
            call.setInt(4, operTypeId);

            call.execute();
        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при записи даты окончания действия прайса в базу. Попробуйте еще раз!");
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    // +
    @Override
    public Response savePrice(PriceData price) {
        CallableStatement call = null;
        Connection connection = null;
        Response response = new Response();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{ = call " + SCHEMA_NAME + ".PKG_PRICE.ADD_PRICE(?,?,?,?,?,?,?)}");

            call.setString(1, price.getPriceNumber().trim());
            call.setInt(2, price.getTorId());
            call.setDate(3, new java.sql.Date(formatter.parse(price.getStartDate()).getTime()));
            if (price.getStopDate() != null) {
                call.setDate(4, new java.sql.Date(formatter.parse(price.getStopDate()).getTime()));
            } else {
                call.setNull(4, OracleTypes.DATE);
            }

            List<Content> contents = price.getPriceContents();
            String contentsStr = "";
            for (int i = 0; i < contents.size(); i++) {
                Content content = contents.get(i);
                contentsStr += content.getSubjectTypeId() + "," + content.getDocTypeId() +
                        "," + content.getPriceUrg() + "," + content.getPriceNonUrg() + ";";
            }
            call.setString(5, contentsStr);
            call.setLong(6, price.getUserId());
            if (price.getPriceId() != null) {
                call.setLong(7, price.getPriceId());
            } else {
                call.setNull(7, OracleTypes.NUMBER);
            }
            call.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    public List<OperationTypeDic> getOperType() {
        Session session = null;
        List<OperationTypeDic> result = null;
        try {
            session = sessionFactory.getCurrentSession();
            Query q = session.createQuery("From OperationTypeDic operType where operType.status = :relevant " +
                    " and operType.parentCode = :deact  order by operType.analyticCode");
            q.setBoolean("relevant", RELEVANT);
            q.setInteger("deact", PRICE_DEACT);

            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    @Override
    public Response savePriceNew(SimplePrice price) {
        CallableStatement call = null;
        Connection connection = null;
        Response response = new Response();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{ = call " + SCHEMA_NAME + ".PKG_PRICE.ADD_PRICE(?,?,?,?,?,?)}");

            call.setString(1, price.getNUMBER_PRICE());
            call.setString(2, price.getSTART_DATE());
            call.setString(3, price.getSTOP_DATE());
            call.setString(4, price.getCONTENT());
            call.setLong(5, price.getEXEC());
            call.setLong(6, price.getPREVID());
            call.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }
}
