package org.nka.rs.dao;

import org.nka.rs.entity.dto.*;
import org.nka.rs.entity.responseVar.Response;

import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */

public interface IRegistrDTODao extends IBaseDao {


    //получить список объектов журнала загрузки
    Object[] getRegistr(DataForRegistrWork param);
    //получить список объектов журнала загрузки с настраиваемыми табицами
    /*Object[] getManagedRegistr(DataForRegistrWork param);
    Object[] getAllDataRecords(DataForRegistrWork param);*/
    //получить запись об объекте загрузки по идентификатору
    Registr getUniqueRecord(Long regNum);
    Registr getUniqueRecord(Long regNum, String funCode);
    //получить список документов для данной загрузки
    List<DocumentData> getDocumentList(Long regNum);
    //получить список тематических слоев для данной загрузки
    List<ThematicLayer> getLayerList(Long regNum);
    //получить список зон для данной загрузки
    Object[] getZoneList(Long regNum, Integer sortingParam, String predicate, Integer page);
    //получить список зон данной загрузки с настраиваемой таблицей
    Object[] getManagedZoneList(Long regNum, Integer [] columnNums, Integer sortingParam, String predicate, Integer page);
    //получить АТЕ по части наименования
    List searchByATE(String partName);
    //скачивание документа (
    String downloadDoc(Long docId);
    //получение списка необязательных документов для данной загрузки
    Object[] getLoadInfo(Long regNum);
    //редактирование документов (в том числе и тематических слоев)
    Response saveEditDoc(EditDocumentData data);
    //загрузка нового документа для существующей загрузки (редактирование) - неактивен до сохранения изменений
    Long editFile(String path, Integer docTypeId, Long userId, Long regNum);
    //поиск кода типа слоя по наименованию
    Integer findLayerCodeByLayerFileName(String layerName);
    //загрузка нового тематического слоя для существующей загрузки - неактивен до сохранения изменений
    Response insertEditLayer(Long reg_num, String layerName, Long userId, String values);



}

