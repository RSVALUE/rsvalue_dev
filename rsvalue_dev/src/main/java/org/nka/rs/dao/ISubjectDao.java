package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.Subject;

import java.util.List;

/**
 * Интрейфейс DAO для сущности Subject
 */
public interface ISubjectDao extends IBaseDao<Subject>{

    /**
     * Получает список ФИО из всех сущностей таблицы
     *
     * @return Список ФИО
     */
    List<String> getSubjectNames();

    /**
     * Получает объект по имени, фамилии, отчеству
     *
     * @param surname Фамилия
     * @param firstName Имя
     * @param fatherName Отчество
     * @return объект Subject
     */
    Subject getSubjectByFullName(String surname, String firstName, String fatherName);

    /**
     * Получает ID объекта по имени, фамилии, отчеству
     *
     * @param surname Фамилия
     * @param firstName Имя
     * @param fatherName Отчество
     * @return ID объекта
     */
    Long getIdByFullName(String surname, String firstName, String fatherName);

    /**
     * Получает ФИО объекта по ID
     *
     * @param id ID объекта
     * @return ФИО объекта
     */
    String getFullNameById(Long id);
}
