package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.UserEntity;

import java.util.List;

/**
 * Интрейфейс DAO для сущности UserEntity
 */
public interface IUserEntityDao extends IBaseDao<UserEntity>{

    /**
     * Получается объект UserEntity по логину
     * @param username Логин пользователя
     * @return объект UserEntity
     */
    UserEntity getUserByName(String username);

    /**
     * Получает ID объекта по логину
     * @param username Логин пользователя
     * @return ID объекта
     */
    Long getIdByName(String username);

    /**
     * Получает список всех логинов
     * @return Список логинов
     */
    List<String> getLogins();

    /**
     * Производит выход пользователя из системы
     * @param username Логин пользователя
     * @param token Токен пользователя
     */
    void logout(String username, String token);

    /**
     * Создает и сохраняет токен для пользователя
     * @param id ID пользователя
     * @param username Логин пользователя
     * @return Токен для пользователя
     */
    String createAndSaveToken(Long id, String username);

    /**
     * Получает токен пользователя
     * @param id ID пользователя
     * @return Токен для пользователя
     */
    String getToken(Long id);
}
