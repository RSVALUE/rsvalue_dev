package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.Group;

import java.util.List;

/**
 * Интрейфейс DAO для сущности Group
 */
public interface IGroupDao extends IBaseDao<Group>{

    /**
     * Возвращается список с названиями групп пользователей
     *
     * @return Список всех групп
     */
    List<String> getGroupsNames();

    /**
     * Получает ID группы по имени
     *
     * @param name Имя группы
     * @return ID группы
     */
    Integer getIdByName(String name);
}
