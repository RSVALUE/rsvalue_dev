package org.nka.rs.dao;

import org.nka.rs.entity.dto.DocInfo;

/**
 * Created by zgurskaya on 21.06.2016.
 */
public interface IDocumentLoadDao extends IBaseDao {

    Long saveDoc(DocInfo docInfo, String path, Long userId, Long commonDocId);
    String deleteDoc(Long docId) ;
    Long getCommonDocId();
}
