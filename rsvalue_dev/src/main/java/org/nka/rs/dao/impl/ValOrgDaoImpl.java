package org.nka.rs.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.nka.rs.dao.IValOrgDao;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.ValueOrganizationDic;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zgurskaya on 29.09.2016.
 */

@Repository
public class ValOrgDaoImpl extends BaseDaoSessionImpl implements IValOrgDao, IConstant {

    @Override
    public List<ValueOrganizationDic> getActualOrganization() {
        Session session = null;
        List<ValueOrganizationDic> result = null;
        try {
            session = sessionFactory.getCurrentSession();

            Query q = session.createQuery("From ValueOrganizationDic valOrg where valOrg.status = :relevant");

            q.setBoolean("relevant", RELEVANT);

            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }
}
