package org.nka.rs.dao.JDBCDao;

import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.pojos.security.UserDetail;
import org.nka.rs.entity.security.UserAuthorization;
import org.nka.rs.util.Util;
import org.nka.rs.util.connection.UtilConnection;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by Shcherbina on 26.11.2016.
 */
public class AuthenticationDao {
    public UserAuthorization isAuth(String login, String hash) {
        String select = String.format("select * from %s.USERS t WHERE t.username='" + login
                + "' AND t.password='" + hash + "'", IConstant.SCHEMA_NAME);
        UserAuthorization user = new UserAuthorization();
        try {
            Statement statement = UtilConnection.getJDBCMySqlConnection().createStatement();
            ResultSet rs = statement.executeQuery(select);
            while (rs.next()) {
                user.setCode(1);
                user.setLogin(rs.getString("USERNAME"));
                user.setUserId(rs.getInt("USER_ID"));
                user.setToken(Util.createToken(login));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public UserDetail returnUser(String login, String token) {
        String select = "SELECT SU.SURNAME, SU.FIRSTNAME,SU.FATHERNAME,SU.ACTUAL,AC.ACCOUNT_ID, AC.REGCODE,T.GROUP_ID, T.USERNAME,T.ENABLED\n" +
                "FROM USER_ACCOUNTS AC\n" +
                "LEFT JOIN SUBJECTS SU ON AC.SUBJECT_ID = SU.SUBJECT_ID\n" +
                "LEFT JOIN USERS T ON AC.USER_ID=T.USER_ID\n" +
                "\n" +
                "where exists (select * from user_detail ud where ud.username = t.username and ud.token ='" + token + "' )\n" +
                "and t.username = '" + login + "'";

        UserDetail user = new UserDetail();
        //Закомментил, чтобы компилировалось
//        try {
//            Statement statement = UtilConnection.getJDBCMySqlConnection().createStatement();
//            ResultSet rs = statement.executeQuery(select);
//            while (rs.next()) {
//                user.setCode(1);
//                user.setLogin(rs.getString("USERNAME"));
//                user.setUserId(rs.getInt("USER_ID"));
//                user.setToken(Util.createToken(login));
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        return user;
    }
}
