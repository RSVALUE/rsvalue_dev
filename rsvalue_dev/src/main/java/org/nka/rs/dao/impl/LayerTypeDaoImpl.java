package org.nka.rs.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.nka.rs.dao.ILayerTypeDao;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.LayerTypeDic;
import org.nka.rs.entity.dto.LayerType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 09.06.2016.
 */

@Repository
public class LayerTypeDaoImpl extends BaseDaoSessionImpl implements ILayerTypeDao, IConstant {

    @Override
    public List<LayerType> getValideLayers(List<String> layerNames) {
        Session session = null;
        String result = null;
        List<LayerType> layerTypeList = new ArrayList<LayerType>();
        try {
            session = sessionFactory.getCurrentSession();
            for (String s : layerNames) {
                Query q = session.createQuery("select lay.shortName FROM LayerTypeDic lay " +
                        " where upper(:layerName) like (lay.fileName||'%' )");

                q.setString("layerName", s);
                result = (String) q.uniqueResult();
                if (result != null && !result.equals("")) {
                    LayerType type = new LayerType();
                    type.setLayerName(s);
                    type.setLayerTypeName(result);
                    layerTypeList.add(type);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return layerTypeList;
    }

    @Override
    public List<Integer> getGroundCodeValue() {
        Session session = null;
        List<Integer> groundCodeList = new ArrayList<Integer>();
        try {
            session = sessionFactory.getCurrentSession();
                Query q = session.createQuery("select ground.analyticCode FROM GroundQualityDic ground " +
                        " where ground.status = :relevant ");
            q.setBoolean("relevant", RELEVANT);
            groundCodeList = q.list();

            } catch (Exception e) {
            e.printStackTrace();
        }
        return groundCodeList;
    }

    @Override
    public List<Integer> getDistanceCodeValue() {
        Session session = null;
        List<Integer> disnabceCodeList = new ArrayList<Integer>();
        try {
            session = sessionFactory.getCurrentSession();
            Query q = session.createQuery("select distance.analyticCode FROM TimeToCentreDic distance " +
                    " where distance.status = :relevant ");
            q.setBoolean("relevant", RELEVANT);
            disnabceCodeList = q.list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return disnabceCodeList;
    }

    @Override
    public List<LayerTypeDic> getActualLayers() {
        Session session = null;
        List<LayerTypeDic> result = null;
        try {
            session = sessionFactory.getCurrentSession();

            Query q = session.createQuery("From LayerTypeDic layer where layer.status = :relevant");

            q.setBoolean("relevant", RELEVANT);

            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }
}
