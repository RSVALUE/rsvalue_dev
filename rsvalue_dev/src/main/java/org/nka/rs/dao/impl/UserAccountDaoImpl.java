package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.jdbc.ReturningWork;
import org.nka.rs.dao.IUserAccountDao;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.pojos.common.FilterResponse;
import org.nka.rs.entity.pojos.common.UserAccount;
import org.nka.rs.entity.pojos.common.UserData;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserAccountDaoImpl extends BaseDaoSessionImpl<UserAccount> implements IUserAccountDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<UserAccount> getAllElements(Integer startRecord, Integer recordCount) {
        return sessionFactory.getCurrentSession().createQuery
                ("Select ua.id, ua.profession, ua.regCode.codeName, ua.subject.surname, ua.subject.firstName, " +
                        "ua.subject.fatherName, ua.userEntity.username, ua.userEntity.group.groupName " +
                        "from UserAccount ua order by ua.id")
                .setFirstResult(startRecord)
                .setMaxResults(recordCount)
                .list();
    }

    /**
     * Функция вызывает метод текущей сессии, с помощью которого можно
     * вызвать хранимые на базе функции. Далее создается запрос для
     * функции на базе, парсится объект userData и заполняются поля для
     * запроса, после чего функция выполняется.
     *
     * @param userData Данные, которыми будет заполняться объект UserAccount
     * @return ID нового объекта
     */
    @Override
    public Serializable addElement(final UserData userData) {
        final String[] fullName = userData.getFio().split(" ");
        return sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<Serializable>() {
            @Override
            public Serializable execute(Connection connection) throws SQLException {
                CallableStatement callableStatement = null;
                String sqlString = String.format("{? = call %s.PKG_USERS.INS_USER(?,?,?,?,?,?,?,?,?)}", IConstant.SCHEMA_NAME);
                callableStatement = connection.prepareCall(sqlString);
                callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
                callableStatement.setString(2, fullName[0]);
                callableStatement.setString(3, fullName[1]);
                callableStatement.setString(4, fullName[2]);
                callableStatement.setString(5, userData.getUsername());
                callableStatement.setString(6, userData.getPassword());
                callableStatement.setInt(7, userData.getOrganization());
                callableStatement.setInt(8, userData.getGroup());
                callableStatement.setString(9, userData.getPosition());
                callableStatement.setLong(10, 1L);
                callableStatement.executeQuery();
                callableStatement.getInt(1);
                return callableStatement.getInt(1);
            }
        });
    }

    @Override
    public Long getRowCount() {
        return (Long) sessionFactory.getCurrentSession().createCriteria(UserAccount.class).setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public UserAccount getFullAccount(Long id) {
        UserAccount userAccount = getElementByID(UserAccount.class, id);
        System.out.println(userAccount);
        return userAccount;
    }

    @Override
    public UserAccount getAccountByName(String username) {
        return (UserAccount) sessionFactory.getCurrentSession().createQuery("from UserAccount ua where ua.userEntity.username=:username")
                .setString("username", username)
                .uniqueResult();
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String position, String group, Integer startRecord, Integer recordCount) {
        return sessionFactory.getCurrentSession().createQuery
                ("Select ua.id, ua.profession, ua.regCode.codeName, ua.subject.surname, ua.subject.firstName, " +
                        "ua.subject.fatherName, ua.userEntity.username, ua.userEntity.group.groupName " +
                        "from UserAccount ua where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) " +
                        "like :login and lower(ua.regCode.codeName) like :org and lower(ua.profession) like :position and " +
                        "lower(ua.userEntity.group.groupName) like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("position", "%" + position + "%")
                .setString("group", group + "%")
                .setFirstResult(startRecord)
                .setMaxResults(recordCount)
                .list();
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String group, Integer startRecord, Integer recordCount) {
        return sessionFactory.getCurrentSession().createQuery
                ("Select ua.id, ua.profession, ua.regCode.codeName, ua.subject.surname, ua.subject.firstName, " +
                        "ua.subject.fatherName, ua.userEntity.username, ua.userEntity.group.groupName " +
                        "from UserAccount ua where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) " +
                        "like :login and lower(ua.regCode.codeName) like :org and lower(ua.userEntity.group.groupName) " +
                        "like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("group", group + "%")
                .setFirstResult(startRecord)
                .setMaxResults(recordCount)
                .list();
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String position, String group) {
        return (Long) sessionFactory.getCurrentSession().createQuery("Select count(*) from UserAccount ua " +
                "where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) like :login and " +
                "lower(ua.regCode.codeName) like :org and lower(ua.profession) like :position and " +
                "lower(ua.userEntity.group.groupName) like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("position", "%" + position + "%")
                .setString("group", group + "%")
                .uniqueResult();
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String group) {
        return (Long) sessionFactory.getCurrentSession().createQuery("Select count(*) from UserAccount ua " +
                "where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) like :login and " +
                "lower(ua.regCode.codeName) like :org and lower(ua.userEntity.group.groupName) like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("group", group + "%")
                .uniqueResult();
    }

    /**
     * Функция вызывает метод текущей сессии, с помощью которого можно
     * вызвать хранимые на базе функции. Далее строится запрос на функцию,
     * исходя из условия изменения пароля. Парсится объект userData и
     * устанавливаются данные для запроса. После запросы выполняется
     *
     * @param userData Данные для обновления объекта
     * @param check Условие изменения пароля
     * @return ID обновляемого объекта
     */
    @Override
    public Serializable updateElement(final UserData userData, final boolean check) {
        final String[] fullName = userData.getFio().split(" ");
        return sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<Serializable>() {
            @Override
            public Serializable execute(Connection connection) throws SQLException {
                CallableStatement callableStatement = null;
                String sqlString;
                if (check) {
                    sqlString = String.format("{? = call %s.PKG_USERS.UPD_USER2(?,?,?,?,?,?,?,?)}", IConstant.SCHEMA_NAME);
                    callableStatement = connection.prepareCall(sqlString);
                    callableStatement.setString(8, userData.getUsername());
                    callableStatement.setString(9, userData.getPassword());
                } else {
                    sqlString = String.format("{? = call %s.PKG_USERS.UPD_USER1(?,?,?,?,?,?)}", IConstant.SCHEMA_NAME);
                    callableStatement = connection.prepareCall(sqlString);
                }
                callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
                callableStatement.setLong(2, userData.getId());
                callableStatement.setString(3, userData.getPosition());
                callableStatement.setString(4, fullName[0]);
                callableStatement.setString(5, fullName[1]);
                callableStatement.setString(6, fullName[2]);
                callableStatement.setLong(7, 1L);
                callableStatement.executeQuery();
                callableStatement.getInt(1);
                return callableStatement.getInt(1);
            }
        });
    }

    /**
     * Функция вызывает метод текущей сессии, с помощью которого можно
     * вызвать хранимые на базе функции. Проверяется каждый параметр,
     * в зависимости от его значения он либо добавляется в функцию,
     * либо опускается. После проверки всех параметров запрос выполняется
     * и возвращает результат ввиде списка.
     *
     * @param surname Фамилия
     * @param login Логин
     * @param org Организация
     * @param group Группы пользователя
     * @param page Номер страницы
     * @param recordCount Количество записей
     * @return Список отфильтрованных объектов
     */
    @Override
    public List<FilterResponse> getFilterFunction(final String surname, final String login, final Integer org, final Integer group, final Integer page, final Integer recordCount) {
        return sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<List<FilterResponse>>() {
            @Override
            public List<FilterResponse> execute(Connection connection) throws SQLException {
                String rowCount = "";
                int colCount = 2;
                int surnameCol = 0;
                int loginCol = 0;
                int orgCol = 0;
                int groupCol = 0;
                List<FilterResponse> accounts = new ArrayList<FilterResponse>();
                CallableStatement callableStatement = null;
                String sqlEnd = "v_numpage => ?, v_qtypage => ?)}";
                StringBuilder sqlString = new StringBuilder(String.format("{? = call %s.PKG_USERS.GET_LIST_USER_FOREDIT(", IConstant.SCHEMA_NAME));
                if (!surname.equals("")) {
                    sqlString.append("v_surname => ?, ");
                    surnameCol = colCount++;
                }
                if (!login.equals("")) {
                    sqlString.append("v_login => ?, ");
                    loginCol = colCount++;
                }
                if (!(org == null)) {
                    sqlString.append("v_org => ?, ");
                    orgCol = colCount++;
                }
                if (!(group == null)) {
                    sqlString.append("v_group => ?, ");
                    groupCol = colCount++;
                }
                sqlString.append(sqlEnd);
                callableStatement = connection.prepareCall(sqlString.toString());
                callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
                if (!surname.equals("")) {
                    callableStatement.setString(surnameCol, surname);
                }
                if (!login.equals("")) {
                    callableStatement.setString(loginCol, login);
                }
                if (!(org == null)) {
                    callableStatement.setInt(orgCol, org);
                }
                if (!(group == null)) {
                    callableStatement.setInt(groupCol, group);
                }
                callableStatement.setInt(colCount++, page);
                callableStatement.setInt(colCount, recordCount);
                callableStatement.executeQuery();
                ResultSet rs = (ResultSet) callableStatement.getObject(1);
                while (rs.next()) {
                    rowCount = rs.getString(1);
                    accounts.add(initFilterResponse(rs));
                }
                return accounts;
            }
        });
    }

    @Override
    public int deleteById(Long id) {
        String sqlString = String.format("{ ? = call %s.PKG_USERS.DEL_USER ( ? )}", IConstant.SCHEMA_NAME);

        Connection pooledConnection = null;
        CallableStatement callableStatement = null;
        int result = 0;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
            callableStatement.setLong(2, id);
            callableStatement.execute();

            result = callableStatement.getInt(1);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return result;
    }

    /**
     * Функция инициализирует объект FilterResponse из выборки в ResultSet
     * @param rs Выборка, полученная в результате запроса
     * @return Инициализированный объект FilterResponse
     */
    private FilterResponse initFilterResponse(ResultSet rs) throws SQLException {
        FilterResponse filterResponse = new FilterResponse();
        filterResponse.setId(rs.getLong(2));
        filterResponse.setProfession(rs.getString(3));
        filterResponse.setSurname(rs.getString(4));
        filterResponse.setFirstName(rs.getString(5));
        filterResponse.setFatherName(rs.getString(6));
        filterResponse.setUserName(rs.getString(7));
        filterResponse.setGroupName(rs.getString(9));
        filterResponse.setOrgName(rs.getString(11));
        return filterResponse;
    }

}
