package org.nka.rs.dao;

import java.util.List;

/**
 * Created by zgurskaya on 03.06.2016.
 */
public interface IAddressDao<ATE> extends IBaseDao{

    List<ATE> findAteForCityCostType(Long parentateId);
    List<ATE> findAteForOtherCostType(Long parentateId);
    List<Long> findAteIdForParcelOutNP(Long parentateId);
    List<Long> findAteIdForSNP(Long parentateId);


}
