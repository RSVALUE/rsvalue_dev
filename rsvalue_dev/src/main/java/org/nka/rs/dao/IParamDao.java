package org.nka.rs.dao;

import java.sql.SQLException;

/**
 * Created by zgurskaya on 17.06.2016.
 */
public interface IParamDao extends IBaseDao {

    Object[] validateLoadObject(Integer costTypeId, Long objectnumber, Integer funcCodeId) throws SQLException;
}
