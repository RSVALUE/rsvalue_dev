package org.nka.rs.dao;

import org.nka.rs.entity.dictionary.ValueOrganizationDic;

import java.util.List;

/**
 * Created by zgurskaya on 29.09.2016.
 */
public interface IValOrgDao extends IBaseDao {

    List<ValueOrganizationDic> getActualOrganization();

}
