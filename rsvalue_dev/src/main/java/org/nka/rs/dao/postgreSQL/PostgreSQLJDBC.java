package org.nka.rs.dao.postgreSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by zgurskaya on 17.06.2016.
 */
public class PostgreSQLJDBC {

    //private static final String URL = "jdbc:postgresql://172.31.14.75/test_convert";
    //private static final String URL_TEST = "jdbc:postgresql://172.31.14.46/dev";
    private static final String URL_LIVE = "jdbc:postgresql://172.31.14.45/pbgisdb";

    private static final String LOGIN = "rvalue";

    //private static final String PASSWORD = "2%#hEM5du1";
    //private static final String PASSWORD_TEST = "EGrpe(0)3C";
    private static final String PASSWORD_LIVE = "n?Zp*FzM}R:ud=x^LhF6";

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("hibernate");

    public Connection getConnection() {
        Connection connection = null;
        String url = BUNDLE.getString("postgresql.url");
        String password = BUNDLE.getString("postgresql.password");
        String login = BUNDLE.getString("postgresql.login");
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(url, login, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
