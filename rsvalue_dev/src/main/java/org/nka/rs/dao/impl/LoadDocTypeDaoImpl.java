package org.nka.rs.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.nka.rs.dao.ILoadDocTypeDao;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.LoadDocumentTypeDic;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */

@Repository
public class LoadDocTypeDaoImpl extends BaseDaoSessionImpl implements ILoadDocTypeDao<LoadDocumentTypeDic>, IConstant {

    @Override
    public List<LoadDocumentTypeDic> getNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType) {
        Session session = null;
        List<LoadDocumentTypeDic> result = null;

        session = sessionFactory.getCurrentSession();
        StringBuffer queryStr = new StringBuffer("Select docType.analyticCode as analyticCode, docType.codeName as codeName " +
                " From LoadDocumentTypeDic docType where docType.status = :relevant AND ");

        if (costTypeId.equals(FARM_LANDS)) {
            queryStr.append(" docType.analyticCode in (:decree, :commonReport)");
        } else {
            queryStr.append(" docType.analyticCode in (:copyDecision, :conclusionCadastralValue, :commonReport)");
        }

        System.out.println(queryStr);
        Query q = session.createQuery(queryStr.toString());

        q.setBoolean("relevant", RELEVANT);
        if (costTypeId.equals(FARM_LANDS)) {
            q.setInteger("decree", DECREE);
            q.setInteger("commonReport", COMMON_REPORT);
        } else {
            q.setInteger("copyDecision", COPY_DECISION);
            q.setInteger("conclusionCadastralValue", CONCLUSION_CADASTRAL_VALUE);
            q.setInteger("commonReport", COMMON_REPORT);
        }

        result = q.setResultTransformer(Transformers.aliasToBean(LoadDocumentTypeDic.class)).list();
        return result;
    }

    @Override
    public List<LoadDocumentTypeDic> getNotNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType) {
        Session session = null;
        List<LoadDocumentTypeDic> result = null;
        session = sessionFactory.getCurrentSession();
        StringBuffer queryStr = new StringBuffer("Select docType.analyticCode as analyticCode, docType.codeName as " +
                "codeName From LoadDocumentTypeDic docType where docType.status = :relevant AND docType.parentCode in (:coreDocument, :reportDocument) AND ");

        if (costTypeId.equals(FARM_LANDS)) {
            queryStr.append(" docType.analyticCode not in (:decree, :commonReport)");
        } else {
            queryStr.append(" docType.analyticCode not in (:copyDecision, :conclusionCadastralValue, :commonReport)");
        }

        System.out.println(queryStr);
        Query q = session.createQuery(queryStr.toString());
        q.setBoolean("relevant", RELEVANT);

        q.setInteger("coreDocument", CORE_DOCUMENTS);
        q.setInteger("reportDocument", REPORT_DOCUMENTS);
        if (costTypeId.equals(FARM_LANDS)) {
            q.setInteger("decree", DECREE);
            q.setInteger("commonReport", COMMON_REPORT);
        } else {
            q.setInteger("copyDecision", COPY_DECISION);
            q.setInteger("conclusionCadastralValue", CONCLUSION_CADASTRAL_VALUE);
            q.setInteger("commonReport", COMMON_REPORT);
        }

        result = q.setResultTransformer(Transformers.aliasToBean(LoadDocumentTypeDic.class)).list();

        return result;
    }
}
