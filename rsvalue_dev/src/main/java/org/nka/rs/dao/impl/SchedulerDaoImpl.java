package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.nka.rs.dao.ISchedulerDao;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;

import static org.nka.rs.entity.constant.commonConstant.IConstant.SCHEMA_NAME;

/**
 * Created by zgurskaya on 01.08.2016.
 */

@Repository
public class SchedulerDaoImpl implements ISchedulerDao{


    @Override
    public void updateCurrency(Integer cur_id, Double rate) {
        Connection connection = null;
        CallableStatement call = null;
        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ = call " + SCHEMA_NAME + ".PKG_LOAD.INS_CUR(?,?)}");
            call.setInt(1, cur_id);
            call.setDouble(2, rate);
            call.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    public Clob nightClearData(){
        Connection connection = null;
        CallableStatement call = null;
        Clob pathes = null;
        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_CLEAR.NIGHT_DEL()}");
            //возвращаемый параметр - курсор, сведения по загрузке
            call.registerOutParameter(1, OracleTypes.CLOB);
            call.execute();
            pathes  = call.getClob(1);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return pathes;

    }
}
