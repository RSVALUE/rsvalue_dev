package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.nka.rs.dao.IParamDao;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.nka.rs.entity.constant.commonConstant.IConstant.SCHEMA_NAME;

/**
 * Created by zgurskaya on 17.06.2016.
 */

@Repository
public class ParamDaoImpl extends BaseDaoSessionImpl implements IParamDao {

    @Override
    public Object[] validateLoadObject(Integer costTypeId, Long objectnumber, Integer funcCodeId) throws SQLException {

        Object[] dataFromLoadObject = new Object[4];
        Connection connection = null;
        CallableStatement call = null;
        ResultSet res = null;
        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_LOAD.GET_LAST_REG(?,?,?)}");
            //cursor(reg_num, date_val, date_reg_start, method_type_id)
            call.registerOutParameter(1, OracleTypes.CURSOR);
            //cost_type_id
            call.setInt(2, costTypeId);
            //parentObjectNum
            call.setLong(3, objectnumber);
            //dec_org
            call.setInt(4, funcCodeId);

            call.execute();

            res = (ResultSet) call.getObject(1);
            while (res.next()) {
                //reg_num
                dataFromLoadObject[0] = res.getLong(1);
                //date_val
                dataFromLoadObject[1] = res.getObject(2);
                //date_reg_start
                dataFromLoadObject[2] = res.getObject(3);
                //method_type_id
                dataFromLoadObject[3] = res.getInt(4);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
        return dataFromLoadObject;

    }
}
