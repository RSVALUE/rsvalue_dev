package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.Operation;

import java.sql.Date;

/**
 * Интрейфейс DAO для сущности Operation
 */
public interface IOperationDao extends IBaseDao<Operation>{

    /**
     * Получается объект по ID
     *
     * @param id ID сущности
     * @return объект Operation
     */
    Operation getElementByID(Long id);

    /**
     * Получает ID по дате
     *
     * @param date Дата операции
     * @return ID операции
     */
    Long getIdByDate(Date date);
}
