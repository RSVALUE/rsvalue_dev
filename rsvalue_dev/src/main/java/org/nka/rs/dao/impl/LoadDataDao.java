package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionImpl;
import org.hibernate.transform.Transformers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.nka.rs.dao.ILoadDataDao;
import org.nka.rs.dao.postgreSQL.PostgreSQLJDBC;
import org.nka.rs.entity.constant.commonConstant.CostTypeEnum;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.FuncCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.CurrencyDic;
import org.nka.rs.entity.dto.AdditionalLayer;
import org.nka.rs.entity.dto.Bound;
import org.nka.rs.entity.dto.LoadData;
import org.nka.rs.entity.dto.Zone;
import org.nka.rs.entity.responseVar.Report;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 10.06.2016.
 */

@Repository
public class LoadDataDao implements ILoadDataDao, IConstant {

    @Autowired
    SessionFactory sessionFactory;

    // TODO: 21.11.2016 - это костыль, надо сделать, чтобы при отработке метода ставился флаг о выполнении, затем программа должна смотреть, какие сессии отработаны и их закрывать
    @Override
    public ResponseWithData loadData(LoadData data) {

        Report report = new Report();
        Long regNum = null;
        Long regId = null;
        Long[] dataFromLoadObject = new Long[2];
        ResponseWithData response = new ResponseWithData();

        Long userId = data.getUserId();
        try {

            //вызов функции для вставки данных по операции и объекту загрузки
            response = insertLoadObjectData(data);

            if (response != null && response.getCode() == ErrorCodeEnum.SUCCESS.getValue()) {
                dataFromLoadObject = (Long[]) response.getData();
                if (dataFromLoadObject != null && dataFromLoadObject[0] != null) {
                    //получение идентификатора загрузки
                    regNum = dataFromLoadObject[1];

                    Zone[] zones = data.getZoneList();
                    Arrays.sort(zones);
                    Long objectNumber = zones[0].getObjectnumb();

                    for (int i = 0; i < zones.length; i++) {
                        Zone zone = zones[i];
                        Long tmpObjNum = zone.getObjectnumb();

                        if (i == 0) {
                            response = insertCostObjectData(data, tmpObjNum, dataFromLoadObject);
                            if (response != null && response.getCode() == ErrorCodeEnum.SUCCESS.getValue()) {
                                regId = (Long) response.getData();
                            } else {
                                response.setData(regNum);
                                return response;
                            }
                            response = insertCostZoneData(zone, regNum, regId, data.getDwdId());
                            if (response == null || response.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                                response.setData(regNum);
                                return response;
                            }
                        } else {
                            if (tmpObjNum.equals(objectNumber)) {
                                response = insertCostZoneData(zone, regNum, regId, data.getDwdId());
                                if (response == null || response.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                                    response.setData(regNum);
                                    return response;
                                }
                            } else {
                                response = insertCostObjectData(data, tmpObjNum, dataFromLoadObject);
                                if (response != null && response.getCode() == ErrorCodeEnum.SUCCESS.getValue()) {
                                    regId = (Long) response.getData();
                                } else {
                                    response.setData(regNum);
                                    return response;
                                }
                                response = insertCostZoneData(zone, regNum, regId, data.getDwdId());
                                if (response == null || response.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                                    response.setData(regNum);
                                    return response;
                                }
                                objectNumber = tmpObjNum;
                            }
                        }
                    }

                    //вызов процедуры для вставки данных по участкам
                    Bound[] bounds = data.getBoundList();

                    if (bounds != null && bounds.length != 0) {
                        List<Bound> boundList = Arrays.asList(bounds);
                        response = insertParcelData(boundList, regNum, data.getDwdId());
                        if (response == null || response.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                            response.setData(regNum);
                            return response;
                        }
                    }

                    //вызов процедуры для вставки данных по слоям
                    AdditionalLayer[] layers = data.getLayerList();

                    if (layers != null && layers.length != 0) {
                        List<AdditionalLayer> layerList = Arrays.asList(layers);
                        response = insertLayerData(layerList, dataFromLoadObject, data.getDwdId());
                        if (response == null || response.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                            response.setData(regNum);
                            return response;
                        }
                    }
                    response = endLoad(regNum, data.getCostTypeId(), data.getParentObjectNumber(), data.getFuncCodeId(), data.getDwdId(), userId);
                    if (response == null || response.getCode() != ErrorCodeEnum.SUCCESS.getValue()) {
                        response.setData(regNum);
                        return response;
                    }

                    report = (Report) response.getData();
                    if (report != null) {
                        report.setRegNum(regNum);
                        CostTypeEnum costType = CostTypeEnum.searchByValue(data.getCostTypeId());
                        if (costType != null) {
                            report.setCostTypeName(costType.getTypeName());
                        }

                        FuncCodeEnum funcCode = FuncCodeEnum.searchByValue(data.getFuncCodeId());
                        if (funcCode != null) {
                            report.setFuncCodeName(funcCode.getTypeName());
                        }
                        if (data.getZoneList() != null && data.getZoneList().length != 0) {
                            report.setZonesQuantity(zones.length);
                        }
                        if (data.getBoundList() != null && data.getBoundList().length != 0) {
                            report.setBoundsQuantity(bounds.length);
                        }
                        if (data.getLayerList() != null && data.getLayerList().length != 0) {
                            report.setLayersQuantity(layers.length);
                        }
                        if (data.getDocList() != null && data.getDocList().length != 0) {
                            report.setDocQuantity(data.getDocList().length);
                        }
                        response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                        response.setData(report);
                    } else {
                        response.setCode(ErrorCodeEnum.NOMINAL_SUCCESS.getValue());
                        response.setMessage("Error with getting report");
                    }
                }
            } else {
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    //вызов функции для вставки данных для операции по типу 1100 и данных по объекту загрузки - возвращает ИД операции и ИД загрузки
    private ResponseWithData insertLoadObjectData(LoadData data) throws SQLException {
        ResponseWithData response = new ResponseWithData();
        Long[] dataFromLoadObject = new Long[2];
        String docIdString = "";

        Connection connection = null;
        CallableStatement call = null;
        ResultSet res = null;
        try {
            SessionImpl session = (SessionImpl) sessionFactory.getCurrentSession();
            connection = session.connection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_LOAD.INS_LOAD_GROUP(?,?,?,?,?,?,?,?)}");
            //oper_id
            call.registerOutParameter(1, OracleTypes.CURSOR);
            //user-executor
            call.setLong(2, data.getUserId());
            //dec_num
            call.setString(3, data.getDecNum());
            //dec_org
            call.setInt(4, data.getDecOrgId());

            //dec_date
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String decStr = data.getDecDate();
            Date decDate = null;
            try {
                decDate = formatter.parse(decStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.sql.Date sqlDate = new java.sql.Date(decDate.getTime());
            call.setDate(5, sqlDate);

            //val_org
            call.setInt(6, data.getValOrgId());
            //remark
            if (data.getRemark() != null) {
                call.setString(7, data.getRemark());
            } else {
                call.setNull(7, OracleTypes.VARCHAR);
            }
            //todo - иногда возвращается null вместо номера документа
            if (data.getDocList() != null && data.getDocList().length != 0) {
                List<String> docs = Arrays.asList(data.getDocList());
                for (String s : docs) {
                    if (s != null && !s.equals("")) {
                        docIdString += s + ";";
                    }
                }
            }
            if (!docIdString.equals("")) {
                call.setString(8, docIdString);
            } else {
                call.setNull(8, OracleTypes.VARCHAR);
            }
            // TODO: 26.07.2016 - возвращается null
            if (data.getPolyTypeId() != null) {
                call.setInt(9, data.getPolyTypeId());
            } else {
                call.setInt(9, POINT);
            }

            call.execute();
            res = (ResultSet) call.getObject(1);
            while (res.next()) {
                //oper_id
                dataFromLoadObject[0] = res.getLong(1);
                //reg_num
                dataFromLoadObject[1] = res.getLong(2);
            }
            response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            response.setData(dataFromLoadObject);
        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при сохранении объекта загрузки");
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    private ResponseWithData insertCostObjectData(LoadData data, Long objectnumber, Long[] additionalData) throws SQLException {
        ResponseWithData response = new ResponseWithData();
        Long regId = null;
        CallableStatement call = null;
        Connection connection = null;
        try {
            SessionImpl session = (SessionImpl) sessionFactory.getCurrentSession();
            connection = session.connection();
            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_LOAD.INS_COST_OBJECT(?,?,?,?,?,?,?,?,?,?)}");
            //reg_id
            call.registerOutParameter(1, OracleTypes.NUMBER);
            //идентификатор загрузки
            call.setLong(2, additionalData[1]);
            //идентификатор вида оценки
            call.setInt(3, data.getCostTypeId());
            //идентификатор методики оценки
            call.setInt(4, data.getMethosTypeId());
            //идентификатор функционального использования
            call.setInt(5, data.getFuncCodeId());
            //регистрационный номер родительского адреса
            call.setLong(6, data.getParentObjectNumber());
            //регистрационный номер объекта оценки
            call.setLong(7, objectnumber);
            //население
            if (data.getPopulation() != null) {
                call.setLong(8, data.getPopulation());
            } else {
                call.setNull(8, OracleTypes.NUMBER);
            }
            //дата оценки

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String valStr = data.getDateVal();
            java.util.Date valDate = null;
            try {
                valDate = formatter.parse(valStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            java.sql.Date sqlDate = new java.sql.Date(valDate.getTime());
            call.setDate(9, sqlDate);
            //идентификатор операции внесения данных по объекту загрузки
            call.setLong(10, additionalData[0]);
            if (data.getCurrencyId() != null) {
                call.setInt(11, data.getCurrencyId());
            } else {
                call.setInt(11, CURRENCY_BEL_DEFAULT);
            }
            call.execute();
            regId = call.getLong(1);
            response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            response.setData(regId);
        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при сохранении объекта оценки на Oracle");
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    private ResponseWithData insertCostZoneData(Zone zone, Long regNum, Long regId, Long dwdId) throws SQLException {
        CallableStatement call = null;
        Connection connection = null;

        ResponseWithData response = new ResponseWithData();
        String str;
        String encodeStr;
        byte[] arr;

        try {
            SessionImpl session = (SessionImpl) sessionFactory.getCurrentSession();
            connection = session.connection();
            call = connection.prepareCall("{ = call " + SCHEMA_NAME + ".PKG_LOAD.INS_COST_ZONE(?,?,?,?,?,?,?,?,?,?,?,?)}");
            //номер объекта загрузки
            call.setLong(1, regNum);
            //идентификатор загрузки для постгреса
            call.setLong(2, dwdId);
            //идентификатор объекта оценки
            call.setLong(3, regId);
            //регистрационный номер оценочной зоны
            call.setString(4, zone.getNum());
            //стоимость в долларах
            call.setDouble(5, zone.getCost_d());
            //расчетная площадь оценочной зоны
            if (zone.getArea() != null) {
                call.setDouble(6, zone.getArea());
            } else {
                call.setNull(6, OracleTypes.NUMBER);
            }

            //стоимость в рублях
            call.setDouble(7, zone.getCost_r());

            //описание оценочной зоны (наименование категории - деревня, агрогородок и пр)
            if (zone.getCategory() != null) {
                str = zone.getCategory();
                arr = str.getBytes("windows-1252");
                encodeStr = new String(arr, "windows-1251");
                call.setString(8, encodeStr);
            } else {
                call.setNull(8, OracleTypes.VARCHAR);
            }

            //наименование ближайшего населенного пункта
            if (zone.getNpname() != null && !zone.getNpname().equals("")) {
                str = zone.getNpname();
                arr = str.getBytes("windows-1252");
                encodeStr = new String(arr, "windows-1251");
                call.setString(9, encodeStr);
            } else if (zone.getSs() != null && !zone.getSs().equals("")) {
                str = zone.getSs();
                arr = str.getBytes("windows-1252");
                encodeStr = new String(arr, "windows-1251");
                call.setString(9, encodeStr);
            } else {
                call.setNull(9, OracleTypes.VARCHAR);
            }

            //наименование оценочной зоны
            if (zone.getName() != null && !zone.getName().equals("")) {
                str = zone.getName();
                arr = str.getBytes("windows-1252");
                encodeStr = new String(arr, "windows-1251");
                if (zone.getSs() != null && !zone.getSs().equals("")) {
                    String ss = zone.getSs();
                    byte[] arrSs = ss.getBytes("windows-1252");
                    String encodeSs = new String(arrSs, "windows-1251");
                    call.setString(10, encodeStr + " " + encodeSs);
                } else {
                    call.setString(10, encodeStr);
                }
            } else if (zone.getStname() != null && !zone.getStname().equals("")) {
                str = zone.getStname();
                arr = str.getBytes("windows-1252");
                encodeStr = new String(arr, "windows-1251");
                call.setString(10, encodeStr);
            }

            //УНП землевладельца для СТ и ДК
            if (zone.getUnp() != null) {
                call.setLong(11, zone.getUnp());
            } else {
                call.setNull(11, OracleTypes.NUMBER);
            }
            //примечание - пока его брать неоткуда
            call.setNull(12, OracleTypes.VARCHAR);

            call.execute();
            response.setCode(ErrorCodeEnum.SUCCESS.getValue());

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при сохранении оценочных зон на Oracle");
        } finally {
            if (call != null) {
                call.close();
            }
        }
        return response;
    }

    private ResponseWithData insertParcelData(List<Bound> bounds, Long regNum, Long dwdId) throws SQLException {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        ResponseWithData response = new ResponseWithData();
        String str;
        String encodeStr;
        byte[] arr;
        Long parcel_id = null;
        Long zone_id = null;
        for (int i = 0; i < bounds.size(); i++) {
            try {
                SessionImpl session = (SessionImpl) sessionFactory.getCurrentSession();
                connection = session.connection();
                call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_LOAD.INS_PARCELS(?,?,?,?,?,?,?,?,?,?)}");
                Bound bound = bounds.get(i);
                //курсор, который возвращает ИД участка и ИД зоны
                call.registerOutParameter(1, OracleTypes.CURSOR);
                //идентификатор объекта загрузки
                call.setLong(2, regNum);
                //идентификатор агрузки
                call.setLong(3, dwdId);
                //номер соответствующей оценочной зоны
                call.setString(4, bound.getNum());
                //кадастровый номер земельного участка
                call.setString(5, bound.getCadnum());
                //площадь земельного участка по ЕГРНИ
                if (bound.getSq() != null) {
                    call.setDouble(6, bound.getSq());
                } else {
                    call.setNull(6, OracleTypes.NUMBER);
                }
                //адрес земельного участка строковый
                if (bound.getAddress() != null && !bound.getAddress().equals("")) {
                    str = bound.getAddress();
                    arr = str.getBytes("windows-1252");
                    encodeStr = new String(arr, "windows-1251");
                    call.setString(7, encodeStr);
                } else {
                    call.setNull(7, OracleTypes.VARCHAR);
                }
                //целевое назначение
                if (bound.getPurpose() != null) {
                    str = bound.getPurpose();
                    arr = str.getBytes("windows-1252");
                    encodeStr = new String(arr, "windows-1251");
                    call.setString(8, encodeStr);
                } else {
                    call.setNull(8, OracleTypes.VARCHAR);
                }
                //целевое назначение по решению местной исполнительной власти
                if (bound.getPurpose2() != null) {
                    str = bound.getPurpose2();
                    arr = str.getBytes("windows-1252");
                    encodeStr = new String(arr, "windows-1251");
                    call.setString(9, encodeStr);
                } else {
                    call.setNull(9, OracleTypes.VARCHAR);
                }
                //стоимость в долларах
                call.setDouble(10, bound.getCost_d());
                //стоимость в рублях
                call.setDouble(11, bound.getCost_r());
                call.execute();
                rs = (ResultSet) call.getObject(1);

                while (rs.next()) {
                    parcel_id = rs.getLong(1);
                    zone_id = rs.getLong(2);
                }

                if (parcel_id != null && zone_id != null) {
                    response = saveBoundsToPostgre(parcel_id, zone_id, dwdId, regNum, bound.getCadnum());
                } else {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Ошибка при получении данных по границам в Oracle");
                }
            } catch (Exception e) {
                e.printStackTrace();
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Ошибка при сохранении границ в Oracle");
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                try {
                    if (call != null) {
                        call.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    private ResponseWithData insertLayerData(List<AdditionalLayer> layerList, Long[] additionalData, Long dwdId) throws SQLException {
        CallableStatement call = null;
        Connection connection = null;
        ResponseWithData response = new ResponseWithData();
        for (int i = 0; i < layerList.size(); i++) {
            try {
                SessionImpl session = (SessionImpl) sessionFactory.getCurrentSession();
                connection = session.connection();
                call = connection.prepareCall("{ = call " + SCHEMA_NAME + ".PKG_LOAD.INS_LAYER(?,?,?,?,?)}");
                AdditionalLayer layer = layerList.get(i);
                //идентификатор объекта загрузки
                call.setLong(1, additionalData[1]);
                //идентификатор загрузки
                call.setLong(2, dwdId);
                //код тематического слоя
                call.setInt(3, layer.getAnalyticCode());
                //идентификатор операции
                call.setLong(4, additionalData[0]);
                //код для определения свойства тематического слоя - используется только для грунта и доступности центра
                if (layer.getCode() != null && layer.getCode().length != 0) {
                    String codeString = "";
                    for (int j = 0; j < layer.getCode().length; j++) {
                        codeString += layer.getCode()[j] + ";";
                    }
                    call.setString(5, codeString);
                } else {
                    call.setNull(5, OracleTypes.NUMBER);
                }

                call.execute();
                response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            } catch (Exception e) {
                e.printStackTrace();
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Ошибка при сохранении слоев на Oracle");
            } finally {
                try {
                    if (call != null) {
                        call.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    private ResponseWithData endLoad(Long regNum, Integer costTypeId, Long parentObjectnum, Integer funcCode, Long dwdId, Long userId) {
        ResponseWithData response = new ResponseWithData();
        Report report = new Report();
        Connection connection = null;
        CallableStatement call = null;
        ResultSet rs = null;

        try {
            SessionImpl session = (SessionImpl) sessionFactory.getCurrentSession();
            connection = session.connection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_LOAD.END_LOAD(?,?,?,?,?,?)}");
            //возвращаемый параметр - курсор
            call.registerOutParameter(1, OracleTypes.CURSOR);
            //новый идентификатор объекта загрузки
            call.setLong(2, regNum);
            //вид оценки
            call.setInt(3, costTypeId);
            //АТЕ оценки
            call.setLong(4, parentObjectnum);
            //функциональное назначение
            call.setInt(5, funcCode);
            //идентификатор загрузки
            call.setLong(6, dwdId);
            //пользователь системы
            call.setLong(7, userId);
            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                //наименование АТЕ
                report.setAteName(rs.getString(1));
                //количество деактуализированных записей
                report.setDecObjectQuantity(rs.getInt(2));
                if (costTypeId.equals(RURAL_LOCALITY)) {
                    //количество СНП в базе
                    report.setBaseObjectQuantity(rs.getInt(3));
                    //количествоо СНП в шейпе (количество объектов)
                    report.setShapeObjectQuantity(rs.getInt(4));
                }
            }
            response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            response.setData(report);

        } catch (SQLException e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Error with end load");
        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Error with end load");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    private ResponseWithData saveBoundsToPostgre(Long parcel_id, Long zone_id, Long dwd_id, Long reg_num, String cadnum) {
        PostgreSQLJDBC postgreSQLJDBC = new PostgreSQLJDBC();
        Connection postgreCon = null;
        CallableStatement call = null;
        ResponseWithData response = new ResponseWithData();
        try {
            postgreCon = postgreSQLJDBC.getConnection();
            call = postgreCon.prepareCall("{ ? = call rvalue.insert_to_bounds(?,?,?,?,?)}");
            call.registerOutParameter(1, Types.INTEGER);
            call.setLong(2, parcel_id);
            call.setLong(3, zone_id);
            call.setInt(4, Integer.parseInt(dwd_id.toString()));
            call.setInt(5, Integer.parseInt(reg_num.toString()));
            call.setString(6, cadnum);
            call.execute();
            response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            response.setMessage("");
        } catch (SQLException e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при сохранении участка №" + parcel_id + " на PostgreSQL");
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (postgreCon != null) {
                    postgreCon.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    public String saveZonesGeometry(JSONObject geometry, Long dwd_id) {
        PostgreSQLJDBC postgreSQLJDBC = new PostgreSQLJDBC();
        Connection postgreCon = null;
        CallableStatement call = null;
        String message = "";
        //todo  - проверить, поле д.б AREA - но значение может быть не String
        try {
            if (geometry.has("features")) {
                JSONArray features = geometry.getJSONArray("features");
                for (Object row : features) {
                    JSONObject object1 = (JSONObject) row;
                    if (object1.has("properties")) {
                        JSONObject data = object1.getJSONObject("properties");
                        if (data.has("area")) {
                            String area = (String) data.get("area");
                            if (area == null || area.equals("")) {
                                data.put("area", "0.0");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            message = e.getMessage();
        }
        try {
            postgreCon = postgreSQLJDBC.getConnection();
            call = postgreCon.prepareCall("{ ? = call rvalue.insert_to_zones_var(?,?)}");
            byte[] geometryAr = geometry.toString().getBytes("windows-1252");
            String geometryStr = new String(geometryAr, "windows-1251");

            String regex = "\\u0000";
            Boolean isNullChar = geometryStr.contains(regex);
            if (isNullChar) {
                geometryStr = geometryStr.replace(regex, "0");
            }
            int id = Integer.parseInt(dwd_id.toString());
            call.registerOutParameter(1, Types.INTEGER);
            call.setInt(2, id);
            call.setString(3, geometryStr);
            call.execute();
            message = "success";
        } catch (SQLException e) {
            e.printStackTrace();
            message = e.getMessage();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            message = e.getMessage();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                message = e.getMessage();
            }
            try {
                if (postgreCon != null) {
                    postgreCon.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    @Override
    public String saveBoundsGeometry(JSONObject geometry, Long dwd_id) {
        PostgreSQLJDBC postgreSQLJDBC = new PostgreSQLJDBC();
        Connection postgreCon = postgreSQLJDBC.getConnection();
        CallableStatement call = null;
        String message = "";

        try {
            call = postgreCon.prepareCall("{ ? = call  rvalue.insert_to_bounds_var(?,?)}");
            byte[] geometryAr = geometry.toString().getBytes("windows-1252");
            String geometryStr = new String(geometryAr, "windows-1251");
            String regex = "\\u0000";
            Boolean isNullChar = geometryStr.contains(regex);
            if (isNullChar) {
                geometryStr = geometryStr.replace(regex, "0");
            }
            int id = Integer.parseInt(dwd_id.toString());
            call.registerOutParameter(1, Types.INTEGER);
            call.setInt(2, id);
            call.setString(3, geometryStr);
            call.execute();
            message = "success";
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (postgreCon != null) {
                    postgreCon.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    @Override
    public Response saveLayerGeometry(JSONObject geometry, Long dwd_id, String layerName) {
        Response response = new Response();
        response.setCode(ErrorCodeEnum.SUCCESS.getValue());
        response.setMessage("");
        Integer code = null;
        Session session = null;
        PostgreSQLJDBC postgreSQLJDBC = new PostgreSQLJDBC();
        Connection postgreCon = postgreSQLJDBC.getConnection();
        CallableStatement call = null;

        String geometryStr = geometry.toString();
        try {
            session = sessionFactory.getCurrentSession();

            Query q = session.createQuery("select lay.analyticCode FROM LayerTypeDic lay " +
                    " where lower(:layerName) like (lay.fileName||'%' )");

            q.setString("layerName", layerName);
            code = (Integer) q.uniqueResult();

            if (code != null) {
                //это null в текстовом формате, который может быть как для типа Character, так и для типа Number
                String regex = "\\u0000";
                Boolean isNullChar = geometryStr.contains(regex);
                if (isNullChar) {
                    geometryStr = geometryStr.replace(regex, "0");
                }
                call = postgreCon.prepareCall("{ ? = call  rvalue.insert_to_layers_var(?,?,?)}");

                int id = Integer.parseInt(dwd_id.toString());
                call.registerOutParameter(1, Types.INTEGER);
                call.setInt(2, id);
                call.setInt(3, code);
                call.setString(4, geometryStr.toLowerCase());
                call.execute();

                Integer errorCursor = call.getInt(1);
                //todo 13.09.2016 - проверить работу
                if (errorCursor != 1) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Ошибка при записи геометри слоя " + layerName + " на Postgre");
                }
            } else {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Ошибка при записи геометри слоя " + layerName + " на Postgre");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (postgreCon != null) {
                    postgreCon.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    public List getNationalCurrency() {
        Session session = null;
        List result = null;
        session = sessionFactory.getCurrentSession();

        Query q = session.createQuery("select currency.analyticCode as analyticCode, currency.codeShortName as codeShortName From CurrencyDic as currency Where currency.isForeign = :notRelevant and currency.status = :relevant");
        q.setBoolean("notRelevant", NOT_RELEVANT);
        q.setBoolean("relevant", RELEVANT);
        result = q.setResultTransformer(Transformers.aliasToBean(CurrencyDic.class)).list();
        return result;
    }

    @Override
    public List getForeignCurrency() {
        Session session = null;
        List<String> result = null;

        session = sessionFactory.getCurrentSession();

        Query q = session.createQuery("select currency.analyticCode, currency.codeShortName From CurrencyDic as currency Where currency.isForeign = :relevant and currency.status = :relevant");
        q.setBoolean("relevant", RELEVANT);
        result = q.setResultTransformer(Transformers.aliasToBean(CurrencyDic.class)).list();
        return result;
    }

    @Override
    public Clob clearErrorLoad(Long regNum) {
        Connection connection = null;
        CallableStatement call = null;
        Clob pathes = null;
        try {
            SessionImpl session = (SessionImpl) sessionFactory.getCurrentSession();
            connection = session.connection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_CLEAR.DEL_ERR_LOAD(?)}");
            //возвращаемый параметр - курсор, сведения по загрузке
            call.registerOutParameter(1, OracleTypes.CLOB);
            call.setLong(2, regNum);
            call.execute();
            pathes = call.getClob(1);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return pathes;
    }


}
