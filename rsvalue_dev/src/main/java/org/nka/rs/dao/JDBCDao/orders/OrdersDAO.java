package org.nka.rs.dao.JDBCDao.orders;

import oracle.jdbc.OracleTypes;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.pojos.order.OrderData;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.util.connection.UtilConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO класс, в котором содержатся методы для работы с
 * таблицей заказов
 */
public class OrdersDAO {

    /**
     * Метод, добавляющий новый заказ. На вход поступает объект с
     * данными о заказе, поля этого объекта проверяются на ненулевые\
     * неверные значения.
     *
     * @param order Объект с данными по новому заказу
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData addOrder(OrderData order) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.SAVE_ORDERS ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResponseWithData responseWithData = new ResponseWithData();
        Integer result;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
            if (order.getOrderId() != null) {
                callableStatement.setLong(2, order.getOrderId());
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (checkUserId(order.getUserId(), responseWithData)) {
                callableStatement.setLong(3, order.getUserId());
            } else {
                return responseWithData;
            }
            if (!order.getOrderNumber().equals("")) {
                callableStatement.setString(4, order.getOrderNumber());
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр номер заказа не предоставлен");
                return responseWithData;
            }
            if (order.getUrgency() != null && (order.getUrgency() == 11 || order.getUrgency() == 12)) {
                callableStatement.setInt(5, order.getUrgency());
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр срочность заказа не предоставлен или предоставлен неверно");
                return responseWithData;
            }
            if (order.getIsNb() != null) {
                callableStatement.setInt(6, order.getIsNb());
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр для налогооблажения не предоставлен");
                return responseWithData;
            }
            if (order.getDeclType() != null) {
                callableStatement.setInt(7, order.getDeclType());
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр тип лица не предоставлен");
                return responseWithData;
            }
            if (!order.getDeclName().equals("")) {
                callableStatement.setString(8, order.getDeclName());
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр наименование лица не предоставлен");
                return responseWithData;
            }
            if (!order.getUnp().equals("")) {
                callableStatement.setString(9, order.getUnp());
            } else {
                callableStatement.setNull(9, OracleTypes.VARCHAR);
            }
            if (!order.getNkName().equals("")) {
                callableStatement.setString(10, order.getNkName());
            } else {
                callableStatement.setNull(10, OracleTypes.VARCHAR);
            }
            if (!order.getPhoneNumber().equals("")) {
                callableStatement.setString(11, order.getPhoneNumber());
            } else {
                callableStatement.setNull(11, OracleTypes.VARCHAR);
            }
            if (!order.getEmail().equals("")) {
                callableStatement.setString(12, order.getEmail());
            } else {
                callableStatement.setNull(12, OracleTypes.VARCHAR);
            }
            if (!order.getAddress().equals("")) {
                callableStatement.setString(13, order.getAddress());
            } else {
                callableStatement.setNull(13, OracleTypes.VARCHAR);
            }
            if (!order.getIndex().equals("")) {
                callableStatement.setString(14, order.getIndex());
            } else {
                callableStatement.setNull(14, OracleTypes.VARCHAR);
            }
            if (!order.getAte().equals("")) {
                callableStatement.setString(15, order.getAte());
            } else {
                callableStatement.setNull(15, OracleTypes.VARCHAR);
            }
            if (!order.getNumIsx().equals("")) {
                callableStatement.setString(16, order.getNumIsx());
            } else {
                callableStatement.setNull(16, OracleTypes.VARCHAR);
            }
            if (!order.getDateIsx().equals("")) {
                callableStatement.setString(17, order.getDateIsx());
            } else {
                callableStatement.setNull(17, OracleTypes.VARCHAR);
            }
            if (!order.getNumVx().equals("")) {
                callableStatement.setString(18, order.getNumVx());
            } else {
                callableStatement.setNull(18, OracleTypes.VARCHAR);
            }
            if (!order.getDateVx().equals("")) {
                callableStatement.setString(19, order.getDateVx());
            } else {
                callableStatement.setNull(19, OracleTypes.VARCHAR);
            }
            callableStatement.execute();
            result = callableStatement.getInt(1);
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("OK: Данные записаны");
            responseWithData.setData(result);
        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка при записи данных в базу (%s)", e.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка (%s)", e.getMessage()));
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Метод, получающий список заказов с указанными фильтрами и сортировкой
     *
     * @param userId ID пользователя
     * @param orgId ID организации
     * @param urgency Срочность
     * @param decl Заказчик
     * @param isNb Налогооблажение
     * @param orderState Статус заказа
     * @param filterUserId ID пользователя, принявшего заказ
     * @param sort Поле сортировки
     * @param sortType Тип сортировки
     * @param page Страницы выборки
     * @param qty Количество записей на странице
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getOrders(Long userId, Integer orgId, Integer urgency, String decl, Integer isNb, Integer orderState,
                                      Long filterUserId, Integer sort, String sortType, Integer page, Integer qty) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_JOURNAL_ORDERS ( ?,?,?,?,?,?,?,?,?,?,? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;
        List<Order> orders = new ArrayList<Order>();
        OrderList orderList = new OrderList();
        Order order;
        ResponseWithData responseWithData = new ResponseWithData();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (userId == null) {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр userId не предоставлен");
                return responseWithData;
            } else {
                callableStatement.setLong(2, userId);
            }
            if (orgId != null) {
                callableStatement.setInt(3, orgId);
            } else {
                callableStatement.setNull(3, OracleTypes.NUMBER);
            }
            if (urgency != null) {
                callableStatement.setInt(4, urgency);
            } else {
                callableStatement.setNull(4, OracleTypes.NUMBER);
            }
            if (!decl.equals("")) {
                callableStatement.setString(5, decl);
            } else {
                callableStatement.setNull(5, OracleTypes.VARCHAR);
            }
            if (isNb != null) {
                callableStatement.setInt(6, isNb);
            } else {
                callableStatement.setNull(6, OracleTypes.NUMBER);
            }
            if (orderState != null) {
                callableStatement.setInt(7, orderState);
            } else {
                callableStatement.setNull(7, OracleTypes.NUMBER);
            }
            if (filterUserId != null) {
                callableStatement.setLong(8, filterUserId);
            } else {
                callableStatement.setNull(8, OracleTypes.NUMBER);
            }
            callableStatement.setInt(9, sort);
            if (!sortType.equals("")) {
                callableStatement.setString(10, sortType);
            } else {
                callableStatement.setNull(10, OracleTypes.VARCHAR);
            }
            if (page != null) {
                callableStatement.setInt(11, page);
            } else {
                callableStatement.setNull(11, OracleTypes.NUMBER);
            }
            if (qty != null) {
                callableStatement.setInt(12, qty);
            } else {
                callableStatement.setNull(12, OracleTypes.NUMBER);
            }
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                order = new Order();
                order.setOrderId(rs.getInt(1));
                order.setOrderNumber(rs.getString(2));
                order.setUrgName(rs.getString(3));
                order.setStartDate(rs.getString(4));
                order.setDeclName(rs.getString(5));
                order.setInvoiceNum(rs.getString(6));
                order.setInvoiceDate(rs.getString(7));
                order.setNbName(rs.getString(8));
                order.setStateName(rs.getString(9));
                order.setUserName(rs.getString(10));
                orderList.setCount(rs.getInt(11));
                order.setStateId(rs.getInt(12));
                order.setCanEdit(rs.getInt(13));
                orders.add(order);
            }
            Integer rowCount;
            orderList.setOrderList(orders);
            if (qty != null && orders.size() != 0) {
                if (orderList.getCount() != null) {
                    if ((rowCount = orderList.getCount()) % qty == 0) {
                        orderList.setPages(rowCount / qty);
                    } else {
                        orderList.setPages((rowCount / qty) + 1);
                    }
                } else {
                    orderList.setCount(0);
                    orderList.setPages(0);
                }
            }
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("OK: Данные получены");
            responseWithData.setData(orderList);
        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка получения данных из базы (%s)", e.getMessage()));
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Метод, получающий список пользователей для данной организации
     *
     * @param userId ID пользователя
     * @param orgName Наименование организации
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getUserList(Long userId, Long orgName) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_LIST_USERS ( ?,? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;

        List<User> result = new ArrayList<User>();
        User user;
        ResponseWithData responseWithData = new ResponseWithData();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (userId != null) {
                callableStatement.setLong(2, userId);
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр userId не предоставлен");
                return responseWithData;
            }
            if (orgName != null) {
                callableStatement.setLong(3, orgName);
            } else {
                callableStatement.setNull(3, OracleTypes.VARCHAR);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                user = new User();
                user.setId(rs.getLong(1));
                user.setFullName(rs.getString(2));
                result.add(user);
            }
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("OK: Данные получены");
            responseWithData.setData(result);
        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка получения данных из базы (%s)", e.getMessage()));
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Метод, получающий данные по заказу для просмотра\редактирования
     *
     * @param orderId ID заказа
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getEditableOrder(Long orderId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.EDIT_ORDERS ( ? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;
        FullOrder order = new FullOrder();
        ResponseWithData responseWithData = new ResponseWithData();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (orderId != null) {
                callableStatement.setLong(2, orderId);
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Параметр orderId не предоставлен");
                return responseWithData;
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                order.setOrderId(rs.getLong(1));
                order.setOrderNumber(rs.getString(2));
                order.setUrgency(rs.getInt(3));
                order.setUrgName(rs.getString(4));
                order.setNbType(rs.getInt(5));
                order.setNbName(rs.getString(6));
                order.setDeclType(rs.getInt(7));
                order.setDeclTypeName(rs.getString(8));
                order.setDeclName(rs.getString(9));
                order.setUnp(rs.getString(10));
                order.setPhoneNumber(rs.getString(11));
                order.setEmail(rs.getString(12));
                order.setAddress(rs.getString(13));
                order.setIndex(rs.getString(14));
                order.setAte(rs.getString(15));
                order.setNumIsx(rs.getString(16));
                order.setDateIsx(rs.getString(17));
                order.setNumVx(rs.getString(18));
                order.setDateVx(rs.getString(19));
                order.setNkName(rs.getString(20));
            }
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("OK: Данные получены");
            responseWithData.setData(order);
        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка получения данных из базы (%s)", e.getMessage()));
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Получает номер нового заказа, если пользователя сотрудник НКА
     * и ничего, если другой организации
     *
     * @param userId ID пользователя
     * @return Объект ResponseWithData с данными о результатах выполнения запроса
     */
    public ResponseWithData getNewOrder(Long userId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_NEW_ORDER ( ? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;

        ResponseWithData responseWithData = new ResponseWithData();
        String orderNum;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            if (checkUserId(userId, responseWithData)) {
                callableStatement.setLong(2, userId);
            } else {
                return responseWithData;
            }
            callableStatement.execute();
            orderNum = callableStatement.getString(1);
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("OK: Данные получены");
            responseWithData.setData(orderNum);
        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка получения данных из базы (%s)", e.getMessage()));
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Получает возможные статусы заказов
     *
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData getAllStates() {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.GET_ALL_STATES}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;

        ResponseWithData responseWithData = new ResponseWithData();
        List<State> states = new ArrayList<State>();
        State state;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                state = new State();
                state.setId(rs.getInt(1));
                state.setName(rs.getString(2));
                states.add(state);
            }
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("OK: Данные получены");
            responseWithData.setData(states);
        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка получения данных из базы (%s)", e.getMessage()));
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Метод, меняющий статус заказа
     *
     * @param orderId ID заказа
     * @param state Новый статус
     * @param userId ID пользователя
     * @return Объект ResponseWithData с кодом, сообщением и данными запроса
     */
    public ResponseWithData changeOrderState(Long orderId, Integer state, Long userId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.CHANGE_ORDER_STATE ( ?,?,? )}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;

        ResponseWithData responseWithData = new ResponseWithData();
        Integer result;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
            callableStatement.setLong(2, orderId);
            callableStatement.setInt(3, state);
            if (checkUserId(userId, responseWithData)) {
                callableStatement.setLong(4, userId);
            } else {
                return responseWithData;
            }
            callableStatement.execute();
            result = callableStatement.getInt(1);
            if (result == 1) {
                responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
                responseWithData.setMessage("OK: Данные записаны");
            } else {
                responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                responseWithData.setMessage("ERROR: Ошибка записи данных в базу");
            }
            responseWithData.setData(result);
        } catch (SQLException e) {
            e.printStackTrace();
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage(String.format("ERROR: Ошибка получения данных из базы (%s)", e.getMessage()));
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return responseWithData;
    }

    /**
     * Метод проверяет параметр userId на null значение и устанавливает
     * в объект-ответ соответствующее сообщение при успешной проверке
     * @param userId Параметр ID пользователя
     * @param responseWithData Объект-ответ
     * @return True - параметр not null, false - параметр null
     */
    private boolean checkUserId(Long userId, ResponseWithData responseWithData) {
        if (userId == null) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage("ERROR: Параметр userId не предоставлен");
            return false;
        }
        return true;
    }

}

/**
 * Вспомогательные класс, содержащий сведения о пользователе
 */
class User {

    private Long id;

    private String fullName;

    public Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    void setFullName(String fullName) {
        this.fullName = fullName;
    }

}

/**
 * Вспомогательный класс, содержащий сведения о статусе заказа
 */
class State {

    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

/**
 * Вспомогательный класс, содержащий выборочные сведения о данных заказа
 */
class Order {
    private Integer orderId;

    private String orderNumber;

    private String urgName;

    private String startDate;

    private String declName;

    private String invoiceNum;

    private String invoiceDate;

    private String nbName;

    private String stateName;

    private Integer stateId;

    private String userName;

    private Integer canEdit;

    void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    void setUrgName(String urgName) {
        this.urgName = urgName;
    }

    void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    void setDeclName(String declName) {
        this.declName = declName;
    }

    void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    void setNbName(String nbName) {
        this.nbName = nbName;
    }

    void setStateName(String stateName) {
        this.stateName = stateName;
    }

    void setUserName(String userName) {
        this.userName = userName;
    }

    void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    void setCanEdit(Integer canEdit) {
        this.canEdit = canEdit;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getUrgName() {
        return urgName;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getDeclName() {
        return declName;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public String getNbName() {
        return nbName;
    }

    public String getStateName() {
        return stateName;
    }

    public Integer getStateId() {
        return stateId;
    }

    public String getUserName() {
        return userName;
    }

    public Integer getCanEdit() {
        return canEdit;
    }
}

/**
 * Вспомогательный класс, расширяющий класс Order и добавляющий
 * некоторые поля, для полного соответствия сущности
 */
class FullOrder extends OrderData {

    private String urgName;

    private Integer nbType;

    private String nbName;

    private String declTypeName;

    public String getUrgName() {
        return urgName;
    }

    void setUrgName(String urgName) {
        this.urgName = urgName;
    }

    public Integer getNbType() {
        return nbType;
    }

    void setNbType(Integer nbType) {
        this.nbType = nbType;
    }

    public String getNbName() {
        return nbName;
    }

    void setNbName(String nbName) {
        this.nbName = nbName;
    }

    public String getDeclTypeName() {
        return declTypeName;
    }

    void setDeclTypeName(String declTypeName) {
        this.declTypeName = declTypeName;
    }
}

/**
 * Всопомогательный класс, содержащий количество сведения о таблице
 * заказов
 */
class OrderList {

    private Integer count;

    private Integer pages;

    private List<Order> orderList;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPages() {
        return pages;
    }

    void setPages(Integer pages) {
        this.pages = pages;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
}
