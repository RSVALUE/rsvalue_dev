package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.nka.rs.dao.IRegistrDTODao;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.constant.registrConstant.ZoneColumnParamEnum;
import org.nka.rs.entity.dto.*;
import org.nka.rs.entity.pojos.load.ATE;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */


@Repository
public class RegistrDTODaoImpl extends BaseDaoSessionImpl implements IRegistrDTODao, IConstant {



    @Override
    public Object[] getRegistr(DataForRegistrWork param) {
        Connection connection = null;
        CallableStatement call = null;
        ResultSet rs = null;

        Object[] registrData = new Object[3];
        List registrs = new ArrayList<RegistrCommon>();
        Long amountRow = 0l;
        Integer amount = 0;

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_LOAD.GET_JOURNAL(?,?,?,?,?,?,?,?,?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);

            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

            if (param.getDateFrom() != null && !param.getDateFrom().equals("")) {
                java.util.Date date = formatter.parse(param.getDateFrom());
                call.setDate(2, new java.sql.Date(date.getTime()));
            } else {
                call.setNull(2, OracleTypes.DATE);
            }
            if (param.getDateTo() != null && !param.getDateTo().equals("")) {
                java.util.Date date = formatter.parse(param.getDateTo());
                call.setDate(3, new java.sql.Date(date.getTime()));

            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            if (param.getAteId() != null) {
                call.setLong(4, param.getAteId());
            } else {
                call.setNull(4, OracleTypes.NUMBER);
            }
            if (param.getCostTypeId() != null) {
                call.setInt(5, param.getCostTypeId());
            } else {
                call.setNull(5, OracleTypes.NUMBER);
            }
            if (param.getFuncCodeId() != null) {
                call.setInt(6, param.getFuncCodeId());
            } else {
                call.setNull(6, OracleTypes.NUMBER);
            }
            if (param.getSortingParam() != null) {
                call.setInt(7, param.getSortingParam());
            } else {
                call.setInt(7, SORTING_DEFAULT);
            }
            if (param.getPredicate() != null && !param.getPredicate().equals("")) {
                call.setString(8, param.getPredicate());
            } else {
                call.setString(8, PREDICATE_DEFAULT);
            }
            if (param.getPage() != null) {
                call.setInt(9, param.getPage());
            } else {
                call.setInt(9, PAGE_DEFAULT);
            }
            if (param.getAmount() != null) {
                call.setInt(10, param.getAmount());
                amount = param.getAmount();
            } else {
                call.setInt(10, AMOUNT_DEFAULT);
                amount = AMOUNT_DEFAULT;
            }
            call.execute();

            rs = (ResultSet) call.getObject(1);

            while (rs.next()) {
                RegistrCommon registr = new RegistrCommon();
                registr.setRegNum(rs.getLong(1));
                registr.setDateRegStart(rs.getString(2));
                registr.setDateVal(rs.getString(3));
                registr.setAteName(rs.getString(4));
                registr.setCostTypeName(rs.getString(5));
                registr.setFuncCodeName(rs.getString(6));
                registr.setActual(rs.getBoolean(7));
                registr.setAteId(rs.getLong(8));
                registr.setCostTypeId(rs.getInt(10));
                registr.setFuncCodeId(rs.getInt(9));

                amountRow = rs.getLong(11);
                registrs.add(registr);
            }
            registrData[0] = registrs;
            if(amountRow%amount == 0){
                registrData[1] = amountRow / amount;
            } else {
                registrData[1] = amountRow / amount + 1;
            }
            registrData[2] = amountRow;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return registrData;
    }

    @Override
    public Registr getUniqueRecord(Long regNum) {
        Session session = null;
        Registr record = null;

        try {
            session = sessionFactory.getCurrentSession();
            String selectClause = "Select distinct lo.regNum as regNum, trunc(co.dateRegStart) as dateRegStart, trunc(co.dateVal) as dateVal, ate.ateName as ateName, costType.codeName as costTypeName,  " +
                    "lo.decNum as decNum, trunc(lo.decDate) as decDate, lo.decOrganization.codeName as decOrganization, lo.valOrganization.codeName as valOrganization, trunc(co.dateRegStop) as dateRegStop, " +
                    "funcCode.codeName as funcCodeName, method.codeName as methodTypeName, lo.remark as remark, co.currency.codeShortName as currencyName  ";
            String fromClause = " From LoadObject as lo JOIN lo.costObjectList as co JOIN co.costType as costType JOIN co.parentObjectnumber as ate  JOIN co.funcCode as funcCode JOIN co.methodType as method ";
            String whereClause = " WHERE lo.regNum = :regNum ";

            String hql = selectClause + fromClause + whereClause;
            Query q = session.createQuery(hql);
            q.setLong("regNum", regNum);

            record = (Registr) q.setResultTransformer(Transformers.aliasToBean(Registr.class)).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return record;
    }

    @Override
    public Registr getUniqueRecord(Long regNum, String funCode) {
        Session session = null;
        Registr record = null;

        try {
            session = sessionFactory.getCurrentSession();
            String selectClause = "Select distinct lo.regNum as regNum, trunc(co.dateRegStart) as dateRegStart, trunc(co.dateVal) as dateVal, ate.ateName as ateName, costType.codeName as costTypeName,  " +
                    "lo.decNum as decNum, trunc(lo.decDate) as decDate, lo.decOrganization.codeName as decOrganization, lo.valOrganization.codeName as valOrganization, trunc(co.dateRegStop) as dateRegStop, " +
                    "funcCode.codeName as funcCodeName, method.codeName as methodTypeName, lo.remark as remark, co.currency.codeShortName as currencyName  ";
            String fromClause = " From LoadObject as lo JOIN lo.costObjectList as co JOIN co.costType as costType JOIN co.parentObjectnumber as ate  JOIN co.funcCode as funcCode JOIN co.methodType as method ";
            String whereClause = " WHERE lo.regNum = :regNum AND funcCode.codeName LIKE :funCode ";

            String hql = selectClause + fromClause + whereClause;
            Query q = session.createQuery(hql);
            q.setLong("regNum", regNum);
            q.setString("funCode", funCode);

            record = (Registr) q.setResultTransformer(Transformers.aliasToBean(Registr.class)).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return record;
    }

    @Override
    public List<DocumentData> getDocumentList(Long regNum) {
        Session session = null;
        List<DocumentData> record = null;

        try {
            session = sessionFactory.getCurrentSession();
            String selectClause = "Select regDoc.redDocId as id, docType.analyticCode as typeId, docType.codeName as typeName, docContent.docPath as path, regDoc.loadDate as loadDate ";
            String fromClause = " From RegistrationDocument as regDoc JOIN regDoc.loadDocType as docType JOIN regDoc.documentContent as docContent ";
            String whereClause = " WHERE regDoc.loadObject.regNum = :regNum and regDoc.actual = 1 ";

            String hql = selectClause + fromClause + whereClause;
            Query q = session.createQuery(hql);
            q.setLong("regNum", regNum);

            record = q.setResultTransformer(Transformers.aliasToBean(DocumentData.class)).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return record;
    }

    @Override
    public List<ThematicLayer> getLayerList(Long regNum) {
        Session session = null;
        List record = null;

        try {
            session = sessionFactory.getCurrentSession();
            String selectClause = "Select layer.layerStartDate as startDate, layerType.shortName as name ";
            String fromClause = " From Layer as layer JOIN layer.layerType as layerType ";
            String whereClause = " WHERE layer.loadObject.regNum = :regNum and layer.layerStopDate is null ";

            String hql = selectClause + fromClause + whereClause;
            Query q = session.createQuery(hql);
            q.setLong("regNum", regNum);

            record = q.setResultTransformer(Transformers.aliasToBean(ThematicLayer.class)).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return record;
    }

    @Override
    public Object[] getZoneList(Long regNum, Integer sortingParam, String predicate, Integer page) {
        Session session = null;
        Object[] zoneData = new Object[2];
        List record = null;
        Long pageAmount;

        try {
            session = sessionFactory.getCurrentSession();
            String selectClause = "Select zone.zoneNum as num, parent_ate.ateName as distr, zone_data.zoneName as name, zone.costD as cost_d, zone.costR as cost_r, " +
                    "ate.ateId as objectnumb, zone.square as area, zone_data.zoneDescription as category, zone_data.nearLoc as npname, zone_data.unp as unp ";
            String fromClause = " From CostZone as zone JOIN zone.costObject.objectnumber as ate " +
                    "JOIN zone.costObject.parentObjectnumber as parent_ate JOIN zone.costZoneData as zone_data  ";

            String whereClause = " WHERE zone.costObject.loadObject.regNum = :regNum ";
            if (sortingParam != null && !sortingParam.equals(FALSE_GROUPING)) {
                whereClause += " order by " + ZoneColumnParamEnum.search(sortingParam).getQueryName() + " ";
            } else {
                whereClause += " order by " + ZoneColumnParamEnum.ZONE_NUM.getQueryName() + " ";
            }

            //сортировка по возрастанию/убыванию
            if (predicate != null && !predicate.equals("")) {
                whereClause += predicate;
            } else {
                whereClause += SORTING_DEFAULT;
            }

            //задать номер страницы
            if (page == null) {
                page = PAGE_DEFAULT;
            }
            //задать количество строк на странице
            Integer amount = ZONE_AMOUNT_DEFAULT;

            String hql = selectClause + fromClause + whereClause;
            Query q = session.createQuery(hql).setFirstResult(amount * (page - 1))
                    .setMaxResults(amount);
            q.setLong("regNum", regNum);

            record = q.setResultTransformer(Transformers.aliasToBean(Zone.class)).list();
            zoneData[0] = record;

            selectClause = "Select count(zone.zoneNum)";
            String amountStr = selectClause + fromClause + whereClause;
            Query qAmount = session.createQuery(amountStr);
            qAmount.setLong("regNum", regNum);

            pageAmount = (Long) qAmount.uniqueResult();
            zoneData[1] = pageAmount / amount + 1;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return zoneData;
    }

    @Override
    public Object[] getManagedZoneList(Long regNum, Integer[] columnNums, Integer sortingParam, String
            predicate, Integer page) {
        Session session = null;
        Object[] zoneData = new Object[2];
        List record = null;
        Long pageAmount;

        try {
            session = sessionFactory.getCurrentSession();
            StringBuffer selectClause = new StringBuffer("Select ");
            if (columnNums == null || columnNums[0] == FALSE_GROUPING) {
                columnNums = DEFAULT_ZONE_COLUMNS;
            }
            for (int i = 0; i < columnNums.length; i++) {
                if (i != (columnNums.length - 1)) {
                    selectClause.append(ZoneColumnParamEnum.search(columnNums[i]).getQueryName() + "as " + ZoneColumnParamEnum.search(columnNums[i]).getAlias() + ", ");
                } else {
                    selectClause.append(ZoneColumnParamEnum.search(columnNums[i]).getQueryName() + "as " + ZoneColumnParamEnum.search(columnNums[i]).getAlias());
                }
            }

            String fromClause = " From CostZone as zone JOIN zone.costObject.objectnumber as ate " +
                    "JOIN zone.costObject.parentObjectnumber as parent_ate JOIN zone.costZoneData as zone_data  ";

            String whereClause = " WHERE zone.costObject.loadObject.regNum = :regNum ";
            if (sortingParam != null && !sortingParam.equals(FALSE_GROUPING)) {
                whereClause += " order by " + ZoneColumnParamEnum.search(sortingParam).getQueryName() + " ";
            } else {
                whereClause += " order by " + ZoneColumnParamEnum.search(columnNums[0]).getQueryName() + " ";
            }

            //сортировка по возрастанию/убыванию
            if (predicate != null && !predicate.equals("")) {
                whereClause += predicate;
            } else {
                whereClause += SORTING_DEFAULT;
            }

            //задать номер страницы
            if (page == null) {
                page = PAGE_DEFAULT;
            }
            //задать количество строк на странице
            Integer amount = ZONE_AMOUNT_DEFAULT;

            String hql = selectClause.toString() + fromClause + whereClause;
            Query q = session.createQuery(hql).setFirstResult(amount * (page - 1))
                    .setMaxResults(amount);
            q.setLong("regNum", regNum);

            record = q.setResultTransformer(Transformers.aliasToBean(Zone.class)).list();
            zoneData[0] = record;

            selectClause = new StringBuffer("Select count(" + ZoneColumnParamEnum.search(columnNums[0]).getQueryName() + ")");
            String amountStr = selectClause.toString() + fromClause + whereClause;
            Query qAmount = session.createQuery(amountStr);
            qAmount.setLong("regNum", regNum);

            pageAmount = (Long) qAmount.uniqueResult();
            zoneData[1] = pageAmount / amount + 1;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return zoneData;
    }

    @Override
    public List searchByATE(String partName) {
        Session session = null;
        List record = null;

        try {
            session = sessionFactory.getCurrentSession();
            String selectClause = "Select ate.ateName as ateName, ate.ateId as ateId ";
            String fromClause = " From ATE ate ";
            String whereClause = " WHERE lower(ate.ateName) LIKE lower(:partName) and ate.parentCategory IN (:area, :city)";

            String hql = selectClause + fromClause + whereClause;
            Query q = session.createQuery(hql);
            q.setString("partName", "%" + partName + "%");
            q.setInteger("area", AREA);
            q.setInteger("city", CITY);
            record = q.setResultTransformer(Transformers.aliasToBean(ATE.class)).list();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return record;
    }

    @Override
    public String downloadDoc(Long docId) {
        Session session = null;
        Connection connection = null;
        CallableStatement call = null;
        String path = "";
        try {
            session = sessionFactory.getCurrentSession();

            String selectClause = "Select regDoc.documentContent.docPath ";
            String fromClause = " From RegistrationDocument regDoc";
            String whereClause = " WHERE regDoc.redDocId = :docId and regDoc.actual = true ";

            String hql = selectClause + fromClause + whereClause;
            Query q = session.createQuery(hql);
            q.setLong("docId", docId);

            path = (String) q.uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return path;
    }

    @Override
    public Object[] getLoadInfo(Long regNum) {
        Session session = null;
        Connection connection = null;
        CallableStatement call = null;
        ResultSet res = null;

        Object[] data = new Object[6];

        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_EDIT.GET_LOAD_INF(?)}");
            //возвращаемый параметр - курсор, сведения по загрузке
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, regNum);

            call.execute();
            res = (ResultSet) call.getObject(1);
            while (res.next()) {
                //вид оценки
                data[0] = res.getInt(1);
                //метод оценки
                data[1] = res.getInt(2);
                //вид геометрии
                data[2] = res.getInt(3);
                //функциональное назначение
                data[3] = res.getInt(4);
                //город/район по которому проводилась оценка
                data[4] = res.getLong(5);
                //были ли загружены тематические слои
                data[5] = res.getBoolean(6);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return data;
    }

    @Override
    public Response saveEditDoc(EditDocumentData data) {
        Session session = null;
        Connection connection = null;
        CallableStatement call = null;

        Response response = new Response();

        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_EDIT.EDIT_DOC(?,?,?,?,?,?)}");
            //возвращаемый параметр - курсор, сведения по загрузке
            call.registerOutParameter(1, OracleTypes.CLOB);
            call.setLong(2, data.getRegNum());
            if (data.getDeleteDocList() != null && data.getDeleteDocList().length != 0) {
                String delList = "";
                for (Long delDoc : data.getDeleteDocList()) {
                    delList += delDoc.toString() + ";";
                }
                call.setString(3, delList);
            } else {
                call.setNull(3, OracleTypes.VARCHAR);
            }
            if (data.getSaveDocList() != null && data.getSaveDocList().length != 0) {
                String insList = "";
                for (Long insDoc : data.getSaveDocList()) {
                    insList += insDoc.toString() + ";";
                }
                call.setString(4, insList);
            } else {
                call.setNull(4, OracleTypes.VARCHAR);
            }
            call.setLong(5, data.getUserId());
            call.setBoolean(6, data.getSave());
            if (data.getDwdId() != null) {
                call.setLong(7, data.getDwdId());
            } else {
                call.setNull(7, OracleTypes.NUMBER);
            }

            call.execute();

            String path = call.getString(1);

            response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            response.setMessage(path);

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.EXCEPTION.getValue());
            response.setMessage("Ошибка при сохранении данных");

        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    public Long editFile(String path, Integer docTypeId, Long userId, Long regNum) {
        Session session = null;
        Connection connection = null;
        CallableStatement call = null;
        ResultSet res = null;
        Long regDocId = null;
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_EDIT.INS_DOC(?,?,?,?)}");

            call.registerOutParameter(1, OracleTypes.NUMBER);
            call.setString(2, path);
            call.setInt(3, docTypeId);
            call.setLong(4, userId);
            call.setLong(5, regNum);

            call.execute();
            regDocId = call.getLong(1);

        } catch (Exception e) {
            e.printStackTrace();
            regDocId = null;
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return regDocId;
    }

    @Override
    public Integer findLayerCodeByLayerFileName(String layerName) {
        Integer code = null;
        Session session = null;
        try {
            session = sessionFactory.getCurrentSession();

            Query q = session.createQuery("select lay.analyticCode FROM LayerTypeDic lay " +
                    " where lower(:layerName) like (lay.fileName||'%' )");

            q.setString("layerName", layerName);
            code = (Integer) q.uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }

    @Override
    public Response insertEditLayer(Long reg_num, String layerName, Long userId, String values) {
        Response response = new Response();

        Session session = null;
        Connection connection = null;
        CallableStatement call = null;

        Integer code = findLayerCodeByLayerFileName(layerName);
        if (code == null) {
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении кода слоя");
            return response;
        }

        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ = call " + SCHEMA_NAME + ".PKG_EDIT.INS_LAYER(?,?,?,?)}");
            call.setLong(1, reg_num);
            call.setInt(2, code);
            call.setLong(3, userId);
            call.setString(4, values);
            call.execute();

            response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            response.setMessage("");

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.EXCEPTION.getValue());
            response.setMessage("Ошибка при сохранении данных");

        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return response;
    }

}

