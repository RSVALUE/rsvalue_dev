package org.nka.rs.dao;

import org.hibernate.criterion.DetachedCriteria;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface IBaseDao<E> {

    Serializable addElement(E e) ;

    void updateElement(E el);
    void updateElementById(Class<E> clazz, Serializable id);

    void refreshElement(E el);
    void refreshElementById(Class<E> clazz, Serializable id);

    void deleteElement(E el);
    void deleteElementById(Class<E> clazz, Serializable id);

    List<E> getAllElements(Class<E> clazz);
    E getElementByID(Class<E> clazz, Serializable id);

    List<E> getCriterion(DetachedCriteria crio);

    E getUniqueResult(DetachedCriteria crio);
}
