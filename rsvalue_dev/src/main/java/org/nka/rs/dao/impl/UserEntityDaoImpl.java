package org.nka.rs.dao.impl;

import org.hibernate.SessionFactory;
import org.nka.rs.dao.IUserEntityDao;
import org.nka.rs.entity.pojos.common.UserEntity;
import org.nka.rs.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserEntityDaoImpl extends BaseDaoSessionImpl<UserEntity> implements IUserEntityDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public UserEntity getUserByName(String username) {
        return (UserEntity) sessionFactory.getCurrentSession().createQuery("from UserEntity u where u.username=:username").setString("username", username).uniqueResult();
    }

    @Override
    public Long getIdByName(String username) {
        return (Long) sessionFactory.getCurrentSession().createQuery("select u.id from UserEntity u where u.username=:username").setString("username", username).uniqueResult();
    }

    @Override
    public List<String> getLogins() {
        return sessionFactory.getCurrentSession().createQuery("select u.username from UserEntity u").list();
    }

    /**
     * Находит объект UserEntity с указанным логином пользователя.
     * После чего проводит проверку на соответствия полученного токена и
     * токена взятого из объекта БД, если проверка пройдена, то обнуляет
     * токен пользователя в поле базы.
     *
     * @param username Логин пользователя
     * @param token Токен пользователя
     */
    @Override
    public void logout(String username, String token) {
        UserEntity userEntity = (UserEntity) sessionFactory.getCurrentSession().createQuery("from UserEntity u where u.username=:username")
                .setString("username", username)
                .uniqueResult();
        if (userEntity.getToken().equals(token)) {
            userEntity.setToken("");
            sessionFactory.getCurrentSession().update(userEntity);
        } else {
            throw new BadCredentialsException("Неправильные данные");
        }
    }

    /**
     * Создает токен для пользователя с идентификатором ID используя
     * его логин. После получает объект UserEntity из базы и устанавливает
     * полученный токен в его поле. Сохраняет обновленный объект
     *
     * @param id ID пользователя
     * @param username Логин пользователя
     * @return
     */
    @Override
    public String createAndSaveToken(Long id, String username) {
        String token = Util.createToken(username);
        UserEntity userEntity = (UserEntity) sessionFactory.getCurrentSession().get(UserEntity.class, id);
        userEntity.setToken(token);
        sessionFactory.getCurrentSession().update(userEntity);
        return token;
    }

    @Override
    public String getToken(Long id) {
        return (String) sessionFactory.getCurrentSession().createQuery("select u.token from UserEntity u where u.id=:id")
                .setLong("id",id).uniqueResult();
    }
}
