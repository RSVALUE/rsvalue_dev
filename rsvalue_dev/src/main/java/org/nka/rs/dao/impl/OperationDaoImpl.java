package org.nka.rs.dao.impl;

import org.hibernate.SessionFactory;
import org.nka.rs.dao.IOperationDao;
import org.nka.rs.entity.pojos.common.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;


@Repository
public class OperationDaoImpl extends BaseDaoSessionImpl<Operation> implements IOperationDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Operation getElementByID(Long id) {
        return (Operation) sessionFactory.getCurrentSession().get(Operation.class, id);
    }

    @Override
    public Long getIdByDate(Date date) {
        return (Long) sessionFactory.getCurrentSession()
                .createQuery("Select o.id from Operation o where operDate=:date")
                .setDate("date", date)
                .uniqueResult();
    }
}
