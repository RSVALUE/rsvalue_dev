package org.nka.rs.dao.JDBCDao.docs;

import oracle.jdbc.OracleTypes;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.pojos.docs.ActInfo;
import org.nka.rs.entity.pojos.docs.FormInfo;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.util.connection.UtilConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * DAO класс, в котором содержатся методы для извлечения информации,
 * необходимой для формирования выписок и счет-акта
 */
public class FormExtractDao {

    /**
     * Метод получает и парсит всю необходимую информцию по выписке и
     * инициализирует соответствующий объект.
     *
     * @param objId ID объекта, по которому формируется выписка
     * @param execId ID пользователя, который послал запрос на выписку
     * @return Объект FormInfo с информацией для выписки
     */
    public FormInfo getDocInfo(Long objId, Long execId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.DOC_COST ( ?,? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        FormInfo formInfo = new FormInfo();
        String infoString = "";

        List<String> funcPurpose = new ArrayList<String>();
        List<String> valueDate = new ArrayList<String>();
        List<Long> zoneNumber = new ArrayList<Long>();
        List<String> costUSD = new ArrayList<String>();
        List<String> costBYN = new ArrayList<String>();
        List<String> costSquareUSD = new ArrayList<String>();
        List<String> costSquareBYN = new ArrayList<String>();
        List<Integer> foundationNumber = new ArrayList<Integer>();
        List<Long> zoneList = new ArrayList<Long>();
        List<String> parentAteList = new ArrayList<String>();
        String foundationString = "";
        String attachmentString = "";
        int currency = 0;

        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, objId);
            callableStatement.setLong(3, execId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                formInfo.setFormName(rs.getString(1));
                formInfo.setFormDate(rs.getString(2));
                formInfo.setSearchDate(rs.getString(3));
                formInfo.setFormNumber(rs.getString(4));
                infoString = rs.getString(5);
                currency = (rs.getInt(6));
                funcPurpose.add(rs.getString(7));
                valueDate.add(rs.getString(8));
                zoneNumber.add(rs.getLong(9));
                costUSD.add(rs.getString(10));
                costBYN.add(rs.getString(11));
                costSquareUSD.add(rs.getString(12));
                costSquareBYN.add(rs.getString(13));
                foundationNumber.add(rs.getInt(14));
                foundationString = rs.getString(15);
                attachmentString = rs.getString(16);
                formInfo.setNbYear(rs.getInt(17));
                formInfo.setExecPost(rs.getString(18));
                formInfo.setExecFullName(rs.getString(19));
                zoneList.add(rs.getLong(20));
                parentAteList.add(rs.getString(21));
            }
            if (infoString != null) {
                String[] infoArray = infoString.split(";");
                formInfo.setFormInfo(Arrays.asList(infoArray));
            } else {
                formInfo.setAttachmentList(null);
            }
            if (foundationString != null) {
                String[] foundationArray = foundationString.split("]");
                formInfo.setFoundationList(Arrays.asList(foundationArray));
            } else {
                formInfo.setAttachmentList(null);
            }
            if (attachmentString != null) {
                String[] attachmentArray = attachmentString.split("]");
                formInfo.setAttachmentList(Arrays.asList(attachmentArray));
            } else {
                formInfo.setAttachmentList(null);
            }
            formInfo.setCurrency(currency);
            formInfo.setFuncPurpose(funcPurpose);
            formInfo.setValueDate(valueDate);
            formInfo.setZoneNumber(zoneNumber);
            formInfo.setCostUSD(costUSD);
            formInfo.setCostBYN(costBYN);
            formInfo.setCostSquareUSD(costSquareUSD);
            formInfo.setCostSquareBYN(costSquareBYN);
            formInfo.setFoundationNumber(foundationNumber);
            formInfo.setZoneId(zoneList);
            formInfo.setParentAte(parentAteList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return formInfo;
    }

    /**
     * Метод получает и парсит всю необходимую информцию по счет-акту и
     * инициализирует объект-ответ для отправки на предварительный просмотр
     *
     * @param orderId ID заказа, по которому формируется счет-акт
     * @param execId ID пользователя, который послал запрос на выписку
     * @return Объект ResponseWithData с данными по счет-акту
     */
    public ResponseWithData getActInfo(Long orderId, Long execId) {
        String sqlString = String.format("{ ? = call %s.PKG_ORDERS.DOC_INVOCE( ?, ? )}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        ActInfo actInfo = new ActInfo();
        ResponseWithData responseWithData = new ResponseWithData();
        boolean flag = false;
        List<String> typeList = new ArrayList<String>();
        List<String> addressList = new ArrayList<String>();
        List<String> cadNumList = new ArrayList<String>();

        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, orderId);
            callableStatement.setLong(3, execId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            while (rs.next()) {
                if (flag = checkType(rs.getInt(1), responseWithData)) {
                    break;
                } else {
                    actInfo.setActType(rs.getString(2));
                    actInfo.setActName(rs.getString(3));
                    actInfo.setActDate(rs.getString(4));
                    actInfo.setSubjName(rs.getString(5));
                    actInfo.setCadOrg(rs.getString(6));
                    actInfo.setExecPost(rs.getString(7));
                    actInfo.setExecFullName(rs.getString(8));
                    actInfo.setDateIn(rs.getString(9));
                    actInfo.setNumberIn(rs.getString(10));
                    typeList.add(rs.getString(11));
                    addressList.add(rs.getString(12));
                    cadNumList.add(rs.getString(13));
                    actInfo.setSum(rs.getString(14));
                    actInfo.setSumString(rs.getString(15));
                    actInfo.setNdsSum(rs.getString(16));
                    actInfo.setNdsSumString(rs.getString(17));
                    actInfo.setPriceDate(rs.getString(18));
                    actInfo.setPriceNumber(rs.getString(19));
                    actInfo.setRequisites(rs.getString(20));
                    actInfo.setExecFIO(rs.getString(21));
                    actInfo.setYear(rs.getString(22));
                }
            }
            if (!flag) {
                if (typeList.size() != 0) {
                    actInfo.setObjTypeList(typeList);
                } else {
                    actInfo.setObjTypeList(null);
                }
                if (typeList.size() != 0) {
                    actInfo.setObjAddressList(addressList);
                } else {
                    actInfo.setObjAddressList(null);
                }

                if (typeList.size() != 0) {
                    actInfo.setObjCadNumList(cadNumList);
                } else {
                    actInfo.setObjCadNumList(null);
                }
                responseWithData = initSuccessResponse(actInfo);
            }
            System.out.println(actInfo.toString());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            responseWithData = initUnsuccessResponse();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return responseWithData;
    }

    /**
     * Метод, инициализирующий неуспешный объект-ответ
     *
     * @return Объект ResponseWithData с данными о неуспешном выполнении запроса
     */
    private ResponseWithData initUnsuccessResponse() {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
        responseWithData.setMessage("Ошибка при получении данных из базы.\nОбратитесь к системному адмиристратору");
        return responseWithData;
    }

    /**
     * Метод, инициализирующий успешный объект-ответ с данными
     * по счет-акту
     *
     * @param actInfo Данные по счет-акту
     * @return Объект ResponseWithData с данными по счет-акту
     */
    private ResponseWithData initSuccessResponse(ActInfo actInfo) {
        ResponseWithData responseWithData = new ResponseWithData();
        responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
        responseWithData.setMessage("Данные по заказу найдены");
        responseWithData.setData(actInfo);
        return responseWithData;
    }

    /**
     * Проверяет результат выполнения функции. В зависимости от полученного
     * значения устанавливает необходимое значение в объект-ответ
     *
     * @param type Тип результата выполнения функции
     * @param responseWithData Объект-ответ
     * @return True - если произошла одна из ошибок, false - если все хорошо
     */
    private boolean checkType(int type, ResponseWithData responseWithData) {
        boolean flag = false;
        if (type == 1) {
            responseWithData.setCode(ErrorCodeEnum.SUCCESS.getValue());
            responseWithData.setMessage("Ok");
        } else if (type == -1) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage("Актуальный прейскурант не найден. Стоимость работ определить невозможно");
            flag = true;
        } else if (type == -2) {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage("В прейскуранте отражена стоимость не для всех типов документов.");
            flag = true;
        } else {
            responseWithData.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            responseWithData.setMessage("Неизвестный тип ошибки (тип ошибки не определен");
            flag = true;
        }
        return flag;
    }
}
