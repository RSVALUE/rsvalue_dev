package org.nka.rs.dao.JDBCDao.search;

import oracle.jdbc.OracleTypes;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dto.DataForRegistrWork;
import org.nka.rs.entity.dto.Registr;
import org.nka.rs.entity.dto.RegistrCommon;
import org.nka.rs.entity.dto.ThematicLayer;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.nka.rs.entity.constant.commonConstant.IConstant.*;

/**
 * RegisterDAO переписанное без использования Hibernate
 */

@Repository
public class RegisterDAO {

    public Object[] getRegistr(DataForRegistrWork registrData) {
        Integer amount = getAmount(registrData);
        Long amountRow = 0L;
        RegistrCommon registrCommon = null;
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;
        List<RegistrCommon> registrs = new ArrayList<RegistrCommon>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = initCallableStatement(registrData, pooledConnection);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                registrCommon = initRegisterCommon(rs);
                amountRow = rs.getLong(11);
                registrs.add(registrCommon);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println(e.getErrorCode());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return initRegisterData(registrs, amountRow, amount);
    }

    private RegistrCommon initRegisterCommon(ResultSet rs) throws SQLException {
        RegistrCommon registrCommon = new RegistrCommon();
        registrCommon.setRegNum(rs.getLong(1));
        registrCommon.setDateRegStart(rs.getString(2));
        registrCommon.setDateVal(rs.getString(3));
        registrCommon.setAteName(rs.getString(4));
        registrCommon.setCostTypeName(rs.getString(5));
        registrCommon.setFuncCodeName(rs.getString(6));
        registrCommon.setActual(rs.getBoolean(7));
        registrCommon.setAteId(rs.getLong(8));
        registrCommon.setCostTypeId(rs.getInt(10));
        registrCommon.setFuncCodeId(rs.getInt(9));
        return registrCommon;
    }

    private Object[] initRegisterData(List<RegistrCommon> registers, Long amountRow, Integer amount) {
        Object[] registerData = new Object[3];
        registerData[0] = registers;
        if (amountRow % amount == 0) {
            registerData[1] = amountRow / amount;
        } else {
            registerData[1] = amountRow / amount + 1;
        }
        registerData[2] = amountRow;
        return registerData;
    }

    private CallableStatement initCallableStatement(DataForRegistrWork registrData, Connection pooledConnection) throws SQLException, ParseException {
        String sqlString = String.format("{? = call %s.PKG_LOAD.GET_JOURNAL(?,?,?,?,?,?,?,?,?,?)}", IConstant.SCHEMA_NAME);

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        CallableStatement callableStatement = pooledConnection.prepareCall(sqlString);
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        if (registrData.getDateFrom() != null && !registrData.getDateFrom().equals("")) {
            callableStatement.setDate(2, new java.sql.Date(formatter.parse(registrData.getDateFrom()).getTime()));
        } else {
            callableStatement.setNull(2, OracleTypes.DATE);
        }
        if (registrData.getDateTo() != null && !registrData.getDateTo().equals("")) {
            callableStatement.setDate(3, new java.sql.Date(formatter.parse(registrData.getDateFrom()).getTime()));

        } else {
            callableStatement.setNull(3, OracleTypes.DATE);
        }
        if (registrData.getAteId() != null) {
            callableStatement.setLong(4, registrData.getAteId());
        } else {
            callableStatement.setNull(4, OracleTypes.NUMBER);
        }
        if (registrData.getCostTypeId() != null) {
            callableStatement.setInt(5, registrData.getCostTypeId());
        } else {
            callableStatement.setNull(5, OracleTypes.NUMBER);
        }
        if (registrData.getFuncCodeId() != null) {
            callableStatement.setInt(6, registrData.getFuncCodeId());
        } else {
            callableStatement.setNull(6, OracleTypes.NUMBER);
        }
        if (registrData.getSortingParam() != null) {
            callableStatement.setInt(7, registrData.getSortingParam());
        } else {
            callableStatement.setInt(7, SORTING_DEFAULT);
        }
        if (registrData.getPredicate() != null && !registrData.getPredicate().equals("")) {
            callableStatement.setString(8, registrData.getPredicate());
        } else {
            callableStatement.setString(8, PREDICATE_DEFAULT);
        }
        if (registrData.getPage() != null) {
            callableStatement.setInt(9, registrData.getPage());
        } else {
            callableStatement.setInt(9, PAGE_DEFAULT);
        }
        if (registrData.getAmount() != null) {
            callableStatement.setInt(10, registrData.getAmount());
        } else {
            callableStatement.setInt(10, AMOUNT_DEFAULT);
        }
        if (registrData.getActual() != null) {
            callableStatement.setInt(11, registrData.getActual());
        } else {
            callableStatement.setNull(11, OracleTypes.NUMBER);
        }
        return callableStatement;
    }

    private Integer getAmount(DataForRegistrWork registrData) {
        if (registrData.getAmount() != null) {
            return registrData.getAmount();
        } else {
            return AMOUNT_DEFAULT;
        }
    }

    public Registr getLoadInf(Long regCode, Integer funcCode) {
        String sqlString = String.format("{ ? = call %s.PKG_LOAD.GET_INF_LOAD( ?,? ) }", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;
        List<String> result = new ArrayList<String>();
        Registr registr = new Registr();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, regCode);
            callableStatement.setInt(3, funcCode);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                registr.setRegNum(rs.getLong(1));
                registr.setDateRegStart(rs.getDate(2));
                registr.setDateVal(rs.getDate(3));
                registr.setCostTypeName(rs.getString(4));
                registr.setFuncCodeName(rs.getString(5));
                registr.setMethodTypeName(rs.getString(6));
                registr.setAteName(rs.getString(7));
                registr.setValOrganization(rs.getString(8));
                registr.setDecNum(rs.getString(9));
                registr.setDecDate(rs.getDate(10));
                registr.setDecOrganization(rs.getString(11));
                registr.setCurrencyName(rs.getString(12));
                registr.setRemark(rs.getString(13));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return registr;
    }

    public List<ThematicLayer> getLayerList(Long regNum) {
        String sqlString = String.format("{ ? = call %s.PKG_LOAD.GET_LIST_LAYER( ? ) }", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;
        List<ThematicLayer> thematicLayers = new ArrayList<ThematicLayer>();
        ThematicLayer layer = null;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, regNum);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                layer = new ThematicLayer();
                layer.setName(rs.getString(1));
                layer.setStartDate(rs.getDate(2));
                thematicLayers.add(layer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return thematicLayers;
    }

    public List<Object[]> getZoneList(Long regCode, Integer funcCode) {
        String sqlString = String.format("{ ? = call %s.PKG_LOAD.GET_LIST_ZONE( ?,? ) }", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;
        List<Object[]> result = new ArrayList<Object[]>();
        Object[] zoneData = null;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setLong(2, regCode);
            callableStatement.setInt(3, funcCode);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                zoneData = new Object[4];
                zoneData[0] = rs.getString(1);
                zoneData[1] = rs.getString(2);
                zoneData[2] = rs.getString(3);
                zoneData[3] = rs.getString(4);
                result.add(zoneData);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return result;
    }

}
