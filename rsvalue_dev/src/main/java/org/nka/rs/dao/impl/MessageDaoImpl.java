package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.nka.rs.dao.IMessageDao;
import org.nka.rs.dao.postgreSQL.PostgreSQLJDBC;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dto.emessage.AddressMessage;
import org.nka.rs.entity.dto.emessage.CadastralValueData;
import org.nka.rs.entity.dto.emessage.CadastralValueMessage;
import org.nka.rs.entity.dto.emessage.Foundation;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zgurskaya on 05.10.2016.
 */
@Repository
public class MessageDaoImpl implements IMessageDao, IConstant {

    @Autowired
    SessionFactory sessionFactory;

    //+
    @Override
    public ResponseWithData cadastrValueSearch(Long soato, Integer blockNum, Integer parcelNum, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        ResponseWithData response = new ResponseWithData();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_CADNUM(?, ?, ?, ?, ?)}");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (soato != null) {
                call.setLong(2, soato);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (blockNum != null) {
                call.setInt(3, blockNum);
            } else {
                call.setNull(3, OracleTypes.NUMBER);
            }
            if (parcelNum != null) {
                call.setInt(4, parcelNum);
            } else {
                call.setNull(4, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(5, sqlDate);
            } else {
                call.setNull(5, OracleTypes.DATE);
            }
            call.setBoolean(6, isNb);

            call.execute();
            rs = (ResultSet) call.getObject(1);
            String fStr = "";
            String remark = "";
            int count = 0;

            while (rs.next()) {
                if (rs.getString(1).equals("-1")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельные участки не найдены");
                    return response;
                } else if (rs.getString(1).equals("-2")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельный участок находится в садоводческом товариществе. Осуществите соответствующий поиск");
                    return response;
                } else if (rs.getString(1).equals("-3")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("У данного участка нет геометрии");
                    return response;
                } else if (rs.getString(1).equals("-4")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Не найдены оценочные зоны");
                } else if (rs.getString(1).equals("1")) {
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                }
                if (rs.getString(2) != null) {
                    String commonData = rs.getString(2);
                    String[] parts = commonData.split(";");
                    if (parts != null && parts.length != 0) {
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setAddress(parts[0]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setCadNum(parts[1]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setSquare(Double.parseDouble(parts[2]));
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setPurposeDic(parts[3]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setPurposeGov(parts[4]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setSearchDate(parts[5]);
                        }
                    }
                }
                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                if (rs.getDate(4) != null) {
                    data.setDateVal(rs.getDate(4));
                }
                if (rs.getString(5) != null && !rs.getString(5).equals("")) {
                    data.setZoneNum(rs.getString(5));
                }
                if ((Double) rs.getDouble(6) != null) {
                    data.setCostD(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null && isNb) {
                    data.setCostNB(rs.getDouble(7));
                }
                if ((Double) rs.getDouble(8) != null) {
                    data.setCostR(rs.getDouble(8));
                }
                if (rs.getDate(9) != null) {
                    foundation.setDateRegStart(rs.getDate(9));
                }

                if ((Long) rs.getLong(10) != null) {
                    foundation.setRegNum(rs.getLong(10));
                }
                if (rs.getString(11) != null && !rs.getString(11).equals("")) {
                    foundation.setOrganizationPrintName(rs.getString(11));
                }
                if (rs.getDate(12) != null) {
                    foundation.setDecDate(rs.getDate(12));
                }
                if (rs.getString(13) != null && !rs.getString(13).equals("")) {
                    foundation.setDecNum(rs.getString(13));
                }

                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                if (!fStr.equals(foundation.toString())) {
                    count++;
                    fStr = foundation.toString();
                    remark += "<" + count + ">" + data.getFoundationStr() + "\n";
                }
                if( rs.getString(3) != null && !rs.getString(3).equals("")) {
                    data.setFuncCodeName(rs.getString(3) + "<" + count + ">");
                }
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }
            message.setRemark(remark);
            response.setData(message);


        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении данных с базы");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    //+
    @Override
    public ResponseWithData cadastrValueSearch(String cadNum, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        ResponseWithData response = new ResponseWithData();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_CADNUM(?, ?, ?)}");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (cadNum != null) {
                call.setString(2, cadNum);
            } else {
                call.setNull(2, OracleTypes.VARCHAR);
            }
            if (date != null && !date.equals("")) {
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                Date currentDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(currentDate.getTime());
                call.setDate(3, sqlDate);
            }
            call.setBoolean(4, isNb);

            call.execute();
            rs = (ResultSet) call.getObject(1);
            String fStr = "";
            String remark = "";
            int count = 0;

            while (rs.next()) {
                if (rs.getString(1).equals("-1")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельные участки не найдены");
                    return response;
                } else if (rs.getString(1).equals("-2")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельный участок находится в садоводческом товариществе. Осуществите соответствующий поиск");
                    return response;
                } else if (rs.getString(1).equals("-3")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("У данного участка нет геометрии");
                    return response;
                } else if (rs.getString(1).equals("-4")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Не найдены оценочные зоны");
                } else if (rs.getString(1).equals("1")) {
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                }
                if (rs.getString(2) != null) {
                    String commonData = rs.getString(2);
                    String[] parts = commonData.split(";");
                    if (parts != null && parts.length != 0) {
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setAddress(parts[0]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setCadNum(parts[1]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setSquare(Double.parseDouble(parts[2]));
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setPurposeDic(parts[3]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setPurposeGov(parts[4]);
                        }
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setSearchDate(parts[5]);
                        }
                    }
                }
                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                if (rs.getDate(4) != null) {
                    data.setDateVal(rs.getDate(4));
                }
                if (rs.getString(5) != null && !rs.getString(5).equals("")) {
                    data.setZoneNum(rs.getString(5));
                }
                if ((Double) rs.getDouble(6) != null) {
                    data.setCostD(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null && isNb) {
                    data.setCostNB(rs.getDouble(7));
                }
                if ((Double) rs.getDouble(8) != null) {
                    data.setCostR(rs.getDouble(8));
                }
                if (rs.getDate(9) != null) {
                    foundation.setDateRegStart(rs.getDate(9));
                }

                if ((Long) rs.getLong(10) != null) {
                    foundation.setRegNum(rs.getLong(10));
                }
                if (rs.getString(11) != null && !rs.getString(11).equals("")) {
                    foundation.setOrganizationPrintName(rs.getString(11));
                }
                if (rs.getDate(12) != null) {
                    foundation.setDecDate(rs.getDate(12));
                }
                if (rs.getString(13) != null && !rs.getString(13).equals("")) {
                    foundation.setDecNum(rs.getString(13));
                }

                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                if (!fStr.equals(foundation.toString())) {
                    count++;
                    fStr = foundation.toString();
                    remark += "<" + count + ">" + data.getFoundationStr() + "\n";
                }
                if( rs.getString(3) != null && !rs.getString(3).equals("")) {
                    data.setFuncCodeName(rs.getString(3) + "<" + count + ">");
                }
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }
            message.setRemark(remark);
            response.setData(message);

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении данных с базы");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    //+
    @Override
    public AddressMessage addressSearch(Long objectnumber, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;

        AddressMessage message = new AddressMessage();
        List<String> cadnumList = new ArrayList<String>();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_ADDR1(?, ?, ?)}");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            call.registerOutParameter(1, OracleTypes.VARCHAR);
            if (objectnumber != null) {
                call.setLong(2, objectnumber);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            call.setBoolean(4, isNb);
            Long time = System.currentTimeMillis();

            call.execute();

            time = System.currentTimeMillis() - time;
            System.out.print(time);

            String resultSet = call.getString(1);
            String[] parts = resultSet.split(";");
            if (parts[0].equals("-1")) {
                message.setCode(-1);
                message.setResponse("По адресу " + parts[1] + " не имеется координат. Дальнейший поиск невозможен!");
            } else if (parts[0].equals("1")) {
                for (int i = 1; i < parts.length; i++) {
                    cadnumList.add(parts[i]);
                }
                message.setCode(1);
                message.setCadnumList(cadnumList);
            } else if (parts[0].equals("2")) {
                message.setCode(2);
                message.setAddress(parts[1]);
                message.setZones(parts[2]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    @Override
    public AddressMessage mapSearch(String coord, Boolean isCadNum) {

        PostgreSQLJDBC postgreSQLJDBC = new PostgreSQLJDBC();
        Connection postgreCon = null;
        CallableStatement call = null;

        AddressMessage message = new AddressMessage();
        List<String> cadnumList = new ArrayList<String>();

        try {
            postgreCon = postgreSQLJDBC.getConnection();
            call = postgreCon.prepareCall("{ ? = call rvalue.search_by_address_coord(?,?)}");
            call.registerOutParameter(1, Types.VARCHAR);
            call.setString(2, coord);
            call.setBoolean(3, isCadNum);
            call.execute();

            String resultSet = call.getString(1);
            String[] parts = resultSet.split(";");
            if (parts[0].equals("1")) {
                for (int i = 1; i < parts.length; i++) {
                    cadnumList.add(parts[i]);
                }
                message.setCode(1);
                message.setCadnumList(cadnumList);
            } else if (parts[0].equals("2")) {
                message.setCode(2);
                message.setAddress(parts[1]);
                String zones = "";
                for (int i = 2; i < parts.length; i++) {
                    zones += parts[i] + ";";
                }
                message.setZones(zones);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
                if (postgreCon != null) {
                    postgreCon.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    //+
    @Override
    public ResponseWithData pointValueSearch(String address, String zone, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        ResponseWithData response = new ResponseWithData();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_ADDR2(?, ?, ?, ?)}");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (address != null) {
                call.setString(2, address);
            } else {
                call.setNull(2, OracleTypes.VARCHAR);
            }
            if (zone != null) {
                call.setString(3, zone + ";");
            } else {
                call.setNull(3, OracleTypes.VARCHAR);
            }
            if (date != null && !date.equals("")) {
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(4, sqlDate);
            } else {
                Date currentDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(currentDate.getTime());
                call.setDate(4, sqlDate);
            }
            call.setBoolean(5, isNb);
            long timeout = System.currentTimeMillis();
            call.execute();
            timeout = System.currentTimeMillis() - timeout;
            System.out.print(timeout);

            rs = (ResultSet) call.getObject(1);
            String fStr = "";
            String remark = "";
            int count = 0;

            while (rs.next()) {
                if (rs.getString(1).equals("-1")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельные участки не найдены");
                    return response;
                } else if (rs.getString(1).equals("-2")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельный участок находится в садоводческом товариществе. Осуществите соответствующий поиск");
                    return response;
                } else if (rs.getString(1).equals("-3")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("У данного участка нет геометрии");
                    return response;
                } else if (rs.getString(1).equals("-4")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Не найдены оценочные зоны");
                } else if (rs.getString(1).equals("1")) {
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                }
                if (rs.getString(2) != null) {
                    String commonData = rs.getString(2);
                    String[] parts = commonData.split(";");
                    if (parts != null && parts.length != 0) {
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setAddress(parts[0]);
                        }
                        if (parts[1] != null && !parts[1].equals("")) {
                            message.setSearchDate(parts[1]);
                        }
                    }
                }
                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                if (rs.getDate(4) != null) {
                    data.setDateVal(rs.getDate(4));
                }
                if (rs.getString(5) != null && !rs.getString(5).equals("")) {
                    data.setZoneNum(rs.getString(5));
                }
                if ((Double) rs.getDouble(6) != null) {
                    data.setCostD(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null && isNb) {
                    data.setCostNB(rs.getDouble(7));
                }
                if ((Double) rs.getDouble(8) != null) {
                    data.setCostR(rs.getDouble(8));
                }
                if (rs.getDate(9) != null) {
                    foundation.setDateRegStart(rs.getDate(9));
                }

                if ((Long) rs.getLong(10) != null) {
                    foundation.setRegNum(rs.getLong(10));
                }
                if (rs.getString(11) != null && !rs.getString(11).equals("")) {
                    foundation.setOrganizationPrintName(rs.getString(11));
                }
                if (rs.getDate(12) != null) {
                    foundation.setDecDate(rs.getDate(12));
                }
                if (rs.getString(13) != null && !rs.getString(13).equals("")) {
                    foundation.setDecNum(rs.getString(13));
                }

                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                if (!fStr.equals(foundation.toString())) {
                    count++;
                    fStr = foundation.toString();
                    remark += "<" + count + ">" + data.getFoundationStr() + "\n";
                }
                if( rs.getString(3) != null && !rs.getString(3).equals("")) {
                    data.setFuncCodeName(rs.getString(3) + "<" + count + ">");
                }
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }
            message.setRemark(remark);
            response.setData(message);

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("Ошибка при получении данных с базы");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    //+
    @Override
    public CadastralValueMessage communityGardenNameSearch(Long zoneId, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_ST(?, ?, ?)}");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (zoneId != null) {
                call.setLong(2, zoneId);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            call.setBoolean(4, isNb);

            call.execute();
            rs = (ResultSet) call.getObject(1);

            while (rs.next()) {
                String commonData = rs.getString(1);
                String[] parts = commonData.split(";");
                message.setAddress(parts[0]);
                message.setCommunityGardenName(parts[1]);
                message.setNearLocality(parts[2]);
                message.setSearchDate(parts[3]);

                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                data.setFuncCodeName(rs.getString(2));
                data.setDateVal(rs.getDate(3));
                data.setZoneNum(rs.getString(4));
                if ((Double) rs.getDouble(5) != null) {
                    data.setCostD(rs.getDouble(5));
                }
                if ((Double) rs.getDouble(6) != null && isNb) {
                    data.setCostNB(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null) {
                    data.setCostR(rs.getDouble(7));
                }

                foundation.setDateRegStart(rs.getDate(8));
                foundation.setRegNum(rs.getLong(9));
                foundation.setOrganizationPrintName(rs.getString(10));
                foundation.setDecDate(rs.getDate(11));
                foundation.setDecNum(rs.getString(12));
                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                message.setRemark(foundation.toString());
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    //+
    @Override
    public CadastralValueMessage landsOutLocalitySearch(Long zoneId, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();

        try {
            connection = UtilConnection.getPooledConnection();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_VNP(?, ?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (zoneId != null) {
                call.setLong(2, zoneId);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            call.setBoolean(4, isNb);

            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                String commonData = rs.getString(1);
                String[] parts = commonData.split(";");
                message.setAddress(parts[0]);
                message.setZoneName(parts[1]);
                message.setSearchDate(parts[2]);

                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                data.setFuncCodeName(rs.getString(2));
                data.setDateVal(rs.getDate(3));
                data.setZoneNum(rs.getString(4));
                if ((Double) rs.getDouble(5) != null) {
                    data.setCostD(rs.getDouble(5));
                }
                if ((Double) rs.getDouble(6) != null && isNb) {
                    data.setCostNB(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null) {
                    data.setCostR(rs.getDouble(7));
                }

                foundation.setDateRegStart(rs.getDate(8));
                foundation.setRegNum(rs.getLong(9));
                foundation.setOrganizationPrintName(rs.getString(10));
                foundation.setDecDate(rs.getDate(11));
                foundation.setDecNum(rs.getString(12));
                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                message.setRemark(foundation.toString());
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    //+
    @Override
    public CadastralValueMessage farmLandsUNPSearch(Long objectnumber, Long unp, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();

        try {
            connection = UtilConnection.getPooledConnection();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_CX(?, ?, ?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                call.setLong(2, objectnumber);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            if (unp != null) {
                call.setLong(4, unp);
            } else {
                call.setNull(4, OracleTypes.NUMBER);
            }
            call.setBoolean(5, isNb);

            call.execute();
            rs = (ResultSet) call.getObject(1);
            String fStr = "";
            String remark = "";
            int count = 0;

            while (rs.next()) {
                String commonData = rs.getString(1);
                String[] parts = commonData.split(";");
                message.setAddress(parts[0]);
                message.setZoneName(parts[1]);
                message.setUnp(Long.parseLong(parts[2]));
                message.setSearchDate(parts[3]);

                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                data.setDateVal(rs.getDate(3));
                data.setZoneNum(rs.getString(4));
                if ((Double) rs.getDouble(5) != null) {
                    data.setCostD(rs.getDouble(5));
                }
                if ((Double) rs.getDouble(6) != null && isNb) {
                    data.setCostNB(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null) {
                    data.setCostR(rs.getDouble(7));
                }
                foundation.setDateRegStart(rs.getDate(8));
                foundation.setRegNum(rs.getLong(9));
                foundation.setOrganizationPrintName(rs.getString(10));
                foundation.setDecDate(rs.getDate(11));
                foundation.setDecNum(rs.getString(12));
                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                if (!fStr.equals(foundation.toString())) {
                    count++;
                    fStr = foundation.toString();
                    remark += "<" + count + ">" + data.getFoundationStr() + "\n";
                }
                data.setFuncCodeName(rs.getString(2) + "<" + count + ">");
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }
            message.setRemark(remark);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    //+
    @Override
    public CadastralValueMessage farmLandsNameSearch(Long objectnumber, String tenantName, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();

        try {
            connection = UtilConnection.getPooledConnection();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_CX(?, ?, ?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (tenantName != null && !tenantName.equals("")) {
                call.setString(2, tenantName);
            } else {
                call.setNull(2, OracleTypes.VARCHAR);
            }
            if (objectnumber != null) {
                call.setLong(3, objectnumber);
            } else {
                call.setNull(3, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(4, sqlDate);
            } else {
                call.setNull(4, OracleTypes.DATE);
            }
            call.setBoolean(5, isNb);

            call.execute();
            rs = (ResultSet) call.getObject(1);
            String fStr = "";
            String remark = "";
            int count = 0;

            while (rs.next()) {
                String commonData = rs.getString(1);
                String[] parts = commonData.split(";");
                message.setAddress(parts[0]);
                message.setZoneName(parts[1]);
                message.setUnp(Long.parseLong(parts[2]));
                message.setSearchDate(parts[3]);

                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                data.setDateVal(rs.getDate(3));
                data.setZoneNum(rs.getString(4));
                if ((Double) rs.getDouble(5) != null) {
                    data.setCostD(rs.getDouble(5));
                }
                if ((Double) rs.getDouble(6) != null && isNb) {
                    data.setCostNB(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null) {
                    data.setCostR(rs.getDouble(7));
                }
                foundation.setDateRegStart(rs.getDate(8));
                foundation.setRegNum(rs.getLong(9));
                foundation.setOrganizationPrintName(rs.getString(10));
                foundation.setDecDate(rs.getDate(11));
                foundation.setDecNum(rs.getString(12));
                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                if (!fStr.equals(foundation.toString())) {
                    count++;
                    fStr = foundation.toString();
                    remark += "<" + count + ">" + data.getFoundationStr() + "\n";
                }
                data.setFuncCodeName(rs.getString(2) + "<" + count + ">");
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }

            message.setRemark(remark);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    //+
    @Override
    public CadastralValueMessage farmLandsZonenNumberSearch(String zoneId, Boolean isNb, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();

        try {
            connection = UtilConnection.getPooledConnection();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.SEARCH_CX(?, ?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (zoneId != null) {
                call.setString(2, zoneId);
            } else {
                call.setNull(3, OracleTypes.VARCHAR);
            }
            if (date != null && !date.equals("")) {
                formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            call.setBoolean(4, isNb);

            call.execute();
            rs = (ResultSet) call.getObject(1);
            String fStr = "";
            String remark = "";
            int count = 0;

            while (rs.next()) {
                String commonData = rs.getString(1);
                String[] parts = commonData.split(";");
                message.setAddress(parts[0]);
                message.setZoneName(parts[1]);
                message.setUnp(Long.parseLong(parts[2]));
                message.setSearchDate(parts[3]);

                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                data.setDateVal(rs.getDate(3));
                data.setZoneNum(rs.getString(4));
                if ((Double) rs.getDouble(5) != null) {
                    data.setCostD(rs.getDouble(5));
                }
                if ((Double) rs.getDouble(6) != null && isNb) {
                    data.setCostNB(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null) {
                    data.setCostR(rs.getDouble(7));
                }
                foundation.setDateRegStart(rs.getDate(8));
                foundation.setRegNum(rs.getLong(9));
                foundation.setOrganizationPrintName(rs.getString(10));
                foundation.setDecDate(rs.getDate(11));
                foundation.setDecNum(rs.getString(12));
                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                if (!fStr.equals(foundation.toString())) {
                    count++;
                    fStr = foundation.toString();
                    remark += "<" + count + ">" + data.getFoundationStr() + "\n";
                }
                data.setFuncCodeName(rs.getString(2) + "<" + count + ">");
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);

            }
            message.setRemark(remark);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    //+
    @Override
    public List<String[]> getCommunityGardenList(Long objectnumber, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<String[]> communityGardenList = new ArrayList();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_LISTST(?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                call.setLong(2, objectnumber);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }

            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                String[] data = new String[2];
                data[0] = rs.getString(1);
                data[1] = rs.getString(2);
                communityGardenList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return communityGardenList;
    }

    //+
    @Override
    public List<String> getTenantNameList(Long objectnumber, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<String> tenantNameList = new ArrayList();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_LISTCX_NAME(?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);

            if (objectnumber != null) {
                call.setLong(2, objectnumber);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }

            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                tenantNameList.add(rs.getString(1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tenantNameList;
    }

    //+
    @Override
    public List<Object[]> getZoneNumberList(Integer funcode, Long objectnumber, String date, String zoneName) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<Object[]> zoneNumberList = new ArrayList();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_LISTCC_VNP(?, ?, ?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                call.setLong(2, objectnumber);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            if (funcode != null) {
                call.setInt(4, funcode);
            } else {
                call.setNull(4, OracleTypes.NUMBER);
            }
            if (zoneName != null && !zoneName.equals("")) {
                call.setString(5, zoneName);
            } else {
                call.setNull(5, OracleTypes.VARCHAR);
            }

            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                Object[] data = new Object[2];
                data[0] = rs.getLong(1);
                data[1] = rs.getString(2);
                zoneNumberList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return zoneNumberList;
    }

    //+
    @Override
    public List<String[]> getFarmZoneNumberList(Long objectnumber, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<String[]> zoneNumberList = new ArrayList();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_LISTCX_NUMBER(?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                call.setLong(2, objectnumber);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }

            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                String[] data = new String[2];
                data[0] = rs.getString(1);
                data[1] = rs.getString(2);
                zoneNumberList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return zoneNumberList;
    }

    //+
    @Override
    public List getAddressElementList(Integer parentElementId, Long objectnumber) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<Object[]> addressElementList = new ArrayList();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_LIST_TYPEADDR(?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setInt(2, parentElementId);
            call.setLong(3, objectnumber);
            call.execute();

            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                Object[] data = new Object[2];
                data[0] = rs.getLong(1);
                data[1] = rs.getString(2);
                addressElementList.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return addressElementList;
    }

    //+
    @Override
    public List<String> getLandOutLocalityList(Integer funcode, Long objectnumber, String date) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<String> landOutLocalityList = new ArrayList();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_LISTVNP(?, ?, ?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);

            if (objectnumber != null) {
                call.setLong(2, objectnumber);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date loadDate = null;
                try {
                    loadDate = formatter.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(loadDate.getTime());
                call.setDate(3, sqlDate);
            } else {
                call.setNull(3, OracleTypes.DATE);
            }
            if (funcode != null) {
                call.setInt(4, funcode);
            } else {
                call.setNull(4, OracleTypes.NUMBER);
            }

            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                landOutLocalityList.add(rs.getString(1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return landOutLocalityList;
    }

    //+
    @Override
    public List<String> getPeriodList() {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List<String> periodList = new ArrayList<String>();
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.RETURN_PERIOD()}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.execute();
            rs = (ResultSet) call.getObject(1);
            while (rs.next()) {
                periodList.add(rs.getString(1).split(" ")[0]);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return periodList;
    }

    //+
    @Override
    public List<FunctionalPurposeDic> getFuncCode() {
        Session session = null;
        List<FunctionalPurposeDic> result = null;
        try {
            session = sessionFactory.getCurrentSession();
            Query q = session.createQuery("Select funcDic.analyticCode, funcDic.codeName From FunctionalPurposeDic funcDic where funcDic.status = :relevant " +
                    "and funcDic.analyticCode in (:livingApartmentZone, :livingManorZone, :recreationZone, " +
                    ":businessZone, :productionZone)");
            q.setInteger("livingApartmentZone", LIVING_APARTMENT_ZONE);
            q.setInteger("livingManorZone", LIVING_MANOR_ZONE);
            q.setInteger("recreationZone", RECREATION_ZONE);
            q.setInteger("productionZone", PRODUCTION_ZONE);
            q.setInteger("businessZone", BUSINESS_ZONE);
            q.setBoolean("relevant", RELEVANT);

            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    //+
    @Override
    public List getListAddress(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street, Integer typeIn, Integer typeOut) {
        CallableStatement call = null;
        Connection connection = null;
        ResultSet rs = null;

        List addressList = new ArrayList();

        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_LIST_ADDR (? , ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

            call.registerOutParameter(1, OracleTypes.CURSOR);
            if (region != null) {
                call.setLong(2, region);
            } else {
                call.setNull(2, OracleTypes.NUMBER);
            }
            if (area != null) {
                call.setLong(3, area);
            } else {
                call.setNull(3, OracleTypes.NUMBER);
            }
            if (ss != null) {
                call.setLong(4, ss);
            } else {
                call.setNull(4, OracleTypes.NUMBER);
            }
            if (snp != null) {
                call.setLong(5, snp);
            } else {
                call.setNull(5, OracleTypes.NUMBER);
            }
            if (bjd != null) {
                call.setLong(6, bjd);
            } else {
                call.setNull(6, OracleTypes.NUMBER);
            }
            if (road != null) {
                call.setLong(7, road);
            } else {
                call.setNull(7, OracleTypes.NUMBER);
            }
            if (km != null) {
                call.setLong(8, km);
            } else {
                call.setNull(8, OracleTypes.NUMBER);
            }
            if (street != null) {
                call.setLong(9, street);
            } else {
                call.setNull(9, OracleTypes.NUMBER);
            }
            if (typeIn != null) {
                call.setInt(10, typeIn);
            } else {
                call.setInt(10, OracleTypes.NUMBER);
            }
            if (typeOut != null) {
                call.setInt(11, typeOut);
            } else {
                call.setInt(11, OracleTypes.NUMBER);
            }

            call.execute();
            rs = (ResultSet) call.getObject(1);
            if (rs != null) {
                while (rs.next()) {
                    Object[] data = new Object[2];
                    data[0] = rs.getLong(1);
                    data[1] = rs.getString(2);
                    addressList.add(data);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return addressList;
    }

    @Override
    public Long savePdfPath(String url) {
        CallableStatement call = null;
        Connection connection = null;
        Long id = null;
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{? = call " + SCHEMA_NAME + ".PKG_SEARCH.GET_PDF_ID(?)}");
            call.registerOutParameter(1, OracleTypes.NUMBER);
            call.setString(2, url);
            call.execute();
            id = call.getLong(1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }

    @Override
    public String searchPathById(Long id) {
        Session session = null;
        String path = "";
        try {
            session = sessionFactory.getCurrentSession();
            Query q = session.createQuery("Select request.path From PdfRequest request where request.id = :id ");
            q.setLong("id", id);
            path = (String) q.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    @Override
    public void deletePdfById(Long id) {
        CallableStatement call = null;
        Connection connection = null;
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{= call " + SCHEMA_NAME + ".PKG_SEARCH.DEL_PDF(?)}");
            call.setLong(1, id);
            call.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
