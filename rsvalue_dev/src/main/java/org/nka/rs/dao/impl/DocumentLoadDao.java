package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.Session;
import org.nka.rs.dao.IDocumentLoadDao;
import org.nka.rs.entity.dto.DocInfo;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.stereotype.Repository;

import java.sql.*;

import static org.nka.rs.entity.constant.commonConstant.IConstant.SCHEMA_NAME;

/**
 * Created by zgurskaya on 21.06.2016.
 */

@Repository
public class DocumentLoadDao extends BaseDaoSessionImpl implements IDocumentLoadDao {


    @Override
    public Long saveDoc(DocInfo docInfo, String path, Long userId, Long myid) {
        Session session = null;
        Connection connection = null;
        CallableStatement call = null;
        ResultSet res = null;
        Long regDocId = null;
        try {
            connection = UtilConnection.getPooledConnection();

            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_LOAD.INS_DOC(?,?,?,?,?)}");

            call.registerOutParameter(1, OracleTypes.NUMBER);
            if (docInfo != null && docInfo.getDocNum() != null && !docInfo.getDocNum().equals("")) {
                call.setString(2, docInfo.getDocNum());
            } else {
                call.setNull(2, OracleTypes.VARCHAR);
            }
            call.setString(3, path);
            if(docInfo != null && docInfo.getDocTypeId() != null){
                call.setInt(4, docInfo.getDocTypeId());
            } else {
                call.setNull(4, OracleTypes.NUMBER);
            }
            call.setLong(5, userId);
            if(myid != null){
                call.setLong(6, myid);
            } else {
                call.setNull(6, OracleTypes.NUMBER);
            }
            call.execute();

            regDocId = call.getLong(1);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
        return regDocId;
    }

    @Override
    public String deleteDoc(Long docId) {
        Connection connection = null;
        CallableStatement call = null;
        String path = "";
        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_LOAD.DEL_DOC_LOAD(?)}");

            call.registerOutParameter(1, OracleTypes.VARCHAR);
            call.setLong(2, docId);
            call.execute();

            path = (String) call.getObject(1);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
        return path;
    }

    @Override
    public Long getCommonDocId() {
        Session session = null;
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet res = null;
        Long myId = null;
        try {
            connection = UtilConnection.getPooledConnection();

            String sqlIdentifier = "select " + SCHEMA_NAME+ ".SEQ_DOWNLOAD_ID.NEXTVAL from dual ";
            pst = connection.prepareStatement(sqlIdentifier);

            synchronized (this) {
                res = pst.executeQuery();
                if (res.next()) {
                    myId = res.getLong(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
            try {
                if (pst != null) {
                    pst.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
        return myId;

    }

}
