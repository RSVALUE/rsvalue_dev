package org.nka.rs.dao;

import org.nka.rs.entity.dictionary.LayerTypeDic;
import org.nka.rs.entity.dto.LayerType;

import java.util.List;

/**
 * Created by zgurskaya on 09.06.2016.
 */
public interface ILayerTypeDao extends IBaseDao {

    List<LayerType> getValideLayers(List<String> layerNames);
    List<Integer> getGroundCodeValue();
    List<Integer> getDistanceCodeValue();
    List<LayerTypeDic> getActualLayers();
}
