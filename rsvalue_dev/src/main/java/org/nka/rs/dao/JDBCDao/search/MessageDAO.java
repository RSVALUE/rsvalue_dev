package org.nka.rs.dao.JDBCDao.search;

import oracle.jdbc.OracleTypes;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dto.emessage.AddressMessage;
import org.nka.rs.entity.dto.emessage.CadastralValueData;
import org.nka.rs.entity.dto.emessage.CadastralValueMessage;
import org.nka.rs.entity.dto.emessage.Foundation;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.util.connection.UtilConnection;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * MessageDAO переписанное без использования Hibernate
 */
public class MessageDAO {

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    public AddressMessage addressSearch(Long objectnumber, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_ADDR1(?, ?, ?)}", IConstant.SCHEMA_NAME);
        String[] parts = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.VARCHAR);
            if (objectnumber != null) {
                callableStatement.setLong(2, objectnumber);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            callableStatement.setBoolean(4, isNb);
            callableStatement.execute();
            String result = callableStatement.getString(1);
            parts = result.split(";");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return parseData(parts);
    }

    public ResponseWithData cadastrValueSearch(Long soato, Integer blockNum, Integer parcelNum, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_CADNUM(?, ?, ?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        ResponseWithData response = new ResponseWithData();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (soato != null) {
                callableStatement.setLong(2, soato);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (blockNum != null) {
                callableStatement.setInt(3, blockNum);
            } else {
                callableStatement.setNull(3, OracleTypes.NUMBER);
            }
            if (parcelNum != null) {
                callableStatement.setInt(4, parcelNum);
            } else {
                callableStatement.setNull(4, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(5, new java.sql.Date(formatter.parse(date).getTime()));
            } else
                callableStatement.setNull(5, OracleTypes.DATE);
            callableStatement.setBoolean(6, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            response = initResponse(rs, response, message, valuesCharacters, isNb);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("ERROR: Ошибка получения данных из базы");
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return response;
    }

    public ResponseWithData cadastrValueSearch(String cadNum, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_CADNUM(?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        ResponseWithData response = new ResponseWithData();

        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (cadNum != null) {
                callableStatement.setString(2, cadNum);
            } else {
                callableStatement.setNull(2, OracleTypes.VARCHAR);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            callableStatement.setBoolean(4, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            response = initResponse(rs, response, message, valuesCharacters, isNb);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return response;
    }

    public ResponseWithData pointValueSearch(String address, String zone, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_ADDR2(?, ?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;


        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        ResponseWithData response = new ResponseWithData();

        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (address != null) {
                callableStatement.setString(2, address);
            } else {
                callableStatement.setNull(2, OracleTypes.VARCHAR);
            }
            if (zone != null) {
                callableStatement.setString(3, zone + ";");
            } else {
                callableStatement.setNull(3, OracleTypes.VARCHAR);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(4, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setDate(4, new java.sql.Date(new Date().getTime()));
            }
            callableStatement.setBoolean(5, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);

            String fStr = "";
            String remark = "";
            int count = 0;

            while (rs.next()) {
                if (rs.getString(1).equals("-1")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельные участки не найдены");
                    return response;
                } else if (rs.getString(1).equals("-2")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Земельный участок находится в садоводческом товариществе. Осуществите соответствующий поиск");
                    return response;
                } else if (rs.getString(1).equals("-3")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("У данного участка нет геометрии");
                    return response;
                } else if (rs.getString(1).equals("-4")) {
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Не найдены оценочные зоны");
                } else if (rs.getString(1).equals("1")) {
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    response.setMessage("OK: Данные получены");
                }
                if (rs.getString(2) != null) {
                    String commonData = rs.getString(2);
                    String[] parts = commonData.split(";");
                    if (parts != null && parts.length != 0) {
                        if (parts[0] != null && !parts[0].equals("")) {
                            message.setAddress(parts[0]);
                        }
                        if (parts[1] != null && !parts[1].equals("")) {
                            message.setSearchDate(parts[1]);
                        }
                    }
                }
                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                if (rs.getDate(4) != null) {
                    data.setDateVal(rs.getDate(4));
                }
                if (rs.getString(5) != null && !rs.getString(5).equals("")) {
                    data.setZoneNum(rs.getString(5));
                }
                if ((Double) rs.getDouble(6) != null) {
                    data.setCostD(rs.getDouble(6));
                }
                if ((Double) rs.getDouble(7) != null && isNb) {
                    data.setCostNB(rs.getDouble(7));
                }
                if ((Double) rs.getDouble(8) != null) {
                    data.setCostR(rs.getDouble(8));
                }
                if (rs.getDate(9) != null) {
                    foundation.setDateRegStart(rs.getDate(9));
                }

                if ((Long) rs.getLong(10) != null) {
                    foundation.setRegNum(rs.getLong(10));
                }
                if (rs.getString(11) != null && !rs.getString(11).equals("")) {
                    foundation.setOrganizationPrintName(rs.getString(11));
                }
                if (rs.getDate(12) != null) {
                    foundation.setDecDate(rs.getDate(12));
                }
                if (rs.getString(13) != null && !rs.getString(13).equals("")) {
                    foundation.setDecNum(rs.getString(13));
                }

                data.setFoundation(foundation);
                if (foundation.getDecDate() != null) {
                    data.setFoundationStr(foundation.toString());
                    if (!fStr.equals(foundation.toString())) {
                        count++;
                        fStr = foundation.toString();
                        remark += "<" + count + ">" + data.getFoundationStr() + "\n";
                    }
                    if (rs.getString(3) != null && !rs.getString(3).equals("")) {
                        data.setFuncCodeName(rs.getString(3) + "<" + count + ">");
                    }
                    valuesCharacters.add(data);
                    message.setValuesCharacters(valuesCharacters);
                }
            }
            message.setRemark(remark);
            response.setData(message);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            response.setMessage("ERROR: Ошибка получения данных из базы");
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return response;
    }

    public CadastralValueMessage communityGardenNameSearch(Long zoneId, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_ST(?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;


        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (zoneId != null) {
                callableStatement.setLong(2, zoneId);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            callableStatement.setBoolean(4, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                String commonData = rs.getString(1);
                String[] parts = commonData.split(";");
                message.setAddress(parts[0]);
                message.setCommunityGardenName(parts[1]);
                message.setNearLocality(parts[2]);
                message.setSearchDate(parts[3]);

                CadastralValueData data = new CadastralValueData();
                Foundation foundation = new Foundation();

                data.setFuncCodeName(rs.getString(2));
                data.setDateVal(rs.getDate(3));
                data.setZoneNum(rs.getString(4));
                data.setCostD(rs.getDouble(5));
                if (isNb) {
                    data.setCostNB(rs.getDouble(6));
                }
                data.setCostR(rs.getDouble(7));

                foundation.setDateRegStart(rs.getDate(8));
                foundation.setRegNum(rs.getLong(9));
                foundation.setOrganizationPrintName(rs.getString(10));
                foundation.setDecDate(rs.getDate(11));
                foundation.setDecNum(rs.getString(12));
                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                message.setRemark(foundation.toString());
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return message;
    }

    public CadastralValueMessage landsOutLocalitySearch(Long zoneId, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_VNP(?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (zoneId != null) {
                callableStatement.setLong(2, zoneId);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            callableStatement.setBoolean(4, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                String commonData = rs.getString(1);
                String[] parts = commonData.split(";");
                message.setAddress(parts[0]);
                message.setZoneName(parts[1]);
                message.setSearchDate(parts[2]);

                CadastralValueData data = new CadastralValueData();
                data.setFuncCodeName(rs.getString(2));
                data.setDateVal(rs.getDate(3));
                data.setZoneNum(rs.getString(4));
                data.setCostD(rs.getDouble(5));
                if (isNb) {
                    data.setCostNB(rs.getDouble(6));
                }
                data.setCostR(rs.getDouble(7));

                Foundation foundation = new Foundation();
                foundation.setDateRegStart(rs.getDate(8));
                foundation.setRegNum(rs.getLong(9));
                foundation.setOrganizationPrintName(rs.getString(10));
                foundation.setDecDate(rs.getDate(11));
                foundation.setDecNum(rs.getString(12));
                data.setFoundation(foundation);
                data.setFoundationStr(foundation.toString());
                message.setRemark(foundation.toString());
                valuesCharacters.add(data);
                message.setValuesCharacters(valuesCharacters);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return message;
    }

    public CadastralValueMessage farmLandsUNPSearch(Long objectnumber, Long unp, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_CX(?, ?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                callableStatement.setLong(2, objectnumber);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            if (unp != null) {
                callableStatement.setLong(4, unp);
            } else {
                callableStatement.setNull(4, OracleTypes.NUMBER);
            }
            callableStatement.setBoolean(5, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            initMessage(rs, message, valuesCharacters, isNb);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return message;
    }

    public CadastralValueMessage farmLandsNameSearch(Long objectnumber, String tenantName, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_CX(?, ?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (tenantName != null && !tenantName.equals("")) {
                callableStatement.setString(2, tenantName);
            } else {
                callableStatement.setNull(2, OracleTypes.VARCHAR);
            }
            if (objectnumber != null) {
                callableStatement.setLong(3, objectnumber);
            } else {
                callableStatement.setNull(3, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(4, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(4, OracleTypes.DATE);
            }
            callableStatement.setBoolean(5, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            initMessage(rs, message, valuesCharacters, isNb);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return message;
    }

    public CadastralValueMessage farmLandsZonenNumberSearch(String zoneId, Boolean isNb, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.SEARCH_CX(?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        CadastralValueMessage message = new CadastralValueMessage();
        List<CadastralValueData> valuesCharacters = new ArrayList<CadastralValueData>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (zoneId != null) {
                callableStatement.setString(2, zoneId);
            } else {
                callableStatement.setNull(2, OracleTypes.VARCHAR);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            callableStatement.setBoolean(4, isNb);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            initMessage(rs, message, valuesCharacters, isNb);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return message;
    }

    public List<String[]> getCommunityGardenList(Long objectnumber, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.GET_LISTST(?, ?)}", IConstant.SCHEMA_NAME);
        return getListFromQuery(sqlString, objectnumber, date);
    }

    public List<String> getTenantNameList(Long objectnumber, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.GET_LISTCX_NAME(?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<String> tenantNameList = new ArrayList<String>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                callableStatement.setLong(2, objectnumber);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                tenantNameList.add(rs.getString(1));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return tenantNameList;
    }

    public List<Object[]> getZoneNumberList(Integer funcode, Long objectnumber, String date, String zoneName) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.GET_LISTCC_VNP(?, ?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<Object[]> zoneNumberList = new ArrayList<Object[]>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                callableStatement.setLong(2, objectnumber);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            if (funcode != null) {
                callableStatement.setInt(4, funcode);
            } else {
                callableStatement.setNull(4, OracleTypes.NUMBER);
            }
            if (zoneName != null && !zoneName.equals("")) {
                callableStatement.setString(5, zoneName);
            } else {
                callableStatement.setNull(5, OracleTypes.VARCHAR);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            initObjectList(rs, zoneNumberList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return zoneNumberList;
    }

    public List<String[]> getFarmZoneNumberList(Long objectnumber, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.GET_LISTCX_NUMBER(?, ?)}", IConstant.SCHEMA_NAME);
        return getListFromQuery(sqlString, objectnumber, date);
    }

    private List<String[]> getListFromQuery(String sqlString, Long objectnumber, String date) {
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<String[]> zoneNumberList = new ArrayList<String[]>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                callableStatement.setLong(2, objectnumber);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                String[] data = new String[2];
                data[0] = rs.getString(1);
                data[1] = rs.getString(2);
                zoneNumberList.add(data);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return zoneNumberList;
    }

    public List<Object[]> getAddressElementList(Integer parentElementId, Long objectnumber) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.GET_LIST_TYPEADDR(?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<Object[]> addressElementList = new ArrayList<Object[]>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setInt(2, parentElementId);
            if (objectnumber != null) {
                callableStatement.setLong(3, objectnumber);
            } else {
                callableStatement.setNull(3, OracleTypes.NULL);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            initObjectList(rs, addressElementList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return addressElementList;
    }

    public List<String> getLandOutLocalityList(Integer funcode, Long objectnumber, String date) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.GET_LISTVNP(?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<String> landOutLocalityList = new ArrayList<String>();

        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (objectnumber != null) {
                callableStatement.setLong(2, objectnumber);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (date != null && !date.equals("")) {
                callableStatement.setDate(3, new java.sql.Date(formatter.parse(date).getTime()));
            } else {
                callableStatement.setNull(3, OracleTypes.DATE);
            }
            if (funcode != null) {
                callableStatement.setInt(4, funcode);
            } else {
                callableStatement.setNull(4, OracleTypes.NUMBER);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                landOutLocalityList.add(rs.getString(1));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return landOutLocalityList;
    }

    public List<String> getPeriodList() {
        String sqlString = String.format("{ ? = call %s.PKG_SEARCH.RETURN_PERIOD()}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<String> periodList = new ArrayList<String>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            if (rs.isBeforeFirst()) {
                while (rs.next()) {
                    periodList.add(rs.getString(1));
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return periodList;
    }

    public List getListAddress(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street, Integer typeIn, Integer typeOut) {
        String sqlString = String.format("{ ? = call %s.PKG_SEARCH.GET_LIST_ADDR (? , ?, ?, ?, ?, ?, ?, ?, ?, ?)}", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<Object[]> addressList = new ArrayList<Object[]>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (region != null) {
                callableStatement.setLong(2, region);
            } else {
                callableStatement.setNull(2, OracleTypes.NUMBER);
            }
            if (area != null) {
                callableStatement.setLong(3, area);
            } else {
                callableStatement.setNull(3, OracleTypes.NUMBER);
            }
            if (ss != null) {
                callableStatement.setLong(4, ss);
            } else {
                callableStatement.setNull(4, OracleTypes.NUMBER);
            }
            if (snp != null) {
                callableStatement.setLong(5, snp);
            } else {
                callableStatement.setNull(5, OracleTypes.NUMBER);
            }
            if (bjd != null) {
                callableStatement.setLong(6, bjd);
            } else {
                callableStatement.setNull(6, OracleTypes.NUMBER);
            }
            if (road != null) {
                callableStatement.setLong(7, road);
            } else {
                callableStatement.setNull(7, OracleTypes.NUMBER);
            }
            if (km != null) {
                callableStatement.setLong(8, km);
            } else {
                callableStatement.setNull(8, OracleTypes.NUMBER);
            }
            if (street != null) {
                callableStatement.setLong(9, street);
            } else {
                callableStatement.setNull(9, OracleTypes.NUMBER);
            }
            if (typeIn != null) {
                callableStatement.setInt(10, typeIn);
            } else {
                callableStatement.setNull(10, OracleTypes.NUMBER);
            }
            if (typeOut != null) {
                callableStatement.setInt(11, typeOut);
            } else {
                callableStatement.setNull(11, OracleTypes.NUMBER);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            initObjectList(rs, addressList);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return addressList;
    }

    public Long savePdfPath(String url) {
        String sqlString = String.format("{? = call %s.PKG_SEARCH.GET_PDF_ID( ? )}", IConstant.SCHEMA_NAME);
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;
        Long id = null;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
            callableStatement.setString(2, url);
            callableStatement.execute();
            id = callableStatement.getLong(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return id;
    }

    public String searchPathById(Long id) {
        String sqlString = String.format("SELECT pdf.PDF_PATH FROM %s.PDF_REQUEST pdf WHERE pdf.PDF_ID = ?", IConstant.SCHEMA_NAME);
        ResultSet rs = null;
        Connection pooledConnection = null;
        PreparedStatement preparedStatement = null;

        String path = "";

        try {
            pooledConnection = UtilConnection.getPooledConnection();
            preparedStatement = pooledConnection.prepareCall(sqlString);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                path = rs.getString(1);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return path;
    }

    public void deletePdfById(Long id) {
        String sqlString = String.format("{call %s.PKG_SEARCH.DEL_PDF(?)}", IConstant.SCHEMA_NAME);
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.setLong(1, id);
            callableStatement.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public List<FunctionalPurposeDic> getFuncCode(boolean load) {
        String sqlString = "";
        if (load) {
            sqlString = String.format("{? = call %s.PKG_LOAD.RETURN_FUNC()}", IConstant.SCHEMA_NAME);
        } else {
            sqlString = String.format("{? = call %s.PKG_SEARCH.RETURN_FUNC()}", IConstant.SCHEMA_NAME);
        }
        ResultSet rs = null;
        Connection pooledConnection = null;
        CallableStatement callableStatement = null;

        List<FunctionalPurposeDic> result = new ArrayList<FunctionalPurposeDic>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                FunctionalPurposeDic functionalPurposeDic = new FunctionalPurposeDic();
                functionalPurposeDic.setAnalyticCode(rs.getInt(1));
                functionalPurposeDic.setCodeName(rs.getString(2));
                result.add(functionalPurposeDic);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return result;
    }

    private AddressMessage parseData(String[] data) {
        AddressMessage message = new AddressMessage();
        List<String> cadnumList = new ArrayList<String>();
        if (data[0].equals("-1")) {
            message.setCode(-1);
            message.setResponse("По адресу " + data[1] + " не имеется координат. Дальнейший поиск невозможен!");
        } else if (data[0].equals("1")) {
            for (int i = 1; i < data.length; i++) {
                cadnumList.add(data[i]);
            }
            message.setCode(1);
            message.setCadnumList(cadnumList);
        } else if (data[0].equals("2")) {
            message.setCode(2);
            message.setAddress(data[1]);
            message.setCoordinates(data[2]);
            message.setZones(data[3]);
        }
        return message;
    }

    private ResponseWithData initResponse(ResultSet rs, ResponseWithData response, CadastralValueMessage message, List<CadastralValueData> valuesCharacters, Boolean isNb) throws SQLException {
        String fStr = "";
        String remark = "";
        int count = 0;

        while (rs.next()) {
            if (rs.getString(1).equals("-1")) {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Земельные участки не найдены");
            } else if (rs.getString(1).equals("-2")) {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Земельный участок находится в садоводческом товариществе. Осуществите соответствующий поиск");
            } else if (rs.getString(1).equals("-3")) {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("У данного участка нет геометрии");
            } else if (rs.getString(1).equals("-4")) {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Не найдены оценочные зоны");
            } else if (rs.getString(1).equals("1")) {
                response.setCode(ErrorCodeEnum.SUCCESS.getValue());
            }
            if (response.getCode() != 1) {
                return response;
            }
            if (rs.getString(2) != null) {
                String commonData = rs.getString(2);
                String[] parts = commonData.split(";");
                if (parts != null && parts.length != 0) {
                    if (parts[0] != null && !parts[0].equals("")) {
                        message.setAddress(parts[0]);
                    }
                    if (parts[0] != null && !parts[0].equals("")) {
                        message.setCadNum(parts[1]);
                    }
                    if (parts[0] != null && !parts[0].equals("")) {
                        message.setSquare(Double.parseDouble(parts[2]));
                    }
                    if (parts[0] != null && !parts[0].equals("")) {
                        message.setPurposeDic(parts[3]);
                    }
                    if (parts[0] != null && !parts[0].equals("")) {
                        message.setPurposeGov(parts[4]);
                    }
                    if (parts[0] != null && !parts[0].equals("")) {
                        message.setSearchDate(parts[5]);
                    }
                }
            }
            CadastralValueData data = new CadastralValueData();
            Foundation foundation = new Foundation();
            if (rs.getDate(4) != null) {
                data.setDateVal(rs.getDate(4));
            }
            if (rs.getString(5) != null && !rs.getString(5).equals("")) {
                data.setZoneNum(rs.getString(5));
            }
            if ((Double) rs.getDouble(6) != null) {
                data.setCostD(rs.getDouble(6));
            }
            if ((Double) rs.getDouble(7) != null && isNb) {
                data.setCostNB(rs.getDouble(7));
            }
            if ((Double) rs.getDouble(8) != null) {
                data.setCostR(rs.getDouble(8));
            }
            if (rs.getDate(9) != null) {
                foundation.setDateRegStart(rs.getDate(9));
            }
            if ((Long) rs.getLong(10) != null) {
                foundation.setRegNum(rs.getLong(10));
            }
            if (rs.getString(11) != null && !rs.getString(11).equals("")) {
                foundation.setOrganizationPrintName(rs.getString(11));
            }
            if (rs.getDate(12) != null) {
                foundation.setDecDate(rs.getDate(12));
            }
            if (rs.getString(13) != null && !rs.getString(13).equals("")) {
                foundation.setDecNum(rs.getString(13));
            }
            data.setFoundation(foundation);
            data.setFoundationStr(foundation.toString());
            if (!fStr.equals(foundation.toString())) {
                count++;
                fStr = foundation.toString();
                remark += "<" + count + ">" + data.getFoundationStr() + "\n";
            }
            if (rs.getString(3) != null && !rs.getString(3).equals("")) {
                data.setFuncCodeName(rs.getString(3) + "<" + count + ">");
            }
            valuesCharacters.add(data);
            message.setValuesCharacters(valuesCharacters);
        }
        message.setRemark(remark);
        response.setData(message);
        return response;
    }

    private void initMessage(ResultSet rs, CadastralValueMessage message, List<CadastralValueData> valuesCharacters, Boolean isNb) throws SQLException {
        String fStr = "";
        String remark = "";
        int count = 0;

        while (rs.next()) {
            String commonData = rs.getString(1);
            String[] parts = commonData.split(";");
            message.setAddress(parts[0]);
            message.setZoneName(parts[1]);
            message.setUnp(Long.parseLong(parts[2]));
            message.setSearchDate(parts[3]);

            CadastralValueData data = new CadastralValueData();
            data.setDateVal(rs.getDate(3));
            data.setZoneNum(rs.getString(4));
            data.setCostD(rs.getDouble(5));
            if (isNb) {
                data.setCostNB(rs.getDouble(6));
            }
            data.setCostR(rs.getDouble(7));

            Foundation foundation = new Foundation();
            foundation.setDateRegStart(rs.getDate(8));
            foundation.setRegNum(rs.getLong(9));
            foundation.setOrganizationPrintName(rs.getString(10));
            foundation.setDecDate(rs.getDate(11));
            foundation.setDecNum(rs.getString(12));
            data.setFoundation(foundation);
            data.setFoundationStr(foundation.toString());
            if (!fStr.equals(foundation.toString())) {
                count++;
                fStr = foundation.toString();
                remark += "<" + count + ">" + data.getFoundationStr() + "\n";
            }
            data.setFuncCodeName(rs.getString(2) + "<" + count + ">");
            valuesCharacters.add(data);
            message.setValuesCharacters(valuesCharacters);
        }
        message.setRemark(remark);
    }

    private void initObjectList(ResultSet rs, List<Object[]> list) throws SQLException {
        while (rs.next()) {
            Object[] data = new Object[2];
            data[0] = rs.getLong(1);
            data[1] = rs.getString(2);
            list.add(data);
        }
    }

}

