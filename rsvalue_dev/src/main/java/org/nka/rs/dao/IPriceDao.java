package org.nka.rs.dao;

import org.nka.rs.entity.dictionary.OperationTypeDic;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.dto.price.PriceData;
import org.nka.rs.entity.dto.price.SimplePrice;
import org.nka.rs.entity.pojos.price.GetPriceObj;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;

import java.util.List;

/**
 * Created by zgurskaya on 10.10.2016.
 */
public interface IPriceDao {

    List<TORDic> getTorList (Long userId);
    Object [] getPriceListWithSorting(String priceNum, Integer sortingParam, String predicate, Integer page, Integer amount, Long userId, Integer torId);
    Response stopPrice(Long priceId, String date, Long userId, Integer operTypeId);
    List<OperationTypeDic> getOperType();
    Response savePrice(PriceData price);
    ResponseWithData getPrice(Long priceId, Long userId);
    Response validatePriceNumber(Integer torId, String priceNumber);
    ResponseWithData getPricePattern(Long userId);
    GetPriceObj getPriceNew(Long userId, Long priceId);

    Response savePriceNew(SimplePrice price);
}

