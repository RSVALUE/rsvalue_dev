package org.nka.rs.dao.JDBCDao.search;

import oracle.jdbc.OracleTypes;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.pojos.load.ATE;
import org.nka.rs.util.connection.UtilConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * AddressDAO переписанное без использования Hibernate
 */
public class AddressDAO {

    public List<ATE> findAteForOtherCostType(Long parentateId) {
        String sqlString = String.format("{ ? = call %s.PKG_SEARCH.GET_ATELIST(?)}", IConstant.SCHEMA_NAME);
        CallableStatement callableStatement = null;
        Connection pooledConnection = null;
        ResultSet rs = null;

        List<ATE> result = new ArrayList<ATE>();
        try {
            pooledConnection = UtilConnection.getPooledConnection();
            callableStatement = pooledConnection.prepareCall(sqlString);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            if (parentateId != null) {
                callableStatement.setLong(2, parentateId);
            } else {
                callableStatement.setLong(2, -1L);
            }
            callableStatement.execute();
            rs = (ResultSet) callableStatement.getObject(1);
            while (rs.next()) {
                ATE ate = new ATE();
                ate.setAteId(rs.getLong(1));
                ate.setAteName(rs.getString(2));
                result.add(ate);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (callableStatement != null) {
                    callableStatement.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            try {
                if (pooledConnection != null) {
                    pooledConnection.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return result;
    }
}
