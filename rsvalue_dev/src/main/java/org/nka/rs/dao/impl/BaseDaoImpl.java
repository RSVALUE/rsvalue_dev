package org.nka.rs.dao.impl;

import org.hibernate.*;
import org.hibernate.criterion.DetachedCriteria;
import org.nka.rs.dao.IBaseDao;
import org.nka.rs.util.HibernateUtil;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Repository
public class BaseDaoImpl<E> implements IBaseDao<E> {


    public BaseDaoImpl() {
    }

    @Override
    public Serializable addElement(E el) {
        Serializable id = null;
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            id = session.save(el);
            //log.info("Save: " + el);
            tx.commit();
        } catch (Exception e) {
            //log.error("Save not success for object " + el);
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return id;
    }

    @Override
    public void updateElement(E el) {
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(el);
            //log.info("Update: " + el);
            transaction.commit();
        } catch (Exception e) {
            //log.error("Update not success for object " + el);
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void updateElementById(Class<E> clazz, Serializable id) {
        Session session = null;
        E el;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            el = (E) session.get(clazz, id);
            session.update(el);
            //log.info("Update: " + el);
            transaction.commit();
        } catch (Exception e) {
            //log.error("Update not success for object: type = " + clazz + " and id = " + id);
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void refreshElement(E el) {
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.refresh(el);
            //log.info("Update: " + el);
            transaction.commit();
        } catch (Exception e) {
            //log.error("Update not success for object " + el);
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void refreshElementById(Class<E> clazz, Serializable id) {
        Session session = null;
        E el;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            el = (E) session.get(clazz, id);
            session.refresh(el);
            //log.info("Update: " + el);
            transaction.commit();
        } catch (Exception e) {
            //log.error("Update not success for object: type = " + clazz + " and id = " + id);
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void deleteElement(E el) {
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.delete(el);
            //log.info("Delete:" + el);
            tx.commit();
        } catch (Exception e) {
            //log.error("Delete not success for object " + el);
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void deleteElementById(Class<E> clazz, Serializable id) {
        Session session = null;
        E el;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            el = (E) session.get(clazz, id);
            //log.info("Delete:" + el);
            session.delete(el);
            transaction.commit();
        } catch (Exception e) {
            //log.error("Delete not success for object: type = " + clazz + " and id = " + id);
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public List<E> getAllElements(Class<E> clazz) {
        Session session = null;
        List<E> els = new ArrayList<E>();
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            els = session.createCriteria(clazz).list();
            //log.info("Get all elements");
            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return els;
    }

    @Override
    public E getElementByID(Class<E> clazz, Serializable id) {
        Session session = null;
        E el = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            el = (E) session.get(clazz, id);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return el;
    }

    @Override
    public List<E> getCriterion(DetachedCriteria crio) {
        Session session = null;
        Criteria cria = null;
        List<E> result = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            cria = crio.getExecutableCriteria(session);
            result = cria.list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return result;
    }

    @Override
    public E getUniqueResult(DetachedCriteria crio) {
        Session session = null;
        Criteria cria = null;
        E obj = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            cria = crio.getExecutableCriteria(session);
            obj = (E)cria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return obj;
    }
}
