package org.nka.rs.dao;

import org.nka.rs.entity.dictionary.DecisionOrganizationDic;

import java.util.List;

/**
 * Created by zgurskaya on 29.09.2016.
 */
public interface IDecOrgDao extends IBaseDao {

    List<DecisionOrganizationDic> getActualOrganization();
}
