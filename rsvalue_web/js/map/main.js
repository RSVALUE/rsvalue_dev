/**
 * Created by djinn on 05.10.2016.
 */
$(document).ready(function () {
    renderMap(1);
    //renderMap(2);

    $("#order_process_order").click(function(){
        setTimeout(function () {
            renderMap(2);
        },1000);

    });


});

var layersJson;
var blockL='.left';
var blockR='.right';
var zoomEl = '.ol-zoom.ol-unselectable.ol-control';
$(document).ready(function(){
    if ($('.right-block').css("display") !== 'none') {
        $('.btn-slide-right-block').empty().append('>');
    }
    /*if ($('.left-block').css("display") !== 'none') {
        $('.btn-slide-right-block').empty().append('<');
    }*/

    $('.btn-slide-left-block').css('cursor', 'pointer');
    $('.btn-slide-left-block').click(function(){
        $('.btn-slide-left-block').empty().append('>');
        var hidden = $('.left-block');
        var display = hidden.css( "display" );
        if(display !== 'none'){
            hidden.hide('slide', {direction: 'left'}, 400);

            $(blockL).animate({
                width: "25px",
                /*opacity: 0.4,
                marginLeft: "0.6in",
                fontSize: "3em",
                borderWidth: "10px"*/
            }, 400 );
            $(zoomEl).animate({
                marginLeft: "25px",
                /*opacity: 0.4,
                 marginLeft: "0.6in",
                 fontSize: "3em",
                 borderWidth: "10px"*/
            }, 400 );
        } else {
            showBlock(blockL,zoomEl);
        }

    });

    $('.btn-slide-right-block').css('cursor', 'pointer');
    $('.btn-slide-right-block').click(function(){
        $('.btn-slide-right-block').empty().append('<');
        var hidden = $('.right-block');
        var display = hidden.css( "display" );
        if(display !== 'none'){
            hidden.hide('slide', {direction: 'right'}, 400);
            $(blockR).animate({
                width: "25px",
                /*opacity: 0.4,
                 marginLeft: "0.6in",
                 fontSize: "3em",
                 borderWidth: "10px"*/
            }, 400 );
        } else {
            showBlock(blockR);
        }

    });
/*    $('#show').click(function(){
        var hidden = $('.left-block');

    });*/
});

function showBlock(block,zoomEl) {
    if (block == blockL) {
        $('.btn-slide-left-block').empty().append('<');
        var hiddenB = $(block +'-block');
        var hiddenZEl = $(zoomEl +'-block');
        hiddenB.show('slide', {direction: 'left'}, 400);
        hiddenZEl.show('slide', {direction: 'left'}, 400);

        $(block).animate({
            width: "300px",
            /*opacity: 0.4,
             marginLeft: "0.6in",
             fontSize: "3em",
             borderWidth: "10px"*/
        }, 400 );
        $(zoomEl).animate({
            marginLeft: "300px",
            /*opacity: 0.4,
             marginLeft: "0.6in",
             fontSize: "3em",
             borderWidth: "10px"*/
        }, 400 );
    } else if (block == blockR) {
        $('.btn-slide-right-block').empty().append('>');

        var hiddenB = $(block +'-block');
        hiddenB.show('slide', {direction: 'right'}, 400);
        $(block).animate({
            width: "300px",
             //opacity: 0.4,
             //marginLeft: "0.6in",
             //fontSize: "3em",
             //borderWidth: "10px"
        }, 400 );
    }

}

/** разбор json файла со слоями для построения дерева*/
var setBlockLayersStatus = false;

function renderMap(numMap) {
    var dataObj = getDataJSON();
    //console.log(dataObj);
    if (setBlockLayersStatus == false) {
        console.log(setBlockLayersStatus);
        setBlockLayers(dataObj);
    }
    //console.log(dataObj);
    switch (numMap) {
        case 1:
            initMap(dataObj);
            break;
        case 2:
            initMap2(dataObj);
            break;
        default:
            initMap(dataObj);
            break;
    }
}

function getDataJSON() {
    var dataJSON = $.ajax({
        url: 'data/layers.json',
        type: 'POST',
        dataType: 'json',
        async: false,
        //data: { a:'b' },
        success: function(data) {
            //console.log(data);
            //initMap(data);
        }
    }).responseJSON;
    //console.log(dataJSON);
    return dataJSON;
}
function setBlockLayers(data) {
    //console.log(data);
    var layers = [];
    var CountL = 1;
    //console.log(data.layers);
    layersJson = data.layers;
    var j = 0;
    $.each(data.layers, function (key, val) {
        //console.log(key, val);
        var check_type = val.check_type;

        switch (check_type) {
            case 'checkbox':
                layers.push("<li><span>" + val.title + "</span><ul class='layersGroup " + val.group + "'>" + layersInGroupC(val, check_type) + "</ul></li>");

            function layersInGroupC(val, check_type) {
                var list = '';
                $.each(val.layers, function (key, val) {
                    CountLL = CountL++;
                    if (val.checked && val.checked == true) {
                        var checked = 'checked';
                    }

                    list = list + "<li><fieldset id='layer" + CountLL + "'>" +
                        "<label class='checkbox' for='visible" + CountLL + "'>" +
                        "<input id='visible" + CountLL + "' class='visible' type='" + check_type + "' " + checked + "/>" + val.title +
                        "</label>" +
                        "</fieldset></li>";
                    //console.log(key, val);
                    //console.log(list);
                });
                return list;
            }

                break;
            case 'radio':
                layers.push("<li><span>" + val.title + "</span><ul class='" + val.group + "'>" + layersInGroupR(val, check_type) + "</ul></li>");

            function layersInGroupR(val, check_type) {
                var list = '';
                var n = 0;
                var groupName = val.group;

                $.each(val.layers, function (key, val) {
                    if (val.checked && val.checked == true) {
                        var checked = 'checked';
                    }

                    list = list + "<li><input name='grouplayer" + j + "' id='" + groupName + n + "' class='visible' type='" + check_type + "' " + checked + "/>" + val.title + "</li>";
                    n++;
                    //console.log(key, val);
                    //console.log(list);
                });
                j++;

                return list;
            }

                break;
        }


    });
    $("<div/>", {
        "class": "layers-select",
        html: layers.join("")
    }).appendTo(".right-block");

    $('.layers-select li > span').click(function () {
        $(this).siblings('.layersGroup').toggle();
    }).siblings('.layersGroup').show();

    $('.layers-select li > span').click(function () {
        $(this).siblings('.baselayer').toggle();
    }).siblings('.baselayer').show();

    var baseL = 'grouplayer' + (--j);

    $("input[name=" + baseL + "]").click(function () {
        var idL = this.id;
        console.log(idL);
        //var numL = idL.substr(idL.length-1);
        setBaseLayer(idL);
    });
    setBlockLayersStatus = true;
}
/**функция вывода атрибутов*/
function clearEchoBlock() {
    $("#objInfo").empty();
}

function echoAttribute(obj,layer) {

    //console.log(layer);
    var objStr = '';
    var titleLayer;
    var aliasRu;
    var aliasStatus = false;
    //console.log(layer);
    //console.log(obj.features.length);


    //нужно переделать
    if (alias[layer]) {
        aliasStatus = true;
        var layerAlias = alias[layer];
    }

    //заголовок и json
    $.each(allLayersJson, function (key, val) {
        if(val.layer == layer) {
            titleLayer = val.title;
        }
    });

    //console.log(layerAlias);

    objStr = '<p>' + titleLayer + '</p>';
    for (var i=0; i<obj.features.length;i++){
        $.each(obj.features[i].properties, function (key, val) {
            //console.log(key, val);
            if (aliasStatus === true) {
                aliasRu = layerAlias[key];
            } else {
                aliasRu = key;
            }
            objStr = objStr + "<span>" + aliasRu + ":<br>" + val + '</span><br>';
        });
        $("#objInfo").append(objStr);
    }


}


/*$( "#search_page" ).click(function () {
    $( "#map-main .ol-viewport" ).remove();

    mapStatus['map'] = false;
    if ($("li").attr('class') == "active") {
        renderMap(1);
    }
});*/

/*
$( "#jurnal-zakaz" ).click(function () {
    unClick = false;
    console.log(unClick)
});*/

/*$( "#search_page" ).click(function () {
    setTimeout(function () {
        //$( "#map-main > .ol-viewport" ).remove();
        map.getViewport().remove();
        mapStatus['map'] = false;
        console.log(1);
        map.removeLayer(allLayers);
        renderMap(1);
        map.setView(viewMap);
    }, 500)
});*/

function transformPoint(point) {
    var coord = ol.proj.transform(point.getCoordinates(), 'EPSG:4326', 'EPSG:3857');
    var point = new ol.geom.Point(coord);
    return point;
}

function addFeaturePoint(point, name, layer) {
    var mapPoint = transformPoint(point);
    var feature = new ol.Feature({
        geometry: mapPoint,
        name: name
    });
    layer.addFeature(feature);
}

function doPan(location) {
    // pan from the current center
    var pan = ol.animation.pan({
        source: map.getView().getCenter()
    });
    map.beforeRender(pan);
    // when we set the new location, the map will pan smoothly to it
    map.getView().setCenter(location);
}
function parceXY(point) {
    var regex = /\d+(.)\d+/ig; //match(/\d+(.)\d+/ig)
    var xy = point.match(regex);
    var x = parseFloat(xy[0]);
    var y = parseFloat(xy[1]);

    return [x,y]
}

function doZoom(point) {

    //POINT(27.6023456396907 53.9683792674598)

    //var location = ol.proj.transform([x, y], 'EPSG:4326', 'EPSG:3857');
    var location = ol.proj.transform(parceXY(point), 'EPSG:4326', 'EPSG:3857');
    // zoom from the current resolution
    var zoom = ol.animation.zoom({
        resolution: map.getView().getResolution()
    });
    map.beforeRender(zoom);
    // setting the resolution to a new value will smoothly zoom in or out
    // depending on the factor
    map.getView().setCenter(location);
    map.getView().setResolution('0.14929107086948487');
}