/**
 * Created by ageichik on 30.08.2016.
 */
//var urlGeoserver = 'http://localhost:8080/geoserver/'; //home
/*var urlGeoserverView = 'http://gisserver.nca.by:8080/geoserver/'; // home
 var urlGeoserverRaster = 'http://gisserver2.nca.by:8080/geoserver/'; // home*/
//var urlGeoserver = 'http://gisserver3.nca.by:8080/geoserver/';
var urlGeoserver = 'http://nca-vm-gis:8080/geoserver/';
var urlGeoserverView = 'http://nca-vm-mrrgis-gs:8080/geoserver/';
var urlGeoserverRaster = 'http://arcgisserver:8080/geoserver/';
var typeWMS = 'wms';
var typeWFS = 'wfs';
var typeWPS = 'wps';
var geojsonFormat = new ol.format.GeoJSON();
/*var baselayer = new ol.layer.Tile({
 source: new ol.source.OSM()
 });*/
var map,map2;
var mapStatus = {'map':false,'map2':false};
var unClick = true;
var viewMap;
var evtClick;
var coordClick;
var callbackLayerTest=[];
var statusForTools = false;
var statusFunc = false;
var searchSource,search,selectSingleClick,selectLayer,sourceExport,vectorExport;
var selectActive = true;
var baselayer;
var alias=[];
var allLayers = [];
var arrLayersVisible = [];
var selectLayersId = [];
var selectLayersName= [];
var layersList = [];
var baselayerJson;
var allLayersJson=[];
var typeSelect = 'Clear';
var draw;


viewMap = new ol.View({
    center: [3068176.8128857943, 7151666.96276984],
    projection: 'EPSG:3857',
    //projection: 'EPSG:3395',
    zoom: 9,
    maxZoom: 20
});

function setAllLayers (data) {
    var numL = 0;
    allLayers = [];
    /**добавлние слоев из json*/
    $.each(data.layers, function( key, val ) {

        if (val.group != 'baselayer') {



            $.each(val.layers, function( key, val ) {
                allLayersJson[numL] = val;

                if (val.url_wms) {
                    //selectLayersId[numL] = val.layer;

                    //selectLayersName.push(val.layer);
                    //selectLayersName[val.layer]= {numL:numL, nameL : val.layer, "url_wms":val.url_wms,"url_wfs":val.url_wfs};
                    selectLayersId.push({numL:numL, nameL : val.layer, "url_wms":val.url_wms,"url_wfs":val.url_wfs,"selected":val.selected});

                    if (val.alias) alias[val.layer] = val.alias;
                }

                layersList[val.layer] = new ol.layer[val.type.layer]({
                    source: new ol.source[val.type.source](val.source),
                    visible: val.checked //перепроверить необходимость чека при формировании дерева в main.js
                });

                //allLayers.push( layersList['layer'+numL]);
                allLayers.push(layersList[val.layer]);

                numL++;
            });
        } else {
            baselayerJson = val;
            var idRow;
            $.each(val.layers, function( key, val ) {
                if (val.checked && val.checked == true) {
                    idRow = val.id-1;
                    return false;
                }
            });
            var baseL = val.layers[idRow];

            baselayer = new ol.layer[baseL.type.layer]({
                source: new ol.source[baseL.type.source](baseL.source/*{
                 url:'http://mt{0-3}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'
                 /!*h = roads only
                 m = standard roadmap
                 p = terrain
                 r = somehow altered roadmap
                 s = satellite only
                 t = terrain only
                 y = hybrid*!/
                 }*/)
            });
        }

    });

    /**рабочите слои*/

    selectLayer = new ol.layer.Vector({
        name: 'selectLayer',
        source: new ol.source.Vector({
            projection: 'EPSG:3857'
        }),
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.3)'
            }),
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 0, 255, 0.8)',
                width: 1
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.5)'
                })
            })
        })
    });

    searchSource = new ol.source.Vector({
        format: new ol.format.GeoJSON()
        //projection: 'EPSG:3857',
        //visible: true
    });
    search = new ol.layer.Vector({
        //format: geojsonFormat,
        projection: 'EPSG:3857',
        source: searchSource,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(222, 26, 24, 0.5)'
            }),
            stroke: new ol.style.Stroke({
                color: '#f54040',
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#f54040'
                })
            })
        })
    });

    sourceExport = new ol.source.Vector({wrapX: false});
    vectorExport = new ol.layer.Vector({
        source: sourceExport,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 204, 51, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#ffcc33'
                })
            })
        })
    });

    allLayers.push(search,selectLayer,vectorExport);
}

/** map-main */
function initMap(data) {
    console.log('initMap');
    setAllLayers (data);


    //console.log(layersList);
    //allLayers.push(layersList[0]);
    //allLayers.push(layersList['layer0']);

    //allLayers.push(layersList);
    /*allLayers.push(
     //baselayer,
     lots = new ol.layer.Image({
     visible: true,
     //extent: [-13884991, 2870341, -7455066, 6338219],
     source: new ol.source.ImageWMS({
     //url: 'http://nca-vm-gis:8080/geoserver/wms',
     url: urlGeoserver+'bpd/'+typeWMS, //'http://localhost:8080/geoserver/wms',
     //url: 'http://localhost:8080/geoserver/bpd/wms', //'http://localhost:8080/geoserver/wms',
     params: {
     'LAYERS': 'bpd:lots'//, 'TILED': true
     },
     serverType: 'geoserver'
     })
     }),
     addr = new ol.layer.Tile({
     visible: true,
     //extent: [-13884991, 2870341, -7455066, 6338219],
     source: new ol.source.TileWMS({
     //url: 'http://nca-vm-gis:8080/geoserver/wms',
     url: urlGeoserverView+'RB/'+typeWMS, //'http://localhost:8080/geoserver/wms',
     //url: 'http://localhost:8080/geoserver/bpd/wms', //'http://localhost:8080/geoserver/wms',
     params: {
     'LAYERS': 'RB:addr_points_layer'//, 'TILED': true
     },
     serverType: 'geoserver'
     })
     }),
     search,
     selectLayer,
     vectorExport
     );*/
    proj4.defs('EPSG:900913','+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs');
    proj4.defs('EPSG:3395', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m');
    proj4.defs('EPSG:32635', '+proj=utm +zone=35 +ellps=WGS84 +datum=WGS84 +units=m +no_defs');
    proj4.defs('urn:ogc:def:crs:EPSG::4326', proj4.defs('EPSG:4326'));
    proj4.defs('urn:x-ogc:def:crs:EPSG:32635', proj4.defs('EPSG:32635'));
    //proj4.defs('EPSG:3857', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs ');

    /*viewMap = new ol.View({
        center: [3068176.8128857943, 7151666.96276984],
        projection: 'EPSG:3857',
        //projection: 'EPSG:3395',
        zoom: 9,
        maxZoom: 20
    });*/

    var scaleLineControl = new ol.control.ScaleLine();
    console.log(map);
    if (mapStatus['map'] == false) {
        map = new ol.Map({
            controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                    collapsible: false
                })
            }).extend([
                scaleLineControl,
                /*new ol.control.ZoomToExtent({
                 extent: [
                 2774658.6242707176, 7029367.7175135575, 3361695.001500871, 7273966.208026122
                 ]
                 })*/
            ]),
            //layers: allLayers,
            target: 'map-main',
            //target: 'map-search',
            view: viewMap
        });
        mapStatus['map'] = true;
    }


    for (var i=0; i < allLayers.length;i++) {
        map.addLayer(allLayers[i]);
    }
    addBaseLayer('map');


    map.getLayers().forEach(function(layer, i) {
        bindInputs('#layer' + i, layer);
        if (layer instanceof ol.layer.Group) {
            layer.getLayers().forEach(function(sublayer, j) {
                bindInputs('#layer' + i + j, sublayer);
            });
        }
    });

    /*selectSingleClick = new ol.interaction.Select();
     map.addInteraction(selectSingleClick);*/
    /*selectSingleClick.on('select', function(evt){
     if(evt.selected.length > 0){
     evt.selected.forEach(function(feature){
     var layer = feature.getLayer(map);
     console.info(layer);
     console.info(layer.get('name'));
     });
     }

     });*/

    /**клик по карте*/
    map.on('click', function(evt) {
        if (statusFunc === false)
        {
            clearEchoBlock();
            evtClick = evt;
            //selectLayer.getSource().clear();

            /*var clickCoordinate = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326')*/
            //console.log(selectLayersId);
            //console.log(allLayersJson);
            //console.log(map.getLayers().item(2+1).getSource().getGetFeatureInfoUrl);

            //console.log(evt.coordinate[0] +' '+ evt.coordinate[1]);
            var wgs84coord = proj4('EPSG:3857','EPSG:4326').forward(evt.coordinate);
            var geomWkt = 'POINT('+wgs84coord[0] +' '+ wgs84coord[1]+')';
            coordClick = wgs84coord[0] +' '+ wgs84coord[1];
            //console.log(geomWkt);
            console.log(coordClick);


            var cql_filter = 'INTERSECTS(geom,POINT('+evt.coordinate[0] +' '+ evt.coordinate[1]+'))';


            /**получаем слои для getGetFeatureInfo и отправляем запросы по ним*/
            //console.log(selectLayersName);
            var typeS = 'select';
            getVisibleLayers('selected');



            if(arrLayersVisible.length > 0) {
                console.log(true);
                console.log(arrLayersVisible);
                searchObj(cql_filter,arrLayersVisible,typeS);
            } else {
                console.log(false);
                httpServices.getResultByCoord(coordClick,false);
            }

        }
    });

    addInteraction();

    map.getView().on('change:resolution', function(evt) {
        var resolution =map.getView().getResolution()
        console.log(map.getView().getResolution())
        //требуется доработка
        //не только запрет на tools, но и отправку запросов
        if(resolution<4) {
            statusForTools = true;
            $("#tools").css('pointer-events','visible');
        } else {
            statusForTools = false;
            $("#tools").css('pointer-events','none');
        }
    });
}

function SendPost(url,xml,outType) {

    //отправляю POST запрос и получаю ответ
    $.post(
        'ajax.php',//url адрес файла обработчика
        {url: url, xml: xml, outType: outType},//параметры запроса

        //response:'text',//тип возвращаемого ответа text либо xml
        function (response) {//возвращаемый результат от сервера
            //$('result',$('result').innerHTML+'<br />'+response);
            $( "#result" ).empty().append('<a href="'+response+'">'+response+'</a>');
            //$("body").append("<iframe src='" + response.url+ "' style='display: none;' ></iframe>");
            //alert(response);
            console.log(response);
        }
    );

    /*var product_id = 'abc';
     var image_links = 'somelink.com\/123';

     $.post(
     "ajax.php",
     { id: product_id, images : jQuery.makeArray(image_links) },
     function(response) {
     // Response is automatically a json object
     for(var i = 0; i < response.images.length; i++) {
     //alert(response.images[i]);
     $( "#result" ).empty().append(response.images[i]);
     }
     }, 'json' // <-- HERE
     );*/
}

function setBaseLayer(idBaseLayer) {
    if (map) {
        map.removeLayer(baselayer);
    }
    if (map2) {
        map2.removeLayer(baselayer);
    }

    var idRow;
    $.each(baselayerJson.layers, function( key, val ) {
        if (val.layer == idBaseLayer) {
            idRow = val.id-1;
            return false;
        }
    });
    var baseL = baselayerJson.layers[idRow]
    baselayer = new ol.layer[baseL.type.layer]({
        source: new ol.source[baseL.type.source](baseL.source)
    });

    addBaseLayer('all');
}
function addBaseLayer(numMap) {

    switch (numMap) {
        case 'all':
            if (map) {
                map.getLayers().insertAt(0, baselayer);
            }
            if (map2) {
                map2.getLayers().insertAt(0, baselayer);
            }
            break;
        case 'map':
            map.getLayers().insertAt(0, baselayer);
            break;
        case 'map2':
            map2.getLayers().insertAt(0, baselayer);
            break;
        default :
            break;
        //map.getLayers().insertAt(0, baselayer);
        //map2.getLayers().insertAt(0, baselayer);
    }
}

function bindInputs(layerid, layer) {

    var visibilityInput = $(layerid + ' input.visible');
    visibilityInput.on('change', function() {
        layer.setVisible(this.checked);
    });
    visibilityInput.prop('checked', layer.getVisible());

    /*var opacityInput = $(layerid + ' input.opacity');
     opacityInput.on('input change', function() {
     layer.setOpacity(parseFloat(this.value));
     });
     opacityInput.val(String(layer.getOpacity()));*/
}

// global so we can remove it later
function addInteraction() {
    var value = typeSelect;
    var tt;
    if (value !== 'Clear') {
        var geometryFunction, maxPoints;
        if (value === 'Square') {
            value = 'Circle';
            geometryFunction = ol.interaction.Draw.createRegularPolygon(4);
        } else if (value === 'Box') {
            value = 'LineString';
            maxPoints = 2;
            geometryFunction = function(coordinates, geometry) {
                if (!geometry) {
                    geometry = new ol.geom.Polygon(null);
                }
                var start = coordinates[0];
                var end = coordinates[1];
                geometry.setCoordinates([
                    [start, [start[0], end[1]], end, [end[0], start[1]], start]
                ]);
                //console.log(sourceExport.getFeatures());
                return geometry;

            };
        }
        draw = new ol.interaction.Draw({
            source: sourceExport,
            type: /** @type {ol.geom.GeometryType} */ (value),
            geometryFunction: geometryFunction,
            maxPoints: maxPoints
        });
        draw.on('drawstart',function(e){sourceExport.clear(),selectActive = false});
        draw.on('drawend',function(e){
            //отрисовка ЗУ для экспорта под выделением области

            setTimeout(function() {
                searchObj(getObjects(),['parcels'],'service')
                    , 10000
            });

        });

        map.addInteraction(draw);

    } else {
        sourceExport.clear();
    }

}


/** map-search-zakaz */
function initMap2(data) {
    console.log('initMap2');
    var numL = 0;

    /**добавлние слоев из json*/
    /*$.each(data.layers, function( key, val ) {

        if (val.group != 'baselayer') {



            $.each(val.layers, function( key, val ) {
                allLayersJson[numL] = val;

                if (val.url_wms) {
                    //selectLayersId[numL] = val.layer;

                    //selectLayersName.push(val.layer);
                    //selectLayersName[val.layer]= {numL:numL, nameL : val.layer, "url_wms":val.url_wms,"url_wfs":val.url_wfs};
                    selectLayersId.push({numL:numL, nameL : val.layer, "url_wms":val.url_wms,"url_wfs":val.url_wfs,"selected":val.selected});

                    if (val.alias) alias[val.layer] = val.alias;
                }

                layersList[val.layer] = new ol.layer[val.type.layer]({
                    source: new ol.source[val.type.source](val.source),
                    visible: val.checked //перепроверить необходимость чека при формировании дерева в main.js
                });

                //allLayers.push( layersList['layer'+numL]);
                allLayers.push(layersList[val.layer]);

                numL++;
            });
        } else {
            baselayerJson = val;
            var idRow;
            $.each(val.layers, function( key, val ) {
                if (val.checked && val.checked == true) {
                    idRow = val.id-1;
                    return false;
                }
            });
            var baseL = val.layers[idRow];

            baselayer = new ol.layer[baseL.type.layer]({
                source: new ol.source[baseL.type.source](baseL.source/!*{
                 url:'http://mt{0-3}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'
                 /!*h = roads only
                 m = standard roadmap
                 p = terrain
                 r = somehow altered roadmap
                 s = satellite only
                 t = terrain only
                 y = hybrid*!/
                 }*!/)
            });
        }

    });*/

    /**рабочите слои*/

    /*selectLayer = new ol.layer.Vector({
        name: 'selectLayer',
        source: new ol.source.Vector({
            projection: 'EPSG:3857'
        }),
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.3)'
            }),
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 0, 255, 0.8)',
                width: 1
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.5)'
                })
            })
        })
    });

    searchSource = new ol.source.Vector({
        format: new ol.format.GeoJSON()
        //projection: 'EPSG:3857',
        //visible: true
    });
    search = new ol.layer.Vector({
        //format: geojsonFormat,
        projection: 'EPSG:3857',
        source: searchSource,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(222, 26, 24, 0.5)'
            }),
            stroke: new ol.style.Stroke({
                color: '#f54040',
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#f54040'
                })
            })
        })
    });

    sourceExport = new ol.source.Vector({wrapX: false});
    vectorExport = new ol.layer.Vector({
        source: sourceExport,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 204, 51, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#ffcc33'
                })
            })
        })
    });

    allLayers.push(search,selectLayer,vectorExport);*/


    proj4.defs('EPSG:900913','+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs');
    proj4.defs('EPSG:3395', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m');
    proj4.defs('EPSG:32635', '+proj=utm +zone=35 +ellps=WGS84 +datum=WGS84 +units=m +no_defs');
    proj4.defs('urn:ogc:def:crs:EPSG::4326', proj4.defs('EPSG:4326'));
    proj4.defs('urn:x-ogc:def:crs:EPSG:32635', proj4.defs('EPSG:32635'));
    //proj4.defs('EPSG:3857', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs ');



    var scaleLineControl = new ol.control.ScaleLine();

    if (mapStatus['map2'] == false) {
        map2 = new ol.Map({
            controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                    collapsible: false
                })
            }).extend([
                scaleLineControl,
                /*new ol.control.ZoomToExtent({
                 extent: [
                 2774658.6242707176, 7029367.7175135575, 3361695.001500871, 7273966.208026122
                 ]
                 })*/
            ]),
            layers: allLayers,
            target: 'map-search-zakaz',
            view: viewMap
        });
        mapStatus['map2'] = true;
    }

    for (var i=0; i < allLayers.length;i++) {
        map2.addLayer(allLayers[i]);
    }
    addBaseLayer('map2');


    map2.getLayers().forEach(function(layer, i) {
        bindInputs('#layer' + i, layer);
        if (layer instanceof ol.layer.Group) {
            layer.getLayers().forEach(function(sublayer, j) {
                bindInputs('#layer' + i + j, sublayer);
            });
        }
    });

    /*selectSingleClick = new ol.interaction.Select();
     map.addInteraction(selectSingleClick);*/
    /*selectSingleClick.on('select', function(evt){
     if(evt.selected.length > 0){
     evt.selected.forEach(function(feature){
     var layer = feature.getLayer(map);
     console.info(layer);
     console.info(layer.get('name'));
     });
     }

     });*/


    /**клик по карте*/
    map2.on('click', function(evt) {
        if (unClick == false) {
            if (statusFunc === false)
            {
                clearEchoBlock();
                evtClick = evt;

                //console.log(evt.coordinate[0] +' '+ evt.coordinate[1]);
                var wgs84coord = proj4('EPSG:3857','EPSG:4326').forward(evt.coordinate);
                var geomWkt = 'POINT('+wgs84coord[0] +' '+ wgs84coord[1]+')';
                coordClick = wgs84coord[0] +' '+ wgs84coord[1];
                //console.log(geomWkt);
                console.log(coordClick);

                httpServices.update_location(current_object_id,coordClick);

                $("#order-object-table").removeClass("disabled-div");
                $("#search_tabs").removeClass("disabled-div");
                unClick = true;


                //var cql_filter = 'INTERSECTS(geom,POINT('+evt.coordinate[0] +' '+ evt.coordinate[1]+'))';

                /**получаем слои для getGetFeatureInfo и отправляем запросы по ним*/
                /*var typeS = 'select';
                getVisibleLayers('selected');
                if(arrLayersVisible) {
                    console.log(true);
                    console.log(arrLayersVisible);
                    searchObj(cql_filter,arrLayersVisible,typeS);
                } else {
                    console.log(false);
                    httpServices.getResultByCoord(coordClick,false);

                }*/

            }
        }

    });

    addInteraction();

    map2.getView().on('change:resolution', function(evt) {
        var resolution = map2.getView().getResolution()
        console.log(map2.getView().getResolution())
        //требуется доработка
        //не только запрет на tools, но и отправку запросов
        if(resolution<4) {
            statusForTools = true;
            $("#tools").css('pointer-events','visible');
        } else {
            statusForTools = false;
            $("#tools").css('pointer-events','none');
        }
    });
}