/**
 * Created by djinn on 21.09.2016.
 */

var snp = false;
//function changeFilter() {

//$("#search").submit(function(e) {
$("#searchBtn").click(function(e) {
        clearEchoBlock();
        var value = $("#searchField").val().toLowerCase();
        var cadnumRegex = /^\d{18}$/g;

        if(cadnumRegex.exec(value)) {
            var cql_filter = '';


            //cql_filter = cql_filter + 'strToLowerCase(cadnum)%3D' + /*'%27' +*/ $("#searchField").val().toLowerCase()/*+ '%27'*/;
            cql_filter = cql_filter + 'cad_num=' + /*'%27' +*/ $("#searchField").val().toLowerCase()/*+ '%27'*/;



            var searchLN = ['parcels'];
            var typeS = 'search';

            searchObj(cql_filter,searchLN,typeS);
        } else {
            alert('Неправильный номер');
        }

    }
);

function searchObj(cql_filter,layerNames,typeS) {
    var numQ = 0;
    var urlQuery,layerQuery;
    var layerObj=[];
    var propertyName ='';
    var viewProjection = viewMap.getProjection();
    var viewResolution = viewMap.getResolution();
    searchSource.clear();


    //var extent = map.getView().calculateExtent(map.getSize());

    if (typeS !== 'select') {
        for (var i=0; i<layerNames.length;i++){
            var ajaxR = false;
            //console.log(allLayersJson);
            $.each(allLayersJson, function( key, val ) {
                if (val.layer == layerNames[i]) {
                    if (val.url_wfs) {
                        layerObj = layerNames[i];

                        urlQuery = val.url_wfs;
                        propertyName = val.source.params.propertyName;
                        layerQuery=val.source.params.layers;
                        ajaxR = true;
                    }
                }
            });
            if (ajaxR === true ){
                $.ajax(urlQuery, {
                    type: 'GET',
                    data: {
                        service: 'WFS',
                        version: '1.1.0',
                        request: 'GetFeature',
                        typename: layerQuery,
                        cql_filter: cql_filter,
                        propertyName: propertyName,
                        //outputFormat: 'shape-zip',
                        outputFormat: 'text/javascript',
                        //outputFormat: 'application/json',
                        //format_options: 'callback:loadFeatures',
                        srsname: 'EPSG:4326'
                        //bbox: extent.join(',') + ',EPSG:4326'
                    },
                    dataType: 'jsonp',
                    jsonp: false
                });

            }
        }
    } else {

        $.req = (function () {
            var queue = $.Deferred().resolve();

            return function (options) {

                function _call () {
                    //console.log(options);
                    return $.ajax(options);
                }

                return (queue = queue.then(_call, _call));
            };
        })();


        for (var i=0;i<layerNames.length;i++) {
            var layer = layersList[layerNames[i]];
            callbackLayerTest.push(layerNames[i]);

            if (layer.getSource().i.layers) {
                var query_layers = layer.getSource().i.layers;
            } else {
                var query_layers = layer.getSource().c.layers;
            }

            var urlQ = layer.getSource().getGetFeatureInfoUrl(
                evtClick.coordinate, viewResolution, viewProjection,
                {
                    'query_layers': query_layers,
                    'INFO_FORMAT': 'text/javascript',
                    'feature_count': 50,
                    'format_options': 'callback:loadFeatures'
                }
            );
            $.req({ url: urlQ, dataType: 'jsonp'}).always(function () {
                //console.log(a);
                //console.log(callbackLayerTest);
                var layerQ = callbackLayerTest.shift();

                //вызов функции поиска кадастрового номера в базе
                /*if (layerQ == 'parcels') {
                 //console.log(a.features[0].properties);
                 var cadnum = a.features[0].properties.cad_num;
                 console.log(cadnum);
                 }*/




                if(a.features[0]) {
                    //console.log(a.features[0]);
                    httpServices.getResultByCoord(coordClick,true);
                    if (a.crs) {
                        var crs = a.crs.properties.name;
                        var reg = /EPSG::\d*/;
                        var responseCrs = reg.exec(crs)[0].replace("::", ":");
                    }
                    searchSource.addFeatures(geojsonFormat.readFeatures(a,{dataProjection: responseCrs,featureProjection: 'EPSG:3857'}));
                    /*showBlock(blockL, zoomEl);*/

                    switch (typeS) {
                        case 'search':
                            if ($.isEmptyObject(a.features) == false){
                                map.getView().fit(searchSource.getExtent(), map.getSize());

                                echoAttribute(a,layerObj);
                            } else {
                                //нужно написать функцию
                                //alert('Номер не найден');
                            }

                            break;
                        case 'service':
                            /*searchSource.clear();
                             searchSource.addFeatures(geojsonFormat.readFeatures(response,{dataProjection: 'EPSG:4326',featureProjection: 'EPSG:3857'}));*/
                            wpsObject = a;
                            //wpsService(response);

                            break;
                        case 'select':

                            //var result = makeJson(response);

                            echoAttribute(a,layerQ);

                            /*searchSource.clear();
                             searchSource.addFeatures(geojsonFormat.readFeatures(response,{dataProjection: 'EPSG:4326',featureProjection: 'EPSG:3857'}));*/

                            break;
                        default:
                            break;
                    }
                } else {
                    /**проверка СНП*/
                    snp = false;
                    checkIntersaction('dev:snp',cql_filter);

                    /*if (snp == true) {
                        alert('перейдите к поиску "По сельскому населённому пункту и территории вне населенных пунктов"')
                    } else if (snp == false){
                        httpServices.getResultByCoord(coordClick,false);
                    }*/

                }

            });

        }

    }


    /**проверка на url_wfs и поиск (нет условия если нет url_wfs, но есть url_wms)*/


        //typeS тип поска | map - для визуализации на карте; | service - для сервисов (wps,wfs,wms)
    loadFeatures = function (response) {
        a = response;
        //var feature = response.features[0];
    };
    parseResponse = function (response) {



        if ($.isEmptyObject(response.features) == true){
            alert('Номер не найден');
        };


        if(response.features[0]) {
            //console.log(a.features[0]);
            if (response.crs) {
                var crs = response.crs.properties.name;
                var reg = /EPSG::\d*/;
                var responseCrs = reg.exec(crs)[0].replace("::", ":");
            }
            searchSource.addFeatures(geojsonFormat.readFeatures(response,{dataProjection: responseCrs,featureProjection: 'EPSG:3857'}));
            /*showBlock(blockL, zoomEl);*/

            switch (typeS) {
                case 'search':
                    if ($.isEmptyObject(response.features) == false){
                        map.getView().fit(searchSource.getExtent(), map.getSize());

                        echoAttribute(response,layerObj);
                    } else {
                        //нужно написать функцию
                        alert('Номер не найден');
                    }

                    break;
                case 'service':
                    /*searchSource.clear();
                     searchSource.addFeatures(geojsonFormat.readFeatures(response,{dataProjection: 'EPSG:4326',featureProjection: 'EPSG:3857'}));*/
                    wpsObject = response;
                    //wpsService(response);

                    break;
                case 'select':

                    //var result = makeJson(response);

                    echoAttribute(response,layerQ);

                    /*searchSource.clear();
                     searchSource.addFeatures(geojsonFormat.readFeatures(response,{dataProjection: 'EPSG:4326',featureProjection: 'EPSG:3857'}));*/

                    break;
                default:
                    break;
            }
            numQ++;
        } else {
            numQ++;
        }
    };
}

function checkIntersaction(layerName,cql_filter) {

    var name = layerName;

    $.ajax('http://gisserver3.nca.by:8080/geoserver/wfs', {
        type: 'GET',
        data: {
            service: 'WFS',
            version: '1.0.0',
            request: 'GetFeature',
            cql_filter: cql_filter,
            typename: name,
            srsname: 'EPSG:4326',
            outputFormat: 'text/javascript'
        },
        dataType: 'jsonp',
        jsonpCallback:'callback:loadSnp',
        jsonp: 'format_options'
    });
    loadSnp = function (response) {
        if(response.features[0]) {
            snp = true;
            alert('Поиск по заданному местоположению невозможен. Осуществите поиск "По сельскому населённому пункту и территории вне населенных пунктов".');
        } else {
            snp = false;
            httpServices.getResultByCoord(coordClick,false);
        }
    };
}