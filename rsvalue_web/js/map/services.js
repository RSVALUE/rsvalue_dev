/**
 * Created by Ageichick on 01.09.2016.
 */

var wktExport,wktExport4326,wpsObject;

var xml_test2 = '<?xml version=\"1.0\" encoding=\"UTF-8\"?> <wps:Execute version=\"1.0.0\" service=\"WPS\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opengis.net/wps/1.0.0\" xmlns:wfs=\"http://www.opengis.net/wfs\" xmlns:wps=\"http://www.opengis.net/wps/1.0.0\" xmlns:ows=\"http://www.opengis.net/ows/1.1\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:ogc=\"http://www.opengis.net/ogc\" xmlns:wcs=\"http://www.opengis.net/wcs/1.1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xsi:schemaLocation=\"http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd\">   <ows:Identifier>JTS:buffer</ows:Identifier>   <wps:DataInputs>     <wps:Input>       <ows:Identifier>geom</ows:Identifier>       <wps:Data>         <wps:ComplexData mimeType=\"text/xml; subtype=gml/3.1.1\">             <gml:LineString>                 <gml:posList>0.0 0.0 10.0 0.0 10.0 10.0</gml:posList>             </gml:LineString>         </wps:ComplexData>       </wps:Data>     </wps:Input>     <wps:Input>       <ows:Identifier>distance</ows:Identifier>       <wps:Data>         <wps:LiteralData>2</wps:LiteralData>       </wps:Data>     </wps:Input>   </wps:DataInputs>   <wps:ResponseForm>     <wps:RawDataOutput mimeType=\"text/xml; subtype=gml/3.1.1\">       <ows:Identifier>result</ows:Identifier>     </wps:RawDataOutput>   </wps:ResponseForm> </wps:Execute>';
var xml_test3 = '<?xml version=\"1.0\" encoding=\"UTF-8\"?> <wps:Execute version=\"1.0.0\" service=\"WPS\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.opengis.net/wps/1.0.0\" xmlns:wfs=\"http://www.opengis.net/wfs\" xmlns:wps=\"http://www.opengis.net/wps/1.0.0\" xmlns:ows=\"http://www.opengis.net/ows/1.1\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:ogc=\"http://www.opengis.net/ogc\" xmlns:wcs=\"http://www.opengis.net/wcs/1.1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xsi:schemaLocation=\"http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd\">   <ows:Identifier>JTS:buffer</ows:Identifier>   <wps:DataInputs>     <wps:Input>       <ows:Identifier>geom</ows:Identifier>       <wps:Data>         <wps:ComplexData mimeType=\"text/xml; subtype=gml/3.1.1\">             <gml:LineString>                 <gml:posList>0.0 0.0 10.0 0.0 10.0 10.0</gml:posList>             </gml:LineString>         </wps:ComplexData>       </wps:Data>     </wps:Input>     <wps:Input>       <ows:Identifier>distance</ows:Identifier>       <wps:Data>         <wps:LiteralData>2</wps:LiteralData>       </wps:Data>     </wps:Input>   </wps:DataInputs>   <wps:ResponseForm>     <wps:RawDataOutput mimeType=\"application/gml-3.1.1\">       <ows:Identifier>result</ows:Identifier>     </wps:RawDataOutput>   </wps:ResponseForm> </wps:Execute>';


var xml_test = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
    '<wps:Execute version=\"1.0.0\" service=\"WPS\" ' +
        'xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ' +
        'xmlns=\"http://www.opengis.net/wps/1.0.0\" ' +
        'xmlns:wfs=\"http://www.opengis.net/wfs\" ' +
        'xmlns:wps=\"http://www.opengis.net/wps/1.0.0\" ' +
        'xmlns:ows=\"http://www.opengis.net/ows/1.1\" ' +
        'xmlns:gml=\"http://www.opengis.net/gml\" ' +
        'xmlns:ogc=\"http://www.opengis.net/ogc\" ' +
        'xmlns:wcs=\"http://www.opengis.net/wcs/1.1.1\" ' +
        'xmlns:xlink=\"http://www.w3.org/1999/xlink\" ' +
        'xsi:schemaLocation=\"http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd\">' +
        '<ows:Identifier>gs:Clip</ows:Identifier>' +
        '<wps:DataInputs>' +
            '<wps:Input>' +
                '<ows:Identifier>features</ows:Identifier>' +
                '<wps:Reference mimeType=\"text/xml\" xlink:href=\"http://geoserver/wfs\" method=\"POST\">' +
                    '<wps:Body>' +
                        '<wfs:GetFeature service=\"WFS\" version=\"1.0.0\" outputFormat=\"GML2\" xmlns:topp=\"http://www.openplans.org/topp\">' +
                            '<wfs:Query typeName=\"topp:states\"/>' +
                        '</wfs:GetFeature>' +
                    '</wps:Body>' +
                '</wps:Reference>' +
            '</wps:Input>' +
            '<wps:Input>' +
                '<ows:Identifier>clip</ows:Identifier>' +
                '<wps:Data>' +
                    '<wps:ComplexData mimeType=\"application/wkt\">' +
                    '<![CDATA[POLYGON ((-111.29515000314736 38.35256480611136, -111.29515000314736 23.173351014022927, -92.75022812814737 23.173351014022927, -92.75022812814737 38.35256480611136, -111.29515000314736 38.35256480611136))]]></wps:ComplexData>' +
                '</wps:Data>' +
            '</wps:Input>' +
        '</wps:DataInputs>' +
        '<wps:ResponseForm>' +
        '<wps:RawDataOutput mimeType=\"application/zip\">' +
        '    <ows:Identifier>result</ows:Identifier>' +
        '</wps:RawDataOutput>' +
        '</wps:ResponseForm>' +
    '</wps:Execute>';

//---монипуляции с кнопкапи для карты
function setBtnActive(btn) {
    typeBtn = btn.attr('type');
    valueBtn = btn.attr('value');
    console.log(btn);
    switch (typeBtn) {
        case 'buttonDraw':
            statusFunc = true;
            rmvBtbActive();
            btn.addClass('active');
            BtbDsblF();
            break;
        case 'button':
            if (valueBtn == 'Clear') {
                statusFunc = false;
                searchSource.clear();
                BtbDsblT();
                rmvBtbActive();
            }
            if (valueBtn == 'Export') {
                geoExport();
                rmvBtbActive();
                //BtbDsblT(btn);
                BtbDsblT();
                statusFunc = false;

                //setBtnActive($("button").attr({type:'button',value:'Clear'}));
                sourceExport.clear();
                searchSource.clear();
            }
            break;
    }

    //console.log($('.f-panel button[type="button"]'));

    //alert(btn.attr('type'));
}

//---состояния кнопок
function BtbDsblT(b) {
    if (!b) {
        $('.f-panel button[type="button"]').attr('disabled' , true);
    } else {
        b.attr('disabled' , true);
    }
}
function BtbDsblF() {$('.f-panel button[type="button"]').attr('disabled' , false);}
function rmvBtbActive() {$('.f-panel button').removeClass('active');}

function getObjects() {
    var features = sourceExport.getFeatures();
    var geojson  = new ol.format.GeoJSON();
    var wktf = new ol.format.WKT();



    var json = geojson.writeFeatures(features,
        {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        }
    );
    wktExport = wktf.writeFeatures(features/*,
     {
     dataProjection: 'EPSG:4326',
     featureProjection: 'EPSG:3857'
     }*/
    );
    wktExport4326 = wktf.writeFeatures(features,
        {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        }
    );
    console.log(wktExport);
    //propertyName перечисление полей
    var queryS = 'INTERSECTS(geom,'+wktExport+')';//&propertyName=cadnum,geom';
    console.log(queryS);
    return queryS;

}

function geoExport() {
    /*var queryS = getObjects();

    var layerName = ['local:parcels'];
    searchObj(queryS,layerName,'service');*/
    console.log()
    if (statusForTools === true) {
        wpsService(wpsObject);
    } else {
        alert('Вы пытаетесь сделать экспорт на недопустимом масштабе');
    }

}
function makeJson(queryObj,num) {
    if (num ==1) {
        return $.toJSON(queryObj).replace(/null/g,'\'\'');//реализовано из-за того что не обрабатывается null в json при отправке на wps
    } else {
        return $.toJSON(queryObj).replace(/null/g,'""');//реализовано из-за того что не обрабатывается null в json при отправке на wps
    }

}
function wpsService(queryObj) {
    //var queryObj;
    console.log(queryObj);

    //makeJson(queryObj);

    queryObj = makeJson(queryObj); //реализовано из-за того что не обрабатывается null в json при отправке на wps
    //console.log(queryObj.replace(/null/g,'""'));
    var outType = $( "#outType" ).val();
    var outEpsg = $( "#outEpsg" ).val();
    //console.log(outType,'-',outEpsg);
    //console.log(wktExport);

    var xml = createXml(wktExport4326,outType,outEpsg,queryObj);
    //console.log(xml);
    //var url = 'http://localhost:8080/geoserver/wps';
    SendPost(urlGeoserver+typeWPS,xml,outType);
}

function getVisibleLayers(selected){
    arrLayersVisible = [];
    $.each(selectLayersId,function (key, val) {
        //console.log(key, val);
        var layer = map.getLayers().item(val.numL + 1);
        //console.log(layer);
        if (layer.getVisible() === true) {
            if (selected) {
                if (val.selected === true){
                    arrLayersVisible.push(val.nameL);
                }
            } else {
                arrLayersVisible.push(val.nameL);
            }
        }
    });
}

$('.f-panel button').click(function() {
    typeSelect = $(this).attr('value');
    setBtnActive($(this));
    console.log(typeSelect);
    if(typeSelect == 'Export') {
        map.removeInteraction(draw);
        //searchSource.clear();
    } else{
        map.removeInteraction(draw);
        if(typeSelect != 'none') {
            addInteraction();

        } else {
            //console.log(sourceExport);
            sourceExport.clear();
            var fullExtent = map.getView().calculateExtent(map.getSize());
            var thing = ol.geom.Polygon.fromExtent(fullExtent);
            var featurething = new ol.Feature({
                name: "Thing",
                geometry: thing
            });
            sourceExport.addFeature( featurething );
            /*var query = getObjects();
             console.log(query);
             searchObj(query,'bpd:lots','select');
             console.log(sourceExport);*/
            var layerName = ['parcels1'];
            //arrLayersVisible
            getVisibleLayers();
            searchObj(getObjects(),layerName,'service');
        }
    }

});


function change_location() {
    unClick = false;
    console.log('clicked '+unClick);
    /*map2.on('click', function(evt) {
        console.log(evt.coordinate)
    });*/
};

/*$('#order-object-table > tr').click(function() {
    console.log(current_object)
    var regexp = /\d+/g;
    var idObj = $(this).attr('id').match(regexp)[0];
     var dataObj;

     if (order_scope.order_objects[$(this).index()].objectId == idObj) {
     dataObj = order_scope.order_objects[$(this).index()];
     }
    search_order_obj_for_map(idObj);

});*/

function search_order_obj_for_map(data) {
    //console.log(data);
    searchSource.clear();
    if (data != null) {
        if (data.coordinates != null) {
            var point_geom = new ol.geom.Point(parceXY(data.coordinates));
            addFeaturePoint(point_geom, 'геокод', searchSource);
            doZoom(data.coordinates);
        } else
        if (data.cadNum != null) {
            var cql_filter = 'cad_num=' + data.cadNum;
            searchObj(cql_filter,['parcels'],'search');
        }
    }
}





