/**
 * Created by Shcherbina on 09.06.2016.
 */
/*
 * Функция для добавления строки (<tr>),
 * содержащей информацию о файле
 * в тело таблицы (tbody)
 */

function appendFileInfo(tbody, data) {
    var tr = document.createElement('tr');
    for(var j = 0; j < data.length; j++) {
        td = document.createElement('td');
        td.innerHTML = data[j] || 'неизвестно';
        tr.appendChild(td);
    }
    tbody.appendChild(tr);
    return tbody;
}

/*
 * Эту функцию мы будем вызывать при изменении (onchange)
 * input'а, т.е. когда пользователь выберет файлы.
 */

function onFilesSelect(e) {
    // получаем объект FileList
    var files = e.target.files,
    // div, куда помещается таблица с информацией о файлах
        output = document.getElementById('output'),
    // таблица с информацией
        table = document.createElement('table'),
    // её тело
        tbody = document.createElement('tbody'),
    // строка с информацией о файле (Перезаписывается каждый шаг цикла)
        row,
    // FileReader (Создаётся для каждого файла)
        fr,
    // объект file из FileList'a
        file,
    // массив с информацией о файле
        data;

    // Чистим контейнер с таблицей
    output.innerHTML = '';

    // Вставляем в таблицу её тело
    table.appendChild(tbody);
    // Определяем заголовок таблицы (Названия колонок)
    tbody.innerHTML =
        "<tr><td>Имя</td><td>Размер</td></tr>";

    // Перебираем все файлы в FileList'е
    for(var i = 0; i < files.length; i++) {
        file = files[i];
        // Если в файле содержится изображение

            // то вместо превью выводим соответствующую надпись
            data = [file.name, file.size];
            appendFileInfo(tbody, data);

    }
    // помещаем таблицу с информацией о файле в div
    output.appendChild(table);
}

// проверяем поддерживает ли браузер file API
if(window.File && window.FileReader && window.FileList && window.Blob) {
    // если да, то как только страница загрузится
    onload = function () {
        // вешаем обработчик события, срабатывающий при изменении input'а
        document.querySelector('input').addEventListener('change', onFilesSelect, false);
    }
// если нет, то предупреждаем, что демо работать не будет
} else {
    alert('К сожалению ваш браузер не поддерживает file API');
}