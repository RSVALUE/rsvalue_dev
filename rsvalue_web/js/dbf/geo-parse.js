/**
 * Created by Ageichick on 10.06.2016.
 */
var filesArray = [];
var namesArray = [];
var name = "";
function set_name(name_file){
    name = name_file;
}
var layerJSONCODE=[];
var num = 0;
var ind = 0;
var ADD_ARR = 0;

var parser = new ol.format.GeoJSON({defaultDataProjection: 'EPSG:4326'});

function setFilesArrayLayers(namesArray) {
    //console.log(namesArray.length);
    //var n = namesArray.length;
    /*console.log(n);
     layersArray[n++] = filesArray;
     if (n == 5) {
     console.log(layersArray);
     }*/
    parse(num,namesArray);

    function parse(num,namesArray) {
        if (num<namesArray.length) {
            next(num,namesArray);
            console.log('num', num);
        } else{
            httpServices.ValideLayerValue(layerJSONCODE);
        }
    }

    function next(num,namesArray) {

        if(num<namesArray.length) {
            ind = num;
        }

        shapefile = new Shapefile(
            {
                shp: namesArray[ind]['shp'],
                dbf: namesArray[ind]['dbf'],
                layer: namesArray[ind]['layer'],
                layerNum: ind,
                total: namesArray.length
            },
            function (data) {
                //callback(data);
                var name = data.layer;
                console.log(name)
                var geo = JSON.stringify(data.geojson);
                var dwd_id = getDwdByJSON();
                //console.log(data); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                httpServices.sendLayer(geo, dwd_id, name);
/*
                if(ADD_ARR == 1) {*/
                    for (var i = 0; i < LAYERS.length; i++) {
                        if (LAYERS[i].layerName == data.layer) {
                            var layer =
                            {
                                analyticCode: "",
                                code: []
                            };
                            layer.analyticCode = LAYERS[i].analyticCode;
                            if (layer.analyticCode == 30 || layer.analyticCode == 120) {
                                var codes = [];
                                for (var j = 0; j < data.dbf.records.length; j++) {
                                    var record = data.dbf.records[j];
                                    for (var key in record) {
                                        switch (key.toLowerCase()) {
                                            case 'code':
                                                codes.push(record[key]);
                                                break;
                                        }
                                    }
                                }
                                layer.code = codes;
                            } else {
                                layer.code = [];
                            }
                            layerJSONCODE.push(layer)
                        }
                    }
                    num++;
                    parse(num, namesArray);
                /*}
                else if(ADD_ARR == -1){

                }*/
            }

        );
    }

    /*for (var i = 0; i<(namesArray.length);i++){
     parse(i);
     }*/


    //function parse(filesArray,callback) {
    //console.log(namesArray);
    //console.log(filesArray);


    //}

}

function setFilesArray(namesArray,filesArray,input) {
    //console.log(namesArray,filesArray, input);
    //alert("setFilesArray")
    openModal();

    shapefile = new Shapefile(
        {
            shp: filesArray['shp'],
            dbf: filesArray['dbf']
        },

        function (data) {

            /*var features = parser.readFeatures(data.geojson, {
                dataProjection: 'EPSG:4326',
                featureProjection: 'EPSG:3857'
            });*/

            /*if (input==1) {
                var dataZones = data;
                console.log('dataZones',dataZones);
            } else if (input==2) {
                var dataBounds = data;
                console.log('dataBounds',dataBounds);
            } else if (input==3){
                var dateThems = data;
                console.log('dataThems',dateThems);
            }*/

            var reg_cost_d=/Cost_d/gi, reg_cost_r=/Cost_r/gi,
                cost_r="", cost_d="";
            for(var i= 0, k=0; i< data.dbf.fields.length || k< data.dbf.fields.length; i++, k++){
                if((data.dbf.fields[i].name).match(reg_cost_d)){
                    cost_d=data.dbf.fields[i].name;
                    k=data.dbf.fields.length;
                }
                if((data.dbf.fields[i].name).match(reg_cost_r)){
                    cost_r=data.dbf.fields[i].name;
                    i=data.dbf.fields.length;
                }
            }

            var name;
            if (input==1) {
                var json=[], names=[], records = data.dbf.records, costd = cost_d.toLowerCase(), costr=cost_r.toLowerCase();

                for(var i=0; i<records.length; i++) {
                    zone={
                        'num':'',
                        'area':'',
                        'objectnumb':'',
                        'name':'',
                        'cost_d':'',
                        'cost_r':'',
                        'soato':'',
                        'category':'',
                        'ss':'',
                        'distr':'',
                        'stname':'',
                        'unp':'',
                        'npname':'',
                        'regn_ss':''
                    };
                    var record = records[i];
                    for (var key in record) {
                        switch (key.toLowerCase()){
                            case 'num': zone['num'] = record[key]; break;
                            case 'area': zone['area'] = record[key]; break;
                            case 'objectnumb': zone['objectnumb'] = record[key]; break;
                            case 'name': zone['name'] = record[key]; break;
                            case costd: zone['cost_d'] = record[key]; break;
                            case costr: zone['cost_r'] = record[key]; break;
                            case 'soato': zone['soato'] = record[key]; break;
                            case 'category': zone['category'] = record[key]; break;
                            case 'ss': zone['ss'] = record[key]; break;
                            case 'distr': zone['distr'] = record[key]; break;
                            case 'stname': zone['stname'] = record[key]; break;
                            case 'unp': zone['unp'] = record[key]; break;
                            case 'npname': zone['npname'] = record[key]; break;
                            case 'regn_ss': zone['regn_ss'] = record[key]; break;
                        }
                    }
                    json.push(zone)
                }

                /*for(var i=0; i<data.dbf.records.length; i++) {
                    zone = {
                        num: (data.dbf.records[i])['Num'],
                        area: (data.dbf.records[i])['Area'],
                        cost_d: (data.dbf.records[i])[cost_d],
                        cost_r: (data.dbf.records[i])[cost_r],
                        objectnumb: (data.dbf.records[i])['Objectnumb'],
                        name: (data.dbf.records[i])['Name'],
                        soato: (data.dbf.records[i])['Soato'],
                        category: (data.dbf.records[i])['Category'],
                        ss: (data.dbf.records[i])['Ss'],
                        distr: (data.dbf.records[i])['Distr'],
                        stname: (data.dbf.records[i])['Stname'],
                        unp: (data.dbf.records[i])['Unp'],
                        npname: (data.dbf.records[i])['Npname'],
                        regn_ss: (data.dbf.records[i])['Regn_ss']
                    };
                    json.push(zone);
                }*/


                for(var i=0; i<data.dbf.fields.length; i++) {
                    name = (data.dbf.fields[i]).name;
                   names.push(name)
                }

                for(var i=0; i<data.geojson.features.length; i++){
                    delete (data.geojson.features[i])["properties"];
                    (data.geojson.features[i])["properties"] = json[i];
                }

                var vv = JSON.stringify(data.dbf);
                var zones = JSON.stringify(json);
                get_json().zoneList = zones;

                //alert(6546546546546)

                httpServices.validateFieldsZone(names)
                if(Zone_map) {
                    //alert("Fields")
                    httpServices.validateValuesZone(json)
                    if(Zone_map) {
                        //alert("Values")
                        httpServices.sendGeojson(JSON.stringify(data.geojson), "zone")
                        if(Zone_map) {
                            //alert("send")
                            httpServices.returnGeojson(vv, "zone")
                        } if(Zone_map){
                            //alert("utf")
                            addtoTableDialog_3(data.dbf.records.length, "zone-dialog");
                        }
                    }
                }
                //console.log(decodeURI(encodeURIComponent(json[0].name)))

                //addToTableDocInfoHead(zones_arr, input);
                //addToTableDocInfoBody(data, input);

            } else if (input==2) {
                var json=[], names=[], records = data.dbf.records, costd = cost_d.toLowerCase(), costr=cost_r.toLowerCase();
                for(var i=0; i<records.length; i++) {
                    var bound={
                        'block_numb':'',
                        'cadnum':'',
                        'num':'',
                        'sq':'',
                        'cost_d':'',
                        'cost_r':'',
                        'address':'',
                        'soato':'',
                        'purpose':'',
                        'purpose2':''
                    };
                    var record = records[i];
                    for (var key in record) {
                        switch (key.toLowerCase()){
                            case 'block_numb': bound['block_numb'] = record[key]; break;
                            case 'cadnum': bound['cadnum'] = record[key]; break;
                            case 'num': bound['num'] = record[key]; break;
                            case 'sq': bound['sq'] = record[key]; break;
                            case costd: bound['cost_d'] = record[key]; break;
                            case costr: bound['cost_r'] = record[key]; break;
                            case 'address': bound['address'] = record[key]; break;
                            case 'soato': bound['soato'] = record[key]; break;
                            case 'purpose': bound['purpose'] = record[key]; break;
                            case 'purpose2': bound['purpose2'] = record[key]; break;
                        }
                    }
                    json.push(bound)
                }


                /*for(var i=0; i<data.dbf.records.length; i++) {
                    bound= {
                        block_numb: (data.dbf.records[i])['BLOCK_NUMB'],
                        cadnum: (data.dbf.records[i])['CADNUM'],
                        num: (data.dbf.records[i])['Num'],
                        sq: (data.dbf.records[i])['SQ'],
                        cost_d: (data.dbf.records[i])[cost_d],
                        cost_r: (data.dbf.records[i])[cost_r],
                        address: (data.dbf.records[i])['ADDRESS'],
                        soato: (data.dbf.records[i])['SOATO'],
                        purpose: (data.dbf.records[i])['PURPOSE'],
                        purpose2: (data.dbf.records[i])['PURPOSE2']
                    };

                    json.push(bound);
                }*/

                for(var i=0; i<data.dbf.fields.length; i++) {
                    name = (data.dbf.fields[i]).name;
                    names.push(name)
                }

                for(var i=0; i<data.geojson.features.length; i++){
                    delete (data.geojson.features[i])["properties"];
                    (data.geojson.features[i])["properties"] = json[i];
                }


                var bounds = JSON.stringify(json);
                get_json().boundList = bounds;
                var vv = JSON.stringify(data.dbf);

                httpServices.validateFieldsBounds(JSON.stringify(names))
                if(Bound_map) {
                    httpServices.validateValuesBounds(bounds)
                    if(Bound_map) {
                        httpServices.sendGeojson(JSON.stringify(data.geojson), "bound")
                        if(Bound_map) {
                            httpServices.returnGeojson(vv, "bound")
                        }
                        if(Bound_map){
                            addtoTableDialog_3(data.dbf.records.length, "bound-dialog");
                        }
                    }
                }

               /* addToTableDocInfoHead(data, input);
                addToTableDocInfoBody(data, input);*/

            } else if (input==3){
                console.log(namesArray)
                console.log(data)
            }


            if(input==3){
                addtoTableThemsDialog_3(data.dbf.records.length, "thems-dialog")
            } else if (input == 2 && Bound_map){
            //    addtoTableDialog_3(data.dbf.records.length, "bound-dialog");
            }else if (input == 1 && Zone_map){
             //   addtoTableDialog_3(data.dbf.records.length, "zone-dialog");
            }

            closeModal();
        });
}