﻿/**
 * Created by Ageichick on 10.06.2016.
 */
var myApp;
myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        }

    };
})();



var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');
var popup = new ol.Overlay(({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    }
}));
closer.onclick = function () {
    popup.setPosition(undefined);
    closer.blur();
    return false;
};

var styleZonesVector = [
    new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(49, 159, 211, 0.1)'
        }),
        stroke: new ol.style.Stroke({
            color: '#319FD3',
            width: 1
        }),
        text: new ol.style.Text({
            font: '12px Calibri,sans-serif',
            fill: new ol.style.Fill({
                color: '#000'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 3
            })
        })
    }),
    new ol.style.Style({
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: '#319FD3'
            })
        })
    })
];
var styleBoundsVector = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(249, 155, 132, 0.4)'
    }),
    stroke: new ol.style.Stroke({
        color: '#F99B84',
        width: 1
    }),
    text: new ol.style.Text({
        font: '12px Calibri,sans-serif',
        fill: new ol.style.Fill({
            color: '#000'
        }),
        stroke: new ol.style.Stroke({
            color: '#fff',
            width: 3
        })
    })
});

/*var zonesLayer = new ol.layer.Vector();
 var boundsLayer = new ol.layer.Vector();*/


//var extent,zonesSource,boundsSource,boundsLayer,zonesLayer;

var map = new ol.Map({
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        })//,
        //boundsLayer,
        //zonesLayer
    ],
    overlays: [popup],
    target: 'map',
    controls: ol.control.defaults({
        attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
            collapsible: false
        })
    }),
    view: new ol.View({
        center: [3100000, 7100000],
        zoom: 6.3,
        //projection: 'EPSG:4326',
        projection: 'EPSG:3857'
    })
});


map.addOverlay(popup);


map.on('singleclick', function (evt) {
    var coordinate = evt.coordinate;

    map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {

        //console.log(feature);

        var prop = feature.getProperties();
        if (feature) {
            var proptxt = '';
            content.innerHTML = '<p>Параметры:</p>';
            for (var key in prop) {
                proptxt = proptxt + '' + key + ' = ' + prop[key] + '</br>';
                console.log(proptxt);
            }
            content.innerHTML = '<p>Параметры:</p>' + proptxt;
            popup.setPosition(coordinate);
        }

    });


    //var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326'));

    //var prettyCoord = ol.coordinate.toStringHDMS(ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326'), 2);

    //popup.show(evt.coordinate, '<div><h2>Coordinates</h2><p>' + coordinate + '</p></div>');
});


function loadMap(uploadId,Zmap,Bmap,Lmap) {
    var startTimeZones,endTimeZones,endTimeBounds;
    startTimeZones = Math.round(new Date().getTime());


    console.log(startTimeZones);
    var cql_filter = '';
    //cql_filter += 'dwd_id%3D' + 992;
    cql_filter += 'dwd_id%3D' + uploadId;


    $.ajax({
        url: 'http://gisserver.nca.by:8080/geoserver/RB/wfs?cql_filter=' + cql_filter,
        type: 'GET',
        data: {
            service: 'WFS',
            version: '1.1.0',
            request: 'GetFeature',
            typename: 'RB:zones_preview',
            outputFormat: 'text/javascript',
            srsname: 'EPSG:3857'
            //bbox: extent.join(',') + ',EPSG:3857'
        },
        dataType: 'jsonp',
        jsonpCallback:'callback:loadFeaturesZones',
        jsonp: 'format_options'
    });

    zonesSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        strategy: ol.loadingstrategy.bbox
    });

    loadFeaturesZones = function (data) {
        zonesSource.clear(true);
        $("#loaded-preview-zones").show();
        $("#loading-preview-zones").hide();

        console.log('loadFeaturesZones');
        endTimeZones = Math.round(new Date().getTime());

        console.log(endTimeZones);
        setTimeout(function () {
            map.updateSize();

            var extent = zonesLayer.getSource().getExtent();
            map.getView().fit(extent, map.getSize());
        },(endTimeZones-startTimeZones)+2000);

        map.addLayer(zonesLayer);
        /*var extent = zonesLayer.getSource().getExtent();
        map.getView().fit(extent, map.getSize());*/


        /*setTimeout(function () {
            map.updateSize();
            map.addLayer(zonesLayer);
            map.addLayer(boundsLayer);
            var extent = zonesLayer.getSource().getExtent();
            map.getView().fit(extent, map.getSize());

        }, 1000);*/

        zonesSource.addFeatures(new ol.format.GeoJSON().readFeatures(data));
    };

    zonesLayer = new ol.layer.Vector({
        source: zonesSource,
        style: styleZonesVector
    });

    if(Bmap == true) {
        $.ajax({
            url: 'http://gisserver.nca.by:8080/geoserver/RB/wfs?cql_filter=' + cql_filter,
            type: 'GET',
            data: {
                service: 'WFS',
                version: '1.1.0',
                request: 'GetFeature',
                typename: 'RB:bounds_preview',
                srsname: 'EPSG:3857',
                outputFormat: 'text/javascript'
            },
            dataType: 'jsonp',
            jsonpCallback: 'callback:loadFeaturesBounds',
            jsonp: 'format_options'
        });

        boundsSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            strategy: ol.loadingstrategy.bbox
        });

        loadFeaturesBounds = function (data) {
            boundsSource.clear(true);
            $("#loaded-preview-bounds").show();
            $("#loading-preview-bounds").hide();

            console.log('loadFeaturesBounds');
            endTimeBounds = Math.round(new Date().getTime());
            console.log(endTimeBounds);


            map.addLayer(boundsLayer);
            boundsSource.addFeatures(new ol.format.GeoJSON().readFeatures(data));
        };

        boundsLayer = new ol.layer.Vector({
            source: boundsSource,
            style: styleBoundsVector
        });
    }




    /*setTimeout(function () {
        map.updateSize();
        map.addLayer(zonesLayer);
        map.addLayer(boundsLayer);
        var extent = zonesLayer.getSource().getExtent();
        map.getView().fit(extent, map.getSize());

    }, 1000);*/

}
function clearMap(Zmap,Bmap) {
    /*zonesLayer.getSource().clear();
     boundsLayer.getSource().clear();*/
    map.removeLayer(zonesLayer);
    if (Bmap == true) {
        map.removeLayer(boundsLayer);
    }
};