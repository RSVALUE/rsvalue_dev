/**
 * Created by Ageichick on 10.06.2016.
 */



var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');
var popup = new ol.Overlay(({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    }
}));
closer.onclick = function() {
 popup.setPosition(undefined);
 closer.blur();
 return false;
 };

var styleVector = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(49, 159, 211, 0.1)'
    }),
    stroke: new ol.style.Stroke({
        color: '#319FD3',
        width: 1
    }),
    text: new ol.style.Text({
        font: '12px Calibri,sans-serif',
        fill: new ol.style.Fill({
            color: '#000'
        }),
        stroke: new ol.style.Stroke({
            color: '#fff',
            width: 3
        })
    })
});

var vectorSource = new ol.source.Vector({});
//var vectorSource = new ol.source.GeoJSON({});
var vectorLayer = new ol.layer.Vector({
    source:	vectorSource,
    style: styleVector
});
var parser = new ol.format.GeoJSON({defaultDataProjection: 'EPSG:4326'});


var map = new ol.Map({
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        })//,
        //vectorLayer
    ],
    overlays: [popup],
    target: 'map',
    controls: ol.control.defaults({
        attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
            collapsible: false
        })
    }),
    view: new ol.View({
        center: [0, 0],
        //projection: 'EPSG:4326',
        projection: 'EPSG:3857',
        zoom: 2
    })
});
var filesArray = [];


function setFilesArray(filesArray) {

    shapefile = new Shapefile(
        {
            shp: filesArray['shp'],
            dbf: filesArray['dbf']
            /*shp: "data/ATE_DISTR_utf8.shp",
             dbf: "data/ATE_DISTR_utf8.dbf"*/
        },
        //opts,
        function (data) {
            //$('.nav-tabs a[href="#map-upload-preview"]').tab('show')
            /*data.geojson.src = {
             'type': 'name',
             'properties': {
             'name': 'EPSG:3857'
             //'name': 'EPSG:4326'
             }
             };*/


            var features = parser.readFeatures(data.geojson, {
                dataProjection: 'EPSG:4326',
                featureProjection: 'EPSG:3857'
            });


            vectorSource.addFeatures(features);
            vectorSource.addFeatures(features);


            var extent = vectorLayer.getSource().getExtent();
            //console.log(extent);
            map.getView().fit(extent, map.getSize());

            console.log(data);
            //console.log(extent);
        });
}
map.addLayer(vectorLayer);
map.addOverlay(popup);

map.on('singleclick', function(evt) {
    var coordinate = evt.coordinate;

    map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {

        console.log(feature);

        var prop = feature.getProperties();
        if (feature) {
            var proptxt = '';
            content.innerHTML = '<p>Параметры:</p>';
            for (var key in prop) {
                proptxt = proptxt + '' + key + ' = ' + prop[key] + '</br>';
                console.log(proptxt);
            }
            content.innerHTML = '<p>Параметры:</p>' + proptxt;
            popup.setPosition(coordinate);
        }

    });


    //var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326'));

    //var prettyCoord = ol.coordinate.toStringHDMS(ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326'), 2);

    //popup.show(evt.coordinate, '<div><h2>Coordinates</h2><p>' + coordinate + '</p></div>');
});
$('#map-upload-preview').click(
    function() {
        console.log(1);
        map.updateSize();
    }
)