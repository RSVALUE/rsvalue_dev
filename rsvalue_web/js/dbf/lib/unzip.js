var requestFileSystem = window.webkitRequestFileSystem || window.mozRequestFileSystem || window.requestFileSystem;
var model = (function() {
    var URL = window.webkitURL || window.mozURL || window.URL;

    return {
        getEntries : function(file, onend) {
            zip.createReader(new zip.BlobReader(file), function(zipReader) {
                zipReader.getEntries(onend);
            }, onerror);
        },
        getEntryFile : function(entry, creationMethod, onend, onprogress) {
            var writer, zipFileEntry;

            function getData() {
                entry.getData(writer, function(blob) {
                    var blobURL = creationMethod == "Blob" ? URL.createObjectURL(blob) : zipFileEntry.toURL();
                    onend(blobURL);
                }, onprogress);
            }

            if (creationMethod == "Blob") {
                writer = new zip.BlobWriter();
                getData();
            } else {
                createTempFile(function(fileEntry) {
                    zipFileEntry = fileEntry;
                    writer = new zip.FileWriter(zipFileEntry);
                    getData();
                });
            }
        }
    };
})();

function onerror(message) {
    alert(message);
}

function createTempFile(callback) {
    var tmpFilename = "tmp.dat";
    requestFileSystem(TEMPORARY, 4 * 1024 * 1024 * 1024, function(filesystem) {
        function create() {
            filesystem.root.getFile(tmpFilename, {
                create : true
            }, function(zipFile) {
                callback(zipFile);
            });
        }

        filesystem.root.getFile(tmpFilename, null, function(entry) {
            entry.remove(create, create);
        }, create);
    });
}

var unzipProgress = document.createElement("progress");
//var creationMethodInput = document.getElementById("creation-method-input");
var creationMethodInputValue = 'Blob';
//console.log(creationMethodInputValue);

function download(entry, totalFiles, input) {
    //console.log(input);
    model.getEntryFile(entry, creationMethodInputValue, function(blobURL) {
        var fileType = entry.filename.slice(-3);
        var fileName = entry.filename;
        filesArray[fileType] = blobURL;
        namesArray[fileType] = fileName;

        //console.log(filesArray);
        /*var clickEvent = document.createEvent("MouseEvent");
         if (unzipProgress.parentNode)
         unzipProgress.parentNode.removeChild(unzipProgress);
         unzipProgress.value = 0;
         unzipProgress.max = 0;
         clickEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

         a.href = blobURL;
         a.download = entry.filename;*/
        //a.dispatchEvent(clickEvent);
        //console.log(filesArray);

       // alert(totalFiles)
      //  alert(Object.keys(filesArray).length)

        //if (Object.keys(filesArray).length == totalFiles)  {

            setFilesArray(entry.filename,filesArray,input);
            filesArray=[];
       // }

    }, function(current, total) {
        unzipProgress.value = current;
        unzipProgress.max = total;

    });

}

var inx=0;
function download2(entry, numFile, n) {
    // console.log('input',input);
    // console.log('totalFiles',totalFiles);
    model.getEntryFile(entry, creationMethodInputValue, function(blobURL) {
        var fileType = entry.filename.slice(-3);
        var fileName = entry.filename;
        filesArray[fileType] = blobURL;
        namesArray[fileType] = fileName;

        //console.log('filesArray',filesArray);
        /*var clickEvent = document.createEvent("MouseEvent");
         if (unzipProgress.parentNode)
         unzipProgress.parentNode.removeChild(unzipProgress);
         unzipProgress.value = 0;
         unzipProgress.max = 0;
         clickEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

         a.href = blobURL;
         a.download = entry.filename;*/

        //a.dispatchEvent(clickEvent);
        //console.log('Object.keys(filesArray).length',Object.keys(filesArray).length);

        if (numFile == 1) {
            var arrF = (fileName.split('/'));
            filesArray['layer'] = arrF[arrF.length-1];
            //console.log('filesArray',filesArray);
            namesArray[inx++] = filesArray;
            //console.log('namesArray',namesArray);
            filesArray=[];
        }
        if (n == false) {
            setFilesArrayLayers(namesArray);
        }
    }, function(current, total) {
        unzipProgress.value = current;
        unzipProgress.max = total;

    });
}
