/**
 * Created by Shcherbina on 22.07.2016.
 */
var ID_VIEW;
var ID_FUN;

var view_scope=[];

var zone_enam_obj = {
    1:"num",
    2:"distr",
    3:"name",
    4:"cost_d",
    5:"cost_r",
    6:"objectnumb",
    7:"area",
    8:"category",
    9:"npname",
    10:"unp"
}, zone_enam_header = {
    1:"Номер оценочной зоны",
    2:"distr",
    3:"Наименование оценочной зоны",
    4:"Стоимость (долл. США)",
    5:"Стоимость (бел. руб)",
    6:"objectnumb",
    7:"area",
    8:"category",
    9:"npname",
    10:"unp"
};

function setView(){
    $("#num-load-view").append(view_scope.info_obj.regNum);
    $("#date-load-view").append(view_scope.info_obj.dateRegStart);
    $("#date-value-view").append(view_scope.info_obj.dateVal);
    $("#obj-val-view").append(view_scope.info_obj.ateName);
    $("#type-value-view").append(view_scope.info_obj.costTypeName);
    $("#result-view").append(view_scope.info_obj.decNum);
    $("#org-val-view").append(view_scope.info_obj.valOrganization);
    $("#fun-val-view").append(view_scope.info_obj.funcCodeName);
    $("#method-val-view").append(view_scope.info_obj.methodTypeName);
    $("#type-money-view").append(view_scope.info_obj.currencyName);
    $("#organ-name-view").append(view_scope.info_obj.decOrganization);
    $("#result-view-date").append(view_scope.info_obj.decDate);
    $("#note-value-view").append(function(){
        if(view_scope.info_obj.remark==null)
            return "нет"
        else return view_scope.info_obj.remark;
    });

    $("#table-doc-view").append(function(){
        var tbody_content = "";
        $.each(view_scope.doc_obj, function (i, obj) {
            tbody_content += openTRID(obj.id) +
                tdText(i+1) +
                tdText(obj.typeName) +
                tdText(obj.name) +
                tdText(obj.size) +
                tdText(obj.loadDate) +
                "<td>" + dwn_load(obj.path) + "</td>"+
                closeTR();
        });
        return tbody_content;
    });

    $("#table-thems-view").append(function(){
        var tbody_content = "";

        $.each(view_scope.layers, function (i, obj) {
            tbody_content += openTRID(obj.id) +
                tdText(i+1) +
                tdText(obj.name) +
                tdText(obj.startDate) +
                closeTR();
        });
        return tbody_content;
    });

    $("#header-view-zones").append(function(){
        /*var header = "";
        for(var i=0; i<zones_header.length; i++) {
            header += td_sort(zones_header[i], zone_enam_header[zones_header[i]]);
        };
        console.log(header)
        return header;*/
        return td_simple("Номер оценочной зоны")
            + td_simple("Наименование оценочной зоны")
            + td_simple("Стоимость (долл. США)")
            + td_simple("Стоимость (бел. руб)");
    });

    addZonesBody();
}

function cleanView(){
    $("#num-load-view").empty();
    $("#date-load-view").empty();
    $("#date-value-view").empty();
    $("#obj-val-view").empty();
    $("#type-value-view").empty();
    $("#result-view").empty();
    $("#org-val-view").empty();
    $("#fun-val-view").empty();
    $("#method-val-view").empty();
    $("#note-value-view").empty();
    $("#type-money-view").empty();
    $("#table-doc-view").empty();
    $("#table-thems-view").empty();
    $("#table-val-view").empty();
    $("#header-view-zones").empty();
    $("#organ-name-view").empty();
    $("#result-view-date").empty();
    $("#header-view-zones").append("<td style='width: 8%;'>№ п/п</span></td>");
}

function setViewListeners(){
    for(var i=0; i<zones_header.length; i++){
        document.getElementById(zones_header[i] + "-sort-view").addEventListener('click', sort_zones_view, false);
    }
}

function sort_zones_view(e){
    var id = $(this).attr("id").split('-')[0],
        predicate="desc";
    if($(this).hasClass("glyphicon-sort")) {
        predicate="asc";
        $(this).removeClass("glyphicon-sort");
        $(this).addClass("glyphicon-chevron-up");
    } else if($(this).hasClass("glyphicon-chevron-up")) {
        predicate="desc";
        $(this).addClass("glyphicon-chevron-down");
        $(this).removeClass("glyphicon-chevron-up");
    } else if($(this).hasClass("glyphicon-chevron-down")){
        predicate="asc";
        $(this).addClass("glyphicon-sort");
        $(this).removeClass("glyphicon-chevron-up");
    }
    httpServices.getZonesForEditView(ID_VIEW, ID_FUN, view_scope);
    addZonesBody();
}

function addZonesBody(){
    $("#table-val-view").empty();
    $("#table-val-view").append(function(){
        var tbody = "";
        console.log(view_scope.zonesView)
        for (var i=0; i<view_scope.zonesView.length; i++) {
            var obj = view_scope.zonesView[i];
            console.log(obj)
            tbody += openTR()
                + tdText(i + 1)
                + tdText(obj[0])
                + tdText(obj[1])
                + tdText(obj[2])
                + tdText(obj[3])
                + closeTR();
        }
        return tbody;
    });
}