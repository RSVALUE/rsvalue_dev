/**
 * Created by Shcherbina on 22.07.2016.
 */

var ID_EDIT;
var edit_scope=[];
var N_DOC = 1;
var edit_docs_unimportand = [];
var edit_del_docs = [], edit_added_docs = [], edit_del_docs_important = 0, edit_added_docs_important = 0;
var dic_info_edit = {
    costTypeId: -1,
    funcCode: -1,
    ateId: -1,
    docTypeId: -1,
    docNum: 1
}

function setEdit(){
    console.log(edit_scope.info_obj)
    $("#num-load-edit").append(edit_scope.info_obj.regNum);
    $("#date-load-edit").append(edit_scope.info_obj.dateRegStart);
    $("#date-value-edit").append(edit_scope.info_obj.dateVal);
    $("#obj-val-edit").append(edit_scope.info_obj.ateName);
    $("#type-value-edit").append(edit_scope.info_obj.costTypeName);
    $("#result-edit").append(edit_scope.info_obj.decNum);
    $("#org-val-edit").append(edit_scope.info_obj.valOrganization);
    $("#fun-val-edit").append(edit_scope.info_obj.funcCodeName);
    $("#method-val-edit").append(edit_scope.info_obj.methodTypeName);

    $("#result-date-edit").append(edit_scope.info_obj.decDate);
    $("#org-name-edit").append(edit_scope.info_obj.decOrganization);

    $("#type-money-edit").append(edit_scope.info_obj.currencyName);
    $("#note-value-edit").append(function(){
        if(edit_scope.info_obj.remark==null)
            return "нет";
        else return edit_scope.info_obj.remark;
    });

    $("#table-doc-edit").append(function(){
        var tbody_content = "";
            $.each(edit_scope.doc_obj, function (i, obj) {
                console.log(obj)
                if(obj.need) {
                    obj.typeName += "*";
                }
                if(obj.typeId != 1310 && obj.typeId != 1311) {
                    tbody_content += openTRID(obj.id + "-" + obj.typeId) +
                        tdText(N_DOC) +
                        tdTextId(obj.typeName, obj.typeId) +
                        tdTextId(obj.name, obj.typeId+"-name") +
                        tdTextId(obj.size, obj.typeId+"-size") +
                        tdTextId(obj.loadDate, + obj.typeId+"-date") +
                        "<td>" + remove_edit(obj.id + "-" + obj.typeId, "edit", obj.path) + "</td>" +
                        closeTR();
                    N_DOC += 1;
                }
            });
        return tbody_content;
    });

    $("#table-thems-edit").append(function(){
        var tbody_content = "";
        $.each(edit_scope.layers, function (i, obj) {
            tbody_content += openTRID(obj.id) +
                tdText(i+1) +
                tdText(obj.name) +
                tdText(obj.startDate) +
                closeTR();
        });
        return tbody_content;
    });

    $("#dialog-after-table-edit").append(function(){
        return plus_edit("add-after-table-edit");
    });

    console.log(edit_scope.doc_no_imp)

    $("#doc-div").append(function(){
        var docs = "";
        $.each(edit_scope.doc_no_imp, function(i, obj) {
            if(obj!=null) {
                docs += divOpen_12() + addButtonFile(obj.analyticCode + "-0-doc", obj.codeName) + divClose();
            }
        });
        return docs;
    });

    $.each(edit_scope.doc_no_imp, function (i, obj) {
        document.getElementById(obj.analyticCode + "-0-doc").addEventListener('change', onFilesSelectDocEdit, false);
        document.getElementById(obj.analyticCode + "-0-doc").addEventListener('click', clickDoc, false);
    });
}

function cleanEdit(){
    $("#num-load-edit").empty();
    $("#date-load-edit").empty();
    $("#date-value-edit").empty();
    $("#obj-val-edit").empty();
    $("#type-value-edit").empty();
    $("#result-edit").empty();
    $("#org-val-edit").empty();
    $("#fun-val-edit").empty();
    $("#method-val-edit").empty();
    $("#note-value-edit").empty();
    $("#type-money-edit").empty();
    $("#result-date-edit").empty();
    $("#org-name-edit").empty();

    $("#table-doc-edit").empty();
    $("#table-thems-edit").empty();

    $("#dialog-after-table-edit").empty();

    $("#doc-div").empty();
    $("#no-downloads").removeClass("edit");
    N_DOC = 1;
}

function isImportantDoc(id_num){
    console.log("isImportantDoc(id_num)")
    var importatnt_doc = ($("#" + (id_num.split("-"))[1]).html()).indexOf('*');
    if(importatnt_doc == -1){importatnt_doc = false;} else {importatnt_doc = true;}
    return importatnt_doc;
}

function edit_remove(e){
    var id_num = ($(this).attr("id")).split('_')[0];
    var id = ($(this).attr("id"));
    edit_del_docs.push((id_num.split('-'))[0])
    plus_del_doc_edit_view(id_num, "del");
    var id_tr = '#' + id.split('_')[0];
    //$(id_tr).remove();

    var importatnt_doc = isImportantDoc(id_num);

    if(importatnt_doc) {
        edit_del_docs_important +=1;
        $("#save-edit").attr("disabled","disabled");
        ($(id_tr).children()[2]).textContent = "";
        ($(id_tr).children()[3]).textContent = "";
        ($(id_tr).children()[4]).textContent = "";
    }
    else $(id_tr).remove()
}

function edit_download(e){
    var id = ($(this).attr("id"));
}

function edit_add(e){
    console.log("edit_add(e)")

    var arr_id = (($(this).attr("id")).split('_')[0]).split("-");
    var id_doc = arr_id[0];
    var id_doc_type = arr_id[1];
    var files = e.target.files, file;
    var id=id_doc+"-" + id_doc_type;

    var inp_doc = isImportantDoc(id)

    setDocNum_Edit(arr_id[0])

    var id_del = id_doc + "_del-edit"
    for (var i = 0; i < files.length; i++) {
        file = files[i];
        var reader = new FileReader();

        httpServices.sendDocFile_edit(file, ID_EDIT, id_doc_type, AUTHOBJ.userId, edit_scope);
        if(IS_ADD) {
            edit_added_docs.push(edit_scope.saveDocObj.id)
            $(id_del).addClass(ID_DEL + "");

            console.log($("#"+id_doc_type + "-name"))

            $("#"+id_doc_type + "-name").html(file.name);
            $("#"+id_doc_type + "-size").html(parseFloat((file.size / 1000).toFixed(1)));
            $("#"+id_doc_type + "-date").html(new Date().toLocaleDateString());
            plus_del_doc_edit_view(id, "add");
            IS_ADD = false;
            if(inp_doc){
                edit_added_docs_important += 1;
                if(edit_added_docs_important == edit_del_docs_important){
                    $("#save-edit").removeAttr("disabled");
                }
            }
        }
    }
    this.value = null;
}

function onFilesSelectDocEdit(e) {

    console.log("onFilesSelectDocEdit(e)")
    var title = $(this).prev().text();
    var id_arr = $(this).attr("id").split('-');

    console.log(id_arr)

    var files = e.target.files,
        row, fr, file, data; // строка с информацией о файле (Перезаписывается каждый шаг цикла), FileReader (Создаётся для каждого файла), объект file из FileList'a,  массив с информацией о файле

    //setDocNum(id_arr[0])

    for (var i = 0; i < files.length; i++) {

        file = files[i];

        var id_add = id_arr[0] + "-" + NUM_DOC + "-a";
        var id_del = id_arr[0] + "-" + NUM_DOC + "-d";

        httpServices.sendDocFile_edit(file, ID_EDIT, id_arr[0], AUTHOBJ.userId, edit_scope);

        if (IS_ADD) {
            $("#table-doc-edit").append(function () {
                var tbody_content = openTRID(id_arr[0]) +
                    tdText(N_DOC) +
                    tdText(title) +
                    tdText(file.name) +
                    tdText(file.size) +
                    tdText(new Date().toLocaleDateString()) +
                    "<td>" + remove_edit(id_arr[0] + "_" + i, "edit", file.name) + "</td>" +
                    closeTR();
                return tbody_content;
            });
            edit_added_docs.push(edit_scope.saveDocObj.id)
            N_DOC += 1;
            edit_docs_unimportand.push(id_arr[0] + "_" + i);

            IS_ADD = false;
            NUM_DOC += 1;
        }
    }

}

function setDocNum_Edit(id){

}

function setListenersForDocs() {
    console.log("setListenersForDocs()")
    var SIZE = edit_docs_unimportand.length;
    for (var i = 0; i < SIZE; i++) {
            document.getElementById(edit_docs_unimportand[i] + "_dwn-edit").addEventListener('click', edit_download, false);
            document.getElementById(edit_docs_unimportand[i] + "_del-edit").addEventListener('click', edit_remove, false);
            document.getElementById(edit_docs_unimportand[i] + "_add-edit").addEventListener('click', edit_add, false);
    }

    edit_docs_unimportand=[];
}

function setListeners() {
//ОБРАБОТЧИК С EVENT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    $.each(edit_scope.doc_obj, function (i, obj) {
        if(document.getElementById(obj.id + "-" + obj.typeId+"_dwn-edit") != null) {
            document.getElementById(obj.id + "-" + obj.typeId + "_dwn-edit").addEventListener('click', edit_download, false);
            document.getElementById(obj.id + "-" + obj.typeId + "_del-edit").addEventListener('click', edit_remove, false);
            //document.getElementById(obj.id+"_add-edit").addEventListener('click', edit_add, false);

            document.getElementById(obj.id + "-" + obj.typeId + "_add-edit").addEventListener('change', edit_add, false);
        }
    });
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}