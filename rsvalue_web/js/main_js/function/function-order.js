/**
 * Created by Shcherbina on 21.12.2016.
 */

var order_scope=[];
var searchPdf={
    "searchMethodValue":"",
    "data":{
        "address":"",
        "searchDate":"",
        "cadNum": "",
        "square":"",
        "purposeDic":"",
        "purposeGov":"",
        "communityGardenName":"",
        "nearLocality":"",
        "zoneName":"",
        "unp":"",
        "tenantName":"",
        "remark":""
    },
    "isNb":"",
    "valuesCharacters":[]
}


function changeSearchByAddrObl_order(){
    if($("#search-by-addr-2-order").hasClass("display-visible-2")) {
        $("#search-by-addr-2-order").removeClass("display-visible-2");
        $("#search-by-addr-2-order").addClass("display-none");
    }

    $("#search-raion-nas_p-num").empty();
    changeSearchByAddrRaion()
}

function changeSearchByAddrRaion(){
    if($("#search-by-addr-3-order").hasClass("display-visible-2")) {
        $("#search-by-addr-3-order").removeClass("display-visible-2");
        $("#search-by-addr-3-order").addClass("display-none");
    }

    $("#search-ss-nas_p-avto-num").empty();
    changeSearchByAddrSelSovet()
}

function changeSearchByAddrSelSovet(){
    if($("#search-by-addr-4").hasClass("display-visible-2")) {
        $("#search-by-addr-4").removeClass("display-visible-2");
        $("#search-by-addr-4").addClass("display-none");
    }

    $("#search-nas_p-BGD-num").empty();
    changeSearchByAddrBJD()
}

function changeSearchByAddrBJD(){
    if( $("#search-by-addr-5").hasClass("display-visible-2")) {
        $("#search-by-addr-5").removeClass("display-visible-2");
        $("#search-by-addr-5").addClass("display-none");
    }

    $("#search-yl").empty();
    changeSearchByAddrYlichnoDorojSet()
}

function changeSearchByAddrYlichnoDorojSet(){
    if($("#search-by-addr-6").hasClass("display-visible-2")) {
        $("#search-by-addr-6").removeClass("display-visible-2");
        $("#search-by-addr-6").addClass("display-none");
    }

    $("#9-search-by-addr-num-zdania-5").empty();
}



function closeClearSearchPoint(){
    if(!$("#res-search-addr-1-2").hasClass("display-none")){
        $("#res-search-addr-1-2").removeClass("display-visible");
        $("#res-search-addr-1-2").addClass("display-none");
    }
    if(!$("#res-search-addr-1-1").hasClass("display-none")){
        $("#res-search-addr-1-1").removeClass("display-visible");
        $("#res-search-addr-1-1").addClass("display-none");
    }
    clearDivMoreCad();
}

function append_2input_order(){
    change_2_el_order();
    var val = ($("#2-select-search-order").val());
    if(val != -1){
        order_scope.searchByAddr_2 = val;

        clearAddresEl_2_order();

        httpServices.getAddressData(order_scope, addres_order.region, addres_order.typeIn, addres_order.typeOut, addres_order.area, addres_order.ss, addres_order.snp, addres_order.bjd, addres_order.road, addres_order.km, addres_order.street);
        $("#2-select-search-data-order").append(function(){
            var select = option(-1, -1, "");
            var data = [];
            console.log(addres)
            if(addres_order.typeOut==2){
                data=order_scope.searchAddr_dataRegion;
            } else if(addres_order.typeOut==4){
                data=order_scope.searchAddr_dataNasPunkt;
            } else if(addres_order.typeOut==9){
                data=order_scope.searchAddr_dataNumZd;
            }else if( addres_order.typeOut==5) {
                data = order_scope.searchAddr_dataBGD;
            }else if( addres_order.typeOut==8) {
                data = order_scope.searchAddr_dataYlica;
            }
            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_3input(){
    change_3_el();
    var val = ($("#3-select-search-order").val());
    if(val != -1){

        order_scope.searchByAddr_3 = val;

        clearAddresEl_3();

        httpServices.getAddressData(order_scope, addres_order.region, addres_order.typeIn, addres_order.typeOut, addres_order.area, addres_order.ss, addres_order.snp, addres_order.bjd, addres_order.road, addres_order.km, addres_order.street);
        $("#3-select-search-data-order").append(function(){
            var select = option(-1, -1, "");
            var data = [];

            if( addres_order.typeOut==4) {
                data = order_scope.searchAddr_dataNasPunkt;
            }else if( addres_order.typeOut==9) {
                data = order_scope.searchAddr_dataNumZd;
            }else if( addres_order.typeOut==3) {
                data = order_scope.searchAddr_dataSelsovet;
            }else if( addres_order.typeOut==6) {
                data =  order_scope.searchAddr_dataAvtod;
            }else if( addres_order.typeOut==5) {
                data = order_scope.searchAddr_dataBGD;
            }else if( addres_order.typeOut==8) {
                data = order_scope.searchAddr_dataYlica;
            }
            /*else if( addres_order.typeOut==7) {
             data = order_scope.searchAddr_dataRasst;
             }*/

            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_4input(){
    change_4_el();
    var val = ($("#4-select-search").val());
    if(val != -1){
        order_scope.searchByAddr_4 = val;

        clearAddresEl_4();

        httpServices.getAddressData(order_scope, addres_order.region, addres_order.typeIn, addres_order.typeOut, addres_order.area, addres_order.ss, addres_order.snp, addres_order.bjd, addres_order.road, addres_order.km, addres_order.street);
        $("#4-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];

            if( addres_order.typeOut==4) {
                data = order_scope.searchAddr_dataNasPunkt;
            }else if( addres_order.typeOut==9) {
                data = order_scope.searchAddr_dataNumZd;
            }else if( addres_order.typeOut==3) {
                data = order_scope.searchAddr_dataSelsovet;
            }else if( addres_order.typeOut==6) {
                data =  order_scope.searchAddr_dataAvtod;
            }else if( addres_order.typeOut==5) {
                data = order_scope.searchAddr_dataBGD;
            }else if( addres_order.typeOut==8) {
                data = order_scope.searchAddr_dataYlica;
            }
            else if( addres_order.typeOut==7) {
                data = order_scope.searchAddr_dataRasst;
            }

            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_5input(){
    change_5_el();
    var val = ($("#5-select-search").val());
    if(val != -1){
        order_scope.searchByAddr_5 = val;

        clearAddresEl_5();

        httpServices.getAddressData(order_scope, addres_order.region, addres_order.typeIn, addres_order.typeOut, addres_order.area, addres_order.ss, addres_order.snp, addres_order.bjd, addres_order.road, addres_order.km, addres_order.street);
        $("#5-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];

            if( addres_order.typeOut==4) {
                data = order_scope.searchAddr_dataNasPunkt;
            }else if( addres_order.typeOut==9) {
                data = order_scope.searchAddr_dataNumZd;
            }else if( addres_order.typeOut==3) {
                data = order_scope.searchAddr_dataSelsovet;
            }else if( addres_order.typeOut==6) {
                data =  order_scope.searchAddr_dataAvtod;
            }else if( addres_order.typeOut==5) {
                data = order_scope.searchAddr_dataBGD;
            }else if( addres_order.typeOut==8) {
                data = order_scope.searchAddr_dataYlica;
            }
            else if( addres_order.typeOut==7) {
                data = order_scope.searchAddr_dataRasst;
            }

            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_6input(){
    change_6_el();
    var val = ($("#6-select-search").val());
    if(val != -1){
        order_scope.searchByAddr_6 = val;
        addres_order.typeIn = order_scope.searchByAddr_5;
        addres_order.typeOut = val;
        //alert(addres_order.street)
        httpServices.getAddressData(order_scope, addres_order.region, addres_order.typeIn, addres_order.typeOut, addres_order.area, addres_order.ss, addres_order.snp, addres_order.bjd, addres_order.road, addres_order.km, addres_order.street);
        $("#6-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];
            if( addres_order.typeOut==9) {
                data = order_scope.searchAddr_dataNumZd;
            }
            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

// ОЧИТКИ ПОИСК ПО АДРЕСУ!!!!!!!!!!!!
function change_6_el_order(){
    $("#6-select-search-data").empty();
    closeAddrSearch();
}

function change_5_el_order(){
    change_6_el_order();
    $("#5-select-search-data").empty();
    if(!$("#search-by-addr-6").addClass("display-none")) {
        $("#search-by-addr-6").addClass("display-none")
    }

}

function change_5_data_order(){
    $("#6-select-search-data").empty();
}

function change_4_el_order(){
    change_5_el_order();
    $("#4-select-search-data").empty();
    $("#5-select-search-data").empty();
    if(!$("#search-by-addr-5").addClass("display-none")) {
        $("#search-by-addr-5").addClass("display-none")
    }
}

function change_4_data_order(){
    change_5_data_order();
    if(!$("#search-by-addr-6").addClass("display-none")) {
        $("#search-by-addr-6").addClass("display-none")
    }
    $("#5-select-search").empty();
    $("#5-select-search-data").empty();
}

function change_3_el_order(){
    change_4_el_order();
    $("#3-select-search-data-order").empty();
    $("#4-select-search-data").empty();
    if(!$("#search-by-addr-4").addClass("display-none")) {
        $("#search-by-addr-4").addClass("display-none")
    }
}

function setAddresbyTypeOut(val, out){
    if( out==4) {
        addres_order.snp = val;
    }else if( out==3) {
        addres_order.ss = val;
    }else if( out==6) {
        addres_order.road = val;
    }else if( out==5) {
        addres_order.bjd = val;
    }else if( out==8) {
        addres_order.street = val;
    }else if( out==7) {
        addres_order.km = val;
    }else if( out==9) {
        addres_order.numDom = val;
    }else if( out==2) {
        addres_order.area = val;
    }
}
function clearAddresEl_5(){
    var val_O = $("#5-select-search").val(), val_I = $("#4-select-search").val(), val_reg = addres_order.region;
    addresInit();
    addres_order.typeOut = val_O;
    addres_order.typeIn = val_I;
    addres_order.region = val_reg;
    setAddresbyTypeOut($("#2-select-search-data-order").val(), $("#2-select-search-order").val());
    setAddresbyTypeOut($("#3-select-search-data-order").val(), $("#3-select-search-order").val());
    setAddresbyTypeOut($("#4-select-search-data").val(), $("#4-select-search").val());
}

function clearAddresEl_4(){
    var val_O = $("#4-select-search").val(), val_I = $("#3-select-search-order").val(), val_reg = addres_order.region;
    addresInit();
    addres_order.typeOut = val_O;
    addres_order.typeIn = val_I;
    addres_order.region = val_reg;
    setAddresbyTypeOut($("#2-select-search-data-order").val(), $("#2-select-search-order").val());
    setAddresbyTypeOut($("#3-select-search-data-order").val(), $("#3-select-search-order").val());
}

function clearAddresEl_3(){
    var val_O = $("#3-select-search-order").val(), val_I = $("#2-select-search-order").val(), val_reg = addres_order.region;
    addresInit();
    addres_order.typeOut = val_O;
    addres_order.typeIn = val_I;
    addres_order.region = val_reg;
    setAddresbyTypeOut($("#2-select-search-data-order").val(), $("#2-select-search-order").val())
}

function change_3_data_order(){
    change_4_data_order();
    if(!$("#search-by-addr-5").addClass("display-none")) {
        $("#search-by-addr-5").addClass("display-none")
    }
    $("#4-select-search").empty();
    $("#4-select-search-data").empty();
}

function change_2_el_order(){
    change_3_el_order();
    $("#2-select-search-data-order").empty();
    $("#3-select-search-data-order").empty();
    if(!$("#search-by-addr-3-order").addClass("display-none")) {
        $("#search-by-addr-3-order").addClass("display-none")
    }
}

function clearAddresEl_2_order(){
    var val_O = $("#2-select-search-order").val(), val_I = 1, val_reg = addres_order.region;
    addresInit();
    addres_order.typeOut = val_O;
    addres_order.typeIn = val_I;
    addres_order.region = val_reg;
}

function change_2_data_order(){
    change_3_data_order();
    if(!$("#search-by-addr-4-order").addClass("display-none")) {
        $("#search-by-addr-4-order").addClass("display-none")
    }
    $("#3-select-search-order").empty();
    $("#3-select-search-data-order").empty();
}

function change_1_el_order(){
    change_2_el_order();
    $("#1-select-search-data").empty();
    $("#2-select-search-data-order").empty();
    $("#1-search-by-addr-abl-select-order").val(0).change();
    if(!$("#search-by-addr-2-order").addClass("display-none")) {
        $("#search-by-addr-2-order").addClass("display-none")
    }
}

function change_1_data_order(){
    change_2_data_order();
    if(!$("#search-by-addr-3-order").addClass("display-none")) {
        $("#search-by-addr-3-order").addClass("display-none")
    }
    $("#2-select-search-order").empty();
    $("#2-select-search-data-order").empty();
}

function closeAddrSearch_order(){
    if(!$("#res-search-addr-1-1").hasClass("display-none")){
        $("#res-search-addr-1-1").addClass("display-none")
    }
    if(!$("#res-search-addr-1-2").hasClass("display-none")){
        $("#res-search-addr-1-2").addClass("display-none")
    }
    if(!$("#res-search-addr-1-3").hasClass("display-none")) {
        $("#res-search-addr-1-3").addClass("display-none")
    }

    $("#cad-num-by-addr-2").empty();
    $("#cad-num-by-addr-3").empty();
}