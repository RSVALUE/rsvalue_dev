/**
 * Created by Shcherbina on 30.05.2016.
 */
var valid_next_2 = true;

$(document).ready(function () {

    $(".mult-check-all").click(function () {
        var li = $(".mult-check").parent().parent();
        if (li.hasClass("selected")) {
            console.log($(this).val());
            $("#group-btn").removeClass("disabled");
        }
        else {
            $("#group-btn").addClass("disabled");
        }
    });

    //ЗАГРУЗКА РЕЗУЛЬТАТОВ

    $("#loading-data").click(function () {
        $("#load-modal-1").modal('show');
    });

    $("#next-load-1").click(function () {
        httpServices.validateLoadObject(get_json().costTypeId, get_json().objectNumber, get_json().funcCodeId, get_json().dateVal);
        if (valid_next_2) {
            $("#next-load-1").addClass("click-event")
            $("#load-modal-1").modal('hide')
        }
    });

    $("#cleanAll-1").click(function () {
        cleanAll();
        if (DELETE_ALL_DIALOG) {
            $('#load-modal-1').modal('hide')
        }
    })

    $("#next-load-2").click(function () {
        $("#next-load-2").addClass("click-event");
        //alert("123")
        desBounds();
    });

    $("#prev-load-2").click(function () {
        $("#prev-load-2").addClass("click-event");
    });

    $("#cleanAll-2").click(function () {
        cleanAll();
        if (DELETE_ALL_DIALOG) {
            $('#load-modal-2').modal('hide')
        }
    })

    $("#map-upload-preview").on('hidden.bs.modal', function () {
        if ($("#preview-btn-cls").hasClass("click-event")) {
            $("#load-modal-3").modal('show');
            $("#preview-btn-cls").removeClass("click-event");
        }
    });

    $("#preview-btn-cls").click(function () {
        clearMap(Zone_map, Bound_map);
        $("#loaded-preview-bounds").hide();
        $("#loaded-preview-zones").hide();
        $("#preview-btn-cls").addClass("click-event")
    });

    $("#preview-btn").click(function () {
        if (Zone_map == true) $("#loading-preview-zones").show();
        if (Bound_map == true) $("#loading-preview-bounds").show();

        var uploadId = getDwdByJSON(); //!!!!!идентификатор загрузки

        console.log(uploadId);
        loadMap(uploadId, Zone_map, Bound_map, Layer_map);
        $("#preview-btn").addClass("click-event")
    });

    $("#cleanAll-3").click(function () {
        cleanAll();
        if (DELETE_ALL_DIALOG) {
            $('#load-modal-3').modal('hide')
        }
    })

    $("#prev-load-3").click(function () {
        $("#prev-load-3").addClass("click-event");
    });

    $("#no-downloads").click(function () {
        if (!$("#add-after-table").hasClass("my-load-modal")) {
            $("#no-downloads").addClass("edit");
        }
    })


    $("#no-downloads-doc-info-zone").click(function () {
        $("#no-downloads-doc-info-zone").addClass("click-event");
    })

    $("#no-downloads-doc-info-bound").click(function () {
        $("#no-downloads-doc-info-bound").addClass("click-event");
    })


    $("#load-modal-1").on('hidden.bs.modal', function () {
        if ($("#next-load-1").hasClass("click-event") && valid_next_2) {
            $("#load-modal-2").modal('show');
            $("#next-load-1").removeClass("click-event");
        }
    });

    $("#load-modal-2").on('hidden.bs.modal', function () {
        if ($("#prev-load-2").hasClass("click-event")) {
            $("#load-modal-1").modal('show');
            $("#prev-load-2").removeClass("click-event")
        }
        if ($("#next-load-2").hasClass("click-event")) {
            $("#load-modal-3").modal('show');
            $("#next-load-2").removeClass("click-event");
        }
        if ($("#add-after-table").hasClass("click-event")) {
            $("#load-modal-4").modal('show');
            $("#add-after-table").removeClass("click-event")
            $("#add-after-table").addClass("my-load-modal")
        }
    });

    $("#load-modal-3").on('hidden.bs.modal', function () {
        if ($("#prev-load-3").hasClass("click-event")) {
            $("#load-modal-2").modal('show');
            $("#prev-load-3").removeClass("click-event");
        }
        if ($("#bound-dialog").hasClass("click-event")) {
            $("#load-modal-6").modal('show');
            $("#bound-dialog").removeClass("click-event")
        }
        if ($("#zone-dialog").hasClass("click-event")) {
            $("#load-modal-5").modal('show');
            $("#zone-dialog").removeClass("click-event")
        }
        if ($("#preview-btn").hasClass("click-event")) {
            $("#map-upload-preview").modal("show");
            $("#preview-btn").removeClass("click-event")
        }
    });

    $("#load-modal-4").on('hidden.bs.modal', function () {
        if ($("#no-downloads").hasClass("edit")) {
            $("#edit-modal-1").modal('show');
            $("#no-downloads").removeClass("edit");
            setListenersForDocs();
        } else {
            $("#load-modal-2").modal('show');
            $("#add-after-table").removeClass("my-load-modal")
        }
    });

    $("#load-modal-5").on('hidden.bs.modal', function () {
        if ($("#no-downloads-doc-info-zone").hasClass("click-event")) {
            $("#load-modal-3").modal('show');
            $("#no-downloads-doc-info-zone").removeClass("click-event");
        }
    });

    $("#load-modal-6").on('hidden.bs.modal', function () {
        if ($("#no-downloads-doc-info-bound").hasClass("click-event")) {
            $("#load-modal-3").modal('show');
            $("#no-downloads-doc-info-bound").removeClass("click-event");
        }
    });

    $("#preiscurant-new-modal-1").on('hidden.bs.modal', function() {
        if(price_scope.btn_save){
            reload_table_default();
            price_scope.btn_save = false;
            ID_VIEW_PREISKYRANT='';
            $("#save-close-preiskyrant").attr("disabled","disabled");
        }
    });

    $("#preiscurant-new-modal-2").on('hidden.bs.modal', function() {
        if(price_scope.des_price){
            reload_table_default();
            price_scope.des_price = false;
            ID_VIEW_PREISKYRANT='';
            $("#save-close-preiskyrant").attr("disabled","disabled");
        }
    });


    $("#myBtn").click(function () {
        $("#myModal").modal("show");
    });


    // ПРОСМОТР и РЕДАКТИРОВАНИЕ

    $("#view-btn").click(function () {
        $("#view-modal-1").modal("show");
        setViewEdit();
        cleanView();
        setView();
        setViewListeners();
    })

    $("#edit-btn").click(function () {
        cleanEdit();
        setViewEdit();
        setEdit();
        setListeners();

        $("#edit-modal-1").modal("show");
    })

    $("#edit-modal-1").on('hidden.bs.modal', function () {
        if ($("#add-after-table-edit").hasClass("click-event")) {
            $("#load-modal-4").modal('show');
            $("#add-after-table-edit").removeClass("click-event")
        }
    });

    $("#log-in").click(function () {
        $("#log-in-dialog").modal("show");
    });

    $("#log-out").click(function () {
        httpServices.logOut()
    });

    $("#log-in-open").click(function () {
        var username = $("#login").val(),
            password = $("#password").val();
        httpServices.autorization(username, password)
    });

    $("#search-1-filtr").click(function(){
        $("#search-1-filtr").addClass("filt");
    });



    //ВЫДАЧА ДОКУМЕНТОВ
     $("#jurnal-zakaz").click(function(){
         closeAllBlock();
         if($("#for-jurnal-zakaz").hasClass("display-none")) {
             $("#for-jurnal-zakaz").removeClass("display-none");
         }
         if (order_load == 0) {
             order_load = 1;
             httpServices.get_orders_list(order_filter);
             httpServices.get_order_states();
             create_order_table();

         }
         closeButtons();

     });

    $("#preiskyrant").click(function(){
        closeAllBlock();
        if($("#for-preiskyrant").hasClass("display-none")) {
            $("#for-preiskyrant").removeClass("display-none");
        }
        closeButtons();
    });

    $("#otchet").click(function(){
        closeAllBlock();
        if($("#for-otchet").hasClass("display-none")) {
            $("#for-otchet").removeClass("display-none");
        }
        closeButtons();
    });

    $("#statictika").click(function(){
        closeAllBlock();
        if($("#for-statictika").hasClass("display-none")) {
            $("#for-statictika").removeClass("display-none");
        }
        closeButtons();
    });

    $(".back-get-doc").click(function(){
        closeAllBlock();
        openButtons();
    });

    $(".back-get-order").click(function(){
        closeAllBlockOrder();
        openJurnalOrder();
        reload_order_table();
    });


    // АДМИН

    $("#clean_all").click(function () {
        clean_fields();
    });

    $("#save_user").click(function () {
        if (check_add_fields()) {
            alert("Заполните все поля!!!")
        } else {
            set_user_data();
            httpServices.send_userdata(user_data);
        }
    });

    $("#modal-save").click(function () {
        if (check_update_fields()) {
            alert("Поля не могут быть пустыми!!!")
        } else if(check_update_fields_pass()){
            alert("Пароль пуст! Заполните пароль!");
        }else {
            var result = confirm("Сохранить измененную инфомрацию?");
            if (result) {
                set_edit_user_data();
                httpServices.update_user(edit_user_data);
                edit_table_change(1);
                delete_table_change(1);
                alert("Данные изменены");
                $("#edit_modal").modal("hide");
            }
        }
    })

    $("#edit_tbody").on('click', 'tr', function () {
        set_modal_fields(edit_admin_scope.accounts, $(this).attr("id").substr(5));
        if ($(this).hasClass('selected-tr')) {
            $(this).removeClass('selected-tr');
        } else {
            $(this).siblings().removeClass('selected-tr');
            $(this).addClass('selected-tr');
        }
    });

    $("#edit_modal").on('show.bs.modal', function () {
    });

    $("#edit_modal").on('hidden.bs.modal', function () {
        $("tr.selected-tr").removeClass('selected-tr');
        $("#edit_user_exist").remove();
        $("#edit_modal_password").val('');
        $("#edit_pass_check").removeAttr("checked");
        $("#edit_div_password").addClass('hidden');
        $("#edit_modal_username").attr('readonly', 'readonly');
    });

    $("#edit_pass_check").change(function () {
        if ($(this).is(":checked")) {
            $("#edit_modal_username").removeAttr('readonly');
            $("#edit_div_password").removeClass('hidden');
            $("#edit_modal_username").removeAttr("disabled")
        } else {
            $("#edit_modal_username").attr('readonly', 'readonly');
            $("#edit_div_password").addClass('hidden');
            $("#edit_modal_username").attr("disabled","disabled");
        }
    });

    $("#add_fio").change(function () {
        add_fio_event();
    });

    $("#add_fio").keyup(function () {
        add_fio_event();
    });

    $("#edit_modal_fio").keyup(function () {
        edit_fio_event();
    });

    $("#edit_modal_fio").change(function () {
        edit_fio_event();
    });

    $("#edit_modal_username").keyup(function () {
        $("#edit_user_exist").remove();
        var user_name = $("#edit_modal_username").val().toLowerCase();
        if ($.inArray(user_name, logins) != -1 && user_name != saved_username){
            $("#edit_div_username").append('<p class="form-control clean" id="edit_user_exist">Пользователь с таким логином уже существует</p>');
            $("#modal-save").attr("disabled", "disabled").addClass("disabled_edit_login");
        } else {
            $("#edit_user_exist").remove();
            $("#modal-save").removeClass("disabled_edit_login");
            check_edit_disable();
        }

    });

    $("#delete_page").change(function () {
        delete_record_count = $("#delete_page").val();
        delete_table_change(1);
    });

    $("#edit_page").change(function () {
        edit_record_count = $("#edit_page").val();
        edit_table_change(1);
    });

    $("#add_username").keyup(function () {
        $("#user_exist").remove();
        var user_name = $("#add_username").val().toLowerCase();
        if ($.inArray(user_name, logins) != -1) {
            $("#div_username").append('<p class="form-control clean" id="user_exist">Пользователь с таким логином уже существует</p>');
            $("#save_user").attr("disabled", "disabled").addClass("disabled_login");
        } else {
            $("#user_exist").remove();
            $("#save_user").removeClass("disabled_login");
            check_disable();
        }
    });

    $("#add_org").change(function () {
        select_org_id = $(this).children(":selected").attr("id").substr(4);
    });

    $("#add_group").change(function () {
        select_group_id= $(this).children(":selected").attr("id").substr(6);
    });

    $(".edit-filter").keyup(function (e) {
        if (e.keyCode == 13) {
            set_edit_filter_fields();
            console.log(edit_filter);
            httpServices.set_edit_filter_row(edit_filter);
            edit_table_change(1);
        }
    });

    $(".delete-filter").keyup(function (e) {
        if (e.keyCode == 13) {
            set_delete_filter_fields();
            console.log(delete_filter);
            httpServices.set_delete_filter_row(delete_filter);
            delete_table_change(1);
        }
    });

    $(":button.glyphicon-off").click(function () {
        var result = confirm("Очистить фильтры?");
        if (result) {
            clear_delete_filter_field();
            delete_row_count = row_count;
            delete_table_change(1);
        }
    });

});