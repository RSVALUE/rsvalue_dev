/**
 * Created by Shcherbina on 08.11.2016.
 */
var price_scope = []
var ID_VIEW_PREISKYRANT;
var price_load_param = {
    page: 1,
    amount: '5'

};

//pricesBook_str

function getPricesTable() {
    $("#preiskyrant-table-body").empty();
    $("#preiskyrant-table-body").append(function () {
        var tabl = "";
        $.each(price_scope.pricesBook, function (i, obj) {
            if (obj.stopDate == null) {
                obj.stopDate = "";
            }
            tabl += openTRID(obj.priceId + "-preiskyrant-table");
            tabl += tdText(obj.priceNumber);
            tabl += tdText(obj.startDate);
            tabl += tdTextId(obj.stopDate, obj.priceId + "-stop-date-preiskyrant");
            tabl += tdText(obj.userInfo);
            tabl += "</tr>"
        })
        return tabl;
    });
}

function setListenersPreiskyrantTable() {
    $.each(price_scope.pricesBook, function (i, obj) {
        document.getElementById(obj.priceId + "-preiskyrant-table").addEventListener('click', funClickPreiskyrant, false);
    });
    for (var i = 1; i <= price_scope.pricesBook_str; i++) {
        var id = i + "-table-preiskyrant";
        document.getElementById(id).addEventListener('click', paginationEventFunction, false);
    }
    document.getElementById('prev-str-table-preiskyrant').addEventListener('click', paginationEventFunction, false);
    document.getElementById('next-str-table-preiskyrant').addEventListener('click', paginationEventFunction, false);
}

function paginationEventFunction(e) {
    var id = $(this).attr("id").split('-')[0];
    var id_event = "#" + $(this).attr("id");
    var page_prew = price_load_param.page;
    var id_prew = "#" + page_prew + "-table-preiskyrant";
    var all_page = price_scope.pricesBook_str;
    if (id == 'prev') {
        if (page_prew != 1) {
            $(id_prew).removeClass("active");
            price_load_param.page = page_prew - 1;
            reload_table_default();
            $("#" + price_load_param.page + "-table-preiskyrant").addClass("active");
        }
    } else if (id == 'next') {
        if (page_prew < all_page) {
            $(id_prew).removeClass("active");
            price_load_param.page = page_prew + 1;
            reload_table_default();
            $("#" + price_load_param.page + "-table-preiskyrant").addClass("active");
        }
    } else {
        $(id_prew).removeClass("active");
        price_load_param.page = parseInt(id);
        reload_table_default();
        $(id_event).addClass("active");
    }

}

function funClickPreiskyrant(e) {
    var id = ($(this).attr("id")).split('-')[0],
        id_stop = id + "-stop-date-preiskyrant";
    ID_VIEW_PREISKYRANT = '';
    if ($(this).hasClass("selected-tr")) {
        $(this).removeClass("selected-tr");
        $("#close-preiskyrant").attr("disabled", "disabled");
    } else {
        $(".cursor-select").removeClass("selected-tr")
        $(this).addClass("selected-tr");
        ID_VIEW_PREISKYRANT = id;

        if ($("#" + id_stop).text().length == 0) {
            $("#close-preiskyrant").removeAttr("disabled");
        } else {
            $("#close-preiskyrant").attr("disabled", "disabled");
        }
    }
}

function clearPriceModal() {
    $("#dat-new-preiskyrant").val("");
    $("#num-new-preiskyrant").val("");
    $("#body-price-modal").empty();
}

function createNewTablePrice() {
    $("#dat-new-preiskyrant").val(price_scope.price.start_date);
    $("#num-new-preiskyrant").val(price_scope.price.number_price);
    $("#body-price-modal").append(function () {
        var table;
        $.each(price_scope.price.priceEntity, function (i, obj) {
            table += "<tr id='" + obj.docid + "-" + obj.subj_id + "'>"
                + "<td style='width: 25%;font-family: Constantia; font-weight: bold'>" + obj.docname + "</td>"
                + "<td>" + obj.subname + "</td>"
                + "<td>" + obj.nonurg + "</td>"
                + "<td>" + obj.nonndsurg + "</td>"
                + "<td>" + obj.sum_nonurg + "</td>"
                + "<td>" + obj.urg + "</td>"
                + "<td>" + obj.ndsurg + "</td>"
                + "<td>" + obj.sum_urg + "</td>"
                + "</tr>";
        })
        return table;
    })
}

function createNewTablePriceRedact(isNew) {
    if (price_scope.price.type == -1) {
        alert("Нельзя создать новый прайс, так как не закрыт текущий")
    } else {
        if (isNew) {
            $("#num-new-preiskyrant").removeAttr("disabled");
            $("#dat-new-preiskyrant").removeAttr("disabled");
            $("#dat-new-preiskyrant").val(price_scope.price.start_date);
        } else {
            console.log(price_scope.price.start_date)
            $("#dat-new-preiskyrant").val(price_scope.price.start_date);
            $("#num-new-preiskyrant").val(price_scope.price.number_price);
        }
        $("#body-price-modal").append(function () {
            console.log(price_scope.price);
            var table;
            $.each(price_scope.price.priceEntity, function (i, obj) {
                var id = obj.docid + "-" + obj.subj_id;
                table += "<tr id='" + i + "-price" + "' class='" + id + "'>"
                    + "<td style='padding: 0; width: 25%; font-family: Constantia; font-weight: bold'>" + obj.docname + "</td>"
                    + "<td style='padding: 0'>" + obj.subname + "</td>"
                    + "<td style='padding: 0' ><textarea class='A' id='" + i + "-nonurg" + "'>" + obj.nonurg + "</textarea></td>"
                    + "<td style='padding: 0'><textarea class='A'>" + obj.nonndsurg + "</textarea></td>"
                    + "<td style='padding: 0'><textarea class='A'>" + obj.sum_nonurg + "</textarea></td>"
                    + "<td style='padding: 0'><textarea class='A' id='" + i + "-urg" + "'>" + obj.urg + "</textarea></td>"
                    + "<td style='padding: 0'><textarea class='A'>" + obj.ndsurg + "</textarea></td>"
                    + "<td style='padding: 0'><textarea class='A'>" + obj.sum_urg + "</textarea></td>"
                    + "</tr>";

            });
            return table;
        });
        addEditListener();
    }
}

function addEditListener() {
    $.each(price_scope.price.priceEntity, function (i) {
        document.getElementById(i + "-nonurg").addEventListener('change', priceNonurgEditListener, false);
        document.getElementById(i + "-urg").addEventListener('change', priceUrgEditListener, false);
    });
}

function priceUrgEditListener(e) {
    alert($(this).val());

}

function priceNonurgEditListener(e) {
    alert($(this).val());
}


function setParamsStopPreiskyrant() {
    $("#dat-preiskurant-deistv-stop").val(price_scope.price.start_date);
    $("#num-preiskurant-stop").val(price_scope.price.number_price);
    $("#name-preiskurant-stop").val(price_scope.price.torName);

}

function clearParamsStopPreiskyrant() {
    $("#dat-preiskurant-deistv-stop").val("");
    $("#num-preiskurant-stop").val("");
    $("#name-preiskurant-stop").val("");
    $("#dat-preiskurant-stop").val(now_date);
}

function reloadTable() {
    httpServices.getPrices(price_scope, price_load_param.sorting, price_load_param.predicate, price_load_param.page,price_load_param.tor,price_load_param.amount, AUTHOBJ.userId);
    getPricesTable();
}

function getPreiskurantLi(col) {
    var li = "<li><a  id=\"prev-str-table-preiskyrant\">&laquo;</a></li>";
    for (var i = 1; i <= col; i++) {
        if (i == 1) {
            if (price_load_param.page == 1) {
                li += "<li id=\"" + i + "-table-preiskyrant\" class='active'><a>" + i + "</a></li>";
            } else {
                li += "<li id=\"" + i + "-table-preiskyrant\"><a>" + i + "</a></li>";
            }
        } else {
            li += "<li id=\"" + i + "-table-preiskyrant\"><a>" + i + "</a></li>";
        }
    }
    li += "<li id=\"next-str-table-preiskyrant\"><a>&raquo;</a></li>";
    return li;
}

function getPreiskurantPagination() {
    $("#pagination_table_preiskyrant").empty();
    $("#pagination_table_preiskyrant").append(function () {
        return getPreiskurantLi(price_scope.pricesBook_str);
    })

}

function reload_table_default() {
    httpServices.getPrices(price_scope, price_load_param.sorting, price_load_param.predicate, price_load_param.page, price_load_param.tor,price_load_param.amount, AUTHOBJ.userId);

    reload_table_fun();
}

function reload_table_fun(){
    getPricesTable();
    getPreiskurantPagination();
    setListenersPreiskyrantTable();
}

function setStartParam(){
    price_load_param.page = 1;
    price_load_param.sorting = 3;
    price_load_param.predicate = 'desc';
    price_load_param.tor = '';
    price_load_param.amount=5;
    price_scope.btn_save = false;
    price_scope.des_price = false;
    ID_VIEW_PREISKYRANT='';
}