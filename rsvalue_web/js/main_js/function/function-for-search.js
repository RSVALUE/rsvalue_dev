/**
 * Created by Shcherbina on 21.10.2016.
 */

var search_scope=[];
var searchPdf={
    "searchMethodValue":"",
    "data":{
        "address":"",
        "searchDate":"",
        "cadNum": "",
        "square":"",
        "purposeDic":"",
        "purposeGov":"",
        "communityGardenName":"",
        "nearLocality":"",
        "zoneName":"",
        "unp":"",
        "tenantName":"",
        "remark":""
    },
    "isNb":"",
    "valuesCharacters":[]
}

function menu2(){
    if($("#menu-down-2").hasClass("glyphicon-menu-up")){
        $("#menu-down-2").removeClass("glyphicon-menu-up");
        $("#menu-down-2").addClass("glyphicon-menu-down");
        $("#menu-search-2").removeClass("display-visible");
        $("#menu-search-2").addClass("display-none");
    }else{
        $("#menu-down-2").addClass("glyphicon-menu-up");
        $("#menu-down-2").removeClass("glyphicon-menu-down");
        $("#menu-search-2").addClass("display-visible");
        $("#menu-search-2").removeClass("display-none");
    }
    clear_all_2();
    $("#date-search-2").val(now_date);
}

function menu2_span(){
    if($("#menu-down-2").hasClass("glyphicon-menu-up")){
        $("#menu-down-2").removeClass("glyphicon-menu-up");
        $("#menu-down-2").addClass("glyphicon-menu-down");

    }else{
        $("#menu-down-2").addClass("glyphicon-menu-up");
        $("#menu-down-2").removeClass("glyphicon-menu-down");
    }
}

function menu1(){
    if($("#search-menu-down-1").hasClass("glyphicon-menu-up")){
        $("#menu-search-1").removeClass("display-visible");
        $("#menu-search-1").addClass("display-none");
        $("#search-menu-down-1").addClass("glyphicon-menu-down");
        $("#search-menu-down-1").removeClass("glyphicon-menu-up");
    }else{
        $("#menu-search-1").addClass("display-visible");
        $("#menu-search-1").removeClass("display-none");
        $("#search-menu-down-1").removeClass("glyphicon-menu-down");
        $("#search-menu-down-1").addClass("glyphicon-menu-up");
    }
    clearAllRes();
    $("#select-date-search").val("2017-01-01 00:00:00.0");
    $("#input-date-search").val(now_date);
}

function menu1_span(){
    if($("#search-menu-down-1").hasClass("glyphicon-menu-up")){
        $("#search-menu-down-1").addClass("glyphicon-menu-down");
        $("#search-menu-down-1").removeClass("glyphicon-menu-up");
    }else{
        $("#search-menu-down-1").removeClass("glyphicon-menu-down");
        $("#search-menu-down-1").addClass("glyphicon-menu-up");
    }
}


function changeSearchByAddrObl(){
    if($("#search-by-addr-2").hasClass("display-visible-2")) {
        $("#search-by-addr-2").removeClass("display-visible-2");
        $("#search-by-addr-2").addClass("display-none");
    }

    $("#search-raion-nas_p-num").empty();
    changeSearchByAddrRaion()
}

function changeSearchByAddrRaion(){
    if($("#search-by-addr-3").hasClass("display-visible-2")) {
        $("#search-by-addr-3").removeClass("display-visible-2");
        $("#search-by-addr-3").addClass("display-none");
    }

    $("#search-ss-nas_p-avto-num").empty();
    changeSearchByAddrSelSovet()
}

function changeSearchByAddrSelSovet(){
    if($("#search-by-addr-4").hasClass("display-visible-2")) {
        $("#search-by-addr-4").removeClass("display-visible-2");
        $("#search-by-addr-4").addClass("display-none");
    }

    $("#search-nas_p-BGD-num").empty();
    changeSearchByAddrBJD()
}

function changeSearchByAddrBJD(){
    if( $("#search-by-addr-5").hasClass("display-visible-2")) {
        $("#search-by-addr-5").removeClass("display-visible-2");
        $("#search-by-addr-5").addClass("display-none");
    }

    $("#search-yl").empty();
    changeSearchByAddrYlichnoDorojSet()
}

function changeSearchByAddrYlichnoDorojSet(){
    if($("#search-by-addr-6").hasClass("display-visible-2")) {
        $("#search-by-addr-6").removeClass("display-visible-2");
        $("#search-by-addr-6").addClass("display-none");
    }

    $("#9-search-by-addr-num-zdania-5").empty();
}

function  changeSearchBySTObl(){
    $("#search-raion-1").empty();

    $("#search-raion-1").attr("disabled","disabled");
    changeSearchBySTORaion();
}

function  changeSearchBySTORaion(){
    $("#search-name-st-1").empty();

    $("#search-name-st-1").attr("disabled","disabled");

    $("#search-by-ST-btn").attr("disabled","disabled");
}

function viewPDF (event) {
    var link = encodeURI(event.target.href);
    var ifrm= document.createElement('iframe');
    var width = 800;
    var left = 100;

    ifrm.src = link;
/// для примера
    ifrm.src  = 'http://mozilla.github.io/pdf.js/web/viewer.html?file=http%3A%2F%2Fasync5.org%2Fmoz%2Fpdfjs.pdf';
    ifrm.setAttribute('style', 'z-index: 9999;width: '+width+'px; height: 500px; position: fixed; top: 100px; left: '+left+'px;');
    var close = document.createElement('button');
    close.setAttribute('type', 'button');
    close.innerHTML = 'Закрыть';
    close.onclick = function() {
        document.body.removeChild(ifrm);
        document.body.removeChild(close);
    }
    close.setAttribute('style', 'z-index: 9999;position: fixed; top: 100px; left: '+  (left+width+20) +'px;');
    document.body.appendChild(ifrm);
    document.body.appendChild(close);
}

function closeAllRes(){
    if(!$("#res-search-cadestr-1").hasClass("display-none")){
        $("#res-search-cadestr-1").removeClass("display-visible");
        $("#res-search-cadestr-1").addClass("display-none");
    }

    if(!$("#res-search-vne-1").hasClass("display-none")){
        $("#res-search-vne-1").removeClass("display-visible");
        $("#res-search-vne-1").addClass("display-none");
    }

    if(!$("#res-search-st-1").hasClass("display-none")){
        $("#res-search-st-1").removeClass("display-visible");
        $("#res-search-st-1").addClass("display-none");
    }
    if(!$("#res-search-2").hasClass("display-none")){
        $("#res-search-2").removeClass("display-visible");
        $("#res-search-2").addClass("display-none");
    }
    clearAllRes();
    closeClearSearchPoint();
}

function closeClearSearchPoint(){
    if(!$("#res-search-addr-1-2").hasClass("display-none")){
        $("#res-search-addr-1-2").removeClass("display-visible");
        $("#res-search-addr-1-2").addClass("display-none");
    }
    if(!$("#res-search-addr-1-1").hasClass("display-none")){
        $("#res-search-addr-1-1").removeClass("display-visible");
        $("#res-search-addr-1-1").addClass("display-none");
    }
    clearDivMoreCad();
}

function clearAllRes(){

    clearDivCadNum();
    clearDivNameSad();
    clearAllDivVNP();

    clearInputCadastr();
    clearInputST();
    clearInputVNP();
    change_1_el();
}

function clearInputVNP(){
    $("#search-raion").empty();
    $("#search-ocen_zona").empty();
    $("#search-ocen_zona_num").empty();
    $("#search-fun").val(0).change();
    $("#search-obl2").val(0).change();

    $("#search-raion").removeAttr("disabled");
    $("#search-raion").attr("disabled", "disabled");

    $("#search-ocen_zona").removeAttr("disabled");
    $("#search-ocen_zona").attr("disabled", "disabled");

    $("#search-ocen_zona_num").removeAttr("disabled");
    $("#search-ocen_zona_num").attr("disabled", "disabled");
}

function clearInputCadastr(){
    $("#soato").val("");
    $("#numblock").val("");
    $("#numuch").val("");
}

function clearInputST(){
    $("#search-raion-1").empty();
    $("#search-name-st-1").empty();
    $("#search-obl-1").val(0).change();
    //alert($("select[id=search-obl-1] option").size())
}

function clearAllDivVNP(){
    $("#search-address2").empty();
    $("#search-name_zone").empty();
    $("#search-num_zone").empty();
}

function clearSearchByUnp_obl(){
    $("#search-raion3").empty();
    clearSearchByUnp_raion();
    $("#search-UNP").attr("disabled", "disabled");
}

function clearSearchByUnp_raion(){
    $("#search-UNP").val("");
}

function clearSearchByNumZone_obl(){
    $("#search-raion4").empty();
    clearSearchByNumZone_raion();
    $("#search-name_zemlepol").attr("disabled", "disabled");
}

function clearSearchByNumZone_raion(){
    $("#search-num_oc_zone").empty();
}

function clearSearchByZemlep_obl(){
    $("#search-raion5").empty();
    clearSearchByZemlep_raion();
    $("#search-name_zemlepol").attr("disabled", "disabled");
}

function clearSearchByZemlep_raion(){
    $("#search-name_zemlepol").empty();
}

function clearByRaion_VNP(){
    $("#search-ocen_zona").empty();
    $("#search-ocen_zona_num").attr("disabled", "disabled");
    clearByNameZona_VNP();
}

function clearByObl_VNP(){
    $("#search-raion").empty();
    $("#search-ocen_zona").attr("disabled", "disabled");
    clearByRaion_VNP();
}

function clearByNameZona_VNP(){
    $("#search-ocen_zona_num").empty();
    $("#search-vne").attr("disabled", "disabled");
}

function getOcenZona(){
    httpServices.getocen_zona(search_scope, search_scope.searchAddr_raion_ateID, search_scope.searchVNP_FunCode, search_scope.date_search)
    if(search_scope.searchVNP_NameZone.length != 0) {
        $("#search-ocen_zona").append(function () {
            var option_raion = option(-1, -1, "");
            $.each(search_scope.searchVNP_NameZone, function (i) {
                option_raion += option(i, search_scope.searchVNP_NameZone[i], search_scope.searchVNP_NameZone[i]);
            })
            $("#search-ocen_zona").removeAttr("disabled");
            return option_raion;
        })
    }
}

function allSearchClean(){
    BY_CADASTR = 0; BY_ADDR = 0; BY_ST = 0; BY_VNP = 0; BY_SH_UNP = 0; BY_SH_ZEMLEP = 0; BY_SH_NUM = 0;
}

function append_2input(){
    change_2_el();
    var val = ($("#2-select-search").val());
    if(val != -1){
        search_scope.searchByAddr_2 = val;

        clearAddresEl_2();

        httpServices.getAddressData(search_scope, addres.region, addres.typeIn, addres.typeOut, addres.area, addres.ss, addres.snp, addres.bjd, addres.road, addres.km, addres.street);
        $("#2-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];
            console.log(addres)
            console.log(data)
            if(addres.typeOut==2){
                data=search_scope.searchAddr_dataRegion;
            } else if(addres.typeOut==4){
                data=search_scope.searchAddr_dataNasPunkt;
            } else if(addres.typeOut==9){
                data=search_scope.searchAddr_dataNumZd;
        }else if( addres.typeOut==5) {
            data = search_scope.searchAddr_dataBGD;
        }else if( addres.typeOut==8) {
            data = search_scope.searchAddr_dataYlica;
        }
            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_3input(){
    change_3_el();
    var val = ($("#3-select-search").val());
    if(val != -1){

        search_scope.searchByAddr_3 = val;

        clearAddresEl_3();

        httpServices.getAddressData(search_scope, addres.region, addres.typeIn, addres.typeOut, addres.area, addres.ss, addres.snp, addres.bjd, addres.road, addres.km, addres.street);
        $("#3-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];

            if( addres.typeOut==4) {
                data = search_scope.searchAddr_dataNasPunkt;
            }else if( addres.typeOut==9) {
                data = search_scope.searchAddr_dataNumZd;
            }else if( addres.typeOut==3) {
                data = search_scope.searchAddr_dataSelsovet;
            }else if( addres.typeOut==6) {
                data =  search_scope.searchAddr_dataAvtod;
            }else if( addres.typeOut==5) {
                data = search_scope.searchAddr_dataBGD;
            }else if( addres.typeOut==8) {
                data = search_scope.searchAddr_dataYlica;
            }
            /*else if( addres.typeOut==7) {
             data = search_scope.searchAddr_dataRasst;
             }*/

            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_4input(){
    change_4_el();
    var val = ($("#4-select-search").val());
    if(val != -1){
        search_scope.searchByAddr_4 = val;

        clearAddresEl_4();

        httpServices.getAddressData(search_scope, addres.region, addres.typeIn, addres.typeOut, addres.area, addres.ss, addres.snp, addres.bjd, addres.road, addres.km, addres.street);
        $("#4-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];

            if( addres.typeOut==4) {
                data = search_scope.searchAddr_dataNasPunkt;
            }else if( addres.typeOut==9) {
                data = search_scope.searchAddr_dataNumZd;
            }else if( addres.typeOut==3) {
                data = search_scope.searchAddr_dataSelsovet;
            }else if( addres.typeOut==6) {
                data =  search_scope.searchAddr_dataAvtod;
            }else if( addres.typeOut==5) {
                data = search_scope.searchAddr_dataBGD;
            }else if( addres.typeOut==8) {
                data = search_scope.searchAddr_dataYlica;
            }
            else if( addres.typeOut==7) {
                data = search_scope.searchAddr_dataRasst;
            }

            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_5input(){
    change_5_el();
    var val = ($("#5-select-search").val());
    if(val != -1){
        search_scope.searchByAddr_5 = val;

        clearAddresEl_5();

        httpServices.getAddressData(search_scope, addres.region, addres.typeIn, addres.typeOut, addres.area, addres.ss, addres.snp, addres.bjd, addres.road, addres.km, addres.street);
        $("#5-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];

            if( addres.typeOut==4) {
                data = search_scope.searchAddr_dataNasPunkt;
            }else if( addres.typeOut==9) {
                data = search_scope.searchAddr_dataNumZd;
            }else if( addres.typeOut==3) {
                data = search_scope.searchAddr_dataSelsovet;
            }else if( addres.typeOut==6) {
                data =  search_scope.searchAddr_dataAvtod;
            }else if( addres.typeOut==5) {
                data = search_scope.searchAddr_dataBGD;
            }else if( addres.typeOut==8) {
                data = search_scope.searchAddr_dataYlica;
            }
            else if( addres.typeOut==7) {
                data = search_scope.searchAddr_dataRasst;
            }

            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function append_6input(){
    change_6_el();
    var val = ($("#6-select-search").val());
    if(val != -1){
        search_scope.searchByAddr_6 = val;
        addres.typeIn = search_scope.searchByAddr_5;
        addres.typeOut = val;
        //alert(addres.street)
        httpServices.getAddressData(search_scope, addres.region, addres.typeIn, addres.typeOut, addres.area, addres.ss, addres.snp, addres.bjd, addres.road, addres.km, addres.street);
        $("#6-select-search-data").append(function(){
            var select = option(-1, -1, "");
            var data = [];
            if( addres.typeOut==9) {
                data = search_scope.searchAddr_dataNumZd;
            }
            $.each(data, function (i) {
                select += option(i, data[i][0], data[i][1]);
            })
            return select;
        })
    }
}

function closeDialog(){
    if(!$("#svedenia-cadastr").hasClass("display-none")){
        $("#svedenia-cadastr").addClass("display-none")
    }
    if(!$("#svedenia-adr").hasClass("display-none")){
        $("#svedenia-adr").addClass("display-none")}
    if(!$("#svedenia-vnp").hasClass("display-none")){
        $("#svedenia-vnp").addClass("display-none")}
    if(!$("#svedenia-sh").hasClass("display-none")){
        $("#svedenia-sh").addClass("display-none")}
    if(!$("#svedenia-st").hasClass("display-none")){
        $("#svedenia-st").addClass("display-none")}
}

function changeFun (){
    $("#search-ocen_zona").empty();
    $("#search-ocen_zona_num").empty();
    $("#search-ocen_zona_num").attr("disabled", "disabled");

}

function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    var newWin = window.open("", "")
    newWin.focus();
    newWin.document.body.innerHTML = printContents;

    newWin.print();

    newWin.close();
    return true;

}


// ОЧИТКИ ПОИСК ПО АДРЕСУ!!!!!!!!!!!!
function change_6_el(){
    $("#6-select-search-data").empty();
    closeAddrSearch();
}

function change_5_el(){
    change_6_el();
    $("#5-select-search-data").empty();
    if(!$("#search-by-addr-6").addClass("display-none")) {
        $("#search-by-addr-6").addClass("display-none")
    }

}

function change_5_data(){
    $("#6-select-search-data").empty();
}

function change_4_el(){
    change_5_el();
    $("#4-select-search-data").empty();
    $("#5-select-search-data").empty();
    if(!$("#search-by-addr-5").addClass("display-none")) {
        $("#search-by-addr-5").addClass("display-none")
    }
}

function change_4_data(){
    change_5_data();
    if(!$("#search-by-addr-6").addClass("display-none")) {
        $("#search-by-addr-6").addClass("display-none")
    }
    $("#5-select-search").empty();
    $("#5-select-search-data").empty();
}

function change_3_el(){
    change_4_el();
    $("#3-select-search-data").empty();
    $("#4-select-search-data").empty();
    if(!$("#search-by-addr-4").addClass("display-none")) {
        $("#search-by-addr-4").addClass("display-none")
    }
}

function setAddresbyTypeOut(val, out){
    if( out==4) {
        addres.snp = val;
    }else if( out==3) {
        addres.ss = val;
    }else if( out==6) {
        addres.road = val;
    }else if( out==5) {
        addres.bjd = val;
    }else if( out==8) {
        addres.street = val;
    }else if( out==7) {
        addres.km = val;
    }else if( out==9) {
        addres.numDom = val;
    }else if( out==2) {
        addres.area = val;
    }
}
function clearAddresEl_5(){
    var val_O = $("#5-select-search").val(), val_I = $("#4-select-search").val(), val_reg = addres.region;
    addresInit();
    addres.typeOut = val_O;
    addres.typeIn = val_I;
    addres.region = val_reg;
    setAddresbyTypeOut($("#2-select-search-data").val(), $("#2-select-search").val());
    setAddresbyTypeOut($("#3-select-search-data").val(), $("#3-select-search").val());
    setAddresbyTypeOut($("#4-select-search-data").val(), $("#4-select-search").val());
}

function clearAddresEl_4(){
    var val_O = $("#4-select-search").val(), val_I = $("#3-select-search").val(), val_reg = addres.region;
    addresInit();
    addres.typeOut = val_O;
    addres.typeIn = val_I;
    addres.region = val_reg;
    setAddresbyTypeOut($("#2-select-search-data").val(), $("#2-select-search").val());
    setAddresbyTypeOut($("#3-select-search-data").val(), $("#3-select-search").val());
}

function clearAddresEl_3(){
    var val_O = $("#3-select-search").val(), val_I = $("#2-select-search").val(), val_reg = addres.region;
    addresInit();
    addres.typeOut = val_O;
    addres.typeIn = val_I;
    addres.region = val_reg;
    setAddresbyTypeOut($("#2-select-search-data").val(), $("#2-select-search").val())
}

function change_3_data(){
    change_4_data();
    if(!$("#search-by-addr-5").addClass("display-none")) {
        $("#search-by-addr-5").addClass("display-none")
    }
    $("#4-select-search").empty();
    $("#4-select-search-data").empty();
}

function change_2_el(){
    change_3_el();
    $("#2-select-search-data").empty();
    $("#3-select-search-data").empty();
    if(!$("#search-by-addr-3").addClass("display-none")) {
        $("#search-by-addr-3").addClass("display-none")
    }
}

function clearAddresEl_2(){
    var val_O = $("#2-select-search").val(), val_I = 1, val_reg = addres.region;
    addresInit();
    addres.typeOut = val_O;
    addres.typeIn = val_I;
    addres.region = val_reg;
}

function change_2_data(){
    change_3_data();
    if(!$("#search-by-addr-4").addClass("display-none")) {
        $("#search-by-addr-4").addClass("display-none")
    }
    $("#3-select-search").empty();
    $("#3-select-search-data").empty();
}

function change_1_el(){
    change_2_el();
    $("#1-select-search-data").empty();
    $("#2-select-search-data").empty();
    $("#1-search-by-addr-abl-select").val(0).change();
    if(!$("#search-by-addr-2").addClass("display-none")) {
        $("#search-by-addr-2").addClass("display-none")
    }
}

function change_1_data(){
    change_2_data();
    if(!$("#search-by-addr-3").addClass("display-none")) {
        $("#search-by-addr-3").addClass("display-none")
    }
    $("#2-select-search").empty();
    $("#2-select-search-data").empty();
}

// КАДАСТРОВЫЙ НОМЕР ПОИСК

function searchByCadastrModal(){

    if(CADASTR_RES >0){


        $("#cadastr-addres-cad").append(search_scope.searchCadastr_Res.address);
        $("#cadastr-num-cad").append(search_scope.searchCadastr_Res.cadNum);
        $("#cadastr-cel-klassif-cad").append(search_scope.searchCadastr_Res.purposeDic);
        $("#cadastr-cel-mestn-cad").append(search_scope.searchCadastr_Res.purposeGov);
        $("#cadastr-pl-cad").append(search_scope.searchCadastr_Res.square);

        $("#cadastr-table").append(function () {
            var text = "", i = 0;
            if(search_scope.isNalog){
                if($("#cadastr-nalog-table").hasClass("display-none")){
                    $("#cadastr-nalog-table").removeClass("display-none");
                }
                if(search_scope.searchCadastr_Res.cadNum == null) {
                    $("#cadastr-nalog-table").text("Кадастровая стоимость 1 кв.м земель на "+search_scope.searchCadastr_Res.searchDate+" года для исчисления налоговой базы земельного налога, рублей")
                } else{
                    $("#cadastr-nalog-table").text("Кадастровая стоимость 1 кв.м земельного участка на "+search_scope.searchCadastr_Res.searchDate+" года для исчисления налоговой базы земельного налога, рублей")
                }
                noDisplZvezda(); noDispRub();
                if(!$("#cadastr-search-table-head").hasClass('display-none')){
                    $("#cadastr-search-table-head").addClass('display-none')
                }
            }else{
                if($("#cadastr-search-table-head").hasClass('display-none')){
                    $("#cadastr-search-table-head").removeClass('display-none')
                }
                displZvezda(); dispRub();
                if(!$("#cadastr-nalog-table").hasClass("display-none")){
                    $("#cadastr-nalog-table").addClass("display-none");
                }
                if(search_scope.searchCadastr_Res.cadNum == null) {
                    $("#cadastr-search-table-head").text("Кадастровая стоимость 1 кв. м. земель оценочных зон на " + search_scope.searchCadastr_Res.searchDate)
                }else{
                    $("#cadastr-search-table-head").text("Кадастровая стоимость 1 кв. м. земельного участка по состоянию на " + search_scope.searchCadastr_Res.searchDate)
                }
            }

            for (; i < search_scope.searchCadastr_Res.valuesCharacters.length; i++) {
                text += "<tr>"
                    + "<td>"
                    + search_scope.searchCadastr_Res.valuesCharacters[i].funcCodeName
                    + "</td>"
                    + "<td>"
                    + search_scope.searchCadastr_Res.valuesCharacters[i].dateVal
                    + "</td>"
                    + "<td>"
                    + search_scope.searchCadastr_Res.valuesCharacters[i].zoneNum
                    + "</td>";
                if(!search_scope.isNalog) {
                    text += "<td>"
                        + search_scope.searchCadastr_Res.valuesCharacters[i].costD
                        + "</td>"
                        + "<td>"
                        + search_scope.searchCadastr_Res.valuesCharacters[i].costR
                        + "</td>";
                }
                if(search_scope.isNalog){
                    text+="<td>"
                        + search_scope.searchCadastr_Res.valuesCharacters[i].costNB;
                        + "</td>"
                }
                text+= "</tr>"
            }
            return text;
        });
        $("#text-cadastr").append(function () {
            var j = 0;
            var text = "<p>"+search_scope.searchCadastr_Res.remark + "</p>";
            return text;
        });
    }
}

function searchByCadastr(){

    if(CADASTR_RES >0){
        $("#search-address").append(search_scope.searchCadastr_Res.address);
        $("#search-cad-num").append(search_scope.searchCadastr_Res.cadNum);
        $("#search-sq").append(search_scope.searchCadastr_Res.square);
        $("#search-goal").append(search_scope.searchCadastr_Res.purposeDic);
        $("#search-goal-1").append(search_scope.searchCadastr_Res.purposeGov);
    }
}

function closeAddrSearch(){
    if(!$("#res-search-addr-1-1").hasClass("display-none")){
        $("#res-search-addr-1-1").addClass("display-none")
    }
    if(!$("#res-search-addr-1-2").hasClass("display-none")){
        $("#res-search-addr-1-2").addClass("display-none")
    }
    if(!$("#res-search-addr-1-3").hasClass("display-none")) {
        $("#res-search-addr-1-3").addClass("display-none")
    }

    $("#cad-num-by-addr-2").empty();
    $("#cad-num-by-addr-3").empty();
}

function clearDialogCadNum(){
    $("#cadastr-name-cad").empty();
    $("#cadastr-addres-cad").empty();
    $("#cadastr-num-cad").empty();
    $("#cadastr-pl-cad").empty();
    $("#cadastr-cel-klassif-cad").empty();
    $("#cadastr-cel-mestn-cad").empty();

    $("#cadastr-table").empty();
    $("#text-cadastr").empty();
}

function clearDivCadNum(){
    $("#search-address").empty();
    $("#search-cad-num").empty();
    $("#search-sq").empty();
    $("#search-goal").empty();
    $("#search-goal-1").empty();
}

function clearDivMoreCad(){
    $("#cad-num-by-addr-2").empty();
    $("#cad-num-by-addr-1").empty();
}


// ПОИСК ПО НАИМЕНОВАНИЮ САДОВОГО ТОВАРИЩЕСТВА

function clearDivNameSad(){
    $("#res-search-address-st-1").empty();
    $("#res-search-name-st-1").empty();
    $("#res-search-punkt").empty();
}

function clearModalNameSad(){
    $("#cadastr-addres-st").empty();
    $("#cadastr-zone-st").empty();
    $("#cadastr-num-st").empty();

    $("#st-table").empty();
    $("#text-st").empty();
}

function searchByNameSadDiv(){
    $("#res-search-address-st-1").append(search_scope.searchAddr_raionSTRes_1.address);
    $("#res-search-name-st-1").append(search_scope.searchAddr_raionSTRes_1.communityGardenName );
    $("#res-search-punkt").append(search_scope.searchAddr_raionSTRes_1.nearLocality);
}

function searchByNameSadModal(){
    $("#cadastr-addres-st").append(search_scope.searchAddr_raionSTRes_1.address)
    $("#cadastr-zone-st").append(search_scope.searchAddr_raionSTRes_1.communityGardenName)
    $("#cadastr-num-st").append(search_scope.searchAddr_raionSTRes_1.nearLocality)

    if(search_scope.isNalog){
        if($("#st-nalog-table").hasClass("display-none")){
            $("#st-nalog-table").removeClass("display-none");
        }
        if(search_scope.searchAddr_raionSTRes_1.cadNum == null) {
            $("#st-nalog-table").text("Кадастровая стоимость 1 кв.м земель на "+search_scope.searchAddr_raionSTRes_1.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
        }else{
            $("#st-nalog-table").text("Кадастровая стоимость 1 кв.м земельного участка на "+search_scope.searchAddr_raionSTRes_1.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
        }
        noDisplZvezda(); noDispRub();
        if(!$("#st-search-table-header").hasClass('display-none')){
            $("#st-search-table-header").addClass('display-none')
        }
    }else{
        if($("#st-search-table-header").hasClass('display-none')){
            $("#st-search-table-header").removeClass('display-none')
        }
        displZvezda(); dispRub();
        if(search_scope.searchAddr_raionSTRes_1.cadNum == null) {
            $("#st-search-table-header").text("Кадастровая стоимость 1 кв. м. земель оценочной зоны на " + search_scope.searchAddr_raionSTRes_1.searchDate);
        }else{
            $("#st-search-table-header").text("Кадастровая стоимость 1 кв. м. земельного участка по состоянию на " + search_scope.searchAddr_raionSTRes_1.searchDate);
        }
    }

    $("#st-table").append(function() {
        var text = "";
        var i=0;
        for(;i<search_scope.searchAddr_raionSTRes_1.valuesCharacters.length; i++) {
            text+="<tr>"
                + "<td>"
                + search_scope.searchAddr_raionSTRes_1.valuesCharacters[i].funcCodeName
                + "</td>"
                + "<td>"
                + search_scope.searchAddr_raionSTRes_1.valuesCharacters[i].dateVal
                + "</td>"
                + "<td>"
                + search_scope.searchAddr_raionSTRes_1.valuesCharacters[i].zoneNum
                + "</td>"
            if(!search_scope.isNalog) {
                text += "<td>"
                    + search_scope.searchAddr_raionSTRes_1.valuesCharacters[i].costD
                    + "</td>"
                    + "<td>"
                    + search_scope.searchAddr_raionSTRes_1.valuesCharacters[i].costR
                    + "</td>";
            }
            if(search_scope.isNalog){
                text+="<td>"
                    + search_scope.searchAddr_raionSTRes_1.valuesCharacters[i].costNB
                    + "</td>"
            }
            text+= "</tr>"
        }
        return text;
    });
    $("#text-st").append(function(){
        var j=0;
        var text = search_scope.searchAddr_raionSTRes_1.remark;
        return text;
    });
}

function searchByAddrCleanModal(){
    $("#cadastr-addres-addr").empty();

    $("#addr-table").empty();
    $("#text-addr").empty();
}

function searchByAddrSadModal(){
    $("#cadastr-addres-addr").append(search_scope.resultAddrSearchADDRESS.address)

    if(search_scope.isNalog){
        if($("#addr-nalog-table").hasClass("display-none")){
            $("#addr-nalog-table").removeClass("display-none");
        }
        if(search_scope.resultAddrSearchADDRESS.cadNum == null) {
            $("#addr-nalog-table").text("Кадастровая стоимость 1 кв.м земель на "+search_scope.resultAddrSearchADDRESS.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
        }else{
            $("#addr-nalog-table").text("Кадастровая стоимость 1 кв.м земельного участка на "+search_scope.resultAddrSearchADDRESS.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
        }
        noDisplZvezda(); noDispRub();
        if(!$("#cost-table-addr").hasClass('display-none')){
            $("#cost-table-addr").addClass('display-none')
        }
    }else {
        if($("#cost-table-addr").hasClass('display-none')){
            $("#cost-table-addr").removeClass('display-none')
        }
        displZvezda(); dispRub();
        if (!$("#addr-nalog-table").hasClass("display-none")) {
            $("#addr-nalog-table").addClass("display-none");
        }

        if(search_scope.resultAddrSearchADDRESS.cadNum == null) {
            $("#cost-table-addr").text("Кадастровая стоимость 1 кв. м. земель оценочных зон на " + search_scope.resultAddrSearchADDRESS.searchDate);
        }else{
            $("#cost-table-addr").text("Кадастровая стоимость 1 кв. м. земельного участка по состоянию на " + search_scope.resultAddrSearchADDRESS.searchDate);
        }
    }

    $("#addr-table").append(function() {
        var text = "";
        var i=0;
        for(;i<search_scope.resultAddrSearchADDRESS.valuesCharacters.length; i++) {
            text+="<tr>"
                + "<td>"
                + search_scope.resultAddrSearchADDRESS.valuesCharacters[i].funcCodeName
                + "</td>"
                + "<td>"
                + search_scope.resultAddrSearchADDRESS.valuesCharacters[i].dateVal
                + "</td>"
                + "<td>"
                + search_scope.resultAddrSearchADDRESS.valuesCharacters[i].zoneNum
                + "</td>"
            if(!search_scope.isNalog) {
                text += "<td>"
                    + search_scope.resultAddrSearchADDRESS.valuesCharacters[i].costD
                    + "</td>"
                    + "<td>"
                    + search_scope.resultAddrSearchADDRESS.valuesCharacters[i].costR
                    + "</td>";
            }
            if(search_scope.isNalog){
                text+="<td>"
                    + search_scope.resultAddrSearchADDRESS.valuesCharacters[i].costNB
                    + "</td>"
            }
                text+= "</tr>"
        }
        return text;
    });
    $("#text-addr").append(function(){
        var j=0;
        var text = search_scope.resultAddrSearchADDRESS.remark;
        return text;
    });
}

function onCadastrAddr(soato, blockNum, parceNum){
    $("#addr-2-view").removeAttr("disabled");
    cadNum.soato = soato;
    cadNum.blockNum = blockNum;
    cadNum.parceNum = parceNum;
}

function downloadImg(){
    $( "#dowmload-image" ).removeClass("display-none");
    $("#panelka").addClass("download-background")
    $( "#dialog" ).dialog();
    $(".ui-dialog-title").addClass("display-none");
    $(".ui-draggable").addClass("display-none");
    $(".ui-dialog-titlebar").addClass("display-none");
    $(".ui-widget-content").addClass("ui-widget-content_1");
    return true;
}

function downloadImgClose(){
    $("#dowmload-image").addClass("display-none");
    $("#panelka").removeClass("download-background")
    $("#dialog").dialog("close");
}


function removeDisabled_2(){
    $("#search-unp-2").removeClass("disabled-div");
    $("#sh-search-2-unp").removeAttr("disabled");
    $("#sh-search-2-name").removeAttr("disabled");
    $("#sh-search-2-zone").removeAttr("disabled");
}

function addDisabled_2(){
    $("#search-unp-2").addClass("disabled-div");
    $("#sh-search-2-unp").attr("disabled", "disabled");
    $("#sh-search-2-name").attr("disabled", "disabled");
    $("#sh-search-2-zone").attr("disabled","disabled");
}

//ПОИСК 2
function clearPoUNP(){
    $("#search-obl3").val(0).change();
    $("#search-raion3").empty();
    $("#search-UNP").val("");
}

function clearPoName(){
    $("#search-obl5").val(0).change();
    $("#search-raion5").empty();
    $("#search-name_zemlepol").empty();
}

function clearPoNun(){
    $("#search-obl4").val(0).change();
    $("#search-raion4").empty();
    $("#search-num_oc_zone").empty();
}

function clear_all_2(){
    clearPoUNP();
    clearPoName();
    clearPoNun();
}

function setLastSelect(id){
    id = id +" option"
    var size = $(id).size()-2;
    console.log(size)
    $(id).val(2).change();
}

function setFirstSelect(id){
    $(id).val(0).change();
}

function dateNotNow(date_all){

    var date_user = date_all.split('-');
    var date = new Date(parseInt(date_user[0]), parseInt(date_user[1])-1, parseInt(date_user[2]))
    var date_now = new Date();

    var valid_date = true;

    if (date > date_now) {
        valid_date = false;
        alert("Дата оценки не может быть позже текущей.")
    }
    return valid_date
}


function addCadastrPdf(){
    $("#pdf-cadastr").empty();
    $("#pdf-cadastr").append(function(){
        return createPDFImg(search_scope.pdfUrl)
    })
}

function validSOATO(){
    var soato = $("#soato").val().length,
        numblock = $("#numblock").val().length,
        numuch = $("#numuch").val().length;

    if(soato == 10 && numblock != 0 && numuch != 0){
        return true;
    }
    return false;
}

function isSearchCadastr(){
    if(validSOATO()){
      $("#cadastr-search-btn").removeAttr("disabled");
    } else{
        $("#cadastr-search-btn").attr("disabled","disabled");
    }
}

function displZvezda(){
    if($(".zvezda").hasClass('display-none')){
        $(".zvezda").removeClass('display-none')
    }
}

function noDisplZvezda(){
    if(!$(".zvezda").hasClass('display-none')){
        $(".zvezda").addClass('display-none')
    }
}

function noDispRub(){
    if(!$(".rub-dol").hasClass('display-none')){
        $(".rub-dol").addClass('display-none')
    }
}

function dispRub(){
    if($(".rub-dol").hasClass('display-none')){
        $(".rub-dol").removeClass('display-none')
    }
}

function delete_pdf_document(search_scope) {
    httpServices.deletePdf(search_scope);
    search_scope.pdfUrl = null;
    pdf_created = false;
}