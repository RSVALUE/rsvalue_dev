/**
 * Created by Shcherbina on 05.11.2016.
 */


function closeAllBlock(){
    if(!$("#for-jurnal-zakaz").hasClass("display-none")) {
        $("#for-jurnal-zakaz").addClass("display-none");
    }
    if(!$("#for-preiskyrant").hasClass("display-none")) {
        $("#for-preiskyrant").addClass("display-none");
    }
    if(!$("#for-otchet").hasClass("display-none")) {
        $("#for-otchet").addClass("display-none");
    }
    if(!$("#for-statictika").hasClass("display-none")) {
        $("#for-statictika").addClass("display-none");
    }
}

function closeButtons(){
    if(!$("#buttons-get-doc").hasClass("display-none")){
        $("#buttons-get-doc").addClass("display-none");
    }
}

function openButtons(){
    if($("#buttons-get-doc").hasClass("display-none")){
        $("#buttons-get-doc").removeClass("display-none");
    }
}