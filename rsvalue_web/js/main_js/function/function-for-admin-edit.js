/**
 * Created by tihonovichp on 21.11.2016.
 */

var edit_admin_scope = [];
var edit_record_count = 5;
var edit_row_count;
var edit_filter = [];
var edit_user_data = [];

//Создание таблицы редактирования. Передается edit_admin_scope.accounts
function create_edit_table(data) {
    $('#edit_tbody').empty();
    for (var i = 0; i < data.length; i++) {
        tr = $('<tr id=edit_' + i + ' data-toggle="modal" data-target="#edit_modal"/>');
        tr.append("<td colspan='3'>" + data[i][3] + " " + data[i][4] + " " + data[i][5] + " " + "</td>");
        tr.append("<td>" + data[i][6] + "</td>");
        tr.append("<td>" + data[i][2] + "</td>");
        tr.append("<td>" + data[i][7] + "</td>");
        $('#edit_tbody').append(tr);
    }
    create_edit_pagination();
}

//Создание пагинации для таблицы.
function create_edit_pagination() {
    var page_count = Math.ceil(edit_row_count / edit_record_count);
    var $edit_page = $('#edit_pagination');
    $edit_page.empty();
    if (page_count != 1) {
        for (var i = 0; i < page_count; i++) {
            $edit_page.append("<li><a href='#' class='page-link'>" + (i + 1) + "</a></li>");
        }
        edit_page_click();
    }
}

//Установка значений полей, в поля модального окна передается edit_admin_scope.accounts и id элемента
function set_modal_fields(data, id) {
    $("#edit_modal_fio").val(data[id][3] + " " + data[id][4] + " " + data[id][5])
    $("#edit_modal_username").val(data[id][6]);
    $("#edit_modal_password").val("");
    $("#edit_modal_position").val(data[id][1]);
    $("#edit_modal_id").val("modal_" + data[id][0]);
    $("#edit_modal_org").val(data[id][2]);
    $("#edit_modal_group").val(data[id][7]);
    saved_username = data[id][6].toLowerCase();
}

//Создание события нажатия на номер страницы
function edit_page_click() {
    $('.edit-pagination li a').click(function (e) {
        var page_number = $(e.target).text();
        edit_table_change(page_number);
    });
}

//Необходимые процедуры для обновления таблицы
function edit_table_change(page) {
    if (edit_filter_check()) {
        edit_row_count = row_count;
        httpServices.get_accounts_records(edit_admin_scope, page, edit_record_count);
    } else {
        httpServices.set_edit_filter(edit_filter, page, edit_record_count);
    }
    create_edit_table(edit_admin_scope.accounts);
}

//Установка значения фильтров
function set_edit_filter_fields() {
    edit_filter.surname = $("#edit_surname_filter").val().toLowerCase();
    edit_filter.login = $("#edit_login_filter").val().toLowerCase();
    edit_filter.org = $("#edit_org_filter").val().toLowerCase();
    edit_filter.group = $("#edit_group_filter").val().toLowerCase();
}

//Проверка, установлены ли какие-либо фильтры
function edit_filter_check() {
    if ($("#edit_surname_filter").val() == "" && $("#edit_login_filter").val() == "" && $("#edit_org_filter").val() == "" && $("#edit_group_filter").val() == "") {
        return true;
    }
}
