/**
 * Created by tihonovichp on 08.11.2016.
 */

var admin_scope = [];
var user_data = [];
var logins = [];
var saved_username;
var select_org_id;
var select_group_id;


var FULLNAME_REGEX = new RegExp('^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$', 'i');


function create_groups(data) {
    for (var i = 0; i < data.length; i++) {
        $('#add_group').append('<option id="group_' + data[i].groupId + '">' + data[i].groupName + '</option>');
        $('#delete_group_search').append('<option id="delete_group_' + data[i].groupId + '">' + data[i].groupName + '</option>');
        $('#edit_group_search').append('<option id="edit_group_' + data[i].groupId + '">' + data[i].groupName + '</option>');
    }
}

function create_ogrs(data) {
    for (var i = 0; i < data.length; i++) {
        $('#add_org').append('<option id="tor_'+ data[i].analyticCode +'">' + data[i].codeName + '</option>');
        $('#delete_org_search').append('<option id="delete_tor_'+ data[i].analyticCode +'">' + data[i].codeName + '</option>');
        $('#edit_org_search').append('<option id="edit_tor_'+ data[i].analyticCode +'">' + data[i].codeName + '</option>');
    }
}

function create_subjects(data) {
    for (var i = 0; i < data.length; i++) {
        $('#fio_list').append('<option>' + data[i].surname + ' ' + data[i].firstName + ' ' + data[i].fatherName + ' ' + '</option>');
        $('#edit_fio_list').append('<option>' + data[i].surname + ' ' + data[i].firstName + ' ' + data[i].fatherName + ' ' + '</option>');
    }
}

function clean_fields() {
    $('.clean').val('');
    $('#add_org').val(admin_scope.tors[0].codeName);
    $('#add_group').val(admin_scope.groups[0].groupName);
}

function set_user_data() {
    user_data.fio = $('#add_fio').val();
    user_data.username = $('#add_username').val();
    user_data.password = $('#add_password').val();
    user_data.position = $('#add_position').val();
    user_data.organization = select_org_id;
    user_data.group = select_group_id;
}

function set_edit_user_data() {
    edit_user_data.id = $('#edit_modal_id').val().substr(6);
    edit_user_data.fio = $('#edit_modal_fio').val();
    edit_user_data.username = $('#edit_modal_username').val();
    edit_user_data.password = $('#edit_modal_password').val();
    edit_user_data.position = $('#edit_modal_position').val();
    // edit_user_data.organization = $('#edit_modal_org').val();
    // edit_user_data.group = $('#edit_modal_group').val();
}

function check_disable() {
    var $save = $("#save_user");
    if (!($save.hasClass("disabled_fio") || $save.hasClass("disabled_login"))) {
        $save.removeAttr("disabled");
    }
}

function check_edit_disable() {
    var $save = $("#modal-save")
    if (!($save.hasClass("disabled_edit_fio") || $save.hasClass("disabled_edit_login"))) {
        $save.removeAttr("disabled");
    }
}

function add_fio_event() {
    $("#wrong_fio").remove();
    var user_fio = $("#add_fio").val().toLowerCase();
    if (user_fio.match(FULLNAME_REGEX) == null) {
        $("#div_fio").append('<p class="form-control clean" id="wrong_fio">Неправильный формат ввода</p>')
        $("#save_user").attr("disabled", "disabled").addClass("disabled_fio");
    } else {
        $("#wrong_fio").remove();
        $("#save_user").removeClass("disabled_fio");
        check_disable();
    }
}

function edit_fio_event() {
    $("#wrong_edit_fio").remove();
    var user_fio = $("#edit_modal_fio").val().toLowerCase();
    if (user_fio.match(FULLNAME_REGEX) == null) {
        $("#edit_div_fio").append('<p class="form-control clean" id="wrong_edit_fio">Неправильный формат ввода</p>')
        $("#modal-save").attr("disabled", "disabled").addClass("disabled_edit_fio");
    } else {
        $("#wrong_edit_fio").remove();
        $("#modal-save").removeClass("disabled_edit_fio");
        check_edit_disable();
    }
}

function init_logins() {
    for (var i = 0; i < admin_scope.logins.length; i++) {
        logins[i] = admin_scope.logins[i].toLowerCase();
    }
}

function check_add_fields() {
    if ($("#add_fio").val() == '' || $("#add_username").val() == '' || $("#add_password").val() == '' ||  $("#add_position").val() == '') {
        return true;
    }
}

function check_update_fields() {
    if ($("#edit_pass_check").prop("checked")) {
        if ($("#edit_modal_fio").val() == '' || $("#edit_modal_username").val() == '' || $("#edit_modal_password").val() == '' || $("#edit_modal_position").val() == '') {
            return true;
        }
    } else {
        if ($("#edit_modal_fio").val() == '' || $("#edit_modal_username").val() == '' || $("#edit_modal_position").val() == '') {
            return true;
        }
    }
}

function check_update_fields_pass() {
    if ($("#edit_modal_username").val() == '' || ($("#edit_modal_password").val() == ''&& $("#edit_pass_check").is(":checked"))){
        return true;
    }
}