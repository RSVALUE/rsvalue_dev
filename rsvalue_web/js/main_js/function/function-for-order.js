/**
 * Created by tihonovichp on 21.12.2016.
 */

var order_scope = [];
var responseData = [];
var order_filter = {
    "orgId": "",
    "urgency": "",
    "decl": "",
    "isNb": "",
    "orderState": "",
    "filterUserId": "",
    "sort": 1,
    "sortType": "desc",
    "page": 1,
    "qty": 5
};

var order_object = {
    "orderId": null,
    "searchType": null,
    "date": "",
    "distrId": null,
    "parcelId": null,
    "orgId": null,
    "addressId": null,
    "prevAddress": "",
    "coordinates": "",
    "sId": null
};

var order_load = 0;

var current_order_id,
    current_object_id,
    current_order,
    current_object,
    current_cadnum_id,
    current_st_cadnum_id,
    current_sx_id;

var TYPE_ORDER_SEARCH = 10;

var order_data = {
    "orderId": null,
    "userId": null,
    "orderNumber": "",
    "urgency": null,
    "isNb": null,
    "declType": null,
    "declName": "",
    "unp": "",
    "nkName": "",
    "phoneNumber": "",
    "email": "",
    "address": "",
    "index": "",
    "ate": "",
    "numIsx": "",
    "dateIsx": "",
    "numVx": "",
    "dateVx": ""
};

var urgency_states = {
    "urg": "Срочный",
    "not_urg": "Несрочный"
};

var clickTrash = false;

function clear_ur_div() {
    $("#order_unp").val('');
    $("#order_ur_name").val('');
}

function clear_phys_div() {
    $("#order_fio").val('');
    $("#order_notarius").val('');
}

function clear_notaruis_check() {
    $("#order_notaruis_check").prop("checked", false);
    $("#order_notarius_div").attr("hidden", "hidden");
}

function clear_client_div() {
    $("#order_telephone_number").val('');
    $("#order_email").val('');
    $("#order_address").val('');
    $("#order_index").val('');
    $("#order_ate_te").val('');
}

function clear_request_div() {
    $("#order_number").val('');
    $("#order_number_orig").val('');
    $("#order_date_orig").val('');
    $("#order_number_inc").val('');
    $("#order_date_inc").val('');
}

function clear_additional_div() {
    $("#order_urgency").prop("checked", false);
    $("#order_tax").prop("checked", false);
}

function clear_all_orders() {
    clear_ur_div();
    clear_phys_div();
    clear_notaruis_check();
    clear_client_div();
    clear_request_div();
    clear_additional_div();
}

function init_order_data() {
    order_data.userId = AUTHOBJ.userId;
    order_data.orderNumber = $("#order_number").val();
    order_data.declType = check_decl_type();
    if (order_data.declType == 31) {
        order_data.declName = $("#order_fio").val();
        if ($("#order_notaruis_check").prop("checked")) {
            order_data.nkName = $("#order_notarius").val();
        }
    }
    if (order_data.declType == 32) {
        order_data.unp = $("#order_unp").val();
        order_data.declName = $("#order_ur_name").val();
    }
    order_data.phoneNumber = $("#order_telephone_number").val();
    order_data.email = $("#order_email").val();
    order_data.address = $("#order_address").val();
    order_data.index = $("#order_index").val();
    order_data.ate = $("#order_ate_te").val();
    order_data.numIsx = $("#order_number_orig").val();
    order_data.dateIsx = $("#order_date_orig").val();
    order_data.numVx = $("#order_number_inc").val();
    order_data.dateVx = $("#order_date_inc").val();
    order_data.urgency = check_urgency();
    order_data.isNb = check_nb();
}

function check_decl_type() {
    if ($("#phys_lico").prop("checked")) {
        return 31;
    }
    if ($("#ur_lico").prop("checked")) {
        return 32;
    }
}

function check_urgency() {
    if ($("#order_urgency").prop("checked")) {
        return 11;
    } else {
        return 12;
    }
}

function check_nb() {
    if ($("#order_tax").prop("checked")) {
        return 21;
    } else {
        return 22;
    }
}

function init_func_nazn() {
    var data = search_scope.searchVNP_fun;
    var size = search_scope.searchVNP_fun.length;
    $("#search-fn-snp-order").empty().append("<option value='-1'></option>");
    $("#search-fn-st-notar-order").empty().append("<option value='-1'></option>");
    for (var i = 0; i < size; i++) {
        $("#search-fn-snp-order").append("<option value='" + data[i].analyticCode + "'>" + data[i].codeName + "</option>");
        $("#search-fn-st-notar-order").append("<option value='" + data[i].analyticCode + "'>" + data[i].codeName + "</option>");
    }
}

function create_order_table() {
    load_order_table();
    load_tor_users("");
    get_order_pagination();
    set_listeners_order_table();
    setStatus();
}

function reload_order_table() {
    httpServices.get_orders_list(order_filter);
    load_order_table();
    get_order_pagination();
    set_listeners_order_table();
    setStatus();
}

function setStatus() {
    for (var i = 0; i < order_scope.orders.orderList.length; i++) {
        $("#order-" + i).val(order_scope.orders.orderList[i].stateName);
    }
}
function load_order_table() {
    $("#order_table_body").empty();
    var size = order_scope.orders.orderList.length;
    for (var i = 0; i < size; i++) {
        tr = $('<tr class="cursor-select" id="order_id_' + order_scope.orders.orderList[i].orderId + '"/>');
        tr.append("<td style='text-align: center'>" + order_scope.orders.orderList[i].orderNumber + "</td>");
        tr.append("<td style='text-align: center'>" + create_urgency_td(order_scope.orders.orderList[i].urgName) + "</td>");
        tr.append("<td>" + create_date(order_scope.orders.orderList[i].startDate) + "</td>");
        tr.append("<td>" + order_scope.orders.orderList[i].declName + "</td>");
        tr.append("<td style='text-align: center'>" + order_scope.orders.orderList[i].invoiceNum + "</td>");
        tr.append("<td style='text-align: center'>" + check_null_value(order_scope.orders.orderList[i].invoiceDate) + "</td>");
        tr.append("<td style='text-align: center'>" + order_scope.orders.orderList[i].nbName + "</td>");
        if (equals(order_scope.orders.orderList[i].stateName, order_scope.states[0].name)) {
            tr.append("<td class='order_select'>" + create_states_inwork(order_scope.orders.orderList[i].orderId, i) + "</td>");
        } else if (equals(order_scope.orders.orderList[i].stateName, order_scope.states[1].name)) {
            tr.append("<td class='order_select'>" + create_states_done_without_pay(order_scope.orders.orderList[i].orderId, i) + "</td>");
        } else {
            tr.append("<td class='order_select'>" + order_scope.orders.orderList[i].stateName + "</td>");
        }
        tr.append("<td>" + order_scope.orders.orderList[i].userName + "</td>");

        $("#order_table_body").append(tr);
    }
}

function create_urgency_td(object) {
    if (equals(object, urgency_states.urg)) {
        return "<label class='glyphicon glyphicon-flag urgency'></label>";
    }
    if (equals(object, urgency_states.not_urg)) {
        return "<label class='not_urgency'></label>";
    }
}

function create_date(object) {
    var date = object.split(" ")[0];
    return date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0];
}

function create_states_inwork(id, pos) {
    var order_id = "order_id_state_" + id;
    return select = "<select class='A' id='" + order_id + "'>"
        + "<option id='order_state_" + order_scope.states[0].id + "_" + pos + "' selected>" + order_scope.states[0].name + "</option>"
        + "<option id='order_state_" + order_scope.states[3].id + "_" + pos + "'>" + order_scope.states[3].name + "</option>"
        + "</select>";
}

function create_states_done_without_pay(id, pos) {
    var order_id = "order_id_state_" + id;
    return select = "<select class='A' id='" + order_id + "'>"
        + "<option id='order_state_" + order_scope.states[1].id + "_" + pos + "' selected>" + order_scope.states[1].name + "</option>"
        + "<option id='order_state_" + order_scope.states[2].id + "_" + pos + "'>" + order_scope.states[2].name + "</option>"
        + "</select>";
}

function set_state_change_listeners() {
    for (var i = 0; i < order_scope.orders.orderList.length; i++) {
        if (order_scope.orders.orderList[i].stateId == 0 || order_scope.orders.orderList[i].stateId == 1 || order_scope.orders.orderList[i].stateId == 2) {
            var id = "order_id_state_" + order_scope.orders.orderList[i].orderId;
            document.getElementById(id).addEventListener('change', state_change_listener, false);
        }
    }
}

function state_change_listener() {
    var orderId = $(this).attr("id").split('_')[3];
    var stateId = $(this).children(":selected").prop("id").split("_")[2];
    var elemId = $(this).children(":selected").prop("id").split("_")[3];
    if (confirm("Изменить состояние заказа?")) {
        httpServices.change_order_state(orderId, stateId, AUTHOBJ.userId);
        if (order_scope.change_state == -1) {
            $(this).val(order_scope.orders.orderList[elemId].stateName);
        } else {
            reload_order_table();
        }
    } else {
        $(this).val(order_scope.orders.orderList[elemId].stateName);
    }
}

function check_null_value(object) {
    if (object == null) {
        return object = "";
    }
}

function get_order_li(col) {
    var li = "<li><a  id=\"prev-str-table-order\">&laquo;</a></li>";
    for (var i = 1; i <= col; i++) {
        if (i == 1) {
            if (order_filter.page == 1) {
                li += "<li id=\"" + i + "-table-order\" class='active'><a>" + i + "</a></li>";
            } else {
                li += "<li id=\"" + i + "-table-order\"><a>" + i + "</a></li>";
            }
        } else {
            li += "<li id=\"" + i + "-table-order\"><a>" + i + "</a></li>";
        }
    }
    li += "<li id=\"next-str-table-order\"><a>&raquo;</a></li>";
    return li;
}

function get_order_pagination() {
    $("#order_table_pagination").empty();
    $("#order_table_pagination").append(function () {
        return get_order_li(order_scope.orders.pages);
    })
}

function set_listeners_order_table() {
    $.each(order_scope.orders.orderList, function (i, obj) {
        document.getElementById("order_id_" + obj.orderId).addEventListener('click', click_order_event, false);
    });
    for (var i = 1; i <= order_scope.orders.pages; i++) {
        var id = i + "-table-order";
        document.getElementById(id).addEventListener('click', order_pagination_event, false);
    }
    document.getElementById('prev-str-table-order').addEventListener('click', order_pagination_event, false);
    document.getElementById('next-str-table-order').addEventListener('click', order_pagination_event, false);
    set_state_change_listeners();
}

function order_pagination_event(e) {
    reset_order_edit_process_buttons();
    var id = $(this).attr("id").split('-')[0];
    var id_event = "#" + $(this).attr("id");
    var page_prew = order_filter.page;
    var id_prew = "#" + page_prew + "-table-order";
    var all_page = order_scope.orders.pages;
    if (id == 'prev') {
        if (page_prew != 1) {
            $(id_prew).removeClass("active");
            order_filter.page = page_prew - 1;
            reload_order_table();
            $("#" + order_filter.page + "-table-order").addClass("active");
        }
    } else if (id == 'next') {
        if (page_prew < all_page) {
            $(id_prew).removeClass("active");
            order_filter.page = page_prew + 1;
            reload_order_table();
            $("#" + order_filter.page + "-table-order").addClass("active");
        }
    } else {
        $(id_prew).removeClass("active");
        order_filter.page = parseInt(id);
        reload_order_table();
        $(id_event).addClass("active");
    }
}

function click_order_event(e) {
    var id = ($(this).attr("id")).split('_')[2];
    var sender = e.target;
    if (!equals(sender.tagName, "SELECT")) {
        if ($(this).hasClass('selected-tr')) {
            $(this).removeClass('selected-tr');
            $("#order_edit_order").prop('disabled', 'disabled');
            $("#order_process_order").prop('disabled', 'disabled');
            current_order_id = "";
            current_order = "";
        } else {
            $(this).siblings().removeClass('selected-tr');
            $(this).addClass('selected-tr');
            $("#order_edit_order").removeProp('disabled');
            $("#order_process_order").removeProp('disabled');
            current_order_id = parseInt(id);
            current_order = order_scope.orders.orderList[$(this).index()];
        }
    }
}

function load_tor_users(torId) {
    httpServices.get_user_list(AUTHOBJ.userId, torId);
    $("#order_specialist_filter").empty().append('<option id="tor_user_id_0' + '">Все</option>');
    for (var i = 0; i < order_scope.users.length; i++) {
        $("#order_specialist_filter").append('<option id="tor_user_id_' + order_scope.users[i].id + '">' + order_scope.users[i].fullName + '</option>')
    }
}

function closeAllBlockOrder() {
    if (!$("#new-order").hasClass("display-none")) {
        $("#new-order").addClass("display-none");
    }
    if (!$("#work-with-order").hasClass("display-none")) {
        $("#work-with-order").addClass("display-none");
    }
}

function openJurnalOrder() {
    if (!$("#main-order").hasClass("display-none")) {
        $("#main-order").removeClass("display-none");
    }
}

function open_order() {
    if (!$("#main-order").hasClass("display-none")) {
        $("#main-order").addClass("display-none")
    }
    if (!$("#toMenu").hasClass("display-none")) {
        $("#toMenu").addClass("display-none")
    }
    $("#toOrder").removeClass("display-none");
    $("#new-order").removeClass("display-none");
}

function close_order() {
    if (!$("#new-order").hasClass("display-none")) {
        $("#new-order").addClass("display-none")
    }
    if (!$("#work-with-order").hasClass("display-none")) {
        $("#work-with-order").addClass("display-none")
    }
    if (!$("#toOrder").hasClass("display-none")) {
        $("#toOrder").addClass("display-none")
    }
    $("#toMenu").removeClass("display-none");
    $("#main-order").removeClass("display-none");
    clear_all_orders();
    enable_all_fields();
    $("#order_edit_order").prop("disabled", "disabled");
    $("#order_process_order").prop("disabled", "disabled");
    $("#order_table_body>tr.selected-tr").removeClass("selected-tr");
    order_scope.editable_order = null;
}

function open_process_order() {
    if (!$("#toMenu").hasClass("display-none")) {
        $("#toMenu").addClass("display-none")
    }
    $("#toOrder").removeClass("display-none");
    $("#work-with-order").removeClass("display-none");
    $("#main-order").addClass("display-none");
}

function open_order_edit() {
    init_edit_order_data();
    open_order();
}

function init_edit_order_data() {
    var order = order_scope.editable_order;
    $("#order_number").val(order.orderNumber);
    if (order.declType == 31) {
        $("#phys_div").css('display', 'block');
        $("#ur_div").css('display', 'none');
        $("#phys_lico").prop("checked", "checked");
        $("#ur_lico").removeProp("checked");
        $("#order_fio").val(order.declName);
        if (order.nkName != null) {
            $("#order_notaruis_check").prop("checked", "checked");
            $("#order_notarius_div").css('display', 'block').removeProp("hidden");
            $("#order_notarius").val(order.nkName);
        }
    }
    if (order.declType == 32) {
        $("#ur_div").css('display', 'block');
        $("#phys_div").css('display', 'none');
        $("#ur_lico").prop("checked", "checked");
        $("#phys_lico").removeProp("checked");
        $("#order_unp").val(order.unp);
        $("#order_ur_name").val(order.declName);
    }
    $("#order_telephone_number").val(order.phoneNumber);
    $("#order_email").val(order.email);
    $("#order_address").val(order.address);
    $("#order_index").val(order.index);
    $("#order_ate_te").val(order.ate);
    $("#order_number_orig").val(order.numIsx);
    $("#order_date_orig").val(order.dateIsx);
    $("#order_number_inc").val(order.numVx);
    $("#order_date_inc").val(order.dateVx);
    if (order.nbType == 21) {
        $("#order_tax").prop("checked", "checked");
    }
    if (order.urgency == 11) {
        $("#order_urgency").prop("checked", "checked");
    }
}

function open_order_view() {
    open_order();
    init_edit_order_data();
    disabled_all_fields();
}

function disabled_all_fields() {
    $("#order_number").prop("readonly", "readonly");
    $("#order_fio").prop("readonly", "readonly");
    $("#order_notarius").prop("readonly", "readonly");
    $("#order_unp").prop("readonly", "readonly");
    $("#order_ur_name").prop("readonly", "readonly");
    $("#order_telephone_number").prop("readonly", "readonly");
    $("#order_email").prop("readonly", "readonly");
    $("#order_address").prop("readonly", "readonly");
    $("#order_index").prop("readonly", "readonly");
    $("#order_ate_te").prop("readonly", "readonly");
    $("#order_number_orig").prop("readonly", "readonly");
    $("#order_date_orig").prop("readonly", "readonly");
    $("#order_number_inc").prop("readonly", "readonly");
    $("#order_date_inc").prop("readonly", "readonly");
    $("#phys_lico").prop("disabled", "disabled");
    $("#ur_lico").prop("disabled", "disabled");
    $("#order_notaruis_check").prop("disabled", "disabled");
    $("#order_tax").prop("disabled", "disabled");
    $("#order_urgency").prop("disabled", "disabled");
    $("#order_save").prop("disabled", "disabled");
    $("#order_save_and_process").prop("disabled", "disabled");
}

function enable_all_fields() {
    $("#order_number").removeProp("readonly");
    $("#order_fio").removeProp("readonly");
    $("#order_notarius").removeProp("readonly");
    $("#order_unp").removeProp("readonly");
    $("#order_ur_name").removeProp("readonly");
    $("#order_telephone_number").removeProp("readonly");
    $("#order_email").removeProp("readonly");
    $("#order_address").removeProp("readonly");
    $("#order_index").removeProp("readonly");
    $("#order_ate_te").removeProp("readonly");
    $("#order_number_orig").removeProp("readonly");
    $("#order_date_orig").removeProp("readonly");
    $("#order_number_inc").removeProp("readonly");
    $("#order_date_inc").removeProp("readonly");
    $("#ur_lico").removeProp("disabled").prop("checked", "checked");
    $("#phys_lico").removeProp("disabled").removeProp("checked");
    $("#order_notaruis_check").removeProp("disabled");
    $("#order_tax").removeProp("disabled");
    $("#order_urgency").removeProp("disabled");
    $("#order_save").removeProp("disabled");
    $("#order_save_and_process").removeProp("disabled");
    $("#ur_div").css('display', 'block');
    $("#phys_div").css('display', 'none');
    $("#order_notarius_div").css('display', 'none');
}

function set_active_page() {
    var id = order_filter.page + "-table-order";
    $('#' + id + '').addClass("active");
}

function check_required_fields() {
    var bool = true;
    if ($("#order_number").val() == '') {
        bool = false;
    } else if (check_decl_type() == 31) {
        if ($("#order_fio").val() == '' || $("#order_number_inc").val() == '' || $("#order_date_inc").val() == '') {
            bool = false;
        }
    } else if (check_decl_type() == 32) {
        if ($("#order_ur_name").val() == '' || $("#order_number_inc").val() == '' || $("#order_date_inc").val() == '' || $("#order_number_orig").val() == '' || $("#order_date_orig").val() == '') {
            bool = false;
        }
    }
    return bool;
}

function check_tor() {
    var value = $("#order_select_state").val();
    return !(AUTHOBJ.torId != value && -1 != value);
}

function closeAllOrdersSearch() {
    if (!$("#search-addr-1-order").hasClass("display-none")) {
        $("#search-addr-1-order").addClass("display-none");
    }
    if (!$("#order-cadastr").hasClass("display-none")) {
        $("#order-cadastr").addClass("display-none");
    }
    if (!$("#order-st").hasClass("display-none")) {
        $("#order-st").addClass("display-none");
    }
    if (!$("#order-mesto").hasClass("display-none")) {
        $("#order-mesto").addClass("display-none");
    }
    if (!$("#order-sh").hasClass("display-none")) {
        $("#order-sh").addClass("display-none");
    }
    if (!$("#order-snp").hasClass("display-none")) {
        $("#order-snp").addClass("display-none");
    }
    if (!$("#order-st-notar").hasClass("display-none")) {
        $("#order-st-notar").addClass("display-none");
    }
    if (!$("#search-addr-1-order").hasClass("display-none")) {
        $("#search-addr-1-order").addClass("display-none")
    }
    clearAllFieldsOrder();
}

function get_normal_date() {
    var date = $("#date-order-search").val();
    return date.split("-")[2] + "." + date.split("-")[1] + "." + date.split("-")[0];
}

function init_cadnum_order_result() {
    var data = order_scope.order_process_search;
    if (data == undefined) {
        hide_cadnum_order_result();
    } else {
        var size = data.length;
        for (var i = 0; i < size; i++) {
            var id = data[i].orgId + "-" + data[i].parcelUID,
                info = data[i].info.split(";")[1] + ", " + data[i].info.split(";")[0];
            tr = $('<tr class="cursor-select" id="' + 'cadnum-search-' + id + '"/>');
            tr.append("<td>" + info + "</td>");
            tr.append("<td>" + data[i].square + "</td>");
            tr.append("<td>" + data[i].purposeName + "</td>");
            $("#order-res-search-body").append(tr);
        }
        cadnum_order_result_listener();
    }
}

function cadnum_order_result_listener() {
    var data = order_scope.order_process_search,
        size = data.length;
    for (var i = 0; i < size; i++) {
        var id = data[i].orgId + "-" + data[i].parcelUID;
        document.getElementById("cadnum-search-" + id).addEventListener('click', setListenerTableOrder, false);
    }
}

function init_info_result(data) {
    if (data == undefined) {
        hide_sx_order_result();
    } else {
        var size = data.length;
        for (var i = 0; i < size; i++) {
            var tr = $('<tr class="cursor-select" id="' + 'sx-result-' + data[i].id + '"/>');
            tr.append("<td>" + data[i].info + "</td>");
            tr.append("<td></td>");
            tr.append("<td></td>");
            $("#order-res-sx-search-body").append(tr);
        }
        sx_order_result_listener(data);
    }
}

function sx_order_result_listener(data) {
    var size = data.length;
    for (var i = 0; i < size; i++) {
        document.getElementById('sx-result-' + data[i].id).addEventListener('click', setListenerSXResult, false);
    }
}

function setListenerSXResult() {
    if ($(this).hasClass('selected-tr')) {
        $(this).removeClass('selected-tr');
        current_sx_id = null;
    } else {
        $(this).siblings().removeClass('selected-tr');
        $(this).addClass('selected-tr');
        current_sx_id = $(this).prop('id');
    }
}

function clear_cadnum_search_result() {
    $("#order-res-search-body").empty();
}

function clear_sx_search_result() {
    $("#order-res-sx-search-body").empty();
}

function show_cadnum_order_result() {
    $("#order-res-search").removeClass("display-none");
}

function show_sx_order_result() {
    $("#order-res-sx-search").removeClass("display-none");
}

function hide_cadnum_order_result() {
    $("#order-res-search").addClass("display-none");
}

function hide_sx_order_result() {
    $("#order-res-sx-search").addClass("display-none");
}

function init_process_order() {
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    $("#search_tabs").removeClass("disabled-div");
    $("#zone_tabs").removeClass("disabled-div");
    if (!$("#create-doc").hasClass('disabled-div')) {
        $("#create-doc").addClass('disabled-div');
    }
    if (!$("#change-location").hasClass("disabled-div")) {
        $("#change-location").addClass("disabled-div");
    }
    if (!$("#reset-location").hasClass("disabled-div")) {
        $("#reset-location").addClass("disabled-div");
    }
    $("#search-obl-st-order").empty().append("<option value='-1'></option>");
    $("#order-obl3").empty().append("<option value='-1'></option>");
    $("#order-obl4").empty().append("<option value='-1'></option>");
    $("#search-order-obl5").empty().append("<option value='-1'></option>");
    $("#search-obl-snp-order").empty().append("<option value='-1'></option>");
    $("#search-obl-mesto-order").empty().append("<option value='-1'></option>");
    $("#search-obl-st-notar-order").empty().append("<option value='-1'></option>");
    $("#1-search-by-addr-abl-select-order").empty().append("<option value='-1'></option>");
    for (var i = 0; i < size; i++) {
        $("#search-obl-st-notar-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
        $("#search-obl-st-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
        $("#order-obl3").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
        $("#order-obl4").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
        $("#search-order-obl5").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
        $("#search-obl-snp-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
        $("#search-obl-mesto-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
        $("#1-search-by-addr-abl-select-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_process_order_edit() {
    $("#search_tabs").addClass("disabled-div");
    $("#zone_tabs").addClass("disabled-div");
    if (!$("#create-doc").hasClass('disabled-div')) {
        $("#create-doc").addClass('disabled-div');
    }
    if (!$("#change-location").hasClass("disabled-div")) {
        $("#change-location").addClass("disabled-div");
    }
    if (!$("#save-all").hasClass("disabled-div")) {
        $("#save-all").addClass("disabled-div");
    }
    if (!$("#reset-location").hasClass("disabled-div")) {
        $("#reset-location").addClass("disabled-div");
    }
}

function init_st_areas() {
    $("#search-raion-st-order").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#search-raion-st-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_st() {
    $("#search-name-st-order").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_st_list;
    var size = order_scope.order_process_st_list.length;
    for (var i = 0; i < size; i++) {
        $("#search-name-st-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_st_cadnum_table() {
    $("#order_st_cadnum_table").empty();
    var data = order_scope.order_st_process_search;
    var size = order_scope.order_st_process_search.length;
    for (var i = 0; i < size; i++) {
        var id = data[i].orgId + "-" + data[i].parcelUID,
            tr = $("<tr/>");
        tr.append("<td> <div class='radio'>" +
            "<label style='font-weight: bold' id='label-'" + id + ">" +
            "<input type='radio' id='st-cadnum-search-" + id + "' name='radio-order-cadastr'>" + data[i].info +
            "</label> </div> </td>");
        // tr = $("<tr> <td style='width: 50%'> " +
        //     "<div class='radio'> <label style='font-weight: bold' id='label-'" + id + ">" +
        //     "<input type='radio' id='st-cadnum-search-" + id + "' name='radio-order-cadastr'>" + data[i].info + "</label> " +
        //     "</div> </td> </tr>");
        $("#order_st_cadnum_table").append(tr);
    }
    st_cadnum_order_result_listener();
}

function st_cadnum_order_result_listener() {
    var data = order_scope.order_st_process_search,
        size = data.length;
    for (var i = 0; i < size; i++) {
        id = data[i].orgId + "-" + data[i].parcelUID;
        document.getElementById("st-cadnum-search-" + id).addEventListener('click', setAddress, false);
    }
}


function init_unp_sx_areas() {
    $("#order-raion3").empty().removeProp("disabled").append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#order-raion3").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_name_sx_areas() {
    $("#search-order-raion5").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#search-order-raion5").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_zone_sx_areas() {
    $("#order-raion4").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#order-raion4").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_unp_sx() {
    $("#order-unp").empty().removeProp("disabled").append("<option value='-1'></option>");
    var data = order_scope.order_process_sx_list;
    var size = order_scope.order_process_sx_list.length;
    for (var i = 0; i < size; i++) {
        $("#order-unp").append("<option value='" + data[i] + "'>" + data[i] + "</option>");
    }
}

function init_name_sx() {
    $("#search-order-name_zemlepol").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_sx_list;
    var size = order_scope.order_process_sx_list.length;
    for (var i = 0; i < size; i++) {
        $("#search-order-name_zemlepol").append("<option value='" + data[i] + "'>" + data[i] + "</option>");
    }
}

function init_zone_sx() {
    $("#order-num_oc_zone").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_sx_list;
    var size = order_scope.order_process_sx_list.length;
    for (var i = 0; i < size; i++) {
        $("#order-num_oc_zone").append("<option value='" + data[i] + "'>" + data[i] + "</option>");
    }
}

function init_snp_areas() {
    $("#search-raion-snp-order").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#search-raion-snp-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_snp_ss() {
    $("#search-ss-snp-order").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#search-ss-snp-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_snp_ss_notar() {
    $("#search-raion-st-notar-order").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#search-raion-st-notar-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function init_location_areas() {
    $("#search-raion-mesto-order").empty().append("<option value='-1'></option>");
    var data = order_scope.order_process_ate_areas;
    var size = order_scope.order_process_ate_areas.length;
    for (var i = 0; i < size; i++) {
        $("#search-raion-mesto-order").append("<option value='" + data[i].id + "'>" + data[i].info + "</option>");
    }
}

function change_sx_type(type) {
    clear_sx_fields();
    $("#order-num-2-btn").prop("disabled", "disabled");
    $("#order-2-unp").prop("disabled", "disabled");
    if (type == 81) {
        show_sx_unp();
    } else if (type == 82) {
        show_sx_name();
    } else if (type == 83) {
        show_sx_zone();
    }
}

function show_sx_unp() {
    $("#order-unp-2").slideDown();
    $("#order-num-2").slideUp();
    $("#order-name-2").slideUp();
    // $("#order-unp-2").removeClass("display-none");
    // $("#order-num-2").addClass("display-none");
    // $("#order-name-2").addClass("display-none");
}

function show_sx_name() {
    $("#order-unp-2").slideUp();
    $("#order-num-2").slideUp();
    $("#order-name-2").slideDown();
    // $("#order-unp-2").addClass("display-none");
    // $("#order-num-2").addClass("display-none");
    // $("#order-name-2").removeClass("display-none");
}

function show_sx_zone() {
    $("#order-unp-2").slideUp();
    $("#order-num-2").slideDown();
    $("#order-name-2").slideUp();
    // $("#order-unp-2").addClass("display-none");
    // $("#order-num-2").removeClass("display-none");
    // $("#order-name-2").addClass("display-none");
}

function setNotar() {
    $("#cadastr-order-div").removeClass("disabled-div");
    $("#ch-order-div").removeClass("disabled-div");
    $("#addr-order-div").removeClass("disabled-div");
    $("#mestor-order-div").removeClass("disabled-div");
    $("#snp-order-div").removeClass("disabled-div");
    if (order_scope.editable_order.nkName == null) {
        $("#snp-order-div").addClass("disabled-div");
    } else {
        $("#cadastr-order-div").addClass("disabled-div");
        $("#ch-order-div").addClass("disabled-div");
        $("#addr-order-div").addClass("disabled-div");
        $("#mestor-order-div").addClass("disabled-div");
    }
}

function defaultOrder() {
    $("#cadastr-order").prop('checked', true);
    $("#st-order").prop('checked', false);
    $("#order-cadastr").removeClass("display-none");
    $("#order-st-notar").addClass("display-none");
    $("#snp-order-div").addClass("disabled-div");
}

function defaultOrderNalog() {
    $("#cadastr-order").prop('checked', false);
    $("#st-order").prop('checked', true);
    $("#order-cadastr").addClass("display-none");
    $("#order-st-notar").removeClass("display-none");
    $("#cadastr-order-div").addClass("disabled-div");
    $("#ch-order-div").addClass("disabled-div");
    $("#addr-order-div").addClass("disabled-div");
    $("#mestor-order-div").addClass("disabled-div");
}

function closeAllDialogOrder() {
    $("#search-addr-1-order").addClass("display-none");
    $("#order-cadastr").addClass("display-none");
    $("#order-st").addClass("display-none");
    $("#order-st-notar").addClass("display-none");
    $("#order-mesto").addClass("display-none");
    $("#order-sh").addClass("display-none");
    $("#order-snp").addClass("display-none");
    $("#cadastr-order-div").removeClass("disabled-div");
    $("#st-order-div").removeClass("disabled-div");
    $("#ch-order-div").removeClass("disabled-div");
    $("#addr-order-div").removeClass("disabled-div");
    $("#mestor-order-div").removeClass("disabled-div");
    $("#snp-order-div").removeClass("disabled-div");
    clearAllFieldsOrder();
}

function clearAllFieldsOrder() {
    clear_cadastr_fields();
    clear_st_fields();
    clear_sx_fields();
    clear_location_fields();
    clear_snp_fields();
    clear_st_notar_fields();
}

function clear_cadastr_fields() {
    $("#soato-order").val('');
    $("#numblock-order").val('');
    $("#numuch-order").val('');
    hide_cadnum_order_result();
}

function clear_st_fields() {
    $("#search-obl-st-order").val("-1");
    $("#search-raion-st-order").empty();
    $("#search-name-st-order").empty();
    $("#search-addr-st-order").val('');
    $("#isCadastr").prop("checked", false);
    clear_st_cadaster_fields();

}

function clear_st_cadaster_fields() {
    $("#soato-st-order").val('');
    $("#numblock-st-order").val('');
    $("#numuch-st-order").val('');
}

function clear_sx_fields() {
    $("#order-obl3").val("-1");
    $("#order-obl4").val("-1");
    $("#search-order-obl5").val("-1");
    $("#order-raion3").empty();
    $("#order-unp").empty();
    $("#order-raion4").empty();
    $("#order-num_oc_zone").empty();
    $("#search-order-raion5").empty();
    $("#search-order-name_zemlepol").empty();
    hide_sx_order_result();
}

function clear_location_fields() {
    $("#search-obl-mesto-order").val("-1");
    $("#search-raion-mesto-order").empty();
    $("#search-addr-mesto-order").val('');
}

function clear_snp_fields() {
    $("#search-addr-snp-order").val('');
    $("#search-obl-snp-order").val("-1");
    $("#search-raion-snp-order").empty();
    $("#search-ss-snp-order").empty();
    $("#search-fn-snp-order").val("-1");
}

function clear_st_notar_fields() {
    $("#search-addr-st-notar-order").val('');
    $("#search-obl-st-notar-order").val("-1");
    $("#search-raion-st-notar-order").empty();
    $("#search-fn-st-notar-order").val("-1");
}

function setAddress(e) {
    $("#search-addr-st-order").val($(this).parent().text());
    current_st_cadnum_id = $(this).prop("id");
}

function setListenerTableOrder(e) {
    if ($(this).hasClass('selected-tr')) {
        $(this).removeClass('selected-tr');
        current_cadnum_id = null;
    } else {
        $(this).siblings().removeClass('selected-tr');
        $(this).addClass('selected-tr');
        current_cadnum_id = $(this).prop('id');
    }
}

function input_limit(code, _this, length) {
    if (code != 8 && code != 9 && code != 46 && code != 37 && code != 39) {
        if (code == 16 || code == 187 || code == 107 || code == 189 || code == 109) {
            return false;
        } else if (_this.val().length == length) {
            return false;
        }
    }
}

function close_cadaster_search_result() {
    $("#order-res-search").addClass("display-none");
}

function null_order_object() {
    order_object.orderId = null;
    order_object.searchType = null;
    order_object.date = "";
    order_object.distrId = null;
    order_object.orgId = null;
    order_object.parcelId = null;
    order_object.addressId = null;
    order_object.prevAddress = "";
    order_object.coordinates = "";
    order_object.sId = null;
}

function init_st_notar_object() {
    order_object.orderId = current_order_id;
    order_object.searchType = 50;
    order_object.date = get_normal_date();
    order_object.distrId = $("#search-raion-st-notar-order").val();
}

function init_snp_object() {
    order_object.orderId = current_order_id;
    order_object.searchType = 70;
    order_object.date = get_normal_date();
    order_object.distrId = $("#search-ss-snp-order").val();
    order_object.sId = $("#search-fn-snp-order").val();
}

function init_cadastr_object(parcel_id, org_id) {
    order_object.orderId = current_order_id;
    order_object.searchType = 10;
    order_object.date = get_normal_date();
    order_object.parcelId = parcel_id;
    order_object.orgId = org_id;
}

function check_order_st_notar_address() {
    return equals($("#search-addr-st-notar-order").val(), "");
}

function check_order_st_notar_obl() {
    return equals($("#search-obl-st-notar-order").val(), "-1");
}

function check_order_st_notar_raion() {
    return $("#search-raion-st-notar-order").val() == null || equals($("#search-raion-st-notar-order").val(), "-1");
}

function check_order_st_notar_fn() {
    return equals($("#search-fn-st-notar-order").val(), "-1");
}

function check_order_snp_address() {
    return equals($("#search-addr-snp-order").val(), "");
}

function check_order_snp_obl() {
    return equals($("#search-obl-snp-order").val(), "-1");
}

function check_order_snp_raion() {
    return $("#search-raion-snp-order").val() == null || equals($("#search-raion-snp-order").val(), "-1");
}

function check_order_snp_ss() {
    return $("#search-ss-snp-order").val() == null || equals($("#search-ss-snp-order").val(), "-1");
}

function check_order_snp_fn() {
    return equals($("#search-fn-snp-order").val(), "-1");
}

function load_order_objects_table() {
    $("#order-object-table").empty();
    var data = order_scope.order_objects;
    if (data[0].objectId != 0) {
        for (var i = 0; i < data.length; i++) {
            var galochka = '<span class="glyphicon glyphicon-saved" style="color: #4cae4c; font-size: 180%"></span>';
            $("#save-all").removeClass('disabled-div');
            if (data[i].isDefine == 0) {
                galochka = '';
                if (!$("#save-all").hasClass("disabled-div")) {
                    $("#save-all").addClass("disabled-div");
                }
            }
            var tr = $('<tr class="cursor-select" id="order_object_id_' + data[i].objectId + '"/>');
            tr.append("<td style='text-align: center;' id='" + "or-" + data[i].objectId + "'><span class='glyphicon glyphicon-trash size-span-lg'></span></td>");
            tr.append("<td>" + check_info(data[i].info, data[i].cadNum) + "</td>");
            tr.append("<td>" + data[i].date + "</td>");
            tr.append("<td>" + check_null_number(data[i].square) + "</td>");
            tr.append("<td>" + check_null_string(data[i].purpose) + "</td>");
            tr.append("<td>" + data[i].docName + "</td>");
            tr.append("<td style='text-align: center'>" + galochka + "</td>");
            $("#order-object-table").append(tr);
        }
        set_order_object_listeners();
    } else if (!$("#save-all").hasClass("disabled-div")) {
        $("#save-all").addClass("disabled-div");
    }
}

function load_order_objects_table_edit() {
    $("#order-object-table").empty();
    var data = order_scope.order_objects;
    if (data[0].objectId != 0) {
        for (var i = 0; i < data.length; i++) {
            var galochka = '<span class="glyphicon glyphicon-saved" style="color: #4cae4c; font-size: 180%"></span>'
            if (data[i].isDefine == 0) {
                galochka = '';
                if (!$("#save-all").hasClass("disabled-div")) {
                    $("#save-all").addClass("disabled-div");
                }
            }
            var tr = $('<tr class="cursor-select" id="order_object_id_' + data[i].objectId + '"/>');
            tr.append("<td style='text-align: center;' id='" + "or-" + data[i].objectId + "'><span class='glyphicon glyphicon-trash size-span-lg'></span></td>");
            tr.append("<td>" + check_info(data[i].info, data[i].cadNum) + "</td>");
            tr.append("<td>" + data[i].date + "</td>");
            tr.append("<td>" + check_null_number(data[i].square) + "</td>");
            tr.append("<td>" + check_null_string(data[i].purpose) + "</td>");
            tr.append("<td>" + data[i].docName + "</td>");
            tr.append("<td style='text-align: center'>" + galochka + "</td>");
            $("#order-object-table").append(tr);
        }
        set_order_object_listeners_edit();
    } else if (!$("#save-all").hasClass("disabled-div")) {
        $("#save-all").addClass("disabled-div");
    }
}

function check_info(info, cad_num) {
    if (cad_num == null) {
        return info;
    } else {
        return info + ", " + cad_num;
    }
}

function set_order_object_listeners() {
    var data = order_scope.order_objects;
    for (var i = 0; i < data.length; i++) {
        var id = "order_object_id_" + data[i].objectId,
            id_td = "or-" + data[i].objectId;
        document.getElementById(id).addEventListener('click', click_order_object, false);
        document.getElementById(id_td).addEventListener('click', click_order_object_trach, false);
    }
}

function set_order_object_listeners_edit() {
    var data = order_scope.order_objects;
    for (var i = 0; i < data.length; i++) {
        var id = "order_object_id_" + data[i].objectId,
            id_td = "or-" + data[i].objectId;
        document.getElementById(id).addEventListener('click', click_order_object_edit, false);
    }
}

function click_order_object(e) {
    if (!clickTrash) {
        var id = ($(this).attr("id")).split('_')[3];
        if ($(this).hasClass('selected-tr')) {
            $(this).removeClass('selected-tr');
            if (!$("#change-location").hasClass('disabled-div')) {
                $("#change-location").addClass('disabled-div');
            }
            if (!$("#reset-location").hasClass('disabled-div')) {
                $("#reset-location").addClass('disabled-div');
            }
            if (!$("#create-doc").hasClass('disabled-div')) {
                $("#create-doc").addClass('disabled-div');
            }
            current_object_id = null;
            current_object = null;
        } else {
            $(this).siblings().removeClass('selected-tr');
            $(this).addClass('selected-tr');
            current_object_id = parseInt(id);
            current_object = order_scope.order_objects[$(this).index()];
            if (order_scope.order_objects[$(this).index()].isDefine == 1) {
                if (!$("#change-location").hasClass('disabled-div')) {
                    $("#change-location").addClass('disabled-div')
                }
                if (!$("#save_order").hasClass('disabled-div')) {
                    $("#save_order").addClass('disabled-div')
                }
                $("#create-doc").removeClass('disabled-div');
                $("#reset-location").removeClass('disabled-div');
            } else {
                $("#change-location").removeClass('disabled-div');
                $("#save_order").removeClass('disabled-div');
            }
        }
        search_order_obj_for_map(current_object);
        check_object_id();
    } else {
        clickTrash = false;
    }
}

function click_order_object_edit() {
    var id = ($(this).attr("id")).split('_')[3];
    if ($(this).hasClass('selected-tr')) {
        $(this).removeClass('selected-tr');
        if (!$("#create-doc").hasClass('disabled-div')) {
            $("#create-doc").addClass('disabled-div');
        }
        current_object_id = null;
        current_object = null;
    } else {
        $(this).siblings().removeClass('selected-tr');
        $(this).addClass('selected-tr');
        $("#create-doc").removeClass("disabled-div");
        current_object_id = parseInt(id);
        current_object = order_scope.order_objects[$(this).index()];
    }
    search_order_obj_for_map(current_object);
    check_object_id();
}


function click_order_object_trach(e) {
    clickTrash = true;
    var id = $(this).attr("id").split('-')[1];
    current_object_id = id;
    httpServices.delete_order(id, false);
    $("#order_object_id_" + id).empty();
}


function check_object_id() {
    $("#order-zone-table").empty();
    if (current_object_id != null) {
        load_object_zones_table();
    }
}

function load_object_zones_table() {
    httpServices.get_objects_zones(current_object_id);
    var data = order_scope.object_zones;
    set_zones_in_table(data);
}

function set_zones_in_table(data) {
    if (data != null) {
        $("#order-zone-table").empty();
        var flag = false;
        for (var i = 0; i < data.length; i++) {
            if (data[i].costDate != null && data[i].costDate != undefined && data[i].costDate != '') {
                flag = true;
            }
            if(data[i].zoneNumber == null){
                data[i].zoneNumber = '';
            }
            var tr = $('<tr class="cursor-select/">');
            tr.append("<td id='func_code_" + data[i].funcCode + "'>" + data[i].funcName + "</td>");
            tr.append("<td>" + check_null_string(data[i].costDate) + "</td>");
            tr.append("<td>" + check_null_selectZone(data[i].zoneNumber, data[i].zones, data[i].funcCode) + "</td>");
            tr.append("<td>" + check_null_string(data[i].zoneName) + "</td>");
            tr.append("<td>" + check_null_number(data[i].costUSD) + "</td>");
            tr.append("<td>" + check_null_number(data[i].costBYN) + "</td>");
            tr.append("<td>" + check_null_number(data[i].costNB) + "</td>");
            $("#order-zone-table").append(tr);
            set_listener_for_zones(order_scope.object_zones[i].zones, "zones_fun_" + order_scope.object_zones[i].funcCode, "zone_" + order_scope.object_zones[i].zoneNumber + "_" + order_scope.object_zones[i].funcCode)
        }
        if (!flag) {
            if (!$("#save_order").hasClass('disabled-div')) {
                $("#save_order").addClass('disabled-div')
            }
        } else {
            $("#save_order").removeClass('disabled-div')
        }
    }
}

function check_null_string(string) {
    if (string == null) {
        return "";
    } else {
        return string;
    }
}

function check_null_selectZone(zoneNumber, zones, fun_code) {
        if(zones != null) {
            var zonessplit = zonesarr.split(';'),
                select = '<select class="A" id="zones_fun_' + code + '"><option id="ZONE-' + fun_code + '">' + zoneNumber + '</option>';
            for (var i = 0; i < zonessplit.length; i++) {
                var id = 'sel-' + zonessplit[i].split(',')[0] + '-' + fun_code;
                select += '<option id="' + id + '">' + zonessplit[i].split(',')[1] + '</option>';
            }
            select += '</select>';
            return select;
        } else {
            var input = '<input id="zone_' + zoneNumber + '_' + fun_code + '" class="A" value="' + zoneNumber + '"/>'
            return input;
        }
}


function set_listener_for_zones(zonesarr, code, code2) {
    if (zonesarr != null) {
        document.getElementById(code).addEventListener('change', click_order_change_zone, false);
    } else {
        document.getElementById(code2).addEventListener('change', click_order_change_zone_input, false);
    }
}

function check_null_number(number) {
    if (number == 0) {
        return "";
    } else {
        return number;
    }
}

function check_st_search_type() {
    return ($("#isCadastr").is(":checked"));
}

function check_order_st_obl() {
    return equals($("#search-obl-st-order").val(), "-1");
}

function check_order_st_raion() {
    return $("#search-raion-st-order").val() == null || equals($("#search-raion-st-order").val(), "-1");
}

function check_order_st_name() {
    return $("#search-name-st-order").val() == null || equals($("#search-name-st-order").val(), "-1");
}

function check_order_st_address() {
    return equals($("#search-addr-st-order").val(), "");
}

function check_order_st_cadaster() {
    return current_st_cadnum_id == null;
}

function init_st_address_object() {
    order_object.orderId = current_order_id;
    order_object.searchType = 40;
    order_object.date = get_normal_date();
    order_object.prevAddress = $("#search-addr-st-order").val();
    order_object.sId = parseInt($("#search-name-st-order").val());
}

function init_st_cadaster_object() {
    var value = current_st_cadnum_id.split("-");
    init_st_address_object();
    order_object.prevAddress = "";
    order_object.orgId = parseInt(value[3]);
    order_object.parcelId = parseInt(value[4]);
}

function check_order_mesto_obl() {
    return equals($("#search-obl-mesto-order").val(), "-1");
}

function check_order_mesto_raion() {
    return $("#search-raion-mesto-order").val() == null || equals($("#search-raion-mesto-order").val(), "-1");
}

function check_order_mesto_mesto() {
    return equals($("#search-addr-mesto-order").val(), "");
}

function init_mesto_object() {
    order_object.orderId = current_order_id;
    order_object.searchType = 30;
    order_object.date = get_normal_date();
    order_object.distrId = $("#search-raion-mesto-order").val();
    order_object.prevAddress = $("#search-addr-mesto-order").val();
}

function init_sx_object(zoneId) {
    order_object.orderId = current_order_id;
    order_object.searchType = get_sx_type();
    order_object.date = get_normal_date();
    order_object.sId = zoneId;
}

function get_sx_type() {
    if ($("#sh-order-unp").prop("checked")) {
        return 81;
    } else if ($("#sh-order-name").prop("checked")) {
        return 82;
    } else if ($("#sh-order-zone").prop("checked")) {
        return 83;
    }
}

function click_order_change_zone(e) {
    var funCode = $(this).children(":selected").prop("id").split("-")[2],
        zoneId = $(this).children(":selected").prop("id").split("-")[1];

    httpServices.update_zones(current_object_id, funCode, zoneId);

    set_zones_in_table(order_scope.object_zones);
}

function click_order_change_zone_input(e){
    var funCode = $(this).prop("id").split("_")[2],
        zoneNumLast = $(this).prop("id").split("_")[1],
        zoneNum = $(this).val();
    httpServices.update_zones(current_object_id, funCode, zoneNum);

}

function reset_order_edit_process_buttons() {
    $("#order_edit_order").prop('disabled', 'disabled');
    $("#order_process_order").prop('disabled', 'disabled');
}