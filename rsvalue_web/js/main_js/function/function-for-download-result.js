/**
 * Created by Shcherbina on 13.06.2016.
 */
var COUNT_FILE = 0;
var ID_FILE_3 = 0;
var FILE_NAME = "";

var ID_DEL, ID_DEL_ZIP;

var IS_DELETE = false;
var IS_ADD = false;
var forSENDJSON;
var docID=[], zipID=[], LAYERS=[];

var Zone_map = false, Bound_map = false, Layer_map = false, next_load = false;

var arr = [];

function isNext_2 (json_send) {
    var arr_imp = $(".important");
    var FILE = true;

    //ОКАММЕНТИРОВАТЬ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //ОБЯЗАТЕЛЬНЫЕ ДОКУМЕНТЫ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    for(var i=0; i<arr_imp.length && FILE; i++){
        var text_lenth = (arr_imp[i].textContent).length;
        if(text_lenth == 0){
            FILE = false;
        }
    }

    if(json_send.valOrgId != -1 && json_send.decOrgId != -1 && json_send.decDate != '' && json_send.decNum != undefined && json_send.decNum != '' && FILE) {
        return true;
    }
    else {
        return false;
    }
}

function forFunType (arr_id, download_scope, val) {
    town_change();
    arr_id.town = val;
    httpServices.getFun(download_scope, arr_id.type_val, arr_id.meth_val, arr_id.town);
    $("#func-value").removeAttr("disabled");
    $("#func-value").append(function () {
        var option_func = option(-1, -1, "");
        $.each(download_scope.func_val, function (i, obj) {
            option_func += option(i, obj.analyticCode, obj.codeName);
        })
        return option_func;
    });
}

function createTableDialog_2(download_scope, arr_id) {
    console.log("createTableDialog_2(download_scope, arr_id)")
    httpServices.getDocImportant(download_scope, arr_id.type_val, arr_id.meth_val, arr_id.polyType);
    httpServices.getDoc(download_scope, arr_id.type_val, arr_id.meth_val, arr_id.polyType);

    $("#dialog-table-1").append(function(){
        var tbody_content = "";
        $.each(download_scope.doc_impot, function(i, obj) {
            var id=obj.analyticCode+"-0";
            tbody_content += openTR() +
                td_win_2_star(id, obj.codeName) +
                td_win_2_imp(id+"-name", "") +
                td_win_2(id+"-size", "") +
                openTDaction() +
                remove(id+"-d") +
                plusFile(id+"-a") +
                closeTD() +
                closeTR();
        });
        return tbody_content;
    })

    $("#dialog-after-table").append(function(){
        return plus("add-after-table");
    });

    $("#doc-div").append(function(){
        var docs = "";
        $.each(download_scope.doc, function(i, obj) {
            docs += divOpen_12() + addButtonFile(obj.analyticCode + "-0-doc", obj.codeName) + divClose();
        });
        return docs;
    });

    //Loading documents

}

function createListenerDialog_2(download_scope, json_send){
    console.log("createListenerDialog_2(download_scope, json_send)")
    json_send.docNum = "";
    setJSON(json_send)
    $.each(download_scope.doc_impot, function(i, obj) {
        var id_add=obj.analyticCode + "-0-a";
        var id_del=obj.analyticCode + "-0-d";
        document.getElementById(id_add).addEventListener('change', onFilesSelect, false);
        document.getElementById(id_del).addEventListener('click', onFilesDeleteEvent, false);

        document.getElementById(id_add).addEventListener('click', clickDoc, false);
    });

    $.each(download_scope.doc, function(i, obj) {
        var id=obj.analyticCode+"-0-doc";
        document.getElementById(id).addEventListener('change', onFilesSelectDoc, false);

        document.getElementById(id).addEventListener('click', clickDoc, false);
    });
}

function clickDoc(e) {
    this.value = null;
}

function onFilesSelect(e) {
    console.log("onFilesSelect(e)")

    var arr_id = ($(this).attr("id")).split('-');
    var id = arr_id[0] + "-" + arr_id[1];
    var files = e.target.files, file;
    COUNT_FILE = files.length;
    setDocNum(arr_id[0])
    for (var i = 0; i < COUNT_FILE; i++) {
        var id_add = arr_id[0] + "-" + i + "-a";
        var id_del = "#" + arr_id[0] + "-" + i + "-d";
        file = files[i];
            var reader = new FileReader();
            /*reader.onload = function(readerEvt) {
                /!*var binaryString = readerEvt.target.result;
                console.log(btoa(binaryString));*!/

                console.log(window.btoa(unescape(encodeURIComponent(event.target.result))));
            };*/

        httpServices.sendDocFile(file, false);
        if(IS_ADD) {
            $(id_del).addClass(ID_DEL + "");
            docID.push(ID_DEL);
            console.log(docID)
            var new_tr = true;
            if (i == 0) {
                new_tr = false;
            }
            addtoTableDialog_2(id, file.name, parseFloat((file.size / 1000).toFixed(1)), new_tr, i);
            IS_ADD = false;

            if(isNext_2(json_send)) {
                $("#next-load-2").removeAttr("disabled");
            }else {
                $("#next-load-2").attr("disabled", "disabled");
            }
        }
    }

  /*  setListenersAddingFiles(arr_id[0]);*/
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ИСПРАВИТЬ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function onFilesSelectDoc(e) {

    console.log("onFilesSelectDoc(e)")
    var title = $(this).prev().text();
    var id_arr = $(this).attr("id").split('-');

    json_send.docNum = id_arr[0];

    var files = e.target.files,
        row, fr, file, data; // строка с информацией о файле (Перезаписывается каждый шаг цикла), FileReader (Создаётся для каждого файла), объект file из FileList'a,  массив с информацией о файле

    setDocNum(id_arr[0])

    for (var i = 0; i < files.length; i++) {

        file = files[i];

        var id_add = id_arr[0] + "-" + NUM_DOC + "-a";
        var id_del = id_arr[0] + "-" + NUM_DOC + "-d";

        httpServices.sendDocFile(file, false);

        if (IS_ADD) {
            $("#dialog-table-1").append(function () {
                var tbody_content = openTR() +
                    td_win_2(id_arr[0] + "-" + NUM_DOC, title) +
                    td_win_2(id_arr[0] + "-" + NUM_DOC + "-name", file.name) +
                    td_win_2(id_arr[0] + "-" + NUM_DOC + "-size", parseFloat((file.size / 1000).toFixed(1))) +
                    openTDaction() +
                    plusFile_non_imp(id_add) +
                    remove_cl(id_del, ID_DEL) +
                    closeTD() +
                    closeTR();
                return tbody_content;
            })

            document.getElementById(id_add).addEventListener('change', onFilesSelect, false);
            document.getElementById(id_del).addEventListener('click', onFilesDeleteEvent, false);

            plus_del(id_arr[0] + "-" + NUM_DOC, true)

            docID.push(ID_DEL)
            //alert(1)
            console.log(docID)
            IS_ADD = false;
            NUM_DOC += 1;
        }
    }
}

function addtoTableDialog_2(id, name, size, new_tr, i){
    var id_first = "#" + id;
    var id_el = id.split('-')[0];
    if(new_tr){
        var title =  $(id_first).text().replace('*', '');
       var tr =  $(id_first).parent().parent();
        tr.append(function () {
            var tr_add = openTR() +
            td_win_2(id_el + "-" + i, title) +
            td_win_2(id_el + "-" + i +"-name", name) +
            td_win_2(id_el + "-" + i + "-size", size) +
            openTDaction() +
            plusFile_new(id_el + "-" + i + "-a") +
            remove_new(id_el + "-" + i+"-d") +
            closeTD() +
            closeTR();
            return tr_add;
        })

    } else {
        var td_id_name =id_first + "-name";
        var td_id_size = id_first + "-size";
        $(td_id_name).append(name);
        $(td_id_size).append(size);
        plus_del(id, true)
    }
}

function onFilesDeleteEvent(e) {
    console.log("onFilesDeleteEvent(e)")
    var id_arr = ($(this).attr("id")).split('-');
    var id = id_arr[0] + "-" + id_arr[1];
    var row = true;
    var text = $("#"+id).text();
    if(((text.lastIndexOf('*')+1) == text.length) && text.lastIndexOf('*') != -1){
        row=false;
    }
    onFilesDelete(id, row)
    plus_del(id, false);

    if(isNext_2(json_send)) {
        $("#next-load-2").removeAttr("disabled");
    }else {
        $("#next-load-2").attr("disabled", "disabled");
    }
}

function onFilesDelete(id, row) {
    console.log("onFilesDelete(id, row)")

    var td_id_name = "#"+id+"-name";
    var td_id_size = "#"+id+"-size";
    var td_id_del = "#"+id+"-d";
    var id_td = "#" + id;
    $(td_id_name).empty();
    $(td_id_size).empty();
    var id_doc = parseInt($(td_id_del).attr( 'class' ));

    httpServices.deleteDoc(id_doc, td_id_del);
    if(IS_DELETE) {
        if (row) {
            ($(id_td).parent()).remove();
        }
        docID.splice(docID.indexOf(id_doc), 1);
        IS_DELETE = false;
    }
    console.log(docID)
}

function removeTab() {
    $("#doc-div").empty();
    $("#dialog-table-1").empty();
    $("#add-after-table").remove()
}

function funfun(){
    $("#add-after-table").addClass("click-event")
}

function funfun_edit(){
    $("#add-after-table-edit").addClass("click-event")
}

function createTableBookLoad(download_scope, load_param) {

    download_scope.bookLoad=null;
    httpServices.getBookLoad_2(download_scope, load_param);
    create_table(download_scope, [1, 2, 3, 4, 5, 6, 12])
}

function create_table(download_scope, columnNums){
    appendHeadersZurnal(download_scope, columnNums);
    appendBodyZurnal(download_scope, columnNums)
}

function getPagination(download_scope){
    $("#pagination_table").empty();
    $("#pagination_table").append(function(){
        return getLi(download_scope.colPage);
    })
}

function displayNone(id, plus){
    for(var i=0; i<COUNT_FILE; i++){
        var id_el = id + "-" + i;
        plus_del(id_el, plus);

    }
}



function plus_del(id, plus) {
    var id_add = "#" + id + "-a";
    var id_del = "#" + id + "-d";
    console.log(id_add)
    if(plus) {
        $(id_add).parent().css("display", "none");
        $(id_del).parent().css("display", "");
    }
    else {
        $(id_add).parent().css("display", "");
        $(id_del).parent().css("display", "none");
    }
}

function _plus_del (plus, type) {
    var id_add = "#add-" + type;
    var id_del = "#del-" + type;
    if(plus) {
        $(id_add).parent().css("display", "none");
        $(id_del).parent().css("display", "");

        $(id_del).addClass(ID_DEL_ZIP+"");
        zipID.push(ID_DEL_ZIP)
    }
    else {
        $(id_add).parent().css("display", "");
        $(id_del).parent().css("display", "none");
    }
}

function onFilesDialog_3(e) {
    //alert("onFilesDialog_3(e)")
    openModal();
    var id = ($(this).attr("id")).split('-')[1];
    var name_arr=[], file_name_thems= new Array();
    var files = e.target.files,
        row, fr, file, data; // строка с информацией о файле (Перезаписывается каждый шаг цикла), FileReader (Создаётся для каждого файла), объект file из FileList'a,  массив с информацией о файле
    arr = [];

    for (var i = 0; i < files.length; i++) {
        file = files[i];

        var num = 3;
        if (id == "zone") {
            num = 1;
        } else if (id == "bound") {
            num = 2;
        }

        httpServices.sendZoneBoundLayer_ZIP(file, id)

        if (IS_ADD) {
            if (id != "thems") {
                //alert("entries")
                model.getEntries(file, function (entries) {
                    var totalFiles = entries.length;
                    entries.forEach(function (entry) {
                        name_arr = (entry.filename).split('.');
                        if (name_arr[name_arr.length - 1] == "dbf") {
                            FILE_NAME = entry.filename;
                        }
                        download(entry, totalFiles, num);
                    });

                });
            } else {
                model.getEntries(file, function (entries) {
                    var totalFiles = entries.length;
                    var arrayEntries = [];
                    var local_name = "";
                    entries.sort().forEach(function (entry) {
                        name_arr = (entry.filename).split('.');

                        if (name_arr[name_arr.length - 1] == "dbf" || name_arr[name_arr.length - 1] == "shp") {

                            var arr_fn = (entry.filename).split('/');
                            if (name_arr[name_arr.length - 1] == "shp") {
                                file_name_thems.push(arr_fn[arr_fn.length - 1])
                            }
                            arrayEntries.push(entry)
                        }
                    });
                    arrayEntries = arrayEntries.sort(function (a, b) {
                        var nameA = a.filename.toLowerCase(), nameB = b.filename.toLowerCase()
                        if (nameA < nameB) //sort string ascending
                            return -1
                        if (nameA > nameB)
                            return 1
                        return 0
                    });
                    httpServices.validateLayersNames(file_name_thems);
                    if (arr.length != 0) {
                        next_load = true;
                        var grunt, dostransport;
                        LAYERS = arr;
                        layerJSONCODE = [];
                        for (var i = 0; i < arr.length; i++) {
                            if (((arr[i].layerName).toLowerCase()).indexOf('grunt') != -1) {
                                grunt = arr[i].analyticCode;
                            }
                            if (((arr[i].layerName).toLowerCase()).indexOf('dosttransp') != -1) {
                                dostransport = arr[i].analyticCode;
                            }
                        }

                        forSENDJSON = {
                            "grunt": grunt,
                            "dosttransport": dostransport
                        }

                        var n = true;

                        for (var i = 0; i < (arrayEntries.length); /*i++ ОШИБКА!!! */) {
                            for (var j = 0; j < 2; j++, i++) {
                                if (i == arrayEntries.length - 1) {
                                    n = false;
                                }
                                download2(arrayEntries[i], j, n);
                            }
                        }

                        inx = 0;

                        addtoTableThemsDialog_3(arr, id);
                    }
                });
            }
            ID_FILE_3 = id;
            _plus_del(true, id)
        }
    }
    closeModal();
}

function addtoTableDialog_3( count, id){
    console.log("addtoTableDialog_3( count, id)")
    var id_el = "#" + ID_FILE_3;
    $(id_el).append(function(){
        var tab = openTR() +
            td_win_3(FILE_NAME)+
            td_win_3(count)+
            td_win_3("<span class=\"glyphicon glyphicon-paperclip size-span-lg\" ></span>")+
            td_win_3("<div id='dialog-" + id + "'><a href=\"#load-modal-5\" id=\""+id+"\" data-dismiss=\"modal\">"+
                "<label class=\"btn btn-file btn-link\"><span class=\"glyphicon glyphicon-eye-open size-span-lg\"  onclick='eye_table_"+id.split('-')[0]+"()'></span>" +
                "<input type=\"button\" style=\"display: none;\"></label></a></div>")+
                closeTR;
        return tab;
    })
}

function addtoTableThemsDialog_3(arr, id){
    var layers = arr;
    var id_el = "#" + id;
    $(id_el).append(function(){
        var tab="";
        for(var i=0; i<layers.length; i++) {
            tab+=openTR() +
                td_win_3(layers[i].layerTypeName) +
                td_win_3((layers[i].layerName)) +
                td_win_3("<span class=\"glyphicon glyphicon-paperclip size-span-lg\" ></span>") +
                closeTR;
        }
        return tab;
    })
}

function removeTabStr(id) {
    var id_el = "#" + id;
    $(id_el).empty();
}

function delFilesDialog_3(e){
    console.log("delFilesDialog_3(e)")
    var cl = $(this).attr('class')
    var type = (($(this).attr('id')).split('-'))[1];
    var id = "#" + $(this).attr('id');

    if(type == "zone"){
        Zone_map = false;
        json_send.zoneList = null;
    }else if(type == "bound"){
        Bound_map = false;
        json_send.boundList = null;
    }else if(type == "thems"){
        Layer_map = false;
        layerJSONCODE =null;
    }

    httpServices.deleteDoc(parseInt(cl), id)
    _plus_del(false, type)
    removeTabStr(type);
    if(IS_DELETE) {
        zipID.splice(zipID.indexOf(parseInt(cl)), 1)
        IS_DELETE = false;
        if((Zone_map && Bound_map && isBounds()) || (Zone_map && !isBounds())){
            $("#ok-3").removeAttr("disabled");
            $("#preview-btn").removeAttr("disabled");
        }else {
            $("#ok-3").attr("disabled", "disabled");
            $("#preview-btn").attr("disabled", "disabled");
        }
    }
    console.log(zipID)
}

/*
function setListenersAddingFiles(id){
    alert(22222222222)
    for(var i = 1; i<COUNT_FILE; i++){
        var id_add = id + "-" + i + "-a";
        var id_del = id + "-" + i + "-d";
        document.getElementById(id_del).addEventListener('click', onFilesDeleteEvent, false);
        document.getElementById(id_add).addEventListener('click', clickDoc, false);
    }
    displayNone(id, true)
}*/

function addToTableDocInfoHead(data, input) {

    var id_head = "#read-doc-info";
    var id = "#head-table-doc-info";
    if(input==1){
        id_head += "-zone"
        id+="-zone";
        $(id_head).text("Просмотр таблицы Zones")
    } else if(input==2){
        id_head += "-bound"
        id+="-bound";
        $(id_head).text("Просмотр таблицы Bounds")
    }

    $(id).empty();

    /*for(var i in data){
        var key = i;
        var val = data[i];
        for(var j in val){
            var sub_key = j;
            var sub_val = val[j];
            console.log(sub_key);
        }
    }*/

    var headers = [];

        var val = data[0];
        for(var j in val){
            headers.push(j);
        }

    $(id).append(function(){
        var tab = openTR(), head_text="";
        for (var i = 0; i < headers.length; i++) {
            head_text += openTH()+headers[i]+ closeTH();
        }
           tab += head_text + closeTR();
        return tab;
    })

    return headers;
}

function addToTableDocInfoBody(data, headers, input) {
    var id = "#body-table-doc-info";
    if(input==1){
        id+="-zone";
    } else if(input==2){
        id+="-bound";
    }
    $(id).empty();
    $(id).append(function(){
        var body_text="";
        for(var i=0; i<data.length; i++) {
            body_text += openTR();
            for (var k = 0; k < headers.length; k++) {
                body_text += openTD()+(data[i])[headers[k]] + closeTD();
            }
            body_text+=closeTR();
        }
        return body_text;
    })
}

function eye_table_zone(){
    $("#zone-dialog").addClass("click-event")
}


function eye_table_bound(){
    $("#bound-dialog").addClass("click-event")
}

function getDwd(){
    get_json().dwd = httpServices.getDwd();
}

function getDwdByJSON(){
    return get_json().dwd;
}

function appendHeadersZurnal(download_scope, columnNums){
    $("#book-load-headers").empty();
    $("#book-load-headers").append(function(){
        if(download_scope.bookLoad!=null) {
            var theader_content = "<tr>", thead_sort="<tr>";
            $.each(columnNums, function (i, obj) {
                theader_content += "<th>" + load_zurnal_enam_header[obj] +"&nbsp";
                if(obj == -1) {
                    theader_content += "<span id=" + obj + "-sort" + " class='glyphicon glyphicon-sort'></span>";
                }
                theader_content += "</th>";
                thead_sort += "<th id='"+obj + "-filt"+"'>";
                switch (obj){
                    case 3 : thead_sort += "<input type='date' id='date_cost_table' class='tab_input_width'>"; break;
                    case 4 : thead_sort += "<input id='town_table' class='tab_input_width' list='town_table_list' autocomplete='on'><datalist id='town_table_list'></datalist>"; break;
                    case 5 : thead_sort += "<select class='tab_input_width' id='type_cost_table'></select>"; break;
                    case 6 : thead_sort += "<select class='tab_input_width' id='fun_val_table'></select>"; break;
                    case 9 : thead_sort += "<select class='tab_input_width' id='name_1_table'></select>"; break;
                    case 10: thead_sort += "<select class='tab_input_width' id='org_table'> </select>"; break;
                    case 12: thead_sort += "<select class='tab_input_width' id='actual_table'><option></option><option value='true'>Да</option><option value='false'>Нет</option></select>"; break;
                }
                thead_sort += "</th>";
            });
            theader_content += "</tr>";
            thead_sort += "</tr>";
        }
        return theader_content + thead_sort;
    });
}

function appendBodyZurnal(download_scope, columns){
    $("#book-load").empty();
    $("#book-load").append(function(){
        var tbody_content = "";
        var actual = "Нет";
        if(download_scope.bookLoad!=null) {
            $.each(download_scope.bookLoad, function (i, obj) {
                tbody_content += openTRID(obj.regNum+"_"+obj.funcCodeId);
                $.each(columns, function (i, col_n) {
                    if(col_n == 12){
                        if(obj[load_zurnal_obj_header[col_n]]) {
                            tbody_content += tdText("Да")
                        } else {
                            tbody_content += tdText("Нет")
                        }
                    }else {
                        tbody_content += tdText(obj[load_zurnal_obj_header[col_n]])
                    }
                });
                tbody_content += closeTR();
            });
        }
        return tbody_content;
    })
}