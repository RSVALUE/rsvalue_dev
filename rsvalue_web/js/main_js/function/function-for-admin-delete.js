/**
 * Created by tihonovichp on 21.11.2016.
 */

var delete_admin_scope = [];
var delete_record_count = 5;
var delete_row_count;
var delete_filter = [];

function create_delete_table(data) {
    $('#delete_tbody').empty();
    for (var i = 0; i < data.length; i++) {
        tr = $('<tr/>');
        tr.append("<td colspan='3'>" + data[i][3] + " " + data[i][4] + " " + data[i][5] + " " + "</td>");
        tr.append("<td>" + data[i][6] + "</td>");
        tr.append("<td>" + data[i][2] + "</td>");
        tr.append("<td>" + data[i][1] + "</td>");
        tr.append("<td>" + data[i][7] + "</td>");
        tr.append("<td align='center'> <button type='button' class='glyphicon glyphicon-remove btn btn-default' id=delete_" + data[i][0] + "></button> </td>");
        $('#delete_tbody').append(tr);
    }
    create_delete_pagination();
    delete_click();
}

function create_delete_pagination() {
    var page_count = Math.ceil(delete_row_count / delete_record_count);
    var $delete_page = $('#delete_pagination');
    $delete_page.empty();
    if (page_count != 1) {
        for (var i = 0; i < page_count; i++) {
            $delete_page.append("<li class='class'><a href='#' class='page-link'>" + (i + 1) + "</a></li>");
        }
        delete_page_click();
    }
}

function delete_page_click() {
    $('.delete-pagination li a').click(function (e) {
        var page_number = $(e.target).text();
        delete_table_change(page_number);
    });
}

function delete_click() {
    $(":button.glyphicon-remove").click(function () {
        var result = confirm("Вы точно хотите удалить пользователя?");
        if (result) {
            var id = $(this).prop("id").substr(7);
            httpServices.delete_user(id);
        }
    });
}

function delete_table_change(page) {
    if (delete_filter_check()) {
        httpServices.get_row_count();
        delete_row_count = row_count;
        httpServices.get_accounts_records(delete_admin_scope, page, delete_record_count);
    } else {
        httpServices.set_delete_filter(delete_filter, page, delete_record_count);
    }
    create_delete_table(delete_admin_scope.accounts);
}

function set_delete_filter_fields() {
    delete_filter.surname = $("#delete_surname_filter").val().toLowerCase();
    delete_filter.login = $("#delete_login_filter").val().toLowerCase();
    delete_filter.org = $("#delete_org_filter").val().toLowerCase();
    delete_filter.position = $("#delete_position_filter").val().toLowerCase();
    delete_filter.group = $("#delete_group_filter").val().toLowerCase();
}

function clear_delete_filter_field() {
    $("#delete_surname_filter").val('');
    $("#delete_first_name_filter").val('');
    $("#delete_father_name_filter").val('');
    $("#delete_login_filter").val('');
    $("#delete_org_filter").val('');
    $("#delete_position_filter").val('');
    $("#delete_group_filter").val('');
}

function delete_filter_check() {
    if ($("#delete_surname_filter").val() == "" && $("#delete_login_filter").val() == "" && $("#delete_org_filter").val() == "" && $("#delete_position_filter").val() == "" && $("#delete_group_filter").val() == "") {
        return true;
    }
}