/**
 * Created by Shcherbina on 08.06.2016.
 */

var httpServices = {};
var DOMAIN = window.location.protocol + '//'+ window.location.hostname+":8080";
var app = "/rsValueDev";
var json_send = [];
var doc_info = [];
var now = (new Date().toLocaleDateString()).split(".");
var now_date = now[2] + "-"+ now[1] + "-"+ now[0];

var XMLHttpFactories = [
    function () {
        return new XMLHttpRequest()
    },
    function () {
        return new ActiveXObject("Msxml2.XMLHTTP")
    },
    function () {
        return new ActiveXObject("Msxml3.XMLHTTP")
    },
    function () {
        return new ActiveXObject("Microsoft.XMLHTTP")
    }
];

var createXMLHTTPObject = function () {
    var xmlhttp = false;
    for (var i = 0; i < XMLHttpFactories.length; i++) {
        try {
            xmlhttp = XMLHttpFactories[i]();
        }
        catch (e) {
            continue;
        }
        break
    }
    return xmlhttp;
};

httpServices.getTypeValue = function (download_scope) {
    var url = DOMAIN+app +'/paramLoad/getCostTypes';
    var method = "POST";

    /*var xhrForm = new XMLHttpRequest();
     xhrForm.open(method, url);
     //   xhrForm.send(AUTH_FORMDATA);
     xhrForm.setRequestHeader("username", AUTH_FORMDATA.get('username'));
     xhrForm.setRequestHeader("token", AUTH_FORMDATA.get('token'));
     xhrForm.send();
     if (xhrForm.readyState == 4) {
     /!*scope.var.loading = false;*!/
     if (xhrForm.status == 200) {
     download_scope.type_value = JSON.parse(xhrForm.responseText);
     /!*console.log(http.responseText)*!/
     }
     }*/

    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        //scope.var.loading = false;
        if (http.status == 200) {
            download_scope.type_value = JSON.parse(http.responseText);
            //console.log(http.responseText)
        }
    }

};

httpServices.getMethod = function (download_scope, type_id) {
    var url = DOMAIN+app +'/paramLoad/getMethodType';
    var method = "POST";
    var param = "?costTypeId=" + type_id;
    var http = createXMLHTTPObject();
    http.open(method, url + param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.method = JSON.parse(http.responseText);
        }
    }
};

httpServices.getTerr = function (download_scope, type_id) {
    var url = DOMAIN+app +'/paramLoad/getATE';
    var method = "POST";
    var param = "?costTypeId=" + type_id;
    var http = createXMLHTTPObject();
    http.open(method, url + param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.method = JSON.parse(http.responseText);
        }
    }
};

httpServices.getArea = function (download_scope, type_id, terr_id) {
    var url = DOMAIN+app +'/paramLoad/getATE';
    var method = "POST";
    var param = "?costTypeId=" + type_id + "&ate=" + terr_id;
    var http = createXMLHTTPObject();
    http.open(method, url + param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.method = JSON.parse(http.responseText);
        }
    }
};

httpServices.getFun = function (download_scope, type_id, method_id, obj_num) {
    var url = DOMAIN+app +'/paramLoad/getFuncCode';
    var method = "POST";
    var param = "?costTypeId=" + type_id + "&methodId=" + method_id+"&objectnumber=" + obj_num;
    var http = createXMLHTTPObject();
    http.open(method, url + param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.func_val = JSON.parse(http.responseText);
        }
    }
};

httpServices.getCurrency = function (download_scope) {
    var url = DOMAIN+app +'/shapeLoad/getNationalCurrencyList';
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.valuta = JSON.parse(http.responseText);
        }
    }
};

httpServices.getNameOrg = function (download_scope) {
    var url = DOMAIN+app +'/docLoad/getOrgDic';
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.name_org = (JSON.parse(http.responseText))[1];
            download_scope.name_org_this = (JSON.parse(http.responseText))[0];
        }
    }
};


httpServices.getDocImportant = function (download_scope, type_id, method_id, poly_type) {
    var url = DOMAIN+app +'/docLoad/getNeedDocTypeList';
    var method = "POST";
    var param = "?costTypeId=" + type_id + "&methodTypeId=" + method_id;
    var http = createXMLHTTPObject();
    if(poly_type != -1){
        param += "&polyType=" + poly_type;
    }
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.doc_impot = (JSON.parse(http.responseText));
        }
    }
};

httpServices.getDoc = function (download_scope, type_id, method_id, poly_type) {
    console.log(type_id)
    var url = DOMAIN+app +'/docLoad/getNotNeedDocTypeList';
    var method = "POST";
    var param = "?costTypeId=" + type_id + "&methodTypeId=" + method_id;
    var http = createXMLHTTPObject();
    if(poly_type != -1){
        param += "&polyType=" + poly_type;
    }
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            download_scope.doc = (JSON.parse(http.responseText));
        }
    }
};

httpServices.getBookLoad_2 = function (download_scope, load_param) {
    /*var data = {
        columnNums : load_param.columnNums,
        filtr: load_param.filtr,
        columns : load_param.groupParam,
        sortingParam: load_param.sortingParam,
        predicate:load_param.predicate,
        page: load_param.page,
        amount: load_param.amount,
        dateFrom: load_param.dateFrom,
        dateTo: load_param.dateTo

    }
    var url = DOMAIN+app +'/registr/getManagedRecords';
    $.ajax({
        type:'POST',
        data: JSON.stringify(data),
        url: url,
        async: false,
        contentType: 'application/json',
        success: function(res) {
            console.log(res)
            download_scope.bookLoad = res[0];
            download_scope.colPage = res[1];
            //location.reload();
        },
        error: function(res){
            alert(res);
            //           location.reload();
        }
    });*/

    var data = {
        sortingParam: load_param.sortingParam,
        predicate:load_param.predicate,
        page: load_param.page,
        amount: load_param.amount,
        dateFrom: load_param.dateFrom,
        dateTo: load_param.dateTo,
        ateId: load_param.ateId,
        costTypeId: load_param.costTypeId,
        funcCodeId: load_param.funcCodeId,
        actual: load_param.actual
    }
    var url = DOMAIN+app +'/registr/getRecords';
    $.ajax({
        type:'POST',
        data: JSON.stringify(data),
        url: url,
        async: false,
        contentType: 'application/json',
        success: function(res) {
            console.log(res)
            download_scope.bookLoad = res[0];
            download_scope.colPage = res[1];
            //location.reload();
        },
        error: function(res){
            alert(res);
            //           location.reload();
        }
    });
};

httpServices.sendDocFile = function (file, zip) {
    var formData = new FormData();
    var http = createXMLHTTPObject();


    formData.append("file", file);
    formData.append("costTypeId", doc_info.costTypeId);
    formData.append("funcCode", doc_info.funcCode);
    formData.append("ateId", doc_info.ateId);
    formData.append("docTypeId", doc_info.docTypeId);
    formData.append("docNum", doc_info.docNum);
    formData.append("dwd_id", getDwdByJSON());
    formData.append("userId", AUTHOBJ.userId);

    var url = DOMAIN+app +'/docLoad/saveDoc';
    var method = "POST";
    $.ajax({
        url: url,
        data: formData,
        contentType: false,
        processData: false,
        async: false,
        type: method,
        success: function(data){
            var data_json = JSON.parse(data);
            var mess = "";
            console.log(data_json)
            if(data_json.code == 0 || parseInt(data_json.message) == NaN){
                alert("Ошибка записи в базу файла " + file.name)
                IS_ADD = false;
            }else {
                if(data_json.code == 1){
                    mess = "Файл "+file.name + " успешно загружен";
                } else if (data_json.code == 2){
                    mess = data_json.message;
                }
                if(data_json.code == 1 || data_json.code == 2) {
                    if (!zip) {
                        ID_DEL = (parseInt(data_json.id));
                        //alert(ID_DEL);
                    } else {
                        console.log(data_json)
                        ID_DEL_ZIP = (parseInt(data_json.id))
                        next_load = true;
                    }
                    IS_ADD = true;
                    alert(mess)
                } else {
                    alert(data_json.message);
                    IS_ADD = false;
                    next_load = false;
                }
            }
        },
        error: function(data){
            alert("error")
            console.log(data)
            IS_ADD = false;
        }
    });
};

httpServices.sendDocFile_edit = function (file, regNum, docTypeId, userId, scope) {
    var formData = new FormData();

    formData.append("file", file);
    formData.append("docTypeId", parseInt(docTypeId));
    formData.append("regNum", parseInt(regNum));
    formData.append("userId", userId)

    var url = DOMAIN+app +'/registr/editFile';
    var method = "POST";
    $.ajax({
        url: url,
        data: formData,
        contentType: false,
        processData: false,
        async: false,
        type: method,
        success: function(data){
            scope.saveDocObj = JSON.parse(data);
            if(scope.saveDocObj.code != 1) {
                alert(scope.saveDocObj.message);
            }
            if(scope.saveDocObj.code == 1 || scope.saveDocObj.code == 2){
                IS_ADD = true;
            } else { IS_ADD = false;}
        },
        error: function(data){
            alert("error")
        }
    });
};

httpServices.sendLayer_edit = function (geojson, reg_num, layerName, userId, scope) {
    var formData = new FormData();

    formData.append("geojson", geojson);
    formData.append("reg_num", parseInt(reg_num));
    formData.append("layerName", layerName);
    formData.append("userId", userId);

    var url = DOMAIN+app +'/registr/editLayer';
    var method = "POST";
    $.ajax({
        url: url,
        data: formData,
        contentType: false,
        processData: false,
        async: false,
        type: method,
        success: function(data){
            scope.saveLayerObj = JSON.parse(data);
            if(scope.saveLayerObj.code != 1) {
                alert(scope.saveLayerObj.message);
            }
            if(scope.saveLayerObj.code == 1){
                DWD_ID = parseInt(scope.saveLayerObj.message);
                IS_ADD = true;
            } else { IS_ADD = false;}
        },
        error: function(data){
            alert("error")
        }
    });
};

httpServices.deleteDoc = function (idDoc, idElement) {
    var url = DOMAIN+app +'/docLoad/deleteFile';
    var method = "POST"; //ERROR!!!!!!!!!!!!!!!!!!!!!!
    var param = "?docId=" + idDoc;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            var data = JSON.parse(http.responseText);
            if(data.code == 1) {
                IS_DELETE = true;
                $(idElement).removeClass(idDoc + "");
                //alert($(idElement).attr("class"))
                //alert(http.responseText);
                alert(data.message)
            }
            else {
                alert("Ошибка удаления. "+data.message)
            }
        }
    }
};

httpServices.deleteDoc_byID = function (idDoc) {
    var url = DOMAIN+app +'/docLoad/deleteFile';
    var method = "POST"; //ERROR!!!!!!!!!!!!!!!!!!!!!!
    var param = "?docId=" + idDoc;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            var data = JSON.parse(http.responseText);
            if(data.code == 1) {
                IS_DELETE = true;
                //alert(http.responseText);
                alert(data.message)
            } else{
                alert("Ошибка при удалении файла. "+data.message)
            }
        }
    }
};


httpServices.getBookLoad = function (download_scope, load_param) {
    var url = DOMAIN+app +'/registr/getRecords';
    var method = "POST";
    var param = '?groupParam='+load_param.groupParam+'&sortingParam='+load_param.sortingParam+'&predicate='+load_param.predicate+'&page='+load_param.page+'&amount='+load_param.amount+'&dateFrom='+load_param.dateFrom+'&dateTo='+load_param.dateTo;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            console.log(http.responseText)
            download_scope.bookLoad = (JSON.parse(http.responseText))[0];
            download_scope.colPage = (JSON.parse(http.responseText))[1];
        }
    }
};



httpServices.saveObject = function (data) {
    openModalIndex();
    var url = DOMAIN+app +'/shapeLoad/loadData';
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    console.log(data)
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    $.ajax({
        type:'POST',
        data: data,
        url: url,
        contentType: 'application/json',
        success: function(res) {
            var jsn = res;
            var text = "Регистрационный номер операции : " + jsn.regNum + "\n"
                + "Загружен объект: " + jsn.ateName + "\n"
                + "Вид оценки: " + jsn.costTypeName + "\n"
                + "Вид функционального использования:  " + jsn.funcCodeName + "\n"
                + "Количество записей файла Zones: " + jsn.zonesQuantity + "\n"
                + "Количество записей файла Bounds: " + jsn.boundsQuantity + "\n"
                + "Количество загруженных тематических слоев:  " + jsn.layersQuantity + "\n"
                + "Количество загруженных документов: " + jsn.docQuantity + "\n"
                + "Деактуализировано записей:  " + jsn.decObjectQuantity + "\n"
                + "Количество сельских населенных пунктов в файле:  " + jsn.shapeObjectQuantity + "\n"
                + "Количество сельских населенных пунктов выбранном районе:   " + jsn.baseObjectQuantity + "\n";

            alert(text)

            closeModalIndex();
            //location.reload();
        },
        error: function(res){
            alert(res)
            openModalIndex();
            //           location.reload();
        }
    });
};

httpServices.validateValuesZone = function(values){
    var url = DOMAIN+app;

    var formData = new FormData();
    formData.append("costTypeId", doc_info.costTypeId)
    formData.append("objectnumber", doc_info.ateId)

    var val_obj = {
        zones: values,
        costTypeId: doc_info.costTypeId,
        objectnumber: doc_info.ateId
    };

    var val = JSON.stringify(val_obj);

    url+='/shapeLoad/validateZonesValues';
    formData.append("zones", values)

    $.ajax({
        type: "POST",
        data: val,
        url: url,
        async: false,
        contentType: 'application/json',
//        processData: false,
        success: function(res){
            var data = JSON.parse(res);
            var code = data.code;
            if(code == 2){
                var isNext = confirm(data.message);
                if(!isNext){
                    _plus_del(false, "zone")
                    validZoneBound("zone", false);
                }else{
                    setValidZoneBound("zone", true)
                }
            } else if(code == 3 || code == 4){
                alert(data.message)
                _plus_del(false, "zone")
                validZoneBound("zone", false);
            }else{
                setValidZoneBound("zone", true)
            }
        },
        error: function(res){
            alert("error")
            validZoneBound("zone", false);
            console.log(res)
        }
    });
}

httpServices.validateValuesBounds = function(bounds){
    var url = DOMAIN+app+'/shapeLoad/validateBoundsValues';

    $.ajax({
        type: "POST",
        data: bounds,
        url: url,
        async: false,
        contentType: 'application/json',
//        processData: false,
        success: function(res){
            console.log(res)
            var data = JSON.parse(res);
            var code = data.code;
            if(code == 2){
                var isNext = confirm(data.message);
                if(!isNext){
                    delZip(2);
                    validZoneBound("bound", false);
                }else {
                    setValidZoneBound("bound", true);
                }
            } else if(code == 3 || code == 4){
                alert(data.message)
                delZip(2);
                validZoneBound("bound", false);
            }else{
                setValidZoneBound("bound", true);
            }
        },
        error: function(res){
            validZoneBound("bound", true);
            alert("error")
            console.log(res)
        }
    });
};

httpServices.validateFieldsZone = function(fields){

    var url = DOMAIN+app;

    var formData = new FormData();

    formData.append("fields", fields)

    url+='/shapeLoad/validateZonesFields'
    formData.append("costTypeId", doc_info.costTypeId)

    $.ajax({
        type: "POST",
        data: formData,
        url: url,
        async: false,
        contentType: false,
        processData: false,
        success: function(res){
            var data = JSON.parse(res);
            var code = data.code;
            if(code == 2){
                var isNext = confirm(data.message);
                if(!isNext){
                    _plus_del(false, "zone")
                    validZoneBound("zone", false);
                } else{
                    setValidZoneBound("zone", true);
                }
            } else if(code == 3 || code == 4){
                alert(data.message)
                _plus_del(false, "zone")
                validZoneBound("zone", false);
            }else{
                setValidZoneBound("zone", true)
            }
        },
        error: function(res){
            //console.log(res)
            validZoneBound("zone", false);
            alert((JSON.parse(res)).message)
        }
    });
}

httpServices.validateLayersNames = function(layerNames) {
    var url = DOMAIN + app + '/shapeLoad/getValideLayerName';
    var method = 'POST';
    var params = "?layerNames=" + layerNames;
    var http = createXMLHTTPObject();
    http.open(method, url+params, false);
    http.send();
    if (http.readyState == 4) {
        if (http.status == 200) {
            var layers_zip = (JSON.parse(http.responseText))
            if(layers_zip.code != 1){
                alert(layers_zip.message)
                delZip(3);
                arr=[];
            } else {
                arr = layers_zip.data;
            }
        }
    }
}

httpServices.ValideLayerValue = function(data) {
    var url = DOMAIN + app + '/shapeLoad/getValideLayerValue';
    var method = 'POST';
    var params = JSON.stringify(data);
    $.ajax({
        type: method,
        data: params,
        url: url,
        async: false,
        contentType: 'application/json',
        success: function(res){
            console.log(res)
            var layers_zip = (res);
            if(layers_zip.code != 1){
                alert(layers_zip.message)
                delZip(3);
            }
        },
        error: function(res){
            alert("error")
            delZip(3);
            console.log(res)
        }
    });
}


httpServices.validateFieldsBounds = function(fields){
    var url = DOMAIN+app+'/shapeLoad/validateBoundsFields'

    $.ajax({
        type: "POST",
        data: fields,
        url: url,
        contentType: 'application/json',
        async: false,
        success: function(res){
            console.log(res)

            var data = JSON.parse(res);
            var code = data.code;
            if(code == 2){
                var isNext = confirm(data.message);
                if(!isNext){
                    delZip(2);
                    validZoneBound("bound", false);
                } else {
                    setValidZoneBound("bound", true);
                }
            } else if(code == 3 || code == 4){
                alert(data.message + " Архив будет удален.")
                delZip(2);
                validZoneBound("bound", false);
            } else{
                setValidZoneBound("bound", true);
            }
        },
        error: function(res){
            //console.log(res)
            validZoneBound("bound", false);
            alert((JSON.parse(res)).message)
        }
    });
}


httpServices.sendZoneBoundLayer_ZIP = function(zip_file, type_file){
    doc_info.zipType = 1312;
    if(type_file == "zone"){
        doc_info.zipType = 1310;
    }else if(type_file == "bound"){
        doc_info.zipType = 1311;
    }
    doc_info.docTypeId = doc_info.zipType;
    httpServices.sendDocFile(zip_file, true)
};

httpServices.sendGeojson = function(geojson, type){

    var url = DOMAIN+app;

    if(type == 'zone'){
        url+='/shapeLoad/getZonesGeometry';
    } else {
        url+='/shapeLoad/getBoundsGeometry';
    }
    var formData = new FormData();
    formData.append("geojson", geojson);
    formData.append("dwd_id", getDwdByJSON());


    var method = "POST";

    $.ajax({
        url: url,
        data: formData,
        processData: false,
        contentType: false,
        //contentType: 'application/json',
        type: method,
        async: false,
        success: function(data){
            console.log(data + "\n" + getDwdByJSON() + "\n" + "________________________________________________")
            $("#preview-btn").removeAttr("disabled");
            setValidZoneBound(type, true);
        },
        error: function(data){
            alert("ERROR GEOMETRY")
            console.log(data)
            validZoneBound(type, false);
            _plus_del(false, type)
            closeModal();
        }
    });
};

httpServices.returnGeojson = function(geojson, type){
    var url = DOMAIN+app;
    var input = 1;

    if(type == 'zone'){
        url+='/shapeLoad/returnZones';
    } else {
        url+='/shapeLoad/returnBounds';
        input = 2;
    }

    var formData = new FormData();
    formData.append("geojson", geojson);


    var method = "POST";

    $.ajax({
        url: url,
        data: formData,
        processData: false,
        //contentType: 'application/json',
        contentType: false,
        async: false,
        type: method,
        success: function(data){
            //alert("ssss")
            //console.log(data)
            var headers = addToTableDocInfoHead(data, input);
            addToTableDocInfoBody(data, headers, input);
            validZoneBound(type, true);
        },
        error: function(data){
            alert("ERROR GET GEOMERTRY")
            validZoneBound(type, false);
            _plus_del(false, type)
            console.log(data)
        }
    });
};

httpServices.sendDBFFields = function(fields, type){
    var url = DOMAIN+app;

    if(type == 'zone'){
        url+='/shapeLoad/returnZones';
    } else {
        url+='/shapeLoad/returnBounds';
    }

    var formData = new FormData();
    formData.append("fields", fields);
    formData.append("costTypeId", json_send.costTypeId);


    var method = "POST";

    $.ajax({
        url: url,
        data: formData,
        processData: false,
        //contentType: 'application/json',
        contentType: false,

        type: method,
        success: function(data){
            //alert("ssss")
            console.log(data)
        },
        error: function(data){
            console.log(data)
        }
    });
};


httpServices.getDwd = function(){
    var url = DOMAIN+app +'/docLoad/getCommonDocId';
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            return (http.responseText);
        }
    }
}

httpServices.getLayers = function(layerNames) {
    var url = DOMAIN + app + '/shapeLoad/getLayersData';
    var method = 'POST';
    var params = "?layerNames=" + layerNames;
    var http = createXMLHTTPObject();
    http.open(method, url+params, false);
    http.send();
    if (http.readyState == 4) {
        if (http.status == 200) {
            return http.responseText;
        }
    }
}

httpServices.validateLoadObject = function(costTypeId, objectnumber, funcCodeId, dateValStr) {
    var url = DOMAIN + app + '/paramLoad/validateLoadObject';
    var method = 'POST';
    var params = "?costTypeId=" + costTypeId + "&objectnumber="+objectnumber+"&funcCodeId="+funcCodeId+"&dateValStr="+dateValStr;
    /*alert(params)*/
    var http = createXMLHTTPObject();
    http.open(method, url+params, false);
    http.send();
    if (http.readyState == 4) {
        if (http.status == 200) {
            var date = JSON.parse(http.responseText);
            if(date.code==2){
                valid_next_2 = confirm(date.message);
            }else if(date.code != 1){
                alert(date.message)
                valid_next_2 = false;
            } else{
                valid_next_2 = true;
            }
        }
    }
}

httpServices.sendLayer = function(geojson, dwd_id, layerName){
    var url = DOMAIN + app + '/shapeLoad/getLayerGeometry';
    var formdata = new FormData();
    formdata.append('geojson', geojson);
    formdata.append('dwd_id', dwd_id);
    formdata.append('name', layerName);

    console.log(geojson)

    var method = "POST";

    $.ajax({
        url: url,
        data: formdata,
        processData: false,
        contentType: false,
        async: false,
        //contentType: 'application/json',
        type: method,
        success: function(data){
            //alert("sssss")
            var res = data;
            if(res.code !=1){
                alert(res.message);
                ADD_ARR = -1;
            } else {
                IS_ADD = 1;
            }
            console.log(data)
        },
        error: function(data){
            console.log(data)
            next_load = false;
        }
    });
}

httpServices.sendDocInfo = function(){
    var url = DOMAIN + app + '/docLoad/getDocInfo';
    $.ajax({
        type:'POST',
        data: doc_info,
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        success: function(res){
            console.log(res)
//            location.reload();
        },
        error: function(res){
            console.log(res.responseText)
            //           location.reload();
        }
    });
}

//Serch ATE
httpServices.searchByName = function (name, scope) {
    var url = DOMAIN+app +'/registr/searchByATE?partName=' + name;
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            scope.towns_ate = JSON.parse(http.responseText);
        }
    }
};

// Просмотр и редактирование
// info
httpServices.getInfoForEditView = function (id, id_fun, scope) {
    var url = DOMAIN+app +'/registr/getRegInf?regCode=' + id+"&funcCode="+id_fun;
    var method = "GET";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            scope.info_obj = JSON.parse(http.responseText);
        }
    }
};

//docs
httpServices.getDocsForEditView = function (id, scope) {
    var url = DOMAIN+app +'/registr/getDocumentList?regNum=' + id;
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            scope.doc_obj = JSON.parse(http.responseText);
        }
    }
};

httpServices.getLayersForEditView = function (id, scope) {
    var url = DOMAIN+app +'/registr/getLayerList?regCode=' + id;
    var method = "GET";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            scope.layers = JSON.parse(http.responseText);
        }
    }
};

httpServices.getZonesForEditView = function (id, id_fun, scope) {
    var url = DOMAIN+app +'/registr/getZoneList?regCode=' + id + "&funcCode=" + id_fun;
    var method = "GET";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            scope.zonesView = JSON.parse(http.responseText);
        }
    }
};

httpServices.getDocTypeList = function (id, scope) {
    var url = DOMAIN+app +'/registr/getDocTypeList?regNum=' + id;
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            scope.doc_no_imp = JSON.parse(http.responseText);
        }
    }
};

httpServices.saveEditObject = function (data) {
    var url = DOMAIN+app +'/registr/saveEditDoc';
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    console.log(data)
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    $.ajax({
        type:'POST',
        data: data,
        url: url,
        contentType: 'application/json',
        success: function(res) {
            var jsn = res;
            console.log(jsn);
            if(jsn.code != 1) {
                alert(jsn.message)
            }

        },
        error: function(res){
            alert(res)
            //           location.reload();
        }
    });
};

httpServices.autorization = function (username, password) {

    var url = DOMAIN+app +'/auth/greeting';
    var method = "POST";

    var headers = {'username': username, 'password':password};

    $.ajax({
        url: url,
        async: false,
        type: method,
        data: headers,
        success: function(res) {
            console.log(res)
            AUTHOBJ = res;
            if(AUTHOBJ.code != 1){
                if($("#text-no-user").hasClass("display-none")){
                    $("#text-no-user").removeClass("display-none")
                }
                if($("#text-block").hasClass("display-none")){
                    $("#text-block").removeClass("display-none")
                }
                $("#text-block").text("");
                $("#text-block").text(AUTHOBJ.message);
            }else {
                if($("#text-no-user").hasClass("display-none")){
                    $("#text-no-user").addClass("display-none")
                }
                if($("#text-block").hasClass("display-none")){
                    $("#text-block").addClass("display-none")
                }
                if(AUTHOBJ.userDetails.authorities[0].authority == "ADMINISTRATOR"){
                    location.href = '/rsvalue_web/admin.html?' + '__u=' + AUTHOBJ.userDetails.username + '&' + '__p=' + AUTHOBJ.token;
                }else {
                    location.href = '/rsvalue_web/main.html?' + '__u=' + AUTHOBJ.userDetails.username + '&' + '__p=' + AUTHOBJ.token;
                }
            }
        },
        error: function(res){
            alert(res);
            console.log(res)
        }
    });
};

httpServices.autorization_getUser = function () {

    var url = DOMAIN+app +'/auth/getUser';
    var method = "POST";
    console.log(AUTH_FORMDATA)
    $.ajax({
        url: url,
        async: false,
        type: method,
        data: AUTH_FORMDATA,
        success: function(res) {
            console.log(res)
            AUTHOBJ = res;
        },
        error: function(res){
            alert(res);
            console.log(res)
        }
    });
};

httpServices.logOut = function () {

    var url = DOMAIN+app +'/auth/logout';
    var method = "POST";
    $.ajax({
        url: url,
        type: method,
        data: AUTH_FORMDATA,
        success: function(res) {
            location.href = 'index.html';
        },
        error: function(res){
            alert(res);
            console.log(res)
        }
    });
};

// ПОИСК !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
httpServices.getNalogPeriod = function (search_scope) {
    var url = DOMAIN+app +'/message/getPeriodList';
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            console.log(http.responseText)
            search_scope.nalogDate = JSON.parse(http.responseText);
        }
    }
};

httpServices.getObl = function (search_scope) {
    var url = DOMAIN+app +'/message/getAteList';
    var method = "POST";
    //var param = "?costTypeId=" + type_id;
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchAddr_obl = JSON.parse(http.responseText);
        }
    }
}

httpServices.getFunNazn = function (search_scope) {
    var url = DOMAIN+app +'/message/getFuncCode';
    var method = "POST";
    //var param = "?costTypeId=" + type_id;
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchVNP_fun = JSON.parse(http.responseText);
        }
    }
}

httpServices.getLoadFunNazn = function (search_scope) {
    var url = DOMAIN+app +'/message/getLoadFuncCode';
    var method = "POST";
    //var param = "?costTypeId=" + type_id;
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchLoad_fun = JSON.parse(http.responseText);
        }
    }
}

httpServices.getObl_raion_by_ateId = function (search_scope, ateId) {
    var url = DOMAIN+app +'/message/getAteList';
    var method = "POST";
    var param = "?objectnumber=" + ateId;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/1
        if (http.status == 200) {
            search_scope.searchAddr_raion = JSON.parse(http.responseText);
        }
    }
}

httpServices.getObl_st_by_raion = function (search_scope, ateId) {
    var url = DOMAIN+app +'/message/getCommunityGardenList';
    var method = "POST", date = search_scope.date_search;
    if(search_scope.isNalog){
        date=replaceDate(date)
    }
    var param = "?objectnumber=" + ateId + "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchAddr_raionST = JSON.parse(http.responseText);
        }
    }
}

httpServices.getObl_st_res1 = function (search_scope, zoneId, isNb) {

    var url = DOMAIN+app +'/message/communityGardenNameSearch';
    var method = "POST",
        date=search_scope.date_search;
    if(isNb){
        date = replaceDate(search_scope.date_search);
    }
    var param = "?zoneId=" + zoneId + "&isNb=" + isNb+ "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchAddr_raionSTRes_1 = JSON.parse(http.responseText);

            init_st_pdf_object(JSON.parse(http.responseText), 3);

            //вызов функции поиска по карте
            var cql_filter = 'zone_id='+zoneId;
            searchObj(cql_filter,['zones'],'search');
        }
    }
}


httpServices.getby_unp = function (search_scope, objectnumber, unp, isNb, date) {
    var now = (new Date().toLocaleDateString()).split(".");
    var url = DOMAIN+app +'/message/farmLandsUNPSearch';
    var method = "POST";
    isNb = false;
    if(isNb){
        date = replaceDate(date);
    }
    var param = "?objectnumber=" + objectnumber + "&date=" + date + "&unp=" + unp+"&isNb=" +isNb;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchCadasrtCost_byUnp = JSON.parse(http.responseText);

            //вызов функции поиска по карте

            var ob = JSON.parse(http.responseText);
            var cql_filter = 'zonenumber=' + ob.valuesCharacters[0].zoneNum + ' and ' + 'reg_num=' + ob.valuesCharacters[0].foundation.regNum;
            searchObj(cql_filter,['zones'],'search');
        }
    }
}


httpServices.getocen_zona = function (search_scope, ateId, funcode, date) {
    var url = DOMAIN+app +'/message/getLandOutLocalityList';
    var method = "POST";
    if(search_scope.isNalog){
        date=replaceDate(date)
    }
    var param = "?objectnumber=" + ateId + "&funcode=" + funcode + "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchVNP_NameZone = JSON.parse(http.responseText);
            if(search_scope.searchVNP_NameZone == 0){
                alert("По этим данным нет оценочных зон")
            }
        }
    }
}

httpServices.getocen_zona_num = function (search_scope, ateId, funcode, date, zonaName) {
    var url = DOMAIN+app +'/message/getZoneNumberList';
    var method = "POST";
    if(search_scope.isNalog){
        date=replaceDate(date)
    }
    var param = "?objectnumber=" + ateId + "&funcode=" + funcode + "&date=" + date + "&zoneName=" + zonaName;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchVNP_NumZone = JSON.parse(http.responseText);
        }
    }
}

httpServices.getVNP_res = function (search_scope, zoneId, isNb, date) {
    var url = DOMAIN+app +'/message/landsOutLocalitySearch';
    var method = "POST";
    if(isNb){
        date = replaceDate(date);
    }
    var param = "?zoneId=" + zoneId + "&isNb=" + isNb + "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchVNP_Res = JSON.parse(http.responseText);

            init_st_pdf_object(JSON.parse(http.responseText), 4);

            //вызов функции поиска по карте
            var cql_filter = 'zone_id='+zoneId;
            searchObj(cql_filter,['zones'],'search');


        }
    }
}

httpServices.getname_Zemlep_search2 = function (search_scope, objNum, date) {
    var url = DOMAIN+app +'/message/getTenantNameList';
    var method = "POST";
    var param = "?objectnumber=" + objNum  + "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchZemlep_Name = JSON.parse(http.responseText);
        }
    }
}

httpServices.getresBy_Zemlep_search2 = function (search_scope, objectnumber, tenantName, isNb, date) {
    var url = DOMAIN+app +'/message/farmLandsNameSearch';
    var method = "POST";
    isNb = false;
    if(isNb){
        date = replaceDate(date);
    }
    var param = "?tenantName=" + tenantName + "&isNb=" + isNb + "&date=" + date+"&objectnumber="+objectnumber;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchZemlep_Res = JSON.parse(http.responseText);

            //вызов функции поиска по карте

            var ob = JSON.parse(http.responseText);
            var cql_filter = 'zonenumber=' + ob.valuesCharacters[0].zoneNum + ' and ' + 'reg_num=' + ob.valuesCharacters[0].foundation.regNum;
            searchObj(cql_filter,['zones'],'search');
        }
    }
}


httpServices.getnum_NumZone_search2 = function (search_scope, objNum, date) {
    var url = DOMAIN+app +'/message/getFarmZoneNumberList';
    var method = "POST";
    var param = "?objectnumber=" + objNum  + "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchNumZone_Num = JSON.parse(http.responseText);
        }
    }
}

httpServices.getresBy_NumZone_search2 = function (search_scope, zoneId, isNb, date) {
    var url = DOMAIN+app +'/message/farmLandsZonenNumberSearch';
    var method = "POST";
    isNb = false;
    if(isNb){
        date = replaceDate(date);
    }
    var param = "?zoneId=" + zoneId + "&isNb=" + isNb + "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchNumZone_Res = JSON.parse(http.responseText);

            init_st_pdf_object(JSON.parse(http.responseText), 5);
            //вызов функции поиска по карте
            var cql_filter = 'zone_id='+zoneId;
            searchObj(cql_filter,['zones'],'search');
        }
    }
}


httpServices.getres_byCadastr=function(search_scope, soato, blockNum, parcelNum, isNb, date){
    var url = DOMAIN+app +'/message/cadastrValueSearch';
    var method = "POST";
    if(isNb){
        date = replaceDate(date);
    }
    var param = "?soato=" + soato + "&blockNum=" +blockNum +"&parcelNum=" +parcelNum+ "&isNb=" + isNb + "&date=" + date;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            var ob = JSON.parse(http.responseText);
            if(ob.code == 1) {
                CADASTR_RES=1;

                //вызов функции поиска по карте
                if (ob.data.cadNum) {
                    var cql_filter = 'cad_num='+ob.data.cadNum;
                    searchObj(cql_filter,['parcels'],'search');
                }

                search_scope.searchCadastr_Res = ob.data;
                init_pdf_object(ob, 1);

                //addCadastrPdf();
            }else{
                alert(ob.message);
                CADASTR_RES=-1;
            }
        }
    }
};

httpServices.createPdf=function(search_scope, pdf){
    var url = DOMAIN+app +'/message/createPdf';
    var params = "?isNb=" + pdf.isNb;
    var method = "POST";
    $.ajax({
        type:method,
        data: JSON.stringify(pdf),
        url: url + params,
        async: false,
        contentType: 'application/json',
        success: function(res) {
            search_scope.pdfUrl = res.data;
            alert(search_scope.pdfUrl)
            //location.reload();
        },
        error: function(res){
            alert(res);
            //           location.reload();
        }
    });
};

httpServices.getPdf = function (search_scope) {
    var url = DOMAIN + app + '/message/pdf/get';
    var param = "?fileName=" + search_scope.pdfUrl;
    return url + param;
};

httpServices.deletePdf = function (search_scope) {
    var url = DOMAIN + app + '/message/pdf/delete';
    var param = "?fileName=" + search_scope.pdfUrl;
    var method = "GET";
    var http = createXMLHTTPObject();
    http.open(method, url+param, true);
    http.send();
};


httpServices.getOblAddr = function(search_scope){
    var url = DOMAIN+app +'/message/getListAddress?region=1&typeIn=1&typeOut=1&area=&ss=&snp=&bjd=&road=&km=&street=';
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchAddr_obl = JSON.parse(http.responseText);

        }
    }
}


httpServices.getAddressElement = function(search_scope, idElement, prevEl){
    var url = DOMAIN+app +'/message/getAddressElementList';
    var method = "POST";
    var param = "?parentElementId=" + idElement;
    if(prevEl!=null)
        param+="&objectnumber="+prevEl;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            search_scope.searchAddr_El = JSON.parse(http.responseText);

        }
    }
}

httpServices.getAddressData = function(search_scope, region, typeIn, typeOut, area, ss, snp, bjd, road, km, street){
    var url = DOMAIN+app +'/message/getListAddress';
    var method = "POST";
    var param = "?region="+region+"&typeIn="+typeIn+"&typeOut="+typeOut+"&area="+area+"&ss="+ss+"&snp="+snp+"&bjd="+bjd+"&road="+road+"&km="+km+"&street=" + street;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        /*scope.var.loading = false;*/
        if (http.status == 200) {
            var data = JSON.parse(http.responseText);
            if(typeOut==2) {
                search_scope.searchAddr_dataRegion = data;
            }else if(typeOut==4) {
                search_scope.searchAddr_dataNasPunkt = data;
            }else if(typeOut==9) {
                search_scope.searchAddr_dataNumZd = data;
            }else if(typeOut==3) {
                search_scope.searchAddr_dataSelsovet = data;
            }else if(typeOut==6) {
                search_scope.searchAddr_dataAvtod = data;
            }else if(typeOut==5) {
                search_scope.searchAddr_dataBGD = data;
            }else if(typeOut==7) {
                search_scope.searchAddr_dataRasst = data;
            }else if(typeOut==8) {
                search_scope.searchAddr_dataYlica = data;
            }
        }
    }
}


    httpServices.getAddressResult = function (search_scope, objectnumber, isNb, date) {
        //  downloadImg();
        var url = DOMAIN + app + '/message/addressSearch';
        var method = "POST";
        if (isNb) {
            date = replaceDate(date);
        }
        var param = "?objectnumber=" + objectnumber + "&isNb=" + isNb + "&date=" + date;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            /*scope.var.loading = false;*/
            search_scope.resultAddrSearch = JSON.parse(http.responseText);

            if (http.status == 200) {
                var ob = JSON.parse(http.responseText);
                if (ob.code == 1) {

                    //вызов функции поиска по карте
                    if (ob.cadnumList[0]) {
                        var cql_filter = 'cad_num=' + ob.cadnumList[0];
                        searchObj(cql_filter, ['parcels'], 'search');
                    }

                } else if (ob.code == 2) {
                    var location = ob.coordinates;
                    doZoom(location);
                } else {
                    //  alert(ob.message)
                }
            }
        }
    }

    httpServices.getAddressResultByAddres = function (search_scope, address, zone, isNb, date) {
        var url = DOMAIN + app + '/message/pointValueSearch';
        var method = "POST";
        if (isNb) {
            date = replaceDate(date);
        }
        var param = "?address=" + address + "&zone=" + zone + "&isNb=" + isNb + "&date=" + date;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            /*scope.var.loading = false;*/
            var date = JSON.parse(http.responseText);
            if (date.code == 1) {
                search_scope.resultAddrSearchADDRESS = date.data;
                init_pdf_object(date, 2);
                return 1;
            } else {
                return 0;
            }
        }
    }

    httpServices.getResultByCoord = function (coord, isCadNum) {
        var url = DOMAIN + app + '/message/mapSearch';
        var method = "POST";
        var param = "?coord=" + coord + "&isCadNum=" + isCadNum;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            search_scope.resultAddrSearch = JSON.parse(http.responseText);
            var data = search_scope.resultAddrSearch;
            if (data.code == 1) {
                closeAllRes();
                if (!$("#res-search-addr-1-3").hasClass("display-none")) {
                    $("#res-search-addr-1-3").addClass("display-none");
                }

                // поиск по кадастровому номеру
                if (data.cadnumList.length == 1) {
                    if (!$("#res-search-addr-1-2").hasClass("display-none")) {
                        $("#res-search-addr-1-2").addClass("display-none");
                    }

                    console.log(data.cadnumList)
                    console.log(data.cadnumList.length)
                    if ($("#res-search-addr-1-1").hasClass("display-none")) {
                        $("#res-search-addr-1-1").removeClass("display-none");
                    }
                    $("#cad-num-by-addr-1").text("")
                    $("#cad-num-by-addr-1").text(data.cadnumList[0])
                } else {
                    if (!$("#res-search-addr-1-1").hasClass("display-none")) {
                        $("#res-search-addr-1-1").addClass("display-none");
                    }

                    if ($("#res-search-addr-1-2").hasClass("display-none")) {
                        $("#res-search-addr-1-2").removeClass("display-none");
                    }
                    $("#cad-num-by-addr-2").empty();
                    $("#addr-2-view").attr("disabled", "disabled");
                    $("#cad-num-by-addr-2").append(function () {
                        var text = "";
                        for (var i = 0; i < data.cadnumList.length; i++) {
                            var cad = data.cadnumList[i];
                            var soato = Number(cad.substring(0, 10)),
                                blockNum = Number(cad.substring(10, 12)),
                                parcelNum = Number(cad.substring(12, 19));
                            text += "<div class='radio'><label class='radio-inline'><input type='radio' onclick='onCadastrAddr(" + soato + ", " + blockNum + ", " + parcelNum + ")' name='optradio-addr' id='search-by-cadastr-" + i + "'>Земельный часток с кадастровым номером " + cad + "</label></div>";
                        }
                        return text;
                    })
                }
                allSearchClean();
            } else if (data.code == 2) {
                closeAllRes();
                if (!$("#res-search-addr-1-1").hasClass("display-none")) {
                    $("#res-search-addr-1-1").addClass("display-none");
                }
                if (!$("#res-search-addr-1-2").hasClass("display-none")) {
                    $("#res-search-addr-1-2").addClass("display-none");
                }
                // поиск по адресной точке
                if ($("#res-search-addr-1-3").hasClass("display-none")) {
                    $("#res-search-addr-1-3").removeClass("display-none");
                }
                $("#cad-num-by-addr-3").empty();
                $("#cad-num-by-addr-3").append(function () {
                    var coord = data.address.split(' '),
                        res = parseFloat(coord[0]).toFixed(6) + " " + parseFloat(coord[1]).toFixed(6);
                    return res;
                });

                $("#cad-num-by-addr-3-lable").empty();
                $("#cad-num-by-addr-3-lable").append(function () {
                    return "Координаты выбранного местоположения:";
                });
                allSearchClean();
            } else {
                alert(data.response)
            }
        }
    }

// ПРЕЙСКУРАНТ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// ПРЕЙСКУРАНТ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    httpServices.getTORPrice = function (price_scope, userId) {
        var url = DOMAIN + app + '/price/getTorList';
        var method = "POST";
        var param = "?userId=" + userId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            /*scope.var.loading = false;*/
            price_scope.tors = JSON.parse(http.responseText);
        }
    }

    httpServices.getPrices = function (price_scope, sortingParam, predicate, page, torId, amount, userId) {
        var url = DOMAIN + app + '/price/getPriceListWithSorting';
        var method = "POST";
        var param = "?sortingParam=" + sortingParam + "&predicate=" + predicate + "&page=" + page + "&torId=" + torId + "&amount=" + amount + "&userId=" + userId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            /*scope.var.loading = false;*/
            var obj = JSON.parse(http.responseText);
            price_scope.pricesBook = obj[0];
            price_scope.pricesBook_str = obj[1];
            price_scope.pricesBook_prices = obj[2];
        }
    }

    httpServices.editReadNewPrices = function (price_scope, priceId, userId) {
        var url = DOMAIN + app + '/price/readCreateEditPriceNew';
        var method = "POST";
        var param = "?userId=" + userId + "&priceId=" + priceId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            /*scope.var.loading = false;*/
            var obj = JSON.parse(http.responseText);
            console.log(obj)
            price_scope.price = obj;
        }
    }

    httpServices.getTypesStop = function (price_scope) {
        var url = DOMAIN + app + '/price/getOperType';
        var method = "POST";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            /*scope.var.loading = false;*/
            price_scope.typePriceStop = JSON.parse(http.responseText);
            ;
        }
    }

    httpServices.stopPrice = function (price_scope, userId, priceId, date, operId) {
        var url = DOMAIN + app + '/price/stopPrice';
        var method = "POST";
        var param = "?priceId=" + priceId + "&date=" + date + "&userId=" + userId + "&operTypeId=" + operId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            /*scope.var.loading = false;*/
            price_scope.resPriceStop = JSON.parse(http.responseText);
            ;
        }
    }

    httpServices.addEditPrice = function (price_scope, savePrice) {
        var url = DOMAIN + app + '/price/savePrice';
        var method = "POST";
        $.ajax({
            type: 'POST',
            data: JSON.stringify(savePrice),
            url: url,
            contentType: 'application/json',
            success: function (res) {
                var jsn = res;
                console.log(jsn)
            },
            error: function (res) {
                alert(res)
                //           location.reload();
            }
        });
    }


// АДМИНКА

    httpServices.get_groups = function (admin_scope) {
        var url = DOMAIN + app + '/admin/groups';
        var method = "GET";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            admin_scope.groups = JSON.parse(http.responseText);
        }
    };

    httpServices.get_orgs = function (admin_scope) {
        var url = DOMAIN + app + '/admin/tors';
        var method = "GET";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            admin_scope.tors = JSON.parse(http.responseText);
        }
    };

    httpServices.get_subjects = function (admin_scope) {
        var url = DOMAIN + app + '/admin/subjects';
        var method = "GET";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            admin_scope.subjects = JSON.parse(http.responseText);
        }
    };

    httpServices.send_userdata = function (user_data) {
        var url = DOMAIN + app + '/admin/user';
        var method = "POST";
        var data = {
            fio: user_data.fio,
            username: user_data.username,
            password: user_data.password,
            position: user_data.position,
            organization: user_data.organization,
            group: user_data.group
        };
        $.ajax({
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            processData: false,
            type: method,
            success: function (data) {
                if (data != 0) {
                    alert("Пользователь успешно добавлен");
                    location.reload();
                } else {
                    alert("При добавлении пользователя произошла ошибка");
                    clean_fields();
                }
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    };

    httpServices.getAccounts = function (admin_scope) {
        var url = DOMAIN + app + '/admin/accounts';
        var method = "GET";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            admin_scope.accounts = JSON.parse(http.responseText);
        }
    };

    httpServices.get_accounts_records = function (admin_scope, page, record_count) {
        var url = DOMAIN + app + '/admin/accounts';
        var method = "GET";
        var param = "?page=" + page + "&record_count=" + record_count;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            admin_scope.accounts = JSON.parse(http.responseText);
        }
    };

    httpServices.get_logins = function (admin_scope) {
        var url = DOMAIN + app + '/admin/logins';
        var method = "GET";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            admin_scope.logins = JSON.parse(http.responseText);
        }
    };

    httpServices.get_row_count = function () {
        var url = DOMAIN + app + '/admin/rowcount';
        var method = "GET";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            row_count = http.responseText;
        }
    };

    httpServices.delete_user = function (id) {
        var url = DOMAIN + app + '/admin/user';
        var method = "DELETE";
        var param = "?id=" + id;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            alert("Пользователь успешно удален!");
            delete_table_change(1);
            edit_table_change(1);
        }
    };

    httpServices.update_user = function (user_data) {
        var url = DOMAIN + app + '/admin/user';
        var method = "PUT";
        var data = {
            id: user_data.id,
            fio: user_data.fio,
            username: user_data.username,
            password: user_data.password,
            position: user_data.position,
            organization: user_data.organization,
            group: user_data.group
        };
        $.ajax({
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            processData: false,
            async: false,
            type: method,
            success: function (data) {
                httpServices.get_logins(admin_scope);
                httpServices.get_subjects(admin_scope);
                init_logins();
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    };

    httpServices.set_delete_filter = function (filter, page, record_count) {
        var url = DOMAIN + app + '/admin/delete/filter';
        var method = "GET";
        var param = "?surname=" + filter.surname + "&login=" + filter.login + "&org=" + filter.org + "&position=" + filter.position + "&group=" + filter.group + "&page=" + page + "&record_count=" + record_count;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            delete_admin_scope.accounts = JSON.parse(http.responseText);
        }
    };

    httpServices.set_delete_filter_row = function (filter) {
        var url = DOMAIN + app + '/admin/delete/filter/row';
        var method = "GET";
        var param = "?surname=" + filter.surname + "&login=" + filter.login + "&org=" + filter.org + "&position=" + filter.position + "&group=" + filter.group;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            delete_row_count = JSON.parse(http.responseText);
        }
    };

    httpServices.set_edit_filter = function (filter, page, record_count) {
        var url = DOMAIN + app + '/admin/edit/filter';
        var method = "GET";
        var param = "?surname=" + filter.surname + "&login=" + filter.login + "&org=" + filter.org + "&group=" + filter.group + "&page=" + page + "&record_count=" + record_count;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            edit_admin_scope.accounts = JSON.parse(http.responseText);
        }
    };

    httpServices.set_edit_filter_row = function (filter) {
        var url = DOMAIN + app + '/admin/edit/filter/row';
        var method = "GET";
        var param = "?surname=" + filter.surname + "&login=" + filter.login + "&org=" + filter.org + "&group=" + filter.group;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            edit_row_count = JSON.parse(http.responseText);
        }
    };

// ЗАКАЗЫ

    httpServices.get_order_states = function () {
        var url = DOMAIN + app + '/orders/states';
        var method = "GET";
        var http = createXMLHTTPObject();
        http.open(method, url, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.states = check_response(responseData);
        }
    };

    httpServices.change_order_state = function (orderId, state, userId) {
        var url = DOMAIN + app + '/orders/state';
        var method = "PUT";
        var param = "?orderId=" + orderId + "&state=" + state + "&userId=" + userId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.change_state = check_response(responseData);
        }
    };

    httpServices.get_user_list = function (userId, orgName) {
        var url = DOMAIN + app + '/orders/users';
        var method = "GET";
        var param = "?userId=" + userId + "&orgName=" + orgName;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.users = check_response(responseData);
        }
    };

    httpServices.get_orders_list = function (order_filter) {
        var url = DOMAIN + app + '/orders/list';
        var method = "GET";
        var param = "?userId=" + AUTHOBJ.userId + "&orgId=" + order_filter.orgId + "&urgency=" + order_filter.urgency + "&decl=" + order_filter.decl +
            "&isNb=" + order_filter.isNb + "&orderState=" + order_filter.orderState + "&filterUserId=" + order_filter.filterUserId +
            "&sort=" + order_filter.sort + "&sortType=" + order_filter.sortType + "&page=" + order_filter.page + "&qty=" + order_filter.qty;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.orders = check_response(responseData);
        }
    };

    httpServices.get_editable_order = function (orderId) {
        var url = DOMAIN + app + '/orders/order/edit';
        var method = "GET";
        var param = "?orderId=" + orderId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.editable_order = check_response(responseData);
        }
    };

    httpServices.get_new_order_number = function (userId) {
        var url = DOMAIN + app + '/orders/order/new';
        var method = "GET";
        var param = "?userId=" + userId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_number = check_response(responseData);
        }
    };

    httpServices.add_new_order = function (order_data) {
        var url = DOMAIN + app + '/orders/order/add';
        var method = "POST";
        var http = createXMLHTTPObject();
        var data = {
            orderId: order_data.orderId,
            userId: order_data.userId,
            orderNumber: order_data.orderNumber,
            urgency: order_data.urgency,
            isNb: order_data.isNb,
            declType: order_data.declType,
            declName: order_data.declName,
            unp: order_data.unp,
            nkName: order_data.nkName,
            phoneNumber: order_data.phoneNumber,
            email: order_data.email,
            address: order_data.address,
            index: order_data.index,
            ate: order_data.ate,
            numIsx: order_data.numIsx,
            dateIsx: order_data.dateIsx,
            numVx: order_data.numVx,
            dateVx: order_data.dateVx
        };
        $.ajax({
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            processData: false,
            async: false,
            type: method,
            success: function (data) {
                responseData = data;
                order_scope.add_order = check_response(responseData);
                check_adding_order();
            },
            error: function (data) {
                console.log(data);
            }
        });

    };

    httpServices.carastar_value_order_search = function (soato, block, parcel, date) {
        var url = DOMAIN + app + '/orders/process/cadnum';
        var method = "GET";
        var param = "?soato=" + soato + "&blockNum=" + block + "&parcelNum=" + parcel + "&date=" + date + "&userId=" + AUTHOBJ.userId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_process_search = check_response(responseData);
        }
    };

    httpServices.st_carastar_value_order_search = function (soato, block, parcel, date) {
        var url = DOMAIN + app + '/orders/process/st/cadnum';
        var method = "GET";
        var param = "?soato=" + soato + "&blockNum=" + block + "&parcelNum=" + parcel + "&date=" + date + "&userId=" + AUTHOBJ.userId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_st_process_search = check_response(responseData);
        }
    };

    httpServices.ate_list_order_search = function (parentId) {
        var url = DOMAIN + app + '/orders/process/ate/list';
        var method = "GET";
        var param = "?parentId=" + parentId + "&userId=" + AUTHOBJ.userId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_process_ate_areas = check_response(responseData);
        }
    };

    httpServices.st_list_order_search = function (number, date) {
        var url = DOMAIN + app + '/orders/process/st/list';
        var method = "GET";
        var param = "?number=" + number + "&date=" + date;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_process_st_list = check_response(responseData);
        }
    };

    httpServices.sx_list_order_search = function (number, date, type) {
        var url = DOMAIN + app + '/orders/process/sx/list';
        var method = "GET";
        var param = "?number=" + number + "&date=" + date + "&type=" + type;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_process_sx_list = check_response(responseData);
        }
    };

    httpServices.sx_search_order = function (type, text, ateId, date) {
        var url = DOMAIN + app + '/orders/process/sx/search';
        var method = "GET";
        var param = "?type=" + type + "&text=" + text + "&ateId=" + ateId + "&date=" + date;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_process_sx_search = check_response(responseData);
        }
    };

    httpServices.get_order_objects = function () {
        var url = DOMAIN + app + '/orders/process/info';
        var method = "GET";
        var param = "?orderId=" + current_order_id;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.order_objects = check_response(responseData);
        }
    };

httpServices.save_order = function (objId, execId) {
    var url = DOMAIN + app + '/orders/order/confirm';
    var method = "GET";
    var param = "?objId=" + objId + "&execId=" + execId;
    var http = createXMLHTTPObject();
    http.open(method, url + param, false);
    http.send();
    if (http.readyState == 4) {
        responseData = JSON.parse(http.responseText);
        order_scope.order_objects = check_response(responseData);
    }
};

httpServices.save_Allorder = function (orderId, execId) {
    var url = DOMAIN + app + '/orders/process/save/all';
    var method = "GET";
    var param = "?orderId=" + orderId + "&execId=" + execId;
    var http = createXMLHTTPObject();
    http.open(method, url + param, false);
    http.send();
    if (http.readyState == 4) {
        if((JSON.parse(http.responseText)).data == 1){
            alert("Заказ успешно завершён")
        }else{
            alert((JSON.parse(http.responseText)).message);
        }
    }
};

    httpServices.add_order_object = function () {
        var url = DOMAIN + app + '/orders/process/add';
        var method = "POST";
        var data = {
            orderId: order_object.orderId,
            searchType: order_object.searchType,
            date: order_object.date,
            distrId: order_object.distrId,
            parcelId: order_object.parcelId,
            orgId: order_object.orgId,
            addressId: order_object.addressId,
            prevAddress: order_object.prevAddress,
            coordinates: order_object.coordinates,
            sId: order_object.sId
        };
        $.ajax({
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            processData: false,
            async: false,
            type: method,
            success: function (data) {
                responseData = data;
                check_adding_order_object(responseData);
            },
            error: function (data) {
                console.log(data);
            }
        });
    };

    httpServices.get_objects_zones = function (objId) {
        var url = DOMAIN + app + '/orders/process/cost';
        var method = "GET";
        var param = "?objId=" + objId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.object_zones = check_response(responseData);
        }
    };

    httpServices.update_zones = function (objId, funCode, zoneId) {
        var url = DOMAIN + app + '/orders/process/update/zone';
        var method = "PUT";
        var param = "?objId=" + objId + "&funcCode=" + funCode + "&zoneId=" + zoneId;
        var http = createXMLHTTPObject();
        http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            responseData = JSON.parse(http.responseText);
            order_scope.object_zones = check_response(responseData);
        }
    };


    httpServices.get_doc_v = function (orderId, userId) {

        var url = DOMAIN + app + '/orders/process/doc/create';
        var method = "GET";
        var param = "?objId=" + orderId + "&execId=" + userId;
        var http = createXMLHTTPObject();
        /*http.open(method, url + param, false);
        http.send();
        if (http.readyState == 4) {
            order_scope.docFile = http.responseText;
        }*/
        window.location = url + param;
    };

httpServices.update_location = function (objectId, coordinates) {
    //objId" (id объекта), "coordinates" (координаты)
    var url = DOMAIN + app + '/orders/process/update/location';
    var method = "PUT";
    var param = "?objId=" + objectId + "&coordinates=" + coordinates;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        responseData = JSON.parse(http.responseText);
        order_scope.order_objects = check_response(responseData);
        if (responseData.data && responseData.code == 1) {
            Object.keys(order_scope.order_objects).forEach(function (key) {
                if (order_scope.order_objects[key].objectId == current_object.objectId) {
                    current_object = order_scope.order_objects[key];
                    search_order_obj_for_map(current_object);
                    httpServices.get_objects_zones(objectId);
                    set_zones_in_table(order_scope.object_zones);
                }
            });
        } else {
            console.log(responseData);
        }
    }
};

httpServices.reset_location = function (objectId) {
    var url = DOMAIN  + app + '/orders/process/location/reset';
    var method = POST;
    var param = "?objId=" + objectId;
    var http = createXMLHTTPObject();
    http.open(method, url+param, false);
    http.send();
    if (http.readyState == 4) {
        Object.keys(order_scope.order_objects).forEach(function (key) {
            if (order_scope.order_objects[key].objectId == current_object.objectId) {
                current_object = order_scope.order_objects[key];
                search_order_obj_for_map(current_object);
                httpServices.get_objects_zones(objectId);
                set_zones_in_table(order_scope.object_zones);
            }
        });
    }
};

httpServices.delete_order = function (orderId, all) {
    var url = DOMAIN + app + '/orders/process/remove';
    if(all){
        url += '/all?orderId=' + orderId;
    }else{
        url += '?objId=' + orderId;
    }
    var method = "POST";
    var http = createXMLHTTPObject();
    http.open(method, url, false);
    http.send();
    if (http.readyState == 4) {
        if (!all) {
            console.log(JSON.parse(http.responseText))
        }
    }
}

// ФУНКЦИИ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function setJSON (json_send_var){
    json_send = json_send_var;
    setDocINFO();
}

function setDocNum (doc_num){
    doc_info.docTypeId = doc_num;
}

function setDocINFO (){

    var doc = {
        costTypeId: parseInt(json_send.costTypeId),
        funcCode: parseInt(json_send.funcCodeId),
        ateId: parseInt(json_send.objectNumber),
        docTypeId: parseInt(json_send.docTypeId),
        docNum: 1
    }

    doc_info = doc;
}

function delZip(n){
    var id, type, id_del;
    if(n==1){
        id = parseInt($("#del-zone").attr('class'));
        type = "zone";
        id_del =  "#del-zone";
    }else if(n==2){
        id = parseInt($("#del-bound").attr('class'));
        type = "bound";
        id_del =  "#del-bound";
    } else if(n==3){
        id = parseInt($("#del-thems").attr('class'));
        type = "thems";
        id_del =  "#del-thems";
    }
    httpServices.deleteDoc(id,id_del);
    if(IS_DELETE) {
        removeTabStr(type);
        _plus_del(false, type)
        zipID.splice(zipID.indexOf(id), 1)
        IS_DELETE = false;
    }

}

function check_response(responseData) {
    var dataObject;
    if (responseData.code == 1) {
        dataObject = responseData.data;
    } else if (responseData.code == 2) {
        alert("зон более одной");
        dataObject = responseData.data
    }else if (responseData.code == 3) {
        alert("такой зоны нет");
        dataObject = responseData.data;
    }
    else {
        alert(responseData.message);
    }
    responseData = [];
    return dataObject;
}

function check_adding_order_object(responseData) {
    alert(responseData.message);
    if (responseData.code == 1) {
        order_scope.order_objects = responseData.data;
        load_order_objects_table();
    }
}

function check_adding_order() {
    if (order_scope.add_order == 1) {
        if (order_scope.editable_order == null) {
            alert("Заказ успешно добавлен");

        } else {
            alert("Заказ успешно изменен");
        }
        order_filter.page = 1;
        reload_order_table();
        close_order();
    }
    if (order_scope.add_order == -1) {
        alert("Ошибка при добавлении заказа");
    }
    order_scope.add_order = null;
}

function init_pdf_object(ob, method) {
    null_pdf_object();
    searchPdf.searchMethodValue = method;
    searchPdf.data.address = ob.data.address;
    searchPdf.data.searchDate = ob.data.searchDate;
    searchPdf.data.cadNum = ob.data.cadNum;
    searchPdf.data.square = ob.data.square;
    searchPdf.data.purposeDic = ob.data.purposeDic;
    searchPdf.data.purposeGov = ob.data.purposeGov;
    searchPdf.data.communityGardenName = ob.data.communityGardenName;
    searchPdf.data.nearLocality = ob.data.nearLocality;
    searchPdf.data.zoneName = ob.data.zoneName;
    searchPdf.data.unp = ob.data.unp;
    searchPdf.data.tenantName = ob.data.tenantName;
    searchPdf.data.remark = ob.data.remark;
    searchPdf.valuesCharacters = ob.data.valuesCharacters;
}

function init_st_pdf_object(ob, method) {
    null_pdf_object();
    searchPdf.searchMethodValue = method;
    searchPdf.data.address = ob.address;
    searchPdf.data.searchDate = ob.searchDate;
    searchPdf.data.cadNum = ob.cadNum;
    searchPdf.data.square = ob.square;
    searchPdf.data.purposeDic = ob.purposeDic;
    searchPdf.data.purposeGov = ob.purposeGov;
    searchPdf.data.communityGardenName = ob.communityGardenName;
    searchPdf.data.nearLocality = ob.nearLocality;
    searchPdf.data.zoneName = ob.zoneName;
    searchPdf.data.unp = ob.unp;
    searchPdf.data.tenantName = ob.tenantName;
    searchPdf.data.remark = ob.remark;
    searchPdf.valuesCharacters = ob.valuesCharacters;
}

function null_pdf_object() {
    searchPdf.searchMethodValue = "";
    searchPdf.data.address = "";
    searchPdf.data.searchDate = "";
    searchPdf.data.cadNum = "";
    searchPdf.data.square = "";
    searchPdf.data.purposeDic = "";
    searchPdf.data.purposeGov = "";
    searchPdf.data.communityGardenName = "";
    searchPdf.data.nearLocality = "";
    searchPdf.data.zoneName = "";
    searchPdf.data.unp = "";
    searchPdf.data.tenantName = "";
    searchPdf.data.remark = "";
    searchPdf.valuesCharacters = "";
}