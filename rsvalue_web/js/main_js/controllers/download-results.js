/**
 * Created by Shcherbina on 03.06.2016.
 */
var DELETE_ALL_DIALOG = false;
var arr_id=[];
arr_id.objectnumber = -1;
arr_id.polyType = -1;
arr_id.meth_val=-1;
var TEXT_FIND;
var FILTR = [];

$(document).ready(function () {
    if (param('__p', '__u') != -1) {
        httpServices.autorization_getUser();
        console.log(AUTHOBJ.personData==null)
        if(AUTHOBJ.personData == null || AUTHOBJ == null) {
            $("#str-index").css("display", "none");
            $("#index-404").css("display", "block");
        } else{
            $("#str-index").css("display", "block");
            $("#index-404").css("display", "none");
            $("#auth-name").text(AUTHOBJ.personData);
        }
    }
});

$(document).ready(function () {

    if (AUTHOBJ.personData != null) {

        var json_send = [], download_scope = [];
    json_send.docList = [];
    json_send.decOrgId = -1;
    json_send.valOrgId = -1;
    json_send.layerList = null;
    json_send.boundList = null;
    var DELETE_INFO_STR = "Будут потеряны данные, заполненные на следующей странице. Желаете продолжить?";
    var now = new Date().toLocaleDateString();

    $("#now-date").val(now);
    $("#now-date-2").val(now);
    $("#now-date-3").val(now);

    httpServices.getTypeValue(download_scope);
    httpServices.getNameOrg(download_scope);
    httpServices.getCurrency(download_scope);
    httpServices.searchByName('', download_scope);
        httpServices.getLoadFunNazn(download_scope);
        httpServices.getFunNazn(search_scope);


    var load_page_param = {
        sortingParam: 1,
        predicate: 'desc',
        page: '1',
        amount: '5',
        dateFrom: '',
        dateTo: '',
        ateId: "",
        costTypeId: "",
        funcCodeId: "",
        actual: ""
    };

        reload();
        setFilters();



  /*  function set_group(group) {
        if (load_page_param.groupParam.indexOf(group) >= 0 && load_page_param.groupParam.length != 1) {
            var index = load_page_param.groupParam.indexOf(group);
            load_page_param.groupParam.splice(index, 1);
        } else if (load_page_param.groupParam.length == 1 && load_page_param.groupParam[0] == group) {
            load_page_param.groupParam = [];
        } else if (group == -1) {
            load_page_param.groupParam = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        } else if (group == 0) {
            load_page_param.groupParam = [];
        } else {
            if (load_page_param.groupParam != []) {
                load_page_param.groupParam.push(group);
            }
            else {
                load_page_param.groupParam[0] = group;
            }
        }
        console.log(load_page_param.groupParam)
    }*/




    $('input[type=file]').bootstrapFileInput();

    $("#town_table_list").append(function () {
        var option_type = '';
        $.each(download_scope.towns_ate, function (i, obj) {
            option_type += option_class(obj.ateId + "-datalist-ate", obj.ateName, "town_table_list");
        })
        return option_type;
    })

    $("#type-value").append(function () {
        var option_type = option(-1, -1, "");
        $.each(download_scope.type_value, function (i, obj) {
            option_type += option(i, obj.analyticCode, obj.codeShortName);
        })
        return option_type;
    });

    $("#valuta").append(function () {
        var option_valuta = option(-1, -1, "");
        $.each(download_scope.valuta, function (i, obj) {
            option_valuta += option(i, obj.analyticCode, obj.codeShortName);
        })
        return option_valuta;
    });

    $("#org-name").append(function () {
        var option_org_name = option(-1, -1, "");
        $.each(download_scope.name_org, function (i, obj) {
            option_org_name += option(i, obj.analyticCode, obj.codeName);
        })
        return option_org_name;
    });

    $("#this-org-name").append(function () {
        var option_org_name_this = option(-1, -1, "");
        $.each(download_scope.name_org_this, function (i, obj) {
            option_org_name_this += option(i, obj.analyticCode, obj.codeName);
        })
        return option_org_name_this;
    });

    $("#org_table").append(function () {
        var option_org_name = option(-1, -1, "");
        $.each(download_scope.name_org, function (i, obj) {
            option_org_name += option(i, obj.analyticCode, obj.codeName);
        })
        return option_org_name;
    });


    function setFilters() {
        $("#type_cost_table").append(function () {
            var option_type = option(-1, -1, "");
            $.each(download_scope.type_value, function (i, obj) {
                option_type += option(obj.analyticCode, obj.codeShortName, obj.codeShortName);
            })
            return option_type;
        });

        $("#name_1_table").append(function () {
            var option_org_name_this = option(-1, -1, "");
            $.each(download_scope.name_org_this, function (i, obj) {
                option_org_name_this += option(obj.analyticCode, obj.codeName, obj.codeName);
            })
            return option_org_name_this;
        });

        $("#fun_val_table").append(function () {
            var option_fun = option(-1, -1, "");
            $.each(download_scope.searchLoad_fun, function (i, obj) {
                option_fun += option(obj.analyticCode, obj.codeName, obj.codeName);
            })
            return option_fun;
        })
    }

    $("#type-value").change(function () {
        var DELETE_INFO = true;
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changeType();
            } else {
                $("#type-value").val(json_send.costTypeId);
            }
        } else {
            changeType();
        }
    });

    $("#polygon").change(function () {
        var DELETE_INFO = true;
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changePolygon();
            } else {
                $("#polygon").val(arr_id.polyType);
            }
        } else {
            changePolygon();
        }
    });

    $("#point").change(function () {
        var DELETE_INFO = true;
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changePoint();
            } else {
                if (arr_id.polyType == 10) {
                    $("#point").attr("checked", "checked")
                    $("#polygon").removeAttr("checked")
                } else if (arr_id.polyType == 20) {
                    $("#polygon").attr("checked", "checked")
                    $("#point").removeAttr("checked")
                } else {
                    $("#polygon").removeAttr("checked")
                    $("#point").removeAttr("checked")
                }
            }
        } else {
            changePoint();
        }
    });

    $("#meth-value").change(function () {
        var DELETE_INFO = true;
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changeMeth();
            } else {
                $("#meth-value").val(json_send.methosTypeId);
            }
        } else {
            changeMeth();
        }
    });

    $("#terr-value").change(function () {
        var DELETE_INFO = true;
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changeTerr();
            } else {
                $("#terr-value").val(arr_id.terr_val);
            }
        } else {
            changeTerr();
        }
    });

    $("#area").change(function () {
        var DELETE_INFO = true;
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changeArea();
            } else {
                $("#area").val(arr_id.arrea);
            }
        } else {
            changeArea();
        }
    });

    $("#town").change(function () {
        var DELETE_INFO = true;
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changeTown();
            } else {
                $("#town").val(arr_id.town);
            }
        } else {
            changeTown();
        }
    })

    $("#func-value").change(function () {
        var DELETE_INFO = true;
        json_send.funcCodeId = parseInt($("#func-value").val());
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changeFun();
            } else {
                $("#func-value").val(json_send.funcCodeId);
            }
        } else {
            changeFun();
        }
    });

    $("#valuta").change(function () {
        var DELETE_INFO = true;
        json_send.valuta = parseInt($("#valuta").val());
        if ((docID.length != 0) || (zipID.length != 0)) {
            DELETE_INFO = confirm(DELETE_INFO_STR);
            if (DELETE_INFO) {
                cleaning();
                changeFun();
            } else {
                $("#valuta").val(json_send.valuta);
            }
        } else {
            changeFun();
        }
    })

    $("#date-value").change(function () {
        var date_valid_1 = new Date(2010, 07 - 1, 01)
        var date_valid_2 = new Date(2015, 06 - 1, 30)

        var date_user = ($("#date-value").val()).split('-');
        var date = new Date(parseInt(date_user[0]), parseInt(date_user[1]) - 1, parseInt(date_user[2]))
        var date_now = new Date();


        var no_valid_date = true;

        if (date > date_now) {
            no_valid_date = true;
            alert("Дата оценки не может быть позже текущей.")

        } else if (json_send.methosTypeId == 101 || json_send.methosTypeId == 102) {
            if (date <= date_valid_2) {
                no_valid_date = false;
            } else {
                alert("Для «Методика 2015» или «Методика оценки сельскохозяйственных земель» дата оценки должна быть раньше 30.06.2015")
            }
        } else if (json_send.methosTypeId == 103 || json_send.methosTypeId == 201) {
            if (date >= date_valid_1) {
                no_valid_date = false;
            } else {
                alert("Дата оценки должна быть позже 01.07.2010")
            }
        }

        if (no_valid_date) {
            $("#date-value").val('');
        } else {
            json_send.dateVal = $("#date-value").val();
            if (json_send.dateVal != undefined && json_send.dateVal.trim() != '' && json_send.funcCodeId != -1 && json_send.funcCodeId != undefined && json_send.valuta != undefined && json_send.valuta != -1) {
                removeTab()
                createTableDialog_2(download_scope, arr_id);
                createListenerDialog_2(download_scope, json_send);
                getDwd();
                $("#next-load-1").removeAttr("disabled");
            } else {
                $("#next-load-1").attr("disabled", "disabled");
                removeTab()
            }
        }
    })

    $("#org-name").change(function () {
        json_send.valOrgId = $("#org-name").val();
        if (isNext_2(json_send)) {
            $("#next-load-2").removeAttr("disabled");
        } else {
            $("#next-load-2").attr("disabled", "disabled");
        }
    });

    $("#people").keyup(function () {
        var pp = $("#people").val();
        if (isNaN(parseInt(pp))) {
            alert("Вводите только цифры")
            $("#people").val('');
        } else if (parseInt(pp) < 0) {
            alert("Не вводите отрицательные значения")
            $("#people").val('');
        }
    });

    $("#this-org-name").change(function () {
        json_send.decOrgId = $("#this-org-name").val();
        if (isNext_2(json_send)) {
            $("#next-load-2").removeAttr("disabled");
        } else {
            $("#next-load-2").attr("disabled", "disabled");
        }
    });

    $("#date-reg").change(function () {
        var date_valid_1 = new Date(2015, 07 - 1, 01)
        var date_valid_2 = new Date(2015, 06 - 1, 30)

        var date_user = ($("#date-reg").val()).split('-');
        var date = new Date(parseInt(date_user[0]), parseInt(date_user[1]) - 1, parseInt(date_user[2]))
        var date_now = new Date();


        var no_valid_date = true;

        if (date > date_now) {
            no_valid_date = true;
            alert("Дата решения не может быть позже текущей.")

        } else if (json_send.methosTypeId == 101 || json_send.methosTypeId == 102) {
            if (date <= date_valid_2) {
                no_valid_date = false;
            } else {
                alert("Для «Методика 2015» или «Методика оценки сельскохозяйственных земель» дата решения должна быть раньше 30.06.2015")
            }
        } else if (json_send.methosTypeId == 103 || json_send.methosTypeId == 201) {
            if (date >= date_valid_1) {
                no_valid_date = false;
            } else {
                alert("Дата решения должна быть позже 01.07.2015")
            }
        }

        if (no_valid_date) {
            $("#date-reg").val('');
        } else {
            json_send.decDate = $("#date-reg").val();
            if (isNext_2(json_send)) {
                $("#next-load-2").removeAttr("disabled");
            } else {
                $("#next-load-2").attr("disabled", "disabled");
            }
        }
    });

    $("#num-value").change(function () {
        json_send.decNum = $("#num-value").val();
        if (isNext_2(json_send)) {
            $("#next-load-2").removeAttr("disabled");
        } else {
            $("#next-load-2").attr("disabled", "disabled");
        }
    });

    $("#zone-btn").append(function () {
        var zone =
            plusFileDialog_3("add-zone", "lable-zone") +
            removeDialog_3("del-zone");
        return zone;
    })

    $("#bound-btn").append(function () {
        var bound =
            plusFileDialog_3("add-bound", "lable-bound") +
            removeDialog_3("del-bound");
        return bound;
    });

    $("#thems-btn").append(function () {
        var thems =
            plusFileDialog_3("add-thems", "lable-thems") +
            removeDialog_3("del-thems");
        return thems;
    });

    document.getElementById('add-zone').addEventListener('change', onFilesDialog_3, false);
    document.getElementById('add-bound').addEventListener('change', onFilesDialog_3, false);
    document.getElementById('add-thems').addEventListener('change', onFilesDialog_3, false);


    document.getElementById('del-zone').addEventListener('click', delFilesDialog_3, false);
    document.getElementById('del-bound').addEventListener('click', delFilesDialog_3, false);
    document.getElementById('del-thems').addEventListener('click', delFilesDialog_3, false);

    listenerFilt();

    function listenerFilt() {
        document.getElementById('date_cost_table').addEventListener('change', onChangeDateTable, false);
        document.getElementById('date_cost_table').addEventListener('click', onClickDateTable, false);
        document.getElementById('town_table').addEventListener('input', function () {
            var val = this.value;//название!
            var opt = $('option[value="'+ val +'"]');
            var id = opt.prop("id");
            console.log(val);
            if (opt.valueOf().length > 0) {
                var ids = id.split("-");
                console.log(ids[0]);
                load_page_param.ateId = ids[0];
                load_page_param.page = 1;
                reload_filt();
            }
            if (val.valueOf().length == 0){
                load_page_param.ateId = "";
                load_page_param.page = 1;
                reload_filt();
            }
            console.log(load_page_param)
        });
    }

    $("#add-zone").click(function () {
        this.value = null;
    })

    $("#add-bound").click(function () {
        this.value = null;
    })

    $("#add-thems").click(function () {
        this.value = null;
    })

    $("#type_cost_table").change(function () {
        var id = $(this).children(":selected").prop("id");
        var val = this.value;
        console.log(val);
        console.log(id);
        if (val == -1) {
            load_page_param.costTypeId = "";
            load_page_param.page = 1;
            reload_filt();
        } else {
            load_page_param.costTypeId = id;
            load_page_param.page = 1;
            reload_filt();
        }
        console.log(load_page_param)
    })

        $("#fun_val_table").change(function () {
            var id = $(this).children(":selected").prop("id");
            var val = this.value;
            console.log(val);
            console.log(id);
            if (val == -1) {
                load_page_param.funcCodeId = ""
                load_page_param.page = 1;
                reload_filt();
            } else {
                load_page_param.funcCodeId = id;
                load_page_param.page = 1;
                reload_filt();
            }
            console.log(load_page_param)
        })

    $("#name_1_table").change(function () {
        var val = this.value;//Название
        if (val.length > 0) {
            load_page_param.filtr.splice(load_page_param.filtr.indexOf('9'), 1)
            var f = {9: val}
            load_page_param.filtr.push(f);

            reload_filt();
        }
        console.log(load_page_param)
    })

    $("#actual_table").change(function () {
        var val = this.value;//!!! true/false
        console.log(val);
        if (val.length > 0) {
            if (val.valueOf() == "true".valueOf()) {
                load_page_param.actual = 1;
                load_page_param.page = 1;
            }
            if (val.valueOf() == "false".valueOf()) {
                load_page_param.actual = 0;
                load_page_param.page = 1;
            }
            reload_filt();
        } else {
            load_page_param.actual = "";
            load_page_param.page = 1;
            reload_filt();
        }
        console.log(load_page_param)
    })

    $("#type_cost_table").click(function () {
        //this.value = null;
    })

    $("#name_1_table").click(function () {
        this.value = null;
    })

    $("#actual_table").click(function () {
        //this.value = null;
    })

    $("#ok-3").click(function () {
        if (json_send.polyTypeId == -1) {
            json_send.polyTypeId = 20;
        }

        for (var i = 0; i < zipID.length; i++) {
            docID.push(zipID[i])
        }

        var bound = null;
        if (isBounds()) {
            bound = JSON.parse(json_send.boundList);
        }

        var dataObj = {
            userId: AUTHOBJ.userId,
            costTypeId: json_send.costTypeId,
            methosTypeId: json_send.methosTypeId,
            objectNumber: null,
            currencyId: json_send.valuta,
            parentObjectNumber: arr_id.objectnumber,
            population: $("#people").val(),
            funcCodeId: json_send.funcCodeId,
            dateVal: (json_send.dateVal), //дата оценки
            remark: $("#note").val(),
            dateRegStart: ($("#now-date").val()),
            polyTypeId: json_send.polyTypeId,
            valOrgId: json_send.valOrgId,
            decOrgId: json_send.decOrgId,
            decNum: json_send.decNum,
            decDate: (json_send.decDate), // дата решения
            dwdId: json_send.dwd,
            docList: docID,
            zoneList: JSON.parse(json_send.zoneList),
            //zoneList:null,
            boundList: bound,
            //boundList:json_send.boundList,
            layerList: layerJSONCODE
        };

        var data = JSON.stringify(dataObj);

        /*console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!###########################################")
         console.log(dataObj);
         console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!##########################################")*/

        httpServices.saveObject(data);

    });

    $("#str-table").change(function () {
        load_page_param.amount = $("#str-table").val();
        load_page_param.page = 1
        ;
        reload_filt();
        listenerFilt();
    });


    $("#1-sort").click(function () {
        sort_event(1)
    });

    $("#2-sort").click(function () {
        sort_event(2)
    });

    $("#3-sort").click(function () {
        sort_event(3)
    });

    $("#4-sort").click(function () {
        sort_event(4)
    });

    $("#5-sort").click(function () {
        sort_event(5)
    });

    $("#6-sort").click(function () {
        sort_event(6)
    });

    $("#7-sort").click(function () {
        sort_event(7)
    });

    $("#8-sort").click(function () {
        sort_event(8)
    });

    $("#9-sort").click(function () {
        sort_event(9)
    });

    $("#from-table").change(function () {
        var date = $("#from-table").val();
        var dates = date.split('-');
        console.log(date);
        if (date != '') {
            load_page_param.dateFrom = dates[2] + "." + dates[1] + "." + dates[0];
            console.log(load_page_param.dateFrom);
            reload_filt();
            listenerFilt();
        }
        if (date == '') {
            load_page_param.dateFrom = "";
            reload_filt();
            listenerFilt();
        }
         // reloadBase();
         // listenerFilt();
    })

    $("#to-table").change(function () {
        var date = $("#to-table").val();
        load_page_param.dateTo = date;
        var dates = date.split('-');
        console.log(date);
        if (date != '') {
            load_page_param.dateTo = dates[2] + "." + dates[1] + "." + dates[0];
            console.log(load_page_param.dateTo);
            reload_filt();
            listenerFilt()
        }
        if (date == '') {
            load_page_param.dateTo = "";
            reload_filt();
            listenerFilt();
        }
        // reloadBase();
        // listenerFilt();
    })

    function onChangeDateTable() {
        //var i = load_page_param.filtr.indexOf("3");
        var val3 = $("#date_cost_table").val();
        if (val3.length > 0) {
            load_page_param.filtr.splice(load_page_param.filtr.indexOf('3'), 1)
            var f = {3: val3}
            load_page_param.filtr.push(f);

            reload_filt();
            $("#date_cost_table").val(val3)
        }
        console.log(load_page_param.filtr)
    }

    function onClickDateTable() {
        this.value = null;
    }

    $("#filtr-reset").click(function () {
        $("#from-table").val("");
        $("#to-table").val("");
        $("#date_cost_table").val("");
        $("#town_table").val("");
        $("#type_cost_table").val("");
        $("#name_1_table").val("");
        $("#actual_table").val("");
        $("#fun_val_table").val("");
        load_page_param.dateFrom = '';
        load_page_param.dateTo = '';
        load_page_param.ateId = '';
        load_page_param.costTypeId = '';
        load_page_param.funcCodeId = '';
        load_page_param.actual = '';
        reload_filt();
        listenerFilt();
    })

    function sort_event(num) {
        if (load_page_param.sortingParam != num) {
            var id_prev = "#" + load_page_param.sortingParam + "-sort";
            $(id_prev).addClass("glyphicon-sort")
            $(id_prev).removeClass("glyphicon-chevron-up")
            $(id_prev).removeClass("glyphicon-chevron-down")
        }
        var id = "#" + num + "-sort";
        if ($(id).hasClass("glyphicon-sort")) {
            $(id).removeClass("glyphicon-sort");
            $(id).addClass("glyphicon-chevron-up");
            sort("asc", num)
        } else if ($(id).hasClass("glyphicon-chevron-up")) {
            $(id).removeClass("glyphicon-chevron-up");
            $(id).addClass("glyphicon-chevron-down");
            sort("desc", num)
        } else if ($(id).hasClass("glyphicon-chevron-down")) {
            $(id).removeClass("glyphicon-chevron-down");
            $(id).addClass("glyphicon-chevron-up");
            sort("asc", num)
        }
    }

    function sort(type, column) {
        load_page_param.sortingParam = column;
        load_page_param.predicate = type;
        reload_filt();
    }

    function reload_filt() {
        download_scope.bookLoad = null;
        httpServices.getBookLoad_2(download_scope, load_page_param);
        appendBodyZurnal(download_scope, [1, 2, 3, 4, 5, 6, 12])

        getPagination(download_scope);
        for (var i = 1; i <= download_scope.colPage; i++) {
            var id = i + "-table";
            document.getElementById(id).addEventListener('click', paginationEventFunction, false);
        }
        document.getElementById('prev-str-table').addEventListener('click', paginationEventFunction, false);
        document.getElementById('next-str-table').addEventListener('click', paginationEventFunction, false);

        $.each(download_scope.bookLoad, function (i, obj) {
            /*document.getElementById(obj.regNum).addEventListener('dblclick', funDbClick, false);*/
            document.getElementById(obj.regNum+"_"+obj.funcCodeId).addEventListener('click', funClick, false);
        });
    }

    function reload() {
/*        load_page_param.filtr = []
        load_page_param.columnNums = [1, 2, 3, 4, 5, 7, 8, 9, 12];
        load_page_param.columns = [];*/

        reloadBase();
    }

    function reloadBase() {
        createTableBookLoad(download_scope, load_page_param);
        getPagination(download_scope);
        for (var i = 1; i <= download_scope.colPage; i++) {
            var id = i + "-table";
            document.getElementById(id).addEventListener('click', paginationEventFunction, false);
        }
        document.getElementById('prev-str-table').addEventListener('click', paginationEventFunction, false);
        document.getElementById('next-str-table').addEventListener('click', paginationEventFunction, false);

        $.each(download_scope.bookLoad, function (i, obj) {
            /*document.getElementById(obj.regNum).addEventListener('dblclick', funDbClick, false);*/
            document.getElementById(obj.regNum+"_"+obj.funcCodeId).addEventListener('click', funClick, false);
        });
    }

    function paginationEventFunction(e) {
        var id = $(this).attr("id").split('-')[0];
        var id_event = "#" + $(this).attr("id");
        var page_prew = parseInt(load_page_param.page);
        var id_prew = "#" + page_prew + "-table";
        var all_page = download_scope.colPage;
        if (id == 'prev') {
            if (page_prew != 1) {
                $(id_prew).removeClass("active");
                load_page_param.page = page_prew - 1;
                reload_filt();
                $("#" + load_page_param.page + "-table").addClass("active");
            }
        } else if (id == 'next') {
            if (page_prew < all_page) {
                $(id_prew).removeClass("active");
                load_page_param.page = page_prew + 1;
                reload_filt();
                $("#" + load_page_param.page + "-table").addClass("active");
            }
        } else {
            $(id_prew).removeClass("active");
            load_page_param.page = parseInt(id);
            reload_filt();
            $(id_event).addClass("active");
        }
        listenerFilt();
    }

/*    $(".mult-check").click(function () {
        console.log("Select click")
        var element = $(".mult-check").parent().parent().parent().parent().prev();
        console.log(element)
        var li = $(".mult-check").parent().parent();
        if (li.hasClass("selected")) {
            $("#group-btn").removeClass("disabled");
            var val = ($(this).val());
            if ($('.selected li') == 'on') {
                set_group(-1);
            } else {
                set_group(val);
            }
        }
        else {
            $("#group-btn").addClass("disabled");
            set_group(0);
        }
    });*/

    $("#group-btn").click(function () {
        reload_filt()
    })


    function changeType() {
        var val = $("#type-value").val();
        json_send.costTypeId = parseInt(val);
        if (val == -1) {
            arr_id.polyType = -1;
            arr_id.meth_val = -1;
            type_change();
        } else {
            arr_id.polyType = -1;
            arr_id.meth_val = -1;
            arr_id.type_val = val;
            httpServices.getMethod(download_scope, arr_id.type_val);
            type_change();
            $("#meth-value").removeAttr("disabled");
            $("#meth-value").append(function () {
                var option_meth = option(-1, -1, "");
                $.each(download_scope.method, function (i, obj) {
                    option_meth += option(i, obj.analyticCode, obj.codeName);
                })
                return option_meth;
            });
            if (val == 20) {
                $("#polygon").removeAttr("disabled");
                $("#point").removeAttr("disabled");
            }
            if (val == 10) {
                $("#people").removeAttr("disabled");
            }
        }
    }

    function changePolygon() {
        arr_id.polyType = $("#polygon").val();
        if (arr_id.meth_val != -1) {
            meth_change();
            httpServices.getTerr(download_scope, arr_id.type_val);
            $("#terr-value").removeAttr("disabled");
            $("#terr-value").append(function () {
                var option_terr = option(-1, -1, "");
                $.each(download_scope.method, function (i, obj) {
                    option_terr += option(obj.ateId, obj.ateId, obj.ateName);
                })
                return option_terr;
            });
        }
    }

    function changePoint() {
        arr_id.polyType = $("#point").val();
        if (arr_id.meth_val != -1) {
            meth_change();
            httpServices.getTerr(download_scope, arr_id.type_val);
            $("#terr-value").removeAttr("disabled");
            $("#terr-value").append(function () {
                var option_terr = option(-1, -1, "");
                $.each(download_scope.method, function (i, obj) {
                    option_terr += option(obj.ateId, obj.ateId, obj.ateName);
                })
                return option_terr;
            });
        }
    }

    function changeArea() {
        var val = $("#area").val();
        json_send.objectNumber = val;
        arr_id.objectnumber = val;
        var parent = $("#area").children(":selected").attr("id");
        if (val == -1) {
            area_change();
        } else {
            if (parent == 400 || arr_id.type_val != 10) {
                area_change();
                forFunType(arr_id, download_scope, val);
            } else {
                area_change();
                arr_id.arrea = val;
                httpServices.getArea(download_scope, arr_id.type_val, arr_id.arrea);
                $("#town").removeAttr("disabled");
                $("#town").append(function () {
                    var option_town = option(-1, -1, "");
                    $.each(download_scope.method, function (i, obj) {
                        option_town += option(obj.category.parentCode, obj.ateId, obj.ateName);
                    })
                    return option_town;
                });
            }
        }
    }

    function changeTerr() {
        var val = $("#terr-value").val();
        arr_id.objectnumber = parseInt(val);
        json_send.objectNumber = parseInt(val);
        var parent = $("#terr-value").children(":selected").attr("id");
        if (val == -1) {
            terr_change();
        } else {
            if (parent == 400) {
                terr_change();
                forFunType(arr_id, download_scope, val);
            } else {
                terr_change();
                arr_id.terr_val = val;
                httpServices.getArea(download_scope, arr_id.type_val, arr_id.terr_val);
                $("#area").removeAttr("disabled");
                $("#area").append(function () {
                    var option_area = option(-1, -1, "");
                    $.each(download_scope.method, function (i, obj) {
                        option_area += option(obj.ateId, obj.ateId, obj.ateName); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    })
                    return option_area;
                });
            }
        }
    }

    function changeTown() {
        var val = $("#town").val();
        json_send.objectNumber = val;
        arr_id.objectnumber = val;
        arr_id.town = val;
        if (val == -1) {
            town_change();
        } else {
            forFunType(arr_id, download_scope, val);
        }
    }

    function changeMeth() {
        var val = $("#meth-value").val();
        json_send.methosTypeId = val;
        if (val == -1) {
            arr_id.meth_val = -1;
            meth_change();
            $("#date-value").attr("disabled", "disabled");
        } else {
            meth_change();
            arr_id.meth_val = val;
            $("#date-value").removeAttr("disabled");
            if ((arr_id.type_val == 20 && arr_id.polyType != -1) || (arr_id.type_val != 20 && arr_id.polyType == -1)) {
                httpServices.getTerr(download_scope, arr_id.type_val);
                $("#terr-value").removeAttr("disabled");
                $("#terr-value").append(function () {
                    var option_terr = option(-1, -1, "");
                    $.each(download_scope.method, function (i, obj) {
                        option_terr += option(obj.ateId, obj.ateId, obj.ateName);
                    })
                    return option_terr;
                });
            }
        }
    }

    function changeFun() {
        console.log("changeFun()")
        if (json_send.funcCodeId != -1 && json_send.funcCodeId != undefined && json_send.dateVal != undefined && json_send.dateVal.trim() != '' && json_send.valuta != undefined && json_send.valuta != -1) {
            removeTab()
            createTableDialog_2(download_scope, arr_id);
            createListenerDialog_2(download_scope, json_send);
            getDwd();
            $("#next-load-1").removeAttr("disabled");
        }
        else {
            $("#next-load-1").attr("disabled", "disabled");
            removeTab()
        }
    }
}
});

$(document).on('click', '.remove_new', function(e){
    e.preventDefault();
    var id_arr = $(this).children().next().next().attr("id").split('-');
    onFilesDelete(id_arr[0] + "-" + id_arr[1])
    plus_del(id_arr[0] + "-" + id_arr[1], false)
});

function get_json(){
    return json_send;
}


function cleanDialog_1(){
    type_change();
}

function cleanDialog_2(){
    $("#org-name").val('-1');
    $("#this-org-name").val('-1');
    $("#date-reg").val('');
    $("#num-value").val('');

    for(var i=0; i<docID.length; i++){
        console.log(docID[i])
        httpServices.deleteDoc_byID(docID[i]);
        IS_DELETE=false;
    }
    docID=[];
}

function cleanDialog_3(){
    console.log(zipID)
    if(zipID.length != 0) {
        for (var i = 0; i < zipID.length; i++) {
            console.log(zipID[i])
            httpServices.deleteDoc_byID(zipID[i]);
            IS_DELETE = false;
        }
        removeTabStr("zone");
        removeTabStr("bound");
        removeTabStr("thems");
        zipID = [];
    }
}

function  cleanAll(){
    DELETE_ALL_DIALOG = confirm("Вы действительно хотите закрыть окно? Введенные данные будут потеряны");
    if(DELETE_ALL_DIALOG) {
        cleanDialog_1();
        $("#type-value").val(-1)
        cleaning();
    }
}

function cleaning(){
    cleanDialog_2();
    cleanDialog_3();
    plus_3();
    disable_next();
}

function plus_3(){
    $("#del-zone").parent().css("display", "none");
    $("#add-zone").parent().css("display", "");
    $("#del-bound").parent().css("display", "none");
    $("#add-bound").parent().css("display", "");
    $("#del-thems").parent().css("display", "none");
    $("#add-thems").parent().css("display", "");
}

function disable_next() {
    $("#next-load-1").attr("disabled", "disabled");
    $("#next-load-2").attr("disabled", "disabled");
}