/**
 * Created by Shcherbina on 08.11.2016.
 */

$(document).ready(function () {

    httpServices.getTORPrice(price_scope, AUTHOBJ.userId);

    setStartParam();

    httpServices.getPrices(price_scope, price_load_param.sorting, price_load_param.predicate, price_load_param.page, price_load_param.tor,price_load_param.amount, AUTHOBJ.userId);

    getPricesTable();
    getPreiskurantPagination();
    setListenersPreiskyrantTable();


    $("#select-preiskyrant").append(function(){
        var opt = option(-1, -1, "");
        $.each(price_scope.tors, function (i, obj) {
            opt += option(i, obj.analyticCode, obj.codeName);
        })
        console.log(AUTHOBJ)
        return opt;
    });

    $("#select-preiskyrant").change(function(){
        var val = $("#select-preiskyrant").val();
        if(val==-1){
            val='';
        }
        price_load_param.tor = val;

        httpServices.getPrices(price_scope, price_load_param.sorting, price_load_param.predicate, price_load_param.page, price_load_param.tor,price_load_param.amount, AUTHOBJ.userId);

        reload_table_fun();

        /*getPricesTable();
        setListenersPreiskyrantTable();*/
    })

    $("#close-preiskyrant").click(function(){
        if(ID_VIEW_PREISKYRANT != '' && ID_VIEW_PREISKYRANT != undefined){
            httpServices.editReadNewPrices(price_scope, ID_VIEW_PREISKYRANT, AUTHOBJ.userId);

            var price = price_scope.price;
            if(price.type == -1){
                alert("Невозможно деактуализировать прейскурант")
            }else if(price.type == 0){ // смотреть можно, редактировать нельзя
                alert("Невозможно деактуализировать прейскурант")
            }else if(price.type == 1){ // смотреть можно, редактировать можно
                clearParamsStopPreiskyrant();
                setParamsStopPreiskyrant();
                $("#save-close-preiskyrant").removeAttr("disabled");
                $("#preiscurant-new-modal-2").modal('show');
            } else if(price.type == 3){ // создать новый
                clearParamsStopPreiskyrant();
                setParamsStopPreiskyrant();
                $("#save-close-preiskyrant").removeAttr("disabled");
                $("#preiscurant-new-modal-2").modal('show');
            }

        }else {
            alert("Выберите прейскурант из таблицы")
        }
    })

    $("#view-preiskyrant").click(function(){
        var priceId = ID_VIEW_PREISKYRANT;
        if(ID_VIEW_PREISKYRANT == '' || ID_VIEW_PREISKYRANT == undefined){
            priceId = -1;
        }
        httpServices.editReadNewPrices(price_scope, priceId, AUTHOBJ.userId);
        var price = price_scope.price;
        if(price.type == -1){
            alert("Невозможно создать прейскурант, так как не прекращён текущий")
        }else if(price.type == 0){ // смотреть можно, редактировать нельзя
            $("#save-new-preiskyrant").attr("disabled","disabled");
            clearPriceModal();
            createNewTablePrice();
            $("#preiscurant-new-modal-1").modal('show');
        }else if(price.type == 1){ // смотреть можно, редактировать можно
            clearPriceModal();
            if(price_scope.price.stop_date.length != 0){
                createNewTablePrice();
            }else {
                $("#save-new-preiskyrant").removeAttr("disabled");
                createNewTablePriceRedact(false);
            }
            $("#preiscurant-new-modal-1").modal('show');
        } else if(price.type == 3){ // создать новый
            clearPriceModal();
            $("#save-new-preiskyrant").removeAttr("disabled");
            createNewTablePriceRedact(true);
            $("#preiscurant-new-modal-1").modal('show');
        }
    })

    $("#type-preiskurant-stop").append(function(){
        var types = "";
        httpServices.getTypesStop(price_scope);
        $.each(price_scope.typePriceStop,  function(i, obj){
            types += option(obj.analyticCode, obj.analyticCode, obj.codeName);
        })
        return types;
    })

    $("#save-close-preiskyrant").click(function() {
        var userId = AUTHOBJ.userId,
            priceId = ID_VIEW_PREISKYRANT,
            dateD = $("#dat-preiskurant-stop").val(),
            operId =$("#type-preiskurant-stop").val();
        httpServices.stopPrice(price_scope, userId, priceId, dateD, operId);
        price_scope.des_price = true;
    })

    $("#dat-preiskurant-stop").change(function(){
        if($("#dat-preiskurant-stop").val() == ""){
            $("#save-close-preiskyrant").attr("disabled","disabled");
        }else{
            $("#save-close-preiskyrant").removeAttr("disabled");
        }
    })

    $("#dat-new-preiskyrant").change(function(){
        var val = $("#dat-new-preiskyrant").val(),
            start = price_scope.price.start_date;
        if(val<start){
            alert("Дата должна быть больше или равна " + start);
            $("#dat-new-preiskyrant").val(start);
        }
    })

    $("#str-table-preiskyrant").change(function () {
        var val = $("#str-table-preiskyrant").val()
        price_load_param.amount = val;
        price_load_param.page = 1;
        reload_table_default();
    })


    $("#save-new-preiskyrant").click(function (){
        var isSave = confirm("Сохранить изменения, если они были внесены?");
        if(isSave){
            var savePrice = {
                number_PRICE:"",
                start_DATE:"",
                stop_DATE:"",
                content:"",
                exec : AUTHOBJ.userId,
                previd : ""
            };
            savePrice.number_PRICE = $("#num-new-preiskyrant").val();
            savePrice.start_DATE = $("#dat-new-preiskyrant").val();
            if(price_scope.price.type == 3){
                ID_VIEW_PREISKYRANT = -1;
            }

            savePrice.previd=ID_VIEW_PREISKYRANT;
            var table_size = $("#body-price-modal").children().length,
                content = "";
            for (var i =0; i<table_size; i++) {
                var id = "#"+i+"-price",
                    arr_id=$(id).attr("class").split("-"),
                    urg = $("#"+i+"-urg").val(),
                    nonurg = $("#"+i+"-nonurg").val();
                content +=(arr_id[1]+","+arr_id[0]+"," + urg +  "," +nonurg  + ";");
            }
            savePrice.content = content;
            httpServices.addEditPrice(price_scope, savePrice);

            reload_table_fun();
            price_scope.btn_save = true;
        }
    });

    $("#1-price-sort").click(function(){
        setSort(1);
    })

    $("#2-price-sort").click(function(){
        setSort(2);
    })

    $("#3-price-sort").click(function(){
        setSort(3);
    })
});

function setSort(sortParam){
    price_load_param.sorting = sortParam;
    if(price_load_param.predicate == 'desc'){
        price_load_param.predicate = 'asc'
    }else{
        price_load_param.predicate = 'desc'
    }
    reload_table_default();
}
