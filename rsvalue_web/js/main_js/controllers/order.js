/**
 * Created by Shcherbina on 21.12.2016.
 */

var addres_order = [region = "", typeIn = "", typeOut = "", area = "", ss = "", snp = "", bjd = "", road = "", km = "", street = ""];

var now = (new Date().toLocaleDateString()).split(".");
var now_date = now[2] + "-" + now[1] + "-" + now[0];

function addres_orderInit() {
    addres_order.region = "";
    addres_order.typeIn = "";
    addres_order.typeOut = "";
    addres_order.area = "";
    addres_order.ss = "";
    addres_order.snp = "";
    addres_order.bjd = "";
    addres_order.road = "";
    addres_order.km = "";
    addres_order.street = "";
    addres_order.numDom = "";
}

$(document).ready(function () {

    init_func_nazn();

    $("#date-order-search").val(now_date);

    order_scope.date_search = $("#date-order-search").val();

    $("#main-seach-1-1").removeClass("disabled-div");

    $("#date-order-search").change(function () {
        var d = $("#date-order-search").val();
        if (d.length != 0) {
            if (dateNotNow(d)) {
                order_scope.date_search = d;
            } else {
                $("#date-order-search").val(now_date);
            }
        }
    })

    $("#1-search-by-addr-abl-select-order").change(function () {
        change_1_data_order();
        changeSearchByAddrObl_order();
        addres_orderInit();
        var ateId = ($("#1-search-by-addr-abl-select-order").val());
        if (ateId != -1 && ateId != null && ateId != undefined) {
            addres_order.region = ateId;
            order_scope.selectedData_1 = ateId;
            httpServices.getAddressElement(order_scope, 1, ateId);
            $("#2-select-search-order").append(function () {
                var select = "";
                $.each(order_scope.searchAddr_El, function (i) {
                    select += option(i, order_scope.searchAddr_El[i][0], order_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            $("#search-by-addr-2-oder").removeClass("display-none")
            append_2input_order();
        }
    });

    // Выбор на этапе 2 из 6ти
    $("#2-select-search-order").change(function () {
        append_2input()
    });

    $("#2-select-search-data-order").change(function () {
        change_2_data();
        var val = $("#2-select-search-data-order").val();
        if (val != -1 && order_scope.searchByAddr_2 == 9) {
            $("#search-by-addr-btn").removeAttr("disabled");
        } else {
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if (val != -1 && order_scope.searchByAddr_2 != 9) {
            order_scope.selectedData_2 = val;

            /*            addres.area='';
             addres.ss='';
             addres.snp='';
             addres.bjd='';
             addres.road='';
             addres.km='';
             addres.street='';*/

            if (addres_order.typeOut == 2) {
                addres_order.area = val;
            } else if (addres_order.typeOut == 4) {
                addres_order.snp = val;
            }
            httpServices.getAddressElement(order_scope, order_scope.searchByAddr_2, val);
            $("#3-select-search-order").append(function () {
                var select = "";
                $.each(order_scope.searchAddr_El, function (i) {
                    select += option(i, order_scope.searchAddr_El[i][0], order_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_3input();
            $("#search-by-addr-3-order").removeClass("display-none")
        } else {
            order_scope.searchByAddr_numDom = val;
        }
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Выбор на этапе 3 из 6ти
    $("#3-select-search-order").change(function () {
        append_3input();
    });


    $("#3-select-search-data-order").change(function () {
        change_3_data();
        var val = $("#3-select-search-data-order").val();
        if (val != -1 && order_scope.searchByAddr_3 == 9) {
            $("#search-by-addr-btn").removeAttr("disabled");
        } else {
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if (val != -1 && order_scope.searchByAddr_3 != 9) {

            order_scope.selectedData_3 = val;
            setAddresbyTypeOut(val, addres_order.typeOut);

            httpServices.getAddressElement(order_scope, order_scope.searchByAddr_3, val);
            $("#4-select-search").append(function () {
                var select = "";
                $.each(order_scope.searchAddr_El, function (i) {
                    select += option(i, order_scope.searchAddr_El[i][0], order_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_4input();
            $("#search-by-addr-4-order").removeClass("display-none")
        } else {
            order_scope.searchByAddr_numDom = val;
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //EXAMPLE!!!
    // Выбор на этапе 4 из 6ти
    $("#4-select-search").change(function () {
        append_4input();
    });


    $("#4-select-search-data-order").change(function () {
        change_4_data();
        var val = $("#4-select-search-data").val();
        if (val != -1 && order_scope.searchByAddr_4 == 9) {
            $("#search-by-addr-btn").removeAttr("disabled");
        } else {
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if (val != -1 && order_scope.searchByAddr_4 != 9) {
            order_scope.selectedData_4 = val;

            setAddresbyTypeOut(val, addres_order.typeOut)

            httpServices.getAddressElement(order_scope, order_scope.searchByAddr_4, val);
            $("#5-select-search").append(function () {
                var select = "";
                $.each(order_scope.searchAddr_El, function (i) {
                    select += option(i, order_scope.searchAddr_El[i][0], order_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_5input();
            $("#search-by-addr-5").removeClass("display-none")
        } else {
            order_scope.searchByAddr_numDom = val;
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Выбор на этапе 5 из 6ти
    $("#5-select-search-order").change(function () {
        append_5input();
    });


    $("#5-select-search-data-order").change(function () {
        change_5_data();
        var val = $("#5-select-search-data").val();
        if (val != -1 && order_scope.searchByAddr_5 == 9) {
            $("#search-by-addr-btn").removeAttr("disabled");
        } else {
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if (val != -1 && order_scope.searchByAddr_5 != 9) {
            order_scope.selectedData_4 = val;
            addres_order.typeOut = $("#5-select-search").val();
            setAddresbyTypeOut(val, addres_order.typeOut)
            httpServices.getaddres_ordersElement(order_scope, order_scope.searchByAddr_5, val);
            $("#6-select-search").empty();
            $("#6-select-search-data").empty();
            $("#6-select-search").append(function () {
                var select = "";
                $.each(order_scope.searchAddr_El, function (i) {
                    select += option(i, order_scope.searchAddr_El[i][0], order_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_6input();
            $("#search-by-addr-6").removeClass("display-none")
        } else {
            order_scope.searchByAddr_numDom = val;
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Выбор на этапе 6 из 6ти

    $("#6-select-search-order").change(function () {
        append_6input();
    });


    $("#6-select-search-data-order").change(function () {
        var val = $("#6-select-search-data").val();
        if (val != -1) {
            $("#search-by-addr-btn").removeAttr("disabled");
        } else {
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if (val != -1) {
            order_scope.searchByAddr_numDom = val;
        }

    })

    $("#search-by-addr-btn-order").click(function () {

        closeAddrSearch();
        httpServices.getAddressResult(order_scope, order_scope.searchByAddr_numDom, order_scope.isNalog, order_scope.date_search);

        var data = order_scope.resultAddrSearch;
        if (data.code == 1) {
            // поиск по кадастровому номеру
            if (data.cadnumList.length == 1) {
                console.log(data.cadnumList)
                console.log(data.cadnumList.length)
                if ($("#res-search-addr-1-1").hasClass("display-none")) {
                    $("#res-search-addr-1-1").removeClass("display-none");
                }
                $("#cad-num-by-addr-1").text("")
                $("#cad-num-by-addr-1").text(data.cadnumList[0])
            } else {
                if ($("#res-search-addr-1-2").hasClass("display-none")) {
                    $("#res-search-addr-1-2").removeClass("display-none");
                }
                $("#cad-num-by-addr-2").empty();
                $("#addr-2-view").attr("disabled", "disabled");
                $("#cad-num-by-addr-2").append(function () {
                    var text = "";
                    for (var i = 0; i < data.cadnumList.length; i++) {
                        var cad = data.cadnumList[i];
                        var soato = Number(cad.substring(0, 10)),
                            blockNum = Number(cad.substring(10, 12)),
                            parcelNum = Number(cad.substring(12, 19));
                        text += "<div class='radio'><label class='radio-inline'><input type='radio' onclick='onCadastrAddr(" + soato + ", " + blockNum + ", " + parcelNum + ")' name='optradio-addr' id='search-by-cadastr-" + i + "'>Земельный часток с кадастровым номером " + cad + "</label></div>";
                    }
                    return text;
                })
                //<div class="radio"><label class="radio-inline"><input type="radio" name="optradio2" checked id="search-by-cadastr-1">По кадастровому номеру</label></div>

                /*for(var i= 0; i<data.cadnumList.length; i++){
                 var id = 'search-by-cadastr-' + i;
                 document.getElementById(id).addEventListener('click', onCadastrAddr, false);
                 }*/
            }
        } else if (data.code == 2) {
            // поиск по адресной точке
            if ($("#res-search-addr-1-3").hasClass("display-none")) {
                $("#res-search-addr-1-3").removeClass("display-none");
            }
            $("#cad-num-by-addr-3").empty();
            $("#cad-num-by-addr-3").append(function () {
                return data.address;
            });

        } else {
            alert(data.response)
        }
    });

    $("#addr-2-view-order").click(function () {
        httpServices.getres_byCadastr(order_scope, cadNum.soato, cadNum.blockNum, cadNum.parceNum, order_scope.isNalog, order_scope.date_search);
        if (CADASTR_RES > 0) {
            closeDialog();
            clearDialogCadNum();
            searchByCadastrModal();
            $("#svedenia-cadastr").removeClass("display-none");
            $("#cadastr-stoim-search").modal("show");
        }
    });

    $("#addr-1-view-order").click(function () {
        var cad = $("#cad-num-by-addr-1").text();

        var soato = Number(cad.substring(0, 10)),
            blockNum = Number(cad.substring(10, 12)),
            parcelNum = Number(cad.substring(12, 19));

        httpServices.getres_byCadastr(order_scope, soato, blockNum, parcelNum, order_scope.isNalog, order_scope.date_search);

        if (CADASTR_RES > 0) {
            closeDialog();
            clearDialogCadNum();
            searchByCadastrModal();
            $("#svedenia-cadastr").removeClass("display-none");
            $("#cadastr-stoim-search").modal("show");
        }
    });

    $("#phys_lico").change(function () {
        clear_ur_div();
        clear_phys_div();
        $("#phys_div").slideToggle();
        $("#ur_div").slideToggle();
        $("#order_notaruis_check").removeProp("checked");
        $("#order_notarius_div").slideUp();
    });

    $("#ur_lico").change(function () {
        clear_ur_div();
        clear_phys_div();
        $("#phys_div").slideToggle();
        $("#ur_div").slideToggle();
        $("#order_notaruis_check").removeProp("checked");
        $("#order_notarius_div").slideUp();
    });

    $("#order_notaruis_check").change(function () {
        if (this.checked) {
            $("#order_notarius_div").slideDown();
        } else {
            $("#order_notarius_div").slideUp();
        }
    });

    $("#order_select_state").append(function () {
        var opt = option(-1, -1, "");
        $.each(price_scope.tors, function (i, obj) {
            opt += option(i, obj.analyticCode, obj.codeName);
        })
        return opt;
    });

    $("#order_new_order").click(function () {
        httpServices.get_new_order_number(AUTHOBJ.userId);
        $("#order_number").val(order_scope.order_number);
        open_order();
    });

    $("#toOrder").click(function () {
        close_order();
        closeAllDialogOrder();
        $("#order-zone-table").empty();
        $("#order-object-table").empty();
        clearAllFieldsOrder();
    });

    $("#cadastr-order").click(function () {
        closeAllOrdersSearch();
        close_cadaster_search_result();
        $("#order-cadastr").removeClass("display-none");
        TYPE_ORDER_SEARCH = 10;
    });

    $("#st-order").click(function () {
        closeAllOrdersSearch();
        close_cadaster_search_result();
        $("#block-cadastr-order").addClass('disabled-div-css');
        $("#search-addr-st-order").removeClass('disabled-div-css')
        if (order_scope.order_objects[0].isNk == 0) {
            $("#order-st").removeClass("display-none");
        } else {
            $("#order-st-notar").removeClass("display-none");
        }
    });

    $("#ch-order").click(function () {
        closeAllOrdersSearch();
        close_cadaster_search_result();
        $("#order-sh").removeClass("display-none");
    });

    $("#addr-order").click(function () {
        closeAllOrdersSearch();
        close_cadaster_search_result();
        $("#search-addr-1-order").removeClass("display-none")
    });

    $("#mestor-order").click(function () {
        closeAllOrdersSearch();
        close_cadaster_search_result();
        $("#order-mesto").removeClass("display-none");
    });

    $("#snp-order").click(function () {
        closeAllOrdersSearch();
        close_cadaster_search_result();
        $("#order-snp").removeClass("display-none");
    });

    $("#cadastr-search-order-btn").click(function () {
        var date = $("#date-order-search").val();
        var normalDate = get_normal_date();
        var soato = $("#soato-order").val();
        var block = $("#numblock-order").val();
        var parcel = $("#numuch-order").val();
        clear_cadnum_search_result();
        httpServices.carastar_value_order_search(soato, block, parcel, normalDate);
        init_cadnum_order_result();
        if (order_scope.order_process_search == null) {
            hide_cadnum_order_result();
        } else {
            show_cadnum_order_result();
        }
    });

    $("#order_save").click(function () {
        if (check_required_fields()) {
            if (order_scope.editable_order != null) {
                if (confirm("Изменить данные заказа?")) {
                    httpServices.change_order_state(order_scope.editable_order.orderId, 5, AUTHOBJ.userId);
                    init_order_data();
                    httpServices.add_new_order(order_data);
                }
            } else {
                init_order_data();
                httpServices.add_new_order(order_data);
            }
        } else {
            alert("Заполните все необходимые поля");
        }
    });

    $("#order_select_state").change(function () {
        order_filter.page = 1;
        order_filter.orgId = $(this).children(":selected").prop("value");
        if (order_filter.orgId == -1) {
            order_filter.orgId = "";
        }
        load_tor_users(order_filter.orgId);
        reload_order_table();
    });

    $("#order_table_str").change(function () {
        order_filter.qty = parseInt($(this).val());
        order_filter.page = 1;
        $("#order_edit_order").prop('disabled', 'disabled');
        $("#order_process_order").prop('disabled', 'disabled');
        current_order_id = "";
        reload_order_table();
    });

    $("#order_urgency_filter").change(function () {
        order_filter.page = 1;
        var id = $(this).children(":selected").prop("id").split("_")[2];
        if (id == 0) {
            order_filter.urgency = "";
        } else {
            order_filter.urgency = parseInt(id);
        }
        reset_order_edit_process_buttons();
        reload_order_table();
    });

    $("#order_nb_filter").change(function () {
        order_filter.page = 1;
        var id = $(this).children(":selected").prop("id").split("_")[2];
        if (id == 0) {
            order_filter.isNb = "";
        } else {
            order_filter.isNb = parseInt(id);
        }
        reset_order_edit_process_buttons();
        reload_order_table();
    });

    $("#order_state_filter").change(function () {
        order_filter.page = 1;
        var id = $(this).children(":selected").prop("id").split("_")[2];
        if (id == 0) {
            order_filter.orderState = "";
        } else {
            order_filter.orderState = parseInt(id);
        }
        reset_order_edit_process_buttons();
        reload_order_table();
    });

    $("#order_specialist_filter").change(function () {
        order_filter.page = 1;
        var id = $(this).children(":selected").prop("id").split("_")[3];
        if (id == 0) {
            order_filter.filterUserId = "";
        } else {
            order_filter.filterUserId = parseInt(id);
        }
        reset_order_edit_process_buttons();
        reload_order_table();
    });

    $("#order_decl_name-filter").keyup(function (e) {
        if (e.keyCode == 13) {
            order_filter.page = 1;
            order_filter.decl = $("#order_decl_name-filter").val();
            reset_order_edit_process_buttons();
            reload_order_table();
        }
    });

    $("#order_sort_1").click(function () {
        if (order_filter.sort == 1) {
            if (equals(order_filter.sortType, "desc")) {
                order_filter.sortType = "asc";
            } else if (equals(order_filter.sortType, "asc")) {
                order_filter.sortType = "desc";
            }
        } else {
            order_filter.sort = 1;
            order_filter.sortType = "desc";
        }
        reload_order_table();
        set_active_page();
    });

    $("#order_sort_2").click(function () {
        if (order_filter.sort == 2) {
            if (equals(order_filter.sortType, "desc")) {
                order_filter.sortType = "asc";
            } else if (equals(order_filter.sortType, "asc")) {
                order_filter.sortType = "desc";
            }
        } else {
            order_filter.sort = 2;
            order_filter.sortType = "desc";
        }
        reload_order_table();
        set_active_page();
    });

    $("#order_edit_order").click(function () {
        httpServices.get_editable_order(current_order_id);
        if (check_tor()) {
            if (current_order.stateId != order_scope.states[0].id) {
                open_order_view();
            } else {
                open_order_edit();
            }
        } else {
            open_order_view();
        }

    });

    $("#order_cancel").click(function () {
        close_order();
    });

    $("#order_process_order").click(function () {
        httpServices.get_order_objects(current_order_id);
        httpServices.ate_list_order_search(-1);
        if (current_order.stateId == order_scope.states[0].id) {
            load_order_objects_table();
            if (order_scope.order_objects[0].isNk == 0) {
                defaultOrder();
            } else {
                defaultOrderNalog();
            }
            init_process_order();
            open_process_order();
        } else {
            load_order_objects_table_edit();
            init_process_order_edit();
            open_process_order();
        }
    });

    $("#search-obl-st-order").change(function () {
        var parentId = $("#search-obl-st-order").val();
        $("#search-name-st-order").empty();
        if (parentId == -1) {
            $("#search-raion-st-order").empty();
        } else {
            httpServices.ate_list_order_search(parentId);
            init_st_areas();
        }
    });

    $("#search-raion-st-order").change(function () {
        var number = $("#search-raion-st-order").val();
        var date = get_normal_date();
        if (number == -1) {
            $("#search-name-st-order").empty();
        } else {
            httpServices.st_list_order_search(number, date);
            init_st();
        }
    });

    $("#cadastr-st-search-order-btn").click(function () {
        var soato = $("#soato-st-order").val();
        var block = $("#numblock-st-order").val();
        var parcel = $("#numuch-st-order").val();
        var normalDate = get_normal_date();
        current_st_cadnum_id = null;
        $("#search-addr-st-order").val('');
        httpServices.st_carastar_value_order_search(soato, block, parcel, normalDate);
        if (order_scope.order_st_process_search != null) {
            init_st_cadnum_table();
        }

    });

    $("#order-obl3").change(function () {
        var parentId = $("#order-obl3").val();
        $("#order-unp").empty().prop("disabled", "disabled");
        $("#order-2-unp").prop("disabled", "disabled");
        if (parentId == -1) {
            $("#order-raion3").empty().prop("disabled", "disabled");
        } else {
            httpServices.ate_list_order_search(parentId);
            init_unp_sx_areas();
        }
    });

    $("#order-raion3").change(function () {
        var number = $("#order-raion3").val();
        var date = get_normal_date();
        $("#order-2-unp").prop("disabled", "disabled");
        if (number == -1) {
            $("#order-unp").empty().prop("disabled", "disabled");
        } else {
            httpServices.sx_list_order_search(number, date, 81);
            init_unp_sx();
        }
    });

    $("#order-unp").change(function () {
        var text = $("#order-unp").val();
        if (text == -1) {
            $("#order-2-unp").prop("disabled", "disabled");
        } else {
            $("#order-2-unp").removeProp("disabled");
        }
    });


    // ДОДЕЛАТЬ
    $("#order-2-unp").click(function () {
        var text = $("#order-unp").val();
        var ateId = $("#order-raion3").val();
        var date = get_normal_date();
        httpServices.sx_search_order(81, text, ateId, date);
        clear_sx_search_result();
        if (order_scope.order_process_sx_search == null) {
            hide_sx_order_result();
        } else {
            init_info_result(order_scope.order_process_sx_search);
            show_sx_order_result();
        }
    });

    $("#search-order-obl5").change(function () {
        var parentId = $("#search-order-obl5").val();
        $("#search-order-name_zemlepol").empty();
        if (parentId == -1) {
            $("#search-order-raion5").empty();
        } else {
            httpServices.ate_list_order_search(parentId);
            init_name_sx_areas();
        }
    });

    $("#search-order-raion5").change(function () {
        var number = $("#search-order-raion5").val();
        var date = get_normal_date();
        if (number == -1) {
            $("#search-order-name_zemlepol").empty();
        } else {
            httpServices.sx_list_order_search(number, date, 82);
            init_name_sx();
        }
    });

    $("#search-order-name_zemlepol").change(function () {
        var text = $("#search-order-name_zemlepol").val();
        if (text == -1) {
            $("#order-name-2-btn").prop("disabled", "disabled");
        } else {
            $("#order-name-2-btn").removeProp("disabled");
        }
    });

    $("#order-obl4").change(function () {
        var parentId = $("#order-obl4").val();
        $("#order-num_oc_zone").empty();
        if (parentId == -1) {
            $("#order-raion4").empty();
        } else {
            httpServices.ate_list_order_search(parentId);
            init_zone_sx_areas();
        }
    });

    $("#order-raion4").change(function () {
        var number = $("#order-raion4").val();
        var date = get_normal_date();
        if (number == -1) {
            $("#order-num_oc_zone").empty();
        } else {
            httpServices.sx_list_order_search(number, date, 83);
            init_zone_sx();
        }
    });

    $("#order-num_oc_zone").change(function () {
        var text = $("#order-num_oc_zone").val();
        if (text == -1) {
            $("#order-num-2-btn").prop("disabled", "disabled");
        } else {
            $("#order-num-2-btn").removeProp("disabled");
        }
    });

    $("#order-name-2-btn").click(function () {
        var text = $("#search-order-name_zemlepol").val();
        var ateId = $("#search-order-raion5").val();
        var date = get_normal_date();
        httpServices.sx_search_order(82, text, ateId, date);

        clear_sx_search_result();
        if (order_scope.order_process_sx_search == null) {
            hide_sx_order_result();
        } else {
            init_info_result(order_scope.order_process_sx_search);
            show_sx_order_result();
        }
    })

    $("#order-num-2-btn").click(function () {
        var text = $("#order-num_oc_zone").val();
        var ateId = $("#order-raion4").val();
        var date = get_normal_date();
        httpServices.sx_search_order(83, text, ateId, date);

        clear_sx_search_result();
        if (order_scope.order_process_sx_search == null) {
            hide_sx_order_result();
        } else {
            init_info_result(order_scope.order_process_sx_search);
            show_sx_order_result();
        }
    });

    $("#sh-order-unp").change(function () {
        change_sx_type(81);
    });

    $("#sh-order-name").change(function () {
        change_sx_type(82);
    });

    $("#sh-order-zone").change(function () {
        change_sx_type(83);
    });

    $("#search-obl-snp-order").change(function () {
        var parentId = $("#search-obl-snp-order").val();
        $("#search-ss-snp-order").empty();
        if (parentId == -1) {
            $("#search-raion-snp-order").empty();
        } else {
            httpServices.ate_list_order_search(parentId);
            init_snp_areas();
        }
    });

    $("#search-raion-snp-order").change(function () {
        var parentId = $("#search-raion-snp-order").val();
        if (parentId == -1) {
            $("#search-ss-snp-order").empty();
        } else {
            httpServices.ate_list_order_search(parentId);
            init_snp_ss();
        }
    });

    $("#search-obl-mesto-order").change(function () {
        var parentId = $("#search-obl-mesto-order").val();
        if (parentId == -1) {
            $("#search-raion-mesto-order").empty();
        } else {
            httpServices.ate_list_order_search(parentId);
            init_location_areas();
        }
    });

    $("#search-obl-st-notar-order").change(function () {
        var parentId = $("#search-obl-st-notar-order").val();
        if (parentId == -1) {
            $("#search-obl-st-notar-order").empty();
        } else {
            httpServices.ate_list_order_search(parentId);
            init_snp_ss_notar();
        }
    });

    $("#isCadastr").click(function () {
        if ($("#isCadastr").prop('checked')) {
            $("#block-cadastr-order").removeClass('disabled-div-css');
            $("#search-addr-st-order").addClass('disabled-div-css').val('');
        } else {
            $("#block-cadastr-order").addClass('disabled-div-css');
            $("#search-addr-st-order").removeClass('disabled-div-css').val('');
        }
        clear_st_cadaster_fields();
        $("#order_st_cadnum_table").empty();
    });

    $("#clear-all-cadastr-order").click(function () {
        clear_cadastr_fields();
    });

    $("#soato-order").keydown(function () {
        return input_limit(event.keyCode, $(this), 10);
    });

    $("#numblock-order").keydown(function () {
        return input_limit(event.keyCode, $(this), 2);
    });

    $("#numuch-order").keydown(function () {
        return input_limit(event.keyCode, $(this), 6);
    });

    $("#soato-st-order").keydown(function () {
        return input_limit(event.keyCode, $(this), 10);
    });

    $("#numblock-st-order").keydown(function () {
        return input_limit(event.keyCode, $(this), 2);
    });

    $("#numuch-st-order").keydown(function () {
        return input_limit(event.keyCode, $(this), 6);
    });

    $("#st-notar-add-order-btn").click(function () {
        if (!(check_order_st_notar_address() || check_order_st_notar_obl() || check_order_st_notar_raion() || check_order_st_notar_fn())) {
            null_order_object();
            init_st_notar_object();
            httpServices.add_order_object();
            clear_st_notar_fields();
        } else {
            alert("Заполните все необходимые поля!");
        }
    });


    $("#cadastr-ok-order-btn").click(function () {
        var value = $("#order-res-search-body").find(".selected-tr").children(":first-child").text();
        if (!equals(value, "")) {
            value = current_cadnum_id.split("-"),
                org_id = parseInt(value[2]),
                parcel_id = parseInt(value[3]);
            null_order_object();
            init_cadastr_object(parcel_id, org_id);
            httpServices.add_order_object();
        } else {
            alert("Выберите участок!");
        }
    });


    $("#remove-all-order").click(function(){
        httpServices.delete_order(order_scope.order_objects[0].orderId, true);
        $("#order-object-table").empty();

    })

    $("#snp-add-order-btn").click(function () {
        if (!(check_order_snp_address() || check_order_snp_obl() || check_order_snp_raion() || check_order_snp_ss() || check_order_snp_fn())) {
            null_order_object();
            init_snp_object();
            httpServices.add_order_object();
            clear_snp_fields();
        } else {
            alert("Заполните все необходимые поля!");
        }
    });

    $("#st-add-order-btn").click(function () {
        if (!(check_order_st_obl() || check_order_st_raion() || check_order_st_name() || check_order_st_address())) {
            null_order_object();
            if (check_st_search_type()) {
                init_st_cadaster_object();
            } else {
                init_st_address_object();
            }
            httpServices.add_order_object();
        } else {
            alert("Заполните все необходимые поля!");
        }
    });

    $("#mesto-add-order-btn").click(function() {
       if (!(check_order_mesto_obl() || check_order_mesto_raion() || check_order_mesto_mesto())) {
           null_order_object();
           init_mesto_object();
           httpServices.add_order_object();
       } else {
           alert("Заполните все необходимые поля!");
       }
    });

    $("#sx-ok-order-btn").click(function () {
        var value = $("#order-res-sx-search-body").find(".selected-tr").children(":first-child").text();
        if (!equals(value, "")) {
            value = current_sx_id.split("-");
            var zone_id = parseInt(value[2]);
            null_order_object();
            init_sx_object(zone_id);
            httpServices.add_order_object();
        } else {
            alert("Выберите участок!");
        }
    });

    $("#change-location").click(function(){
        $("#order-object-table").addClass("disabled-div");
        $("#search_tabs").addClass("disabled-div");
        change_location();
    });

    $("#create-doc").click(function () {
        httpServices.get_doc_v(current_object_id, AUTHOBJ.userId);
        //window.location = httpServices.getPdf(search_scope);
    });

    $("#save_order").click(function(){
        httpServices.save_order(current_object_id, AUTHOBJ.userId);
        load_order_objects_table();
        alert("Объект был сохранён");
        $("#save_order").addClass('disabled-div');
        $("#change-location").addClass('disabled-div');

    });

    $("#save-all").click(function(){
        console.log(current_order.orderId);
        if(confirm("Вы уверены, что хотите завершить заказ?")) {
            httpServices.save_Allorder(current_order.orderId, AUTHOBJ.userId)
            if (!$("#save-all").hasClass('disabled-div')) {
                $("#save-all").addClass('disabled-div');
            }
        }
    });
    
    $("#reset-location").click(function () {
        httpServices.reset_location(current_object_id);
        $("#reset-location").addClass('disabled-div');
    });
});
