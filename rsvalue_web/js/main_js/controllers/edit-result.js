/**
 * Created by Shcherbina on 22.07.2016.
 */
var load_zurnal_obj_header = {
    1:"regNum",
    2:"dateRegStart",
    3:"dateVal",
    4:"ateName",
    5:"costTypeName",
    6:"funcCodeName",
    7:"decNum",
    8:"decDate",
    9:"decOrganization",
    10:"valOrganization",
    11: "methodTypeName",
    12: "actual"
}, load_zurnal_enam_header = {
    1:"Рег. номер",
    2:"Дата загрузки",
    3:"Дата оценки",
    4:"Город/ район",
    5:"Вид оценки",
    6:"Функциональное назначение",
    7:"№ решения",
    8:"Дата решения",
    9:"Наименование местного исполнительного органа",
    10:"Организация, выполнившая оценку",
    11: "Методика оценки",
    12: "Актуальность"
};
var DWD_ID = null;
$(document).ready(function () {

    $("#save-edit").click(function () {
        if(edit_added_docs !=0 || edit_del_docs != 0) {
            var isNext = confirm("Сохранить изменения?")
            if (isNext) {
                var object_save = {
                    userId: AUTHOBJ.userId,
                    regNum: ID_EDIT,
                    deleteDocList: edit_del_docs,
                    saveDocList: edit_added_docs,
                    dwdId: DWD_ID,
                    save: true
                };

                console.log(object_save)
                httpServices.saveEditObject(JSON.stringify(object_save))
            }
        }
    })

    $("#close-edit").click(function () {
        if(edit_added_docs !=0 || edit_del_docs != 0) {
            var isNext = confirm("Закрыть окно без сохранения изменений?")
            if (isNext) {
                var object_close = {
                    userId: AUTHOBJ.userId,
                    regNum: ID_EDIT,
                    deleteDocList: edit_del_docs,
                    saveDocList: edit_added_docs,
                    dwdId: DWD_ID,
                    save: false
                };
                httpServices.saveEditObject(JSON.stringify(object_close))
            }
        }
    })


});