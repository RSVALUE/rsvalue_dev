/**
 * Created by Shcherbina on 21.10.2016.
 */

var BY_CADASTR = 0, BY_ADDR = 0, BY_ST = 0, BY_VNP = 0, BY_SH_UNP = 0, BY_SH_ZEMLEP = 0, BY_SH_NUM = 0;
var CADASTR_RES = 0;
var cadNum = [soato="", blockNum="", parceNum=""];
var addres = [region = "", typeIn="", typeOut="", area="", ss="", snp="", bjd="", road="", km="", street=""];

var now = (new Date().toLocaleDateString()).split(".");
var now_date = now[2] + "-"+ now[1] + "-"+ now[0];
var pdf_created = false;

function addresInit(){
    addres.region = "";
    addres.typeIn="";
    addres.typeOut="";
    addres.area="";
    addres.ss="";
    addres.snp="";
    addres.bjd="";
    addres.road="";
    addres.km="";
    addres.street="";
    addres.numDom = "";
}

$(document).ready(function () {

    search_scope.isNalog = false;
    httpServices.getObl(search_scope);

    $("#input-date-search").val(now_date);
    $("#date-search-2").val(now_date);

    search_scope.date_search = $("#input-date-search").val();
    search_scope.search_2_date = $("#date-search-2").val();

    removeDisabled_2();


    $("#main-seach-1-1").removeClass("disabled-div");
    search_scope.date_search


    $('[data-toggle="popover-search"]').popover();

    $("#clear-all-cadastr").click(function(){
        $("#soato").val('');
        $("#numblock").val('');
        $("#numuch").val('');
        $("#cadastr-search-btn").attr("disabled","disabled");
    })

   $("#cad-st").change(function(){
       if(!$("#input-date-search").hasClass("display-visible")){
           $("#input-date-search").removeClass("display-none");
           $("#input-date-search").addClass("display-visible");
       }
       if(!$("#select-date-search").hasClass("display-none")){
           $("#select-date-search").addClass("display-none");
           $("#select-date-search").removeClass("display-visible");
       }
       if(!$("#cad-st-text").hasClass("select-radio")){
           $("#cad-st-text").addClass("select-radio");
           $("#nalog-db-text").removeClass("select-radio");
       }
       searchPdf.isNb = false;
       search_scope.isNalog = false;
       if($("#input-date-search").val() == -1){
           $("#main-seach-1-1").addClass("disabled-div");
       }else{
           $("#main-seach-1-1").removeClass("disabled-div");
       }

       search_scope.date_search = $("#input-date-search").val();
       clearAllRes();
       $("#select-date-search").val("01.01.2017");
       $("#input-date-search").val(now_date);
   })

    $("#input-date-search").change(function(){
        var d = $("#input-date-search").val();
        if(d.length !=0) {
            if(dateNotNow(d)) {
                search_scope.date_search = d;
                $("#main-seach-1-1").removeClass("disabled-div");
                allSearchClean();
                clearAllRes();
                BY_CADASTR = 1;
            }else{
                $("#input-date-search").val(now_date);
            }
        }else{
            $("#main-seach-1-1").addClass("disabled-div");
        }
    })

    $("#nalog-db").change(function(){
        if(!$("#select-date-search").hasClass("display-visible")){
            $("#select-date-search").removeClass("display-none");
            $("#select-date-search").addClass("display-visible");
        }
        if(!$("#input-date-search").hasClass("display-none")){
            $("#input-date-search").removeClass("display-visible");
            $("#input-date-search").addClass("display-none");
        }
        if(!$("#nalog-db-text").hasClass("select-radio")){
            $("#nalog-db-text").addClass("select-radio");
            $("#cad-st-text").removeClass("select-radio");
        }

        searchPdf.isNb = true;
        search_scope.isNalog = true;
        if($("#select-date-search").val() == -1){
            $("#main-seach-1-1").addClass("disabled-div");
        }else{
            $("#main-seach-1-1").removeClass("disabled-div");
        }

        clearAllRes();
        $("#select-date-search").val('01.01.2017');
        $("#input-date-search").val(now_date);
        search_scope.date_search = $("#select-date-search").val();
    })

    $("#search-by-cadastr-1").change(function(){
        closeAllRes();
        if(!$("#search-cadastr-1").hasClass("display-visible")){
            $("#search-cadastr-1").removeClass("display-none");
            $("#search-cadastr-1").addClass("display-visible");
        }
        if(!$("#search-addr-1").hasClass("display-none")){
            $("#search-addr-1").removeClass("display-visible");
            $("#search-addr-1").addClass("display-none");
        }
        if(!$("#search-terr-vne-1").hasClass("display-none")){
            $("#search-terr-vne-1").removeClass("display-visible");
            $("#search-terr-vne-1").addClass("display-none");
        }
        if(!$("#search-st-1").hasClass("display-none")){
            $("#search-st-1").removeClass("display-visible");
            $("#search-st-1").addClass("display-none");
        }
        allSearchClean();
        BY_CADASTR = 1;
    })

    $("#search-by-addr-1").change(function(){
        closeAllRes();
        if(!$("#search-addr-1").hasClass("display-visible")){
            $("#search-addr-1").removeClass("display-none");
            $("#search-addr-1").addClass("display-visible");
        }
        if(!$("#search-cadastr-1").hasClass("display-none")){
            $("#search-cadastr-1").removeClass("display-visible");
            $("#search-cadastr-1").addClass("display-none");
        }
        if(!$("#search-terr-vne-1").hasClass("display-none")){
            $("#search-terr-vne-1").removeClass("display-visible");
            $("#search-terr-vne-1").addClass("display-none");
        }
        if(!$("#search-st-1").hasClass("display-none")){
            $("#search-st-1").removeClass("display-visible");
            $("#search-st-1").addClass("display-none");
        }
    })

    $("#search-by-ct-1").change(function(){
        closeAllRes();
        if(!$("#search-st-1").hasClass("display-visible")){
            $("#search-st-1").removeClass("display-none");
            $("#search-st-1").addClass("display-visible");
        }
        if(!$("#search-cadastr-1").hasClass("display-none")){
            $("#search-cadastr-1").removeClass("display-visible");
            $("#search-cadastr-1").addClass("display-none");
        }
        if(!$("#search-addr-1").hasClass("display-none")){
            $("#search-addr-1").removeClass("display-visible");
            $("#search-addr-1").addClass("display-none");
        }
        if(!$("#search-terr-vne-1").hasClass("display-none")){
            $("#search-terr-vne-1").removeClass("display-visible");
            $("#search-terr-vne-1").addClass("display-none");
        }
        allSearchClean();
        BY_ST = 1;
    })

    $("#search-by-vnp-1").change(function(){
        closeAllRes();
        if(!$("#search-terr-vne-1").hasClass("display-visible")){
            $("#search-terr-vne-1").removeClass("display-none");
            $("#search-terr-vne-1").addClass("display-visible");
        }
        if(!$("#search-cadastr-1").hasClass("display-none")){
            $("#search-cadastr-1").removeClass("display-visible");
            $("#search-cadastr-1").addClass("display-none");
        }
        if(!$("#search-addr-1").hasClass("display-none")){
            $("#search-addr-1").removeClass("display-visible");
            $("#search-addr-1").addClass("display-none");
        }
        if(!$("#search-st-1").hasClass("display-none")){
            $("#search-st-1").removeClass("display-visible");
            $("#search-st-1").addClass("display-none");
        }
        allSearchClean();
        BY_VNP = 1;
    })

    $("#sh-search-2-unp").change(function(){
        closeAllRes();
        clear_all_2();
        if(!$("#search-unp-2").hasClass("display-visible")){
            $("#search-unp-2").removeClass("display-none");
            $("#search-unp-2").addClass("display-visible");
        }
        if(!$("#search-num-2").hasClass("display-none")){
            $("#search-num-2").removeClass("display-visible");
            $("#search-num-2").addClass("display-none");
        }
        if(!$("#search-name-2").hasClass("display-none")){
            $("#search-name-2").removeClass("display-visible");
            $("#search-name-2").addClass("display-none");
        }
        allSearchClean();
        BY_SH_UNP = 1;
    })

    $("#sh-search-2-name").change(function(){
        closeAllRes();
        clear_all_2();
        if(!$("#search-name-2").hasClass("display-visible")){
            $("#search-name-2").removeClass("display-none");
            $("#search-name-2").addClass("display-visible");
        }
        if(!$("#search-unp-2").hasClass("display-none")){
            $("#search-unp-2").removeClass("display-visible");
            $("#search-unp-2").addClass("display-none");
        }
        if(!$("#search-num-2").hasClass("display-none")){
            $("#search-num-2").removeClass("display-visible");
            $("#search-num-2").addClass("display-none");
        }
        allSearchClean();
        BY_SH_ZEMLEP = 1;
    })

    $("#sh-search-2-zone").change(function(){
        closeAllRes();
        clear_all_2();
        if(!$("#search-num-2").hasClass("display-visible")){
            $("#search-num-2").removeClass("display-none");
            $("#search-num-2").addClass("display-visible");
        }
        if(!$("#search-unp-2").hasClass("display-none")){
            $("#search-unp-2").removeClass("display-visible");
            $("#search-unp-2").addClass("display-none");
        }
        if(!$("#search-name-2").hasClass("display-none")){
            $("#search-name-2").removeClass("display-visible");
            $("#search-name-2").addClass("display-none");
        }
        allSearchClean();
        BY_SH_NUM=1;
    })

    $("#menu-down-2").click(function(){
        menu2();
        menu1();
        BY_SH_UNP = 1;
    });

    $("#menu-down-2-2").click(function(){
        menu2();
        menu1();
        BY_SH_UNP = 1;
    });

    $("#menu-1-1").click(function(){
        menu1_span();
        menu2_span();
    })

    $("#menu-2-2").click(function(){
        BY_SH_UNP = 1;
        menu1_span();
        menu2_span();
    })


    $("#search-menu-down-1").click(function(){
       menu1();
        menu2();
    })

    $("#search-menu-down-1-1").click(function(){
       menu1();
        menu2();
    })

    $("#select-date-search").append(function(){
        httpServices.getNalogPeriod(search_scope);
        var option_date_nalog="";
        var objects = search_scope.nalogDate;
        console.log(objects)
        var size = objects.length;
        for(var i = size-1; i>=0; i--){
            option_date_nalog += option(i, search_scope.nalogDate[i], search_scope.nalogDate[i]);
        }
       /* $.each(search_scope.nalogDate, function (i) {
            option_date_nalog += option(i, search_scope.nalogDate[i], search_scope.nalogDate[i]);
        })*/
        return option_date_nalog;
    });



    $("#search-obl-1").append(function(){
        var opt_st = option(-1, -1, "");
        $.each(search_scope.searchAddr_obl, function (i) {
            opt_st += option(i, search_scope.searchAddr_obl[i].ateId, search_scope.searchAddr_obl[i].ateName);
        });
        return opt_st;
    });

    $("#select-date-search").change(function(){
        var d = $("#select-date-search").val();
        if(d !=-1) {
            search_scope.date_search = d;
            $("#main-seach-1-1").removeClass("disabled-div");
        }else{
            $("#main-seach-1-1").addClass("disabled-div");
        }
    })

    $("#search-by-ct-1").click(function(){
        $("#search-by-ST-btn").attr("disabled","disabled");

        $("#search-raion-1").attr("disabled","disabled");
        $("#search-name-st-1").attr("disabled","disabled");
    });

    $("#search-obl-1").change(function(){
        changeSearchBySTObl()
        var ateId = ($("#search-obl-1").val());
        if(ateId !=-1 && ateId!=null){
            search_scope.searchAddr_obl_ateID = ateId;
            httpServices.getObl_raion_by_ateId(search_scope, search_scope.searchAddr_obl_ateID);
        $("#search-raion-1").append(function() {
            var option_raion = option(-1, -1, "");
            $.each(search_scope.searchAddr_raion, function (i) {
                option_raion += option(i, search_scope.searchAddr_raion[i].ateId, search_scope.searchAddr_raion[i].ateName);
            })
            return option_raion;
        });
            $("#search-raion-1").removeAttr("disabled");
        }
    });

    $("#search-raion-1").change(function(){
        changeSearchBySTORaion()
        var ateId = ($("#search-raion-1").val());
        if(ateId !=-1){
            search_scope.searchAddr_raion_ateID = ateId;
            httpServices.getObl_st_by_raion(search_scope, search_scope.searchAddr_raion_ateID);
            if(search_scope.searchAddr_raionST.length != 0) {
                $("#search-name-st-1").removeAttr("disabled");
                $("#search-name-st-1").append(function () {
                    var option_raion = option(-1, -1, "");
                    $.each(search_scope.searchAddr_raionST, function (i) {
                        option_raion += option(i, search_scope.searchAddr_raionST[i][0], search_scope.searchAddr_raionST[i][1]);
                    })
                    return option_raion;
                });
            }else{
                alert("По Вашему запросу ничего не найдено")
            }
        }
    });

    $("#search-name-st-1").change(function(){
        var zone = $("#search-name-st-1").val();
        if(zone!=-1) {
            $("#search-by-ST-btn").removeAttr("disabled");
            search_scope.searchST_ST = zone;
        }
    })

    $("#search-by-ST-btn").click(function(){
        clearDivNameSad();
        httpServices.getObl_st_res1(search_scope, search_scope.searchST_ST, search_scope.isNalog);
        if($("#res-search-st-1").hasClass("display-none")){
            $("#res-search-st-1").removeClass("display-none");
            $("#res-search-st-1").addClass("display-visible");
        }
        searchByNameSadDiv();
    });

    $("#cadastr-for-me-st").click(function(){
        closeDialog();
        clearModalNameSad();
        searchByNameSadModal();
        $("#svedenia-st").removeClass("display-none");
        $("#cadastr-stoim-search").modal("show");
    })

    $("#search-cadastr-close").click(function(){
        closeDialog();
    });

    $("#search-by-vnp-1").click(function(){
        $("#search-fun").empty();
        httpServices.getFunNazn(search_scope);
        $("#search-fun").append(function(){
            var text =  option(-1, -1, "");
            var obj = search_scope.searchVNP_fun;
            for(var i=0; i<obj.length; i++){
                text+=option(i, obj[i].analyticCode, obj[i].codeName);
            }
            return text;
        })
    })

    $("#date-search-2").change(function(){
        var d = $("#date-search-2").val();
        if(d.length >0) {
            if(dateNotNow(d)) {
                removeDisabled_2();
                search_scope.search_2_date = d;
            }else{
                $("#date-search-2").val(now_date);
            }
        }else{
            addDisabled_2();
        }
        clear_all_2();
    })

    $("#search-obl3").append(function(){
        var text =  option(-1, -1, "");
        $.each(search_scope.searchAddr_obl, function (i, obj) {
            text+=option(i, search_scope.searchAddr_obl[i].ateId, search_scope.searchAddr_obl[i].ateName);
        });
        return text
    });

    $("#search-obl3").change(function(){
        clearSearchByUnp_obl();
        var ateId = $("#search-obl3").val();
        if(ateId !=-1 && ateId!=null){
            search_scope.searchAddr_obl_ateID = ateId;
            httpServices.getObl_raion_by_ateId(search_scope, search_scope.searchAddr_obl_ateID);
            $("#search-raion3").append(function(){
                var option_raion = option(-1, -1, "");
                $.each(search_scope.searchAddr_raion, function (i) {
                    option_raion += option(i, search_scope.searchAddr_raion[i].ateId, search_scope.searchAddr_raion[i].ateName);
                });
                $("#search-raion3").removeAttr("disabled");
                return option_raion;
            })
        }
    })

    $("#search-raion3").change(function(){
        clearSearchByUnp_raion();
        var ateId = $("#search-raion3").val();
        if(ateId !=-1){
            search_scope.searchAddr_raion_ateID = ateId;
                $("#search-UNP").removeAttr("disabled");
        }
    });

    $("#search-UNP").change(function(){
        var val = $("#search-UNP").val();
        if(val.length !=0){
            $("#search-2-unp").removeAttr("disabled");
            search_scope.searchCadasrtCost_unp = val;
        }
    })

    $("#search-2-unp").click(function() {
        var unp = $("#search-UNP").val();
        if(unp.length != 0) {
            $("#search-address3").empty();
            $("#search-name").empty();
            $("#search-num").empty();

            $("#cadastr-name-sh").empty();
            $("#cadastr-addres-sh").empty();
            $("#cadastr-num-sh").empty();

            $("#sh-table").empty();
            $("#text-sh").empty();

            httpServices.getby_unp(search_scope, search_scope.searchAddr_raion_ateID, search_scope.searchCadasrtCost_unp, search_scope.isNalog, search_scope.search_2_date);

            $("#res-search-2").removeClass("display-none")


            $("#svedenia-sh").removeClass("display-none")
            $("#search-address3").append(search_scope.searchCadasrtCost_byUnp.address);
            $("#search-name").append(search_scope.searchCadasrtCost_byUnp.zoneName);
            $("#search-num").append(search_scope.searchCadasrtCost_byUnp.unp);

            $("#cadastr-name-sh").append(search_scope.searchCadasrtCost_byUnp.zoneName);
            $("#cadastr-addres-sh").append(search_scope.searchCadasrtCost_byUnp.address);
            $("#cadastr-num-sh").append(search_scope.searchCadasrtCost_byUnp.unp);

            if(search_scope.isNalog){
                if($("#sh-nalog-table").hasClass("display-none")){
                    $("#sh-nalog-table").removeClass("display-none");
                }
                // не менять!!!
                $("#sh-nalog-table").text("Кадастровая стоимость 1 кв.м земельного участка на "+search_scope.searchCadasrtCost_byUnp.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
                noDisplZvezda(); noDispRub();
                if(!$("#ch-table-search-header").hasClass('display-none')){
                    $("#ch-table-search-header").addClass('display-none')
                }
            }else {
                if($("#ch-table-search-header").hasClass('display-none')){
                    $("#ch-table-search-header").removeClass('display-none')
                }
                $("#ch-table-search-header").text("Кадастровая стоимость 1 гектара земельного участка по состоянию на " + search_scope.searchCadasrtCost_byUnp.searchDate)
                displZvezda(); dispRub();
            }
            $("#sh-table").append(function () {
                var text = "", i = 0;
                for (; i < search_scope.searchCadasrtCost_byUnp.valuesCharacters.length; i++) {
                    text += "<tr>"
                        + "<td>"
                        + search_scope.searchCadasrtCost_byUnp.valuesCharacters[i].funcCodeName
                        + "</td>"
                        + "<td>"
                        + search_scope.searchCadasrtCost_byUnp.valuesCharacters[i].dateVal
                        + "</td>"
                        + "<td>"
                        + search_scope.searchCadasrtCost_byUnp.valuesCharacters[i].zoneNum
                        + "</td>";
                    if(!search_scope.isNalog) {
                        text += "<td>"
                            + search_scope.searchCadasrtCost_byUnp.valuesCharacters[i].costD
                            + "</td>"
                            + "<td>"
                            + search_scope.searchCadasrtCost_byUnp.valuesCharacters[i].costR
                            + "</td>";
                    }
                    if(search_scope.isNalog){
                        text+="<td>"
                            + search_scope.searchCadasrtCost_byUnp.valuesCharacters[i].costNB
                            + "</td>"
                    }
                    text+= "</tr>"
                }
                return text;
            });
            $("#text-sh").append(function () {
                var j = 0;
                var text = search_scope.searchCadasrtCost_byUnp.remark;
                return text;
            });
        }
    })

    $("#search-num-2-btn").click(function(){
        var num = $("#search-ocen_zona_num").val();
        if(num != -1) {
            $("#search-address3").empty();
            $("#search-name").empty();
            $("#search-num").empty();

            $("#cadastr-name-sh").empty();
            $("#cadastr-addres-sh").empty();
            $("#cadastr-num-sh").empty();

            $("#sh-table").empty();
            $("#text-sh").empty();

            httpServices.getresBy_NumZone_search2(search_scope, search_scope.searchCadasrtCost_zemlep,  search_scope.isNalog, search_scope.search_2_date);

            $("#res-search-2").removeClass("display-none")


            $("#svedenia-sh").removeClass("display-none")
            $("#search-address3").append(search_scope.searchNumZone_Res.address);
            $("#search-name").append(search_scope.searchNumZone_Res.zoneName);
            $("#search-num").append(search_scope.searchNumZone_Res.unp);

            $("#cadastr-name-sh").append(search_scope.searchNumZone_Res.zoneName);
            $("#cadastr-addres-sh").append(search_scope.searchNumZone_Res.address);
            $("#cadastr-num-sh").append(search_scope.searchNumZone_Res.unp);
            if(search_scope.isNalog){
                if($("#sh-nalog-table").hasClass("display-none")){
                    $("#sh-nalog-table").removeClass("display-none");
                }
                $("#sh-nalog-table").text("Кадастровая стоимость 1 кв.м земельного участка на "+search_scope.searchNumZone_Res.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
                noDisplZvezda(); noDispRub();
                if(!$("#ch-table-search-header").hasClass('display-none')){
                    $("#ch-table-search-header").addClass('display-none')
                }
            }else {
                // не менять!!!
                $("#ch-table-search-header").text("Кадастровая стоимость 1 гектара земельного участка по состоянию на " + search_scope.searchNumZone_Res.searchDate)
                displZvezda(); dispRub();
                if($("#ch-table-search-header").hasClass('display-none')){
                    $("#ch-table-search-header").removeClass('display-none')
                }
            }
            $("#sh-table").append(function () {
                var text = "", i = 0;
                for (; i < search_scope.searchNumZone_Res.valuesCharacters.length; i++) {
                    text += "<tr>"
                        + "<td>"
                        + search_scope.searchNumZone_Res.valuesCharacters[i].funcCodeName
                        + "</td>"
                        + "<td>"
                        + search_scope.searchNumZone_Res.valuesCharacters[i].dateVal
                        + "</td>"
                        + "<td>"
                        + search_scope.searchNumZone_Res.valuesCharacters[i].zoneNum
                        + "</td>"
                    if(!search_scope.isNalog) {
                        text += "<td>"
                            + search_scope.searchNumZone_Res.valuesCharacters[i].costD
                            + "</td>"
                            + "<td>"
                            + search_scope.searchNumZone_Res.valuesCharacters[i].costR
                            + "</td>";
                    }
                    if(search_scope.isNalog){
                        text+="<td>"
                            + search_scope.searchNumZone_Res.valuesCharacters[i].costNB
                            + "</td>"
                    }
                    text+= "</tr>"
                }
                return text;
            });
            $("#text-sh").append(function () {
                var j = 0;
                var text = search_scope.searchNumZone_Res.remark;
                return text;
            });
        }
    })

    $("#search-obl2").append(function(){
        var select = option(-1, -1, "");
        $.each(search_scope.searchAddr_obl, function (i) {
            select += option(i, search_scope.searchAddr_obl[i].ateId, search_scope.searchAddr_obl[i].ateName);
        })
        return select;
    })

    $("#search-fun").change(function(){
        changeFun();
        var val = $("#search-fun").val();
        if(val != -1 && val!=null){
            search_scope.searchVNP_FunCode = val;
            if($("#search-raion").val() != -1 && $("#search-raion").val() != null) {
                getOcenZona();
            }
        }
    });

    $("#search-obl2").change(function(){
        clearByObl_VNP();
        var val = $("#search-obl2").val();
        if(val!=null) {
            if (val != -1) {
                search_scope.searchAddr_raion_ateID = val;
                httpServices.getObl_raion_by_ateId(search_scope, search_scope.searchAddr_raion_ateID);
                $("#search-raion").append(function () {
                    var option_raion = option(-1, -1, "");
                    $.each(search_scope.searchAddr_raion, function (i) {
                        option_raion += option(i, search_scope.searchAddr_raion[i].ateId, search_scope.searchAddr_raion[i].ateName);
                    })
                    return option_raion;
                })
                $("#search-raion").removeAttr("disabled");
            } else {
                $("#search-raion").removeAttr("disabled");
                $("#search-raion").attr("disabled", "disabled");

            }
        }
    });


    $("#search-raion").change(function(){
        clearByRaion_VNP();
        var val = $("#search-raion").val();
        if(val != -1){
            search_scope.searchAddr_raion_ateID = val;
            if($("#search-fun").val() != -1 && $("#search-fun").val() != null) {
                getOcenZona();
            }
        }else {
            $("#search-ocen_zona").removeAttr("disabled");
            $("#search-ocen_zona").attr("disabled", "disabled");
        }
    });

    $("#search-ocen_zona").change(function(){
        clearByNameZona_VNP();
        var val = $("#search-ocen_zona").val();
        if(val != -1){
            search_scope.searchOcen_ZoneName = val;
            httpServices.getocen_zona_num(search_scope, search_scope.searchAddr_raion_ateID, search_scope.searchVNP_FunCode, search_scope.date_search, search_scope.searchOcen_ZoneName)
            $("#search-ocen_zona_num").append(function () {
                var option_raion = option(-1, -1, "");
                $.each(search_scope.searchVNP_NumZone, function (i, obj) {
                    option_raion += option(i, obj[0], obj[1]);
                })
                return option_raion;
            })
            $("#search-ocen_zona_num").removeAttr("disabled")
        }
    });

    $("#search-ocen_zona_num").change(function(){
        var val = $("#search-ocen_zona_num").val();
        if(val != -1) {
            $("#search-vne").removeAttr("disabled");
            search_scope.searchOcen_ZoneNum = val;
        }else{
            $("#search-vne").attr("disabled", "disabled");
        }
    })

    $("#search-vne").click(function(){
        $("#res-search-vne-1").removeClass("display-none")

        clearAllDivVNP();

        $("#cadastr-addres-vnp").empty();
        $("#cadastr-zone-vnp").empty();
        $("#cadastr-num-vnp").empty();


        $("#vnp-table").empty();
        $("#text-vnp").empty();
        httpServices.getVNP_res(search_scope, search_scope.searchOcen_ZoneNum, search_scope.isNalog, search_scope.date_search);
        search_scope.searchVNP_Res
        $("#res-search-vne-1").removeClass("display-none")


        $("#search-address2").append(search_scope.searchVNP_Res.address);
        $("#search-name_zone").append(search_scope.searchVNP_Res.zoneName);
        $("#search-num_zone").append(search_scope.searchVNP_Res.valuesCharacters[0].zoneNum);

        $("#cadastr-addres-vnp").append(search_scope.searchVNP_Res.address)
        $("#cadastr-zone-vnp").append(search_scope.searchVNP_Res.zoneName)
        $("#cadastr-num-vnp").append(search_scope.searchVNP_Res.valuesCharacters[0].zoneNum);

        if(search_scope.isNalog){
            if($("#vnp-nalog-table").hasClass("display-none")){
                $("#vnp-nalog-table").removeClass("display-none");
            }
            if(search_scope.searchVNP_Res.cadNum == null)
            {
                $("#vnp-nalog-table").text("Кадастровая стоимость 1 кв.м земельного участка на "+search_scope.searchVNP_Res.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
            }else{
                $("#vnp-nalog-table").text("Кадастровая стоимость 1 кв.м земель на "+search_scope.searchVNP_Res.searchDate+" года для исчисления налоговой базы земельного налога, рублей");
            }
            noDisplZvezda(); noDispRub();
            if(!$("#vnp-search-table-header").hasClass('display-none')){
                $("#vnp-search-table-header").addClass('display-none')
            }
        }else {
            displZvezda(); dispRub();
            if (search_scope.searchVNP_Res.cadNum == null) {
                $("#vnp-search-table-header").text("Кадастровая стоимость 1 кв. м. земель оценочной зоны на " + search_scope.searchVNP_Res.searchDate)
            } else {
                $("#vnp-search-table-header").text("Кадастровая стоимость 1 кв. м. земельного участка по состоянию на " + search_scope.searchVNP_Res.searchDate)
            }
            if($("#vnp-search-table-header").hasClass('display-none')){
                $("#vnp-search-table-header").removeClass('display-none')
            }
        }
        $("#vnp-table").append(function() {
            var text = "", i=0;
            for(;i<search_scope.searchVNP_Res.valuesCharacters.length; i++) {
                text+="<tr>"
                    + "<td>"
                    + search_scope.searchVNP_Res.valuesCharacters[i].funcCodeName
                    + "</td>"
                    + "<td>"
                    + search_scope.searchVNP_Res.valuesCharacters[i].dateVal
                    + "</td>"
                    + "<td>"
                    + search_scope.searchVNP_Res.valuesCharacters[i].zoneNum
                    + "</td>"
                if(!search_scope.isNalog) {
                    text += "<td>"
                        + search_scope.searchVNP_Res.valuesCharacters[i].costD
                        + "</td>"
                        + "<td>"
                        + search_scope.searchVNP_Res.valuesCharacters[i].costR
                        + "</td>";
                }
                if(search_scope.isNalog){
                    text+="<td>"
                        + search_scope.searchVNP_Res.valuesCharacters[i].costNB
                        + "</td>"
                }
                text+= "</tr>"
            }
            return text;
        });
        $("#text-vnp").append(function(){
            var j=0;
            var text =search_scope.searchVNP_Res.remark;
            return text;
        });
    })

    $("#view-vne").click(function(){
        closeDialog();
        $("#svedenia-vnp").removeClass("display-none");
        $("#cadastr-stoim-search").modal("show");
    })

    $("#search-obl4").append(function(){
        var select = option(-1, -1, "");
        $.each(search_scope.searchAddr_obl, function (i) {
            select += option(i, search_scope.searchAddr_obl[i].ateId, search_scope.searchAddr_obl[i].ateName);
        })
        return select;
    })

    $("#search-obl5").append(function(){
        var select = option(-1, -1, "");
        $.each(search_scope.searchAddr_obl, function (i) {
            select += option(i, search_scope.searchAddr_obl[i].ateId, search_scope.searchAddr_obl[i].ateName);
        })
        return select;
    })

    $("#search-obl4").change(function(){
        clearSearchByNumZone_obl();
        var ateId = $("#search-obl4").val();
        if(ateId !=-1 && ateId!=null){
            search_scope.searchAddr_obl_ateID = ateId;
            httpServices.getObl_raion_by_ateId(search_scope, search_scope.searchAddr_obl_ateID);
            $("#search-raion4").append(function(){
                var option_raion = option(-1, -1, "");
                $.each(search_scope.searchAddr_raion, function (i) {
                    option_raion += option(i, search_scope.searchAddr_raion[i].ateId, search_scope.searchAddr_raion[i].ateName);
                });
                $("#search-raion4").removeAttr("disabled");
                return option_raion;
            })
        }
    })

    $("#search-raion4").change(function(){
        clearSearchByNumZone_raion();
        var ateId = $("#search-raion4").val();
        if(ateId !=-1){
            search_scope.searchAddr_raion_ateID = ateId;
            httpServices.getnum_NumZone_search2(search_scope, search_scope.searchAddr_raion_ateID,search_scope.search_2_date)
            $("#search-num_oc_zone").append(function(){
                var option_raion = option(-1, -1, "");
                $.each(search_scope.searchNumZone_Num, function (i, obj) {
                    option_raion += option(i, search_scope.searchNumZone_Num[i][0], search_scope.searchNumZone_Num[i][1]);
                });
                $("#search-raion4").removeAttr("disabled");
                return option_raion;
            })
            $("#search-num_oc_zone").removeAttr("disabled");
        }
    });

    $("#search-num_oc_zone").change(function(){
        var val = $("#search-num_oc_zone").val();
        if(val != -1){
            $("#search-2-zemlep").removeAttr("disabled");
            search_scope.searchCadasrtCost_zemlep = val;

        }
    })

    $("#search-obl5").change(function(){
        clearSearchByZemlep_obl();
        var ateId = $("#search-obl5").val();
        if(ateId !=-1 && ateId!=null){
            search_scope.searchAddr_obl_ateID = ateId;
            httpServices.getObl_raion_by_ateId(search_scope, search_scope.searchAddr_obl_ateID);
            $("#search-raion5").append(function(){
                var option_raion = option(-1, -1, "");
                $.each(search_scope.searchAddr_raion, function (i) {
                    option_raion += option(i, search_scope.searchAddr_raion[i].ateId, search_scope.searchAddr_raion[i].ateName);
                });
                $("#search-raion5").removeAttr("disabled");
                return option_raion;
            })
        }
    })

    $("#search-raion5").change(function(){
        clearSearchByZemlep_raion();
        var ateId = $("#search-raion5").val();
        if(ateId !=-1){
            search_scope.searchAddr_raion_ateID = ateId;
            httpServices.getname_Zemlep_search2(search_scope, search_scope.searchAddr_raion_ateID,search_scope.search_2_date);
            console.log(search_scope.searchZemlep_Name)
            $("#search-name_zemlepol").append(function(){
                var option_raion = option(-1, -1, "");
                $.each(search_scope.searchZemlep_Name, function (i, obj) {
                    option_raion += option(i, search_scope.searchZemlep_Name[i], search_scope.searchZemlep_Name[i]);
                });
                return option_raion;
            })
            $("#search-name_zemlepol").removeAttr("disabled");
        }
    });

    $("#search-name_zemlepol").change(function(){
        var val = $("#search-name_zemlepol").val();
        if(val.length !=0){
            $("#search-2-zemlep").removeAttr("disabled");
            search_scope.searchCadasrtCost_zemlepName = val;
        }
    });

    $("#search-name-zemlep-btn").click(function(){
        $("#search-address3").empty();
        $("#search-name").empty();
        $("#search-num").empty();

        $("#cadastr-name-sh").empty();
        $("#cadastr-addres-sh").empty();
        $("#cadastr-num-sh").empty();

        $("#sh-table").empty();
        $("#text-sh").empty();

        httpServices.getresBy_Zemlep_search2(search_scope, search_scope.searchAddr_raion_ateID, search_scope.searchCadasrtCost_zemlepName, search_scope.isNalog,search_scope.search_2_date);

        $("#res-search-2").removeClass("display-none")


        $("#svedenia-sh").removeClass("display-none")
        $("#search-address3").append(search_scope.searchZemlep_Res.address);
        $("#search-name").append(search_scope.searchZemlep_Res.zoneName );
        $("#search-num").append(search_scope.searchZemlep_Res.unp);

        $("#cadastr-name-sh").append(search_scope.searchZemlep_Res.zoneName);
            $("#cadastr-addres-sh").append(search_scope.searchZemlep_Res.address);
            $("#cadastr-num-sh").append(search_scope.searchZemlep_Res.unp);

        if(search_scope.isNalog){
            if($("#sh-nalog-table").hasClass("display-none")){
                $("#sh-nalog-table").removeClass("display-none");
            }
            $("#sh-nalog-table").text("Кадастровая стоимость 1 кв.м земель на "+search_scope.searchZemlep_Res.searchDate+" года для исчисления налоговой базы земельного налог, рублей");
            noDisplZvezda(); noDispRub();
            if($("#ch-table-search-header").hasClass('display-none')){
                $("#ch-table-search-header").removeClass('display-none')
            }
        }else {
// не менять!!!
            $("#ch-table-search-header").text("Кадастровая стоимость 1 гектара земельного участка по состоянию на " + search_scope.searchZemlep_Res.searchDate);
            displZvezda(); dispRub();
            if($("#ch-table-search-header").hasClass('display-none')){
                $("#ch-table-search-header").removeClass('display-none')
            }
        }
            $("#sh-table").append(function () {
                var text = "", i = 0;
                for (; i < search_scope.searchZemlep_Res.valuesCharacters.length; i++) {
                    text += "<tr>"
                        + "<td>"
                        + search_scope.searchZemlep_Res.valuesCharacters[i].funcCodeName
                        + "</td>"
                        + "<td>"
                        + search_scope.searchZemlep_Res.valuesCharacters[i].dateVal
                        + "</td>"
                        + "<td>"
                        + search_scope.searchZemlep_Res.valuesCharacters[i].zoneNum
                        + "</td>"
                    if(!search_scope.isNalog) {
                        text += "<td>"
                            + search_scope.searchZemlep_Res.valuesCharacters[i].costD
                            + "</td>"
                            + "<td>"
                            + search_scope.searchZemlep_Res.valuesCharacters[i].costR
                            + "</td>";
                    }
                    if(search_scope.isNalog){
                        text+="<td>"
                            + search_scope.searchZemlep_Res.valuesCharacters[i].costNB
                            + "</td>"
                    }
                    text+= "</tr>"
                }
                return text;
            });
            $("#text-sh").append(function () {
                var j = 0;
                var text = search_scope.searchZemlep_Res.remark;
                return text;
            });
    });

    $("#view-ch-search-2").click(function(){
        closeDialog();
        $("#svedenia-sh").removeClass("display-none");
        $("#cadastr-stoim-search").modal("show");
    })

    $("#cadastd-view").click(function(){
        closeDialog();
        clearDialogCadNum();
        searchByCadastrModal();
        $("#svedenia-cadastr").removeClass("display-none");
        $("#cadastr-stoim-search").modal("show");
    });

    $("#cadastr-search-btn").click(function(){
        closeClearSearchPoint();
        var soato = $("#soato").val(),
            blockNum = $("#numblock").val(),
            parcelNum = $("#numuch").val();

        httpServices.getres_byCadastr(search_scope, soato, blockNum, parcelNum, search_scope.isNalog,search_scope.date_search);

        clearDivCadNum();

        if(CADASTR_RES != -1) {
            searchByCadastr();

            if ($("#res-search-cadestr-1").hasClass("display-none")) {
                $("#res-search-cadestr-1").removeClass("display-none")
            }
        }else{
            if (!$("#res-search-cadestr-1").hasClass("display-none")) {
                $("#res-search-cadestr-1").addClass("display-none")
            }
        }
    })

    $("#printer-cadastr").click(function(){
        printDiv("svedenia-cadastr");
    })

    $("#printer-addr").click(function(){
        printDiv("svedenia-adr");
    })

    $("#printer-st").click(function(){
        printDiv("svedenia-st");
    })
    $("#printer-vnp").click(function(){
        printDiv("svedenia-vnp");
    })
    $("#printer-sh").click(function(){
        printDiv("svedenia-sh");
    })

    $("#1-search-by-addr-abl-select").append(function(){

        httpServices.getOblAddr(search_scope)
        var select = option(-1, -1, "");
        $.each(search_scope.searchAddr_obl, function (i) {
            select += option(i, search_scope.searchAddr_obl[i][0], search_scope.searchAddr_obl[i][1]);
        })
        return select;
    });

    $("#1-search-by-addr-abl-select").change(function(){
        change_1_data();
        changeSearchByAddrObl();
        addresInit();
        var ateId = ($("#1-search-by-addr-abl-select").val());
        if(ateId !=-1 && ateId !=null && ateId!=undefined){
            addres.region = ateId;
            search_scope.selectedData_1 = ateId;
            httpServices.getAddressElement(search_scope, 1, ateId);
            $("#2-select-search").append(function(){
                var select = "";
                $.each(search_scope.searchAddr_El, function (i) {
                    select += option(i, search_scope.searchAddr_El[i][0], search_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            $("#search-by-addr-2").removeClass("display-none")
                append_2input();
        }
    });

    // Выбор на этапе 2 из 6ти
    $("#2-select-search").change(function(){
        append_2input()
    });

    $("#2-select-search-data").change(function(){
        change_2_data();
        var val = $("#2-select-search-data").val();
        if(val != -1 && search_scope.searchByAddr_2 == 9){
            $("#search-by-addr-btn").removeAttr("disabled");
        }else{
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if(val != -1 && search_scope.searchByAddr_2 != 9) {
            search_scope.selectedData_2 = val;

/*            addres.area='';
            addres.ss='';
            addres.snp='';
            addres.bjd='';
            addres.road='';
            addres.km='';
            addres.street='';*/

            if(addres.typeOut==2){
                addres.area = val;
            } else if(addres.typeOut==4){
                addres.snp = val;
            }
            httpServices.getAddressElement(search_scope, search_scope.searchByAddr_2, val);
            $("#3-select-search").append(function(){
                var select = "";
                $.each(search_scope.searchAddr_El, function (i) {
                    select += option(i, search_scope.searchAddr_El[i][0], search_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_3input();
            $("#search-by-addr-3").removeClass("display-none")
        }else{
            search_scope.searchByAddr_numDom =  val;
        }
        });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Выбор на этапе 3 из 6ти
    $("#3-select-search").change(function(){
        append_3input();
    });


    $("#3-select-search-data").change(function(){
        change_3_data();
        var val = $("#3-select-search-data").val();
        if(val != -1 && search_scope.searchByAddr_3 == 9){
            $("#search-by-addr-btn").removeAttr("disabled");
        }else{
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if(val != -1 && search_scope.searchByAddr_3 != 9) {

            search_scope.selectedData_3 = val;
            setAddresbyTypeOut(val, addres.typeOut);

            httpServices.getAddressElement(search_scope, search_scope.searchByAddr_3, val);
            $("#4-select-search").append(function(){
                var select = "";
                $.each(search_scope.searchAddr_El, function (i) {
                    select += option(i, search_scope.searchAddr_El[i][0], search_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_4input();
            $("#search-by-addr-4").removeClass("display-none")
        }else{
            search_scope.searchByAddr_numDom =  val;
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //EXAMPLE!!!
    // Выбор на этапе 4 из 6ти
    $("#4-select-search").change(function(){
        append_4input();
    });


    $("#4-select-search-data").change(function(){
        change_4_data();
        var val = $("#4-select-search-data").val();
        if(val != -1 && search_scope.searchByAddr_4 == 9){
            $("#search-by-addr-btn").removeAttr("disabled");
        }else{
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if(val != -1 && search_scope.searchByAddr_4 != 9) {
            search_scope.selectedData_4 = val;

            setAddresbyTypeOut(val, addres.typeOut)

            httpServices.getAddressElement(search_scope, search_scope.searchByAddr_4, val);
            $("#5-select-search").append(function(){
                var select = "";
                $.each(search_scope.searchAddr_El, function (i) {
                    select += option(i, search_scope.searchAddr_El[i][0], search_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_5input();
            $("#search-by-addr-5").removeClass("display-none")
        }else{
            search_scope.searchByAddr_numDom =  val;
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Выбор на этапе 5 из 6ти
    $("#5-select-search").change(function(){
        append_5input();
    });


    $("#5-select-search-data").change(function(){
        change_5_data();
        var val = $("#5-select-search-data").val();
        if(val != -1 && search_scope.searchByAddr_5 == 9){
            $("#search-by-addr-btn").removeAttr("disabled");
        }else{
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if(val != -1 && search_scope.searchByAddr_5 != 9) {
            search_scope.selectedData_4 = val;
            addres.typeOut = $("#5-select-search").val();
            setAddresbyTypeOut(val, addres.typeOut)
            httpServices.getAddressElement(search_scope, search_scope.searchByAddr_5, val);
            $("#6-select-search").empty();
            $("#6-select-search-data").empty();
            $("#6-select-search").append(function(){
                var select = "";
                $.each(search_scope.searchAddr_El, function (i) {
                    select += option(i, search_scope.searchAddr_El[i][0], search_scope.searchAddr_El[i][1]);
                })
                return select;
            })
            append_6input();
            $("#search-by-addr-6").removeClass("display-none")
        }else{
            search_scope.searchByAddr_numDom =  val;
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Выбор на этапе 6 из 6ти

    $("#6-select-search").change(function(){
        append_6input();
    });


    $("#6-select-search-data").change(function(){
        var val = $("#6-select-search-data").val();
        if(val != -1 ){
            $("#search-by-addr-btn").removeAttr("disabled");
        }else{
            $("#search-by-addr-btn").attr("disabled", "disabled");
        }
        if(val!=-1){
            search_scope.searchByAddr_numDom =  val;
        }

    })

    $("#search-by-addr-btn").click(function(){

        closeAddrSearch();
        httpServices.getAddressResult(search_scope, search_scope.searchByAddr_numDom, search_scope.isNalog,search_scope.date_search);

        var data = search_scope.resultAddrSearch;
        if(data.code == 1){
            // поиск по кадастровому номеру
            if(data.cadnumList.length == 1){
                console.log(data.cadnumList)
                console.log(data.cadnumList.length)
                if($("#res-search-addr-1-1").hasClass("display-none")){
                    $("#res-search-addr-1-1").removeClass("display-none");
                }
                $("#cad-num-by-addr-1").text("")
                $("#cad-num-by-addr-1").text(data.cadnumList[0])
            }else{
                if($("#res-search-addr-1-2").hasClass("display-none")){
                    $("#res-search-addr-1-2").removeClass("display-none");
                }
                $("#cad-num-by-addr-2").empty();
                $("#addr-2-view").attr("disabled", "disabled");
                $("#cad-num-by-addr-2").append(function(){
                var text = "";
                for(var i= 0; i<data.cadnumList.length; i++){
                    var cad = data.cadnumList[i];
                    var soato = Number(cad.substring(0, 10)),
                        blockNum = Number(cad.substring(10, 12)),
                        parcelNum = Number(cad.substring(12, 19));
                    text += "<div class='radio'><label class='radio-inline'><input type='radio' onclick='onCadastrAddr("+soato+", "+blockNum+", "+parcelNum+")' name='optradio-addr' id='search-by-cadastr-" + i + "'>Земельный часток с кадастровым номером " + cad +"</label></div>";
                }
                    return text;
                })
            //<div class="radio"><label class="radio-inline"><input type="radio" name="optradio2" checked id="search-by-cadastr-1">По кадастровому номеру</label></div>

                /*for(var i= 0; i<data.cadnumList.length; i++){
                    var id = 'search-by-cadastr-' + i;
                    document.getElementById(id).addEventListener('click', onCadastrAddr, false);
                }*/
            }
        }else if(data.code ==2){
            // поиск по адресной точке
            if($("#res-search-addr-1-3").hasClass("display-none")){
                $("#res-search-addr-1-3").removeClass("display-none");
            }

            $("#cad-num-by-addr-3").empty();
            $("#cad-num-by-addr-3").append(function(){
                return data.address;
            });

            $("#cad-num-by-addr-3-lable").empty();
            $("#cad-num-by-addr-3-lable").append(function(){
                return "Участки не найдены. Сведения об объекте:";
            });

        }else{
            alert(data.response)
        }
    });

    $("#addr-2-view").click(function(){
        httpServices.getres_byCadastr(search_scope, cadNum.soato, cadNum.blockNum, cadNum.parceNum, search_scope.isNalog,search_scope.date_search);
        if(CADASTR_RES>0) {
            closeDialog();
            clearDialogCadNum();
            searchByCadastrModal();
            $("#svedenia-cadastr").removeClass("display-none");
            $("#cadastr-stoim-search").modal("show");
        }
    });

    $("#addr-1-view").click(function(){
        var cad = $("#cad-num-by-addr-1").text();

        var soato = Number(cad.substring(0, 10)),
            blockNum = Number(cad.substring(10, 12)),
            parcelNum = Number(cad.substring(12, 19));

        httpServices.getres_byCadastr(search_scope, soato, blockNum, parcelNum, search_scope.isNalog,search_scope.date_search);

        if(CADASTR_RES>0) {
            closeDialog();
            clearDialogCadNum();
            searchByCadastrModal();
            $("#svedenia-cadastr").removeClass("display-none");
            $("#cadastr-stoim-search").modal("show");
        }
    });

    $("#addr-3-view").click(function(){
        closeDialog();
        var addr = search_scope.resultAddrSearch.address,
         zone = (search_scope.resultAddrSearch.zones).split(",");
        if(zone.length>0) {

            var result = httpServices.getAddressResultByAddres(search_scope, addr, zone, search_scope.isNalog, search_scope.date_search);
            searchByAddrCleanModal();
            if (result == 1) {
                searchByAddrSadModal();
                $("#svedenia-adr").removeClass("display-none");
                $("#cadastr-stoim-search").modal("show");
            } else{
                alert("По данному адресу нет зоны")
            }
        } else{
            alert("По данному адресу нет зоны")
        }
    });

    $("#soato").keyup(function(event){
        isSearchCadastr();
    })

    $("#numblock").keyup(function(event){
        isSearchCadastr();
    })

    $("#numuch").keyup(function(event){
        isSearchCadastr();
    });

    $("#soato").keydown(function(event){
        var code = (event.keyCode);
        if(code != 8 && code !=9 && code !=46&& code !=37 && code != 39) {
            if(code==16 ||code==187 ||code==107 ||code==189 ||code==109){
                return false;
            } else if (($("#soato").val()).length == 10) {
                return false;
            }
        }
        return true;
    });

    $("#numblock").keydown(function(event){
        var code = (event.keyCode);
        if(code != 8 && code !=9 && code !=46&& code !=37 && code != 39) {
            if(code==16 ||code==187 ||code==107 ||code==189 ||code==109){
                return false;
            } else if (($("#numblock").val()).length == 2) {
                return false;
            }
        }
        return true;
    });

    $("#numuch").keydown(function(event){
        var code = (event.keyCode);
        if(code != 8 && code !=9 && code !=46&& code !=37 && code != 39) {
            if(code==16 ||code==187 ||code==107 ||code==189 ||code==109){
                return false;
            } else if (($("#numuch").val()).length == 6) {
                return false;
            }
        }
        return true;
    });

    $("#cadastr-stoim-search").on('hidden.bs.modal', function () {
        $("#cadastr-addres-cad").empty();
        $("#cadastr-search-table-head").empty();
        $("#cadastr-nalog-table").empty();
        $("#cadastr-num-cad").empty();
        $("#cadastr-pl-cad").empty();
        $("#cadastr-cel-klassif-cad").empty();
        $("#cadastr-cel-mestn-cad").empty();
        $("#cadastr-table").empty();
        $("#text-cadastr").empty();

        $("#cadastr-addres-addr").empty();
        $("#cost-table-addr").empty();
        $("#addr-table").empty();
        $("#text-addr").empty();

        $("#cadastr-addres-st").empty();
        $("#cadastr-zone-st").empty();
        $("#cadastr-num-st").empty();
        $("#st-search-table-header").empty();
        $("#st-table").empty();
        $("#text-st").empty();

        $("#cadastr-addres-vnp").empty();
        $("#cadastr-zone-vnp").empty();
        $("#cadastr-num-vnp").empty();
        $("#vnp-search-table-header").empty();
        $("#vnp-table").empty();
        $("#text-vnp").empty();

        $("#cadastr-addres-sh").empty();
        $("#cadastr-name-sh").empty();
        $("#cadastr-num-sh").empty();
        $("#ch-table-search-header").empty();
        $("#sh-table").empty();
        $("#text-sh").empty();

        if (pdf_created) {
            delete_pdf_document(search_scope);
        }
    });

    $(".pdf-link").click(function () {
        searchPdf.isNb = search_scope.isNalog;
        if (!pdf_created) {
            httpServices.createPdf(search_scope, searchPdf);
            pdf_created = true;
        }
        window.location = httpServices.getPdf(search_scope);
    });

});
