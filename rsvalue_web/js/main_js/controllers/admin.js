/**
 * Created by tihonovichp on 08.11.2016.
 */

$(document).ready(function () {
    if (param('__p', '__u') != -1) {
        httpServices.autorization_getUser();
        console.log(AUTHOBJ.personData==null)
        if(AUTHOBJ.personData == null || AUTHOBJ == null) {
            $("#str-index").css("display", "none");
            $("#index-404").css("display", "block");
        } else{
            $("#str-index").css("display", "block");
            $("#index-404").css("display", "none");
            $("#auth-name").text(AUTHOBJ.personData);

            httpServices.get_groups(admin_scope);
            httpServices.get_orgs(admin_scope);
            httpServices.get_subjects(admin_scope);
            httpServices.get_logins(admin_scope);
            httpServices.get_accounts_records(admin_scope, 1, 5);
            httpServices.get_row_count();
            delete_admin_scope.accounts = admin_scope.accounts;
            edit_admin_scope.accounts = admin_scope.accounts;
            delete_row_count = row_count;
            edit_row_count = row_count;
            select_group_id = 3;
            select_org_id = 300;
            init_logins();
            create_ogrs(admin_scope.tors);
            create_groups(admin_scope.groups);
            create_subjects(admin_scope.subjects);
            create_delete_table(delete_admin_scope.accounts);
            create_edit_table(edit_admin_scope.accounts);
        }
    }
});