/**
 * Created by Shcherbina on 22.07.2016.
 */

function funClick(e){
    var id = $(this).attr("id");
    ID_VIEW = id.split("_")[0];
    ID_FUN = id.split("_")[1];

    if($(this).hasClass("selected-tr")){
        $(this).removeClass("selected-tr");
    }else{
        $(".cursor-select").removeClass("selected-tr")
        $(this).addClass("selected-tr");
    }
}

function funDbClick(e){
    //alert(838388383)
}

function setViewEdit(){
    if(ID_EDIT != ID_VIEW) {
        var scope = [];
        httpServices.getInfoForEditView(ID_VIEW, ID_FUN, scope);
        httpServices.getDocsForEditView(ID_VIEW, scope);
        httpServices.getLayersForEditView(ID_VIEW, scope);
        ID_EDIT = ID_VIEW;
        httpServices.getDocTypeList(ID_VIEW, scope);
        edit_scope = scope;
        httpServices.getZonesForEditView(ID_VIEW, ID_FUN, scope);
        view_scope = scope;
    }
}

function plus_del_doc_edit_view(id, type){
    //alert(id)
    var add = "_add-edit", del = "_del-edit", dwn = "_dwn-edit";
    var id_add = "#" + id + add,
        id_del = "#" + id + del,
        id_dwn = "#" + id + dwn;
    if(type == 'del'){
        ($(id_add).prev()).removeClass("display-none");
        ($(id_del).prev()).addClass("display-none");
        ($(id_dwn).prev()).addClass("display-none");
    }else if(type == 'add'){
        ($(id_add).prev()).addClass("display-none");
        ($(id_del).prev()).removeClass("display-none");
        ($(id_dwn).prev()).removeClass("display-none");
    }
}