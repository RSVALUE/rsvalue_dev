/**
 * Created by Shcherbina on 03.06.2016.
 */

var NUM_DOC = 0;
var AUTH_FORMDATA, AUTHOBJ;

function disable_all() {
    $("#meth-value").attr("disabled","disabled");
    $("#func-value").attr("disabled","disabled");
    $("#town").attr("disabled","disabled");
    $("#area").attr("disabled","disabled");
    $("#terr-value").attr("disabled","disabled");
    $("#polygon").attr("disabled","disabled");
    $("#point").attr("disabled","disabled");
    $("#polygon").prop('checked', false);
    $("#point").prop('checked', false);
    $("#people").attr("disabled","disabled");
    $("#date-value").attr("disabled","disabled");
}

function empty_all() {
    $("#meth-value").empty();
    $("#terr-value").empty();
    $("#area").empty();
    $("#town").empty();
    $("#func-value").empty();
    $("#next-load-1").attr("disabled","disabled");
    $("#people").val('');
    $("#date-value").val('');
    $("#note").val('');
    $("#date-value").val('');
}

function disable_by_meth(){
    $("#func-value").attr("disabled","disabled");
    $("#town").attr("disabled","disabled");
    $("#area").attr("disabled","disabled");
    $("#terr-value").attr("disabled","disabled");
}

function empty_by_meth() {
    $("#terr-value").empty();
    $("#area").empty();
    $("#town").empty();
    $("#func-value").empty();
    $("#next-load-1").attr("disabled","disabled");
    $("#date-value").val('');
}

function disable_by_terr(){
    $("#func-value").attr("disabled","disabled");
    $("#town").attr("disabled","disabled");
    $("#area").attr("disabled","disabled");
}

function empty_by_terr() {
    $("#area").empty();
    $("#town").empty();
    $("#func-value").empty();
    $("#next-load-1").attr("disabled","disabled");
}

function disable_by_area(){
    $("#func-value").attr("disabled","disabled");
    $("#town").attr("disabled","disabled");
}

function empty_by_area() {
    $("#town").empty();
    $("#func-value").empty();
    $("#next-load-1").attr("disabled","disabled");
}

function disable_by_town(){
    $("#func-value").attr("disabled","disabled");
}

function empty_by_town() {
    $("#func-value").empty();
    $("#next-load-1").attr("disabled","disabled");
}

function meth_change() {
    disable_by_meth();
    empty_by_meth();
}

function type_change() {
    disable_all();
    empty_all();
}

function terr_change() {
    disable_by_terr();
    empty_by_terr();
}

function area_change() {
    disable_by_area();
    empty_by_area();
}

function town_change() {
    disable_by_town();
    empty_by_town();
}

function redStar () {
    return "<span style=\"color: red\">*</span>";
}

function openTD(){
    return "<td>";
}

function closeTD(){
    return "</td>";
}

function openTR(){
    return "<tr>";
}

function openTRID(id){
    return "<tr class='cursor-select' id=" + id + " >";
}


function closeTR(){
    return "</tr>";
}

function closeTH(){
    return "</th>";
}

function openTH(){
    return "<th>";
}

function td_win_2 (id, td_text) {
    return "<td class=\"font_small\" id="+id+" >" + td_text + closeTD();
}

function td_win_2_imp (id, td_text) {
    return "<td class=\"font_small important\" id="+id+" >" + td_text + closeTD();
}

function td_win_3 (td_text) {
    return "<td class=\"font_small\" >" + td_text + closeTD();
}

function td_win_2_star (id, td_text) {
    return "<td class=\"font_small\" id="+id+" >" + td_text + redStar() + closeTD();
}

function openTDaction () {
    return "<td style=\"text-align: center\">";
}

function td_paperclip () {
    return  "<td>"+
        "<span class=\"glyphicon glyphicon-paperclip size-span-lg marg-btn-r \" ></span>"+
            "</td>";
}

function td_sort (id, name){
    return "<td>" + name+"<span id="+id +"-sort-view" + " class='glyphicon glyphicon-sort cursor-pointer'></span></td>";
}

function td_simple (name){
    return "<td>" + name+"</td>";
}

function remove (id_input) {
    return "<label class=\"btn btn-file btn-link\" style=\"display: none\">" +
        "<span class=\"glyphicon glyphicon-paperclip size-span-lg marg-btn-r \" ></span>"+
        "<span class=\"glyphicon glyphicon-trash size-span-lg marg-btn-r span-close-color\" ></span> <input type=\"button\" id=" + id_input +
        " style=\"display: none;\"></label>";
}

function remove_cl (id_input, cl) {
    return "<label class=\"btn btn-file btn-link\" style=\"display: none\">" +
        "<span class=\"glyphicon glyphicon-paperclip size-span-lg marg-btn-r \" ></span>"+
        "<span class=\"glyphicon glyphicon-trash size-span-lg marg-btn-r span-close-color\" ></span> <input type=\"button\" id=" + id_input +
        " style=\"display: none;\" class=" +cl+ "></label>";
}

function removeDialog_3 (id_input) {
    return "<label class=\"btn btn-file btn-link\" style=\"display: none\">" +
        "<span class=\"glyphicon glyphicon-trash size-span-lg marg-btn-r span-close-color\" ></span> <input type=\"button\" id=" + id_input +
        " style=\"display: none;\"></label>";
}

function remove_edit (id_input, cl, href) {
    return "<label class=\"btn btn-file btn-link\">"
        +"<span class=\"glyphicon glyphicon-circle-arrow-down span-add-color size-span-lg\" ></span>"
        +"<input type=\"button\" dowload id=" + id_input +"_dwn-edit"+" style=\"display: none;\" class=" +cl+ ">"
        +"</label>"
        +"<label class=\"btn btn-file btn-link\">"+ "<span class=\"glyphicon glyphicon-trash size-span-lg marg-btn-r span-close-color\" ></span> <input type=\"button\" id=" + id_input +"_del-edit"
        +" style=\"display: none;\" class=" +cl+ ">"+"</label>"
        +"<label class=\"btn btn-file btn-link\">"+ "<span class=\"glyphicon glyphicon-plus size-span-lg marg-btn-r span-add-color display-none\"></span> <input type=\"file\" id=" + id_input +"_add-edit"
        +" style=\"display: none;\" class=" +cl+ ">"+"</label>";
}

function dwn_view (id_input, cl) {
    return "<label class=\"btn btn-file btn-link\">" +
        "<span class=\"glyphicon glyphicon-circle-arrow-down span-add-color size-span-lg\" ></span>"+"<input type=\"button\" id=" + id_input +"-dwn-edit"+
        " style=\"display: none;\" class=" +cl+ ">"+
        "</label>";
}

function dwn_load (path) {
    return "<label class=\"btn btn-file btn-link\"><a href='"+path+"' download><span class=\"glyphicon glyphicon-circle-arrow-down span-add-color size-span-lg\" ></span></a></label>";
}

function addButtonFile (id_input, title) {
    return "<label class=\"btn btn-file btn-link btn-block\">" +
        "<span class=\"btn btn-default btn-block\">"+title+"</span>" +
        "<input type=\"file\" multiple=\"multiple\" id=" + id_input +
        " style=\"display: none;\">"+
        "</label>";
}

function plusFile (id_input) {
    return "<label class=\"btn btn-file btn-link\">" +
        "<span class=\"glyphicon glyphicon-plus size-span-lg span-add-color\"></span>" +
        "<input type=\"file\" id=" + id_input +
        " style=\"display: none;\">"+
        "</label>";
}

function plusFile_non_imp (id_input) {
    return "<label class=\"btn btn-file btn-link\">" +
        "<span class=\"glyphicon glyphicon-plus size-span-lg span-add-color\"></span>" +
        "<input type=\"file\" multiple=\"multiple\" id=" + id_input +
        " style=\"display: none;\">"+
        "</label>";
}

function plusFileDialog_3 (id_input, id_lable) {
    return "<label class=\"btn btn-file btn-link\" id="+id_lable+">" +
        "<span class=\"glyphicon glyphicon-plus size-span-lg span-add-color\"></span>" +
        "<input type=\"file\" accept='application/zip' id=" + id_input +
        " style=\"display: none;\">"+
        "</label>";
}

function plus (id_input) {
    return "<a href='#load-modal-4' id="+id_input+" data-dismiss='modal'><label class=\"btn btn-file btn-link\">" +
        "<span class='glyphicon glyphicon-plus size-span-lg span-add-color' onclick='funfun()' id=5></span>"+
        "<input type='button' style='display: none;'>"+
        "</label></a>";
}

function plus_edit (id_input) {
    return "<a href='#load-modal-4' id="+id_input+" data-dismiss='modal'><label class=\"btn btn-file btn-link\">" +
        "<span class='glyphicon glyphicon-plus size-span-lg span-add-color' onclick='funfun_edit()' ></span>"+
        "<input type='button' style='display: none;'>"+
        "</label></a>";
}

function divOpen_12() {
    return "<div class=\"col-sm-12\">";
}

function divClose() {
    return "</div>";
}

function buttonFile (id_input, text) {
    return "<label class=\"btn btn-file btn-link\">" +
        "<span>"+text+"</span>" +
        "<input type=\"file\" id=" + id_input +
        " style=\"display: none;\">"+
        "</label>";
}

function option (id, value, title){
    return "<option value='" + value + "' id='"+id+"'>" + title + "</option>";
}

function option_class (id, value, clazz){
    return "<option value='" + value + "' class='"+clazz+"' id='"+id+"'></option>";
}

function tdText (text) {
    return "<td>" + text + "</td>";
}

function tdTextId (text, id) {
    return "<td id='"+id+"'>" + text + "</td>";
}

function plusFile_new (id_input) {
    return "<label class=\"btn btn-file btn-link\" style=\"display: none\">" +
        "<span class=\"glyphicon glyphicon-plus size-span-lg span-add-color\"></span>" +
        "<input type=\"file\" multiple=\"multiple\" onclick='clickDoc()' id=" + id_input +
        " style=\"display: none;\">"+
        "</label>";
}
function remove_new (id_input) {
    return "<label class='btn btn-file btn-link remove_new'>" +
        "<span class=\"glyphicon glyphicon-paperclip size-span-lg marg-btn-r \" ></span>"+
        "<span class=\"glyphicon glyphicon-trash size-span-lg marg-btn-r span-close-color\" ></span> <input type=\"button\" id=" + id_input +
        " style=\"display: none;\"></label>";
}

function getLi (col) {
    var li = "<li><a  id=\"prev-str-table\">&laquo;</a></li>";
    for(var i=1; i<=col; i++) {
        li += "<li id=\""+i+"-table\"><a>"+i+"</a></li>";
    }
    li+="<li id=\"next-str-table\"><a>&raquo;</a></li>";
    return li;
}

function setDisabledBounds(){
    ($("#add-bound")).attr("disabled", "disabled")
    ($("#lable-bound")).attr("disabled", "disabled")
}

function delDisabledBounds(){
    ($("#add-bound")).removeAttr("disabled")
    ($("#lable-bound")).removeAttr("disabled")
}

function desBounds(){
    if(isBounds()){
        delDisabledBounds();
    }else {
        setDisabledBounds();
    }
}

function isBounds(){
    return (arr_id.type_val == 10 || (arr_id.type_val == 20 && arr_id.polyType == 20));
}

function isSaveClick(){
    if((Zone_map && Bound_map && isBounds()) || (Zone_map && !isBounds())){
        $("#ok-3").removeAttr("disabled");
        $("#preview-btn").removeAttr("disabled");
    }else {
        $("#ok-3").attr("disabled", "disabled");
        $("#preview-btn").attr("disabled", "disabled");
    }
}

function validZoneBound(type, val){
    if(type == 'zone'){
        Zone_map = val;
    } else {
        Bound_map = val;
    }
    isSaveClick();
}

function setValidZoneBound(type, val){
    if(type == 'zone'){
        Zone_map = val;
    } else {
        Bound_map = val;
    }
}

function openModal() {
    $('#load-modal-1').css('pointer-events','none');
    $('#load-modal-2').css('pointer-events','none');
    $('#load-modal-3').css('pointer-events','none');
    $('#load-modal-4').css('pointer-events','none');
    $('#load-modal-5').css('pointer-events','none');
    $('#load-modal-6').css('pointer-events','none');
    $('#load-modal-7').css('pointer-events','none');

    $('#load-modal-1').css('opacity', 0.8);
    $('#load-modal-2').css('opacity', 0.8);
    $('#load-modal-3').css('opacity', 0.8);
    $('#load-modal-4').css('opacity', 0.8);
    $('#load-modal-5').css('opacity', 0.8);
    $('#load-modal-6').css('opacity', 0.8);
    $('#load-modal-7').css('opacity', 0.8);
}

function closeModal() {
    $('#load-modal-1').css('pointer-events','');
    $('#load-modal-2').css('pointer-events','');
    $('#load-modal-3').css('pointer-events','');
    $('#load-modal-4').css('pointer-events','');
    $('#load-modal-5').css('pointer-events','');
    $('#load-modal-6').css('pointer-events','');
    $('#load-modal-7').css('pointer-events','');

    $('#load-modal-1').css('opacity', 1);
    $('#load-modal-2').css('opacity', 1);
    $('#load-modal-3').css('opacity', 1);
    $('#load-modal-4').css('opacity', 1);
    $('#load-modal-5').css('opacity', 1);
    $('#load-modal-6').css('opacity', 1);
    $('#load-modal-7').css('opacity', 1);
}

function openModalIndex() {
    $('#index-body').css('pointer-events','none');

    $('#index-body').css('opacity', 0.5);
}

function closeModalIndex() {
    $('#index-body').css('pointer-events','');

    $('#index-body').css('opacity', 1);
}
/*
function searhAtebyName(name){
    $("#town_table").
}*/

function equals(text_1, text_2){
    var res = true;
    if(text_1 != text_2){
        res = false;
    } else if(text_1.length != text_2.length){
        res = false;
    }
    return res;
}

function isNotEmptyEquals (text_1, text_2){
    var res = true;
    if(text_2.length == 0){
        res = false;
    } else if(text_1.length != text_2.length){
        res = false;
    }
    return res;
}

function param(token, user)
{
    var Params = location.search.substring(1).split("&");
    var tokenV,
        userV;
    if(Params.length == 2) {
        for (var i = 0; i < Params.length; i++) {
            var arr = Params[i].split("=");
            if (arr[0] == token) {
                tokenV = arr[1];
            } else if (arr[0] == user) {
                userV = arr[1];
            }
        }
        AUTH_FORMDATA = {"username": userV, "token": tokenV};
        return 1;
    }
    return -1
}

function createPDFImg(path){
    return "<a href=\"#\" onclick=\"window.open('"+path+"', '_blank', 'fullscreen=yes'); return false;\"><img src='img/pdf.png' style='width: 20%'></a>";
}

function replaceDate(date){
    var arr = date.split('.');
    return arr[2]+"-" + arr[1] + "-" + arr[0];
}

function openTable(a){
    closeAllTable();
    switch (a){
        case 1: openCadTable(); break;
        case 2: openaddrTable(); break;
        case 3: openstTable(); break;
        case 4: openvnpTable(); break;
        case 5: openshTable(); break;
        case 6: openAllTable(); break;
        case 7: openSTable(); break;
    }
}

function closeAllTable(){
    if(!$("#statictic-cadastr").hasClass("display-none")){
        $("#statictic-cadastr").addClass("display-none")
    }
    if(!$("#statictic-addr").hasClass("display-none")){
        $("#statictic-addr").addClass("display-none")
    }
    if(!$("#statictic-st").hasClass("display-none")){
        $("#statictic-st").addClass("display-none")
    }
    if(!$("#statictic-vnp").hasClass("display-none")){
        $("#statictic-vnp").addClass("display-none")
    }
    if(!$("#statictic-sh").hasClass("display-none")){
        $("#statictic-sh").addClass("display-none")
    }
    if(!$("#statictic-all").hasClass("display-none")){
        $("#statictic-all").addClass("display-none")
    }
    if(!$("#success-res").hasClass("display-none")){
        $("#success-res").addClass("display-none")
    }
}

function openAllTable(){
    if($("#statictic-all").hasClass("display-none")){
        $("#statictic-all").removeClass("display-none")
    }
}

function openCadTable(){
    if($("#statictic-cadastr").hasClass("display-none")){
        $("#statictic-cadastr").removeClass("display-none")
    }
}

function openaddrTable(){
    if($("#statictic-addr").hasClass("display-none")){
        $("#statictic-addr").removeClass("display-none")
    }
}

function openstTable(){
    if($("#statictic-st").hasClass("display-none")){
        $("#statictic-st").removeClass("display-none")
    }
}

function openvnpTable(){
    if($("#statictic-vnp").hasClass("display-none")){
        $("#statictic-vnp").removeClass("display-none")
    }
}

function openshTable(){
    if($("#statictic-sh").hasClass("display-none")){
        $("#statictic-sh").removeClass("display-none")
    }
}

function openSTable(){
    if($("#success-res").hasClass("display-none")){
        $("#success-res").removeClass("display-none")
    }
}